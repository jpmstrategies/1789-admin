<?php

declare(strict_types=1);

use Rector\CodeQuality\Rector\Class_\InlineConstructorDefaultToPropertyRector;
use Rector\Php54\Rector\Array_\LongArrayToShortArrayRector;
use Rector\Php73\Rector\FuncCall\JsonThrowOnErrorRector;
use Rector\Php74\Rector\LNumber\AddLiteralSeparatorToNumberRector;
use Rector\Php74\Rector\Property\TypedPropertyRector;
use Rector\Php80\Rector\FunctionLike\UnionTypesRector;
use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        __DIR__ . '/src'
    ]);

    // define sets of rules
    $rectorConfig->sets([
        // upgrade 7.4 first
        LevelSetList::UP_TO_PHP_74,
        // then 8.0
        LevelSetList::UP_TO_PHP_80,
        // then 8.1
        LevelSetList::UP_TO_PHP_81,
        // then 8.2
        LevelSetList::UP_TO_PHP_82,
    ]);

    $rectorConfig->skip([
        __DIR__ . 'composer.json',
        __DIR__ . 'composer.lock',
        __DIR__ . '/bin',
        __DIR__ . '/config',
        __DIR__ . '/logs',
        __DIR__ . '/plugins/legiscan',
        __DIR__ . '/src/Application.php',
        __DIR__ . '/src/Console',
        __DIR__ . '/src/View/AjaxView.php',
        __DIR__ . '/src/Shell',
        __DIR__ . '/tests/TestCase/ApplicationTest.php',
        __DIR__ . '/tests/bootstrap.php',
        __DIR__ . '/tmp',
        __DIR__ . '/vendor',
        __DIR__ . '/webroot',
        // never remove these rules
        JsonThrowOnErrorRector::class,
        TypedPropertyRector::class,
        // comment one-by-one
//        UnionTypesRector::class,
//        AddLiteralSeparatorToNumberRector::class,
//        LongArrayToShortArrayRector::class,
    ]);
};
