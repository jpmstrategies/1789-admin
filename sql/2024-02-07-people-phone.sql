use `1789_cornerstone`;

alter table `people`
    add column `phone` varchar(15) not null after `email`;