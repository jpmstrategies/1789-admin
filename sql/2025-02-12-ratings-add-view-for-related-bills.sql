drop view legiscan_bill_related;

create view legiscan_bill_related as
select bill_id,
       sast_bill_id     as related_bill_id,
       sast_bill_number as related_bill_number,
       sast_description as related_bill_method
from legiscan.ls_bill_sast
         inner join legiscan.ls_sast_type on ls_bill_sast.sast_type_id = ls_sast_type.sast_id;

--
-- Validation
--

-- original bill
select current.name, current.slug, related.name, related.slug, legiscan_bill_related.related_bill_method
from legiscan_bill_related
         inner join ratings current on legiscan_bill_related.bill_id = current.legiscan_bill_id
         inner join ratings related on legiscan_bill_related.related_bill_id = related.legiscan_bill_id
where current.id = '2ebd0c88-a788-11ef-aef9-bc24114a0529'
  and related.is_enabled = 1;

-- related bill
select current.name, current.slug, related.name, related.slug
from legiscan_bill_related
         inner join ratings current on legiscan_bill_related.bill_id = current.legiscan_bill_id
         inner join ratings related on legiscan_bill_related.related_bill_id = related.legiscan_bill_id
where current.id = '2ebe4347-a788-11ef-aef9-bc24114a0529' and related.is_enabled = 1;