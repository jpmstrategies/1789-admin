use cornerstone;

alter table ratings
    add column `vote_references` text null after `vote_recommendation_notes`;