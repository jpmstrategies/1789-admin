use cornerstone;

--
-- Rename the column in the bills table
--
alter table bills
    rename column current_bill_icon_id to current_bill_phase_icon_id;

alter table bills
    drop CONSTRAINT XFK9_bills;

alter table bills
    ADD CONSTRAINT XFK9_bills
        FOREIGN KEY (current_bill_phase_icon_id) REFERENCES bill_enums (id);

--
-- Add a new column called "Bill Rating Icon"
--

alter table bills
    add column current_bill_rating_icon_id char(36) null after current_bill_phase_icon_id;

alter table bills
    add CONSTRAINT XFK13_bills
        FOREIGN KEY (current_bill_rating_icon_id) REFERENCES bill_enums (id);