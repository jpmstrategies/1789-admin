use cornerstone;

alter table bills
    change `title` `official_title` varchar(255) not null;

alter table bills
    add column `explainer_title` varchar(255) not null after `official_title`;

update bills
set `explainer_title` = `official_title`;
