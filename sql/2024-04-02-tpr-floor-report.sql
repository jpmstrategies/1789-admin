use `1789_cornerstone`;

-- add `legiscan_bill_id` to `ratings`
alter table `ratings`
    add column `legiscan_bill_id` mediumint unsigned null after `slug`,
    add constraint XUN4_ratings unique (tenant_id, legiscan_bill_id);

-- increase length of criterias
alter table `criterias`
    modify column `th_detail_name` varchar(45) not null,
    modify column `th_vote_name` varchar(45) not null;

-- enable `use_criteria` configuration
update configurations
set value = 'yes'
where tenant_id = '42ed9a71-aab0-40b2-919d-370b6b325618'
  and configuration_type_id = '7c9d902c-c7b2-47bd-9e39-e764dfed527d';

-- add criterias
INSERT INTO criterias (id, tenant_id, name, th_detail_name, th_vote_name, method, party_id, sort_order, created,
                       modified)
VALUES ('220c3a1b-b537-4f47-9955-b6d9d9ef5c2b', '42ed9a71-aab0-40b2-919d-370b6b325618', 'Free Enterprise',
        'free-enterprise', 'free-enterprise', 'S', null, 1, now(), now());
INSERT INTO criterias (id, tenant_id, name, th_detail_name, th_vote_name, method, party_id, sort_order, created,
                       modified)
VALUES ('40bd5d5e-e20c-48e8-99eb-48456a3da215', '42ed9a71-aab0-40b2-919d-370b6b325618', 'Individual Liberty',
        'individual-liberty', 'individual-liberty', 'S', null, 5, now(), now());
INSERT INTO criterias (id, tenant_id, name, th_detail_name, th_vote_name, method, party_id, sort_order, created,
                       modified)
VALUES ('edde57fa-4f74-4a17-8909-d188627d276d', '42ed9a71-aab0-40b2-919d-370b6b325618', 'Limited Government',
        'limited-government', 'limited-government', 'S', null, 4, now(), now());
INSERT INTO criterias (id, tenant_id, name, th_detail_name, th_vote_name, method, party_id, sort_order, created,
                       modified)
VALUES ('b6d165d7-d907-4d45-a62a-2cb92c262a1d', '42ed9a71-aab0-40b2-919d-370b6b325618', 'Personal Responsibility',
        'personal-responsibility', 'personal-responsibility', 'S', null, 3, now(), now());
INSERT INTO criterias (id, tenant_id, name, th_detail_name, th_vote_name, method, party_id, sort_order, created,
                       modified)
VALUES ('a6b42a8e-2db7-4f77-b138-5ca3facc30b6', '42ed9a71-aab0-40b2-919d-370b6b325618', 'Property Rights',
        'property-rights', 'property-rights', 'S', null, 2, now(), now());

-- add criteria_details
insert criteria_details (id, tenant_id, criteria_id, name, sort_order, created, modified)
select uuid()                                 as id,
       '42ed9a71-aab0-40b2-919d-370b6b325618' as tenant_id,
       criterias.id                           as criteria_id,
       results.name                           as name,
       results.sort                           as sort_order,
       now()                                  as created,
       now()                                  as modified
from (select 'Positive' as name, 1 as sort
      union
      select 'Neutral' as name, 2 as sort
      union
      select 'Negative' as name, 3 as sort) results
         cross join criterias
where tenant_id = '42ed9a71-aab0-40b2-919d-370b6b325618'
order by criterias.name, sort;

UPDATE `1789_cornerstone`.tenants
SET name     = 'Texas Policy Research',
    type     = 'F',
    layout   = 'floor-report',
    modified = now()
WHERE id = '42ed9a71-aab0-40b2-919d-370b6b325618';

-- add positions
INSERT INTO positions (id, tenant_id, name, display_code, created, modified)
VALUES ('3399eb08-fdd8-4d24-974e-1531becadc57', '42ed9a71-aab0-40b2-919d-370b6b325618', 'Neutral', 'N',
        now(), now());
INSERT INTO positions (id, tenant_id, name, display_code, created, modified)
VALUES ('574177b8-83ea-47a7-b4db-256d55a88730', '42ed9a71-aab0-40b2-919d-370b6b325618', 'Vote No; Amend', 'O',
        now(), now());
INSERT INTO positions (id, tenant_id, name, display_code, created, modified)
VALUES ('e3aa11ba-498b-4230-a9a4-fbfe5a2a8571', '42ed9a71-aab0-40b2-919d-370b6b325618', 'No', 'O',
        now(), now());
INSERT INTO positions (id, tenant_id, name, display_code, created, modified)
VALUES ('ee74702d-af76-45bc-924f-7ad58499b6ee', '42ed9a71-aab0-40b2-919d-370b6b325618', 'Vote Yes; Amend', 'F',
        now(), now());
INSERT INTO positions (id, tenant_id, name, display_code, created, modified)
VALUES ('f8bfe9a1-5b45-41a2-ac9a-2dc1c22ff535', '42ed9a71-aab0-40b2-919d-370b6b325618', 'Yes', 'F',
        now(), now());

-- add new analysis fields
alter table `ratings`
    add column `fiscal_note`               text null after `description`,
    add column `digest`                    text null after `fiscal_note`,
    add column `vote_recommendation_notes` text null after `digest`;

-- add floor reports
create table floor_reports
(
    id                     char(36)             not null primary key,
    tenant_id              char(36)             not null,
    legislative_session_id char(36)             not null,
    chamber_id             char(36)             not null,
    report_date            date                 null,
    report_url             varchar(255)         null,
    import_log             text                 null,
    is_approved            tinyint(1) default 0 not null,
    created                datetime             null,
    modified               datetime             null,
    constraint XUN1_floor_reports
        unique (tenant_id, legislative_session_id, chamber_id, report_date),
    constraint XFK1_floor_reports
        foreign key (chamber_id) references chambers (id),
    constraint XFK2_floor_reports
        foreign key (legislative_session_id) references legislative_sessions (id),
    constraint XFK3_floor_reports
        foreign key (tenant_id) references tenants (id)
) charset = utf8mb3;

create index FK_chamber_id
    on floor_reports (chamber_id);

create index FK_legislative_session_id
    on floor_reports (legislative_session_id);

create index FK_tenant_id
    on floor_reports (tenant_id);

create table floor_reports_ratings
(
    id              char(36) not null primary key,
    tenant_id       char(36) not null,
    floor_report_id char(36) not null,
    rating_id       char(36) not null,
    sort_order      int      null,
    created         datetime null,
    modified        datetime null,
    constraint XUN1_floor_reports_ratings
        unique (tenant_id, floor_report_id, rating_id),
    constraint XFK1_floor_reports_ratings
        foreign key (floor_report_id) references floor_reports (id),
    constraint XFK2_floor_reports_ratings
        foreign key (rating_id) references ratings (id),
    constraint XFK3_floor_reports_ratings
        foreign key (tenant_id) references tenants (id)
) charset = utf8mb3;

create index FK_floor_report_id
    on floor_reports_ratings (floor_report_id);

create index FK_rating_id
    on floor_reports_ratings (rating_id);

create index FK_tenant_id
    on floor_reports_ratings (tenant_id);
