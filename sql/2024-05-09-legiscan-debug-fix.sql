use `cornerstone`;

alter table `ratings`
    drop constraint XUN4_ratings;

alter table `ratings`
    add constraint XUN4_ratings unique (tenant_id, chamber_id, legiscan_bill_id);