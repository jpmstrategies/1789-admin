use cornerstone;

-- add new types
alter table bill_enums
    modify column type enum ('stage', 'status', 'cta', 'progress', 'icon', 'phase-icon', 'rating-icon') not null;

-- reclassify icons as phase-icon
update bill_enums
set type = 'phase-icon'
where type = 'icon';

-- add new rating-icon type
INSERT INTO cornerstone.bill_enums (id, tenant_id, type, parent_id, name, description, css_class, branch, sort_order,
                                    created, modified)
VALUES (uuid(), '15629c88-a9cc-44a3-9cd7-69701d845f6b', 'rating-icon', null,
        'Bill w/Hat', '', 'current', null, 1, now(), now());

INSERT INTO cornerstone.bill_enums (id, tenant_id, type, parent_id, name, description, css_class, branch, sort_order,
                                    created, modified)
VALUES (uuid(), '15629c88-a9cc-44a3-9cd7-69701d845f6b', 'rating-icon', null, 'Poison',
        '', 'poison', null, 2, now(), now());

-- remap previous icons to rating icons
update bills b
    inner join
    (select bills.id  as billId,
            be_new.id as ratingIconId
     from bills
              inner join bill_enums be_org on be_org.id = bills.current_bill_rating_icon_id
              inner join bill_enums be_new on be_org.css_class = be_new.css_class and be_new.type = 'rating-icon'
     where current_bill_rating_icon_id is not null) results on b.id = results.billId
set b.current_bill_rating_icon_id = results.ratingIconId
where current_bill_rating_icon_id is not null;

-- remove old icon type
alter table bill_enums
    modify column type enum ('stage', 'status', 'cta', 'progress', 'phase-icon', 'rating-icon') not null;
