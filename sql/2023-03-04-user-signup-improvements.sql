create table signup_domains
(
    id       int unsigned auto_increment
        primary key,
    name     varchar(255)                        not null,
    regex    varchar(255)                        not null,
    modified timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
    created  timestamp default CURRENT_TIMESTAMP not null,
    constraint XUK1_signup_domains
        unique (regex)
) charset = utf8mb3;

alter table accounts
    modify column status varchar(255) not null default 'PENDING';

update accounts
set status = 'PENDING'
where status = 'P';

update accounts
set status = 'ACTIVE'
where status = 'A';

-- track who invited the user
alter table accounts
    add column `invited_by_user_id` char(36) null after `user_id`;