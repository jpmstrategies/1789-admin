alter table configuration_types
    add column `scope` varchar(255) null,
    add column `is_required`      tinyint(1)   not null default 0 after `is_public`,
    add column `required_message` varchar(255) null after `is_required`,
    add column `required_regex`   varchar(255) null after `required_message`,
    add column `field_mask`       varchar(255) null after `required_regex`,
    add column `sort_order`       int          not null after `required_regex`;

alter table configuration_types
    modify column `tab` varchar(255) null;

alter table users
    add column `user_details` json null after `security_code`;

INSERT INTO configuration_types (id, scope, name, description, data_type, default_value, tab, is_editable, is_enabled,
                                 is_public, is_required, required_message, required_regex, field_mask, sort_order,
                                 modified, created)
VALUES (uuid(), '', 'first_name', 'First Name', 'text', '', 'user_details', 1, 1, 1, 1, 'This field is required.', '.*',
        null, 1, now(), now());

INSERT INTO configuration_types (id, scope, name, description, data_type, default_value, tab, is_editable, is_enabled,
                                 is_public, is_required, required_message, required_regex, field_mask, sort_order,
                                 modified, created)
VALUES (uuid(), '', 'last_name', 'Last Name', 'text', '', 'user_details', 1, 1, 1, 1, 'This field is required.', '.*',
        null, 2, now(), now());

INSERT INTO configuration_types (id, scope, name, description, data_type, default_value, tab, is_editable, is_enabled,
                                 is_public, is_required, required_message, required_regex, field_mask, sort_order,
                                 modified, created)
VALUES (uuid(), '', 'phone', 'Phone', 'text', '', 'user_details', 1, 1, 1, 1, 'This field is required.',
        '^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}$', '999-999-9999', 4, now(), now());

INSERT INTO configuration_types (id, scope, name, description, data_type, default_value, tab, is_editable, is_enabled,
                                 is_public, is_required, required_message, required_regex, field_mask, sort_order,
                                 modified, created)
VALUES (uuid(), '', 'organization', 'Office / Organization', 'text', '', 'user_details', 1, 1, 1, 1,
        'This field is required.', '.*', null, 3, now(), now());
