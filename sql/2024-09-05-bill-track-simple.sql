use cornerstone;

DROP TABLE if exists bills;

DROP TABLE if exists bill_enums;

CREATE TABLE bill_enums
(
    id          CHAR(36)                                         NOT NULL PRIMARY KEY,
    tenant_id   CHAR(36)                                         NOT NULL,
    type        ENUM ('stage', 'status', 'cta', 'color', 'icon') NOT NULL,
    parent_id   CHAR(36)                                         NULL, -- Only relevant for certain types (status)
    name        VARCHAR(45)                                      NOT NULL,
    description VARCHAR(255)                                     NULL,
    css_class   VARCHAR(255)                                     NULL, -- Only relevant for bill_colors
    branch      ENUM ('exe', 'leg')                              NULL, -- Only relevant for bill_stages
    sort_order  INT                                              NULL, -- Only relevant for bill_stages
    created     DATETIME                                         NULL,
    modified    DATETIME                                         NULL,
    CONSTRAINT XUN1_bill_enums
        UNIQUE (tenant_id, type, name),
    CONSTRAINT XFK1_bill_enums
        FOREIGN KEY (tenant_id) REFERENCES tenants (id),
    CONSTRAINT XFK2_bill_enums
        FOREIGN KEY (parent_id) REFERENCES bill_enums (id)             -- Applies to statuses
)
    ENGINE = InnoDB
    CHARSET = utf8mb3;

CREATE TABLE bills
(
    id                     CHAR(36)             NOT NULL PRIMARY KEY,
    tenant_id              CHAR(36)             NOT NULL,
    legislative_session_id CHAR(36)             NOT NULL,
    filed_chamber_id       CHAR(36)             NOT NULL,
    name                   VARCHAR(255)         NOT NULL,
    description            LONGTEXT             NULL,
    notes                  LONGTEXT             NULL,
    sponsors               VARCHAR(255)         NULL,
    legiscan_bill_id       MEDIUMINT UNSIGNED   NULL,
    current_chamber_id     CHAR(36)             NULL,
    current_bill_stage_id  CHAR(36)             NULL, -- References bill_enums with type = 'stage'
    current_bill_status_id CHAR(36)             NULL, -- References bill_enums with type = 'status'
    current_bill_cta_id    CHAR(36)             NULL, -- References bill_enums with type = 'cta'
    current_bill_color_id  CHAR(36)             NULL, -- References bill_enums with type = 'color'
    current_bill_icon_id   CHAR(36)             NULL, -- References bill_enums with type = 'icon'
    house_rating_id        CHAR(36)             NULL, -- Still references the ratings table
    senate_rating_id       CHAR(36)             NULL, -- Still references the ratings table
    slug                   VARCHAR(255)         NOT NULL,
    created                DATETIME             NULL,
    modified               DATETIME             NULL,
    is_enabled             TINYINT(1) DEFAULT 0 NOT NULL,
    CONSTRAINT XUN1_bills
        UNIQUE (tenant_id, legislative_session_id, filed_chamber_id, name),
    CONSTRAINT XUN2_bills
        UNIQUE (tenant_id, slug),
    CONSTRAINT XUN3_bills
        UNIQUE (tenant_id, legiscan_bill_id),
    CONSTRAINT XFK1_bills
        FOREIGN KEY (tenant_id) REFERENCES tenants (id),
    CONSTRAINT XFK2_bills
        FOREIGN KEY (legislative_session_id) REFERENCES legislative_sessions (id),
    CONSTRAINT XFK3_bills
        FOREIGN KEY (filed_chamber_id) REFERENCES chambers (id),
    CONSTRAINT XFK4_bills
        FOREIGN KEY (current_chamber_id) REFERENCES chambers (id),
    CONSTRAINT XFK5_bills
        FOREIGN KEY (current_bill_stage_id) REFERENCES bill_enums (id),
    CONSTRAINT XFK6_bills
        FOREIGN KEY (current_bill_status_id) REFERENCES bill_enums (id),
    CONSTRAINT XFK7_bills
        FOREIGN KEY (current_bill_cta_id) REFERENCES bill_enums (id),
    CONSTRAINT XFK8_bills
        FOREIGN KEY (current_bill_color_id) REFERENCES bill_enums (id),
    CONSTRAINT XFK9_bills
        FOREIGN KEY (current_bill_icon_id) REFERENCES bill_enums (id),
    CONSTRAINT XFK10_bills
        FOREIGN KEY (house_rating_id) REFERENCES ratings (id),
    CONSTRAINT XFK11_bills
        FOREIGN KEY (senate_rating_id) REFERENCES ratings (id)
)
    ENGINE = InnoDB
    CHARSET = utf8mb3;

alter table tenants
    add column has_tracking tinyint(1) default 0 not null after `layout`;

-- Honor Wyoming
update tenants
set has_tracking = 1
where id = '15629c88-a9cc-44a3-9cd7-69701d845f6b';

-- add more fields to bills
alter table bills
    add `bill_number`          varchar(45)  not null after `filed_chamber_id`,
    add `current_bill_cta_url` varchar(255) null after `current_bill_cta_id`;

-- rename bills.name to bills.title
alter table bills
    change `name` `title` varchar(255) not null;

ALTER TABLE bills
    DROP CONSTRAINT XUN1_bills;

ALTER TABLE bills
    ADD CONSTRAINT XUN1_bills
        UNIQUE (tenant_id, legislative_session_id, filed_chamber_id, bill_number);

-- switch current_bill_color_id to current_bill_progress_id
ALTER TABLE bill_enums
    MODIFY COLUMN `type` ENUM ('stage', 'status', 'cta', 'color', 'progress', 'icon') NOT NULL;

UPDATE bill_enums
SET `type` = 'progress'
WHERE `type` = 'color';

ALTER TABLE bill_enums
    MODIFY COLUMN `type` ENUM ('stage', 'status', 'cta', 'progress', 'icon') NOT NULL;

ALTER TABLE bills
    DROP FOREIGN KEY XFK8_bills;

ALTER TABLE bills
    CHANGE `current_bill_color_id` `current_bill_progress_id` CHAR(36) NULL;

ALTER TABLE bills
    ADD CONSTRAINT XFK8_bills
        FOREIGN KEY (current_bill_progress_id) REFERENCES bill_enums (id);

-- add bill tracking vote textarea
alter table bills
    add `house_rating_votes`  text null after `house_rating_id`,
    add `senate_rating_votes` text null after `senate_rating_id`;