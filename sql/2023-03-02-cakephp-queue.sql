create table queue_phinxlog
(
    version        bigint               not null
        primary key,
    migration_name varchar(100)         null,
    start_time     timestamp            null,
    end_time       timestamp            null,
    breakpoint     tinyint(1) default 0 not null
)
    collate = utf8mb4_unicode_ci;

create table queue_processes
(
    id        int auto_increment
        primary key,
    pid       varchar(40)          not null,
    created   datetime             not null,
    modified  datetime             not null,
    terminate tinyint(1) default 0 not null,
    server    varchar(90)          null,
    workerkey varchar(45)          not null,
    constraint pid
        unique (pid, server),
    constraint workerkey
        unique (workerkey)
)
    collate = utf8mb4_unicode_ci;

create table queued_jobs
(
    id              int auto_increment
        primary key,
    job_task        varchar(90)   not null,
    data            text          null,
    job_group       varchar(255)  null,
    reference       varchar(255)  null,
    created         datetime      not null,
    notbefore       datetime      null,
    fetched         datetime      null,
    completed       datetime      null,
    progress        float         null,
    failed          int default 0 not null,
    failure_message text          null,
    workerkey       varchar(45)   null,
    status          varchar(255)  null,
    priority        int default 5 not null
)
    collate = utf8mb4_unicode_ci;