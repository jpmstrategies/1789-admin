#!/bin/bash

export SCRIPTS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Generate a random GUID for the run ID
RUN_ID=$(uuidgen)

# Check if HEALTHCHECKS_PING_KEY is defined
if [ -z "$HEALTHCHECKS_PING_KEY" ]; then
  echo "HEALTHCHECKS_PING_KEY is not defined. Skipping health check pings."
  SEND_PINGS=false
else
  SEND_PINGS=true
  # Healthchecks.io endpoints
  START_ENDPOINT="https://hc-ping.com/$HEALTHCHECKS_PING_KEY/legiscan-bulk/start?rid=$RUN_ID"
  SUCCESS_ENDPOINT="https://hc-ping.com/$HEALTHCHECKS_PING_KEY/legiscan-bulk?rid=$RUN_ID"
  FAILURE_ENDPOINT="https://hc-ping.com/$HEALTHCHECKS_PING_KEY/legiscan-bulk/fail?rid=$RUN_ID"
fi

# Notify Healthchecks.io that the script is starting
if $SEND_PINGS; then
  echo "Sending start notification"
  curl -fsS --retry 3 "$START_ENDPOINT" > /dev/null
fi

# Execute the command
php "$SCRIPTS/../plugins/legiscan/legiscan-bulk.php" --bulk --import --verbose --yes
EXIT_STATUS=$?

# Check the exit status of the command
if [ $EXIT_STATUS -eq 0 ]; then
  if $SEND_PINGS; then
    echo "Sending success notification"
    # Notify Healthchecks.io that the script succeeded
    curl -fsS --retry 3 "$SUCCESS_ENDPOINT" > /dev/null
  fi
else
  if $SEND_PINGS; then
    echo "Sending failure notification"
    # Notify Healthchecks.io that the script failed
    curl -fsS --retry 3 "$FAILURE_ENDPOINT" > /dev/null
  fi
fi

# Exit with the same status as the command
exit $EXIT_STATUS
