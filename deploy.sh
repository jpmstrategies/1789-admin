#!/bin/bash
bin/cake Setup.MaintenanceMode activate

echo "### CODE ###";
git pull

php composer.phar install --prefer-dist --no-dev --optimize-autoloader --no-interaction

mkdir -p ./tmp
mkdir -p ./logs
mkdir -p ./webroot/js/cjs/
mkdir -p ./webroot/css/ccss/

echo "### DB MIGRATION ###";
bin/cake Migrations migrate -p Geo // and all other plugin migrations
bin/cake Migrations migrate

echo "### ASSETS ###";
bower install // or npm -i etc

mkdir -p ./webroot/css/fonts
cp -R ./webroot/assets/bootstrap/dist/fonts/* ./webroot/css/fonts/

bin/cake AssetCompress.AssetCompress build

echo "### CLEANUP ###";
bin/cake clear cache

echo "### CACHE WARMING ###;
bin/cake orm_cache build

echo "### DONE ###";
bin/cake Setup.MaintenanceMode deactivate