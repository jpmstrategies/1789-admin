# CakePHP Application Skeleton

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/0d12a5ddf729416abc8f7714bdc757e7)](https://www.codacy.com/app/JPM/1789alliancescorecard?utm_source=jpmconsulting@bitbucket.org&amp;utm_medium=referral&amp;utm_content=jpmconsulting/1789alliancescorecard&amp;utm_campaign=Badge_Grade)

## Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `php composer.phar create-project --prefer-dist cakephp/app [app_name]`.

If Composer is installed globally, run

```bash
composer create-project --prefer-dist cakephp/app
```

In case you want to use a custom app dir name (e.g. `/myapp/`):

```bash
composer create-project --prefer-dist cakephp/app myapp
```

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Configuration

Read and edit `config/app.php` and setup the `'Datasources'` and any other
configuration relevant for your application.
