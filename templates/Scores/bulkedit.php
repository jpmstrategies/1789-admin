<?php

use Cake\Routing\Router;

$this->Html->script('View/bulkedit.js?v=20220816', ['block' => true]);
$this->Html->css('bulkedit.css', ['block' => true]);

?>
<div class="ratings view">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-header">
                <h1><?php echo __('Organization Scores Bulk Edit'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <?php echo $this->Form->create(null, ['class' => 'form-horizontal', 'id' => 'FilterAdminBulkeditForm']); ?>
            <input type="hidden" name="_csrfToken" value="<?php echo $_COOKIE['csrfToken']; ?>"/>
            <input type="hidden" name="url"
                   value="<?php echo Router::url(['controller' => 'Scores', 'action' => 'bulkedit'], true); ?>">
            <input type="hidden" name="saveUrl"
                   value="<?php echo Router::url(['controller' => 'Scores', 'action' => 'bulk-update-row'], true); ?>">
            <div class="row">
                <div class="form-group form-group-sm col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('organization', [
                                'label' => false,
                                'name' => 'organization',
                                'class' => 'custom-select',
                                'default' => $selectedFilters['orgId']]); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('session',
                                [
                                    'label' => false,
                                    'class' => 'custom-select',
                                    'placeholder' => 'Legislative Session Id',
                                    'default' => $selectedFilters['session']]); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('chamber', [
                                'label' => false,
                                'name' => 'chamber',
                                'class' => 'custom-select',
                                'default' => $selectedFilters['chamber']]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<div class="related row">
    <div class="col-xl-12">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="scores-tab" data-toggle="tab" href="#scores" role="tab"
                   aria-controls="scores" aria-selected="true">Scores</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="scores" role="tabpanel" aria-labelledby="scores-tab">
                <?php echo $this->element('Scores/bulkedit_scores'); ?>
            </div>
        </div>
    </div>
    <!-- end col md 12 -->
</div>
<script type="application/javascript">
    $('table#ScoresTable').fixedHeader();
    // hidden until we add more tags in the future
    // $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    //     let targetTableId = (e.target.id === 'scores-tab' ? 'ScoresTable' : 'DetailsTable');
    //     $('table#' + targetTableId).fixedHeader();
    // })
</script>
