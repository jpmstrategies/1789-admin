<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanBillVote $legiscanBillVote
 */
?>
<div class="legiscanBillVotes view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($legiscanBillVote->roll_call_desc) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>
                                Edit Legiscan Bill Vote')
                                    , ['action' => 'edit', $legiscanBillVote->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>
                                List Legiscan Bill Votes')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>
                                New Legiscan Bill Vote')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('State') ?></th>
                    <td><?= h($legiscanBillVote->state) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Bill') ?></th>
                    <td><?= $legiscanBillVote->has('legiscan_bill') ?
                            $this->Html->link($legiscanBillVote->legiscan_bill
                                ->title, [
                                'controller' => 'LegiscanBills',
                                'action' => 'view',
                                $legiscanBillVote->legiscan_bill
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Bill Number') ?></th>
                    <td><?= h($legiscanBillVote->bill_number) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Roll Call Desc') ?></th>
                    <td><?= h($legiscanBillVote->roll_call_desc) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Roll Call Body Abbr') ?></th>
                    <td><?= h($legiscanBillVote->roll_call_body_abbr) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Roll Call Body Short') ?></th>
                    <td><?= h($legiscanBillVote->roll_call_body_short) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Roll Call Body Name') ?></th>
                    <td><?= h($legiscanBillVote->roll_call_body_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Url') ?></th>
                    <td><?= h($legiscanBillVote->legiscan_url) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('State Url') ?></th>
                    <td><?= h($legiscanBillVote->state_url) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('State Name') ?></th>
                    <td><?= h($legiscanBillVote->state_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Session') ?></th>
                    <td><?= $legiscanBillVote->has('legiscan_session') ?
                            $this->Html->link($legiscanBillVote->legiscan_session
                                ->session_name, [
                                'controller' => 'LegiscanSessions',
                                'action' => 'view',
                                $legiscanBillVote->legiscan_session
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Origination Chamber') ?></th>
                    <td><?= $legiscanBillVote->has('origination_chamber') ?
                            $this->Html->link($legiscanBillVote->origination_chamber
                                ->body_name, [
                                'controller' => 'LegiscanChambers',
                                'action' => 'view',
                                $legiscanBillVote->origination_chamber
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Current Chamber') ?></th>
                    <td><?= $legiscanBillVote->has('current_chamber') ?
                            $this->Html->link($legiscanBillVote->current_chamber
                                ->body_name, [
                                'controller' => 'LegiscanChambers',
                                'action' => 'view',
                                $legiscanBillVote->current_chamber
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Bill Vote Chamber Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVote->legiscan_bill_vote_chamber_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Yea') ?></th>
                    <td><?= $this->Number->format($legiscanBillVote->yea) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Nay') ?></th>
                    <td><?= $this->Number->format($legiscanBillVote->nay) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Nv') ?></th>
                    <td><?= $this->Number->format($legiscanBillVote->nv) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Absent') ?></th>
                    <td><?= $this->Number->format($legiscanBillVote->absent) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Total') ?></th>
                    <td><?= $this->Number->format($legiscanBillVote->total) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Passed') ?></th>
                    <td><?= $this->Number->format($legiscanBillVote->passed) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan State Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVote->legiscan_state_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Bill Type Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVote->legiscan_bill_type_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Status Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVote->legiscan_status_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Pending Committee Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVote->legiscan_pending_committee_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Has Bill Vote Details') ?></th>
                    <td><?= $this->Number->format($legiscanBillVote->has_bill_vote_details) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Roll Call Date') ?></th>
                    <td><?= h($legiscanBillVote->roll_call_date) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($legiscanBillVote->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Updated') ?></th>
                    <td><?= h($legiscanBillVote->updated) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>

<!-- start related Legiscan Bill Vote Details -->
<div class="Legiscan Bill Vote Details related row">
    <div class="col-lg-12">
        <h3><?= __('Related Legiscan Bill Vote Details') ?></h3>
        <?php if (!empty($legiscanBillVote->legiscan_bill_vote_details)): ?>
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="col"><?= __('Bill Number') ?></th>
                    <th scope="col"><?= __('Vote Desc') ?></th>
                    <th scope="col"><?= __('Party Abbr') ?></th>
                    <th scope="col"><?= __('Role Abbr') ?></th>
                    <th scope="col"><?= __('Role Name') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('First Name') ?></th>
                    <th scope="col"><?= __('Last Name') ?></th>
                    <th scope="col"><?= __('Suffix') ?></th>
                    <th scope="col"><?= __('Nickname') ?></th>
                    <th scope="col"><?= __('Ballotpedia') ?></th>
                    <th scope="col"><?= __('Person Hash') ?></th>
                    <th scope="col"><?= __('Person District') ?></th>
                    <th scope="col"><?= __('State Name') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($legiscanBillVote->legiscan_bill_vote_details as $legiscanBillVoteDetails): ?>
                    <tr>
                        <td><?= h($legiscanBillVoteDetails->bill_number) ?></td>
                        <td><?= h($legiscanBillVoteDetails->vote_desc) ?></td>
                        <td><?= h($legiscanBillVoteDetails->party_abbr) ?></td>
                        <td><?= h($legiscanBillVoteDetails->role_abbr) ?></td>
                        <td><?= h($legiscanBillVoteDetails->role_name) ?></td>
                        <td><?= h($legiscanBillVoteDetails->name) ?></td>
                        <td><?= h($legiscanBillVoteDetails->first_name) ?></td>
                        <td><?= h($legiscanBillVoteDetails->last_name) ?></td>
                        <td><?= h($legiscanBillVoteDetails->suffix) ?></td>
                        <td><?= h($legiscanBillVoteDetails->nickname) ?></td>
                        <td><?= h($legiscanBillVoteDetails->ballotpedia) ?></td>
                        <td><?= h($legiscanBillVoteDetails->person_hash) ?></td>
                        <td><?= h($legiscanBillVoteDetails->person_district) ?></td>
                        <td><?= h($legiscanBillVoteDetails->state_name) ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , [
                                    'controller' => 'LegiscanBillVoteDetails',
                                    'action' => 'view',
                                    $legiscanBillVoteDetails->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , [
                                    'controller' => 'LegiscanBillVoteDetails',
                                    'action' => 'edit',
                                    $legiscanBillVoteDetails->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?php // External ID exists and when populated, hide delete icon ?>
                            <?php if (!$legiscanBillVoteDetails->followthemoney_eid) : ?>
                                <?=
                                $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                    , [
                                        'controller' => 'LegiscanBillVoteDetails',
                                        'action' => 'delete',
                                        $legiscanBillVoteDetails->id]
                                    , [
                                        'escape' => false,
                                        'confirm' => __('Are you sure you want to delete #
                            {0}?', $legiscanBillVoteDetails->id)]
                                )
                                ?>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else : ?>
            <div class="alert alert-primary" role="alert">
                No Legiscan Bill Vote Details to display!
            </div>
        <?php endif; ?>
    </div>
    <!-- end col lg 12 -->
</div>
<!-- end related Legiscan Bill Vote Details -->
<!-- start related Ratings -->
<div class="Ratings related row">
    <div class="col-lg-12">
        <h3><?= __('Related Ratings') ?></h3>
        <?php if (!empty($legiscanBillVote->ratings)): ?>
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Record Vote') ?></th>
                    <th scope="col"><?= __('Rating Weight') ?></th>
                    <th scope="col"><?= __('Sort Order') ?></th>
                    <th scope="col"><?= __('Slug') ?></th>
                    <th scope="col"><?= __('Json') ?></th>
                    <th scope="col"><?= __('Is Enabled') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($legiscanBillVote->ratings as $ratings): ?>
                    <tr>
                        <td><?= h($ratings->name) ?></td>
                        <td><?= h($ratings->record_vote) ?></td>
                        <td><?= h($ratings->rating_weight) ?></td>
                        <td><?= h($ratings->sort_order) ?></td>
                        <td><?= h($ratings->slug) ?></td>
                        <td><?= h($ratings->json) ?></td>
                        <td><?= h($ratings->is_enabled) ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['controller' => 'Ratings', 'action' => 'view', $ratings->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['controller' => 'Ratings', 'action' => 'edit', $ratings->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['controller' => 'Ratings', 'action' => 'delete', $ratings->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete #
                            {0}?', $ratings->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else : ?>
            <div class="alert alert-primary" role="alert">
                No Ratings to display!
            </div>
        <?php endif; ?>
    </div>
    <!-- end col lg 12 -->
</div>
<!-- end related Ratings -->
