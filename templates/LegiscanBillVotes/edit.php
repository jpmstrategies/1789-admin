<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanBillVote $legiscanBillVote
 */
?>
<div class="legiscanBillVotes form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= __('Edit Legiscan Bill Vote') ?></h1>
            </div>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill
                                                ')
                                    , ['controller' => 'LegiscanBills', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Session
                                                ')
                                    , ['controller' => 'LegiscanSessions', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Chamber
                                                ')
                                    , ['controller' => 'LegiscanChambers', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Vote Detail
                                                ')
                                    , ['controller' => 'LegiscanBillVoteDetails', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating
                                                ')
                                    , ['controller' => 'Ratings', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Legiscan Bill Votes
                                ')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link'])
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?= $this->Form->create($legiscanBillVote) ?>
            <div class="form-group">
                <?php
                echo $this->Form->control('state', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_bill_id', [
                    'options' => $legiscanBills,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('bill_number', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('roll_call_date', [
                    'empty' => true,
                    'class' => 'form-control',
                    'placeholder' =>
                        '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('roll_call_desc', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_bill_vote_chamber_id', [
                    'class' => 'form-control',
                    'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('roll_call_body_abbr', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('roll_call_body_short', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('roll_call_body_name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('yea', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('nay', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('nv', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('absent', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('total', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('passed', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_url', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('state_url', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_state_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('state_name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_session_id', [
                    'options' => $legiscanSessions,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_chamber_id', [
                    'options' => $legiscanChambers,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_current_chamber_id', [
                    'options' => $legiscanCurrentChambers,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_bill_type_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_status_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_pending_committee_id', [
                    'class' => 'form-control',
                    'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('has_bill_vote_details', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            <?php echo $this->Html->link(
                'Cancel',
                ['controller' => 'legiscanBillVotes', 'action' => 'index', '_full' => true],
                ['class' => 'btn btn-secondary', 'id' => 'cancel']
            ); ?>
            <?= $this->Form->end() ?>
        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->