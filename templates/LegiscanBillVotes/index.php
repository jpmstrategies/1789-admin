<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanBillVote[]|\Cake\Collection\CollectionInterface $legiscanBillVotes
 */
?>
<div class="legiscanBillVotes index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Legiscan Bill Votes') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Vote')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill
                                ')
                    , ['controller' => 'LegiscanBills', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Session
                                ')
                    , ['controller' => 'LegiscanSessions', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Chamber
                                ')
                    , ['controller' => 'LegiscanChambers', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Vote Detail
                                ')
                    , ['controller' => 'LegiscanBillVoteDetails', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating
                                ')
                    , ['controller' => 'Ratings', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_bill_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('bill_number') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('roll_call_date') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('roll_call_desc') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_bill_vote_chamber_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('roll_call_body_abbr') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('roll_call_body_short') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('roll_call_body_name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('yea') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('nay') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('nv') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('absent') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('total') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('passed') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_url') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('state_url') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_state_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('state_name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_session_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_chamber_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_current_chamber_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_bill_type_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_status_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_pending_committee_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('has_bill_vote_details') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('updated') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($legiscanBillVotes as $legiscanBillVote): ?>
                    <tr>
                        <td>
                            <?= $legiscanBillVote->has('legiscan_bill') ?
                                $this->Html->link($legiscanBillVote->legiscan_bill
                                    ->title, [
                                    'controller' => 'LegiscanBills
                                            ',
                                    'action' => 'view',
                                    $legiscanBillVote->legiscan_bill
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVote->bill_number) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVote->roll_call_date) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVote->roll_call_desc) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVote->legiscan_bill_vote_chamber_id) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVote->roll_call_body_abbr) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVote->roll_call_body_short) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVote->roll_call_body_name) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVote->yea) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVote->nay) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVote->nv) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVote->absent) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVote->total) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVote->passed) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVote->legiscan_url) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVote->state_url) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVote->legiscan_state_id) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVote->state_name) ?>
                        </td>
                        <td>
                            <?= $legiscanBillVote->has('legiscan_session') ?
                                $this->Html->link($legiscanBillVote->legiscan_session
                                    ->session_name, [
                                    'controller' => 'LegiscanSessions
                                            ',
                                    'action' => 'view',
                                    $legiscanBillVote->legiscan_session
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $legiscanBillVote->has('origination_chamber') ?
                                $this->Html->link($legiscanBillVote->origination_chamber
                                    ->body_name, [
                                    'controller' => 'LegiscanChambers
                                            ',
                                    'action' => 'view',
                                    $legiscanBillVote->origination_chamber
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $legiscanBillVote->has('current_chamber') ?
                                $this->Html->link($legiscanBillVote->current_chamber
                                    ->body_name, [
                                    'controller' => 'LegiscanChambers
                                            ',
                                    'action' => 'view',
                                    $legiscanBillVote->current_chamber
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVote->legiscan_bill_type_id) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVote->legiscan_status_id) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVote->legiscan_pending_committee_id) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVote->has_bill_vote_details) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVote->updated) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $legiscanBillVote->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $legiscanBillVote->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $legiscanBillVote->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete # {0}?', $legiscanBillVote->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
