<?php
$this->assign('title', 'Admin Dashboard');
?>
<h1>Admin Dashboard</h1>
<div class="organizations index">
    <div class="row">

        <div class="col-lg-4">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Ratings</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <?php
                                if ($currentTenantDataSource === 'legiscan') {
                                    echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating'),
                                        ['controller' => 'LegiscanBills', 'action' => 'index'], ['escape' => false, 'class' => 'nav-link']);
                                } else {
                                    echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating'),
                                        ['controller' => 'Ratings', 'action' => 'add'], ['escape' => false, 'class' => 'nav-link']);
                                }
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>

        <div class="col-lg-4">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Bulk Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp;&nbsp;Edit Legislators'),
                                    ['controller' => 'Legislators', 'action' => 'bulkedit'],
                                    ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp;&nbsp;Edit People'),
                                    ['controller' => 'People', 'action' => 'bulkedit'],
                                    ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp;&nbsp;Edit Ratings'),
                                    ['controller' => 'Ratings', 'action' => 'bulkedit'],
                                    ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <?php if ($currentTenantType !== 'D' && $currentTenantType !== 'F') : ?>
                                <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp;&nbsp;Edit Records'),
                                        ['controller' => 'Records', 'action' => 'bulkedit'],
                                        ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>

        <?php //if ($user_role === 'administrator') : ?>
        <div class="col-lg-4">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Taxonomy</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Action Type'),
                                    ['controller' => 'ActionTypes', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Action'),
                                    ['controller' => 'Actions', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Address'),
                                    ['controller' => 'Addresses', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Address Type'),
                                    ['controller' => 'AddressTypes', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <?php if ($currentTenantType !== 'D' && $currentTenantType !== 'F') : ?>
                                <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Grade'),
                                        ['controller' => 'grades', 'action' => 'add'],
                                        ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <?php endif; ?>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legislative Session'),
                                    ['controller' => 'LegislativeSessions', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <?php if ($currentTenantType !== 'D') : ?>
                                <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Position'),
                                        ['controller' => 'positions', 'action' => 'add'],
                                        ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <?php endif; ?>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legislator'),
                                    ['controller' => 'Legislators', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Party'),
                                    ['controller' => 'parties', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Person'),
                                    ['controller' => 'People', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating Type'),
                                    ['controller' => 'RatingTypes', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <?php if ($is_super) : ?>
                                <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New User'),
                                        ['controller' => 'Users', 'action' => 'add'],
                                        ['escape' => false, 'class' => 'nav-link']); ?></li>
                                <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Tenant'),
                                        ['controller' => 'Tenants', 'action' => 'add'],
                                        ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <?php //endif; ?>
    </div>
