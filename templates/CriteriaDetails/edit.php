<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CriteriaDetail $criteriaDetail
 */
?>
<div class="criteriaDetails form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= __('Edit Criteria Detail') ?></h1>
            </div>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Criteria
                                                ')
                                    , ['controller' => 'Criterias', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating Criteria
                                                ')
                                    , ['controller' => 'RatingCriterias', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Criteria Details
                                ')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link'])
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?= $this->Form->create($criteriaDetail) ?>
            <?php
            echo $this->Form->control('tenant_id', ['type' => 'hidden']);
            ?>
            <div class="form-group">
                <?php
                echo $this->Form->control('criteria_id', ['options' => $criterias, 'class' =>
                    'form-control', 'placeholder' => '', 'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('sort_order', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <div class="small">Set to the desired position, and the other grades will be automatically
                    re-numbered when a conflict occurs. Leave blank to add to the end.
                </div>
            </div>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            <?php echo $this->Html->link(
                'Cancel',
                ['controller' => 'criteriaDetails', 'action' => 'index', '_full' => true],
                ['class' => 'btn btn-secondary', 'id' => 'cancel']
            ); ?>
            <?= $this->Form->end() ?>
        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->