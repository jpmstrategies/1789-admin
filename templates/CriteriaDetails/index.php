<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CriteriaDetail[]|\Cake\Collection\CollectionInterface $criteriaDetails
 */
?>
<div class="criteriaDetails index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Criteria Details') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Criteria Detail')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Criteria
                                ')
                    , ['controller' => 'Criterias', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating Criteria
                                ')
                    , ['controller' => 'RatingCriterias', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('Criterias.name', 'Criteria') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('CriteriaDetails.name', 'Detail Name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('CriteriaDetails.sort_order', 'Detail Sort Order') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($criteriaDetails as $criteriaDetail): ?>
                    <tr>
                        <td>
                            <?= $criteriaDetail->has('criteria') ?
                                $this->Html->link($criteriaDetail->criteria
                                    ->name, ['controller' => 'Criterias
                                            ',
                                    'action' => 'view', $criteriaDetail->criteria
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= h($criteriaDetail->name) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($criteriaDetail->sort_order) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $criteriaDetail->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $criteriaDetail->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $criteriaDetail->id]
                                , ['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $criteriaDetail->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
