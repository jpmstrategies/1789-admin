<div class="people form">

    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Import Configurations'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-desktop-download"></span>&nbsp;&nbsp;Export Configurations'),
                                    ['action' => 'export'], [
                                        'escape' => false,
                                        'class' => 'nav-link',
                                        'target' => '_blank']); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create(null, ['type' => 'file']); ?>
            <div class="form-group">
                <?php echo $this->Form->input('file', [
                    'type' => 'file',
                    'class' => 'form-control-file',
                    'label' => '']); ?>
                <small class="form-text text-muted">This will delete all the current configurations and replace with the
                    ones contained in the uploaded file.
                </small>
            </div>

            <div class="form-group">
                <?php echo $this->Form->button(__('Import Configurations'), [
                    'type' => 'submit',
                    'class' => 'btn btn-primary']); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
