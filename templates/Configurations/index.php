<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Configuration[]|\Cake\Collection\CollectionInterface $configurations
 */

use Cake\Routing\Router;

$this->Html->script('View/Configurations/bulkedit.js?v=20221117', ['block' => true]);
?>
<style type="text/css">
    .error-message {
        height: auto !important;
    }

    .alert-success {
        height: auto !important;
    }

    .field {
        padding-top: 20px;
    }

    .default-changed input, .default-changed textarea, .default-changed select {
        background-color: #fff3cd;
    }
</style>
<?php
$tabs = $this->ConfigurationTab->get_options();

// user details are not tenant specific
unset($tabs['user_details']);

if (empty($is_super)) {
    unset($tabs['admin']);
}
if ($currentTenantType === 'D') {
    unset($tabs['home']);
    unset($tabs['vote']);
    unset($tabs['legislator']);
    unset($tabs['display']);
    unset($tabs['grade']);
}
if($currentTenantType === 'F') {
    unset($tabs['home']);
    unset($tabs['vote']);
    unset($tabs['legislator']);
    unset($tabs['display']);
    unset($tabs['grade']);
}
?>
<div class="configurations index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Configurations') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <?php if ($is_super) : ?>
        <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
            <ul class="navbar-nav mr-auto">
                <li>
                    <?= $this->Html->link(__('<span class="octicon octicon-desktop-download"></span>&nbsp;&nbsp;Export Configurations')
                        , ['action' => 'export']
                        , ['escape' => false, 'class' => 'nav-link', 'target' => '_blank'])
                    ?>
                </li>
                <li>
                    <?= $this->Html->link(__('<span class="octicon octicon-cloud-upload"></span>&nbsp;&nbsp;Import Configurations')
                        , ['action' => 'import']
                        , ['escape' => false, 'class' => 'nav-link'])
                    ?>
                </li>
            </ul>
        </nav>
        <br/>
        <!-- end nav -->
    <?php endif; ?>
    <?php echo $this->Form->create($configurations, ['id' => 'ConfigurationsBulkEdit']); ?>
    <input type="hidden" name="_csrfToken" value="<?php echo $this->request->getAttribute('csrfToken') ?? ''; ?>"/>
    <input type="hidden" name="saveUrl"
           value="<?php echo Router::url(['controller' => 'Configurations', 'action' => 'bulk-update-row'], true); ?>">
    <div class="row">
        <div class="col-2">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <?php
                $i = 0;
                foreach ($tabs as $tabId => $tabName) :
                    if (empty($currentTab)) {
                        $navClass = ($i === 0 ? ' active' : '');
                    } else {
                        $navClass = ($currentTab === $tabId ? ' active' : '');
                    }
                    ?>
                    <a class="nav-link<?php echo $navClass; ?>" id="v-pills-global-tab" data-toggle="pill"
                       href="#v-pills-<?php echo $tabId; ?>" role="tab" onclick="updateTab('<?php echo $tabId; ?>')"
                       aria-controls="v-pills-global" aria-selected="false"><?php echo $tabName; ?></a>
                    <?php
                    $i++;
                endforeach;
                ?>
            </div>
        </div>
        <div class="col-10">
            <div class="tab-content" id="v-pills-tabContent">
                <?php
                $i = 0;
                foreach ($tabs as $tabId => $tabName) :
                    if (empty($currentTab)) {
                        $navClass = ($i === 0 ? ' show active' : ' fade');
                    } else {
                        $navClass = ($currentTab === $tabId ? ' show active' : ' fade');
                    }
                    ?>
                    <div class="tab-pane fade<?php echo $navClass; ?>" id="v-pills-<?php echo $tabId; ?>"
                         role="tabpanel"
                         aria-labelledby="v-pills-<?php echo $tabId; ?>-tab">
                        <?php
                        foreach ($configurations->toArray() as $configuration) {
                            echo $this->element('Configurations/tab', [
                                "configuration_type" => $configuration->configuration_type,
                                "data_id" => 'ct-' . $configuration->configuration_type->id,
                                "field_name" => 'value',
                                "label_value" => false,
                                "input_value" => $configuration->value,
                                "configuration_options" => $configurationOptions[$configuration->configuration_type->id] ?? [],
                                "target_tab" => $tabId,
                                "description" => $configuration->configuration_type->description,
                                "ajax_save" => true,

                                "configuration_id" => $configuration->id,
                            ]);
                        }
                        ?>
                    </div>
                    <?php
                    $i++;
                endforeach;
                ?>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<!-- end containing of content -->

<script type="application/javascript">
    function updateTab(tabName) {
        const params = new URLSearchParams(location.search);
        params.set('tab', tabName);
        window.history.replaceState({}, '', `${location.pathname}?${params}`);
    }

    jQuery(document).ready(function () {
        <?php
        foreach ($configurations->toArray() as $userField) {
            if (isset($userField->configuration_type->field_mask)) {
                echo 'jQuery("[data-id=ct-' . $userField->id . ']").mask("' . $userField->configuration_type->field_mask . '");';
            }
        }
        ?>
    });
</script>
