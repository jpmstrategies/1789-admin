<div class="ratingTypes view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Rating Type'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp&nbsp;Edit Rating Type'),
                                    ['action' => 'edit', $ratingType['id']],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete Rating Type'),
                                    ['action' => 'delete', $ratingType['id']],
                                    [
                                        'escape' => false,
                                        'class' => 'nav-link'], __('Are you sure you want to delete # {0}?',
                                        $ratingType['id'])); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp&nbsp;List Rating Types'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Rating Type'),
                                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Action'),
                                    ['controller' => 'Actions', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>

                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">
                <tbody>
                <tr>
                    <th><?php echo __('Id'); ?></th>
                    <td>
                        <?php echo h($ratingType['id']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Name'); ?></th>
                    <td>
                        <?php echo h($ratingType['name']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Table Detail Column'); ?></th>
                    <td>
                        <?php echo h($ratingType['th_detail_name']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Table Vote Column'); ?></th>
                    <td>
                        <?php echo h($ratingType['th_vote_name']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Method'); ?></th>
                    <td>
                        <?php echo $this->Method->help_text($ratingType['method']); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Sort Order'); ?></th>
                    <td>
                        <?php echo h($ratingType['sort_order']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td>
                        <?php echo h($ratingType['created']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td>
                        <?php echo h($ratingType['modified']); ?>
                        &nbsp;
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
        <!-- end col md 9 -->

    </div>
</div>
