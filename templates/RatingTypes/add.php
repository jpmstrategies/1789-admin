<div class="ratingTypes form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Add Rating Type'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">

                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Rating Types'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($ratingType); ?>

            <div class="form-group">
                <?php echo $this->Form->control('name', ['class' => 'form-control', 'placeholder' => 'Name']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('th_detail_name', [
                    'class' => 'form-control',
                    'placeholder' => 'Left Column Header',
                    'label' => 'Left Column Header']); ?>
                <small class="form-text text-muted">This controls the left column header for the vote display on the
                    Legislator Page.
                </small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('th_vote_name', [
                    'class' => 'form-control',
                    'placeholder' => 'Right Column Headern',
                    'label' => 'Right Column Header']); ?>
                <small class="form-text text-muted">This controls the right column header for the vote display on the
                    Legislator Page.
                </small>
            </div>
            <div class="form-group">
                <?php
                echo $this->Method->control('method', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <div class="small">
                    <?php
                    foreach ($this->Method->options as $key => $description) {
                        echo $this->Method->help_text($key);
                        echo "<br />";
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $this->Form->submit(__('Submit'), ['class' => 'btn btn-primary']); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>

<script>
    $(document).ready(function () {
        // remove 'P' from the method field
        $('#method').find('option[value="P"]').remove();
    });
</script>