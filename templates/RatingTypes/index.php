<div class="ratingTypes index">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Rating Types'); ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating Type'),
                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?></li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('RatingTypes.name', 'Name'); ?></th>
                    <th><?php echo $this->Paginator->sort('RatingTypes.sort_order', 'Sort Order'); ?></th>
                    <th><?php echo $this->Paginator->sort('RatingTypes.method', 'Method'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($ratingTypes as $ratingType): ?>
                    <tr>
                        <td><?php echo h($ratingType['name']); ?>&nbsp;</td>
                        <td><?php echo h($ratingType['sort_order']); ?>&nbsp;</td>
                        <td><?php echo $this->Method->display_text($ratingType['method']); ?>&nbsp;</td>
                        <td class="actions">
                            <?php echo $this->Html->link('<span class="octicon octicon-search"></span>',
                                ['action' => 'view', $ratingType['id']],
                                ['escape' => false]); ?>
                            <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                                ['action' => 'edit', $ratingType['id']],
                                ['escape' => false]); ?>
                            <?php echo $this->Form->postLink('<span class="octicon octicon-trashcan"></span>',
                                ['action' => 'delete', $ratingType['id']], ['escape' => false],
                                __('Are you sure you want to delete # {0}?', $ratingType['id'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <p>
                <small><?php ?></small>
            </p>

            <?php
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div><!-- end containing of content -->
