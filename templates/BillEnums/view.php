<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BillEnum $billEnum
 */
?>
<div class="billEnums view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($billEnum->name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>
                                Edit Bill Enum')
                                    , ['action' => 'edit', $billEnum->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>
                                List Bill Enums')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>
                                New Bill Enum')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('Type') ?></th>
                    <td><?= $this->BillEnumTypeSuperAdmin->display_text($billEnum->type) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Parent Bill Enum') ?></th>
                    <td><?= $billEnum->has('parent_bill_enum') ?
                            $this->Html->link($billEnum->parent_bill_enum
                                ->name, ['controller' => 'BillEnums',
                                'action' => 'view', $billEnum->parent_bill_enum
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($billEnum->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Description') ?></th>
                    <td><?= h($billEnum->description) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('CSS Class') ?></th>
                    <td><?= h($billEnum->css_class) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Branch') ?></th>
                    <td><?= $this->BillEnumBranch->display_text($billEnum->branch) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sort Order') ?></th>
                    <td><?= $this->Number->format($billEnum->sort_order) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>

<!-- start related Bill Enums -->
<?php if (!empty($billEnum->child_bill_enums)): ?>
    <div class="Bill Enums related row">
        <div class="col-lg-12">
            <h3><?= __('Related Bill Enums') ?></h3>
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="col"><?= __('Type') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Sort Order') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($billEnum->child_bill_enums as $childBillEnums): ?>
                    <tr>
                        <td><?= $this->BillEnumType->display_text($childBillEnums->type) ?></td>
                        <td><?= h($childBillEnums->name) ?></td>
                        <td><?= h($childBillEnums->sort_order) ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['controller' => 'BillEnums', 'action' => 'view', $childBillEnums->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['controller' => 'BillEnums', 'action' => 'edit', $childBillEnums->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['controller' => 'BillEnums', 'action' => 'delete', $childBillEnums->id]
                                , ['escape' => false, 'confirm' => __('Are you sure you want to delete #
                            {0}?', $childBillEnums->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end related Bill Enums -->
<?php endif; ?>
    