<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BillEnum $billEnum
 */
?>
<div class="billEnums form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= __('Edit Bill Enum') ?></h1>
            </div>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Bill Enums
                                ')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link'])
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?= $this->Form->create($billEnum) ?>
            <?php
            echo $this->Form->control('tenant_id', ['type' => 'hidden']);
            ?>
            <div class="form-group" id="type-group">
                <?php
                if ($is_super) {
                    echo $this->BillEnumTypeSuperAdmin->control('type', ['class' => 'form-control', 'placeholder' => '']);
                } else {
                    echo $this->BillEnumType->control('type', ['class' => 'form-control', 'placeholder' => '']);
                }
                ?>
            </div>
            <div class="form-group" id="name-group">
                <?php
                echo $this->Form->control('name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group" id="description-group">
                <?php
                echo $this->Form->control('description', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <small class="form-text text-muted">
                    Supports dynamic replacement of the following variables: [chamber]
                </small>
            </div>
            <div class="form-group" id="css-class-group">
                <?php
                echo $this->Form->control('css_class', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group" id="branch-group">
                <?php
                echo $this->BillEnumBranch->control('branch', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group" id="parent-group">
                <?php
                echo $this->Form->control('parent_id', ['options' => $parentBillEnums, 'empty' => true,
                    'class' => 'custom-select', 'label' => 'Stage']);
                ?>
            </div>

            <div class="form-group" id="name-group">
                <?php
                echo $this->Form->control('sort_order', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            <?php echo $this->Html->link(
                'Cancel',
                ['controller' => 'billEnums', 'action' => 'index', '_full' => true],
                ['class' => 'btn btn-secondary', 'id' => 'cancel']
            ); ?>
            <?= $this->Form->end() ?>
        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->

<script>
    $(document).ready(function () {
        // Function to handle type change and update field visibility and requirements
        function updateRequirements() {
            var selectedType = $('#type').val();

            // Hide all fields except type, name, and description by default
            $('.form-group').not('#type-group, #name-group').hide();

            // Show and make branch required if type is 'stage'
            if (selectedType === 'stage') {
                $('#branch').prop('required', true).closest('.form-group').show();
            } else {
                $('#branch').prop('required', false);
            }

            // Show and make parent_id required if type is 'status'
            if (selectedType === 'status') {
                $('#parent-id').prop('required', true).closest('.form-group').show();
                $('#description-group').show();
            } else {
                $('#parent-id').prop('required', false);
                $('#description-group').hide();
            }

            // Show and make css_class required if type is 'icon' or 'color'
            if (selectedType === 'phase-icon' || selectedType === 'rating-icon' || selectedType === 'color') {
                $('#css-class').prop('required', true).closest('.form-group').show();
            } else {
                $('#css-class').prop('required', false);
            }
        }

        // Trigger initial field visibility and validation on page load
        updateRequirements();

        // Update on type change
        $('#type').change(function () {
            updateRequirements();
        });
    });
</script>
