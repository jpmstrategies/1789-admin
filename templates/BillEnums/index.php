<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BillEnum[]|\Cake\Collection\CollectionInterface $billEnums
 */
?>
<div class="billEnums index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Bill Enums') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Bill Enum')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('type') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('parent_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('css_class', 'CSS Class') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('branch') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('sort_order', 'Order') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($billEnums as $billEnum): ?>
                    <tr>
                        <td>
                            <?= $this->BillEnumTypeSuperAdmin->display_text($billEnum->type) ?>
                        </td>
                        <td>
                            <?= $billEnum->has('parent_bill_enum') ?
                                $this->Html->link($billEnum->parent_bill_enum
                                    ->name, ['controller' => 'BillEnums
                                            ',
                                    'action' => 'view', $billEnum->parent_bill_enum
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= h($billEnum->name) ?>
                        </td>
                        <td>
                            <?= h($billEnum->css_class) ?>
                        </td>
                        <td>
                            <?= $this->BillEnumBranch->display_text($billEnum->branch) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($billEnum->sort_order) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $billEnum->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $billEnum->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $billEnum->id]
                                , ['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $billEnum->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
