<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tenant[]|\Cake\Collection\CollectionInterface $tenants
 */
?>
<div class="tenants index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Tenants') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Tenant')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('domain_name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('data_source') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('state') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('is_enabled', 'Enabled') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($tenants as $tenant): ?>
                    <tr>
                        <td>
                            <?= h($tenant->name) ?>
                        </td>
                        <td>
                            <?= h($tenant->domain_name) ?>
                        </td>
                        <td>
                            <?= h($tenant->data_source) ?>
                        </td>
                        <td>
                            <?= h($tenant->state) ?>
                        </td>
                        <td>
                            <?php echo $this->Enable->display_text($tenant->is_enabled) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $tenant->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $tenant->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?php
                            /* disable deletes for now, since this would be quite destructive
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $tenant->id]
                                , ['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $tenant->id)]
                            )
                            */
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->



