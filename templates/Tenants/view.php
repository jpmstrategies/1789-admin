<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tenant $tenant
 */
?>
<div class="tenants view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($tenant->name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>  Edit Tenant')
                                    , ['action' => 'edit', $tenant->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>  List Tenants')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>  New Tenant')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($tenant->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Domain Name') ?></th>
                    <td><?= h($tenant->domain_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Data Source') ?></th>
                    <td><?= h($tenant->data_source) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('State') ?></th>
                    <td><?= h($tenant->state) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($tenant->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($tenant->modified) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Enabled') ?></th>
                    <td><?php echo $this->Enable->display_text($tenant->is_enabled); ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
