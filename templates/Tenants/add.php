<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tenant $tenant
 */
?>
<div class="tenants form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= __('Add Tenant') ?></h1>
            </div>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Tenants')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link'])
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?= $this->Form->create($tenant) ?>
            <div class="form-group">
                <?php
                echo $this->Form->control('name', ['class' => 'form-control', 'placeholder' => 'Tenant Name']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('domain_name', [
                    'class' => 'form-control',
                    'placeholder' => 'White Label Domain Name']);
                ?>
            </div>
            <div class="form-group">
                <?php
                $types = ['S' => 'Scorecard', 'D' => 'Directory', 'C' => 'Citizens Alliance of America', 'F' => 'Floor Report'];
                echo $this->Form->control('type', [
                    'class' => 'form-control',
                    'placeholder' => 'Type',
                    'options' => $types]);
                ?>
            </div>
            <div class="form-group">
                <?php
                $types = ['jpm' => 'JPM Scraper', 'legiscan' => 'LegiScan'];
                echo $this->Form->control('data_source', [
                    'class' => 'form-control',
                    'placeholder' => 'Data Source',
                    'options' => $types]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('layout', ['class' => 'form-control', 'placeholder' => 'Layout']);
                ?>
                <small>"default" or custom value from public project.</small>
            </div>
            <div class="form-group">
                <?php
                echo $this->StateList->control('state', ['class' => 'form-control', 'placeholder' => 'State']);
                ?>
            </div>
            <div class="form-check">
                <?php
                echo $this->Form->control('is_enabled', ['class' => 'form-check-input', 'label' => 'Enabled']);
                ?>
            </div>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>
        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
