<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ConfigurationType[]|\Cake\Collection\CollectionInterface $configurationTypes
 */
?>
<div class="configurationTypes index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Configuration Types') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Configuration Type')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;Insert Configurations')
                    , ['action' => 'insertConfigurations']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('data_type') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('tab') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('is_editable') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('is_enabled') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('is_public') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('is_required') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('sort_order') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($configurationTypes as $configurationType): ?>
                    <tr>
                        <td>
                            <?= h($configurationType->name) ?>
                        </td>
                        <td>
                            <?= h($configurationType->data_type) ?>
                        </td>
                        <td>
                            <?php echo $this->Enable->display_text($configurationType->is_editable) ?>
                        </td>
                        <td>
                            <?= $this->Enable->display_text($configurationType->is_editable) ?>
                        </td>
                        <td>
                            <?= $this->Enable->display_text($configurationType->is_enabled) ?>
                        </td>
                        <td>
                            <?= $this->Enable->display_text($configurationType->is_public) ?>
                        </td>
                        <td>
                            <?= $this->Enable->display_text($configurationType->is_required) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($configurationType->sort_order) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $configurationType->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $configurationType->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $configurationType->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete # {0}?', $configurationType->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
