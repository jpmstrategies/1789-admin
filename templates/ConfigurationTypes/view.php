<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ConfigurationType $configurationType
 */
?>
<div class="configurationTypes view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($configurationType->name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>  Edit Configuration Type')
                                    , ['action' => 'edit', $configurationType->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>  List Configuration Types')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="row"><?= __('Scope') ?></th>
                    <td><?= h($configurationType->scope) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($configurationType->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Description') ?></th>
                    <td><?= h($configurationType->description) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Data Type') ?></th>
                    <td><?= h($configurationType->data_type) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Tab') ?></th>
                    <td><?= $this->ConfigurationTab->display_text($configurationType->tab) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Required Message') ?></th>
                    <td><?= h($configurationType->required_message) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Required Regex') ?></th>
                    <td><?= h($configurationType->required_regex) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sort Order') ?></th>
                    <td><?= $this->Number->format($configurationType->sort_order) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Is Editable') ?></th>
                    <td><?= $this->Enable->display_text($configurationType->is_editable); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Is Enabled') ?></th>
                    <td><?= $this->Enable->display_text($configurationType->is_enabled); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Is Public') ?></th>
                    <td><?= $this->Enable->display_text($configurationType->is_public); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Is Required') ?></th>
                    <td><?= $this->Enable->display_text($configurationType->is_required); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($configurationType->modified) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($configurationType->created) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>

