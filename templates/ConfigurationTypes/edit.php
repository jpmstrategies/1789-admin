<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ConfigurationType $configurationType
 */
?>
<div class="configurationTypes form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= __('Edit Configuration Type') ?></h1>
            </div>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Configuration Types')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link'])
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?= $this->Form->create($configurationType) ?>
            <div class="form-group">
                <?php
                echo $this->Form->control('scope', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('description', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->ConfigurationTab->control('tab', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->DataType->control('data_type', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('default_value', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('required_message', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('required_regex', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('sort_order', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <div class="small">Set to the desired position, and the other items will be automatically
                    re-numbered when a conflict occurs.
                </div>
            </div>
            <div class="form-check">
                <?php
                echo $this->Form->control('is_editable', ['class' => 'form-check-input', 'label' => 'Editable']);
                ?>
            </div>
            <div class="form-check">
                <?php
                echo $this->Form->control('is_enabled', ['class' => 'form-check-input', 'label' => 'Enabled']);
                ?>
            </div>
            <div class="form-check">
                <?php
                echo $this->Form->control('is_required', ['class' => 'form-check-input', 'label' => 'Required']);
                ?>
            </div>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            <?php echo $this->Html->link(
                'Cancel',
                ['controller' => 'configurationTypes', 'action' => 'index', '_full' => true],
                ['class' => 'btn btn-secondary', 'id' => 'cancel']
            ); ?>
            <?= $this->Form->end() ?>
        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->