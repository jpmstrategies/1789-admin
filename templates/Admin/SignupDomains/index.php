<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SignupDomain[]|\Cake\Collection\CollectionInterface $signupDomains
 */
?>
<div class="signupDomains index">
    <div class="alert alert-info">
        If you would like to limit the email domains that are allowed for user sign-ups, you can configure Signup
        Domains. However, if you don't configure any domains, anyone can sign up without needing approval.
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Signup Domains') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Signup Domain')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('regex') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($signupDomains as $signupDomain): ?>
                    <tr>
                        <td>
                            <?= h($signupDomain->name) ?>
                        </td>
                        <td>
                            <?= h($signupDomain->regex) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $signupDomain->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $signupDomain->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $signupDomain->id]
                                , ['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $signupDomain->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
