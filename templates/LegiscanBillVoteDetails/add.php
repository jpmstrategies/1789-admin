<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanBillVoteDetail $legiscanBillVoteDetail
 */
?>
<div class="legiscanBillVoteDetails form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= __('Add Legiscan Bill Vote Detail') ?></h1>
            </div>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill
                                                ')
                                    , ['controller' => 'LegiscanBills', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Vote
                                                ')
                                    , ['controller' => 'LegiscanBillVotes', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Party
                                                ')
                                    , ['controller' => 'LegiscanParties', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Session
                                                ')
                                    , ['controller' => 'LegiscanSessions', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Chamber
                                                ')
                                    , ['controller' => 'LegiscanChambers', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Legiscan Bill Vote Details
                                ')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link'])
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?= $this->Form->create($legiscanBillVoteDetail) ?>
            <div class="form-group">
                <?php
                echo $this->Form->control('state', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_bill_id', [
                    'options' => $legiscanBills,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_bill_vote_id', [
                    'options' => $legiscanBillVotes,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_bill_vote_chamber_id', [
                    'class' => 'form-control',
                    'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_people_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('bill_number', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_vote_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('vote_desc', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_party_id', [
                    'options' => $legiscanParties,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('party_abbr', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_role_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('role_abbr', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('role_name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('first_name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('middle_name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('last_name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('suffix', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('nickname', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('ballotpedia', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('followthemoney_eid', [
                    'class' => 'form-control',
                    'placeholder' => '',
                    'disabled' => true,
                    'label' => 'Followthemoney (External ID)']);
                ?>
                <small class="form-text text-muted">This field is disabled because it is only used
                    for data from external systems.</small>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('votesmart_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('opensecrets_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('knowwho_pid', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('person_hash', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('person_district', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_people_chamber_id', [
                    'class' => 'form-control',
                    'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_state_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('state_name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_session_id', [
                    'options' => $legiscanSessions,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_chamber_id', [
                    'options' => $legiscanChambers,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_current_chamber_id', [
                    'options' => $legiscanCurrentChambers,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_bill_type_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_status_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_pending_committee_id', [
                    'class' => 'form-control',
                    'placeholder' => '']);
                ?>
            </div>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            <?php echo $this->Html->link(
                'Cancel',
                ['controller' => 'legiscanBillVoteDetails', 'action' => 'index', '_full' => true],
                ['class' => 'btn btn-secondary', 'id' => 'cancel']
            ); ?>
            <?= $this->Form->end() ?>
        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->