<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanBillVoteDetail[]|\Cake\Collection\CollectionInterface $legiscanBillVoteDetails
 */
?>
<div class="legiscanBillVoteDetails index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Legiscan Bill Vote Details') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Vote Detail')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill
                                ')
                    , ['controller' => 'LegiscanBills', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Vote
                                ')
                    , ['controller' => 'LegiscanBillVotes', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Party
                                ')
                    , ['controller' => 'LegiscanParties', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Session
                                ')
                    , ['controller' => 'LegiscanSessions', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Chamber
                                ')
                    , ['controller' => 'LegiscanChambers', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_bill_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_bill_vote_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_bill_vote_chamber_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_people_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('bill_number') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_vote_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('vote_desc') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_party_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('party_abbr') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_role_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('role_abbr') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('role_name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('first_name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('middle_name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('last_name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('suffix') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('nickname') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('ballotpedia') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('votesmart_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('opensecrets_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('knowwho_pid') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('person_hash') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('person_district') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_people_chamber_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_state_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('state_name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_session_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_chamber_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_current_chamber_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_bill_type_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_status_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_pending_committee_id') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($legiscanBillVoteDetails as $legiscanBillVoteDetail): ?>
                    <tr>
                        <td>
                            <?= $legiscanBillVoteDetail->has('legiscan_bill') ?
                                $this->Html->link($legiscanBillVoteDetail->legiscan_bill
                                    ->title, [
                                    'controller' => 'LegiscanBills
                                            ',
                                    'action' => 'view',
                                    $legiscanBillVoteDetail->legiscan_bill
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $legiscanBillVoteDetail->has('legiscan_bill_vote') ?
                                $this->Html->link($legiscanBillVoteDetail->legiscan_bill_vote
                                    ->roll_call_desc, [
                                    'controller' => 'LegiscanBillVotes
                                            ',
                                    'action' => 'view',
                                    $legiscanBillVoteDetail->legiscan_bill_vote
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVoteDetail->legiscan_bill_vote_chamber_id) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVoteDetail->legiscan_people_id) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->bill_number) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVoteDetail->legiscan_vote_id) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->vote_desc) ?>
                        </td>
                        <td>
                            <?= $legiscanBillVoteDetail->has('legiscan_party') ?
                                $this->Html->link($legiscanBillVoteDetail->legiscan_party
                                    ->party_name, [
                                    'controller' => 'LegiscanParties
                                            ',
                                    'action' => 'view',
                                    $legiscanBillVoteDetail->legiscan_party
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->party_abbr) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVoteDetail->legiscan_role_id) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->role_abbr) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->role_name) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->name) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->first_name) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->middle_name) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->last_name) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->suffix) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->nickname) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->ballotpedia) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVoteDetail->votesmart_id) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->opensecrets_id) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVoteDetail->knowwho_pid) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->person_hash) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->person_district) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVoteDetail->legiscan_people_chamber_id) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVoteDetail->legiscan_state_id) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillVoteDetail->state_name) ?>
                        </td>
                        <td>
                            <?= $legiscanBillVoteDetail->has('legiscan_session') ?
                                $this->Html->link($legiscanBillVoteDetail->legiscan_session
                                    ->session_name, [
                                    'controller' => 'LegiscanSessions
                                            ',
                                    'action' => 'view',
                                    $legiscanBillVoteDetail->legiscan_session
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $legiscanBillVoteDetail->has('origination_chamber') ?
                                $this->Html->link($legiscanBillVoteDetail->origination_chamber
                                    ->body_name, [
                                    'controller' => 'LegiscanChambers
                                            ',
                                    'action' => 'view',
                                    $legiscanBillVoteDetail->origination_chamber
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $legiscanBillVoteDetail->has('current_chamber') ?
                                $this->Html->link($legiscanBillVoteDetail->current_chamber
                                    ->body_name, [
                                    'controller' => 'LegiscanChambers
                                            ',
                                    'action' => 'view',
                                    $legiscanBillVoteDetail->current_chamber
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVoteDetail->legiscan_bill_type_id) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVoteDetail->legiscan_status_id) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillVoteDetail->legiscan_pending_committee_id) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $legiscanBillVoteDetail->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $legiscanBillVoteDetail->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?php // External ID exists and when populated, hide delete icon ?>
                            <?php if (!$legiscanBillVoteDetail->followthemoney_eid) : ?>
                                <?=
                                $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                    , ['action' => 'delete', $legiscanBillVoteDetail->id]
                                    , [
                                        'escape' => false,
                                        'confirm' => __('Are you sure you want to delete # {0}?', $legiscanBillVoteDetail->id)]
                                )
                                ?>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
