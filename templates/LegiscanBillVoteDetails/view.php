<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanBillVoteDetail $legiscanBillVoteDetail
 */
?>
<div class="legiscanBillVoteDetails view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($legiscanBillVoteDetail->name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>
                                Edit Legiscan Bill Vote Detail')
                                    , ['action' => 'edit', $legiscanBillVoteDetail->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>
                                List Legiscan Bill Vote Details')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>
                                New Legiscan Bill Vote Detail')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('State') ?></th>
                    <td><?= h($legiscanBillVoteDetail->state) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Bill') ?></th>
                    <td><?= $legiscanBillVoteDetail->has('legiscan_bill') ?
                            $this->Html->link($legiscanBillVoteDetail->legiscan_bill
                                ->title, [
                                'controller' => 'LegiscanBills',
                                'action' => 'view',
                                $legiscanBillVoteDetail->legiscan_bill
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Bill Vote') ?></th>
                    <td><?= $legiscanBillVoteDetail->has('legiscan_bill_vote') ?
                            $this->Html->link($legiscanBillVoteDetail->legiscan_bill_vote
                                ->roll_call_desc, [
                                'controller' => 'LegiscanBillVotes',
                                'action' => 'view',
                                $legiscanBillVoteDetail->legiscan_bill_vote
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Bill Number') ?></th>
                    <td><?= h($legiscanBillVoteDetail->bill_number) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Vote Desc') ?></th>
                    <td><?= h($legiscanBillVoteDetail->vote_desc) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Party') ?></th>
                    <td><?= $legiscanBillVoteDetail->has('legiscan_party') ?
                            $this->Html->link($legiscanBillVoteDetail->legiscan_party
                                ->party_name, [
                                'controller' => 'LegiscanParties',
                                'action' => 'view',
                                $legiscanBillVoteDetail->legiscan_party
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Party Abbr') ?></th>
                    <td><?= h($legiscanBillVoteDetail->party_abbr) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Role Abbr') ?></th>
                    <td><?= h($legiscanBillVoteDetail->role_abbr) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Role Name') ?></th>
                    <td><?= h($legiscanBillVoteDetail->role_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($legiscanBillVoteDetail->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('First Name') ?></th>
                    <td><?= h($legiscanBillVoteDetail->first_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Middle Name') ?></th>
                    <td><?= h($legiscanBillVoteDetail->middle_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Last Name') ?></th>
                    <td><?= h($legiscanBillVoteDetail->last_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Suffix') ?></th>
                    <td><?= h($legiscanBillVoteDetail->suffix) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Nickname') ?></th>
                    <td><?= h($legiscanBillVoteDetail->nickname) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Ballotpedia') ?></th>
                    <td><?= h($legiscanBillVoteDetail->ballotpedia) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Opensecrets Id') ?></th>
                    <td><?= h($legiscanBillVoteDetail->opensecrets_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Person Hash') ?></th>
                    <td><?= h($legiscanBillVoteDetail->person_hash) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Person District') ?></th>
                    <td><?= h($legiscanBillVoteDetail->person_district) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('State Name') ?></th>
                    <td><?= h($legiscanBillVoteDetail->state_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Session') ?></th>
                    <td><?= $legiscanBillVoteDetail->has('legiscan_session') ?
                            $this->Html->link($legiscanBillVoteDetail->legiscan_session
                                ->session_name, [
                                'controller' => 'LegiscanSessions',
                                'action' => 'view',
                                $legiscanBillVoteDetail->legiscan_session
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Origination Chamber') ?></th>
                    <td><?= $legiscanBillVoteDetail->has('origination_chamber') ?
                            $this->Html->link($legiscanBillVoteDetail->origination_chamber
                                ->body_name, [
                                'controller' => 'LegiscanChambers',
                                'action' => 'view',
                                $legiscanBillVoteDetail->origination_chamber
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Current Chamber') ?></th>
                    <td><?= $legiscanBillVoteDetail->has('current_chamber') ?
                            $this->Html->link($legiscanBillVoteDetail->current_chamber
                                ->body_name, [
                                'controller' => 'LegiscanChambers',
                                'action' => 'view',
                                $legiscanBillVoteDetail->current_chamber
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Bill Vote Chamber Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVoteDetail->legiscan_bill_vote_chamber_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan People Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVoteDetail->legiscan_people_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Vote Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVoteDetail->legiscan_vote_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Role Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVoteDetail->legiscan_role_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Followthemoney Eid') ?></th>
                    <td><?= $this->Number->format($legiscanBillVoteDetail->followthemoney_eid) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Votesmart Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVoteDetail->votesmart_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Knowwho Pid') ?></th>
                    <td><?= $this->Number->format($legiscanBillVoteDetail->knowwho_pid) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan People Chamber Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVoteDetail->legiscan_people_chamber_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan State Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVoteDetail->legiscan_state_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Bill Type Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVoteDetail->legiscan_bill_type_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Status Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVoteDetail->legiscan_status_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Pending Committee Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillVoteDetail->legiscan_pending_committee_id) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>

