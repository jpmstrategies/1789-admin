<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanSession $legiscanSession
 */
?>
<div class="legiscanSessions view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($legiscanSession->session_name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>
                                Edit Legiscan Session')
                                    , ['action' => 'edit', $legiscanSession->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>
                                List Legiscan Sessions')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>
                                New Legiscan Session')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('State') ?></th>
                    <td><?= h($legiscanSession->state) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Session Name') ?></th>
                    <td><?= h($legiscanSession->session_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Session Title') ?></th>
                    <td><?= h($legiscanSession->session_title) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Session Tag') ?></th>
                    <td><?= h($legiscanSession->session_tag) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Import Hash') ?></th>
                    <td><?= h($legiscanSession->import_hash) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Year Start') ?></th>
                    <td><?= $this->Number->format($legiscanSession->year_start) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Year End') ?></th>
                    <td><?= $this->Number->format($legiscanSession->year_end) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Prefile') ?></th>
                    <td><?= $this->Number->format($legiscanSession->prefile) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sine Die') ?></th>
                    <td><?= $this->Number->format($legiscanSession->sine_die) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Prior') ?></th>
                    <td><?= $this->Number->format($legiscanSession->prior) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Special') ?></th>
                    <td><?= $this->Number->format($legiscanSession->special) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Import Date') ?></th>
                    <td><?= h($legiscanSession->import_date) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>

<!-- start related Legiscan Bill Subjects -->
<div class="Legiscan Bill Subjects related row">
    <div class="col-lg-12">
        <h3><?= __('Related Legiscan Bill Subjects') ?></h3>
        <?php if (!empty($legiscanSession->legiscan_bill_subjects)): ?>
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="col"><?= __('Bill Number') ?></th>
                    <th scope="col"><?= __('Subject Name') ?></th>
                    <th scope="col"><?= __('State Name') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($legiscanSession->legiscan_bill_subjects as $legiscanBillSubjects): ?>
                    <tr>
                        <td><?= h($legiscanBillSubjects->bill_number) ?></td>
                        <td><?= h($legiscanBillSubjects->subject_name) ?></td>
                        <td><?= h($legiscanBillSubjects->state_name) ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , [
                                    'controller' => 'LegiscanBillSubjects',
                                    'action' => 'view',
                                    $legiscanBillSubjects->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , [
                                    'controller' => 'LegiscanBillSubjects',
                                    'action' => 'edit',
                                    $legiscanBillSubjects->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , [
                                    'controller' => 'LegiscanBillSubjects',
                                    'action' => 'delete',
                                    $legiscanBillSubjects->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete #
                            {0}?', $legiscanBillSubjects->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else : ?>
            <div class="alert alert-primary" role="alert">
                No Legiscan Bill Subjects to display!
            </div>
        <?php endif; ?>
    </div>
    <!-- end col lg 12 -->
</div>
<!-- end related Legiscan Bill Subjects -->
<!-- start related Legiscan Bill Vote Details -->
<div class="Legiscan Bill Vote Details related row">
    <div class="col-lg-12">
        <h3><?= __('Related Legiscan Bill Vote Details') ?></h3>
        <?php if (!empty($legiscanSession->legiscan_bill_vote_details)): ?>
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="col"><?= __('Bill Number') ?></th>
                    <th scope="col"><?= __('Vote Desc') ?></th>
                    <th scope="col"><?= __('Party Abbr') ?></th>
                    <th scope="col"><?= __('Role Abbr') ?></th>
                    <th scope="col"><?= __('Role Name') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('First Name') ?></th>
                    <th scope="col"><?= __('Last Name') ?></th>
                    <th scope="col"><?= __('Suffix') ?></th>
                    <th scope="col"><?= __('Nickname') ?></th>
                    <th scope="col"><?= __('Ballotpedia') ?></th>
                    <th scope="col"><?= __('Person Hash') ?></th>
                    <th scope="col"><?= __('Person District') ?></th>
                    <th scope="col"><?= __('State Name') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($legiscanSession->legiscan_bill_vote_details as $legiscanBillVoteDetails): ?>
                    <tr>
                        <td><?= h($legiscanBillVoteDetails->bill_number) ?></td>
                        <td><?= h($legiscanBillVoteDetails->vote_desc) ?></td>
                        <td><?= h($legiscanBillVoteDetails->party_abbr) ?></td>
                        <td><?= h($legiscanBillVoteDetails->role_abbr) ?></td>
                        <td><?= h($legiscanBillVoteDetails->role_name) ?></td>
                        <td><?= h($legiscanBillVoteDetails->name) ?></td>
                        <td><?= h($legiscanBillVoteDetails->first_name) ?></td>
                        <td><?= h($legiscanBillVoteDetails->last_name) ?></td>
                        <td><?= h($legiscanBillVoteDetails->suffix) ?></td>
                        <td><?= h($legiscanBillVoteDetails->nickname) ?></td>
                        <td><?= h($legiscanBillVoteDetails->ballotpedia) ?></td>
                        <td><?= h($legiscanBillVoteDetails->person_hash) ?></td>
                        <td><?= h($legiscanBillVoteDetails->person_district) ?></td>
                        <td><?= h($legiscanBillVoteDetails->state_name) ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , [
                                    'controller' => 'LegiscanBillVoteDetails',
                                    'action' => 'view',
                                    $legiscanBillVoteDetails->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , [
                                    'controller' => 'LegiscanBillVoteDetails',
                                    'action' => 'edit',
                                    $legiscanBillVoteDetails->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?php // External ID exists and when populated, hide delete icon ?>
                            <?php if (!$legiscanBillVoteDetails->followthemoney_eid) : ?>
                                <?=
                                $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                    , [
                                        'controller' => 'LegiscanBillVoteDetails',
                                        'action' => 'delete',
                                        $legiscanBillVoteDetails->id]
                                    , [
                                        'escape' => false,
                                        'confirm' => __('Are you sure you want to delete #
                            {0}?', $legiscanBillVoteDetails->id)]
                                )
                                ?>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else : ?>
            <div class="alert alert-primary" role="alert">
                No Legiscan Bill Vote Details to display!
            </div>
        <?php endif; ?>
    </div>
    <!-- end col lg 12 -->
</div>
<!-- end related Legiscan Bill Vote Details -->
<!-- start related Legiscan Bill Votes -->
<div class="Legiscan Bill Votes related row">
    <div class="col-lg-12">
        <h3><?= __('Related Legiscan Bill Votes') ?></h3>
        <?php if (!empty($legiscanSession->legiscan_bill_votes)): ?>
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="col"><?= __('Bill Number') ?></th>
                    <th scope="col"><?= __('Roll Call Date') ?></th>
                    <th scope="col"><?= __('Roll Call Desc') ?></th>
                    <th scope="col"><?= __('Roll Call Body Abbr') ?></th>
                    <th scope="col"><?= __('Roll Call Body Short') ?></th>
                    <th scope="col"><?= __('Roll Call Body Name') ?></th>
                    <th scope="col"><?= __('Yea') ?></th>
                    <th scope="col"><?= __('Nay') ?></th>
                    <th scope="col"><?= __('Nv') ?></th>
                    <th scope="col"><?= __('Absent') ?></th>
                    <th scope="col"><?= __('Total') ?></th>
                    <th scope="col"><?= __('Passed') ?></th>
                    <th scope="col"><?= __('Legiscan Url') ?></th>
                    <th scope="col"><?= __('State Url') ?></th>
                    <th scope="col"><?= __('State Name') ?></th>
                    <th scope="col"><?= __('Has Bill Vote Details') ?></th>
                    <th scope="col"><?= __('Updated') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($legiscanSession->legiscan_bill_votes as $legiscanBillVotes): ?>
                    <tr>
                        <td><?= h($legiscanBillVotes->bill_number) ?></td>
                        <td><?= h($legiscanBillVotes->roll_call_date) ?></td>
                        <td><?= h($legiscanBillVotes->roll_call_desc) ?></td>
                        <td><?= h($legiscanBillVotes->roll_call_body_abbr) ?></td>
                        <td><?= h($legiscanBillVotes->roll_call_body_short) ?></td>
                        <td><?= h($legiscanBillVotes->roll_call_body_name) ?></td>
                        <td><?= h($legiscanBillVotes->yea) ?></td>
                        <td><?= h($legiscanBillVotes->nay) ?></td>
                        <td><?= h($legiscanBillVotes->nv) ?></td>
                        <td><?= h($legiscanBillVotes->absent) ?></td>
                        <td><?= h($legiscanBillVotes->total) ?></td>
                        <td><?= h($legiscanBillVotes->passed) ?></td>
                        <td><?= h($legiscanBillVotes->legiscan_url) ?></td>
                        <td><?= h($legiscanBillVotes->state_url) ?></td>
                        <td><?= h($legiscanBillVotes->state_name) ?></td>
                        <td><?= h($legiscanBillVotes->has_bill_vote_details) ?></td>
                        <td><?= h($legiscanBillVotes->updated) ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['controller' => 'LegiscanBillVotes', 'action' => 'view', $legiscanBillVotes->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['controller' => 'LegiscanBillVotes', 'action' => 'edit', $legiscanBillVotes->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['controller' => 'LegiscanBillVotes', 'action' => 'delete', $legiscanBillVotes->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete #
                            {0}?', $legiscanBillVotes->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else : ?>
            <div class="alert alert-primary" role="alert">
                No Legiscan Bill Votes to display!
            </div>
        <?php endif; ?>
    </div>
    <!-- end col lg 12 -->
</div>
<!-- end related Legiscan Bill Votes -->
<!-- start related Legiscan Bills -->
<div class="Legiscan Bills related row">
    <div class="col-lg-12">
        <h3><?= __('Related Legiscan Bills') ?></h3>
        <?php if (!empty($legiscanSession->legiscan_bills)): ?>
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="col"><?= __('Bill Number') ?></th>
                    <th scope="col"><?= __('Status Desc') ?></th>
                    <th scope="col"><?= __('Status Date') ?></th>
                    <th scope="col"><?= __('Title') ?></th>
                    <th scope="col"><?= __('Bill Type Name') ?></th>
                    <th scope="col"><?= __('Bill Type Abbr') ?></th>
                    <th scope="col"><?= __('Body Abbr') ?></th>
                    <th scope="col"><?= __('Body Short') ?></th>
                    <th scope="col"><?= __('Body Name') ?></th>
                    <th scope="col"><?= __('Current Body Abbr') ?></th>
                    <th scope="col"><?= __('Current Body Short') ?></th>
                    <th scope="col"><?= __('Current Body Name') ?></th>
                    <th scope="col"><?= __('Pending Committee Body Abbr') ?></th>
                    <th scope="col"><?= __('Pending Committee Body Short') ?></th>
                    <th scope="col"><?= __('Pending Committee Body Name') ?></th>
                    <th scope="col"><?= __('Pending Committee Name') ?></th>
                    <th scope="col"><?= __('Legiscan Url') ?></th>
                    <th scope="col"><?= __('State Url') ?></th>
                    <th scope="col"><?= __('Change Hash') ?></th>
                    <th scope="col"><?= __('Updated') ?></th>
                    <th scope="col"><?= __('State Name') ?></th>
                    <th scope="col"><?= __('Session Year Start') ?></th>
                    <th scope="col"><?= __('Session Year End') ?></th>
                    <th scope="col"><?= __('Session Prefile') ?></th>
                    <th scope="col"><?= __('Session Sine Die') ?></th>
                    <th scope="col"><?= __('Session Prior') ?></th>
                    <th scope="col"><?= __('Session Special') ?></th>
                    <th scope="col"><?= __('Session Tag') ?></th>
                    <th scope="col"><?= __('Session Title') ?></th>
                    <th scope="col"><?= __('Session Name') ?></th>
                    <th scope="col"><?= __('Has Bill Vote Details') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($legiscanSession->legiscan_bills as $legiscanBills): ?>
                    <tr>
                        <td><?= h($legiscanBills->bill_number) ?></td>
                        <td><?= h($legiscanBills->status_desc) ?></td>
                        <td><?= h($legiscanBills->status_date) ?></td>
                        <td><?= h($legiscanBills->title) ?></td>
                        <td><?= h($legiscanBills->bill_type_name) ?></td>
                        <td><?= h($legiscanBills->bill_type_abbr) ?></td>
                        <td><?= h($legiscanBills->body_abbr) ?></td>
                        <td><?= h($legiscanBills->body_short) ?></td>
                        <td><?= h($legiscanBills->body_name) ?></td>
                        <td><?= h($legiscanBills->current_body_abbr) ?></td>
                        <td><?= h($legiscanBills->current_body_short) ?></td>
                        <td><?= h($legiscanBills->current_body_name) ?></td>
                        <td><?= h($legiscanBills->pending_committee_body_abbr) ?></td>
                        <td><?= h($legiscanBills->pending_committee_body_short) ?></td>
                        <td><?= h($legiscanBills->pending_committee_body_name) ?></td>
                        <td><?= h($legiscanBills->pending_committee_name) ?></td>
                        <td><?= h($legiscanBills->legiscan_url) ?></td>
                        <td><?= h($legiscanBills->state_url) ?></td>
                        <td><?= h($legiscanBills->change_hash) ?></td>
                        <td><?= h($legiscanBills->updated) ?></td>
                        <td><?= h($legiscanBills->state_name) ?></td>
                        <td><?= h($legiscanBills->session_year_start) ?></td>
                        <td><?= h($legiscanBills->session_year_end) ?></td>
                        <td><?= h($legiscanBills->session_prefile) ?></td>
                        <td><?= h($legiscanBills->session_sine_die) ?></td>
                        <td><?= h($legiscanBills->session_prior) ?></td>
                        <td><?= h($legiscanBills->session_special) ?></td>
                        <td><?= h($legiscanBills->session_tag) ?></td>
                        <td><?= h($legiscanBills->session_title) ?></td>
                        <td><?= h($legiscanBills->session_name) ?></td>
                        <td><?= h($legiscanBills->has_bill_vote_details) ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['controller' => 'LegiscanBills', 'action' => 'view', $legiscanBills->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['controller' => 'LegiscanBills', 'action' => 'edit', $legiscanBills->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['controller' => 'LegiscanBills', 'action' => 'delete', $legiscanBills->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete #
                            {0}?', $legiscanBills->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else : ?>
            <div class="alert alert-primary" role="alert">
                No Legiscan Bills to display!
            </div>
        <?php endif; ?>
    </div>
    <!-- end col lg 12 -->
</div>
<!-- end related Legiscan Bills -->
<!-- start related Legislative Sessions -->
<div class="Legislative Sessions related row">
    <div class="col-lg-12">
        <h3><?= __('Related Legislative Sessions') ?></h3>
        <?php if (!empty($legiscanSession->legislative_sessions)): ?>
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Year') ?></th>
                    <th scope="col"><?= __('Legislature Number') ?></th>
                    <th scope="col"><?= __('Session Type') ?></th>
                    <th scope="col"><?= __('Special Session Number') ?></th>
                    <th scope="col"><?= __('Speaker Journal Name') ?></th>
                    <th scope="col"><?= __('Lt Gov Journal Name') ?></th>
                    <th scope="col"><?= __('Slug') ?></th>
                    <th scope="col"><?= __('State Pk') ?></th>
                    <th scope="col"><?= __('Os Pk') ?></th>
                    <th scope="col"><?= __('Is Enabled') ?></th>
                    <th scope="col"><?= __('Show Scores') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($legiscanSession->legislative_sessions as $legislativeSessions): ?>
                    <tr>
                        <td><?= h($legislativeSessions->name) ?></td>
                        <td><?= h($legislativeSessions->year) ?></td>
                        <td><?= h($legislativeSessions->legislature_number) ?></td>
                        <td><?= h($legislativeSessions->session_type) ?></td>
                        <td><?= h($legislativeSessions->special_session_number) ?></td>
                        <td><?= h($legislativeSessions->speaker_journal_name) ?></td>
                        <td><?= h($legislativeSessions->lt_gov_journal_name) ?></td>
                        <td><?= h($legislativeSessions->slug) ?></td>
                        <td><?= h($legislativeSessions->state_pk) ?></td>
                        <td><?= h($legislativeSessions->os_pk) ?></td>
                        <td><?= h($legislativeSessions->is_enabled) ?></td>
                        <td><?= h($legislativeSessions->show_scores) ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['controller' => 'LegislativeSessions', 'action' => 'view', $legislativeSessions->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['controller' => 'LegislativeSessions', 'action' => 'edit', $legislativeSessions->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , [
                                    'controller' => 'LegislativeSessions',
                                    'action' => 'delete',
                                    $legislativeSessions->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete #
                            {0}?', $legislativeSessions->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else : ?>
            <div class="alert alert-primary" role="alert">
                No Legislative Sessions to display!
            </div>
        <?php endif; ?>
    </div>
    <!-- end col lg 12 -->
</div>
<!-- end related Legislative Sessions -->
