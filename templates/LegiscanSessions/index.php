<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanSession[]|\Cake\Collection\CollectionInterface $legiscanSessions
 */
?>
<div class="legiscanSessions index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Legiscan Sessions') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Session')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Subject
                                ')
                    , ['controller' => 'LegiscanBillSubjects', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Vote Detail
                                ')
                    , ['controller' => 'LegiscanBillVoteDetails', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Vote
                                ')
                    , ['controller' => 'LegiscanBillVotes', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill
                                ')
                    , ['controller' => 'LegiscanBills', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legislative Session
                                ')
                    , ['controller' => 'LegislativeSessions', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('year_start') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('year_end') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('prefile') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('sine_die') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('prior') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('special') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('session_name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('session_title') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('session_tag') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('import_date') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('import_hash') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($legiscanSessions as $legiscanSession): ?>
                    <tr>
                        <td>
                            <?= $this->Number->format($legiscanSession->year_start) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanSession->year_end) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanSession->prefile) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanSession->sine_die) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanSession->prior) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanSession->special) ?>
                        </td>
                        <td>
                            <?= h($legiscanSession->session_name) ?>
                        </td>
                        <td>
                            <?= h($legiscanSession->session_title) ?>
                        </td>
                        <td>
                            <?= h($legiscanSession->session_tag) ?>
                        </td>
                        <td>
                            <?= h($legiscanSession->import_date) ?>
                        </td>
                        <td>
                            <?= h($legiscanSession->import_hash) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $legiscanSession->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $legiscanSession->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $legiscanSession->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete # {0}?', $legiscanSession->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
