<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Grade[]|\Cake\Collection\CollectionInterface $grades
 */
?>
<div class="grades index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Grades') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Grade')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legislator')
                    , ['controller' => 'Legislators', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('css_color') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('sort_order') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('lower_range') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('upper_range') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($grades as $grade): ?>
                    <tr>
                        <td>
                            <?= h($grade->name) ?>
                        </td>
                        <td>
                            <div style="width: 25px; float: left; background-color: <?php echo $grade->css_color; ?>">
                                &nbsp;
                            </div>
                            <div style="float: left; padding-left: 5px">
                                <?= h($grade->css_color) ?>
                            </div>
                        </td>
                        <td>
                            <?= $this->Number->format($grade->sort_order) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($grade->lower_range) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($grade->upper_range) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $grade->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $grade->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $grade->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete # {0}?', $grade->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->



