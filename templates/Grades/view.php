<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Grade $grade
 */
?>
<div class="grades view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($grade->name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>  Edit Grade')
                                    , ['action' => 'edit', $grade->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>  List Grades')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>  New Grade')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($grade->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Css Color') ?></th>
                    <td>
                        <div style="width: 25px; float: left; background-color: <?php echo $grade->css_color; ?>">
                            &nbsp;
                        </div>
                        <div style="float: left; padding-left: 5px">
                            <?= h($grade->css_color) ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Lower Range') ?></th>
                    <td><?= $this->Number->format($grade->lower_range) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Upper Range') ?></th>
                    <td><?= $this->Number->format($grade->upper_range) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sort Order') ?></th>
                    <td><?= $this->Number->format($grade->sort_order) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($grade->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($grade->modified) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>