<?php

use Cake\Core\Configure;
use Cake\Routing\Router;

$this->Html->script('View/bulkedit.js?v=20220816', ['block' => true]);
$this->Html->css('bulkedit.css', ['block' => true]);

if (isset($currentTenantConfigurations['tusa_widget'])) {
    $enable_tusa = $currentTenantConfigurations['tusa_widget'];
} else {
    $enable_tusa = 'no';
}

$integrations = ['enable_tusa' => $enable_tusa];
?>
<div class="ratings view">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-header">
                <h1><?php echo __('People Bulk Edit'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <?php echo $this->Form->create(null, ['class' => 'form-horizontal', 'id' => 'FilterAdminBulkeditForm']); ?>
            <input type="hidden" name="_csrfToken" value="<?php echo $_COOKIE['csrfToken']; ?>"/>
            <input type="hidden" class="tenant-state" value="<?php echo $currentTenantState; ?>"/>
            <input type="hidden" class="ttx-lookup-url"
                   value="<?php echo Configure::read('App.tusa_api_url'); ?>/v2/search"/>
            <input type="hidden" name="url"
                   value="<?php echo Router::url(['controller' => 'People', 'action' => 'bulkedit'], true); ?>">
            <input type="hidden" name="saveUrl"
                   value="<?php echo Router::url(['controller' => 'People', 'action' => 'bulk-update-row'], true); ?>">
            <div class="row">
                <div class="form-group form-group-sm col-sm-9">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('session',
                                [
                                    'label' => false,
                                    'class' => 'custom-select',
                                    'placeholder' => 'Legislative Session Id',
                                    'default' => $selectedFilters['session']]); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('chamber', [
                                'label' => false,
                                'name' => 'chamber',
                                'class' => 'custom-select',
                                'default' => $selectedFilters['chamber']]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<div class="related row">
    <div class="col-xl-12">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab"
                   aria-controls="details"
                   aria-selected="false">Details</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                   aria-controls="contact" aria-selected="false">Contact Info</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="social-tab" data-toggle="tab" href="#social" role="tab"
                   aria-controls="social" aria-selected="false">Social Handles</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tags-tab" data-toggle="tab" href="#tags" role="tab"
                   aria-controls="tags" aria-selected="true">Tags</a>
            </li>
            <?php if ($currentTenantType !== 'D' && $currentTenantType !== 'F') : ?>
                <li class="nav-item">
                    <a class="nav-link" id="scores-tab" data-toggle="tab" href="#scores" role="tab"
                       aria-controls="scores" aria-selected="false">Scores</a>
                </li>
                <?php
                if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes') :
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" id="criteria-tab" data-toggle="tab" href="#criteria" role="tab"
                           aria-controls="criteria" aria-selected="false">Criteria Scores</a>
                    </li>
                <?php endif; ?>
            <?php endif ?>
            <?php if ($enable_tusa === 'yes') : ?>
                <li class="nav-item">
                    <a class="nav-link" id="integrations-tab" data-toggle="tab" href="#integrations" role="tab"
                       aria-controls="integrations" aria-selected="false">Integrations</a>
                </li>
            <?php endif; ?>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                <?php echo $this->element('People/bulkedit_details'); ?>
            </div>
            <div class="tab-pane fade" id="scores" role="tabpanel" aria-labelledby="scores-tab">
                <?php
                if (empty($calculatedScores)) {
                    echo "<div class='alert alert-warning'>Please check back later.</div>";
                } else {
                    $exportURL = Router::url(['controller' => 'People', 'action' => 'exportScoreCsv',], true);
                    $exportURL .= '/' . $selectedFilters['session'];
                    $exportURL .= '/' . $selectedFilters['chamber'];
                    echo $this->element('People/bulkedit_scores', [
                        'exportURL' => $exportURL
                    ]);
                }
                ?>
            </div>
            <?php
            if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes') :
                ?>
                <div class="tab-pane fade" id="criteria" role="tabpanel" aria-labelledby="criteria-tab">
                    <?php
                    if (empty($calculatedScores)) {
                        echo "<div class='alert alert-warning'>Please check back later.</div>";
                    } else {
                        $exportURL = Router::url(['controller' => 'People', 'action' => 'exportScoreCsv',], true);
                        $exportURL .= '/' . $selectedFilters['session'];
                        $exportURL .= '/' . $selectedFilters['chamber'];

                        echo $this->element('People/bulkedit_scores_criteria', [
                            'exportURL' => $exportURL
                        ]);
                    }
                    ?>
                </div>
            <?php endif; ?>
            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                <?php echo $this->element('People/bulkedit_contact'); ?>
            </div>
            <div class="tab-pane fade" id="social" role="tabpanel" aria-labelledby="social-tab">
                <?php echo $this->element('People/bulkedit_social'); ?>
            </div>
            <div class="tab-pane fade" id="tags" role="tabpanel" aria-labelledby="tags-tab">
                <?php echo $this->element('People/bulkedit_tags', [
                    'tags' => $tags->toArray(),
                    'peopleTags' => $peopleTags
                ]); ?>
            </div>
            <?php if (sizeof($integrations) > 0) : ?>
                <div class="tab-pane fade" id="integrations" role="tabpanel" aria-labelledby="integrations-tab">
                    <?php echo $this->element('People/bulkedit_integrations', [
                        'integrations' => $integrations
                    ]); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <!-- end col md 12 -->
</div>
<script type="application/javascript">
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        let targetTableId;
        switch (e.target.id) {
            case 'scores-tab':
                targetTableId = 'ScoresTable';
                break;
            case 'integrations-tab':
                targetTableId = 'IntegrationTable';
                break;
            case 'social-tab':
                targetTableId = 'SocialTable';
                break;
            case 'tags-tab':
                targetTableId = 'TagsTable';
                break;
            case 'contact-tab':
                targetTableId = 'ContactTable';
                break;
            default:
                targetTableId = 'DetailsTable';
        }
        $('table#' + targetTableId).fixedHeader();
    })
</script>
