<div class="people index">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('People'); ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Person'),
                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?>
            </li>
            <li class="nav-item">
                <?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp;&nbsp;Bulk Edit People'),
                    ['action' => 'bulkedit'], ['escape' => false, 'class' => 'nav-link']); ?>
            </li>
            <li class="nav-item">
                <?php echo $this->Html->link(__('<span class="octicon octicon-desktop-download"></span>&nbsp;&nbsp;CSV Export'),
                    ['action' => 'exportCsv'], ['escape' => false, 'class' => 'nav-link']); ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('full_name'); ?></th>
                    <th><?php echo $this->Paginator->sort('first_name'); ?></th>
                    <th><?php echo $this->Paginator->sort('last_name'); ?></th>
                    <th><?php echo $this->Paginator->sort('is_enabled', 'Enabled'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($people as $person): ?>
                    <tr>
                        <td><?php echo $person['full_name']; ?>&nbsp;</td>
                        <td><?php echo $person['first_name']; ?>&nbsp;</td>
                        <td><?php echo $person['last_name']; ?>&nbsp;</td>
                        <td><?php echo $this->Enable->display_text($person['is_enabled']); ?>&nbsp;</td>
                        <td class="actions">
                            <?php echo $this->Html->link('<span class="octicon octicon-search"></span>',
                                ['action' => 'view', $person['id']], ['escape' => false]); ?>
                            <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                                ['action' => 'edit', $person['id']], ['escape' => false]); ?>
                            <?php echo $this->Form->postLink('<span class="octicon octicon-trashcan"></span>',
                                ['action' => 'delete', $person['id']], ['escape' => false],
                                __('Are you sure you want to delete # {0}?', $person['id'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div><!-- end containing of content -->
