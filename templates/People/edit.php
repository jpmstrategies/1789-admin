<div class="people form">

    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Edit Person'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete'),
                                    ['action' => 'delete', $person->id],
                                    [
                                        'escape' => false,
                                        'class' => 'nav-link'], __('Are you sure you want to delete # {0}?',
                                        $person->id)); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List People'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($person, ['role' => 'form']); ?>

            <div class="form-group">
                <?php echo $this->Form->control('id', ['class' => 'form-control', 'placeholder' => 'Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('full_name',
                    ['class' => 'form-control', 'placeholder' => 'Full Name', 'disabled' => 'disabled']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('nickname',
                    ['class' => 'form-control', 'placeholder' => 'Nickname']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('salutation',
                    ['class' => 'form-control', 'placeholder' => 'Salutation']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('first_name',
                    ['class' => 'form-control', 'placeholder' => 'First Name']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('middle_name',
                    ['class' => 'form-control', 'placeholder' => 'Middle Name']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('initials',
                    ['class' => 'form-control', 'placeholder' => 'Initials']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('last_name',
                    ['class' => 'form-control', 'placeholder' => 'Last Name']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('image_url',
                    ['class' => 'form-control', 'placeholder' => 'Image URL', 'label' => 'Image URL']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('suffix', ['class' => 'form-control', 'placeholder' => 'Suffix']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('bio', ['class' => 'form-control', 'placeholder' => 'Bio']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('committee_list', [
                    'class' => 'form-control',
                    'placeholder' => 'Committee List']); ?>
                <small class="form-text text-muted">Note: the contents of this field can be overwritten by JPM processes
                    if we manage the creation of legislators for you.</small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('ballotpedia_url', [
                    'class' => 'form-control',
                    'placeholder' => 'Ballotpedia URL',
                    'label' => 'Ballotpedia URL']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('first_elected',
                    ['class' => 'form-control', 'placeholder' => 'YYYY']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('district_city',
                    ['class' => 'form-control', 'placeholder' => 'District City']); ?>
            </div>
            <div class="form-group">
                <?php
                if (sizeof($grades->toArray()) > 0) {
                    $type = '';
                    $empty = true;
                } else {
                    $type = 'hidden';
                    $empty = false;
                }
                echo $this->Form->control('grade_id',
                    ['class' => 'form-control', 'placeholder' => ' Career', 'type' => $type, 'empty' => $empty]);
                ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('grade_number',
                    ['class' => 'form-control', 'placeholder' => 'Grade Career Score', 'type' => 'text']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('slug',
                    ['class' => 'form-control', 'placeholder' => 'First Name-Last Name-Suffix']); ?>
                <small class="form-text text-muted">The friendly URL. It should contain no spaces and use dashes
                    instead.</small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('state_pk', [
                    'class' => 'form-control',
                    'label' => 'State Unique ID',
                    'type' => 'text']); ?>
                <small class="form-text text-muted">Please enter a unique ID that references the primary source.
                </small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('os_pk',
                    ['class' => 'form-control', 'label' => 'Open States ID']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('email',
                    ['class' => 'form-control', 'label' => 'Email']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('phone',
                    ['class' => 'form-control phone-input', 'label' => 'Phone']); ?>
            </div>
            <div class="form-group">
                <label for="facebook">Facebook</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">fb.com/</span>
                    </div>
                    <?php echo $this->Form->control('facebook', ['class' => 'form-control', 'label' => false]); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="twitter">Twitter</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon2">@</span>
                    </div>
                    <?php echo $this->Form->control('twitter', ['class' => 'form-control', 'label' => false]); ?>
                </div>
            </div>
            <div class="form-check">
                <?php echo $this->Form->control('is_enabled', ['label' => 'Enabled', 'class' => 'form-check-input']); ?>
            </div>
            <div class="form-check">
                <?php echo $this->Form->control('is_retired', ['label' => 'Retired', 'class' => 'form-check-input']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->submit(__('Submit'), ['class' => 'btn btn-primary']); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $('#bio').summernote({
            height: "250px"
        });
        $('#committee-list').summernote({
            height: "250px"
        });
        $('.phone-input').on('input', function() {
            var number = $(this).val().replace(/[^\d]/g, '');
            if (number.length > 6) {
                number = '(' + number.substring(0, 3) + ') ' + number.substring(3, 6) + '-' + number.substring(6, 10);
            } else if (number.length > 3) {
                number = '(' + number.substring(0, 3) + ') ' + number.substring(3);
            } else {
                number = number ? '(' + number : '';
            }
            $(this).val(number);
        }).on('blur', function() {
            // If the value is just "(", clear the field.
            if($(this).val() === '(') {
                $(this).val('');
            }
        });
    });
</script>
