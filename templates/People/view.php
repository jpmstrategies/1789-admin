<div class="people view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Person'); ?></h1>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp&nbsp;Edit Person'),
                                    ['action' => 'edit', $person['id']],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete Person'),
                                    ['action' => 'delete', $person['id']], ['escape' => false, 'class' => 'nav-link'],
                                    __('Are you sure you want to delete # {0}?', $person['id'])); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp&nbsp;List People'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Person'),
                                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Address'),
                                    ['controller' => 'Addresses', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Legislator'),
                                    ['controller' => 'Legislators', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->

        <div class="col-md-9">
            <table class="table table-striped table-sm">
                <tbody>
                <tr>
                    <th><?php echo __('Full Name'); ?></th>
                    <td>
                        <?php echo $person['full_name']; ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Nickname'); ?></th>
                    <td>
                        <?php echo $person['nickname']; ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Salutation'); ?></th>
                    <td>
                        <?php echo h($person['salutation']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('First Name'); ?></th>
                    <td>
                        <?php echo $person['first_name']; ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Middle Name'); ?></th>
                    <td>
                        <?php echo $person['middle_name']; ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Initials'); ?></th>
                    <td>
                        <?php echo h($person['initials']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Last Name'); ?></th>
                    <td>
                        <?php echo $person['last_name']; ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Image URL'); ?></th>
                    <td>
                        <?php echo h($person['image_url']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Suffix'); ?></th>
                    <td>
                        <?php echo h($person['suffix']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Bio'); ?></th>
                    <td>
                        <?php echo $person['bio']; ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Committee List'); ?></th>
                    <td>
                        <?php echo $person['committee_list']; ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Ballotpedia URL'); ?></th>
                    <td>
                        <?php echo h($person['ballotpedia_url']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('First Elected'); ?></th>
                    <td>
                        <?php echo h($person['first_elected']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('District City'); ?></th>
                    <td>
                        <?php echo h($person['district_city']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Slug'); ?></th>
                    <td>
                        <?php echo h($person['slug']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('State Unique ID'); ?></th>
                    <td>
                        <?php echo h($person['state_pk']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Open States ID'); ?></th>
                    <td>
                        <?php echo h($person['os_pk']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('LegiScan ID'); ?></th>
                    <td>
                        <?php echo h($person['legiscan_people_id']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Email'); ?></th>
                    <td>
                        <?php echo h($person['email']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Phone'); ?></th>
                    <td>
                        <?php echo h($person['phone']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Facebook'); ?></th>
                    <td>
                        <?php if ($person['facebook']) : ?>
                            <a href="https://facebook.com/<?php echo h($person['facebook']); ?>" target="_blank">
                                <?php echo 'fb.com/' . h($person['facebook']); ?>
                            </a>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Twitter'); ?></th>
                    <td>
                        <?php if ($person['twitter']) : ?>
                            <a href="https://twitter.com/<?php echo h($person['twitter']); ?>" target="_blank">
                                <?php echo '@' . h($person['twitter']); ?>
                            </a>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td>
                        <?php echo h($person['created']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td>
                        <?php echo h($person['modified']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Enabled'); ?></th>
                    <td>
                        <?php echo $this->Enable->display_text($person['is_enabled']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Retired'); ?></th>
                    <td>
                        <?php echo $this->Enable->display_text($person['is_retired']); ?>
                        &nbsp;
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
        <!-- end col md 9 -->

    </div>
</div>
<div class="related row">
    <div class="col-lg-12">
        <h3><?php echo __('Related Addresses'); ?></h3>

        <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Address'),
                        ['controller' => 'Addresses', 'action' => 'add'],
                        ['escape' => false, 'class' => 'btn btn-outline-success']); ?>
                </li>
            </ul>
            <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
        </nav>
        <?php if (!empty($addresses)): ?>
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?php echo __('Type'); ?></th>
                    <th><?php echo __('Capitol Address'); ?></th>
                    <th><?php echo __('Full Address'); ?></th>
                    <th><?php echo __('Phone'); ?></th>
                    <th></th>
                </tr>
                <thead>
                <tbody>
                <?php foreach ($addresses as $address): ?>
                    <tr>
                        <td>
                            <?php echo $this->Html->link($address['address_type']['name'],
                                [
                                    'controller' => 'AddressTypes',
                                    'action' => 'view',
                                    $address['address_type']['id']]); ?>
                        </td>
                        <td><?php echo $address['capitol_address']; ?></td>
                        <td><?php echo $address['full_address']; ?></td>
                        <td><?php echo $address['phone']; ?></td>
                        <td class="actions">
                            <?php echo $this->Html->link(__('<span class="octicon octicon-search"></span>'),
                                ['controller' => 'Addresses', 'action' => 'view', $address['id']],
                                ['escape' => false]); ?>
                            <?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>'),
                                ['controller' => 'Addresses', 'action' => 'edit', $address['id']],
                                ['escape' => false]); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
    <!-- end col md 12 -->
</div>
<div class="related row">
    <div class="col-lg-12">
        <h3><?php echo __('Related Legislators'); ?></h3>

        <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legislator'),
                        ['controller' => 'Legislators', 'action' => 'add'],
                        ['escape' => false, 'class' => 'btn btn-outline-success']); ?>
                </li>
            </ul>
            <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
        </nav>
        <?php if (!empty($legislators)): ?>
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?php echo __('Legislative Session'); ?></th>
                    <th><?php echo __('Chamber'); ?></th>
                    <th><?php echo __('Party'); ?></th>
                    <th><?php echo __('District'); ?></th>
                    <th><?php echo __('Journal Name'); ?></th>
                    <th><?php echo __('Number Grade'); ?></th>
                    <th><?php echo __('Enabled'); ?></th>
                    <th></th>
                </tr>
                <thead>
                <tbody>
                <?php foreach ($legislators as $legislator): ?>
                    <tr>
                        <td>
                            <?php echo $this->Html->link($legislator['legislative_session']['name'], [
                                'controller' => 'legislative_sessions',
                                'action' => 'view',
                                $legislator['legislative_session']['id']]); ?>
                        </td>
                        <td>
                            <?php echo $this->Html->link($legislator['chamber']['name'], [
                                'controller' => 'chambers',
                                'action' => 'view',
                                $legislator['chamber']['id']]); ?>
                        </td>
                        <td>
                            <?php echo $this->Html->link($legislator['party']['name'], [
                                'controller' => 'parties',
                                'action' => 'view',
                                $legislator['party']['id']]); ?>
                        </td>
                        <td><?php echo $legislator['district']; ?></td>
                        <td><?php echo $legislator['journal_name']; ?></td>
                        <td><?php echo $legislator['grade_number']; ?></td>
                        <td><?php echo $this->Enable->display_text($legislator['is_enabled']); ?></td>
                        <td class="actions">
                            <?php echo $this->Html->link(__('<span class="octicon octicon-search"></span>'),
                                ['controller' => 'Legislators', 'action' => 'view', $legislator['id']],
                                ['escape' => false]); ?>
                            <?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>'),
                                ['controller' => 'Legislators', 'action' => 'edit', $legislator['id']],
                                ['escape' => false]); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
    <!-- end col md 12 -->
</div>
