<div class="actions form">

    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Edit Action'); ?></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete'),
                                    ['action' => 'delete', $action->id],
                                    [
                                        'escape' => false,
                                        'class' => 'nav-link'], __('Are you sure you want to delete # {0}?',
                                        $action->id)); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Actions'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($action); ?>

            <div class="form-group">
                <?php echo $this->Form->control('id', ['class' => 'form-control', 'placeholder' => 'Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('rating_type_id',
                    ['class' => 'form-control', 'placeholder' => 'Rating Type Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('action_type_id',
                    ['class' => 'form-control', 'placeholder' => 'Action Type Id']); ?>
                <small class="form-text text-muted">The "Stance Evaluation*" action type is used to indicate which
                    actions can be used to evaluate if a legislator voted correctly.
                </small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('name', ['class' => 'form-control', 'placeholder' => 'Name']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('description',
                    ['class' => 'form-control', 'placeholder' => 'Description']); ?>
                <small class="form-text text-muted">BETA - The text as it is used for house journal vote statements.
                </small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('denom_value', [
                    'class' => 'form-control',
                    'placeholder' => '0',
                    'label' => 'Denominator Value',
                    'type' => 'text',
                    'default' => '0']); ?>
                <small class="form-text text-muted">BETA - Should this vote type increase or decrease the total count of
                    the denominator.
                </small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('multiplier_support', [
                    'class' => 'form-control',
                    'placeholder' => '0.00',
                    'label' => 'Support Multiplier',
                    'type' => 'text',
                    'default' => '0.00']); ?>
                <small class="form-text text-muted">BETA - What values should we multiply the rating weight by when they
                    "Support" our position.
                </small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('multiplier_oppose', [
                    'class' => 'form-control',
                    'placeholder' => '0.00',
                    'label' => 'Oppose Multiplier',
                    'type' => 'text',
                    'default' => '0.00']); ?>
                <small class="form-text text-muted">BETA - What values should we multiply the rating weight by when they
                    "Oppose" our position.
                </small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->submit(__('Submit'), ['class' => 'btn btn-primary']); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
