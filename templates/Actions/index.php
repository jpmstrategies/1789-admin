<?php
//pr($actions);
?>
<div class="actions index">

    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Actions'); ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <div class="alert alert-primary">
        <b>About the "Stance Evaluation" action type</b>
        <hr>
        This action type is a system default that cannot be removed. It is used to help the system identify actions that
        can be used to evaluate whether or not the legislator voted with the organization's configured stance on a
        rating. Typically, this will be Yea and Nay votes.
    </div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Action'),
                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?></li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('rating_type_id'); ?></th>
                    <th><?php echo $this->Paginator->sort('action_type_id'); ?></th>
                    <th><?php echo $this->Paginator->sort('name'); ?></th>
                    <th><?php echo $this->Paginator->sort('description'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($actions as $action): ?>
                    <tr>
                        <td>
                            <?php echo $this->Html->link($action['rating_type']['name'], [
                                'controller' => 'rating_types',
                                'action' => 'view',
                                $action['rating_type']['id']]); ?>
                        </td>
                        <td>
                            <?php echo $this->Html->link($action['action_type']['name'], [
                                'controller' => 'action_types',
                                'action' => 'view',
                                $action['action_type']['id']]); ?>
                        </td>
                        <td><?php echo h($action['name']); ?>&nbsp;</td>
                        <td><?php echo h($action['description']); ?>&nbsp;</td>
                        <td class="actions">
                            <?php if ($action['name'] !== 'N/A') : ?>
                                <?php echo $this->Html->link('<span class="octicon octicon-search"></span>',
                                    ['action' => 'view', $action['id']], ['escape' => false]); ?>
                                <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                                    ['action' => 'edit', $action['id']], ['escape' => false]); ?>
                                <?php echo $this->Form->postLink('<span class="octicon octicon-trashcan"></span>',
                                    ['action' => 'delete', $action['id']], ['escape' => false],
                                    __('Are you sure you want to delete # {0}?', $action['id'])); ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <p>
                <small><?php ?></small>
            </p>

            <?php
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->

</div><!-- end containing of content -->
