<div class="actions view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Action'); ?></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp&nbsp;Edit Action'),
                                    ['action' => 'edit', $action['id']],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete Action'),
                                    ['action' => 'delete', $action['id']], ['escape' => false, 'class' => 'nav-link'],
                                    __('Are you sure you want to delete # {0}?', $action['id'])); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp&nbsp;List Actions'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Action'),
                                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Action Type'),
                                    ['controller' => 'ActionTypes', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Rating Type'),
                                    ['controller' => 'RatingTypes', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">
                <tbody>
                <tr>
                    <th><?php echo __('Id'); ?></th>
                    <td>
                        <?php echo h($action['id']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Rating Type'); ?></th>
                    <td>
                        <?php echo $this->Html->link($action['rating_type']['name'],
                            ['controller' => 'RatingTypes', 'action' => 'view', $action['rating_type']['id']]); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Action Type'); ?></th>
                    <td>
                        <?php echo $this->Html->link($action['action_type']['name'],
                            ['controller' => 'ActionTypes', 'action' => 'view', $action['action_type']['id']]); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Name'); ?></th>
                    <td>
                        <?php echo h($action['name']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Description'); ?></th>
                    <td>
                        <?php echo h($action['description']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Denominator Value'); ?></th>
                    <td>
                        <?php echo h($action['denom_value']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Support Multiplier'); ?></th>
                    <td>
                        <?php echo h($action['multiplier_support']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Oppose Multiplier'); ?></th>
                    <td>
                        <?php echo h($action['multiplier_oppose']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td>
                        <?php echo h($action['created']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td>
                        <?php echo h($action['modified']); ?>
                        &nbsp;
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
        <!-- end col md 9 -->

    </div>
</div>
