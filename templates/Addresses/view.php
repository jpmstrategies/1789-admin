<div class="addresses view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Address'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp&nbsp;Edit Address'),
                                    ['action' => 'edit', $address['id']],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete Address'),
                                    ['action' => 'delete', $address['id']], ['escape' => false, 'class' => 'nav-link'],
                                    __('Are you sure you want to delete # {0}?', $address['id'])); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp&nbsp;List Addresses'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Address'),
                                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Person'),
                                    ['controller' => 'People', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">
                <tbody>
                <tr>
                    <th><?php echo __('Person'); ?></th>
                    <td>
                        <?php echo $this->Html->link($address->person['full_name'],
                            ['controller' => 'People', 'action' => 'view', $address->person['id']]); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Type'); ?></th>
                    <td>
                        <?php echo $this->Html->link($address['address_type']['name'],
                            ['controller' => 'AddressTypes', 'action' => 'view', $address['address_type']['id']]); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Capitol Address'); ?></th>
                    <td>
                        <?php echo h($address['capitol_address']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Full Address'); ?></th>
                    <td>
                        <?php echo $address['full_address']; ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Phone'); ?></th>
                    <td>
                        <?php echo h($address['phone']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Fax'); ?></th>
                    <td>
                        <?php echo h($address['fax']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td>
                        <?php echo h($address['created']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td>
                        <?php echo h($address['modified']); ?>
                        &nbsp;
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- end col md 9 -->
    </div>
</div>
