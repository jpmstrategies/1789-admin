<div class="addresses index">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Addresses'); ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Address'),
                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?></li>
            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Address Type'),
                    ['controller' => 'AddressTypes', 'action' => 'add'], [
                        'escape' => false,
                        'class' => 'nav-link']); ?></li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('People.full_name', 'Person'); ?></th>
                    <th><?php echo $this->Paginator->sort('AddressTypes.name', 'Address Type'); ?></th>
                    <th><?php echo $this->Paginator->sort('Addresses.full_address', 'Full Address'); ?></th>
                    <th><?php echo $this->Paginator->sort('Addresses.capitol_address', 'Capitol Address'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($addresses as $address): ?>
                    <tr>
                        <td>
                            <?php echo $this->Html->link($address->person['full_name'],
                                ['controller' => 'People', 'action' => 'view', $address->person['id']]); ?>
                        </td>
                        <td>
                            <?php echo $this->Html->link($address['address_type']['name'],
                                [
                                    'controller' => 'AddressTypes',
                                    'action' => 'view',
                                    $address['address_type']['id']]); ?>
                        </td>
                        <td><?php echo $address['full_address']; ?></td>
                        <td><?php echo h($address['capitol_address']); ?></td>
                        <td class="actions">
                            <?php echo $this->Html->link('<span class="octicon octicon-search"></span>',
                                ['action' => 'view', $address['id']], ['escape' => false]); ?>
                            <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                                ['action' => 'edit', $address['id']], ['escape' => false]); ?>
                            <?php echo $this->Form->postLink('<span class="octicon octicon-trashcan"></span>',
                                ['action' => 'delete', $address['id']], ['escape' => false],
                                __('Are you sure you want to delete # {0}?', $address['id'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <p>
                <small><?php ?></small>
            </p>

            <?php
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div><!-- end containing of content -->
