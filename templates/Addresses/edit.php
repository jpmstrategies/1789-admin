<div class="addresses form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Edit Address'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete'),
                                    ['action' => 'delete', $address->id],
                                    [
                                        'escape' => false,
                                        'class' => 'nav-link'], __('Are you sure you want to delete # {0}?',
                                        $address->id)); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Addresses'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($address); ?>

            <div class="form-group">
                <?php echo $this->Form->control('id', ['class' => 'form-control', 'placeholder' => 'Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('person_id',
                    [
                        'class' => 'select2-drop-mask',
                        'label' => 'Person',
                        'empty' => '',
                        'options' => $people,
                        'between' => '<br/>',
                        'selected' => $address['person_id']]); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('address_type_id', [
                    'class' => 'form-control',
                    'placeholder' => 'Type']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('capitol_address',
                    ['class' => 'form-control', 'placeholder' => 'Capitol Address']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('full_address',
                    ['class' => 'form-control', 'placeholder' => 'Full Address']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('fax', [
                    'class' => 'form-control',
                    'placeholder' => '(XXX) XXX-XXXX']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('phone', [
                    'class' => 'form-control',
                    'placeholder' => '(XXX) XXX-XXXX']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->submit(__('Submit'), ['class' => 'btn btn-primary']); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

        $('#full-address').summernote({
            height: "250px"
        });

        jQuery("#person-id").select2({
            theme: "bootstrap4",
            placeholder: "- select -",
            allowClear: true,
            width: "100%"
        });

        jQuery('#zip').mask("99999?-9999");
        jQuery("#fax").mask("(999) 999-9999");
        jQuery("#phone").mask("(999) 999-9999");

    });
</script>
