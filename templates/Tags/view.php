<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tag $tag
 */
?>
<div class="tags view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($tag->name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>
                                Edit Tag')
                                    , ['action' => 'edit', $tag->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>
                                List Tags')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>
                                New Tag')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($tag->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Slug') ?></th>
                    <td><?= h($tag->slug) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Css Class') ?></th>
                    <td><?= h($tag->css_class) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sort Order') ?></th>
                    <td><?= $this->Number->format($tag->sort_order) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($tag->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($tag->modified) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>

<!-- start related People -->
<div class="People related row">
    <div class="col-lg-12">
        <h3><?= __('Related People') ?></h3>
        <?php if (!empty($tag->people)): ?>
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="col"><?= __('Full Name') ?></th>
                    <th scope="col"><?= __('Nickname') ?></th>
                    <th scope="col"><?= __('Salutation') ?></th>
                    <th scope="col"><?= __('First Name') ?></th>
                    <th scope="col"><?= __('Initials') ?></th>
                    <th scope="col"><?= __('Last Name') ?></th>
                    <th scope="col"><?= __('Suffix') ?></th>
                    <th scope="col"><?= __('Bio') ?></th>
                    <th scope="col"><?= __('Committee List') ?></th>
                    <th scope="col"><?= __('Image Url') ?></th>
                    <th scope="col"><?= __('Ballotpedia Url') ?></th>
                    <th scope="col"><?= __('First Elected') ?></th>
                    <th scope="col"><?= __('District City') ?></th>
                    <th scope="col"><?= __('Grade Number') ?></th>
                    <th scope="col"><?= __('Slug') ?></th>
                    <th scope="col"><?= __('Os Pk') ?></th>
                    <th scope="col"><?= __('State Pk') ?></th>
                    <th scope="col"><?= __('Email') ?></th>
                    <th scope="col"><?= __('Facebook') ?></th>
                    <th scope="col"><?= __('Twitter') ?></th>
                    <th scope="col"><?= __('Is Enabled') ?></th>
                    <th scope="col"><?= __('Is Retired') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($tag->people as $people): ?>
                    <tr>
                        <td><?= h($people->full_name) ?></td>
                        <td><?= h($people->nickname) ?></td>
                        <td><?= h($people->salutation) ?></td>
                        <td><?= h($people->first_name) ?></td>
                        <td><?= h($people->initials) ?></td>
                        <td><?= h($people->last_name) ?></td>
                        <td><?= h($people->suffix) ?></td>
                        <td><?= h($people->bio) ?></td>
                        <td><?= h($people->committee_list) ?></td>
                        <td><?= h($people->image_url) ?></td>
                        <td><?= h($people->ballotpedia_url) ?></td>
                        <td><?= h($people->first_elected) ?></td>
                        <td><?= h($people->district_city) ?></td>
                        <td><?= h($people->grade_number) ?></td>
                        <td><?= h($people->slug) ?></td>
                        <td><?= h($people->os_pk) ?></td>
                        <td><?= h($people->state_pk) ?></td>
                        <td><?= h($people->email) ?></td>
                        <td><?= h($people->facebook) ?></td>
                        <td><?= h($people->twitter) ?></td>
                        <td><?= h($people->is_enabled) ?></td>
                        <td><?= h($people->is_retired) ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['controller' => 'People', 'action' => 'view', $people->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['controller' => 'People', 'action' => 'edit', $people->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['controller' => 'People', 'action' => 'delete', $people->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete #
                            {0}?', $people->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else : ?>
            <div class="alert alert-primary" role="alert">
                No People to display!
            </div>
        <?php endif; ?>
    </div>
    <!-- end col lg 12 -->
</div>
<!-- end related People -->
