<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bill $bill
 */
use Cake\Core\Configure;
?>
<style>
.upload-image {
    width: 50%;
    /* Set a classic checkerboard grid as background */
    background-image: linear-gradient(45deg, #ccc 25%, transparent 25%, transparent 75%, #ccc 75%, #ccc),
                      linear-gradient(45deg, #ccc 25%, transparent 25%, transparent 75%, #ccc 75%, #ccc);
    background-size: 20px 20px; /* Size of the checkerboard squares */
    background-position: 0 0, 10px 10px; /* Offset second layer for grid alignment */
}
</style>
<div class="bills view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($bill->name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>
                                Edit Bill')
                                    , ['action' => 'edit', $bill->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>
                                List Bills')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>
                                New Bill')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row" style="width: 200px"><?= __('Legislative Session') ?></th>
                    <td><?= $bill->has('legislative_session') ?
                            $this->Html->link($bill->legislative_session
                                ->name, ['controller' => 'LegislativeSessions',
                                'action' => 'view', $bill->legislative_session
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Filed Chamber') ?></th>
                    <td><?= $bill->has('filed_chamber') ?
                            $this->Html->link($bill->filed_chamber
                                ->name, ['controller' => 'Chambers',
                                'action' => 'view', $bill->filed_chamber
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Bill Number') ?></th>
                    <td><?= h($bill->bill_number) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Official Title') ?></th>
                    <td><?= h($bill->official_title) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Explainer Title') ?></th>
                    <td><?= h($bill->explainer_title) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sponsors') ?></th>
                    <td><?= h($bill->sponsors) ?></td>
                </tr>
                <?php
                $fields = [
                    'current_chamber' => ['Chambers', 'Current Chamber', 'No Chamber'],
                    'current_bill_stage' => ['BillEnums', 'Current Bill Stage', 'No Stage'],
                    'current_bill_status' => ['BillEnums', 'Current Bill Status', 'No Status'],
                    'current_bill_progres' => ['BillEnums', 'Current Bill Progress', 'No Progress'],
                    'current_bill_phase_icon' => ['BillEnums', 'Current Bill Process Phase Icon', 'No Phase Icon'],
                    'current_bill_rating_icon' => ['BillEnums', 'Current Bill Rating Icon', 'No Rating Icon'],
                    'current_bill_ctum' => ['BillEnums', 'Current Bill CTA', 'No Call to Action'],
                ];

                foreach ($fields as $field => $info) {
                    [$controller, $label, $noDataText] = $info;
                    ?>
                    <tr>
                        <th scope="row"><?= __($label) ?></th>
                        <td>
                            <?= $bill->has($field) ?
                                $this->Html->link($bill->{$field}->name, ['controller' => $controller, 'action' => 'view', $bill->{$field}->id]) :
                                $noDataText ?>
                        </td>
                    </tr>
                <?php } ?>
                <tr>
                    <th scope="row"><?= __('Current Bill CTA URL') ?></th>
                    <td><?= h($bill->current_bill_cta_url) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('LegiScan Bill') ?></th>
                    <td>
                        <?php
                        if ($bill->legiscan_bill_id > 0) {
                            ?>
                            <a href="<?php echo $bill->legiscan_bill->legiscan_url; ?>" target="_blank">
                                <?php echo $this->LegiscanBill->getBillName($bill->legiscan_bill->bill_number) ?>
                                <span class="octicon octicon-link-external"></span>
                            </a>
                            <?php
                        } else {
                            echo 'No LegiScan Bill';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('House Rating') ?></th>
                    <td><?= $bill->has('house_rating') ?
                            $this->Html->link($bill->house_rating
                                ->name, ['controller' => 'Ratings',
                                'action' => 'view', $bill->house_rating
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('House Rating Votes') ?></th>
                    <td><?php echo $bill->house_rating_votes; ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Senate Rating') ?></th>
                    <td><?= $bill->has('senate_rating') ?
                            $this->Html->link($bill->senate_rating
                                ->name, ['controller' => 'Ratings',
                                'action' => 'view', $bill->senate_rating
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Senate Rating Votes') ?></th>
                    <td><?php echo $bill->senate_rating_votes; ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Slug') ?></th>
                    <td><?= h($bill->slug) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Visibility') ?></th>
                    <td><?= $this->Visibility->display_text($bill->is_enabled); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Bill Id') ?></th>
                    <td><?= $this->Number->format($bill->legiscan_bill_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Feature Image') ?></th>
                    <td>
                        <?php
                            $hasCurrentUploadSrc = !empty($bill->upload->path);
                            $currentUploadSrc = $hasCurrentUploadSrc ? Configure::read('App.r2_base_url') . '/' . $bill->upload->path : null;
                            if ($hasCurrentUploadSrc) {
                                echo $this->Html->image($currentUploadSrc, ['class' => 'upload-image img-fluid']);
                            }
                        ?>
                    </td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>

    