<?php

use Cake\Routing\Router;
use Cake\Core\Configure;

$this->Html->script('View/bulkedit.js?v=20220816', ['block' => true]);
$this->Html->css('bulkedit.css', ['block' => true]);
?>
<style>
    .minimal-row {
        padding: 5px 0;
        border-bottom: 1px solid #ddd;
    }

    .minimal-row:last-child {
        border-bottom: none;
    }

    .first-column {
        width: 33%;
    }

    .upload-image {
        max-width: 50%;
        /* Set a classic checkerboard grid as background */
        background-image: linear-gradient(45deg, #ccc 25%, transparent 25%, transparent 75%, #ccc 75%, #ccc),
                          linear-gradient(45deg, #ccc 25%, transparent 25%, transparent 75%, #ccc 75%, #ccc);
        background-size: 20px 20px; /* Size of the checkerboard squares */
        background-position: 0 0, 10px 10px; /* Offset second layer for grid alignment */
    }
</style>
<div class="bills index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Bills') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-xl-12">
            <?php echo $this->Form->create(null, ['class' => 'form-horizontal', 'id' => 'FilterForm']); ?>
            <input type="hidden" name="_csrfToken" value="<?php echo $_COOKIE['csrfToken'] ?? null; ?>"/>
            <input type="hidden" name="url"
                   value="<?php echo Router::url(['controller' => 'Bills', 'action' => 'index'], true); ?>">
            <input type="hidden" name="saveUrl"
                   value="<?php echo Router::url([
                       'controller' => 'LegiscanBills',
                       'action' => 'bulk-update-row'], true); ?>">
            <div class="row">
                <div class="form-group form-group-sm col-sm-12">
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <?php echo $this->Form->control('session',
                                [
                                    'id' => 'session',
                                    'name' => 'session',
                                    'label' => false,
                                    'class' => 'custom-select',
                                    'placeholder' => 'Legislative Session Id',
                                    'default' => $selectedFilters['session'],
                                    'options' => $sessions]); ?>
                        </div>
                        <div class="col-sm-6">
                            <?php echo $this->Form->control('chamber',
                                [
                                    'id' => 'chamber',
                                    'name' => 'chamber',
                                    'label' => false,
                                    'class' => 'custom-select',
                                    'default' => $selectedFilters['chamber'],
                                    'empty' => '- All Chambers -',
                                    'options' => $chambers]); ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <?php
                            echo $this->Form->content('bill', [
                                'id' => 'bill',
                                'name' => 'bill',
                                'label' => false,
                                'class' => 'form-control',
                                'placeholder' => 'Filter Bill Number',
                                'default' => $selectedFilters['bill']
                            ])
                            ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <input type="submit" value="Filter Bills" class="btn btn-primary" id="filter-submit-btn"/>
                            <input type="submit" value="Reset" class="btn btn-danger" id="filter-reset-btn"/>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Bill')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-desktop-download"></span>&nbsp;&nbsp;CSV Export')
                    , ['action' => 'exportCsv',
                        '?' => ['session' => $selectedFilters['session'],
                            'chamber' => $selectedFilters['chamber'],
                            ]]
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col" class="first-column">
                        <?= $this->Paginator->sort('bill_number') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('current_chamber_id', 'Current Information') ?>
                    </th>
                    <th>
                        LegiScan Bill / Ratings
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($bills as $bill): ?>
                    <tr>
                        <td>
                            <div class="minimal-row">
                                <b><?= h($bill->bill_number) ?></b>
                            </div>
                            <div class="minimal-row">
                                <?= h($bill->official_title) ?>
                            </div>
                            <div class="minimal-row">
                                <?php
                                $this->Visibility->display_text($bill->is_enabled);
                                ?>
                            </div>
                            <div class="minimal-row">
                                <?php
                                    $hasCurrentUploadSrc = !empty($bill->upload->path);
                                    $currentUploadSrc = $hasCurrentUploadSrc ? Configure::read('App.r2_base_url') . '/' . $bill->upload->path : null;
                                    if ($hasCurrentUploadSrc) {
                                        echo $this->Html->image($currentUploadSrc, ['class' => 'upload-image img-fluid']);
                                    }
                                ?>
                            </div>
                        </td>
                        <td>
                            <?php
                            $fields = [
                                'current_chamber' => ['Chambers', 'No Chamber'],
                                'current_bill_stage' => ['BillEnums', 'No Stage'],
                                'current_bill_status' => ['BillEnums', 'No Status'],
                                'current_bill_ctum' => ['BillEnums', 'No Call to Action'],
                                'current_bill_progres' => ['BillEnums', 'No Progress'],
                                'current_bill_phase_icon' => ['BillEnums', 'No Phase Icon'],
                                'current_bill_rating_icon' => ['BillEnums', 'No Rating Icon'],
                            ];

                            foreach ($fields as $field => $info) {
                                [$controller, $noDataText] = $info;
                                echo '<div class="minimal-row">';
                                echo $bill->has($field) ?
                                    $this->Html->link($bill->{$field}->name, ['controller' => $controller, 'action' => 'view', $bill->{$field}->id]) :
                                    $noDataText;
                                echo '</div>';
                            }
                            ?>
                        </td>
                        <td>
                            <div class="minimal-row">
                                <?php
                                if ($bill->legiscan_bill_id > 0) {
                                    ?>
                                    <a href="<?php echo $bill->legiscan_bill->legiscan_url; ?>" target="_blank">
                                        <?php echo $this->LegiscanBill->getBillName($bill->legiscan_bill->bill_number) ?>
                                        <span class="octicon octicon-link-external"></span>
                                    </a>
                                    <?php
                                } else {
                                    echo 'No LegiScan Bill';
                                }
                                ?>
                            </div>
                            <?php
                            $fields = [
                                'house_rating' => ['Ratings', 'House', 'L'],
                                'senate_rating' => ['Ratings', 'Senate', 'U'],
                            ];

                            foreach ($fields as $field => $info) {
                                [$controller, $chamber, $chamberType] = $info;
                                echo '<div class="minimal-row">';

                                if (isset($bill->{$field}->id)) {
                                    echo $this->Html->link(__($chamber . ' Rating Linked'),
                                        ['controller' => 'Ratings', 'action' => 'view', $bill->{$field}->id],
                                        ['escape' => false, 'target' => '_blank']
                                    );
                                    echo ' ';
                                    echo $this->Html->link(__('Unlink'),
                                        ['controller' => 'Bills', 'action' => 'unlink-rating',
                                            '?' => ['bill_chamber' => $chamberType, 'bill_uuid' => $bill->id]],
                                        ['escape' => false, 'class' => 'btn btn-outline-danger btn-sm']);
                                } else {
                                    echo 'No ' . $chamber . ' Rating';
                                    echo ' ';
                                    echo $this->Html->link(__('Link'),
                                        ['controller' => 'LegiscanBills', 'action' => 'view', $bill->legiscan_bill_id,
                                            '?' => ['bill_chamber' => $chamberType, 'bill_uuid' => $bill->id]],
                                        ['escape' => false, 'target' => '_blank', 'class' => 'btn btn-outline-success btn-sm']);
                                }

                                echo '</div>';
                            }
                            ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $bill->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $bill->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $bill->id]
                                , ['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $bill->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->

<script type="text/javascript">
    function serializeFormJson(fields) {
        let o = {};
        let a = fields.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }

    function objectToQueryString(obj) {
        let str = [];
        for (let p in obj) {
            if (obj.hasOwnProperty(p) && obj[p] !== '' && obj[p] != null) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        }

        if (str.length > 0) {
            return '?' + str.join("&");
        }
    }

    jQuery(document).ready(function ($) {
        const filterForm = $('#FilterForm');

        jQuery('input#filter-submit-btn').on("click", function (e) {
            e.preventDefault();

            const formData = serializeFormJson(filterForm.find(':input'));
            delete formData.saveUrl
            delete formData.url
            delete formData._csrfToken

            let qs = objectToQueryString(formData)
            if (qs && qs.length > 0) {
                let newUrl = filterForm.find('[name="url"]').val() + qs
                window.location.replace(newUrl);
            }
        });

        jQuery('input#filter-reset-btn').on("click", function (e) {
            e.preventDefault();
            jQuery('select#chamber').val(jQuery('input#chamber option:first').val())
            jQuery('select#session').val(jQuery('input#session option:first').val())
            jQuery('input#bill').val('')

            // remove URL query strings
            let uri = window.location.href.toString();
            if (uri.indexOf("?") > 0) {
                let clean_uri = uri.substring(0, uri.indexOf("?"));
                window.history.replaceState({}, document.title, clean_uri);
            }
            window.location.reload(true)
        });
    });
</script>
