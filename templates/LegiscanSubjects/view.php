<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanSubject $legiscanSubject
 */
?>
<div class="legiscanSubjects view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($legiscanSubject->subject_name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>
                                Edit Legiscan Subject')
                                    , ['action' => 'edit', $legiscanSubject->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>
                                List Legiscan Subjects')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>
                                New Legiscan Subject')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('State') ?></th>
                    <td><?= h($legiscanSubject->state) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Subject Name') ?></th>
                    <td><?= h($legiscanSubject->subject_name) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>

<!-- start related Legiscan Bill Subjects -->
<div class="Legiscan Bill Subjects related row">
    <div class="col-lg-12">
        <h3><?= __('Related Legiscan Bill Subjects') ?></h3>
        <?php if (!empty($legiscanSubject->legiscan_bill_subjects)): ?>
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="col"><?= __('Bill Number') ?></th>
                    <th scope="col"><?= __('Subject Name') ?></th>
                    <th scope="col"><?= __('State Name') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($legiscanSubject->legiscan_bill_subjects as $legiscanBillSubjects): ?>
                    <tr>
                        <td><?= h($legiscanBillSubjects->bill_number) ?></td>
                        <td><?= h($legiscanBillSubjects->subject_name) ?></td>
                        <td><?= h($legiscanBillSubjects->state_name) ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , [
                                    'controller' => 'LegiscanBillSubjects',
                                    'action' => 'view',
                                    $legiscanBillSubjects->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , [
                                    'controller' => 'LegiscanBillSubjects',
                                    'action' => 'edit',
                                    $legiscanBillSubjects->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , [
                                    'controller' => 'LegiscanBillSubjects',
                                    'action' => 'delete',
                                    $legiscanBillSubjects->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete #
                            {0}?', $legiscanBillSubjects->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else : ?>
            <div class="alert alert-primary" role="alert">
                No Legiscan Bill Subjects to display!
            </div>
        <?php endif; ?>
    </div>
    <!-- end col lg 12 -->
</div>
<!-- end related Legiscan Bill Subjects -->
