<div class="ratings form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Add Rating'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Ratings'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($rating, ['role' => 'form']); ?>
            <?php
            $useCriteria = isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes';
            if ($currentTenantType === 'F') {
                $useCriteria = false;
            }
            if (isset($legiscanBill['id'])) {
                echo $this->Form->control('legiscan_bill_id', [
                    'type' => 'hidden',
                    'default' => $legiscanBill['id']
                ]);
            }
            if (isset($legiscanBillVote['id'])) {
                echo $this->Form->control('legiscan_bill_vote_id', [
                    'type' => 'hidden',
                    'default' => $legiscanBillVote['id']
                ]);
            }
            ?>

            <div class="form-group">
                <?php echo $this->Form->control('legislative_session_id',
                    [
                        'class' => 'form-control',
                        'placeholder' => 'Legislative Session Id',
                        'empty' => true,
                        'default' => $defaultSessionId]); ?>
                <?php if (sizeof($legislativeSessions->toArray()) === 0) : ?>
                    <div class="error-message">Please configure Legislative Sessions by clicking
                        <?php echo $this->Html->link(__('here'),
                            ['controller' => 'LegislativeSessions', 'action' => 'add']); ?>.
                    </div>
                <?php endif; ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('chamber_id',
                    [
                        'class' => 'form-control',
                        'placeholder' => 'Chamber',
                        'empty' => true,
                        'default' => $defaultChamberId]); ?>
            </div>
            <div class="form-group">
                <?php
                if (isset($legiscanBillVote['bill_number']) && isset($legiscanBillVote['roll_call_desc'])) {
                    $name = $this->LegiscanBill->getBillName($legiscanBillVote['bill_number']) . ': ' . $legiscanBillVote['roll_call_desc'];
                } elseif(isset($legiscanBill['bill_number'])) {
                    $name = $this->LegiscanBill->getBillName($legiscanBill['bill_number']);
                } else {
                    $name = null;
                }

                echo $this->Form->control('name', [
                    'class' => 'form-control',
                    'placeholder' => 'Name',
                    'default' => $name]); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('description',
                    [
                        'class' => 'form-control',
                        'placeholder' => 'Description',
                        'default' => $legiscanBill['description'] ?? null]); ?>
            </div>

            <?php if ($currentTenantType === 'F') : ?>
                <div class="form-group">
                    <?php echo $this->Form->control('fiscal_note',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Fiscal Note',
                            'default' => $legiscanBill['fiscal_note'] ?? null]); ?>
                </div>

                <div class="form-group">
                    <?php echo $this->Form->control('digest',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Digest',
                            'default' => $legiscanBill['digest'] ?? null]); ?>
                </div>

                <div class="form-group">
                    <?php echo $this->Form->control('vote_recommendation_notes',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Vote Recommendation Notes',
                            'default' => $legiscanBill['vote_recommendation_notes'] ?? null]); ?>
                </div>

                <div class="form-group">
                    <?php echo $this->Form->control('vote_references',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'References',
                            'default' => $legiscanBill['vote_references'] ?? null]); ?>
                </div>
            <?php endif; ?>

            <?php if ($useCriteria) : ?>
            <!-- end accordion header !-->
            <div class="form-group">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="additionalConfiguration">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                                        aria-expanded="false" aria-controls="collapseOne" type="button">
                                    Additional Configurations
                                </button>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse hide" aria-labelledby="additionalConfiguration"
                             data-parent="#accordion">
                            <div class="card-body">
                                <?php endif; ?>
                                <!-- end accordion header !-->

                                <!-- start additional configuration fields !-->
                                <div class="form-group">
                                    <?php
                                    $ratingTypes = $ratingTypes->toArray();

                                    if (sizeof($ratingTypes) === 1) {
                                        $defaultRatingId = key($ratingTypes);
                                    } else {
                                        $defaultRatingId = null;
                                    }
                                    echo $this->Form->control('rating_type_id',
                                        [
                                            'type' => ($currentTenantType === 'F' ? 'hidden' : 'select'),
                                            'class' => 'form-control',
                                            'placeholder' => 'Rating Type Id',
                                            'empty' => true,
                                            'default' => $defaultRatingId]); ?>
                                    <?php if (sizeof($ratingTypes) === 0) : ?>
                                        <div class="error-message">Please configure Rating Types by clicking
                                            <?php echo $this->Html->link(__('here'),
                                                ['controller' => 'RatingTypes', 'action' => 'add']); ?>.
                                        </div>
                                    <?php else : ?>
                                        <?php if ($currentTenantType !== 'F') : ?>
                                            <small class="form-text text-muted">The value select impacts the Action option
                                                below.</small>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                </div>
                                <?php if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] !== 'yes') : ?>
                                    <div class="form-group">
                                        <?php echo $this->Form->control('action_id',
                                            ['class' => 'form-control', 'placeholder' => 'Action', 'empty' => true]); ?>
                                        <?php if (sizeof($actions) === 0) : ?>
                                            <div class="error-message">Please configure an Action with "Evaluation
                                                Stance" as the action
                                                type by
                                                clicking
                                                <?php echo $this->Html->link(__('here'),
                                                    ['controller' => 'Actions', 'action' => 'add']); ?>.
                                            </div>
                                        <?php else : ?>
                                            <small class="form-text text-muted">Select the correct action for a
                                                legislator to get points.
                                                Only
                                                actions with "Evaluation Stance" set as the action type will show up in
                                                this option.
                                            </small>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                                <div class="form-group">
                                    <?php echo $this->Form->control('position_id',
                                        ['class' => 'form-control', 'placeholder' => 'Position', 'empty' => true]); ?>
                                    <?php if (sizeof($positions->toArray()) === 0) : ?>
                                        <div class="error-message">Please configure Positions by clicking
                                            <?php echo $this->Html->link(__('here'),
                                                ['controller' => 'Positions', 'action' => 'add']); ?>.
                                        </div>
                                    <?php else : ?>
                                        <small class="form-text text-muted">Select the position taken on this
                                            rating.</small>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <?php
                                    if (isset($legiscanBillVote['roll_call_desc'])) {
                                        preg_match('/.*RV#([0-9]+)/i', (string)$legiscanBillVote['roll_call_desc'], $recordVoteMatches);
                                        $defaultRecordVote = $recordVoteMatches[1] ?? 0;
                                    } else {
                                        $defaultRecordVote = 0;
                                    }
                                    if ($currentTenantType !== 'F') {
                                        echo $this->Form->control('record_vote',
                                            [
                                                'class' => 'form-control',
                                                'label' => 'Record Vote/Senate Journal Page',
                                                'type' => 'text',
                                                'default' => $defaultRecordVote]);
                                    }
                                    ?>
                                </div>
                                <?php if ($currentTenantType !== 'F') : ?>
                                    <div class="form-group">
                                        <?php echo $this->Form->control('rating_weight',
                                            ['class' => 'form-control', 'placeholder' => 'Rating Weight', 'type' => 'text', 'default' => 1]); ?>
                                        <small class="form-text text-muted">Used to automatically calculate scores.</small>
                                    </div>
                                <?php endif; ?>
                                <div class="form-group">
                                    <?php
                                    if (!empty($legiscanBill)) {
                                        $defaultSlug = $legiscanSession['slug'] . '-' . $legiscanBill['current_body_short'];
                                        if ($defaultRecordVote > 0) {
                                            $defaultSlug .= '-record-vote-' . $defaultRecordVote;
                                        } elseif (isset($legiscanBillVote['id'])) {
                                            $defaultSlug .= '-' . $legiscanBillVote['id'];
                                        } elseif (isset($legiscanBill['id'])) {
                                            $defaultSlug = $legiscanSession['slug'] . '-' . $this->LegiscanBill->getBillName($legiscanBill['bill_number']);
                                        }
                                        $defaultSlug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $defaultSlug)));
                                    } else {
                                        $defaultSlug = null;
                                    }
                                    ?>
                                    <?php echo $this->Form->control('slug',
                                        ['class' => 'form-control',
                                            'placeholder' => 'Year-Chamber-Rating Type-Unique Identifier',
                                            'default' => $defaultSlug]); ?>
                                    <small class="form-text text-muted">The friendly URL. It should contain no spaces
                                        and use dashes
                                        instead. For Example:<br/><br/>
                                        YYYY-house-vote-rv##<br/>
                                        YYYY-senate-vote-jp##
                                    </small>
                                </div>
                                <div class="form-check">
                                    <?php echo $this->Form->control('is_enabled', ['label' => 'Enabled', 'class' => 'form-check-input']); ?>
                                </div>
                                <!-- end additional configuration fields !-->


                                <!-- start accordion footer !-->
                                <?php if ($useCriteria) : ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end accordion footer !-->
        <?php endif; ?>

            <div class="form-group">
                <?php echo $this->Form->submit(__('Submit'), ['class' => 'btn btn-primary']); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $('#description').summernote({
            height: "250px"
        });

        <?php if($currentTenantType === 'F') : ?>
        $('#fiscal-note').summernote({
            height: "250px"
        });

        $('#digest').summernote({
            height: "250px"
        });

        $('#vote-recommendation-notes').summernote({
            height: "250px"
        });

        $('#vote-references').summernote({
            height: "250px"
        });
        <?php endif; ?>

        // disable action
        if ($("#rating-type-id option:selected").val() === '') {
            $("#action-id").prop("disabled", true);
        }

        $("#rating-type-id").on("change", function () {
            // get current label text
            var rating_type_name = $("#rating-type-id option:selected").text();

            var actionSelect = $("#action-id");

            if (rating_type_name === '') {
                // unset current action
                actionSelect.find('option:selected').remove();

                // add blank action
                actionSelect.find('option[value=""]').remove();
                actionSelect.prepend("<option value='' selected='selected'></option>").prop("disabled", false);

                // disable action
                actionSelect.prop("disabled", true);

            } else {
                // unset current action
                actionSelect.find('option:selected').remove();

                // disable all option groups
                actionSelect.hideOptionGroup();

                // add blank action
                actionSelect.find('option[value=""]').remove();
                actionSelect.prepend("<option value='' selected='selected'></option>").prop("disabled", false);

                // show new correct group
                var optionGroup = actionSelect.find("optgroup[label='" + rating_type_name + "']");

                optionGroup.prop("disabled", false);

                // enable action select
                actionSelect.prop("disabled", false).show();
            }
        });
    });

    $.fn.hideOptionGroup = function () {
        $(this).hide();
        $(this).children().each(function () {
            $(this).attr("disabled", "disabled").removeAttr("selected");
        });
        $(this).appendTo($(this).parent());

    }
</script>
