<div class="ratings index">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Ratings'); ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <?php
                if ($currentTenantDataSource === 'legiscan') {
                    echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating'),
                        ['controller' => 'LegiscanBills', 'action' => 'index'], ['escape' => false, 'class' => 'nav-link']);
                } else {
                    echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating'),
                        ['controller' => 'Ratings', 'action' => 'add'], ['escape' => false, 'class' => 'nav-link']);
                }
                ?>
            </li>
            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp;&nbsp;Bulk Edit Ratings'),
                    ['controller' => 'Ratings', 'action' => 'bulkedit'], [
                        'escape' => false,
                        'class' => 'nav-link']); ?></li>
            <?php if($currentTenantType !== 'F') : ?>
                <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp;&nbsp;Bulk Edit Records'),
                        ['controller' => 'Records', 'action' => 'bulkedit'], [
                            'escape' => false,
                            'class' => 'nav-link']); ?></li>
            <?php endif; ?>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('LegislativeSessions.name', 'Legislative Session'); ?></th>
                    <th><?php echo $this->Paginator->sort('Chambers.name', 'Chamber'); ?></th>
                    <th><?php echo $this->Paginator->sort('Ratings.name', 'Rating Title'); ?></th>
                    <?php if($currentTenantType === 'F') : ?>
                        <th><?php echo $this->Paginator->sort('Positions.name', 'Position'); ?></th>
                    <?php else : ?>
                        <th><?php echo $this->Paginator->sort('Actions.name', 'Action'); ?></th>
                    <?php endif; ?>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($ratings as $rating): ?>
                    <tr>
                        <td>
                            <?php echo $this->Html->link($rating['legislative_session']['name'], [
                                'controller' => 'legislative_sessions',
                                'action' => 'view',
                                $rating['legislative_session']['id']]); ?>
                        </td>
                        <td>
                            <?php echo $this->Html->link($rating['chamber']['name'], [
                                'controller' => 'chambers',
                                'action' => 'view',
                                $rating['chamber']['id']]); ?>
                        </td>
                        <td style="width: 500px"><?php echo h($rating['name']); ?>&nbsp;</td>
                        <?php if($currentTenantType === 'F') : ?>
                            <td>
                                <?php echo isset($rating['position']['name']) ? $this->Html->link($rating['position']['name'], [
                                    'controller' => 'positions',
                                    'action' => 'view',
                                    $rating['position']['id']]) : 'n/a'; ?>
                            </td>
                        <?php else : ?>
                            <td>
                                <?php echo isset($rating['action']['name']) ? $this->Html->link($rating['action']['name'], [
                                    'controller' => 'actions',
                                    'action' => 'view',
                                    $rating['action']['id']]) : 'n/a'; ?>
                            </td>
                        <?php endif; ?>
                        <td class="actions" style="text-align: center">
                            <?php echo $this->Html->link('<span class="octicon octicon-search" style="padding-right: 8px" title="View Rating"></span>',
                                ['action' => 'view', $rating['id']], ['escape' => false]); ?>
                            <?php if($currentTenantType !== 'F') : ?>
                                <?php echo $this->Html->link('<span class="octicon octicon-tasklist" style="padding-right: 8px" title="Bulk Import Votes"></span>',
                                    [
                                        'controller' => 'Records',
                                        'action' => 'bulk-import',
                                        $rating['id']], ['escape' => false]); ?>
                                <?php echo $this->Html->link('<span class="octicon octicon-desktop-download" style="padding-right: 8px" title="Download Votes"></span>',
                                    [
                                        'controller' => 'Records',
                                        'action' => 'exportCsv',
                                        $rating['id']], ['escape' => false]); ?>
                            <?php endif; ?>
                            <?php echo $this->Html->link('<span class="octicon octicon-pencil" style="padding-right: 8px" title="Edit Rating"></span>',
                                ['action' => 'edit', $rating['id']], ['escape' => false]); ?>
                            <?php echo $this->Form->postLink('<span class="octicon octicon-trashcan" title="Delete Rating"></span>',
                                ['action' => 'delete', $rating['id']], ['escape' => false],
                                __('Are you sure you want to delete # {0}?', $rating['id'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <p>
                <small><?php ?></small>
            </p>

            <?php
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div><!-- end containing of content -->
