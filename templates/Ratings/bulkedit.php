<?php

use Cake\Routing\Router;

$this->Html->script('View/bulkedit.js?v=20220816', ['block' => true]);
$this->Html->css('bulkedit.css', ['block' => true]);
?>
<div class="ratings view">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-header">
                <h1><?php echo __('Rating Bulk Edit'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <?php echo $this->Form->create(null, ['class' => 'form-horizontal', 'id' => 'FilterAdminBulkeditForm']); ?>
            <input type="hidden" name="_csrfToken" value="<?php echo $_COOKIE['csrfToken']; ?>"/>
            <input type="hidden" name="url"
                   value="<?php echo Router::url(['controller' => 'Ratings', 'action' => 'bulkedit'], true); ?>">
            <input type="hidden" name="saveUrl"
                   value="<?php echo Router::url(['controller' => 'Ratings', 'action' => 'bulk-update-row'], true); ?>">
            <div class="row">
                <div class="form-group form-group-sm col-sm-9">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('session',
                                [
                                    'label' => false,
                                    'class' => 'custom-select',
                                    'placeholder' => 'Legislative Session Id',
                                    'default' => $selectedFilters['session']]); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('chamber', [
                                'label' => false,
                                'name' => 'chamber',
                                'class' => 'custom-select',
                                'default' => $selectedFilters['chamber']]); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'no') : ?>
                                <a href="<?php echo $downloadUrl; ?>" class="btn btn-success" target="_blank">
                                    <span class="octicon octicon-desktop-download"></span>&nbsp; &nbsp; Vote Grid
                                    Export
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<div class="related row">
    <div class="col-xl-12">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab"
                   aria-controls="details"
                   aria-selected="true">Details</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="priorities-tab" data-toggle="tab" href="#priorities" role="tab"
                   aria-controls="priorities"
                   aria-selected="true">Priorities</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="scores-tab" data-toggle="tab" href="#scores" role="tab"
                   aria-controls="scores" aria-selected="false">Scoring</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                <?php echo $this->element('Ratings/bulkedit_details'); ?>
            </div>
            <div class="tab-pane fade" id="priorities" role="tabpanel" aria-labelledby="priorities-tab">
                <?php echo $this->element('Ratings/bulkedit_priorities', [
                    'priorities' => $priorities->toArray(),
                    'ratingsPriorities' => $ratingsPriorities
                ]); ?>
            </div>
            <div class="tab-pane fade" id="scores" role="tabpanel" aria-labelledby="scores-tab">
                <?php echo $this->element('Ratings/bulkedit_scores'); ?>
            </div>
        </div>
    </div>
    <!-- end col md 12 -->
</div>
<script type="application/javascript">
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        let targetTableId;
        switch (e.target.id) {
            case 'details-tab':
                targetTableId = 'DetailsTable';
                break;
            case 'priorities-tab':
                targetTableId = 'PrioritiesTable';
                break;
            case 'scores-tab':
                targetTableId = 'ScoresTable';
                break;
            default:
                targetTableId = 'DetailsTable';
        }
        $('table#' + targetTableId).fixedHeader();
    })
</script>
