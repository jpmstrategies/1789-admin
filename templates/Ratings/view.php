<?php
use Cake\Routing\Router;
?>
<div class="ratings view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Rating'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp&nbsp;Edit Rating'),
                                    ['action' => 'edit', $rating['id']],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete Rating'),
                                    ['action' => 'delete', $rating['id']], ['escape' => false, 'class' => 'nav-link'],
                                    __('Are you sure you want to delete # {0}?', $rating['id'])); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp&nbsp;List Ratings'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <?php if($currentTenantType !== 'F') : ?>
                                <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-desktop-download"></span>&nbsp&nbsp;CSV Export'),
                                        [
                                            'controller' => 'Records',
                                            'action' => 'exportCsv',
                                            $rating['id']], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">
                <tbody>
                <tr>
                    <th><?php echo __('Legislative Session'); ?></th>
                    <td>
                        <?php echo $this->Html->link($rating['legislative_session']['name'], [
                            'controller' => 'legislative_sessions',
                            'action' => 'view',
                            $rating['legislative_session']['id']]); ?>
                        &nbsp;
                    </td>
                </tr>
                <?php if ($currentTenantType !== 'F') : ?>
                    <tr>
                        <th><?php echo __('Rating Type'); ?></th>
                        <td>
                            <?php echo $this->Html->link($rating['rating_type']['name'],
                                ['controller' => 'RatingTypes', 'action' => 'view', $rating['rating_type']['id']]); ?>
                            &nbsp;
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <th><?php echo __('Chamber'); ?></th>
                    <td>
                        <?php echo $this->Html->link($rating['chamber']['name'], [
                            'controller' => 'chambers',
                            'action' => 'view',
                            $rating['chamber']['id']]); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Name'); ?></th>
                    <td>
                        <?php echo h($rating['name']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Description'); ?></th>
                    <td>
                        <?php echo $rating['description']; ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Position'); ?></th>
                    <td>
                        <?php echo isset($rating['position']['name']) ? $this->Html->link($rating['position']['name'], [
                            'controller' => 'positions',
                            'action' => 'view',
                            $rating['position']['id']]) : 'n/a'; ?>
                        &nbsp;
                    </td>
                </tr>
                <?php if ($currentTenantType !== 'F') : ?>
                    <tr>
                        <th><?php echo __('Action'); ?></th>
                        <td>
                            <?php echo isset($rating['action']['name']) ? $this->Html->link($rating['action']['name'], [
                                'controller' => 'actions',
                                'action' => 'view',
                                $rating['action']['id']]) : 'n/a'; ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Record Vote/Senate Journal Page'); ?></th>
                        <td>
                            <?php echo h($rating['record_vote']); ?>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Rating Weight'); ?></th>
                        <td>
                            <?php echo h($rating['rating_weight']); ?>
                            &nbsp;
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <th><?php echo __('Slug'); ?></th>
                    <td>
                        <?php echo h($rating['slug']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Sort Order'); ?></th>
                    <td>
                        <?php echo h($rating['sort_order']); ?>
                        &nbsp;
                    </td>
                </tr>
                <?php if ($currentTenantType === 'F') : ?>
                    <tr>
                        <th><?php echo __('LegiScan Bill ID'); ?></th>
                        <td>
                            <?php echo h($rating['legiscan_bill_id']); ?>
                            &nbsp;
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if ($currentTenantType !== 'F') : ?>
                    <tr>
                        <th><?php echo __('LegiScan Vote ID'); ?></th>
                        <td>
                            <?php echo h($rating['legiscan_bill_vote_id']); ?>
                            &nbsp;
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <th><?php echo __('Enabled'); ?></th>
                    <td>
                        <?php echo $this->Enable->display_text($rating['is_enabled']); ?>
                        &nbsp;
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
        <!-- end col md 9 -->

    </div>
</div>

<?php if ((is_countable($criterias) ? count($criterias) : 0)) : ?>
    <div class="row">
        <div class="col-lg-12">
            <h3><?php echo __('Criteria'); ?></h3>
            <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <?php
                        $editCriteriaUrl = Router::url(['controller' => 'rating-criterias', 'action' => 'bulkedit',], true);
                        $editCriteriaUrl .= '/' . $rating->legislative_session_id;
                        $editCriteriaUrl .= '/' . $rating->id;
                        ?>
                        <a href="<?php echo $editCriteriaUrl; ?>" class="nav-link" target="_blank">
                            <span class="octicon octicon-pencil"></span>&nbsp; &nbsp; Edit Criteria Positions
                        </a>
                    </li>
                </ul>
            </nav>
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?php echo __('Criteria'); ?></th>
                    <th><?php echo __('Value'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($criterias as $criteria) : ?>
                    <?php
                    $ratingCriteria = $ratingCriterias[$criteria['id']] ?? 'not-configured';
                    $criteriaDisplay = $criteriaDetails[$ratingCriteria] ?? 'Not Configured';
                    ?>
                    <tr>
                        <td><?php echo $criteria['name']; ?></td>
                        <td><?php echo $criteriaDisplay; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>


<?php if($currentTenantType === 'F') : ?>
<div class="related row">
    <div class="col-lg-12">
        <h3><?php echo __('Bill Analysis'); ?></h3>

        <h4><?php echo __('Digest'); ?></h4>
        <?php echo $rating['digest']; ?>

        <h4><?php echo __('Fiscal Note'); ?></h4>
        <?php echo $rating['fiscal_note']; ?>

        <h4><?php echo __('Vote Recommendation Notes'); ?></h4>
        <?php echo $rating['vote_recommendation_notes']; ?>

        <h4><?php echo __('Vote References'); ?></h4>
        <?php echo $rating['vote_references']; ?>
    </div>
</div>
<?php endif; ?>

<?php if(!empty($floorReports)) : ?>
<div class="related row">
    <div class="col-lg-12">
        <h3><?php echo __('Floor Reports'); ?></h3>
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th><?php echo __('Report Date'); ?></th>
                <th><?php echo __('Approved'); ?></th>
                <th><?php echo __('Action'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($floorReports as $floorReport) : ?>
                <tr>
                    <td><?php echo $floorReport['report_date']; ?></td>
                    <td><?php echo $this->Enable->display_text($floorReport['is_approved']); ?></td>
                    <td>
                        <?php echo $this->Html->link(__('<span class="octicon octicon-search"></span>'),
                            ['controller' => 'FloorReports', 'action' => 'view', $floorReport['id']],
                            ['escape' => false]); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php endif; ?>

<?php if($currentTenantType !== 'F') : ?>
<div class="related row">
    <div class="col-lg-12">
        <h3><?php echo __('Related Records'); ?></h3>

        <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
            <ul class="navbar-nav mr-auto">
                <?php if (sizeof($records->toArray()) > 0) : ?>
                    <li class="nav-item">
                        <?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp;&nbsp;Bulk Edit'),
                            ['controller' => 'Records', 'action' => 'bulkedit', $rating['id']],
                            ['escape' => false, 'class' => 'nav-link']); ?>
                    </li>
                <?php endif; ?>
                <li class="nav-item">
                    <?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;Bulk Import'),
                        ['controller' => 'Records', 'action' => 'bulkImport', $rating['id']],
                        ['escape' => false, 'class' => 'nav-link']); ?>
                </li>
            </ul>
            <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
        </nav>
        <?php if (!empty($records)): ?>
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?php echo __('Action'); ?></th>
                    <th><?php echo __('Legislator'); ?></th>
                    <th><?php echo __('Chair'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($records as $record): ?>
                    <tr>
                        <td><?php echo $record->action['description']; ?></td>
                        <td><?php echo isset($record->legislator) ? $record->legislator['journal_name'] : null; ?></td>
                        <td><?php echo $this->Enable->display_text($record->is_chair); ?></td>
                        <td class="actions">
                            <?php echo $this->Html->link(__('<span class="octicon octicon-search"></span>'),
                                ['controller' => 'Records', 'action' => 'view', $record['id']],
                                ['escape' => false]); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
    <!-- end col md 12 -->
</div>
<?php endif; ?>