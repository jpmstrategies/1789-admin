<div class="ratings form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Edit Rating'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">

                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete'),
                                    ['action' => 'delete', $rating->id],
                                    [
                                        'escape' => false,
                                        'class' => 'nav-link'], __('Are you sure you want to delete # {0}?',
                                        $rating->id)); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Ratings'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($rating, ['role' => 'form']); ?>
            <?php
            if (isset($rating['legiscan_bill_id'])) {
                echo $this->Form->control('legiscan_bill_id', [
                    'type' => 'hidden',
                    'default' => $rating['legiscan_bill_id']
                ]);
            }
            if (isset($rating['legiscan_bill_vote_id'])) {
                echo $this->Form->control('legiscan_bill_vote_id', [
                    'type' => 'hidden',
                    'default' => $rating['legiscan_bill_vote_id']
                ]);
            }
            ?>

            <div class="form-group">
                <?php echo $this->Form->control('id', ['class' => 'form-control', 'placeholder' => 'Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('legislative_session_id',
                    ['class' => 'form-control', 'placeholder' => 'Legislative Session Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('chamber_id',
                    ['class' => 'form-control', 'placeholder' => 'Chamber']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('name', ['class' => 'form-control', 'placeholder' => 'Name']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('description',
                    ['class' => 'form-control', 'placeholder' => 'Description']); ?>
            </div>

            <?php if ($currentTenantType === 'F') : ?>
                <div class="form-group">
                    <?php echo $this->Form->control('fiscal_note',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Fiscal Note',
                            'default' => $legiscanBill['fiscal_note'] ?? null]); ?>
                </div>

                <div class="form-group">
                    <?php echo $this->Form->control('digest',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Digest',
                            'default' => $legiscanBill['digest'] ?? null]); ?>
                </div>

                <div class="form-group">
                    <?php echo $this->Form->control('vote_recommendation_notes',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Vote Recommendation Notes',
                            'default' => $legiscanBill['vote_recommendation_notes'] ?? null]); ?>
                </div>

                <div class="form-group">
                    <?php echo $this->Form->control('vote_references',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Vote References',
                            'default' => $legiscanBill['vote_references'] ?? null]); ?>
                </div>
            <?php endif; ?>

            <div class="form-group">
                <?php echo $this->Form->control('rating_type_id',
                    ['class' => 'form-control', 'placeholder' => 'Rating Type Id', 'empty' => true,
                        'type' => ($currentTenantType === 'F' ? 'hidden' : 'select')]); ?>
                <?php if ($currentTenantType !== 'F') : ?>
                    <small class="form-text text-muted">The value select impacts the Action option
                        below.</small>
                <?php endif; ?>
            </div>
            <?php if ($currentTenantConfigurations['use_criteria'] !== 'yes') : ?>
                <div class="form-group">
                    <?php echo $this->Form->control('action_id',
                        ['class' => 'form-control', 'placeholder' => 'Action', 'empty' => true]); ?>
                    <small class="form-text text-muted">Select the correct action for a legislator to get points. Only
                        actions with "Evaluation Stance" set as the action type will show up in this option.
                    </small>
                </div>
            <?php endif; ?>
            <div class="form-group">
                <?php echo $this->Form->control('position_id',
                    ['class' => 'form-control', 'placeholder' => 'Position']); ?>
                <small class="form-text text-muted">Select the position taken on this rating.</small>
            </div>
            <?php if ($currentTenantType !== 'F') : ?>
                <div class="form-group">
                    <?php echo $this->Form->control('record_vote',
                        ['class' => 'form-control', 'label' => 'Record Vote/Senate Journal Page', 'type' => 'text']); ?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->control('rating_weight',
                        ['class' => 'form-control', 'placeholder' => 'Rating Weight', 'type' => 'text']); ?>
                    <small class="form-text text-muted">BETA - Used to calculate scores.</small>
                </div>
            <?php endif; ?>
            <div class="form-group">
                <?php echo $this->Form->control('slug',
                    ['class' => 'form-control', 'placeholder' => 'Year-Chamber-Rating Type-Unique Identifier']); ?>
                <small class="form-text text-muted">The friendly URL. It should contain no spaces and use dashes
                    instead.</small>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('sort_order', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <div class="small">Set to the desired position, and the other ratings will be automatically
                    re-numbered when a conflict occurs.
                </div>
            </div>
            <div class="form-check">
                <?php echo $this->Form->control('is_enabled', ['label' => 'Enabled', 'class' => 'form-check-input']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->submit(__('Submit'), ['class' => 'btn btn-primary']); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $('#description').summernote({
            height: "250px"
        });

        <?php if($currentTenantType === 'F') : ?>
        $('#fiscal-note').summernote({
            height: "250px"
        });

        $('#digest').summernote({
            height: "250px"
        });

        $('#vote-recommendation-notes').summernote({
            height: "250px"
        });

        $('#vote-references').summernote({
            height: "250px"
        });
        <?php endif; ?>

        //
        // Action - initial load logic
        //
        var actionSelect = $("#action-id");
        var rating_type_name = $("#rating-type-id option:selected").text();

        // check if selected
        if (actionSelect.val() !== '') {

            // disable all option groups
            actionSelect.hideOptionGroup();

            // show new correct group
            var optionGroup = actionSelect.find("optgroup[label='" + rating_type_name + "']");

            optionGroup.prop("disabled", false);

            // enable action select
            actionSelect.prop("disabled", false).show();
        }

        $("#rating-type-id").on("change", function () {
            // get current label text
            var actionSelect = $("#action-id");
            var rating_type_name = $("#rating-type-id option:selected").text();

            if (rating_type_name === '') {
                // unset current action
                actionSelect.find('option:selected').remove();

                // add blank action
                actionSelect.find('option[value=""]').remove();
                actionSelect.prepend("<option value='' selected='selected'></option>").prop("disabled", false);

                // disable action
                actionSelect.prop("disabled", true);

            } else {
                // unset current action
                actionSelect.find('option:selected').remove();

                // disable all option groups
                actionSelect.hideOptionGroup();

                // add blank action
                actionSelect.find('option[value=""]').remove();
                actionSelect.prepend("<option value='' selected='selected'></option>").prop("disabled", false);

                // show new correct group
                var optionGroup = actionSelect.find("optgroup[label='" + rating_type_name + "']");

                optionGroup.prop("disabled", false);

                // enable action select
                actionSelect.prop("disabled", false).show();
            }
        });
    });

    $.fn.hideOptionGroup = function () {
        $(this).hide();
        $(this).children().each(function () {
            $(this).attr("disabled", "disabled").removeAttr("selected");
        });
        $(this).appendTo($(this).parent());

    }
</script>
