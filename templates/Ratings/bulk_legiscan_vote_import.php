<div class="ratings form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= __('Bulk LegiScan Vote Import'); ?></h1>
            </div>
            <div class="alert alert-danger">
                This utility deletes and re-imports the latest legislator votes for the selected Legislative Session and
                all configured ratings.
                <br/><br/>
                <strong>Any manually added overrides would be reset.</strong>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $this->Form->create(null, [
                'url' => ['action' => 'bulkLegiscanVoteImport'],
                'type' => 'post',
                'id' => 'bulkImportForm'
            ]) ?>
            <div class="form-group">
                <?= $this->Form->control('session', [
                    'label' => 'Legislative Session',
                    'class' => 'custom-select',
                    'placeholder' => 'Legislative Session Id',
                    'default' => $selectedFilters['session']
                ]) ?>
            </div>
            <div class="form-group">
                <?= $this->Form->button(__('Bulk Import Votes'), [
                    'class' => 'btn btn-danger',
                    'onclick' => 'return confirmBulkImport()'
                ]) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    function confirmBulkImport() {
        return confirm('Are you sure you want to bulk import votes? This will delete any existing legislator votes for the selected session.');
    }
</script>
