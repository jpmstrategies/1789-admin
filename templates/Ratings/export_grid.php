<div class="ratings form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Vote Grid Export'); ?></h1>
            </div>
            <div class="alert alert-info">
                This generates an Excel spreadsheet for the selected Legislative Session and Vote Display with a tab for
                each chamber.
                <hr/>
                "Actual Legislator Votes" shows the raw voting data, while "Evaluated Legislator Votes" evaluates each
                vote against your organization's position.
            </div>
        </div>
    </div>
    <div class="row">
        <!-- end col md 3 -->
        <div class="col-md-4">
            <?php echo $this->Form->hidden('t', ['id' => 't', 'value' => md5("1789")
            ]); ?>
            <div class="form-group">
                <?php echo $this->Form->control('session',
                    [
                        'label' => 'Legislative Session',
                        'class' => 'custom-select',
                        'placeholder' => 'Legislative Session Id',
                        'default' => $selectedFilters['session']]); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('vote-display', [
                    'name' => 'vote-display',
                    'class' => 'custom-select',
                    'options' => [
                        'evaluated' => 'Evaluated Legislator Votes',
                        'actual' => 'Actual Legislator Votes'
                    ],
                    'default' => 'evaluated']); ?>
            </div>
            <div class="form-group">
                <a onclick="getDownloadUrl()" class="btn btn-success">
                    <span class="octicon octicon-desktop-download"></span>&nbsp; &nbsp; Vote Grid
                    Export
                </a>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<script type="text/javascript">
    function getDownloadUrl() {
        let url = '<?php echo $downloadUrl; ?>';
        url += '?sessionId=' + jQuery('#session').val();
        url += '&voteDisplay=' + jQuery('#vote-display').val();
        url += '&t=' + jQuery('#t').val();

        window.open(url, '_blank');
    }
</script>
