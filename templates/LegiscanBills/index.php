<?php

use Cake\Routing\Router;

$this->Html->script('View/bulkedit.js?v=20220816', ['block' => true]);
$this->Html->css('bulkedit.css', ['block' => true]);
?>
<div class="ratings view">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-header">
                <h1><?php echo __('LegiScan Bill Search'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <?php echo $this->Form->create(null, ['class' => 'form-horizontal', 'id' => 'FilterForm']); ?>
            <input type="hidden" name="_csrfToken" value="<?php echo $_COOKIE['csrfToken'] ?? null; ?>"/>
            <input type="hidden" name="url"
                   value="<?php echo Router::url(['controller' => 'LegiscanBills', 'action' => 'index'], true); ?>">
            <input type="hidden" name="saveUrl"
                   value="<?php echo Router::url([
                       'controller' => 'LegiscanBills',
                       'action' => 'bulk-update-row'], true); ?>">
            <div class="row">
                <div class="form-group form-group-sm col-sm-12">
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <?php echo $this->Form->control('session',
                                [
                                    'id' => 'session',
                                    'name' => 'session',
                                    'label' => false,
                                    'class' => 'custom-select',
                                    'placeholder' => 'Legislative Session Id',
                                    'default' => $selectedFilters['session'],
                                    'options' => $sessions]); ?>
                        </div>
                        <div class="col-sm-6">
                            <?php echo $this->Form->control('chamber',
                                [
                                    'id' => 'chamber',
                                    'name' => 'chamber',
                                    'label' => false,
                                    'class' => 'custom-select',
                                    'default' => $selectedFilters['chamber'],
                                    'empty' => '- All Chambers -',
                                    'options' => $chambers]); ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <?php echo $this->Form->control('status',
                                [
                                    'id' => 'status',
                                    'name' => 'status',
                                    'label' => false,
                                    'class' => 'custom-select',
                                    'empty' => '- All Bill Statuses -',
                                    'default' => $selectedFilters['status'],
                                    'options' => $statuses]); ?>
                        </div>
                        <div class="col-sm-6">
                            <?php echo $this->Form->control('type',
                                [
                                    'id' => 'type',
                                    'name' => 'type',
                                    'label' => false,
                                    'class' => 'custom-select',
                                    'default' => $selectedFilters['type'],
                                    'empty' => '- All Bill Types -',
                                    'options' => $billTypes]); ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <?php
                            echo $this->Form->content('bill', [
                                'id' => 'bill',
                                'name' => 'bill',
                                'label' => false,
                                'class' => 'form-control',
                                'placeholder' => 'Filter Bill Number',
                                'default' => $selectedFilters['bill']
                            ])
                            ?>
                        </div>
                        <div class="col-sm-6">
                            <?php
                            echo $this->Form->content('title', [
                                'id' => 'title',
                                'name' => 'title',
                                'label' => false,
                                'class' => 'form-control',
                                'placeholder' => 'Filter Title by Keyword',
                                'default' => $selectedFilters['title']
                            ])
                            ?>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-6">
                            <?php
                            echo $this->Form->content('subject', [
                                'id' => 'subject',
                                'name' => 'subject',
                                'label' => false,
                                'class' => 'form-control',
                                'placeholder' => 'Filter Subject by Keyword',
                                'default' => $selectedFilters['subject']
                            ])
                            ?>
                        </div>
                        <div class="col-sm-6">
                            <?php echo $this->Form->control('priority', [
                                'id' => 'priority',
                                'name' => 'priority',
                                'label' => false,
                                'class' => 'custom-select',
                                'empty' => '- Filter by Priority -',
                                'options' => $priorities->toArray(),
                                'default' => $selectedFilters['priority']]); ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <?php
                            echo $this->BillTrackingStatus->control('tracking', [
                                'name' => 'tracking',
                                'label' => false,
                                'class' => 'custom-select',
                                'default' => $selectedFilters['tracking'],
                                'empty' => '- Filter by Tracking -',
                                'templates' => [
                                    'inputContainer' => '<div class="input {{type}}{{required}}">{{content}}</div>'
                                ]]);
                            ?>
                        </div>
                        <div class="col-sm-6">
                            <?php

                            $scoredFilterText = 'Scored';
                            if($currentTenantType === 'F') {
                                $scoredFilterText = 'Tracked';
                            }

                            echo $this->Enable->control('scored', [
                                'name' => 'scored',
                                'label' => false,
                                'class' => 'custom-select',
                                'default' => $selectedFilters['scored'],
                                'empty' => '- Filter by ' . $scoredFilterText . ' -',
                                'templates' => [
                                    'inputContainer' => '<div class="input {{type}}{{required}}">{{content}}</div>'
                                ]]);
                            ?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <input type="submit" value="Filter Bills" class="btn btn-primary" id="filter-submit-btn"/>
                            <input type="submit" value="Reset" class="btn btn-danger" id="filter-reset-btn"/>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
    <?php if (sizeof($priorities->toArray()) === 0) : ?>
        <div class="row">
            <div class="col-xl-12">
                <div class="alert alert-warning" role="alert">
                    To link a bill with a specific Priority, please configure some by clicking
                    <?php echo $this->Html->link(__('here'),
                        ['controller' => 'Priorities', 'action' => 'index']); ?>.
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="related row">
    <div class="col-xl-12">
        <?php echo $this->element('LegiscanBills/index_details', [
            'selectedFilters' => $selectedFilters
        ]); ?>
    </div>
    <!-- end col md 12 -->
</div>

<script type="text/javascript">
    function serializeFormJson(fields) {
        let o = {};
        let a = fields.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }

    function objectToQueryString(obj) {
        let str = [];
        for (let p in obj) {
            if (obj.hasOwnProperty(p) && obj[p] !== '' && obj[p] != null) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        }

        if (str.length > 0) {
            return '?' + str.join("&");
        }
    }

    jQuery(document).ready(function ($) {
        const filterForm = $('#FilterForm');

        jQuery('input#filter-submit-btn').on("click", function (e) {
            e.preventDefault();

            const formData = serializeFormJson(filterForm.find(':input'));
            delete formData.saveUrl
            delete formData.url
            delete formData._csrfToken

            let qs = objectToQueryString(formData)
            if (qs && qs.length > 0) {
                let newUrl = filterForm.find('[name="url"]').val() + qs
                window.location.replace(newUrl);
            }
        });

        jQuery('input#filter-reset-btn').on("click", function (e) {
            e.preventDefault();
            jQuery('select#chamber').val(jQuery('input#chamber option:first').val())
            jQuery('select#status').val(jQuery('input#status option:first').val());
            jQuery('select#type').val(jQuery('input#type option:first').val());
            jQuery('input#bill').val('')
            jQuery('input#title').val('')
            jQuery('input#subject').val('')
            jQuery('select#priority').val(jQuery('input#priority option:first').val());
            jQuery('select#tracking').val(jQuery('input#tracking option:first').val());
            jQuery('select#scored').val(jQuery('input#scored option:first').val());

            // remove URL query strings
            let uri = window.location.href.toString();
            if (uri.indexOf("?") > 0) {
                let clean_uri = uri.substring(0, uri.indexOf("?"));
                window.history.replaceState({}, document.title, clean_uri);
            }
            window.location.reload(true)
        });
    });
</script>
