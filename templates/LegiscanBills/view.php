<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanBill $legiscanBill
 */

$billChamber = ($selectedFilters['bill_chamber'] === 'L' ? 'House' : 'Senate');
?>
<div class="ratings view">
    <?php if (isset($selectedFilters['bill_chamber'])) : ?>
        <div class="row">
            <div class="col-xl-12">
                <div class="alert alert-warning" role="alert">
                    You are searching for votes from the Bill Tracker. We have filter votes by the selected chamber:
                    <strong>
                        <?php
                        echo $billChamber;
                        ?>
                    </strong>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-xl-12">
            <div class="page-header">
                <h1>Record Votes for <?php echo $this->LegiscanBill->getBillName($bill['bill_number']) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <p>
                <?php
                echo $bill['title'];
                ?>
            </p>
        </div>
    </div>

    <hr/>

    <!-- start related Legiscan Bill Votes -->
    <div class="Legiscan Bill Votes related row">
        <div class="col-lg-12">
            <h3><?= __('Legiscan Bill Votes') ?></h3>
            <?php if (!empty($legiscanBillVotes)): ?>
                <div class="alert alert-primary" role="alert">
                    Click an individual row to see the vote breakdown (if provided by LegiScan).
                </div>
                <table class="table">
                    <tr>
                        <th scope="col"><?= __('Date') ?></th>
                        <th scope="col"><?= __('Roll Call Description') ?></th>
                        <th scope="col"><?= __('Chamber') ?></th>
                        <th scope="col"><?= __('Yea') ?></th>
                        <th scope="col"><?= __('Nay') ?></th>
                        <th scope="col"><?= __('Nv') ?></th>
                        <th scope="col"><?= __('Absent') ?></th>
                        <th scope="col"><?= __('Total') ?></th>
                        <th scope="col"><?= __('Passed') ?></th>
                        <th scope="col"><?= __('External Links') ?></th>
                        <?php if($currentTenantType === 'C') : ?>
                            <th scope="col"><?= __('Actions') ?></th>
                        <?php endif; ?>
                    </tr>
                    <?php
                    $row = 0;
                    foreach ($legiscanBillVotes as $legiscanBillVote):
                        ?>
                        <tr data-toggle="collapse" data-target=".row<?php echo $row; ?>"
                            <?php if ($legiscanBillVote->has_bill_vote_details > 0) echo 'style="cursor: pointer"'; ?>
                        >
                            <td><?= h($legiscanBillVote->roll_call_date) ?></td>
                            <td><?= h($legiscanBillVote->roll_call_desc) ?></td>
                            <td><?= h($legiscanBillVote->roll_call_body_short) ?></td>
                            <td><?= h($legiscanBillVote->yea) ?></td>
                            <td><?= h($legiscanBillVote->nay) ?></td>
                            <td><?= h($legiscanBillVote->nv) ?></td>
                            <td><?= h($legiscanBillVote->absent) ?></td>
                            <td><?= h($legiscanBillVote->total) ?></td>
                            <td><?= $this->Enable->display_text($legiscanBillVote->passed) ?></td>
                            <td>
                                <a href="<?php echo $legiscanBillVote->legiscan_url; ?>" target="_blank">
                                    <span class="octicon octicon-link-external"></span> LegiScan
                                </a>
                                <br/>
                                <a href="<?php echo $legiscanBillVote->state_url; ?>" target="_blank">
                                    <span class="octicon octicon-link-external"></span> State
                                </a>
                            </td>
                            <?php if($currentTenantType === 'C') : ?>
                                <td>
                                    <?php if (isset($legiscanBillVote['ratings'][0])) : ?>
                                        <?php echo $this->Html->link(__('<span class="octicon octicon-search"></span> View Rating'),
                                            [
                                                'controller' => 'ratings',
                                                'action' => 'view',
                                                $legiscanBillVote['ratings'][0]['id']],
                                            ['escape' => false, 'class' => 'btn btn-info score-vote', 'target' => '_blank']);
                                        ?>
                                        <?php if (isset($selectedFilters['bill_chamber'])) : ?>
                                            <br/>
                                            <br/>
                                            <?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span> Link Bill'),
                                                [
                                                    'controller' => 'bills',
                                                    'action' => 'link-rating',
                                                    '?' =>
                                                        ['rating_id' => $legiscanBillVote['ratings'][0]['id'],
                                                            'bill_chamber' => $selectedFilters['bill_chamber'] ?? null,
                                                            'bill_uuid' => $selectedFilters['bill_uuid'] ?? null,
                                                            'legiscan_vote_id' => $legiscanBillVote->id
                                                        ],
                                                ],
                                                ['escape' => false, 'class' => 'btn btn-success score-vote']);
                                            ?>
                                        <?php endif; ?>

                                    <?php else : ?>
                                        <?php if ($legiscanBillVote->has_bill_vote_details > 0) : ?>
                                            <?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span> Score Vote'),
                                                [
                                                    'controller' => 'ratings',
                                                    'action' => 'add',
                                                    '?' =>
                                                        ['legiscan_bill_vote_id' => $legiscanBillVote->id,
                                                            'bill_chamber' => $selectedFilters['bill_chamber'] ?? null,
                                                            'bill_uuid' => $selectedFilters['bill_uuid'] ?? null
                                                        ]
                                                ],
                                                ['escape' => false, 'class' => 'btn btn-success score-vote', 'target' => '_blank']);
                                            ?>
                                        <?php else : ?>
                                            <button type="button" class="btn btn-secondary" disabled>
                                                <span class="octicon octicon-plus"></span> Score Vote
                                            </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            <?php endif; ?>
                        </tr>
                        <?php
                        if (isset($legiscanVoteGroupsByParty[$legiscanBillVote->id])) {
                            ?>
                            <tr class="collapse row<?php echo $row; ?>">
                                <td colspan="11">
                                    <h3>Breakdown by Party</h3>
                                    <table class="table table-striped table-sm" style="width: 600px;">
                                        <tr>
                                            <th scope="col"><?= __('Party Name') ?></th>
                                            <th scope="col"><?= __('Yea') ?></th>
                                            <th scope="col"><?= __('Nay') ?></th>
                                            <th scope="col"><?= __('Not Voting') ?></th>
                                            <th scope="col"><?= __('Absent') ?></th>
                                        </tr>
                                        <?php foreach ($legiscanVoteGroupsByParty[$legiscanBillVote->id] as $group_name => $breakdown): ?>
                                            <tr>
                                                <td><?= h($group_name) ?></td>
                                                <td><?= h($breakdown['Yea']) ?></td>
                                                <td><?= h($breakdown['Nay']) ?></td>
                                                <td><?= h($breakdown['Not Voting']) ?></td>
                                                <td><?= h($breakdown['Absent']) ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        <?php
                        if (isset($legiscanVoteGroupsByTag[$legiscanBillVote->id])) {
                            ?>
                            <tr class="collapse row<?php echo $row; ?>">
                                <td colspan="11">
                                    <h3>Breakdown by Tag</h3>
                                    <table class="table table-striped table-sm" style="width: 600px;">
                                        <tr>
                                            <th scope="col"><?= __('Tag Name') ?></th>
                                            <th scope="col"><?= __('Yea') ?></th>
                                            <th scope="col"><?= __('Nay') ?></th>
                                            <th scope="col"><?= __('Not Voting') ?></th>
                                            <th scope="col"><?= __('Absent') ?></th>
                                        </tr>
                                        <?php foreach ($legiscanVoteGroupsByTag[$legiscanBillVote->id] as $group_name => $breakdown): ?>
                                            <tr>
                                                <td><?= h($group_name) ?></td>
                                                <td><?= h($breakdown['Yea']) ?></td>
                                                <td><?= h($breakdown['Nay']) ?></td>
                                                <td><?= h($breakdown['Not Voting']) ?></td>
                                                <td><?= h($breakdown['Absent']) ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        <?php
                        $row++;
                    endforeach;
                    ?>
                </table>
            <?php else : ?>
                <div class="alert alert-primary" role="alert">
                    No LegiScan Bill Votes to display!
                </div>
            <?php endif; ?>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end related Legiscan Bill Votes -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        // prevent row from expanding when clicking score vote button
        jQuery('.score-vote').click(function (e) {
                e.stopPropagation()
            }
        )
    })
</script>
