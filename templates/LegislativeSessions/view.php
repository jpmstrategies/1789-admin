<div class="legislativeSessions view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Legislative Session'); ?></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp&nbsp;Edit Legislative Session'),
                                    ['action' => 'edit', $legislativeSession['id']],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete Legislative Session'),
                                    ['action' => 'delete', $legislativeSession['id']],
                                    [
                                        'escape' => false,
                                        'class' => 'nav-link'], __('Are you sure you want to delete # {0}?',
                                        $legislativeSession['id'])); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Legislative Sessions'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Legislative Session'),
                                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Legislator'),
                                    ['controller' => 'Legislators', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <?php
                            if ($currentTenantDataSource === 'legiscan') {
                                echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating'),
                                    ['controller' => 'LegiscanBills', 'action' => 'index'], ['escape' => false, 'class' => 'nav-link']);
                            } else {
                                echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating'),
                                    ['controller' => 'Ratings', 'action' => 'add'], ['escape' => false, 'class' => 'nav-link']);
                            }
                            ?>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->

        <div class="col-md-9">
            <table class="table table-striped table-sm">
                <tbody>
                <tr>
                    <th><?php echo __('Id'); ?></th>
                    <td>
                        <?php echo h($legislativeSession['id']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Name'); ?></th>
                    <td>
                        <?php echo h($legislativeSession['name']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Year'); ?></th>
                    <td>
                        <?php echo h($legislativeSession['year']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Legislature Number'); ?></th>
                    <td>
                        <?php echo h($legislativeSession['legislature_number']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Session Type'); ?></th>
                    <td>
                        <?php echo $this->SessionType->display_text($legislativeSession['session_type']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Special Session Number'); ?></th>
                    <td>
                        <?php echo h($legislativeSession['special_session_number']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Speaker Journal Name'); ?></th>
                    <td>
                        <?php echo h($legislativeSession['speaker_journal_name']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Lt Gov Journal Name'); ?></th>
                    <td>
                        <?php echo h($legislativeSession['lt_gov_journal_name']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Slug'); ?></th>
                    <td>
                        <?php echo h($legislativeSession['slug']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('State Unique ID'); ?></th>
                    <td>
                        <?php echo h($legislativeSession['state_pk']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Open States ID'); ?></th>
                    <td>
                        <?php echo h($legislativeSession['os_pk']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('LegiScan ID'); ?></th>
                    <td>
                        <?php echo h($legislativeSession['legiscan_session_id']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td>
                        <?php echo h($legislativeSession['created']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td>
                        <?php echo h($legislativeSession['modified']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Enabled'); ?></th>
                    <td>
                        <?php echo $this->Enable->display_text($legislativeSession['is_enabled']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Scores Public'); ?></th>
                    <td>
                        <?php echo $this->Enable->display_text($legislativeSession['show_scores']); ?>
                        &nbsp;
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
        <!-- end col md 9 -->

    </div>
</div>
