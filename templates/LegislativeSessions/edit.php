<div class="legislativeSessions form">

    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Edit Legislative Session'); ?></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">

                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete'),
                                    ['action' => 'delete', $legislativeSession->id],
                                    [
                                        'escape' => false,
                                        'class' => 'nav-link'], __('Are you sure you want to delete # {0}?',
                                        $legislativeSession->id)); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Legislative Sessions'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($legislativeSession); ?>

            <div class="form-group">
                <?php echo $this->Form->control('id', ['class' => 'form-control', 'placeholder' => 'Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('name', ['class' => 'form-control', 'placeholder' => 'Name']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('year', ['class' => 'form-control', 'placeholder' => 'YYYY']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('legislature_number',
                    ['class' => 'form-control', 'placeholder' => 'Legislature Number', 'type' => 'text']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->SessionType->control('session_type',
                    ['class' => 'form-control', 'placeholder' => 'Session Type']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('special_session_number',
                    [
                        'class' => 'form-control',
                        'placeholder' => 'Special Session Number',
                        'type' => 'text',
                        'default' => '0']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('speaker_journal_name',
                    ['class' => 'form-control', 'placeholder' => 'Speaker Journal Name']); ?>
                <small class="form-text text-muted">BETA - The text as it is used for house journal vote
                    statements.</small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('lt_gov_journal_name',
                    ['class' => 'form-control', 'placeholder' => 'Lt Gov Journal Name']); ?>
                <small class="form-text text-muted">BETA - The text as it is used for house journal vote
                    statements.</small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('slug',
                    ['class' => 'form-control', 'placeholder' => 'YYYY-index']); ?>
                <small class="form-text text-muted">The friendly URL. It should contain no spaces and use dashes
                    instead.</small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('state_pk', [
                    'class' => 'form-control',
                    'placeholder' => 'State Unique ID',
                    'label' => 'State Unique ID']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('os_pk', [
                    'class' => 'form-control',
                    'placeholder' => 'Open States ID',
                    'label' => 'Open States ID',
                    'type' => 'text']); ?>
            </div>
            <div class="form-check">
                <?php echo $this->Form->control('is_enabled', ['label' => 'Enabled', 'class' => 'form-check-input']); ?>
            </div>
            <div class="form-check">
                <?php echo $this->Form->control('show_scores', [
                    'label' => 'Show Scores Publicly',
                    'class' => 'form-check-input']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->submit(__('Submit'), ['class' => 'btn btn-primary']); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
