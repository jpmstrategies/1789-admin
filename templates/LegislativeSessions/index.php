<div class="legislativeSessions index">

    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Legislative Sessions'); ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legislative Session'),
                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?></li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('name'); ?></th>
                    <th><?php echo $this->Paginator->sort('legislature_number'); ?></th>
                    <th><?php echo $this->Paginator->sort('session_type'); ?></th>
                    <th><?php echo $this->Paginator->sort('special_session_number'); ?></th>
                    <th><?php echo $this->Paginator->sort('is_enabled', 'Enabled'); ?></th>
                    <th><?php echo $this->Paginator->sort('show_scores', 'Scores Public'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($legislativeSessions as $legislativeSession): ?>
                    <tr>
                        <td><?php echo h($legislativeSession['name']); ?>&nbsp;</td>
                        <td><?php echo h($legislativeSession['legislature_number']); ?>&nbsp;</td>
                        <td><?php echo $this->SessionType->display_text($legislativeSession['session_type']); ?>&nbsp;
                        </td>
                        <td><?php echo h($legislativeSession['special_session_number']); ?>&nbsp;</td>
                        <td><?php echo $this->Enable->display_text($legislativeSession['is_enabled']); ?>&nbsp;</td>
                        <td><?php echo $this->Enable->display_text($legislativeSession['show_scores']); ?>&nbsp;</td>
                        <td class="actions">
                            <?php echo $this->Html->link('<span class="octicon octicon-search"></span>',
                                ['action' => 'view', $legislativeSession['id']],
                                ['escape' => false]); ?>
                            <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                                ['action' => 'edit', $legislativeSession['id']],
                                ['escape' => false]); ?>
                            <?php echo $this->Form->postLink('<span class="octicon octicon-trashcan"></span>',
                                ['action' => 'delete', $legislativeSession['id']],
                                ['escape' => false], __('Are you sure you want to delete # {0}?',
                                    $legislativeSession['id'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->

</div><!-- end containing of content -->
