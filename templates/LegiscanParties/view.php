<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanParty $legiscanParty
 */
?>
<div class="legiscanParties view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($legiscanParty->party_name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>
                                Edit Legiscan Party')
                                    , ['action' => 'edit', $legiscanParty->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>
                                List Legiscan Parties')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>
                                New Legiscan Party')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('State') ?></th>
                    <td><?= h($legiscanParty->state) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Party Abbr') ?></th>
                    <td><?= h($legiscanParty->party_abbr) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Party Short') ?></th>
                    <td><?= h($legiscanParty->party_short) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Party Name') ?></th>
                    <td><?= h($legiscanParty->party_name) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>

<!-- start related Legiscan Bill Vote Details -->
<div class="Legiscan Bill Vote Details related row">
    <div class="col-lg-12">
        <h3><?= __('Related Legiscan Bill Vote Details') ?></h3>
        <?php if (!empty($legiscanParty->legiscan_bill_vote_details)): ?>
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="col"><?= __('Bill Number') ?></th>
                    <th scope="col"><?= __('Vote Desc') ?></th>
                    <th scope="col"><?= __('Party Abbr') ?></th>
                    <th scope="col"><?= __('Role Abbr') ?></th>
                    <th scope="col"><?= __('Role Name') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('First Name') ?></th>
                    <th scope="col"><?= __('Last Name') ?></th>
                    <th scope="col"><?= __('Suffix') ?></th>
                    <th scope="col"><?= __('Nickname') ?></th>
                    <th scope="col"><?= __('Ballotpedia') ?></th>
                    <th scope="col"><?= __('Person Hash') ?></th>
                    <th scope="col"><?= __('Person District') ?></th>
                    <th scope="col"><?= __('State Name') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($legiscanParty->legiscan_bill_vote_details as $legiscanBillVoteDetails): ?>
                    <tr>
                        <td><?= h($legiscanBillVoteDetails->bill_number) ?></td>
                        <td><?= h($legiscanBillVoteDetails->vote_desc) ?></td>
                        <td><?= h($legiscanBillVoteDetails->party_abbr) ?></td>
                        <td><?= h($legiscanBillVoteDetails->role_abbr) ?></td>
                        <td><?= h($legiscanBillVoteDetails->role_name) ?></td>
                        <td><?= h($legiscanBillVoteDetails->name) ?></td>
                        <td><?= h($legiscanBillVoteDetails->first_name) ?></td>
                        <td><?= h($legiscanBillVoteDetails->last_name) ?></td>
                        <td><?= h($legiscanBillVoteDetails->suffix) ?></td>
                        <td><?= h($legiscanBillVoteDetails->nickname) ?></td>
                        <td><?= h($legiscanBillVoteDetails->ballotpedia) ?></td>
                        <td><?= h($legiscanBillVoteDetails->person_hash) ?></td>
                        <td><?= h($legiscanBillVoteDetails->person_district) ?></td>
                        <td><?= h($legiscanBillVoteDetails->state_name) ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , [
                                    'controller' => 'LegiscanBillVoteDetails',
                                    'action' => 'view',
                                    $legiscanBillVoteDetails->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , [
                                    'controller' => 'LegiscanBillVoteDetails',
                                    'action' => 'edit',
                                    $legiscanBillVoteDetails->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?php // External ID exists and when populated, hide delete icon ?>
                            <?php if (!$legiscanBillVoteDetails->followthemoney_eid) : ?>
                                <?=
                                $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                    , [
                                        'controller' => 'LegiscanBillVoteDetails',
                                        'action' => 'delete',
                                        $legiscanBillVoteDetails->id]
                                    , [
                                        'escape' => false,
                                        'confirm' => __('Are you sure you want to delete #
                            {0}?', $legiscanBillVoteDetails->id)]
                                )
                                ?>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else : ?>
            <div class="alert alert-primary" role="alert">
                No Legiscan Bill Vote Details to display!
            </div>
        <?php endif; ?>
    </div>
    <!-- end col lg 12 -->
</div>
<!-- end related Legiscan Bill Vote Details -->
<!-- start related Parties -->
<div class="Parties related row">
    <div class="col-lg-12">
        <h3><?= __('Related Parties') ?></h3>
        <?php if (!empty($legiscanParty->parties)): ?>
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Abbreviation') ?></th>
                    <th scope="col"><?= __('Css Color') ?></th>
                    <th scope="col"><?= __('Is Enabled') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($legiscanParty->parties as $parties): ?>
                    <tr>
                        <td><?= h($parties->name) ?></td>
                        <td><?= h($parties->abbreviation) ?></td>
                        <td><?= h($parties->css_color) ?></td>
                        <td><?= h($parties->is_enabled) ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['controller' => 'Parties', 'action' => 'view', $parties->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['controller' => 'Parties', 'action' => 'edit', $parties->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['controller' => 'Parties', 'action' => 'delete', $parties->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete #
                            {0}?', $parties->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else : ?>
            <div class="alert alert-primary" role="alert">
                No Parties to display!
            </div>
        <?php endif; ?>
    </div>
    <!-- end col lg 12 -->
</div>
<!-- end related Parties -->
