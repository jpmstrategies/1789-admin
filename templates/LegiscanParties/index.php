<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanParty[]|\Cake\Collection\CollectionInterface $legiscanParties
 */
?>
<div class="legiscanParties index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Legiscan Parties') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Party')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Vote Detail
                                ')
                    , ['controller' => 'LegiscanBillVoteDetails', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Party
                                ')
                    , ['controller' => 'Parties', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('party_abbr') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('party_short') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('party_name') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($legiscanParties as $legiscanParty): ?>
                    <tr>
                        <td>
                            <?= h($legiscanParty->party_abbr) ?>
                        </td>
                        <td>
                            <?= h($legiscanParty->party_short) ?>
                        </td>
                        <td>
                            <?= h($legiscanParty->party_name) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $legiscanParty->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $legiscanParty->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $legiscanParty->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete # {0}?', $legiscanParty->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
