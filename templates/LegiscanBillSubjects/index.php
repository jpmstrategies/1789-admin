<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanBillSubject[]|\Cake\Collection\CollectionInterface $legiscanBillSubjects
 */
?>
<div class="legiscanBillSubjects index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Legiscan Bill Subjects') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Subject')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Subject
                                ')
                    , ['controller' => 'LegiscanSubjects', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill
                                ')
                    , ['controller' => 'LegiscanBills', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Session
                                ')
                    , ['controller' => 'LegiscanSessions', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Chamber
                                ')
                    , ['controller' => 'LegiscanChambers', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_subject_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_bill_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('bill_number') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('subject_name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_state_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('state_name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_session_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_chamber_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legiscan_current_chamber_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legsican_bill_type_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legsican_status_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('legsican_pending_committee_id') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($legiscanBillSubjects as $legiscanBillSubject): ?>
                    <tr>
                        <td>
                            <?= $legiscanBillSubject->has('legiscan_subject') ?
                                $this->Html->link($legiscanBillSubject->legiscan_subject
                                    ->subject_name, [
                                    'controller' => 'LegiscanSubjects
                                            ',
                                    'action' => 'view',
                                    $legiscanBillSubject->legiscan_subject
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $legiscanBillSubject->has('legiscan_bill') ?
                                $this->Html->link($legiscanBillSubject->legiscan_bill
                                    ->title, [
                                    'controller' => 'LegiscanBills
                                            ',
                                    'action' => 'view',
                                    $legiscanBillSubject->legiscan_bill
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= h($legiscanBillSubject->bill_number) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillSubject->subject_name) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillSubject->legiscan_state_id) ?>
                        </td>
                        <td>
                            <?= h($legiscanBillSubject->state_name) ?>
                        </td>
                        <td>
                            <?= $legiscanBillSubject->has('legiscan_session') ?
                                $this->Html->link($legiscanBillSubject->legiscan_session
                                    ->session_name, [
                                    'controller' => 'LegiscanSessions
                                            ',
                                    'action' => 'view',
                                    $legiscanBillSubject->legiscan_session
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $legiscanBillSubject->has('origination_chamber') ?
                                $this->Html->link($legiscanBillSubject->origination_chamber
                                    ->body_name, [
                                    'controller' => 'LegiscanChambers
                                            ',
                                    'action' => 'view',
                                    $legiscanBillSubject->origination_chamber
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $legiscanBillSubject->has('current_chamber') ?
                                $this->Html->link($legiscanBillSubject->current_chamber
                                    ->body_name, [
                                    'controller' => 'LegiscanChambers
                                            ',
                                    'action' => 'view',
                                    $legiscanBillSubject->current_chamber
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillSubject->legsican_bill_type_id) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillSubject->legsican_status_id) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanBillSubject->legsican_pending_committee_id) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $legiscanBillSubject->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $legiscanBillSubject->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $legiscanBillSubject->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete # {0}?', $legiscanBillSubject->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
