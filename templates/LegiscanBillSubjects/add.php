<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanBillSubject $legiscanBillSubject
 */
?>
<div class="legiscanBillSubjects form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= __('Add Legiscan Bill Subject') ?></h1>
            </div>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Subject
                                                ')
                                    , ['controller' => 'LegiscanSubjects', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill
                                                ')
                                    , ['controller' => 'LegiscanBills', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Session
                                                ')
                                    , ['controller' => 'LegiscanSessions', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Chamber
                                                ')
                                    , ['controller' => 'LegiscanChambers', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Legiscan Bill Subjects
                                ')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link'])
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?= $this->Form->create($legiscanBillSubject) ?>
            <div class="form-group">
                <?php
                echo $this->Form->control('state', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_subject_id', [
                    'options' => $legiscanSubjects,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_bill_id', [
                    'options' => $legiscanBills,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('bill_number', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('subject_name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_state_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('state_name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_session_id', [
                    'options' => $legiscanSessions,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_chamber_id', [
                    'options' => $legiscanChambers,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legiscan_current_chamber_id', [
                    'options' => $legiscanCurrentChambers,
                    'class' =>
                        'form-control',
                    'placeholder' => '',
                    'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legsican_bill_type_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legsican_status_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legsican_pending_committee_id', [
                    'class' => 'form-control',
                    'placeholder' => '']);
                ?>
            </div>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            <?php echo $this->Html->link(
                'Cancel',
                ['controller' => 'legiscanBillSubjects', 'action' => 'index', '_full' => true],
                ['class' => 'btn btn-secondary', 'id' => 'cancel']
            ); ?>
            <?= $this->Form->end() ?>
        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->