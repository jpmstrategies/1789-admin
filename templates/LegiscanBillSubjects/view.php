<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanBillSubject $legiscanBillSubject
 */
?>
<div class="legiscanBillSubjects view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($legiscanBillSubject->subject_name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>
                                Edit Legiscan Bill Subject')
                                    , ['action' => 'edit', $legiscanBillSubject->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>
                                List Legiscan Bill Subjects')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>
                                New Legiscan Bill Subject')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('State') ?></th>
                    <td><?= h($legiscanBillSubject->state) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Subject') ?></th>
                    <td><?= $legiscanBillSubject->has('legiscan_subject') ?
                            $this->Html->link($legiscanBillSubject->legiscan_subject
                                ->subject_name, [
                                'controller' => 'LegiscanSubjects',
                                'action' => 'view',
                                $legiscanBillSubject->legiscan_subject
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Bill') ?></th>
                    <td><?= $legiscanBillSubject->has('legiscan_bill') ?
                            $this->Html->link($legiscanBillSubject->legiscan_bill
                                ->title, [
                                'controller' => 'LegiscanBills',
                                'action' => 'view',
                                $legiscanBillSubject->legiscan_bill
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Bill Number') ?></th>
                    <td><?= h($legiscanBillSubject->bill_number) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Subject Name') ?></th>
                    <td><?= h($legiscanBillSubject->subject_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('State Name') ?></th>
                    <td><?= h($legiscanBillSubject->state_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan Session') ?></th>
                    <td><?= $legiscanBillSubject->has('legiscan_session') ?
                            $this->Html->link($legiscanBillSubject->legiscan_session
                                ->session_name, [
                                'controller' => 'LegiscanSessions',
                                'action' => 'view',
                                $legiscanBillSubject->legiscan_session
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Origination Chamber') ?></th>
                    <td><?= $legiscanBillSubject->has('origination_chamber') ?
                            $this->Html->link($legiscanBillSubject->origination_chamber
                                ->body_name, [
                                'controller' => 'LegiscanChambers',
                                'action' => 'view',
                                $legiscanBillSubject->origination_chamber
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Current Chamber') ?></th>
                    <td><?= $legiscanBillSubject->has('current_chamber') ?
                            $this->Html->link($legiscanBillSubject->current_chamber
                                ->body_name, [
                                'controller' => 'LegiscanChambers',
                                'action' => 'view',
                                $legiscanBillSubject->current_chamber
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan State Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillSubject->legiscan_state_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legsican Bill Type Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillSubject->legsican_bill_type_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legsican Status Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillSubject->legsican_status_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legsican Pending Committee Id') ?></th>
                    <td><?= $this->Number->format($legiscanBillSubject->legsican_pending_committee_id) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>

