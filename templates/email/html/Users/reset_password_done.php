<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    The password for the account associated with this email address, <?php echo $user->email; ?>, has been changed.
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    Your password was changed by a user at the following IP address: <?php echo $ip; ?>
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    If you did not make this change, please contact us immediately!
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    Thank you for using Cornerstone.
</p>
<?php echo $this->element('email/html_footer'); ?>