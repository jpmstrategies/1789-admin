<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    You have just created an account with Cornerstone. For your records, your username is <?php echo $user->email; ?>.
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    Here are some helpful links to get you through activating your account:
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
<ul>
    <li>
        Sample Link 1
    </li>
    <li>
        Sample Link 2
    </li>
    <li>
        Sample Link 3
    </li>
</ul>
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    Welcome to Cornerstone,
</p>
<?php echo $this->element('email/html_footer'); ?>