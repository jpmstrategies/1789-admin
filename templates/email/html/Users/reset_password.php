<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    You've asked to reset your password for the Cornerstone account associated with this email address
    (<?php echo $email; ?>). To get the password reset code, please click on the following link:
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    <a href="<?php echo $url; ?>" target="_blank"><?php echo $url; ?></a>
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    You can also copy and paste the link above into a new browser window.
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    Or enter the reset code directly into the password page:
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    <?php echo $security_code; ?>
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    If you didn't make the request, you can ignore this email and do nothing. Another user likely entered your email
    address by mistake while trying to reset a password.
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    Replies to this email are not monitored or answered.
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    Thank you for using Cornerstone.
</p>
<?php echo $this->element('email/html_footer'); ?>