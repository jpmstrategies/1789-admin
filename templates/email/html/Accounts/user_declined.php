<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    We were unable to verify the information you provided. If you have any questions, contact <a
            href="mailto: admin@jpmsrv.com">admin@jpmsrv.com</a>.
</p>
<?php echo $this->element('email/html_footer'); ?>