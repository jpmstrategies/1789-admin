<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    Hi,
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    <?php echo($user->is_super ? 'An Administrator' : $user->email); ?> has invited you to join and
    manage <b><?php echo $tenant->name; ?></b> on Cornerstone.
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    To accept click on the following link:
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    <a href="<?php echo $url; ?>" target="_blank"><?php echo $url; ?></a>
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    Thanks,
</p>
<?php echo $this->element('email/html_footer'); ?>