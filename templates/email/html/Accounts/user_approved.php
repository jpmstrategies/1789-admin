<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    Your request has been approved. Your JPM account is now active.
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    To accept click on the following link:
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    <a href="<?php echo $url; ?>" target="_blank"><?php echo $url; ?></a>
</p>
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
    If you have questions about your account or JPM features, contact <a href="mailto: admin@jpmsrv.com">
        admin@jpmsrv.com</a>.
</p>
<?php echo $this->element('email/html_footer'); ?>