Your request has been approved. Your JPM account is now active.

To accept click on the following link:

<?php echo $url; ?>


If you have questions about your account or JPM features, contact admin@jpmsrv.com

<?php echo $this->element('email/html_footer'); ?>