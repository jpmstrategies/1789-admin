Hi,

<?php echo($user->is_super ? 'An Administrator' : $user->email); ?> has invited you to join and manage
<b><?php echo $tenant->name; ?></b> on Cornerstone.

To accept click on the following link:

<?php echo $url; ?>

Thanks,

<?php echo $this->element('email/text_footer'); ?>