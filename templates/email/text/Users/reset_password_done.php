The password for the account associated with this email address, <?php echo $user->email; ?>, has been changed.

Your password was changed by a user at the following IP address: <?php echo $ip; ?>

If you did not make this change, please contact us immediately!

Thank you for using Cornerstone.

<?php echo $this->element('email/text_footer'); ?>