You've asked to reset your password for the Cornerstone account associated with this email address
(<?php echo $email; ?>). To get the password reset code, please click on the following link:

<?php echo $url; ?>


You can also copy and paste the link above into a new browser window.

Or enter the reset code directly into the password page:

<?php echo $security_code; ?>


If you didn't make the request, you can ignore this email and do nothing. Another user likely entered your email
address by mistake while trying to reset a password.

Replies to this email are not monitored or answered.

Thank you for using Cornerstone.

<?php echo $this->element('email/text_footer'); ?>