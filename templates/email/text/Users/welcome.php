You have just created an account with Cornerstone. For your records, your username is <?php echo $user->email; ?>.

Here are some helpful links to get you through activating your account:

- Sample Link 1

- Sample Link 2

- Sample Link 3

Welcome to Cornerstone,

<?php echo $this->element('email/text_footer'); ?>