<?php

use Cake\Routing\Router;

$this->Html->script('View/bulkedit.js?v=20230629', ['block' => true]);
$this->Html->css('bulkedit.css', ['block' => true]);
?>
<div class="ratings view">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-header">
                <h1><?php echo __('Rating Criteria Bulk Edit'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <?php echo $this->Form->create(null, ['class' => 'form-horizontal', 'id' => 'FilterAdminBulkeditForm']); ?>
            <input type="hidden" name="_csrfToken" value="<?php echo $_COOKIE['csrfToken']; ?>"/>
            <input type="hidden" name="url"
                   value="<?php echo Router::url(['controller' => 'RatingCriterias', 'action' => 'bulkedit'], true); ?>">
            <input type="hidden" name="saveUrl"
                   value="<?php echo Router::url(['controller' => 'RatingCriterias', 'action' => 'bulk-update-row'], true); ?>">
            <div class="row">
                <div class="form-group form-group-sm col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <?php echo $this->Form->control('session',
                                [
                                    'label' => false,
                                    'class' => 'custom-select',
                                    'placeholder' => 'Legislative Session Id',
                                    'default' => $selectedFilters['session']]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
    <div class="alert alert-primary" role="alert">
        Click an individual row to see additional information.
    </div>
</div>
<div class="related row">
    <div class="col-xl-12">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <?php
            $i = 0;
            foreach ($ratingTypes as $ratingTypeId => $ratingTypeName):
                ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo($i === 0 ? 'active' : '') ?>" id="<?php echo $ratingTypeName ?>-tab"
                       data-toggle="tab"
                       href="#<?php echo $ratingTypeName ?>" role="tab"
                       aria-controls="<?php echo $ratingTypeName ?>"
                       aria-selected="true"><?php echo $ratingTypeName; ?></a>
                </li>
                <?php $i++; endforeach; ?>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <?php
            $i = 0;
            foreach ($ratingTypes as $ratingTypeId => $ratingTypeName):
                ?>
                <div class="tab-pane fade <?php echo($i === 0 ? 'show active' : '') ?>"
                     id="<?php echo $ratingTypeName ?>"
                     role="tabpanel" aria-labelledby="<?php echo $ratingTypeName ?>-tab">
                    <?php echo $this->element('RatingCriterias/bulkedit_details', [
                        'tabRatingType' => [
                            'id' => $ratingTypeId,
                            'name' => $ratingTypeName
                        ],
                        'currentRatingId' => $selectedFilters['ratingId'] ?? null,
                    ]); ?>
                </div>
                <?php $i++; endforeach; ?>
        </div>
    </div>
    <!-- end col md 12 -->
</div>
<?php if (isset($selectedFilters['ratingId'])) : ?>

    <a class="btn btn-success"
       href="<?php echo Router::url(['controller' => 'RatingCriterias', 'action' => 'bulkedit', $selectedFilters['session']], true); ?>"
       role="button">View All Bills</a>
<?php endif; ?>
<script type="application/javascript">
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        let targetTableId;
        switch (e.target.id) {
        <?php
            $defaultTable = '';
            $i = 0;
            foreach ($ratingTypes as $ratingTypeId => $ratingTypeName):
            ?>
            case '<?php echo $ratingTypeName?>-tab':
                targetTableId = '<?php echo $ratingTypeName?>-table';
                break;
        <?php
            if ($i === 0) {
                $defaultTable = $ratingTypeName . '-table';
            }
            $i++; endforeach;
            ?>
            default:
                targetTableId = '<?php echo $defaultTable; ?>';
        }
        $('table#' + targetTableId).fixedHeader();
    })
    jQuery(document).ready(function ($) {
        // prevent row from expanding when changing selects
        jQuery('tr').click(function (e) {
            if (e.target.tagName !== 'TD') {
                e.stopPropagation()
            }
        })
    })
</script>