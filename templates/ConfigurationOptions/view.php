<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ConfigurationOption $configurationOption
 */
?>
<div class="configurationOptions view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($configurationOption->name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>  Edit Configuration Option')
                                    , ['action' => 'edit', $configurationOption->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>  List Configuration Options')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>  New Configuration Option')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('Configuration Type') ?></th>
                    <td><?= $configurationOption->has('configuration_type') ? $this->Html->link($configurationOption->configuration_type->name, [
                            'controller' => 'ConfigurationTypes',
                            'action' => 'view',
                            $configurationOption->configuration_type->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($configurationOption->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sort Order') ?></th>
                    <td><?= $this->Number->format($configurationOption->sort_order) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($configurationOption->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($configurationOption->modified) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>

