<?php

use Cake\Core\Configure;

$cakeDescription = 'Cornerstone';
$rootUrl = $this->Url->build('/', ['fullBase' => true]);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta property="og:site_name" content="<?= $cakeDescription ?>"/>
    <meta property="og:url"
          content="<?php echo $this->Url->build($this->request->getRequestTarget(), ['fullBase' => true]); ?>"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="<?= $this->fetch('title') ?> | <?= $cakeDescription ?>"/>
    <meta property="og:description"
          content="Cornerstone Boilerplate"/>
    <meta property="og:site_name" content="<?= $cakeDescription ?>"/>
    <meta property="article:publisher" content="https://www.facebook.com/jpmstrategies"/>
    <meta property="og:image"
          content="<?php echo $rootUrl; ?>img/social-share.png"/>
    <meta property="og:image:secure_url"
          content="<?php echo $rootUrl; ?>img/social-share.png"/>
    <meta property="og:image:alt" content="<?= $this->fetch('title') ?> | <?= $cakeDescription ?>"/>
    <meta property="og:image:width" content="1500"/>
    <meta property="og:image:height" content="788"/>
    <meta property="fb:app_id" content=""/>
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="<?= $this->fetch('title') ?> | <?= $cakeDescription ?>"/>
    <meta name="twitter:site" content="@jpmstrategies"/>
    <meta name="twitter:image"
          content="<?php echo $rootUrl; ?>img/social-share.png"/>
    <meta name="twitter:creator" content="@jpmstrategies"/>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css"
          integrity="sha512-oc9+XSs1H243/FRN9Rw62Fn8EtxjEYWHXRvjS43YtueEewbS6ObfXcJNyohjHqVKFPoXXUxwc+q1K7Dee6vv9g=="
          crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/3.5.0/octicons.css"
          integrity="sha512-anof6FfHcp88hfx4BTym0HrMXnRR7t0mTu7YdG0VMSBFH6gXvIT0dODcgKFZuSoFUv4otTOG6Hx/GtTNNoOvjg=="
          crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
          integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw=="
          crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote-bs4.min.css"
          integrity="sha512-pDpLmYKym2pnF0DNYDKxRnOk1wkM9fISpSOjt8kWFKQeDmBTjSnBZhTd41tXwh8+bRMoSaFsRnznZUiH9i3pxA=="
          crossorigin="anonymous"/>

    <?php
    # https://github.com/ttskch/select2-bootstrap4-theme
    $this->Html->css('select2/select2-bootstrap4.min.css', ['block' => true]);
    $this->Html->css('fixedHeader/fixedHeader.bootstrap4.min.css', ['block' => true]);
    ?>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
            integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"
            integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA=="
            crossorigin="anonymous"></script>
    <?php //TODO upgrade this library ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha512-Ua/7Woz9L5O0cwB/aYexmgoaD7lw3dWe9FvXejVdgqu71gRog3oJgjSWQR55fwWx+WKuk8cl7UwA1RS6QCadFA=="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.min.js"
            integrity="sha512-8qmis31OQi6hIRgvkht0s6mCOittjMa9GMqtK9hes5iEQBQE/Ca6yGE5FsW36vyipGoWQswBj/QBm2JR086Rkw=="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"
            integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A=="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"
            integrity="sha512-d4KkQohk+HswGs6A1d6Gak6Bb9rMWtxjOa0IiY49Q3TeFd5xAzjWXDCBW9RS7m86FQ4RzM2BdHmdJnnKRYknxw=="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote-bs4.min.js"
            integrity="sha512-+cXPhsJzyjNGFm5zE+KPEX4Vr/1AbqCUuzAS8Cy5AfLEWm9+UI9OySleqLiSQOQ5Oa2UrzaeAOijhvV/M4apyQ=="
            crossorigin="anonymous"></script>

    <?php
    $this->Html->script('fixedHeaders.js', ['block' => true]);
    $this->Html->script('session-timeout.js?v=20210313', ['block' => true]);
    ?>

    <?php
    echo $this->Html->meta('icon');

    if (!empty($controllerStylesheet) && $controllerStylesheet === 'public') {
        echo $this->Html->css('public');
    }

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $rootUrl; ?>img/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $rootUrl; ?>img/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $rootUrl; ?>img/icons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $rootUrl; ?>img/icons/site.webmanifest">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"
            integrity="sha512-UDJtJXfzfsiPPgnI5S1000FPLBHMhvzAMX15I+qG2E2OAzC9P1JzUwJOfnypXiOH7MRPaqzhPbBGDNNj7zBfoA=="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"
            integrity="sha512-qWVvreMuH9i0DrugcOtifxdtZVBBL0X75r9YweXsdCHtXUidlctw7NXg5KVP3ITPtqZ2S575A0wFkvgS2anqSA=="
            crossorigin="anonymous"></script>
    <![endif]-->

    <style type="text/css">
        body {
            padding: 70px 0;
        }

        .error-message {
            color: #b94a48;
            background-color: #f2dede;
            height: 34px;
            padding: 6px 12px;
            margin-top: 10px;
            margin-bottom: 10px;
            border: 1px solid #eed3d7;
            border-radius: 4px;
        }
    </style>

    <meta name="google" content="notranslate">
</head>

<body>

<?php echo $this->Element('navigation'); ?>

<div class="container">

    <?php echo $this->Flash->render(); ?>
    <?php echo $this->fetch('content'); ?>

</div>
<!-- /.container -->

<!-- Modal -->
<div class="modal fade" id="session-timeout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Session Timeout Warning</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Your session will expire in <span id="timeout-remaining"></span>.
                <br/>
                <br/>
                It looks like you've been inactive for a while. For your security, we will automatically log you off
                after a period of inactivity.
            </div>
            <div class="modal-footer" id="session-options-countdown">
                <button type="button" class="btn btn-primary" id="extend-timeout-session">Extend Session</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="session-logout">Logout</button>
            </div>
        </div>
    </div>
</div>
<!-- /.Modal -->
<?php if (isset($logged_in) && $logged_in) : ?>
    <script type="text/javascript">
        sessionTimeout(<?php echo Configure::read('Session.timeout') ? Configure::read('Session.timeout') * 60 : ini_get('session.gc_maxlifetime');?>)
    </script>
<?php endif ?>
<?php echo $this->element('google-analytics'); ?>
</body>

</html>