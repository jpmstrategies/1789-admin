<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Criteria[]|\Cake\Collection\CollectionInterface $criterias
 */
?>
<div class="criterias index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Criterias') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Criteria')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Party
                                ')
                    , ['controller' => 'Parties', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>

        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('method') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('party_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('sort_order') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($criterias as $criteria): ?>
                    <tr>
                        <td>
                            <?= h($criteria->name) ?>
                        </td>
                        <td>
                            <?php echo $this->Method->display_text($criteria->method); ?>
                        </td>
                        <td>
                            <?= $criteria->has('party') ?
                                $this->Html->link($criteria->party
                                    ->name, ['controller' => 'Parties
                                            ',
                                    'action' => 'view', $criteria->party
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $this->Number->format($criteria->sort_order) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $criteria->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $criteria->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $criteria->id]
                                , ['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $criteria->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
