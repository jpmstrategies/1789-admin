<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Criteria $criteria
 */
?>
<div class="criterias view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($criteria->name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>
                                Edit Criteria')
                                    , ['action' => 'edit', $criteria->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>
                                List Criterias')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>
                                New Criteria')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($criteria->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Th Detail Name') ?></th>
                    <td><?= h($criteria->th_detail_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Th Vote Name') ?></th>
                    <td><?= h($criteria->th_vote_name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Method') ?></th>
                    <td><?php echo $this->Method->help_text($criteria->method); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Party') ?></th>
                    <td><?= $criteria->has('party') ?
                            $this->Html->link($criteria->party
                                ->name, ['controller' => 'Parties',
                                'action' => 'view', $criteria->party
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sort Order') ?></th>
                    <td><?= $this->Number->format($criteria->sort_order) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($criteria->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($criteria->modified) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
