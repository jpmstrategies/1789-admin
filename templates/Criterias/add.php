<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Criteria $criteria
 */
?>
<div class="criterias form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= __('Add Criteria') ?></h1>
            </div>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Criterias
                                ')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link'])
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($criteria); ?>

            <div class="form-group">
                <?php echo $this->Form->control('name', ['class' => 'form-control', 'placeholder' => 'Name']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('th_detail_name', [
                    'class' => 'form-control',
                    'placeholder' => 'Left Column Header',
                    'label' => 'Left Column Header']); ?>
                <small class="form-text text-muted">This controls the left column header for the vote display on the
                    Legislator Page.
                </small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('th_vote_name', [
                    'class' => 'form-control',
                    'placeholder' => 'Right Column Headern',
                    'label' => 'Right Column Header']); ?>
                <small class="form-text text-muted">This controls the right column header for the vote display on the
                    Legislator Page.
                </small>
            </div>
            <div class="form-group">
                <?php
                echo $this->Method->control('method', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <div class="small">
                    <?php
                    foreach ($this->Method->options as $key => $description) {
                        echo $this->Method->help_text($key);
                        echo "<br />";
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('party_id', ['options' => $parties, 'empty' => true,
                    'class' => 'custom-select']);
                ?>
            </div>


            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            <?php echo $this->Html->link(
                'Cancel',
                ['controller' => 'criterias', 'action' => 'index', '_full' => true],
                ['class' => 'btn btn-secondary', 'id' => 'cancel']
            ); ?>
            <?= $this->Form->end() ?>
        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->

<script>
    $(document).ready(function () {
        // disable party_id unless the value of method is "P"
        $('#party-id').prop('disabled', true);

        $('#method').change(function () {
            if ($(this).val() === 'P') {
                $('#party-id').prop('disabled', false);
            } else {
                $('#party-id').val('');
                $('#party-id').prop('disabled', true);
            }
        });
    });
</script>