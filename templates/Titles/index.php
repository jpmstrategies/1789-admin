<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Title[]|\Cake\Collection\CollectionInterface $titles
 */
?>
<div class="titles index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Titles') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Title')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('chamber_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('branch') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('office') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('title_long') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('title_short') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('sort_order') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($titles as $title): ?>
                    <tr>
                        <td>
                            <?= $title->has('chamber') ?
                                $this->Html->link($title->chamber
                                    ->name, [
                                    'controller' => 'Chambers',
                                    'action' => 'view',
                                    $title->chamber
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= h($title->branch) ?>
                        </td>
                        <td>
                            <?= h($title->office) ?>
                        </td>
                        <td>
                            <?= h($title->title_long) ?>
                        </td>
                        <td>
                            <?= h($title->title_short) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($title->sort_order) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $title->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $title->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $title->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete # {0}?', $title->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->



