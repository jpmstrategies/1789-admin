<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Title $title
 */
?>
<div class="titles view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($title->title_long) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>
                                Edit Title')
                                    , ['action' => 'edit', $title->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>
                                List Titles')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>
                                New Title')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('Chamber') ?></th>
                    <td><?= $title->has('chamber') ?
                            $this->Html->link($title->chamber
                                ->name, [
                                'controller' => 'Chambers',
                                'action' => 'view',
                                $title->chamber
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Branch') ?></th>
                    <td><?= h($title->branch) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Office') ?></th>
                    <td><?= h($title->office) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Long Title') ?></th>
                    <td><?= h($title->title_long) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Short Title') ?></th>
                    <td><?= h($title->title_short) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sort Order') ?></th>
                    <td><?= $this->Number->format($title->sort_order) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($title->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($title->modified) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
