<div class="ratingFields form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Edit Rating Field'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">

                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete'),
                                    ['action' => 'delete', $ratingField->id],
                                    [
                                        'escape' => false,
                                        'class' => 'nav-link'], __('Are you sure you want to delete # {0}?',
                                        $ratingField->id)); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Rating Fields'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($ratingField); ?>

            <div class="form-group">
                <?php echo $this->Form->control('id', ['class' => 'form-control', 'placeholder' => 'Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('name', ['class' => 'form-control', 'placeholder' => 'Name']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('json_path', [
                    'class' => 'form-control',
                    'placeholder' => 'JSON Path']); ?>
                <small class="form-text text-muted">Please use the dot notion.</small>
            </div>
            <div class="form-group">
                <?php echo $this->RatingField->control('type', ['class' => 'form-control', 'placeholder' => 'Type']); ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('sort_order', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <div class="small">Set to the desired position, and the other ratings will be automatically
                    re-numbered when a conflict occurs.
                </div>
            </div>
            <div class="form-group">
                <?php echo $this->Form->submit(__('Submit'), ['class' => 'btn btn-primary']); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
