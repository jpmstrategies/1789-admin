<div class="ratingFields view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Rating Field'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp&nbsp;Edit Rating Field'),
                                    ['action' => 'edit', $ratingField['id']],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete Rating Field'),
                                    ['action' => 'delete', $ratingField['id']],
                                    [
                                        'escape' => false,
                                        'class' => 'nav-link'], __('Are you sure you want to delete # {0}?',
                                        $ratingField['id'])); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp&nbsp;List Rating Fields'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Rating Field'),
                                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?> </li>

                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">
                <tbody>
                <tr>
                    <th><?php echo __('Id'); ?></th>
                    <td>
                        <?php echo h($ratingField['id']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Name'); ?></th>
                    <td>
                        <?php echo h($ratingField['name']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('JSON Path'); ?></th>
                    <td>
                        <?php echo h($ratingField['json_path']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Type'); ?></th>
                    <td>
                        <?php echo $this->RatingField->display_text($ratingField['type']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Sort Order'); ?></th>
                    <td>
                        <?php echo h($ratingField['sort_order']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td>
                        <?php echo h($ratingField['created']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td>
                        <?php echo h($ratingField['modified']); ?>
                        &nbsp;
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
        <!-- end col md 9 -->

    </div>
</div>
