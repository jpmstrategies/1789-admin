<div class="page-content container">
    <div class="row">
        <div class="col-md-8 offset-md-2 col-12 offset-0">
            &nbsp;<?php use Cake\Core\Configure;

            echo $this->Flash->render(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 offset-md-2 col-12 offset-0">
            <div class="login-wrapper">
                <div class="box">
                    <div class="content-wrap">
                        <div style="margin-bottom: 25px;">
                            <h2>Sign in</h2>

                            <?php
                            if (!empty($inviteSession) && Configure::read('users.self_signup_enabled') !== 1) :
                                ?>
                                <h4>
                                    <small class="text-muted">You've been invited to manage <b><?php
                                            $tenant = $inviteSession['tenant']['name'];
                                            if (strtolower(substr((string)$tenant, -1)) === 's') {
                                                echo '<b>' . $inviteSession['tenant']['name'] . '</b>\'';
                                            } else {
                                                echo '<b>' . $inviteSession['tenant']['name'] . '</b>\'s';
                                            } ?>
                                        </b>
                                        account.
                                    </small>
                                </h4>
                            <?php endif; ?>
                        </div>

                        <div class="col-md-8 offset-md-2">
                            <?php echo $this->Form->create(null, ['class' => 'form-signin']); ?>
                            <?php
                            $emailAttributes = [
                                'class' => 'form-control',
                                'type' => 'email',
                                'placeholder' => 'Email Address',
                                'label' => false,
                                'required' => true];

                            echo $this->Form->control('email', $emailAttributes);
                            ?>
                            <?php echo $this->Form->control('password',
                                [
                                    'class' => 'form-control',
                                    'type' => 'password',
                                    'placeholder' => 'Password',
                                    'label' => false,
                                    'required' => true]); ?>
                            <small class="form-text text-muted" style="text-align: left">
                                <?php
                                echo $this->Html->link(__('Forgot your password?'),
                                    ['controller' => 'users', 'action' => 'forgotPassword'], [
                                        'escape' => false,
                                        'class' => 'text-muted']);
                                ?>
                            </small>

                            <div class="action">
                                <?= $this->Form->button(__('Sign in'), ['class' => 'btn btn-lg btn-primary btn-block']); ?>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>

                <?php
                if (Configure::read('users.self_signup_enabled')) :
                    ?>
                    <div class="col-md-8 offset-md-2">
                        <div class="already">
                            <?php
                            echo $this->Html->link(__('New to Cornerstone? Sign up today!'),
                                ['controller' => 'users', 'action' => 'signup'], ['escape' => false]);
                            ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
