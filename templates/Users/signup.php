<div class="page-content container">
    <div class="row">
        <div class="col-md-8 offset-md-2 col-12 offset-0">
            &nbsp;<?php echo $this->Flash->render(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 offset-md-2 col-12 offset-0">
            <div class="login-wrapper">
                <div class="box">
                    <div class="content-wrap">
                        <div style="margin-bottom: 25px;">
                            <h2>Sign up</h2>
                        </div>

                        <div class="col-md-8 offset-md-2">
                            <?= $this->Form->create(null, [
                                'id' => 'signup-form',
                                'class' => 'needs-validation form-signin',
                                'novalidate' => true]) ?>
                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control" autocomplete="email"
                                       placeholder="Email Address" required="required"
                                       pattern="^\S+@\S+\.\S+$"
                                       aria-required="true" aria-label="Email Address"
                                       value="<?php echo isset($inviteSession) ? $inviteSession['user']['email'] : ''; ?>">
                                <div class="invalid-feedback">
                                    Please enter a valid email address.
                                </div>
                            </div>
                            <small class="form-text">
                                By clicking Create Account, I agree to JPM's <?php
                                echo $this->Html->link(__('Terms'),
                                    ['controller' => 'pages', 'action' => 'terms'], [
                                        'escape' => false,
                                        'target' => '_blank']);
                                ?>, <?php
                                echo $this->Html->link(__('Privacy Policy'),
                                    ['controller' => 'pages', 'action' => 'privacyPolicy'], [
                                        'escape' => false,
                                        'target' => '_blank']);
                                ?>, and <?php
                                echo $this->Html->link(__('Cookie Policy'),
                                    ['controller' => 'pages', 'action' => 'cookiePolicy'], [
                                        'escape' => false,
                                        'target' => '_blank']);
                                ?>.
                            </small>
                            <div class="action">
                                <?= $this->Form->button(__('Create Account'), ['class' => 'btn btn-lg btn-primary btn-block']); ?>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-8 offset-md-2">
                    <div class="already">
                        <?php if (!empty($inviteSession)) : ?>
                            <div style="float:left">
                                <?= $this->Html->link(__('Sign up'),
                                    ['controller' => 'users', 'action' => 'finishSignup'],
                                    ['escape' => false]);
                                ?>
                            </div>
                            <?php $passwordFloat = 'right'; ?>
                        <?php else : ?>
                            <?php $passwordFloat = 'none'; ?>
                        <?php endif; ?>
                        <div style="float:<?php echo $passwordFloat; ?>">
                            <?php
                            echo $this->Html->link(__('Already have an account? Sign in'),
                                ['controller' => 'users', 'action' => 'login'], ['escape' => false]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    (function () {
        'use strict';
        window.addEventListener('load', function () {

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            let forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            let validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);

                let emailInput = document.getElementById('email');
                emailInput.addEventListener('keyup', (e) => {
                    if (/.+@.+\..{3}/.test(emailInput.value)) {
                        form.classList.add('was-validated');
                    }
                });
                emailInput.addEventListener('blur', (e) => {
                    form.classList.add('was-validated');
                });
            });
        }, false);
    })();
</script>
