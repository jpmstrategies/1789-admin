<div class="users form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>
                    <?php
                    $title = ($usingSession ? 'Edit My Profile' : 'Edit User');
                    echo __($title);
                    ?>
                </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <?php
                                $id = ($usingSession ? '' : $user['id']);
                                $linkTitle = ($usingSession ? 'My Profile' : 'View User Profile');
                                echo $this->Html->link(__('<span class="octicon octicon-search"></span>&nbsp;&nbsp;' . $linkTitle),
                                    ['action' => 'view', $id], ['escape' => false, 'class' => 'nav-link']);
                                ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($user); ?>

            <div class="form-group">
                <?php echo $this->Form->control('id', ['class' => 'form-control', 'placeholder' => 'Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('email', [
                    'class' => 'form-control',
                    'placeholder' => 'Email',
                    'readonly' => ($is_super ? '' : 'readonly')]); ?>
                <?php if ($currentTenantRole === 'U') : ?>
                    <small class="form-text text-muted">Please contact an administrator to change your email
                        address.</small>
                <?php endif; ?>
            </div>
            <?php
            echo $this->element('Users/user_details_form', ['userFields' => $userFields]);
            ?>
            <div class="form-check">
                <?php
                $formType = ($usingSession ? 'hidden' : 'checkbox');
                echo $this->Form->control('is_enabled', [
                    'label' => 'Enabled',
                    'class' => 'form-check-input',
                    'type' => $formType]);
                ?>
            </div>
            <div class="form-check">
                <?php
                $formType = ($usingSession ? 'hidden' : 'checkbox');
                echo $this->Form->control('is_super', [
                    'label' => 'Super Admin',
                    'class' => 'form-check-input',
                    'type' => $formType]);
                ?>
            </div>
            <div class="form-group">
                <?= $this->Form->button(__('Update'), ['class' => 'btn btn-primary']) ?>
                <?php echo $this->Html->link(
                    'Cancel',
                    ['controller' => 'users', 'action' => 'view', '_full' => true],
                    ['class' => 'btn btn-secondary', 'id' => 'cancel']
                ); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
