<div class="users index">

    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Users'); ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New User'),
                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?></li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('email'); ?></th>
                    <?php
                    if (isset($userFields)) {
                        foreach ($userFields as $userField) {
                            ?>
                            <th><?php echo $userField['description']; ?></th>
                            <?php
                        }
                    }
                    ?>
                    <th><?php echo $this->Paginator->sort('is_enabled', 'Enabled'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $user): ?>
                    <tr>
                        <td><?php echo h($user['email']); ?>&nbsp;</td>
                        <?php
                        if (isset($userFields)) {
                            foreach ($userFields as $userField) {
                                ?>
                                <td><?php echo $user->user_details[$userField['name']] ?? null; ?></td>
                                <?php
                            }
                        }
                        ?>
                        <td><?php echo $this->Enable->display_text($user['is_enabled']); ?>&nbsp;</td>
                        <td class="actions">
                            <?php echo $this->Html->link('<span class="octicon octicon-search"></span>',
                                ['action' => 'view', $user['id']], ['escape' => false]); ?>
                            <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                                ['action' => 'edit', $user['id']], ['escape' => false]); ?>
                            <?php echo $this->Form->postLink('<span class="octicon octicon-trashcan"></span>',
                                ['action' => 'delete', $user['id']], ['escape' => false],
                                __('Are you sure you want to delete # {0}?', $user['id'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <p>
                <small><?php ?></small>
            </p>

            <?php
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->


</div><!-- end containing of content -->
