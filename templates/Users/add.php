<div class="users form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Add User'); ?></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">

                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Users'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($user); ?>

            <div class="form-group">
                <?php echo $this->Form->control('email', ['class' => 'form-control', 'placeholder' => 'Email']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('password',
                    ['class' => 'form-control', 'placeholder' => 'Password', 'type' => 'password']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('password_confirm',
                    ['class' => 'form-control', 'placeholder' => 'Confirm Password', 'type' => 'password']); ?>
            </div>
            <div class="form-check">
                <?php echo $this->Form->control('is_enabled', ['label' => 'Enabled', 'class' => 'form-check-input']); ?>
            </div>
            <div class="form-check">
                <?php echo $this->Form->control('is_super', [
                    'label' => 'Super Admin',
                    'class' => 'form-check-input']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->submit(__('Submit'), ['class' => 'btn btn-primary']); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
