<div class="page-content container">
    <div class="row">
        <div class="col-md-8 offset-md-2 col-12 offset-0">
            &nbsp;<?php echo $this->Flash->render(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 offset-md-2 col-12 offset-0">
            <div class="login-wrapper">
                <div class="box">
                    <div class="content-wrap">
                        <div style="margin-bottom: 25px;">
                            <h2>Reset Your Password</h2>
                            <h4>
                                <small class="text-muted">
                                    Check your email for the password reset code.<br/>
                                    Then click the link or enter the reset code below to reset your password.
                                </small>
                            </h4>
                        </div>

                        <div class="col-md-12">
                            <?php echo $this->Form->create(null); ?>
                            <div class="form-group">
                                <?php
                                echo $this->Form->control('security_code',
                                    [
                                        'class' => 'form-control',
                                        'placeholder' => 'Reset code',
                                        'label'
                                        => 'Security Code',
                                        'value' => $security_code,
                                        'required' => true]);
                                ?>
                            </div>
                            <div class="form-group">
                                <?php
                                echo $this->Form->control('password',
                                    [
                                        'class' => 'form-control',
                                        'type' => 'password',
                                        'placeholder' => 'New Password',
                                        'label'
                                        => 'New Password',
                                        'required' => true,
                                        'pattern' => '.{8,20}',
                                        'title' => 'Password must be between 8 and 20 characters']);
                                ?>
                                <small class="form-text text-muted">Password must be between 8 and 20
                                    characters.</small>
                            </div>
                            <div class="form-group">
                                <?php
                                echo $this->Form->control('password_confirm',
                                    [
                                        'class' => 'form-control',
                                        'type' => 'password',
                                        'placeholder' => 'Confirm Password',
                                        'label'
                                        => 'Confirm Password',
                                        'required' => true,
                                        'pattern' => '.{8,20}',
                                        'title' => 'Password must be between 8 and 20 characters']);
                                ?>
                                <small class="form-text text-muted">Please re-enter your new password.</small>
                            </div>
                            <div class="signup">
                                <?= $this->Form->button(__('Reset Password'), ['class' => 'btn btn-lg btn-primary btn-block']); ?>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 offset-md-2">
                    <div class="already">
                        <p>
                            <?php
                            echo $this->Html->link(__('Already have an account? Sign in'),
                                ['controller' => 'users', 'action' => 'login'], ['escape' => false]);
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
