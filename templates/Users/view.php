<div class="users view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>
                    <?php
                    $title = ($usingSession ? 'My Profile' : 'User');
                    echo __($title);
                    ?>
                </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <?php
                                $id = ($usingSession ? '' : $user['id']);
                                $linkTitle = ($usingSession ? 'Edit My Profile' : 'Edit User');
                                echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp&nbsp;' . $linkTitle),
                                    ['action' => 'edit', $id], ['escape' => false, 'class' => 'nav-link']);
                                ?>
                            </li>
                            <?php if ($usingSession) : ?>
                                <li class="nav-item">
                                    <?php
                                    echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp&nbsp; Edit Password'),
                                        ['action' => 'editPassword', $id], ['escape' => false, 'class' => 'nav-link']);
                                    ?>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->

        <div class="col-md-9">
            <table class="table table-striped table-sm">
                <tbody>
                <tr>
                    <th><?php echo __('Email'); ?></th>
                    <td>
                        <?php echo h($user['email']); ?>
                        &nbsp;
                    </td>
                </tr>
                <?php
                if (isset($userFields)) {
                    $userDetails = $user->user_details;
                    foreach ($userFields as $userField) {
                        $key = $userField['name'];
                        ?>
                        <tr>
                            <th><?php echo $userField['description']; ?></th>
                            <td>
                                <?php echo $userDetails[$key] ?? null; ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
                <?php if ($usingSession === FALSE) : ?>
                    <tr>
                        <th><?php echo __('Enabled'); ?></th>
                        <td>
                            <?php echo $this->Enable->display_text($user['is_enabled']); ?>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Super Admin'); ?></th>
                        <td>
                            <?php echo $this->Enable->display_text($user['is_super']); ?>
                            &nbsp;
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if ($currentTenantRole !== 'U') : ?>
                    <tr>
                        <th><?php echo __('Created'); ?></th>
                        <td>
                            <?php echo h($user['created']); ?>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Modified'); ?></th>
                        <td>
                            <?php echo h($user['modified']); ?>
                            &nbsp;
                        </td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>

        </div>
        <!-- end col md 9 -->

    </div>
</div>

