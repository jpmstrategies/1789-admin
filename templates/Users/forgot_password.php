<div class="page-content container">
    <div class="row">
        <div class="col-md-8 offset-md-2 col-12 offset-0">
            &nbsp;<?php echo $this->Flash->render(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 offset-md-2 col-12 offset-0">
            <div class="login-wrapper">
                <div class="box">
                    <div class="content-wrap">
                        <div style="margin-bottom: 25px;">
                            <h2>Forgot Your Password?</h2>
                            <h4>
                                <small class="text-muted">Enter your email address below and
                                    we'll send you instructions on how to reset it securely!
                                </small>
                            </h4>
                        </div>

                        <div class="col-md-10 offset-md-1">
                            <?php echo $this->Form->create(null); ?>
                            <?php
                            echo $this->Form->control('email', [
                                'class' => 'form-control',
                                'type' => 'email',
                                'placeholder' => 'Email Address',
                                'label' => false,
                                'required' => true]);
                            ?>
                            <div class="signup">
                                <?= $this->Form->button(__('Send'), ['class' => 'btn btn-lg btn-primary btn-block']); ?>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 offset-md-2">
                    <div class="already">
                        <p>
                            <?php
                            echo $this->Html->link(__('Already have an account? Sign in'),
                                ['controller' => 'users', 'action' => 'login'], ['escape' => false]);
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
