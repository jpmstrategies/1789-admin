<div class="page-content container">
    <div class="row">
        <div class="col-md-8 offset-md-2 col-12 offset-0">
            &nbsp;<?php use Cake\Core\Configure;

            echo $this->Flash->render(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 offset-md-2 col-12 offset-0">
            <div class="login-wrapper">
                <div class="box">
                    <div class="content-wrap">
                        <div style="margin-bottom: 25px;">
                            <h2>Get Started with Cornerstone</h2>
                            <?php
                            if (!empty($inviteSession) && Configure::read('users.self_signup_enabled') !== 1) :
                                ?>
                                <h4>
                                    <small class="text-muted">You've been invited to manage <b><?php
                                            $tenant = $inviteSession->tenant->name;
                                            if (strtolower(substr((string)$tenant, -1)) === 's') {
                                                echo '<b>' . $tenant . '</b>\'';
                                            } else {
                                                echo '<b>' . $tenant . '</b>\'s';
                                            } ?>
                                        </b>
                                        account.
                                    </small>
                                </h4>
                            <?php endif; ?>
                        </div>

                        <?php if (!empty($user)) : ?>
                            <div class="col-md-12">
                                <?php echo $this->Form->create($user); ?>
                                <?php echo $this->Form->control('id', ['type' => 'hidden']); ?>
                                <?php echo $this->Form->control('is_enabled', ['type' => 'hidden']); ?>
                                <?php
                                if (!empty($inviteSession)) {
                                    // custom fields from configuration-types
                                    foreach ($userFields as $userField) {
                                        echo $this->element('Configurations/tab', [
                                            "configuration_type" => $userField,
                                            "data_id" => 'ct-' . $userField['id'],
                                            "field_name" => 'ct[' . $userField['name'] . ']',
                                            "label_value" => $userField->description,
                                            "input_value" => $userDetails['decoded'][$userField['name']] ?? null,
                                            "configuration_options" => $userFieldOptions[$userField->id] ?? [],
                                            "target_tab" => 'user_details',
                                            "description" => null,
                                            "ajax_save" => false,
                                            "error" => in_array($userField['name'], $userDetails['validation_failed']) ?? 0
                                        ]);
                                    }

                                    $email = $inviteSession->user->email;
                                    echo $this->Form->control('email',
                                        [
                                            'class' => 'form-control',
                                            'label' => 'Email Address',
                                            'value' => $email,
                                            'readonly' => 'readonly']);
                                    ?>
                                    <?php
                                    echo $this->Form->control('password',
                                        [
                                            'class' => 'form-control',
                                            'type' => 'password',
                                            'placeholder' => 'Password',
                                            'label' => 'Password',
                                            'required' => true,
                                            'pattern' => '.{8,20}',
                                            'title' => 'Password must be between 8 and 20 characters']);
                                    ?>
                                    <small class="form-text text-muted">Password must be between 8 and 20
                                        characters.</small>
                                    <?php
                                    echo $this->Form->control('password_confirm',
                                        [
                                            'class' => 'form-control',
                                            'type' => 'password',
                                            'placeholder' => 'Confirm Password',
                                            'label' => 'Confirm Password',
                                            'required' => true,
                                            'pattern' => '.{8,20}',
                                            'title' => 'Password must be between 8 and 20 characters']);
                                    ?>
                                    <small class="form-text text-muted">Please re-enter your password.</small>
                                    <?php
                                } ?>
                                <br/>
                                <small class="form-text text-muted">By clicking Create Account, I agree to
                                    JPM's <?php
                                    echo $this->Html->link(__('Terms'),
                                        ['controller' => 'pages', 'action' => 'terms'], [
                                            'escape' => false,
                                            'target' => '_blank']);
                                    ?>, <?php
                                    echo $this->Html->link(__('Privacy Policy'),
                                        ['controller' => 'pages', 'action' => 'privacyPolicy'], [
                                            'escape' => false,
                                            'target' => '_blank']);
                                    ?>, and <?php
                                    echo $this->Html->link(__('Cookie Policy'),
                                        ['controller' => 'pages', 'action' => 'cookiePolicy'], [
                                            'escape' => false,
                                            'target' => '_blank']);
                                    ?>.
                                </small>
                                <div class="signup">
                                    <?= $this->Form->button(__('Create Account'), ['class' => 'btn btn-lg btn-primary btn-block']); ?>
                                </div>
                                <?= $this->Form->end() ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-8 offset-md-2">
                    <div class="already">
                        <p>
                            <?php
                            echo $this->Html->link(__('Already have an account? Sign in'),
                                ['controller' => 'users', 'action' => 'login'], ['escape' => false]);
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        <?php
        foreach ($userFields as $userField) {
            if (isset($userField->field_mask)) {
                echo 'jQuery("[data-id=ct-' . $userField->id . ']").mask("' . $userField->field_mask . '");';
            }
        }
        ?>
    });
</script>
