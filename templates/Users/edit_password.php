<div class="users form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>
                    <?php
                    $title = ($usingSession ? 'Edit My Password' : 'Edit User Password');
                    echo __($title);
                    ?>
                </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <?php
                                $id = ($usingSession ? '' : $user['id']);
                                $linkTitle = ($usingSession ? 'My Profile' : 'View User Profile');
                                echo $this->Html->link(__('<span class="octicon octicon-search"></span>&nbsp;&nbsp;' . $linkTitle),
                                    ['action' => 'view', $id], ['escape' => false, 'class' => 'nav-link']);
                                ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($user); ?>

            <div class="form-group">
                <?php echo $this->Form->control('id', ['class' => 'form-control', 'placeholder' => 'Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->hidden('email'); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('current_password',
                    [
                        'class' => 'form-control',
                        'placeholder' => 'Password',
                        'type' => 'password',
                        'label' => 'Current Password']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('password_update',
                    [
                        'class' => 'form-control',
                        'placeholder' => 'New Password',
                        'type' => 'password',
                        'label' => 'New Password',
                        'required' => true,
                        'pattern' => '.{8,20}',
                        'title' => 'Password must be between 8 and 20 characters']); ?>
                <small class="form-text text-muted">Password must be between 8 and 20 characters.</small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('password_update_confirm',
                    [
                        'class' => 'form-control',
                        'placeholder' => 'Confirm Password',
                        'type' => 'password',
                        'label' => 'New Password Confirmation',
                        'required' => true,
                        'pattern' => '.{8,20}',
                        'title' => 'Password must be between 8 and 20 characters']); ?>
                <small class="form-text text-muted">Please re-enter your new password.</small>
            </div>
            <div class="form-group">
                <?= $this->Form->button(__('Update'), ['class' => 'btn btn-primary']) ?>
                <?php echo $this->Html->link(
                    'Cancel',
                    ['controller' => 'users', 'action' => 'view', '_full' => true],
                    ['class' => 'btn btn-secondary', 'id' => 'cancel']
                ); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
