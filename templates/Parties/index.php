<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Party[]|\Cake\Collection\CollectionInterface $parties
 */
?>
<div class="parties index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Parties') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Party')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legislator')
                    , ['controller' => 'Legislators', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('abbreviation') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('css_color') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('is_enabled', 'Enabled') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($parties as $party): ?>
                    <tr>
                        <td>
                            <?= h($party->name) ?>
                        </td>
                        <td>
                            <?= h($party->abbreviation) ?>
                        </td>
                        <td>
                            <div style="width: 25px; float: left; background-color: <?php echo $party->css_color; ?>">
                                &nbsp;
                            </div>
                            <div style="float: left; padding-left: 5px">
                                <?= h($party->css_color) ?>
                            </div>
                        </td>
                        <td>
                            <?php echo $this->Enable->display_text($party->is_enabled) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $party->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $party->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $party->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete # {0}?', $party->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->



