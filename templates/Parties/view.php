<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Party $party
 */
?>
<div class="parties view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($party->name) ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>  Edit Party')
                                    , ['action' => 'edit', $party->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>  List Parties')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>  New Party')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($party->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Abbreviation') ?></th>
                    <td><?= h($party->abbreviation) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Css Color') ?></th>
                    <td>
                        <div style="width: 25px; float: left; background-color: <?php echo $party->css_color; ?>">
                            &nbsp;
                        </div>
                        <div style="float: left; padding-left: 5px">
                            <?= h($party->css_color) ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Legiscan ID') ?></th>
                    <td><?= h($party->legiscan_party_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Enabled') ?></th>
                    <td><?php echo $this->Enable->display_text($party->is_enabled); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($party->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($party->modified) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>