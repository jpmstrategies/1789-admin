<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FloorReport $floorReport
 */
?>
<div class="floorReports view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($floorReport->chamber->name . ' Report (' . $floorReport->report_date . ')') ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-mail"></span>
                                Email Export')
                                    , ['action' => 'emailExport', $floorReport->id]
                                    , ['escape' => false, 'class' => 'nav-link', 'target' => '_blank']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>
                                Edit Floor Report')
                                    , ['action' => 'edit', $floorReport->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>
                                List Floor Reports')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>
                                New Floor Report')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('Legislative Session') ?></th>
                    <td><?= $floorReport->has('legislative_session') ?
                            $this->Html->link($floorReport->legislative_session
                                ->name, ['controller' => 'LegislativeSessions',
                                'action' => 'view', $floorReport->legislative_session
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Chamber') ?></th>
                    <td><?= $floorReport->has('chamber') ?
                            $this->Html->link($floorReport->chamber
                                ->name, ['controller' => 'Chambers',
                                'action' => 'view', $floorReport->chamber
                                    ->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Report Url') ?></th>
                    <td><?= h($floorReport->report_url) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Report Date') ?></th>
                    <td><?= h($floorReport->report_date) ?></td>
                </tr>
                <tr <?php echo $floorReport->is_approved == 0 ? 'class="table-danger"' : 'class="table-success"'; ?>>
                    <th scope="row"><?= __('Is Approved') ?></th>
                    <td><?= $this->Enable->display_text($floorReport->is_approved); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($floorReport->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($floorReport->modified) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>

<!-- start Import Logs -->
<?php
$importLog = explode('--', $floorReport->import_log);

// discard logs that have "skipping"
$importLog = array_filter($importLog, function ($log) {
    return strpos($log, 'Skipping') === false;
});
$importLog = array_values($importLog);

preg_match_all('/([A-z0-9]+) at position (\d+)/', $importLog[0], $matches);

$missingRatings = $matches[1] ?? [];

preg_match('/Success: (\d+) bill\(s\) found./', $importLog[0], $matches);
$success = $matches[1] ?? 0;

preg_match('/Error: (\d+) bill\(s\) not found:/', $importLog[0], $matches);
$error = $matches[1] ?? 0;
?>
<div class="Logs related row">
    <div class="col-lg-12">
        <h3><?= __('Import Log') ?></h3>
        <table class="table table-striped table-sm">
            <tr>
                <th scope="col"><?= __('Bills Found') ?></th>
                <td><?= $success ?></td>
            </tr>
            <tr <?php echo $error > 0 ? 'class="table-danger"' : ''; ?>>
                <th scope="col"><?= __('Bills Not Found') ?></th>
                <td><?= $error ?></td>
            </tr>
        </table>
    </div>
    <div class="col-lg-12">
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="additionalConfiguration">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                                aria-expanded="false" aria-controls="collapseOne" type="button">
                            Import Log
                        </button>
                    </h5>
                </div>
                <div id="collapseOne" class="collapse hide" aria-labelledby="additionalConfiguration"
                     data-parent="#accordion">
                    <div class="card-body">
                        <div class="form-group">
                            <?= $floorReport->import_log ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<!-- end Import Logs -->

<!-- start Missing Ratings -->
<?php
$legiscanSessionId = null;
$ratingSession = $floorReport->legislative_session->name;
foreach ($legiscanSessions as $id => $name) {
    if ($ratingSession == $name) {
        $legiscanSessionId = $id;
        break;
    }
}
?>
<?php if (!empty($missingRatings)): ?>
    <div class="Ratings related row">
        <div class="col-lg-12">
            <h3><?= __('Missing Ratings') ?></h3>
            <?php if (!empty($floorReport->ratings)): ?>
                <table class="table table-striped table-sm">
                    <tr>
                        <th scope="col"><?= __('Name') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                    <?php foreach ($missingRatings as $missingRating): ?>
                        <tr>
                            <td><?= $this->LegiscanBill->getBillName($missingRating) ?></td>
                            <td class="actions">
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                    , ['controller' => 'LegiscanBills', 'action' => 'index', '?' => [
                                        'session' => $legiscanSessionId, 'bill' => $missingRating]]
                                    , ['escape' => false, 'target' => '_blank']
                                )
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            <?php else : ?>
                <div class="alert alert-primary" role="alert">
                    No Ratings to display!
                </div>
            <?php endif; ?>
        </div>
        <!-- end col lg 12 -->
    </div>
<?php endif; ?>
<!-- end Missing Ratings -->

<!-- start Found Ratings -->
<div class="Ratings related row">
    <div class="col-lg-12">
        <h3><?= __('Matched Ratings') ?></h3>
        <?php if (!empty($floorReport->ratings)): ?>
            <?=
            $this->Form->postLink(__('Bulk Set Ratings to Public')
                , ['action' => 'bulkSetRatingStatus', $floorReport->id, 1]
                , ['escape' => false, 'confirm' => __('Are you sure you want to bulk set ratings to be public?', $floorReport->id)]
            )
            ?>
            <br/>
            <?=
        $this->Form->postLink(__('Bulk Set Ratings to Private')
            , ['action' => 'bulkSetRatingStatus', $floorReport->id, 0]
            , ['escape' => false, 'confirm' => __('Are you sure you want to bulk set ratings to be private?', $floorReport->id)]
        )
            ?>
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Sort Order') ?></th>
                    <th scope="col"><?= __('Analysis Public') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($floorReport->ratings as $ratings): ?>
                    <tr>
                        <td><?= $this->LegiscanBill->getBillName($ratings->name) ?></td>
                        <td><?= h($ratings['_joinData']['sort_order']) ?></td>
                        <td><?= $this->Enable->display_text($ratings->is_enabled) ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['controller' => 'Ratings', 'action' => 'view', $ratings->id]
                                , ['escape' => false, 'target' => '_blank']
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['controller' => 'Ratings', 'action' => 'edit', $ratings->id]
                                , ['escape' => false, 'target' => '_blank']
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else : ?>
            <div class="alert alert-primary" role="alert">
                No Ratings to display!
            </div>
        <?php endif; ?>
    </div>
    <!-- end col lg 12 -->
</div>
<!-- end Found Ratings -->
