<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FloorReport $floorReport
 */
?>
<div class="floorReports form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= __('Edit Floor Report') ?></h1>
            </div>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Floor Reports
                                ')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link'])
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?= $this->Form->create($floorReport) ?>
            <?php
            echo $this->Form->control('tenant_id', ['type' => 'hidden']);
            ?>
            <div class="form-group">
                <?php
                echo $this->Form->control('legislative_session_id', ['options' => $legislativeSessions, 'class' =>
                    'form-control', 'placeholder' => '', 'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('chamber_id', ['options' => $chambers, 'class' =>
                    'form-control', 'placeholder' => '', 'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('report_date', ['empty' => true, 'class' => 'form-control', 'placeholder' =>
                    '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('report_url', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('import_log', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-check">
                <?php
                echo $this->Form->control('import_bills', ['class' => 'form-check-input', 'type' => 'checkbox']);
                ?>
            </div>
            <div class="form-check">
                <?php
                echo $this->Form->control('is_approved', ['class' => 'form-check-input']);
                ?>
            </div>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            <?php echo $this->Html->link(
                'Cancel',
                ['controller' => 'floorReports', 'action' => 'index', '_full' => true],
                ['class' => 'btn btn-secondary', 'id' => 'cancel']
            ); ?>
            <?= $this->Form->end() ?>
        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
<script type="text/javascript">
    jQuery(document).ready(function () {
        $('#import-log').summernote({
            height: "250px"
        });
    });
</script>
