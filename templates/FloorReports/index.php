<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FloorReport[]|\Cake\Collection\CollectionInterface $floorReports
 */
?>
<div class="floorReports index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Floor Reports') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Floor Report')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('legislative_session_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('chamber_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('report_date') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('is_approved') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($floorReports as $floorReport): ?>
                    <tr>
                        <td>
                            <?= $floorReport->has('legislative_session') ?
                                $this->Html->link($floorReport->legislative_session
                                    ->name, ['controller' => 'LegislativeSessions
                                            ',
                                    'action' => 'view', $floorReport->legislative_session
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= $floorReport->has('chamber') ?
                                $this->Html->link($floorReport->chamber
                                    ->name, ['controller' => 'Chambers
                                            ',
                                    'action' => 'view', $floorReport->chamber
                                        ->id]) : '' ?>
                        </td>
                        <td>
                            <?= h($floorReport->report_date) ?>
                        </td>
                        <td>
                            <?= $this->Enable->display_text($floorReport->is_approved) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-mail"></span>')
                                , ['action' => 'emailExport', $floorReport->id]
                                , ['escape' => false, 'target' => '_blank']
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $floorReport->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $floorReport->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $floorReport->id]
                                , ['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $floorReport->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
