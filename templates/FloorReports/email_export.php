<meta charset="UTF-8">
<meta name="google" content="notranslate">
<meta http-equiv="Content-Language" content="en">
<?php

use Cake\Utility\Text;

function getDomainName($configurations): string
{
    return $configurations['wp_domain_name'] ?? '';
}

function generateUrl($domainName, $sessionSlug, $ratingSlug): string
{
    return sprintf('https://%s/bill/%s/%s',
        strtolower($domainName),
        strtolower($sessionSlug),
        strtolower($ratingSlug));
}

$wordpressDomainName = getDomainName($currentTenantConfigurations);

if (empty($wordpressDomainName)) {
    echo 'No domain name found. Please configure "wp domain name" in the tenant configurations.';
    return;
}

$sessions = array_column($legislativeSessions, null, 'id');
$positions = array_column($positions, null, 'id');

// reorder based on sort_order
$ratings = [];
foreach ($floorReport->ratings as $rating) {
    $ratings[$rating->sort_order] = $rating;
}

foreach ($ratings as $rating) {
    if (!isset($sessions[$rating->legislative_session_id])) {
        echo 'Session not found for rating ID: ' . $rating->name . '<br>';
        continue;
    }

    if (!isset($positions[$rating->position_id])) {
        echo 'Position not found for rating ID: ' . $rating->name . '<br>';
        continue;
    }

    $sessionSlug = $sessions[$rating->legislative_session_id]->legislature_number;
    $positionName = $positions[$rating->position_id]->name;
    $ratingName = $rating->name;
    $ratingSlug = Text::slug($this->LegiscanBill->getBillName($ratingName));
    $publicUrl = generateUrl($wordpressDomainName, $sessionSlug, $ratingSlug);

    echo $this->FloorReport->formattedLink($ratingName, $positionName, $publicUrl);
}
