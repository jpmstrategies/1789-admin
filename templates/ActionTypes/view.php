<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ActionType $actionType
 */
?>
<div class="actionTypes view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= h($actionType->name) ?></h1>
            </div>
        </div>
    </div>
    <?php
    $readOnly = ($actionType->evaluation_code === 'N' ? '' : 'readonly');

    if ($readOnly === 'readonly') :
        ?>
        <div class="alert alert-primary">
            <b>This is a system default that cannot be removed.</b>
            <hr>
            <?php if ($actionType->evaluation_code === 'F') : ?>
                It represents the action type displayed when the legislator's vote matches the organization's configured stance on a rating.
            <?php endif; ?>
            <?php if ($actionType->evaluation_code === 'O') : ?>
                It represents the action type displayed when the legislator's vote does not match the organization's configured stance on a rating.
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-pencil"></span>  Edit Action Type')
                                    , ['action' => 'edit', $actionType->id]
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>  List Action Types')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?=
                                $this->Html->link(__('<span class="octicon octicon-plus"></span>  New Action Type')
                                    , ['action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">

                <tr>
                    <th scope="row"><?= __('Legend Text') ?></th>
                    <td>
                        <?= h($actionType->name) ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Icon Text') ?></th>
                    <td>
                        <?= h($actionType->icon_text) ?>
                        <?php if ($actionType->evaluation_code === 'F') : ?>
                            <i>A check icon will be displayed.</i>
                        <?php endif; ?>
                        <?php if ($actionType->evaluation_code === 'O') : ?>
                            <i>An "x" icon will be displayed.</i>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Css Color') ?></th>
                    <td>
                        <div style="width: 25px; float: left; background-color: <?php echo $actionType->css_color; ?>">
                            &nbsp;
                        </div>
                        <div style="float: left; padding-left: 5px">
                            <?= h($actionType->css_color) ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Score Type') ?></th>
                    <td>
                        <div style="float: left; padding-left: 5px">
                            <?php if ($actionType->score_type === 'N') : ?>
                                <i>Negative Points. If we evaluate a legislators' vote to be incorrect, they will not
                                    receive any points.</i>
                            <?php endif; ?>
                            <?php if ($actionType->score_type === 'P') : ?>
                                <i>Positive Points. If we evaluate a legislators' vote to be correct, they will receive
                                    points.</i>
                            <?php endif; ?>
                            <?php if ($actionType->score_type === 'Z') : ?>
                                <i>No Points. We will ignore the vote as if it never happened.</i>
                            <?php endif; ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Export Text') ?></th>
                    <td>
                        <?= h($actionType->export_text) ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sort Order') ?></th>
                    <td>
                        <?= $this->Number->format($actionType->sort_order) ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($actionType->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($actionType->modified) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>