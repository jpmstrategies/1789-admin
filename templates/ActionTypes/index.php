<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Grade[]|\Cake\Collection\CollectionInterface $grades
 */
?>
<div class="grades index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Action Types') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Action Type')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Action')
                    , ['controller' => 'Actions', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('name', 'Legend Text') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('evaluation_code') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('icon_text') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('export_text') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('css_color') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('score_type') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($actionTypes as $actionType): ?>
                    <tr>
                        <td>
                            <?= h($actionType->name) ?>
                        </td>
                        <td>
                            <?php echo $this->EvaluationCode->display_text($actionType->evaluation_code) ?>
                        </td>
                        <td>
                            <?= h($actionType->icon_text) ?>
                        </td>
                        <td>
                            <?= h($actionType->export_text) ?>
                        </td>
                        <td>
                            <div style="width: 25px; float: left; background-color: <?php echo $actionType->css_color; ?>">
                                &nbsp;
                            </div>
                            <div style="float: left; padding-left: 5px">
                                <?= h($actionType->css_color) ?>
                            </div>
                        </td>
                        <td>
                            <?php echo $this->ScoreType->display_text($actionType->score_type) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $actionType->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?php
                            if (!$actionType->is_calculated) {
                                echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                    , ['action' => 'edit', $actionType->id]
                                    , ['escape' => false]
                                );
                            }
                            ?>
                            <?php
                            if ($actionType->evaluation_code === 'N' && !$actionType->is_calculated) {
                                echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                    , ['action' => 'delete', $actionType->id]
                                    , [
                                        'escape' => false,
                                        'confirm' => __('Are you sure you want to delete # {0}?', $actionType->id)]
                                );
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->



