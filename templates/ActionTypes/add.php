<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ActionType $actionType
 */
?>
<div class="actionTypes form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= __('Add ActionType') ?></h1>
            </div>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Action Types')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link'])
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?= $this->Form->create($actionType) ?>
            <?php
            echo $this->Form->control('tenant_id', ['type' => 'hidden']);
            ?>
            <div class="form-group">
                <?php
                echo $this->Form->control('name', [
                    'class' => 'form-control',
                    'placeholder' => '',
                    'label' => 'Legend Text']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('icon_text', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <div class="small">This is the text that will be displayed on the public web application
                </div>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('export_text', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <div class="small">This is the text that will be displayed on CSV exports after vote evaluation
                </div>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('css_color', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->ScoreType->control('score_type', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <div class="small">This field determines how we calculate scores automatically.
                </div>
            </div>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>
        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->