<div class="records view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Record'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp&nbsp;Edit Record'),
                                    ['action' => 'edit', $record->id],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete Record'),
                                    ['action' => 'delete', $record->id], ['escape' => false, 'class' => 'nav-link'],
                                    __('Are you sure you want to delete # {0}?', $record->id)); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp&nbsp;List Records'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Action'),
                                    ['controller' => 'Actions', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Legislator'),
                                    ['controller' => 'Legislators', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item">
                                <?php
                                if ($currentTenantDataSource === 'legiscan') {
                                    echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating'),
                                        ['controller' => 'LegiscanBills', 'action' => 'index'], ['escape' => false, 'class' => 'nav-link']);
                                } else {
                                    echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Rating'),
                                        ['controller' => 'Ratings', 'action' => 'add'], ['escape' => false, 'class' => 'nav-link']);
                                }
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">
                <tbody>
                <tr>
                    <th><?php echo __('Id'); ?></th>
                    <td>
                        <?php echo h($record['id']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Action'); ?></th>
                    <td>
                        <?php echo $this->Html->link($record->action['name'],
                            ['controller' => 'Actions', 'action' => 'view', $record->action['id']]); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Legislator'); ?></th>
                    <td>
                        <?php echo $this->Html->link($record->legislator['journal_name'],
                            ['controller' => 'Legislators', 'action' => 'view', $record->legislator['id']]); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Rating'); ?></th>
                    <td>
                        <?php echo $this->Html->link($record->rating['name'],
                            ['controller' => 'Ratings', 'action' => 'view', $record->rating['id']]); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Chair'); ?></th>
                    <td>
                        <?php echo $this->Enable->display_text($record['is_chair']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Statement of Intent'); ?></th>
                    <td>
                        <?php echo $this->Enable->display_text($record['has_statement']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td>
                        <?php echo h($record['created']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td>
                        <?php echo h($record['modified']); ?>
                        &nbsp;
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
</div>
