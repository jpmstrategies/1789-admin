<?php

use Cake\Routing\Router;

$this->Html->script('View/Records/bulkedit.js', ['block' => true]);
$this->Html->css('bulkedit.css', ['block' => true]);
?>
<div class="ratings view">
    <div class="row">
        <div class="col-xl-6">
            <div class="page-header">
                <h1><?php echo __('Record Bulk Edit'); ?></h1>
            </div>
        </div>
        <div class="col-xl-6">
            <?php if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes') : ?>
                <?php
                $editCriteriaUrl = Router::url(['controller' => 'rating-criterias', 'action' => 'bulkedit',], true);
                $editCriteriaUrl .= '/' . $selectedFilters['legislative_session']['id'];
                $editCriteriaUrl .= '/' . $selectedFilters['id'];
                ?>
                <a href="<?php echo $editCriteriaUrl; ?>" class="btn btn-success" style="float: right">
                    <span class="octicon octicon-pencil"></span>&nbsp; &nbsp; Edit Criteria Positions
                </a>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <?php echo $this->Form->create(null, ['class' => 'form-horizontal', 'id' => 'FilterAdminBulkeditForm']); ?>
            <input type="hidden" name="_csrfToken" value="<?php echo $_COOKIE['csrfToken']; ?>"/>
            <input type="hidden" name="url"
                   value="<?php echo Router::url(['controller' => 'Records', 'action' => 'bulkedit'], true); ?>">
            <input type="hidden" name="saveUrl"
                   value="<?php echo Router::url(['controller' => 'Records', 'action' => 'api'], true); ?>">
            <div class="row">
                <div class="form-group form-group-sm col-sm-12">
                    <div class="row">
                        <div class="col-sm-3">
                            <select name="session" class="custom-select update-ratings" placeholder="Session">
                                <?php foreach ($sessions as $id => $name): ?>
                                    <option value="<?php echo $id; ?>"<?php echo $id == $selectedFilters['legislative_session']['id'] ? ' selected' : ''; ?>><?php echo $name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select name="chamber" class="custom-select update-ratings" placeholder="Chamber">
                                <?php foreach ($chambers as $id => $name): ?>
                                    <option value="<?php echo $id; ?>"<?php echo $id == $selectedFilters['chamber_id'] ? ' selected' : ''; ?>><?php echo $name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <select name="rating" class="custom-select" placeholder="Rating">
                                <?php foreach ($ratings as $id => $name): ?>
                                    <option value="<?php echo $id; ?>"<?php echo $id == $selectedFilters['id'] ? ' selected' : ''; ?>><?php echo $name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<div class="related row">
    <div class="col-xl-12">
        <?php if (!empty($rows)): ?>
            <?php echo $this->Form->create(null, ['id' => 'RecordAdminBulkeditForm']); ?>
            <table class="table table-striped vcenter table-fixed-header" cellpadding="0" cellspacing="0">
                <thead class="header">
                <tr>
                    <th class="center"><?php echo __('Legislator'); ?></th>
                    <th class="center"><?php echo __('Action'); ?></th>
                    <th class="center"><?php echo __('Statement'); ?></th>
                    <th class="center"><?php echo __('Chair'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($rows as $row): ?>
                    <?php
                    $current_action = $row['Records']['action_id'];
                    $action_description = [];

                    if (array_key_exists($current_action, $actions)) {
                        $action_description = $actions[$current_action];
                    }

                    if (empty($row['RecordId'])) {
                        $row_class = ' danger';
                    } else {
                        if ($action_description === 'N/A') {
                            $row_class = ' warning';

                        } else {
                            $row_class = '';
                        }
                    }
                    ?>
                    <tr>
                        <input type="hidden" name="rating_id" value="<?php echo $row['RatingId']; ?>"/>
                        <input type="hidden" name="id" value="<?php echo $row['RecordId']; ?>"/>
                        <input type="hidden" name="legislator_id" value="<?php echo $row['LegislatorId']; ?>"/>
                        <td class="<?php echo $row_class; ?>"><?php echo $row['Legislators']['journal_name']; ?></td>
                        <td class="action<?php echo $row_class; ?>">
                            <select class="custom-select" name="action_id">
                                <?php if (empty($row['Records']['action_id'])): ?>
                                    <option value="" disabled selected></option>
                                <?php endif; ?>
                                <?php foreach ($actions as $actionId => $actionValue): ?>
                                    <option value="<?php echo $actionId; ?>"<?php echo $actionId == $row['Records']['action_id'] ? ' selected' : ''; ?>><?php echo $actionValue; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td class="center<?php echo $row_class; ?>"><input class="checkbox input-sm" type="checkbox"
                                                                           name="has_statement"
                                                                           value="true"<?php echo $row['Records']['has_statement'] ? ' checked' : ''; ?>>
                        </td>
                        <td class="center<?php echo $row_class; ?>"><input class="radio input-sm" type="radio"
                                                                           name="is_chair"
                                                                           value="true"<?php echo $row['Records']['is_chair'] ? ' checked' : ''; ?>>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->Form->end(); ?>
        <?php endif; ?>
    </div>
    <!-- end col md 12 -->
</div>
