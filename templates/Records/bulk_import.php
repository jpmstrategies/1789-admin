<?php

use Cake\Routing\Router;

$this->Html->script('View/Records/bulk_import.js?v=1.0', ['block' => true]);

$rating_ajax_url = Router::url(['controller' => 'Ratings', 'action' => 'getRatingsAjax']);
$action_ajax_url = Router::url(['controller' => 'Records', 'action' => 'getActionsAjax']);
$bulkedit_url = Router::url(['controller' => 'Records', 'action' => 'bulkedit']);

$selected_session = ($selectedFilters['legislative_session_id'] ?? '');
$selected_chamber = ($selectedFilters['chamber_id'] ?? '');
$selected_rating = ($selectedFilters['id'] ?? '');

?>
<div class="ratings form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Bulk Import Records'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?php echo $this->Form->create(null, ['role' => 'form']); ?>

            <?php echo $this->Form->hidden('rating_ajax_url', ['type' => 'hidden', 'value' => $rating_ajax_url]); ?>
            <?php echo $this->Form->hidden('action_ajax_url', ['type' => 'hidden', 'value' => $action_ajax_url]); ?>
            <?php echo $this->Form->hidden('bulkedit_url', ['type' => 'hidden', 'value' => $bulkedit_url]); ?>
            <?php echo $this->Form->hidden('selected_id', ['type' => 'hidden', 'value' => $selected_rating]); ?>

            <div class="form-group">
                <?php echo $this->Form->control('legislative_session_id', [
                    'class' => 'custom-select',
                    'placeholder' => 'Legislative Session Id',
                    'default' => $selected_session]); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('chamber', [
                    'class' => 'custom-select',
                    'placeholder' => 'Chamber',
                    'default' => $selected_chamber]); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('rating_id',
                    ['class' => 'custom-select', 'placeholder' => 'Rating', 'type' => 'select']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('action_id',
                    ['class' => 'custom-select', 'placeholder' => 'Action', 'type' => 'select']); ?>
                <small class="form-text text-muted">This determines the default Action when the legislator is not
                    present in the vote text.</small>
            </div>
            <div class="form-group">
                <div class="alert alert-secondary" role="alert">
                    Instructions for the <strong>Record Vote Import</strong> field
                    <hr/>
                    <?php if ($currentTenantState === 'IN') : ?>
                        Visit the official <a href="http://iga.in.gov/legislative/2021/bills/" target="_blank">Indiana
                            General
                            Assembly</a>
                        website. After finding the appropriate Roll Call vote, enter the "Roll Call #" in the textarea below.
                        <br/><br/>
                        For example, "Roll Call #117 (House)", you would input "117" below.
                    <?php elseif ($currentTenantState === 'PA') : ?>
                        Visit the official <a href="https://www.legis.state.pa.us/" target="_blank">Pennsylvania
                            General
                            Assembly</a>
                        website. After finding the appropriate Roll Call vote, enter the "RCS No." in the textarea below.
                        <br/><br/>
                        For example, "House RCS No. 42", you would input "42" below.
                    <?php elseif ($currentTenantState === 'TX') : ?>
                        Visit the <a href="https://lrl.texas.gov/collections/journals/journals.cfm" target="_blank">Legislative
                            Reference Library of Texas</a>
                        website and pick a PDF version of a journal for a chamber and date. After finding the appropriate Roll Call vote, copy the vote block into the textarea below.
                        <br/><br/>
                        Do not change any of the content as it may look malformed. For example,
                        <strong>ii</strong> shows up in the Senate and
                        <strong>´ or ˜</strong> for names with special characters.
                    <?php endif ?>
                </div>
                <?php
                echo $this->Form->control('record_vote_text', [
                    'class' => 'form-control',
                    'type' => 'textarea',
                    'escape' => true,
                    'label' => 'Record Vote Import',
                    'rows' => 22]);
                ?>

            </div>
            <div class="form-group">
                <?php echo $this->Form->submit(__('Submit'), ['class' => 'btn btn-primary']); ?>
            </div>
            <?php echo $this->Html->link(__('Skip to Bulk Edit'),
                ['controller' => 'Records', 'action' => 'bulkedit', $id],
                ['escape' => false, 'id' => 'BulkEditLink']); ?>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>




