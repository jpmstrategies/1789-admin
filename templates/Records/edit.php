<?php
// Import 3rd party libraries
?>
<div class="records form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Edit Record'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete'),
                                    ['action' => 'delete', $record->id],
                                    [
                                        'escape' => false,
                                        'class' => 'nav-link'], __('Are you sure you want to delete # {0}?',
                                        $record->id)); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Records'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($record, ['role' => 'form']); ?>

            <div class="form-group">
                <?php echo $this->Form->control('id', ['class' => 'form-control', 'placeholder' => 'Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('action_id',
                    ['class' => 'form-control', 'placeholder' => 'Action Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('legislator_id',
                    ['class' => 'form-control', 'placeholder' => 'Legislator Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('rating_id',
                    ['class' => 'form-control', 'placeholder' => 'Rating Id']); ?>
            </div>
            <div class="form-check">
                <?php echo $this->Form->control('is_chair', [
                    'label' => 'Chair during Record Vote',
                    'class' => 'form-check-input']); ?>
            </div>
            <div class="form-check">
                <?php echo $this->Form->control('has_statement', [
                    'label' => 'Statement of Intent',
                    'class' => 'form-check-input']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->submit(__('Submit'), ['class' => 'btn btn-primary']); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#legislator-id").select2({
            theme: "bootstrap4",
            placeholder: "- select -",
            allowClear: true,
            width: "100%"
        });

        jQuery("#rating-id").select2({
            theme: "bootstrap4",
            placeholder: "- select -",
            allowClear: true,
            width: "100%"
        });

    });
</script>
