<div class="records index">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Records'); ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp;&nbsp;Bulk Edit Records'),
                    ['action' => 'bulkedit'], ['escape' => false, 'class' => 'nav-link']); ?></li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('action_id'); ?></th>
                    <th><?php echo $this->Paginator->sort('legislator_id'); ?></th>
                    <th><?php echo $this->Paginator->sort('rating_id'); ?></th>
                    <th><?php echo $this->Paginator->sort('is_chair', 'Chair'); ?></th>
                    <th><?php echo $this->Paginator->sort('has_statement', 'Statement'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($records as $record): ?>
                    <tr>
                        <td>
                            <?php echo $this->Html->link($record->action['description'],
                                ['controller' => 'Actions', 'action' => 'view', $record->action['id']]); ?>
                        </td>
                        <td>
                            <?php echo $this->Html->link($record->legislator['journal_name'], [
                                'controller' => 'legislators',
                                'action' => 'view',
                                $record->legislator['id']]); ?>
                        </td>
                        <td>
                            <?php echo $this->Html->link($this->Text->truncate($record->rating['name'], 40),
                                ['controller' => 'Ratings', 'action' => 'view', $record->rating['id']]); ?>
                        </td>
                        <td><?php echo $this->Enable->display_text($record->is_chair); ?>&nbsp;</td>
                        <td><?php echo $this->Enable->display_text($record->has_statement); ?>&nbsp;</td>
                        <td class="actions">
                            <?php echo $this->Html->link('<span class="octicon octicon-search"></span>',
                                ['action' => 'view', $record['id']], ['escape' => false]); ?>
                            <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                                ['action' => 'edit', $record['id']], ['escape' => false]); ?>
                            <?php echo $this->Form->postLink('<span class="octicon octicon-trashcan"></span>',
                                ['action' => 'delete', $record['id']], ['escape' => false],
                                __('Are you sure you want to delete # {0}?', $record['id'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <p>
                <small><?php ?></small>
            </p>

            <?php
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div><!-- end containing of content -->
