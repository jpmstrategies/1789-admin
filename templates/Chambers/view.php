<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Chamber $chamber
 */
?>
<div class="chambers view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Chamber'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li><?= $this->Html->link(__('<span class="octicon octicon-pencil"></span>  Edit Chamber'), [
                                    'action' => 'edit',
                                    $chamber->id], ['escape' => false, 'class' => 'nav-link']) ?> </li>
                            <li><?= $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>  List Chambers'), ['action' => 'index'], [
                                    'escape' => false,
                                    'class' => 'nav-link']) ?> </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= h($chamber->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Type') ?></th>
                    <td><?php echo $this->ChamberType->display_text($chamber->type) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Name') ?></th>
                    <td><?= h($chamber->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Title Short') ?></th>
                    <td><?= h($chamber->title_short) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Title Long') ?></th>
                    <td><?= h($chamber->title_long) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Presiding Legislator Short') ?></th>
                    <td><?= h($chamber->presiding_legislator_short) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Presiding Legislator Long') ?></th>
                    <td><?= h($chamber->presiding_legislator_long) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Slug') ?></th>
                    <td><?= h($chamber->slug) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('LegiScan ID') ?></th>
                    <td><?= h($chamber->legiscan_chamber_id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Enabled') ?></th>
                    <td><?= $chamber->is_enabled ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Default') ?></th>
                    <td><?= $chamber->is_default ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Sort Order') ?></th>
                    <td><?= h($chamber->sort_order) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($chamber->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($chamber->modified) ?></td>
                </tr>
            </table>

        </div>
        <!-- end col md 9 -->
    </div>
</div>