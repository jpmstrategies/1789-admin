<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Chamber[]|\Cake\Collection\CollectionInterface $chambers
 */
?>
<div class="chambers index">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Chambers'); ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?= $this->Paginator->sort('type') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('is_enabled', 'Enabled') ?></th>
                    <th><?= $this->Paginator->sort('is_default', 'Default') ?></th>
                    <th><?= $this->Paginator->sort('sort_order', 'Sort Order') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($chambers as $chamber): ?>
                    <tr>
                        <td><?php echo $this->ChamberType->display_text($chamber->type) ?></td>
                        <td><?= h($chamber->name) ?></td>
                        <td><?php echo $this->Enable->display_text($chamber->is_enabled) ?></td>
                        <td><?php echo $this->Enable->display_text($chamber->is_default) ?></td>
                        <td><?= h($chamber->sort_order) ?></td>
                        <td class="actions">
                            <?php echo $this->Html->link('<span class="octicon octicon-search"></span>',
                                ['action' => 'view', $chamber['id']], ['escape' => false]); ?>
                            <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                                ['action' => 'edit', $chamber['id']], ['escape' => false]); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
    </div>
</div>
