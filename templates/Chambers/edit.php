<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Chamber $chamber
 */
?>
<div class="addresses form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Edit Address'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li><?= $this->Html->link(__('<span class="octicon octicon-pencil"></span>  Edit Chamber'), [
                                    'action' => 'edit',
                                    $chamber->id], ['escape' => false, 'class' => 'nav-link']) ?> </li>
                            <li><?= $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>  List Chambers'), ['action' => 'index'], [
                                    'escape' => false,
                                    'class' => 'nav-link']) ?> </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?= $this->Form->create($chamber) ?>
            <?php
            echo $this->ChamberType->control('type',
                [
                    'disabled' => 'disabled',
                    'class' => 'form-control',
                    'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]
            );
            echo $this->Form->control('name',
                [
                    'class' => 'form-control',
                    'placeholder' => 'Name',
                    'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]);
            echo $this->Form->control('title_short',
                [
                    'class' => 'form-control',
                    'placeholder' => 'Title (Short)',
                    'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]
            );
            echo $this->Form->control('title_long',
                [
                    'class' => 'form-control',
                    'placeholder' => 'Title (Long)',
                    'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]
            );
            echo $this->Form->control('presiding_legislator_short',
                [
                    'class' => 'form-control',
                    'label' => 'Presiding Officer Title (Short)',
                    'placeholder' => 'Presiding Officer Title (Short)',
                    'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]
            );
            echo $this->Form->control('presiding_legislator_long',
                [
                    'class' => 'form-control',
                    'label' => 'Presiding Officer Title (Long)',
                    'placeholder' => 'Presiding Officer Title (Long)',
                    'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]
            );
            echo $this->Form->control('slug',
                [
                    'class' => 'form-control',
                    'placeholder' => 'Slug',
                    'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']]
            );
            echo $this->Form->control('is_enabled',
                [
                    'class' => 'form-check-input',
                    'label' => 'Enabled',
                    'templates' => ['inputContainer' => '<div class="form-check">{{content}}</div>']]
            );
            echo $this->Form->control('is_default',
                [
                    'class' => 'form-check-input',
                    'label' => 'Default',
                    'templates' => ['inputContainer' => '<div class="form-check">{{content}}</div>']]
            );
            ?>
            <div class="form-group">
                <?php
                echo $this->Form->control('sort_order', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <div class="small">Set to the desired position, and the other chambers will be automatically
                    re-numbered when a conflict occurs.
                </div>
            </div>
            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>
        </div>
        <!-- end col md 9 -->
    </div>
</div>
