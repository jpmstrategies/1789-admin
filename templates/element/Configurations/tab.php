<?php

if ($configuration_type->tab === $target_tab) :

    $data_type = $configuration_type->data_type;
    $div_class = 'form-group';

    $default_value = $configuration_type->default_value;
    $is_editable = $configuration_type->is_editable;
    $readonly = ($is_editable || $is_super ? '' : 'readonly');

    $changed_class = '';
    $btn_reset = 'style="display:none"';
    if ($default_value != $input_value) {
        $changed_class = ' default-changed';
        $btn_reset = ($is_editable || $is_super ? '' : 'style="display:none"');

        if ($data_type === 'checkbox') {
            $changed_class = ' default-changed alert-warning';
        }
    }

    if ($data_type === 'checkbox') {
        $div_class = 'form-check';
        $btn_reset = 'style="display:none"';
    }

    if (isset($ajax_save) && $ajax_save === true) {
        echo '<div class="row field' . $changed_class . '">';
        echo '<div class="col-2">';
        echo str_replace('_', ' ', (string)$configuration_type->name);
        echo '</div>';

        echo '<div class="col-8">';

        echo $this->Form->control('id', ['type' => 'hidden', 'value' => $configuration_id]);
        echo $this->Form->control('data_type', ['type' => 'hidden', 'value' => $data_type]);
        echo $this->Form->control('default_value', ['type' => 'hidden', 'value' => $default_value]);
        echo $this->Form->control('current_value', ['type' => 'hidden', 'value' => $input_value]);
    }

    echo '<div class="' . $div_class . (isset($error) && $error ? ' error' : '') . '">';

    // check if configuration_options
    if ($data_type === 'select' && isset($configuration_options)) {
        echo $this->Form->control($field_name, [
            'data-id' => $data_id,
            'label' => $label_value,
            'class' => 'custom-select',
            'type' => 'select',
            'value' => $input_value,
            'options' => $configuration_options,
            'templates' => [
                'inputContainer' => '{{content}}'
            ],
            'required' => $configuration_type->is_required,
        ]);
    } else {
        if ($data_type === 'checkbox') {
            echo $this->Form->control($field_name, [
                'data-id' => $data_id,
                'label' => ($description !== '' ? $description : false),
                'class' => 'form-check-input',
                'type' => 'checkbox',
                'checked' => ($input_value === 'yes' ? 'checked' : ''),
                'readonly' => $readonly,
                'templates' => [
                    'inputContainer' => '{{content}}'
                ],
                'required' => $configuration_type->is_required,
            ]);
        } else {
            echo $this->Form->control($field_name, [
                'data-id' => $data_id,
                'label' => $label_value,
                'class' => 'form-control',
                'type' => $configuration_type->data_type,
                'value' => $input_value,
                'readonly' => $readonly,
                'templates' => [
                    'inputContainer' => '{{content}}'
                ],
                'required' => $configuration_type->is_required,
                'title' => $configuration_type->required_message,
                'pattern' => $configuration_type->required_regex
            ]);
        }
    }

    echo '<small class="form-text text-muted">' . $description . '</small>';

    if (isset($error) && $error) {
        $error_message = $configuration_type->required_message ?? 'This field is required.';
        echo '<div class="error-message" id="password-confirm-error">' . $error_message . '</div>';
    }

    echo '</div>';
    ?>
    <?php if (isset($ajax_save) && $ajax_save === true) : ?>
    </div>
    <div class="col-2">
        <div class="btn-changes" style="display:none">
            <button type="button" class="btn btn-success" id="submit-button"><span
                        class="octicon octicon-check"></span></button>&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" id="cancel-button"><span
                        class="octicon octicon-x"></span></button>
        </div>
        <div class="btn-reset" <?php echo $btn_reset; ?>>
            <button type="button" class="btn btn-outline-secondary" id="reset-default-button"><span
                        class="octicon octicon-history"></span>&nbsp;&nbsp;Reset
            </button>
        </div>
    </div>
    </div>
<?php
endif; //end ajax_save
endif; //end tab check
?>
