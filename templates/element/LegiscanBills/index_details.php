<?php if (!empty($bills)): ?>
    <?php echo $this->Form->create($bills, ['class' => 'form-bulkedit']); ?>
    <table id="DetailsTable" class="table table-striped vcenter table-fixed-header">
        <thead class="header">
        <tr>
            <th class="center" style="width: 175px"><?php echo __('Bill'); ?></th>
            <th class="center"><?php echo __('Details'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php $previousBillType = []; ?>
        <?php
        $row = 0;
        foreach ($bills as $bill): ?>
            <tr>
                <?php echo $this->Form->control('id' . '-details-' . $row, [
                    'name' => 'legiscan_bill_id',
                    'label' => false,
                    'class' => '',
                    'type' => 'hidden',
                    'default' => $bill['id']]); ?>
                <td style="vertical-align: top">
                    <h2><?php echo $this->LegiscanBill->getBillName($bill['bill_number']) ?></h2>
                    <a href="<?php echo $bill['legiscan_url']; ?>" target="_blank">
                        <span class="octicon octicon-link-external"></span> LegiScan
                    </a>
                    <br/>
                    <a href="<?php echo $bill['state_url']; ?>" target="_blank">
                        <span class="octicon octicon-link-external"></span> State
                    </a>
                    <hr/>
                    <?php
                    if ($bill['has_bill_vote_details'] > 0) {
                        echo $this->Html->link('<span class="octicon octicon-tasklist"></span> View Votes',
                            ['controller' => 'LegiscanBills', 'action' => 'view', $bill['id']], [
                                'escape' => false,
                                'target' => '_blank']);
                    } else {
                        echo 'No LegiScan Votes';
                    }
                    ?>
                    <?php
                    if ($currentTenantType === 'F') :
                        ?>
                        <hr/>
                        <?php if (isset($bill['Ratings']['id'])) : ?>
                        <?php echo $this->Html->link(__('<span class="octicon octicon-search"></span> Review'),
                            [
                                'controller' => 'ratings',
                                'action' => 'view',
                                $bill['Ratings']['id']],
                            ['escape' => false, 'class' => 'btn btn-info score-vote', 'target' => '_blank']);
                        ?>
                    <?php else : ?>
                        <?php if ($bill['id'] > 0) : ?>
                            <?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span> Track'),
                                [
                                    'controller' => 'ratings',
                                    'action' => 'add',
                                    '?' =>
                                        ['legiscan_bill_id' => $bill['id']]],
                                ['escape' => false, 'class' => 'btn btn-success score-vote', 'target' => '_blank']);
                            ?>
                        <?php else : ?>
                            <button type="button" class="btn btn-secondary" disabled>
                                <span class="octicon octicon-plus"></span> Score Vote
                            </button>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php endif; ?>
                </td>
                <td>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <p>
                                    <?php
                                    if (isset($selectedFilters['title'])) {
                                        echo preg_replace('/\b(' . $selectedFilters['title'] . ')\b/i', '<mark>$1</mark>', (string)$bill['title']);
                                    } else {
                                        echo $bill['title'];
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>

                        <?php if (sizeof($bill['legiscan_bill_subjects']) > 0) : ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php
                                    foreach ($bill['legiscan_bill_subjects'] as $billSubject) {
                                        if (isset($selectedFilters['subject'])) {
                                            if (str_contains(strtolower((string)$billSubject['subject_name']), strtolower((string)$selectedFilters['subject']))) {
                                                echo '<div class="badge badge-success">' . $billSubject['subject_name'] . ' </div> ';
                                            } else {
                                                echo '<div class="badge badge-light">' . $billSubject['subject_name'] . ' </div> ';
                                            }
                                        } else {
                                            echo '<div class="badge badge-info">' . $billSubject['subject_name'] . ' </div> ';
                                        }
                                    }
                                    ?>
                                    <hr/>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="row form-group">
                            <div class="col-sm-6">
                                <?php
                                $default_status = 'PENDING';
                                if (isset($bill['tenants_bills'][0])) {
                                    $default_status = $bill['tenants_bills'][0]['status'];
                                }
                                echo $this->BillTrackingStatus->control('tracking' . '-' . $row, [
                                    'name' => 'tracking',
                                    'label' => false,
                                    'class' => 'custom-select',
                                    'default' => $default_status,
                                    'templates' => [
                                        'inputContainer' => '<div class="input {{type}}{{required}}">{{content}}</div>'
                                    ]]);
                                ?>
                            </div>
                            <div class="col-sm-6">
                                <?php
                                if (sizeof($priorities->toArray()) > 0) {
                                    $default_priority_id = null;
                                    if (isset($bill['tenants_bills'][0])) {
                                        $default_priority_id = $bill['tenants_bills'][0]['priority_id'];
                                    }
                                    echo $this->Form->control('priority_id' . '-' . $row, [
                                        'name' => 'priority_id',
                                        'label' => false,
                                        'class' => 'custom-select',
                                        'default' => $default_priority_id,
                                        'empty' => '- Select Priority -',
                                        'templates' => [
                                            'inputContainer' => '<div class="input {{type}}{{required}}">{{content}}</div>'
                                        ],
                                        'options' => $priorities]);
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-12">
                                <?php
                                $default_notes = '';
                                if (isset($bill['tenants_bills'][0])) {
                                    $default_notes = $bill['tenants_bills'][0]['notes'];
                                }
                                echo $this->BillTrackingStatus->control('notes' . '-' . $row, [
                                    'name' => 'notes',
                                    'type' => 'textarea',
                                    'rows' => 2,
                                    'label' => false,
                                    'placeholder' => 'Notes',
                                    'class' => 'form-control',
                                    'default' => $default_notes,
                                    'templates' => [
                                        'inputContainer' => '<div class="input {{type}}{{required}}">{{content}}</div>'
                                    ]]);
                                ?>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <?php $row++; endforeach; ?>
        </tbody>
    </table>

    <?php
    // Pagination element
    $params = $this->Paginator->params();
    if ($params['pageCount'] > 1) {
        ?>
        <?php echo $this->element('paging'); ?>
    <?php } ?>

    <?php echo $this->Form->end(); ?>
<?php endif; ?>