<nav aria-label="Page navigation">
    <ul class="pagination justify-content-center">
        <?php
        $this->Paginator->setTemplates([
            'prevActive' => '<li class="page-item"><a class="page-link" aria-label="Previous" href="{{url}}">{{text}}</a></li>',
            'prevDisabled' => '<li class="page-item disabled"><a class="page-link text-muted" aria-label="Previous"><span aria-hidden="true">{{text}}</span></a></li>',
        ]);
        ?>
        <?= $this->Paginator->prev('Previous') ?>
        <?php
        $this->Paginator->setTemplates([
            'first' => '<li class="page-item"><a class="page-link" aria-label="First" href="{{url}}">{{text}}</a></li>',
            'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
            'current' => '<li class="page-item active"><a class="page-link" href="#">{{text}}</a></li>',
            'last' => '<li class="page-item"><a class="page-link" aria-label="Last" href="{{url}}">{{text}}</a></li>',
            'ellipsis' => '<li class="page-item disabled"><a class="page-link" aria-label="Ellipsis" href="{{url}}">...</a></li>',
        ]);
        ?>
        <?= $this->Paginator->numbers(['first' => 2, 'last' => 2]) ?>
        <?php
        $this->Paginator->setTemplates([
            'nextActive' => '<li class="page-item"><a class="page-link" aria-label="Next" href="{{url}}">{{text}}</a></li>',
            'nextDisabled' => '<li class="page-item disabled"><a class="page-link text-muted" aria-label="Next"><span aria-hidden="true">{{text}}</span></a></li>',
        ]);
        ?>
        <?= $this->Paginator->next('Next') ?>
    </ul>
</nav>
