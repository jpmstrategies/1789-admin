<?php
foreach ($userFields as $userField) {
    echo $this->element('Configurations/tab', [
        "configuration_type" => $userField,
        "data_id" => 'ct-' . $userField['id'],
        "field_name" => 'ct[' . $userField['name'] . ']',
        "label_value" => $userField->description,
        "input_value" => $userDetails['decoded'][$userField['name']] ?? null,
        "configuration_options" => $userFieldOptions[$userField->id] ?? [],
        "target_tab" => 'user_details',
        "description" => null,
        "ajax_save" => false,
        "error" => in_array($userField['name'], $userDetails['validation_failed']) ?? 0
    ]);
}
?>

<script type="text/javascript">
    jQuery(document).ready(function () {
        <?php
        foreach ($userFields as $userField) {
            if (isset($userField->field_mask)) {
                echo 'jQuery("[data-id=ct-' . $userField->id . ']").mask("' . $userField->field_mask . '");';
            }
        }
        ?>
    });
</script>
