<?php if (!empty($calculatedScores['partyVotingSummary'])): ?>
    <h2>Party Breakdown</h2>
    <table id="MetricsTable" class="table table-striped vcenter">
        <thead class="header">
        <tr>
            <th class="center"><?php echo __('Party'); ?></th>
            <th class="center"><?php echo __('Total Scores'); ?></th>
            <th class="center"><?php echo __('# Legislators'); ?></th>
            <th class="center"><?php echo __('Average'); ?></th>
            <th class="center"><?php echo __('Grade'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($calculatedScores['partyVotingSummary'] as $party => $partyDetails): ?>
            <tr>
                <td><?php echo $party; ?></td>
                <td class="center"><?php echo $this->Number->format($partyDetails['sum']); ?></td>
                <td class="center"><?php echo $this->Number->format($partyDetails['count']); ?></td>
                <td class="center"><?php echo $this->Number->precision($partyDetails['average'], 2); ?></td>
                <td class="center"><?php echo $partyDetails['grade']['name'] ?? 'N/A'; ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <div class="alert alert-secondary" role="alert">
        No score metrics to display just yet. Add some ratings and check back!
    </div>
<?php endif; ?>