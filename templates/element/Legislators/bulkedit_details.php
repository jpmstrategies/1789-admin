<?php if (!empty($legislators)): ?>
    <?php echo $this->Form->create($legislators, ['class' => 'form-bulkedit']); ?>
    <table id="DetailsTable" class="table table-striped vcenter table-fixed-header">
        <thead class="header">
        <tr>
            <th class="center"><?php echo __('Name'); ?></th>
            <th class="center"><?php echo __('Party'); ?></th>
            <th class="center"><?php echo __('District'); ?></th>
            <th class="center"><?php echo __('Journal Name'); ?></th>
            <th class="center"><?php echo __('Enabled'); ?></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $row = 0;
        foreach ($legislators as $legislator): ?>
            <tr>
                <?php echo $this->Form->control('id' . '-details-' . $row, [
                    'name' => 'id',
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'default' => $legislator['id']]); ?>
                <?php echo $this->Form->control('legislative_session_id' . '-' . $row, [
                    'name' => 'legislative_session_id',
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'default' => $legislator['legislative_session_id']]); ?>
                <?php echo $this->Form->control('person_id' . '-' . $row, [
                    'name' => 'person_id',
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'default' => $legislator['person_id']]); ?>
                <?php echo $this->Form->control('chamber_id' . '-' . $row, [
                    'name' => 'chamber_id',
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'value' => $legislator['chamber_id']]); ?>
                <td><?php echo sprintf('%s, %s', $legislator->person['last_name'], $legislator->person['first_name']) ?></td>
                <td><?php echo $this->Form->control('party_id' . '-' . $row, [
                        'name' => 'party_id',
                        'label' => false,
                        'class' => 'custom-select',
                        'default' => $legislator['party_id'],
                        'options' => $parties]); ?>
                </td>
                <td><?php echo $this->Form->control('district' . '-' . $row, [
                        'name' => 'district',
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'text',
                        'default' => $legislator['district']]); ?></td>
                <td><?php echo $this->Form->control('journal_name' . '-' . $row, [
                        'name' => 'journal_name',
                        'label' => false,
                        'class' => 'form-control form-control-sm',
                        'type' => 'text',
                        'default' => $legislator['journal_name']]); ?></td>
                <td class="center">
                    <?php echo $this->Form->control('is_enabled' . '-' . $row, [
                        'name' => 'is_enabled',
                        'label' => false,
                        'class' => 'checkbox form-control-sm',
                        'type' => 'checkbox',
                        'default' => $legislator['is_enabled']]); ?>
                </td>
                <td class="center"><?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                        ['action' => 'edit', $legislator['id']],
                        ['escape' => false]); ?></td>
            </tr>
            <?php $row++; endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>