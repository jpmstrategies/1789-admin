<?php

use Cake\Routing\Router;

$criteria = $calculatedScores['use_criteria']['criteria'];
function getCalculatedCriteriaScore($calculatedScores, $legislatorOrPersonId, $criteriaId)
{
    if (isset($calculatedScores['use_criteria']['scores'][$legislatorOrPersonId][$criteriaId])) {
        $score = $calculatedScores['use_criteria']['scores'][$legislatorOrPersonId][$criteriaId]['score'];
        $partyMatch = $calculatedScores['use_criteria']['scores'][$legislatorOrPersonId][$criteriaId]['partyMatch'];
        if ($partyMatch === 'false') {
            return 'N/A';
        } else {
            return $score;
        }
    } else {
        return 'N/A';
    }
}

?>
<?php if (!empty($legislators)): ?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="<?php echo $exportURL; ?>" class="nav-link"><span
                            class="octicon octicon-desktop-download"></span>&nbsp;&nbsp;CSV Export</a></li>
        </ul>
    </nav>
    <table id="CriteriaTable" class="table table-striped vcenter">
        <thead class="header">
        <tr>
            <th class="center"><?php echo __('Name'); ?></th>
            <?php
            foreach ($criteria as $criterion) {
                echo '<th class="center">' . $criterion['name'] . '</th>';
            }
            ?>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $row = 0;
        foreach ($legislators as $legislator): ?>
            <tr>
                <td>
                    <?php echo sprintf('%s, %s', $legislator->person['last_name'], $legislator->person['first_name']); ?>
                </td>
                <?php
                foreach ($criteria as $criterion) {
                    echo '<td class="center">' . getCalculatedCriteriaScore($calculatedScores, $legislator['id'], $criterion['id']) . '</td>';
                }
                ?>
                <td class="center">
                    <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                        ['action' => 'edit', $legislator['id']], ['escape' => false]); ?>
                </td>
            </tr>
            <?php $row++; endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
