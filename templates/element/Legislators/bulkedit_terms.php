<?php
$title_options = $titles->toArray();
$title_link = $this->Url->build('/titles', ['fullBase' => true]);
?>
<?php if (!empty($legislators)): ?>
    <?php if (sizeof($title_options) === 0) : ?>
        <div class="alert alert-secondary" role="alert">
            Optional Feature: No titles are configured for this chamber. These can be configured by clicking
            <?php echo $this->Html->link(__('here'),
                ['controller' => 'Titles', 'action' => 'index']); ?>.
        </div>
    <?php endif; ?>
    <?php echo $this->Form->create($legislators, ['class' => 'form-bulkedit']); ?>
    <table id="DetailsTable" class="table table-striped vcenter table-fixed-header">
        <thead class="header">
        <tr>
            <th class="center"><?php echo __('Name'); ?></th>
            <?php if (sizeof($title_options) > 0) : ?>
                <th class="center"><?php echo __('Title'); ?></th>
            <?php endif; ?>
            <th class="center"><?php echo __('Term Starts'); ?></th>
            <th class="center"><?php echo __('Term Ends'); ?></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $row = 0;
        foreach ($legislators as $legislator): ?>
            <tr>
                <?php echo $this->Form->control('id' . '-details-' . $row, [
                    'name' => 'id',
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'default' => $legislator['id']]); ?>
                <?php echo $this->Form->control('legislative_session_id' . '-' . $row, [
                    'name' => 'legislative_session_id',
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'default' => $legislator['legislative_session_id']]); ?>
                <?php echo $this->Form->control('person_id' . '-' . $row, [
                    'name' => 'person_id',
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'default' => $legislator['person_id']]); ?>
                <?php echo $this->Form->control('chamber_id' . '-' . $row, [
                    'name' => 'chamber_id',
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'value' => $legislator['chamber_id']]); ?>
                <td><?php echo sprintf('%s, %s', $legislator->person['last_name'], $legislator->person['first_name']) ?></td>
                <?php if (sizeof($title_options) > 0) : ?>
                    <td><?php
                        echo $this->Form->control('title_id' . '-' . $row, [
                            'name' => 'title_id',
                            'label' => false,
                            'class' => 'custom-select',
                            'default' => $legislator['title_id'],
                            'empty' => true,
                            'options' => $title_options]);
                        ?>
                    </td>
                <?php endif; ?>
                <td><?php echo $this->Form->control('term_starts' . '-' . $row, [
                        'name' => 'term_starts',
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'date',
                        'default' => $legislator['term_starts']]); ?>
                </td>
                <td><?php echo $this->Form->control('term_ends' . '-' . $row, [
                        'name' => 'term_ends',
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'date',
                        'default' => $legislator['term_ends']]); ?>
                </td>
                <td class="center"><?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                        ['action' => 'edit', $legislator['id']],
                        ['escape' => false]); ?></td>
            </tr>
            <?php $row++; endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>
