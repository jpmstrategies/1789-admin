<?php
function getCalculatedScore($calculatedScores, $legislatorId, $attribute)
{
    if (isset($calculatedScores['scores'][$legislatorId][$attribute])) {
        return $calculatedScores['scores'][$legislatorId][$attribute];
    } else {
        return 'N/A';
    }
}

?>
<?php if (!empty($legislators)): ?>
    <div class="alert alert-secondary" role="alert">
        <strong>Override Text</strong> will replace both the <i>Score</i> and <i>Grade</i> display. For example, some
        scorecards prefer to show "Speaker" instead of a zero score. Some legislators also serve only a partial
        term, so "N/A" might make more sense than a score of partial votes.
    </div>
    <?php echo $this->Form->create($legislators, ['class' => 'form-bulkedit']); ?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="<?php echo $exportURL; ?>" class="nav-link"><span
                            class="octicon octicon-desktop-download"></span>&nbsp;&nbsp;CSV Export</a></li>
        </ul>
    </nav>
    <table id="ScoresTable" class="table table-striped vcenter">
        <thead class="header">
        <tr>
            <th class="center"><?php echo __('Name'); ?></th>
            <th class="center"><?php echo __('Calculated Score'); ?></th>
            <th class="center"><?php echo __('Override Score'); ?></th>
            <th class="center"><?php echo __('Calculated Grade'); ?></th>
            <th class="center"><?php echo __('Override Grade'); ?></th>
            <th class="center"><?php echo __('Override Text'); ?></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $row = 0;
        foreach ($legislators as $legislator): ?>
            <tr>
                <?php echo $this->Form->control('id' . '-scores-' . $row, [
                    'name' => 'id',
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'default' => $legislator['id']]); ?>
                <?php echo $this->Form->control('legislative_session_id' . '-' . $row, [
                    'name' => 'legislative_session_id',
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'default' => $legislator['legislative_session_id']]); ?>
                <?php echo $this->Form->control('person_id' . '-' . $row, [
                    'name' => 'person_id',
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'default' => $legislator['person_id']]); ?>
                <?php echo $this->Form->control('chamber_id' . '-' . $row, [
                    'name' => 'chamber_id',
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'value' => $legislator['chamber_id']]); ?>
                <td><?php echo sprintf('%s, %s', $legislator->person['last_name'], $legislator->person['first_name']); ?></td>
                <td style="text-align: center">
                    <?php echo getCalculatedScore($calculatedScores, $legislator['id'], 'score'); ?>
                </td>
                <td><?php echo $this->Form->control('grade_number' . '-' . $row, [
                        'name' => 'grade_number',
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'text',
                        'default' => $legislator['grade_number']]); ?></td>
                <td style="text-align: center">
                    <?php echo getCalculatedScore($calculatedScores, $legislator['id'], 'grade'); ?>
                </td>
                <td>
                    <?php
                    if (sizeof($grades->toArray()) > 0) {
                        $type = '';
                        $empty = true;
                    } else {
                        $type = 'hidden';
                        $empty = false;
                    }
                    echo $this->Form->control('grade_id' . '-' . $row, [
                        'name' => 'grade_id',
                        'label' => false,
                        'class' => 'custom-select',
                        'default' => $legislator['grade_id'],
                        'type' => $type,
                        'empty' => $empty,
                        'options' => $grades]);
                    ?>
                </td>
                <td><?php echo $this->Form->control('override_text' . '-' . $row, [
                        'name' => 'override_text',
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'text',
                        'default' => $legislator['override_text']]); ?></td>
                <td class="center"><?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                        ['action' => 'edit', $legislator['id']],
                        ['escape' => false]); ?></td>
            </tr>
            <?php $row++; endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>
