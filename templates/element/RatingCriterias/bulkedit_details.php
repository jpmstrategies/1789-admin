<?php
$criteriaList = $criterias->toArray();
$criteriaCount = (is_countable($criteriaList) ? count($criteriaList) : 0);
?>
<?php if (!empty($ratings)): ?>
    <?php echo $this->Form->create($ratings, ['class' => 'form-bulkedit']); ?>
    <table id="<?php echo $tabRatingType['name']; ?>-table" class="table vcenter table-fixed-header">
        <thead class="header">
        <tr>
            <th class="center"><?php echo __('Legiscan Bill'); ?></th>
            <?php foreach ($criterias as $criteriaId => $criteriaName) : ?>
                <th class="center"><?php echo $criteriaName; ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php
        $row = 0;
        foreach ($ratings as $rating): ?>
            <?php
            if ($tabRatingType['id'] === $rating['RatingTypes']['id']) :
                ?>
                <tr data-toggle="collapse" data-target=".row<?php echo $row; ?>" style="cursor: pointer">
                    <?php echo $this->Form->control('id' . '-details-' . $row, [
                        'name' => 'legiscan_bill_id',
                        'label' => false,
                        'class' => '',
                        'type' => 'hidden',
                        'default' => $rating['id']]); ?>
                    <td style="width:175px"><?php echo $this->LegiscanBill->getBillName($rating['bill_number']); ?></td>
                    <?php foreach ($criteriaList as $criteriaId => $criteriaName) :
                        ?>
                        <td class="center">
                            <?php
                            if ($currentTenantType === 'F') {
                                echo $this->Form->control('criterias' . '-action-' . $row, [
                                    'type' => 'hidden',
                                    'name' => "criterias[$criteriaId][$row][action-id]",
                                    'label' => false,
                                    'default' => null]);
                            } else {
                                echo $this->Form->control('criterias' . '-action-' . $row, [
                                    'name' => "criterias[$criteriaId][$row][action-id]",
                                    'label' => false,
                                    'class' => 'custom-select',
                                    'default' => $ratingCriterias[$rating['id']][$criteriaId]['action_id'] ?? null,
                                    'empty' => ['0' => ''],
                                    'options' => $actions[$tabRatingType['name']] ?? [],
                                    'templates' => [
                                        'inputContainer' => '<div class="input {{type}}{{required}}" style="width:200px">{{content}}</div>'
                                    ]]);
                                echo '<br/>';
                            }
                            ?>
                            <?php echo $this->Form->control('criterias' . '-criteria-detail-' . $row, [
                                'name' => "criterias[$criteriaId][$row][criteria-detail-id]",
                                'label' => false,
                                'class' => 'custom-select',
                                'default' => $ratingCriterias[$rating['id']][$criteriaId]['criteria_detail_id'] ?? null,
                                'empty' => ['0' => ''],
                                'options' => $criteriaDetails[$criteriaId] ?? [],
                                'templates' => [
                                    'inputContainer' => '<div class="input {{type}}{{required}}" style="width:200px">{{content}}</div>'
                                ]]); ?>
                        </td>
                    <?php
                    endforeach;
                    ?>
                </tr>
                <tr class="<?php echo isset($currentRatingId) ? '' : 'collapse' ?> row<?php echo $row; ?>">
                    <td colspan="<?php echo $criteriaCount + 1 ?>">
                        <?php echo $this->Form->control('id' . '-details-' . $row, [
                            'name' => 'legiscan_bill_id',
                            'label' => false,
                            'class' => '',
                            'type' => 'hidden',
                            'default' => $rating['id']]); ?>
                        <div class="related row">
                            <div class="col-10">
                                <b><?php echo $rating['title']; ?></b>
                                <br/>
                                <?php echo trim((string)$rating['description']); ?>

                            </div>
                            <div class="col-2">
                                <a href="<?php echo $rating['legiscan_url']; ?>" target="_blank">
                                    <span class="octicon octicon-link-external"></span> LegiScan
                                </a>
                                <br/>
                                <a href="<?php echo $rating['state_url']; ?>" target="_blank">
                                    <span class="octicon octicon-link-external"></span> State
                                </a>
                            </div>
                        </div>
                        <div class="related row form-group">
                            <div class="col-sm-12">
                                <hr/>
                                <?php
                                $default_notes = '';
                                if (isset($rating['TenantsBills']['notes'])) {
                                    $default_notes = $rating['TenantsBills']['notes'];
                                }
                                echo $this->Form->control('notes' . '-' . $row, [
                                    'name' => 'notes',
                                    'type' => 'textarea',
                                    'rows' => 3,
                                    'label' => false,
                                    'placeholder' => 'Notes',
                                    'class' => 'form-control',
                                    'default' => $default_notes,
                                    'templates' => [
                                        'inputContainer' => '<div class="input {{type}}{{required}}">{{content}}</div>'
                                    ]]);
                                ?>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endif; ?>
            <?php $row++; endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>
