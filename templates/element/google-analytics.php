<?php

use Cake\Core\Configure;

$gaCode = Configure::read('google-analytics.tracker-code');

if ($gaCode) {
    $googleAnalytics = <<<EOD
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=$gaCode"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '$gaCode');
</script>
EOD;
    echo $googleAnalytics;
}
