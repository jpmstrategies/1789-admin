<?php if (!empty($ratings)): ?>
    <?php if (sizeof($priorities) === 0) : ?>
        <div class="alert alert-warning" role="alert">
            Please configure Priorities by clicking
            <?php echo $this->Html->link(__('here'),
                ['controller' => 'Priorities', 'action' => 'add']); ?>.
        </div>
    <?php else : ?>
        <?php echo $this->Form->create($ratings, ['class' => 'form-bulkedit']); ?>
        <table id="IntegrationTable" class="table table-striped vcenter">
            <thead class="header">
            <tr>
                <th class="center">Name</th>
                <th class="center" style="width: 80%"><?php echo __('Priorities'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $row = 0;
            foreach ($ratings as $rating):
                ?>
                <tr>
                    <?php echo $this->Form->control('id' . '-integrations-' . $row, [
                        'name' => 'id',
                        'label' => false,
                        'type' => 'hidden',
                        'default' => $rating['id']]); ?>
                    <td>
                        <?php echo $rating['name']; ?>
                    </td>
                    <td>
                        <?php echo $this->Form->control('ratings_priorities[]' . '-' . $row, [
                            'name' => 'ratings_priorities',
                            'label' => false,
                            'class' => 'custom-select ratings-priorities-select',
                            'type' => 'select',
                            'options' => $priorities,
                            'val' => $ratingsPriorities[$rating['id']] ?? null,
                            'multiple' => true]); ?>
                    </td>
                </tr>
                <?php $row++; endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->Form->end(); ?>
    <?php endif; ?>
<?php endif; ?>
<script type="application/javascript">
    $('.ratings-priorities-select').select2({
        theme: 'bootstrap4',
        minimumResultsForSearch: -1,
        allowClear: true,
        multiple: true,
        dropdownAutoWidth: true,
        placeholder: 'Select priorities'
    });
</script>
