<?php if (!empty($ratings)): ?>
    <?php echo $this->Form->create($ratings, ['class' => 'form-bulkedit']); ?>
    <table id="ScoresTable" class="table table-striped vcenter table-fixed-header">
        <thead class="header">
        <tr>
            <th class="center"><?php echo __('Sort Order'); ?></th>
            <th class="center"><?php echo __('Rating Name'); ?></th>
            <th class="center"><?php echo __('Position'); ?></th>
            <?php if ($currentTenantType !== 'F') : ?>
                <th class="center"><?php echo __('Action'); ?></th>
                <th class="center"><?php echo __('Weight'); ?></th>
            <?php endif; ?>
            <th class=""></th>
        </tr>
        </thead>
        <tbody>
        <?php $previousRatingType = []; ?>
        <?php
        $row = 0;
        foreach ($ratings as $rating): ?>
            <?php
            if (!in_array($rating['rating_type']['name'], $previousRatingType)) :
                array_push($previousRatingType, $rating['rating_type']['name']);
                ?>
                <tr>
                    <td colspan="9" class="table-info"><h4><?php echo $rating['rating_type']['name']; ?></h4></td>
                </tr>
            <?php endif; ?>
            <tr>
                <?php echo $this->Form->control('id' . '-scores-' . $row, [
                    'name' => 'id',
                    'label' => false,
                    'class' => '',
                    'type' => 'hidden',
                    'default' => $rating['id']]); ?>
                <td width="50px"><?php echo $rating['sort_order']; ?></td>
                <td width="350px"><?php echo $rating['name']; ?></td>
                <td><?php echo $this->Form->control('position_id' . '-' . $row, [
                        'name' => 'position_id',
                        'label' => false,
                        'class' => 'custom-select',
                        'default' => $rating['position_id'] ?? null,
                        'empty' => ['0' => ''],
                        'templates' => [
                            'inputContainer' => '<div class="input {{type}}{{required}}" style="width:300px">{{content}}</div>'
                        ],
                        'options' => $positions ?? []]); ?>
                </td>
                <?php if ($currentTenantType !== 'F') : ?>
                    <td>
                        <?php echo $this->Form->control('action_id' . '-' . $row, [
                            'name' => 'action_id',
                            'label' => false,
                            'class' => 'custom-select',
                            'default' => $rating['action_id'] ?? null,
                            'empty' => ['0' => ''],
                            'disabled' => isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes',
                            'options' => isset($rating['rating_type']['name']) ? $actions[$rating['rating_type']['name']] : [],
                            'templates' => [
                                'inputContainer' => '<div class="input {{type}}{{required}}" style="width:150px">{{content}}</div>'
                            ]]); ?>
                    </td>
                    <td><?php echo $this->Form->control('rating_weight' . '-' . $row, [
                            'name' => 'rating_weight',
                            'label' => false,
                            'class' => 'form-control',
                            'type' => 'text',
                            'default' => $rating['rating_weight'],
                            'templates' => [
                                'inputContainer' => '<div class="input {{type}}{{required}}" style="width:80px">{{content}}</div>'
                            ]]); ?>
                    </td>
                <?php endif; ?>
                <td class="center">
                    <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                        ['action' => 'edit', $rating['id']], ['escape' => false]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>