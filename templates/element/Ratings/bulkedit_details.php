<?php if (!empty($ratings)): ?>
    <?php echo $this->Form->create($ratings, ['class' => 'form-bulkedit']); ?>
    <table id="DetailsTable" class="table table-striped vcenter table-fixed-header">
        <thead class="header">
        <tr>
            <th class="center"><?php echo __('Sort Order'); ?></th>
            <th class="center"><?php echo __('Rating Name'); ?></th>
            <?php if ($currentTenantType !== 'F') : ?>
                <th class="center"><?php echo __('Record Vote'); ?></th>
            <?php endif; ?>
            <th class="center"><?php echo __('Slug'); ?></th>
            <th class="center"><?php echo __('Enabled'); ?></th>
            <th class=""></th>
        </tr>
        </thead>
        <tbody>
        <?php $previousRatingType = []; ?>
        <?php
        $row = 0;
        foreach ($ratings as $rating): ?>
            <?php
            if (!in_array($rating['rating_type']['name'], $previousRatingType)) :
                array_push($previousRatingType, $rating['rating_type']['name']);
                ?>
                <tr>
                    <td colspan="9" class="table-info"><h4><?php echo $rating['rating_type']['name']; ?></h4></td>
                </tr>
            <?php endif; ?>
            <tr>
                <?php echo $this->Form->control('id' . '-details-' . $row, [
                    'name' => 'id',
                    'label' => false,
                    'class' => '',
                    'type' => 'hidden',
                    'default' => $rating['id']]); ?>
                <td style="width:50px"><?php echo $rating['sort_order']; ?></td>
                <td style="width:500px"><?php echo $this->Form->control('name' . '-' . $row, [
                        'name' => 'name',
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'textarea',
                        'rows' => 2,
                        'default' => $rating['name'],
                        'templates' => [
                            'inputContainer' => '<div class="input {{type}}{{required}}" style="width:500px">{{content}}</div>'
                        ]]); ?></td>
                <?php if ($currentTenantType !== 'F') : ?>
                    <td><?php echo $this->Form->control('record_vote' . '-' . $row, [
                            'name' => 'record_vote',
                            'label' => false,
                            'class' => 'form-control',
                            'type' => 'text',
                            'default' => $rating['record_vote'],
                            'templates' => [
                                'inputContainer' => '<div class="input {{type}}{{required}}" style="width:80px">{{content}}</div>'
                            ]]); ?>
                    </td>
                <?php endif; ?>
                <td><?php echo $this->Form->control('slug' . '-' . $row, [
                        'name' => 'slug',
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'textarea',
                        'rows' => 2,
                        'default' => $rating['slug'],
                        'templates' => [
                            'inputContainer' => '<div class="input {{type}}{{required}}" style="width:300px">{{content}}</div>'
                        ]]); ?></td>
                <td class="center">
                    <?php echo $this->Form->control('is_enabled' . '-' . $row, [
                        'name' => 'is_enabled',
                        'label' => false,
                        'class' => 'checkbox form-control-sm',
                        'type' => 'checkbox',
                        'default' => $rating['is_enabled']]); ?>
                </td>
                <td class="center">
                    <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                        ['action' => 'edit', $rating['id']], ['escape' => false]); ?>
                </td>
            </tr>
            <?php $row++; endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>