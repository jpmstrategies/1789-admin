<?php
use Cake\Core\Configure;
$hasCurrentUploadSrc = !empty($bill->upload->path);
$currentUploadSrc = $hasCurrentUploadSrc ? Configure::read('App.r2_base_url') . '/' . $bill->upload->path : null;
?>

<?= $this->Form->create($bill, ['id' => 'bill-form']) ?>
<?php
echo $this->Form->control('tenant_id', ['type' => 'hidden']);
?>
<?= $this->Form->hidden('statuses_url', ['value' => $this->Url->build(['controller' => 'BillEnums', 'action' => 'getStatusesByStage']), 'id' => 'statuses-url']) ?>
<style>
.upload-image {
    margin-top: 10px;
    width: 100%;
    /* Set a classic checkerboard grid as background */
    background-image: linear-gradient(45deg, #ccc 25%, transparent 25%, transparent 75%, #ccc 75%, #ccc),
                      linear-gradient(45deg, #ccc 25%, transparent 25%, transparent 75%, #ccc 75%, #ccc);
    background-size: 20px 20px; /* Size of the checkerboard squares */
    background-position: 0 0, 10px 10px; /* Offset second layer for grid alignment */
}
</style>

    <!-- General Bill Information -->
    <div class="card mb-3">
        <div class="card-header">Bill Details</div>
        <div class="card-body">
            <div class="alert alert-info">
                All fields in this section are required.
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('legislative_session_id', ['options' => $legislativeSessions, 'class' =>
                    'form-control', 'placeholder' => '', 'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('filed_chamber_id', ['options' => $filedChamber, 'class' =>
                    'form-control', 'placeholder' => '', 'empty' => true]);
                ?>
                <small class="form-text text-muted">
                    This is used to determine which chamber will be displayed first on the bill tracker.
                </small>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('bill_number', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('official_title', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <small class="form-text text-muted">
                    This should be the filed title of the bill and will be displayed on the listing page.
                </small>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('explainer_title', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <small class="form-text text-muted">
                    This should be a more user-friendly title of the bill and will be displayed on the bill detail page.
                </small>
            </div>
            <div class="form-group">
                <?php
                echo $this->Visibility->control('is_enabled', ['class' => 'form-control', 'label' => 'Visibility']);
                ?>
                <small class="form-text text-muted">
                    All "Current Bill" fields must be populated out before the bill can be made visible in the public app.
                </small>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('slug', ['class' => 'form-control', 'placeholder' => '']);
                ?>
                <small class="form-text text-muted">The friendly URL. It should contain no spaces
                    and use dashes
                    instead. For Example:<br/><br/>
                    <?php echo date('Y'); ?>-house-bill-12<br/>
                    <?php echo date('Y'); ?>-senate-bill-42
                </small>
            </div>
        </div>
    </div>

    <!-- Current Information -->
    <div class="card mb-3">
        <div class="card-header">Current Bill Information</div>
        <div class="card-body">

            <div class="form-group">
                <?php
                echo $this->Form->control('current_chamber_id', ['options' => $currentChamber, 'empty' => true,
                    'class' => 'custom-select', 'label' => 'Chamber']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('current_bill_stage_id', ['options' => $currentBillStage, 'empty' => true,
                    'class' => 'custom-select', 'label' => 'Stage']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('current_bill_status_id', ['options' => [], 'empty' => true,
                    'class' => 'custom-select', 'disabled' => true, 'label' => 'Status',
                    'data-current-value' => $bill->current_bill_status_id]);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('current_bill_progress_id', ['options' => $currentBillProgress, 'empty' => true,
                    'class' => 'custom-select', 'label' => 'Progress']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('current_bill_phase_icon_id', ['options' => $currentBillPhaseIcon, 'empty' => true,
                    'class' => 'custom-select', 'label' => 'Process Phase Icon']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('current_bill_rating_icon_id', ['options' => $currentBillRatingIcon, 'empty' => true,
                    'class' => 'custom-select', 'label' => 'Rating Icon']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('current_bill_cta_id', ['options' => $currentBillCta, 'empty' => true,
                    'class' => 'custom-select', 'label' => 'Call to Action']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('current_bill_cta_url', ['empty' => true,
                    'class' => 'form-control',  'label' => 'Call to Action URL']);
                ?>
            </div>
        </div>
    </div>

    <!-- Feature Image -->
    <div class="card mb-3">
        <div class="card-header">Featured Image</div>
        <div class="card-body">
            <div class="form-group">
                <div class="input text">
                    <?= $this->Form->hidden('upload_id', ['value' => $bill->upload_id, 'id' => 'upload-id']); ?>
                    <?= $this->Form->hidden('upload_path', ['value' => '', 'id' => 'upload-path']); ?>
                    <?= $this->Form->hidden('upload_mime', ['value' => '', 'id' => 'upload-mime']); ?>
                    <?= $this->Form->hidden('upload_width', ['value' => '', 'id' => 'upload-width']); ?>
                    <?= $this->Form->hidden('upload_height', ['value' => '', 'id' => 'upload-height']); ?>
                    <?php if (!empty($currentUploadSrc)) { ?>
                        <label>Replace Image</label>
                    <?php } else { ?>
                        <label>Upload Image</label>
                    <?php } ?>
                    <div class="input-group">
                        <div class="custom-file">
                            <!-- nameless input to prevent sending file to server unnecessarily -->
                            <input type="file" id="upload-input" class="custom-file-input">
                            <label id="upload-input-label" class="custom-file-label" for="upload-input">Choose file...</label>
                        </div>
                        <div class="input-group-append">
                           <button id="upload-clear" class="btn btn-outline-secondary" type="button">Clear</button>
                        </div>
                    </div>
                    <?php if ($hasCurrentUploadSrc) { ?>
                        <div id="upload-delete-wrapper" class="form-check" style="margin-top: 10px;">
                            <?php echo $this->Form->control('delete_upload', ['class' => 'form-check-input', 'label' => 'Delete Current Image', 'type' => 'checkbox']); ?>
                        </div>
                    <?php } ?>
                    <small class="form-text text-muted">Image changes will not be saved until the page is submitted.</small>
                </div>
            </div>
            <div id="upload-preview-wrapper" class="form-group" style="display: none;">
                <label>Preview Image</label>
                <img id="upload-preview" src="#" alt="Image Preview" class="upload-image">
            </div>
            <?php if ($hasCurrentUploadSrc) { ?>
                <div class="form-group">
                    <label>Current Image</label>
                    <?php echo $this->Html->image($currentUploadSrc, ['class' => 'upload-image']); ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <!-- Supplementary Information -->
    <div class="card mb-3">
        <div class="card-header">Supplementary Information</div>
        <div class="card-body">
            <div class="form-group">
                <?php
                echo $this->Form->control('description', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('notes', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('sponsors', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
        </div>
    </div>

    <!-- LegiScan / Ratings Information -->
    <div class="card mb-3">
        <div class="card-header">LegiScan</div>
        <div class="card-body">
            <div class="form-group">
                <?= $this->Form->hidden('legiscan_search_url', [
                    'value' => $this->Url->build(['controller' => 'LegiscanBills', 'action' => 'search']),
                    'id' => 'legiscan-search-url'
                ]); ?>

                <?php

                $legiscanDefaultOption = ['' => ''];

                if ($bill->legiscan_bill_id) {
                    $legiscanDefaultOption[$legiscanBill->id] = $this->LegiscanBill->getBillName($legiscanBill->bill_number);
                }

            echo $this->Form->control('legiscan_bill_id', [
                'name' => 'legiscan_bill_id',
                'label' => 'Legiscan Bill',
                'class' => 'custom-select legiscan-bill-select',
                'type' => 'select',
                'style' => 'width: 100%',
                'empty' => '',
                'disabled' => empty($bill->legiscan_bill_id),
                'options' => $legiscanDefaultOption,
                'selected' => $bill->legiscan_bill_id]); ?>
            <small class="form-text text-muted">
                Try searching for bills such as "HB 1" or "SB 2"
            </small>
            <?php echo $this->Form->hidden('house_rating_id', [
                'id' => 'house-rating-id',
                'value' => $bill->house_rating_id]); ?>
            <?php echo $this->Form->hidden('senate_rating_id', [
                'id' => 'senate-rating-id',
                'value' => $bill->senate_rating_id]); ?>
        </div>
        <div class="form-group">
            <?php echo $this->Form->control('house_rating_votes', [
                'id' => 'house-rating-votes',
                'type' => 'textarea',
                'class' => 'form-control',
                'default' => $bill->house_rating_votes]); ?>
        </div>
        <div class="form-group">
            <?php echo $this->Form->control('senate_rating_votes', [
                'id' => 'senate-rating-votes',
                'type' => 'textarea',
                'class' => 'form-control',
                'default' => $bill->senate_rating_votes]); ?>
        </div>
    </div>
</div>

<?= $this->Form->button(__('Submit'), ['id' => 'bill-form-submit', 'class' => 'btn btn-primary', 'style' => 'margin-right: 10px;']) ?>
<?php echo $this->Html->link(
    'Cancel',
    ['controller' => 'bills', 'action' => 'index', '_full' => true],
    ['class' => 'btn btn-secondary', 'id' => 'cancel']
); ?>
<?= $this->Form->end() ?>


<script type="text/javascript">
    jQuery(document).ready(function () {
        $('#house-rating-votes').summernote({
            height: "300px"
        });
        $('#senate-rating-votes').summernote({
            height: "300px"
        });
        $('#description').summernote({
            height: "300px"
        });
        $('#notes').summernote({
            height: "300px"
        });
        $('#sponsors').summernote({
            height: "300px"
        });

        // copy over official title to explainer title by default, unless explainer is already set
        $('#official-title').on('change', function () {
            if ($('#explainer-title').val() === '') {
                $('#explainer-title').val($(this).val());
            }
        });

        const hasCurrentUpload = <?= $hasCurrentUploadSrc ? 'true' : 'false' ?>;
        const billForm = $('#bill-form');
        const billFormSubmit = $('#bill-form-submit');
        const uploadInput = $('#upload-input');
        const uploadInputLabel = $('#upload-input-label');
        const uploadClear = $('#upload-clear');
        const uploadDeleteWrapper = $('#upload-delete-wrapper');
        const uploadStatus = $('#upload-status');
        const uploadPreviewWrapper = $('#upload-preview-wrapper');
        const uploadPreview = $('#upload-preview');
        const uploadPath = $('#upload-path');
        const uploadType = $('#upload-mime');
        const uploadWidth = $('#upload-width');
        const uploadHeight = $('#upload-height');
        let file = null;

        uploadClear.click(function (e) {
            uploadInput.val('');
            uploadInput.trigger('change');
        });

        // Load a preview of the selected image.
        uploadInput.change(async function (e) {
            file = e.target.files[0];

            if (!file) {
                file = null;
                uploadInputLabel.text('Choose file...');
                uploadPreview.attr('src', '#');
                uploadPreview.hide();
                uploadPreviewWrapper.hide();
                uploadStatus.hide();
                uploadDeleteWrapper.show();
                return;
            }

            try {
                console.log('Selected file:', file);
                uploadInputLabel.text(file.name);
                const reader = new FileReader();
                reader.onload = function(e) {
                    const src = e.target.result;
                    uploadPreview.attr('src', src);
                    uploadPreview.show();
                    uploadPreviewWrapper.show();
                    if (hasCurrentUpload) uploadStatus.text('The image will not be replaced until the page is submitted.');
                    else uploadStatus.text('The image will not be saved until the page is submitted.');
                    uploadStatus.show();
                    uploadDeleteWrapper.hide();
                };
                reader.readAsDataURL(file); // Read the file as a data URL
            } catch (error) {
                console.error(error);
                file = null;
                uploadInputLabel.text('Choose file...');
                uploadPreview.attr('src', '#');
                uploadPreview.hide();
                uploadPreviewWrapper.hide();
                uploadStatus.text('');
                uploadStatus.show();
                uploadDeleteWrapper.show();
            }
        });

        function getImageResolution(file) {
            return new Promise((resolve, reject) => {
                const img = new Image();
                img.onload = function () {
                    resolve({ width: img.width, height: img.height });
                };
                img.src = file;
            });
        }

        billFormSubmit.on('click', async function (e) {
            e.preventDefault();
            if (file) {
                console.log('Uploading file:', file);
                // Get a signed URL for the upload directly to CDN.
                const response = await fetch(`<?= $this->Url->build(['controller' => 'api/uploads', 'action' => 'getSignedUpload']); ?>?key=${file.name}`, {
                    method: 'GET'
                });
                const data = await response.json();

                // Use signed url to upload file to CDN.
                const uploadResponse = await fetch(data.uploadUrl, {
                    method: 'PUT',
                    body: file,
                });

                if (!uploadResponse.ok) {
                    alert('File upload failed, old image retained. We will attempt to save the rest of your changes.');
                } else {
                    uploadPath.val(data.uploadPath);
                    uploadType.val(file.type);
                    const resolution = await getImageResolution(URL.createObjectURL(file));
                    uploadWidth.val(resolution.width);
                    uploadHeight.val(resolution.height);
                }
            }

            billForm.submit();
        });
    });
</script>
