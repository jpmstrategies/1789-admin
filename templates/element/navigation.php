<?php

use Cake\Core\Configure;

$controller = $this->request->getParam('controller');
$legislators_active = ' class="nav-item dropdown"';
$people_active = ' class="nav-item dropdown"';
$ratings_active = ' class="nav-item dropdown"';
$org_active = ' class="nav-item dropdown"';
$taxonomy_active = ' class="nav-item dropdown"';
$logs_active = '';

switch (strtolower((string)$controller)) {
    case 'tracking':
        $tracking_active = ' class="nav-item dropdown active"';
        break;
    case 'ratings':
        $ratings_active = ' class="nav-item dropdown active"';
        break;
    case 'actions':
        $taxonomy_active = ' class="nav-item dropdown active"';
        break;
    case 'legislative_sessions':
        $taxonomy_active = ' class="nav-item dropdown active"';
        break;
    case 'legislators':
        $legislators_active = ' class="nav-item dropdown active"';
        break;
    case 'people':
        $people_active = ' class="nav-item dropdown active"';
        break;
    case 'rating_types':
        $taxonomy_active = ' class="nav-item dropdown active"';
        break;
    case 'logs':
        $logs_active = ' class="nav-item dropdown active"';
        break;
    case 'addresses':
        $taxonomy_active = ' class=" nav-item dropdown active"';
        break;
    case 'organizations':
        $org_active = ' class=" nav-item dropdown active"';
        break;
    default:
        $taxonomy_active = ' class="nav-item dropdown active"';
}
?>

<div class="navbar navbar-dark bg-dark fixed-top navbar-expand-lg">
    <div class="container">
        <div style="min-width: 200px;">
            <?php if (isset($currentTenantId)) : ?>
                <?php
                echo $this->Html->link($currentTenantName, [
                    'controller' => 'Pages',
                    'action' => 'display',
                    'home',
                    'plugin' => false,
                    'prefix' => false
                ], ['class' => 'navbar-brand']);
                ?>
            <?php else : ?>
                <div class="navbar-brand">
                    Cornerstone
                </div>
            <?php endif; ?>
        </div>
        <?php if (isset($logged_in) && !empty($logged_in)) : ?>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                    <?php if ($is_super) : ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarSuperAdmin"
                               data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                SA
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarSuperAdmin">
                                <?php echo $this->Html->link('Configuration Options', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'configuration_options',
                                    'action' => 'index',
                                    'prefix' => false], ['class' => 'dropdown-item']);
                                ?>
                                <?php echo $this->Html->link('Configuration Types', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'configuration_types',
                                    'action' => 'index',
                                    'prefix' => false], ['class' => 'dropdown-item']);
                                ?>
                                <?php echo $this->Html->link('Tenants', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'tenants',
                                    'action' => 'index',
                                    'prefix' => false], ['class' => 'dropdown-item']);
                                ?>
                                <?php echo $this->Html->link('Users', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'Users',
                                    'action' => 'index',
                                    'prefix' => false], ['class' => 'dropdown-item']);
                                ?>
                            </div>
                        </li>
                    <?php endif; ?>
                    <?php if (!empty($currentTenantId)) : ?>
                        <?php if ($currentTenantRole === 'A') : ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarAdmin" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    Admin
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarAdmin">
                                    <?php echo $this->Html->link('Configurations', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'configurations',
                                        'action' => 'index',
                                        'prefix' => false
                                    ], ['class' => 'dropdown-item']);
                                    ?>
                                    <?php echo $this->Html->link('Members', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'accounts',
                                        'action' => 'members',
                                        'prefix' => false
                                    ], ['class' => 'dropdown-item']);
                                    ?>
                                    <?php if ($is_super) : ?>
                                        <?php echo $this->Html->link('Rating Fields', [
                                            'admin' => 'admin',
                                            'plugin' => false,
                                            'controller' => 'rating_fields',
                                            'action' => 'index',
                                            'prefix' => false
                                        ], ['class' => 'dropdown-item']);
                                        ?>
                                    <?php endif; ?>
                                </div>
                            </li>
                        <?php endif; ?>

                        <?php if ($currentTenantDataSource === 'legiscan') : ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarLegiscan"
                                   data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    LegiScan
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarLegiscan">

                                    <?php echo $this->Html->link('Bills', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'LegiscanBills',
                                        'action' => 'index'], ['class' => 'dropdown-item']);
                                    ?>
                                    <?php if ($is_super) : ?>
                                        <?php echo $this->Html->link('Bill Subjects', [
                                            'admin' => 'admin',
                                            'plugin' => false,
                                            'controller' => 'LegiscanBillSubjects',
                                            'action' => 'index',
                                            'prefix' => false
                                        ], ['class' => 'dropdown-item']);
                                        ?>
                                        <?php echo $this->Html->link('Bill Votes', [
                                            'admin' => 'admin',
                                            'plugin' => false,
                                            'controller' => 'LegiscanBillVotes',
                                            'action' => 'index',
                                            'prefix' => false
                                        ], ['class' => 'dropdown-item']);
                                        ?>
                                        <?php echo $this->Html->link('Bill Vote Details', [
                                            'admin' => 'admin',
                                            'plugin' => false,
                                            'controller' => 'LegiscanBillVoteDetails',
                                            'action' => 'index',
                                            'prefix' => false
                                        ], ['class' => 'dropdown-item']);
                                        ?>
                                        <?php echo $this->Html->link('Chambers', [
                                            'admin' => 'admin',
                                            'plugin' => false,
                                            'controller' => 'LegiscanChambers',
                                            'action' => 'index'], ['class' => 'dropdown-item']);
                                        ?>
                                        <?php echo $this->Html->link('Parties', [
                                            'admin' => 'admin',
                                            'plugin' => false,
                                            'controller' => 'LegiscanParties',
                                            'action' => 'index'], ['class' => 'dropdown-item']);
                                        ?>
                                        <?php echo $this->Html->link('Sessions', [
                                            'admin' => 'admin',
                                            'plugin' => false,
                                            'controller' => 'LegiscanSessions',
                                            'action' => 'index'], ['class' => 'dropdown-item']);
                                        ?>
                                        <?php echo $this->Html->link('Subjects', [
                                            'admin' => 'admin',
                                            'plugin' => false,
                                            'controller' => 'LegiscanSubjects',
                                            'action' => 'index'], ['class' => 'dropdown-item']);
                                        ?>
                                    <?php endif; ?>
                                </div>
                            </li>
                        <?php endif; ?>

                        <li <?php echo $legislators_active; ?>>
                            <a class="nav-link dropdown-toggle" href="#" id="navbarLegislator" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                Legislators
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarLegislator">
                                <?php echo $this->Html->link('View All', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'legislators',
                                    'action' => 'index'], ['class' => 'dropdown-item']);
                                ?>
                                <?php echo $this->Html->link('Bulk Edit', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'legislators',
                                    'action' => 'bulkedit'], ['class' => 'dropdown-item']);
                                ?>
                            </div>
                        </li>
                        <li <?php echo $people_active; ?>>
                            <a class="nav-link dropdown-toggle" href="#" id="navbarPeople" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                People
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarPeople">
                                <?php echo $this->Html->link('View All', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'people',
                                    'action' => 'index'], ['class' => 'dropdown-item']);
                                ?>
                                <?php echo $this->Html->link('Bulk Edit', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'people',
                                    'action' => 'bulkedit'], ['class' => 'dropdown-item']);
                                ?>
                            </div>
                        </li>
                        <?php if ($currentTenantType !== 'D' && $currentTenantType !== 'F') : ?>
                            <li <?php echo $ratings_active; ?>>
                                <a class="nav-link dropdown-toggle" href="#" id="navbarRatings" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    Ratings
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarRatings">
                                    <?php echo $this->Html->link('View All', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'ratings',
                                        'action' => 'index',
                                        '?' => [
                                            'sort' => 'LegislativeSessions.name',
                                            'direction' => 'desc']], ['class' => 'dropdown-item']);
                                    ?>
                                    <?php echo $this->Html->link('Bulk Edit Ratings', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'ratings',
                                        'action' => 'bulkedit'], ['class' => 'dropdown-item']);
                                    ?>
                                    <?php if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes') : ?>
                                        <?php echo $this->Html->link('Bulk Edit Criterias', [
                                            'admin' => 'admin',
                                            'plugin' => false,
                                            'controller' => 'RatingCriterias',
                                            'action' => 'bulkedit'], ['class' => 'dropdown-item']);
                                        ?>
                                    <?php endif; ?>
                                    <?php if ($currentTenantType !== 'F') : ?>
                                        <?php echo $this->Html->link('Bulk Edit Records', [
                                            'admin' => 'admin',
                                            'plugin' => false,
                                            'controller' => 'records',
                                            'action' => 'bulkedit'], ['class' => 'dropdown-item']);
                                        ?>
                                        <?php if(isset($currentTenantDataSource) && $currentTenantDataSource !== 'legiscan') : ?>
                                            <?php echo $this->Html->link('Bulk Import Records', [
                                                'admin' => 'admin',
                                                'plugin' => false,
                                                'controller' => 'records',
                                                'action' => 'bulk_import'], ['class' => 'dropdown-item']);
                                            ?>
                                        <?php else : ?>
                                            <?php echo $this->Html->link('Bulk Re-Import Records', [
                                                'plugin' => false,
                                                'controller' => 'ratings',
                                                'action' => 'bulkLegiscanVoteImport'], ['class' => 'dropdown-item']);
                                            ?>
                                        <?php endif; ?>
                                        <?php if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'no') : ?>
                                            <?php echo $this->Html->link('Export Vote Grid', [
                                                'admin' => 'admin',
                                                'plugin' => false,
                                                'controller' => 'ratings',
                                                'action' => 'export_grid'], ['class' => 'dropdown-item']);
                                            ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </li>
                        <?php endif ?>

                        <?php if (isset($currentTenantFeatures['has_tracking']) && $currentTenantFeatures['has_tracking'] === true) : ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarTracking"
                                   data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    Tracking
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarTracking">

                                    <?php echo $this->Html->link('Bills', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'Bills',
                                        'action' => 'index'], ['class' => 'dropdown-item']);
                                    ?>
                                    <?php echo $this->Html->link('Bill Enums', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'BillEnums',
                                        'action' => 'index',
                                        'prefix' => false
                                    ], ['class' => 'dropdown-item']);
                                    ?>
                                </div>
                            </li>
                        <?php endif; ?>

                        <?php if ($currentTenantType === 'F') : ?>
                            <li <?php echo $ratings_active; ?>>
                                <a class="nav-link dropdown-toggle" href="#" id="navbarRatings" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    Floor Reports
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarRatings">
                                    <?php echo $this->Html->link('View Floor Reports', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'floor-reports',
                                        'action' => 'index',
                                        '?' => [
                                            'sort' => 'LegislativeSessions.name',
                                            'direction' => 'desc']], ['class' => 'dropdown-item']);
                                    ?>
                                    <?php echo $this->Html->link('View Ratings', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'ratings',
                                        'action' => 'index',
                                        '?' => [
                                            'sort' => 'LegislativeSessions.name',
                                            'direction' => 'desc']], ['class' => 'dropdown-item']);
                                    ?>
                                    <?php echo $this->Html->link('Bulk Edit Ratings', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'ratings',
                                        'action' => 'bulkedit'], ['class' => 'dropdown-item']);
                                    ?>
                                    <?php if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes') : ?>
                                        <?php echo $this->Html->link('Bulk Edit Criterias', [
                                            'admin' => 'admin',
                                            'plugin' => false,
                                            'controller' => 'RatingCriterias',
                                            'action' => 'bulkedit'], ['class' => 'dropdown-item']);
                                        ?>
                                    <?php endif; ?>
                                    <?php if ($currentTenantType !== 'F') : ?>
                                        <?php echo $this->Html->link('Bulk Edit Records', [
                                            'admin' => 'admin',
                                            'plugin' => false,
                                            'controller' => 'records',
                                            'action' => 'bulkedit'], ['class' => 'dropdown-item']);
                                        ?>
                                        <?php if(isset($currentTenantDataSource) && $currentTenantDataSource !== 'legiscan') : ?>
                                            <?php echo $this->Html->link('Bulk Import Records', [
                                                'admin' => 'admin',
                                                'plugin' => false,
                                                'controller' => 'records',
                                                'action' => 'bulk_import'], ['class' => 'dropdown-item']);
                                            ?>
                                        <?php endif; ?>
                                        <?php if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'no') : ?>
                                            <?php echo $this->Html->link('Export Vote Grid', [
                                                'admin' => 'admin',
                                                'plugin' => false,
                                                'controller' => 'ratings',
                                                'action' => 'export_grid'], ['class' => 'dropdown-item']);
                                            ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </li>
                        <?php endif ?>
                        <?php if ($currentTenantType === 'D') : ?>
                            <li <?php echo $org_active; ?>>
                                <a class="nav-link dropdown-toggle" href="#" id="navbarOrganizations"
                                   data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    Orgs
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarOrganizations">
                                    <?php echo $this->Html->link('View All', [
                                        'plugin' => false,
                                        'controller' => 'organizations',
                                        'action' => 'index'], ['class' => 'dropdown-item']);
                                    ?>
                                    <?php echo $this->Html->link('Bulk Edit Scores', [
                                        'plugin' => false,
                                        'controller' => 'scores',
                                        'action' => 'bulkedit'], ['class' => 'dropdown-item']);
                                    ?>
                                </div>
                            </li>
                        <?php endif ?>
                        <li <?php echo $taxonomy_active; ?>>
                            <a class="nav-link dropdown-toggle" href="#" id="navbarTaxonomy" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                Taxonomy
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarTaxonomy">
                                <?php if ($currentTenantType !== 'D') : ?>
                                    <?php echo $this->Html->link('Actions', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'actions',
                                        'action' => 'index'], ['class' => 'dropdown-item']);
                                    ?>
                                    <?php echo $this->Html->link('Action Types', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'ActionTypes',
                                        'action' => 'index'], ['class' => 'dropdown-item']);
                                    ?>
                                <?php endif ?>
                                <?php echo $this->Html->link('Addresses', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'addresses',
                                    'action' => 'index'], ['class' => 'dropdown-item']);
                                ?>
                                <?php echo $this->Html->link('Address Types', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'AddressTypes',
                                    'action' => 'index'], ['class' => 'dropdown-item']);
                                ?>
                                <?php echo $this->Html->link('Chambers', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'chambers',
                                    'action' => 'index'], ['class' => 'dropdown-item']);
                                ?>
                                <?php if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes') : ?>
                                    <?php echo $this->Html->link('Criterias', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'criterias',
                                        'action' => 'index'], ['class' => 'dropdown-item']);
                                    ?>
                                    <?php echo $this->Html->link('Criteria Details', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'criteria-details',
                                        'action' => 'index'], ['class' => 'dropdown-item']);
                                    ?>
                                <?php endif; ?>
                                <?php if ($currentTenantType !== 'D' && $currentTenantType !== 'F') : ?>
                                    <?php echo $this->Html->link('Grades', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'grades',
                                        'action' => 'index'], ['class' => 'dropdown-item']);
                                    ?>
                                <?php endif ?>
                                <?php echo $this->Html->link('Legislative Sessions', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'legislative_sessions',
                                    'action' => 'index'], ['class' => 'dropdown-item']);
                                ?>
                                <?php echo $this->Html->link('Parties', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'parties',
                                    'action' => 'index'], ['class' => 'dropdown-item']);
                                ?>
                                <?php echo $this->Html->link('Positions', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'positions',
                                    'action' => 'index'], ['class' => 'dropdown-item']);
                                ?>
                                <?php echo $this->Html->link('Priorities', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'priorities',
                                    'action' => 'index'], ['class' => 'dropdown-item']);
                                ?>
                                <?php if ($currentTenantType !== 'D' && $currentTenantType !== 'F') : ?>
                                    <?php echo $this->Html->link('Rating Types', [
                                        'admin' => 'admin',
                                        'plugin' => false,
                                        'controller' => 'rating_types',
                                        'action' => 'index'], ['class' => 'dropdown-item']);
                                    ?>
                                <?php endif ?>

                                <?php echo $this->Html->link('Tags', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'tags',
                                    'action' => 'index'], ['class' => 'dropdown-item']);
                                ?>
                                <?php echo $this->Html->link('Titles', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'titles',
                                    'action' => 'index'], ['class' => 'dropdown-item']);
                                ?>
                            </div>
                        </li>
                    <?php endif; ?>
                </ul>
                <div class="mt-2 mt-md-0">
                    <?php if ($numberOfTenants > 1 || $is_super) : ?>
                        <div style="float:left; padding-right: 10px;">
                            <?php
                            $switchAction = ($currentTenantId ? 'Switch Accounts' : 'Set Account');
                            echo $this->Html->link($switchAction, [
                                'admin' => 'admin',
                                'plugin' => false,
                                'controller' => 'accounts',
                                'action' => 'index',
                                'prefix' => false
                            ], ['class' => 'btn btn-outline-light my-2 my-sm-0']);
                            ?>
                        </div>
                    <?php endif; ?>
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li <?php echo $people_active; ?>>
                            <a class="nav-link dropdown-toggle" href="#" id="navbarPeople" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                My Account
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarPeople">
                                <?php echo $this->Html->link('Profile', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'users',
                                    'action' => 'view',
                                    'prefix' => false
                                ], ['class' => 'dropdown-item']);
                                ?>
                                <?php echo $this->Html->link('Edit Password', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'users',
                                    'action' => 'editPassword',
                                    'prefix' => false
                                ], ['class' => 'dropdown-item']);
                                ?>
                                <?php echo $this->Html->link('Logout', [
                                    'admin' => 'admin',
                                    'plugin' => false,
                                    'controller' => 'users',
                                    'action' => 'logout?source=user',
                                    'prefix' => false
                                ], ['class' => 'dropdown-item']);
                                ?>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--/.nav-collapse -->
        <?php else : ?>
            <div class="mt-0">
                <?php echo $this->Html->link('Sign in', [
                    'admin' => false,
                    'plugin' => false,
                    'controller' => 'users',
                    'action' => 'login',
                    'prefix' => false], ['class' => 'btn btn-dark']);
                ?>
                <?php if (Configure::read('users.self_signup_enabled')) : ?>
                    <?php echo $this->Html->link('Sign up', [
                        'admin' => false,
                        'plugin' => false,
                        'controller' => 'users',
                        'action' => 'signup',
                        'prefix' => false], ['class' => 'btn btn-outline-light']);
                    ?>
                <?php endif; ?>
            </div>
        <?php endif ?>
    </div>
</div>
