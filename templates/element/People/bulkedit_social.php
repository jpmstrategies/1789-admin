<?php if (!empty($people)): ?>
    <?php echo $this->Form->create($people, ['class' => 'form-bulkedit']); ?>
    <table id="SocialTable" class="table table-striped vcenter table-fixed-header">
        <thead class="header">
        <tr>
            <th class="center">Name</th>
            <th class="center"><?php echo __('Facebook'); ?></th>
            <th class="center"><?php echo __('Twitter'); ?></th>
            <th class="center"></th>
        </tr>
        </thead>
        <tbody>
        <tr class="table-primary">
            <td colspan="5">
                Please include only the Facebook and Twitter usernames.
            </td>
        </tr>
        <?php
        $row = 0;
        foreach ($people as $person): ?>
            <tr>
                <?php echo $this->Form->control('id' . '-social-' . $row, [
                    'name' => 'id',
                    'label' => false,
                    'type' => 'hidden',
                    'default' => $person['person']['id']]); ?>
                <td class="center" style="width: 25%">
                    <div class="input-group ">
                        <?php echo $person['person']['last_name'] . ', ' . $person['person']['first_name']; ?>
                    </div>
                </td>
                <td class="center" style="width: 35%">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">fb.com/</span>
                        </div>
                        <?php echo $this->Form->control('facebook' . '-' . $row, [
                            'name' => 'facebook',
                            'label' => false,
                            'class' => 'form-control',
                            'default' => $person['person']['facebook']]); ?>
                    </div>
                </td>
                <td class="center" style="width: 35%">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon2">@</span>
                        </div>
                        <?php echo $this->Form->control('twitter' . '-' . $row, [
                            'name' => 'twitter',
                            'label' => false,
                            'class' => 'form-control',
                            'default' => $person['person']['twitter']]); ?>
                    </div>
                </td>
                <td class="center" class="center" style="width: 5%">
                    <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                        ['action' => 'edit', $person['person']['id']], ['escape' => false]); ?>
                </td>
            </tr>
            <?php $row++; endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>