<?php
function getCalculatedScore($calculatedScores, $personId, $attribute)
{
    if (isset($calculatedScores['scores'][$personId][$attribute])) {
        return $calculatedScores['scores'][$personId][$attribute];
    } else {
        return 'N/A';
    }
}

?>
<?php if (!empty($people)): ?>
    <?php echo $this->Form->create($people, ['class' => 'form-bulkedit']); ?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="<?php echo $exportURL; ?>" class="nav-link"><span
                            class="octicon octicon-desktop-download"></span>&nbsp;&nbsp;CSV Export</a></li>
        </ul>
    </nav>
    <table id="ScoresTable" class="table table-striped vcenter">
        <thead class="header">
        <tr>
            <th class="center">Name</th>
            <th class="center">Calculated Career Score</th>
            <th class="center">Override Career Score</th>
            <th class="center">Calculated Career Grade</th>
            <th class="center">Override Career Grade</th>
            <th class="center"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $row = 0;
        foreach ($people as $person): ?>
            <tr>
                <?php echo $this->Form->control('id' . '-scores-' . $row, [
                    'name' => 'id',
                    'label' => false,
                    'type' => 'hidden',
                    'default' => $person['person']['id']]); ?>
                <td>
                    <?php echo $person['person']['last_name'] . ', ' . $person['person']['first_name']; ?>
                </td>
                <td style="text-align: center">
                    <?php echo getCalculatedScore($calculatedScores, $person['person']['id'], 'score'); ?>
                </td>
                <td>
                    <?php echo $this->Form->control('grade_number' . '-' . $row, [
                        'name' => 'grade_number',
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'text',
                        'default' => $person['person']['grade_number'],
                        'templates' => [
                            'inputContainer' => '<div class="input {{type}}{{required}}">{{content}}</div>'
                        ]]); ?>
                </td>
                <td style="text-align: center">
                    <?php echo getCalculatedScore($calculatedScores, $person['person']['id'], 'grade'); ?>
                </td>
                <td>
                    <?php
                    if (sizeof($grades->toArray()) > 0) {
                        $type = '';
                        $empty = true;
                    } else {
                        $type = 'hidden';
                        $empty = false;
                    }
                    echo $this->Form->control('grade_id' . '-' . $row, [
                        'name' => 'grade_id',
                        'label' => false,
                        'class' => 'custom-select',
                        'default' => $person['person']['grade_id'],
                        'templates' => [
                            'inputContainer' => '<div class="input {{type}}{{required}}">{{content}}</div>'
                        ],
                        'options' => $grades,
                        'type' => $type,
                        'empty' => $empty]);
                    ?>
                </td>
                <td class="center">
                    <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                        ['action' => 'edit', $person['person']['id']], ['escape' => false]); ?>
                </td>
            </tr>
            <?php $row++; endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>
