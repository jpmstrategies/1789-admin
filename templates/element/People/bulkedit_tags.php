<?php if (!empty($people)): ?>
    <?php if (sizeof($tags) === 0) : ?>
        <div class="alert alert-warning" role="alert">
            Please configure Tags by clicking
            <?php echo $this->Html->link(__('here'),
                ['controller' => 'Tags', 'action' => 'add']); ?>.
        </div>
    <?php else : ?>
        <?php echo $this->Form->create($people, ['class' => 'form-bulkedit']); ?>
        <table id="IntegrationTable" class="table table-striped vcenter">
            <thead class="header">
            <tr>
                <th class="center">Name</th>
                <th class="center" style="width: 80%"><?php echo __('Tags'); ?></th>
                <th class="center"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $row = 0;
            foreach ($people as $person):
                ?>
                <tr>
                    <?php echo $this->Form->control('id' . '-integrations-' . $row, [
                        'name' => 'id',
                        'label' => false,
                        'type' => 'hidden',
                        'default' => $person['person']['id']]); ?>
                    <td>
                        <?php echo $person['person']['last_name'] . ', ' . $person['person']['first_name']; ?>
                    </td>
                    <td>
                        <?php echo $this->Form->control('people_tags[]' . '-' . $row, [
                            'name' => 'people_tags',
                            'label' => false,
                            'class' => 'custom-select people-tag-select',
                            'type' => 'select',
                            'options' => $tags,
                            'val' => $peopleTags[$person['person']['id']] ?? null,
                            'multiple' => true]); ?>
                    </td>
                    <td class="center">
                        <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                            ['action' => 'edit', $person['person']['id']], ['escape' => false]); ?>
                    </td>
                </tr>
                <?php $row++; endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->Form->end(); ?>
    <?php endif; ?>
<?php endif; ?>
<script type="application/javascript">
    $('.people-tag-select').select2({
        theme: 'bootstrap4',
        minimumResultsForSearch: -1,
        allowClear: true,
        multiple: true,
        dropdownAutoWidth: true,
        placeholder: 'Select tags'
    });
</script>
