<?php if (!empty($people)): ?>
    <?php echo $this->Form->create($people, ['class' => 'form-bulkedit']); ?>
    <table id="DetailsTable" class="table table-striped vcenter table-fixed-header">
        <thead class="header">
        <tr>
            <th class="center"><?php echo __('Last Name'); ?></th>
            <th class="center"><?php echo __('First Name'); ?></th>
            <th class="center"><?php echo __('First Elected'); ?></th>
            <th class="center"><?php echo __('District City'); ?></th>
            <th class="center"><?php echo __('Slug'); ?></th>
            <th class="center"><?php echo __('Enabled'); ?></th>
            <th class="center"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $row = 0;
        foreach ($people as $person): ?>
            <tr>
                <?php echo $this->Form->control('id' . '-details-' . $row, [
                    'name' => 'id',
                    'label' => false,
                    'type' => 'hidden',
                    'default' => $person['person']['id']]); ?>
                <td><?php echo $this->Form->control('last_name' . '-' . $row, [
                        'name' => 'last_name',
                        'placeholder' => $person['person']['full_name'],
                        'label' => false,
                        'class' => 'form-control form-control-sm',
                        'default' => $person['person']['last_name']]); ?></td>
                <td><?php echo $this->Form->control('first_name' . '-' . $row, [
                        'name' => 'first_name',
                        'placeholder' => $person['person']['full_name'],
                        'label' => false,
                        'class' => 'form-control form-control-sm',
                        'default' => $person['person']['first_name']]); ?></td>
                <td><?php echo $this->Form->control('first_elected' . '-' . $row, [
                        'name' => 'first_elected',
                        'label' => false,
                        'class' => 'form-control form-control-sm',
                        'default' => $person['person']['first_elected']]); ?>
                </td>
                <td><?php echo $this->Form->control('district_city' . '-' . $row, [
                        'name' => 'district_city',
                        'label' => false,
                        'class' => 'form-control form-control-sm',
                        'type' => 'text',
                        'default' => $person['person']['district_city']]); ?>
                </td>
                <td><?php echo $this->Form->control('slug' . '-' . $row, [
                        'name' => 'slug',
                        'label' => false,
                        'class' => 'form-control form-control-sm',
                        'type' => 'text',
                        'default' => $person['person']['slug']]); ?>
                </td>
                <td class="center">
                    <?php echo $this->Form->control('is_enabled' . '-' . $row, [
                        'name' => 'is_enabled',
                        'label' => false,
                        'class' => 'checkbox form-control-sm',
                        'type' => 'checkbox',
                        'default' => $person['person']['is_enabled']]); ?>
                </td>
                <td class="center">
                    <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                        ['action' => 'edit', $person['person']['id']], ['escape' => false]); ?>
                </td>
            </tr>
            <?php $row++; endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>