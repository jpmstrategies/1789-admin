<?php if (!empty($people)): ?>
    <?php echo $this->Form->create($people, ['class' => 'form-bulkedit']); ?>
    <table id="ContactTable" class="table table-striped vcenter table-fixed-header">
        <thead class="header">
        <tr>
            <th>Name</th>
            <th class="center"><?php echo __('Email'); ?></th>
            <th class="center"><?php echo __('Phone'); ?></th>
            <th class="center"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $row = 0;
        foreach ($people as $person): ?>
            <tr>
                <?php echo $this->Form->control('id' . '-social-' . $row, [
                    'name' => 'id',
                    'label' => false,
                    'type' => 'hidden',
                    'default' => $person['person']['id']]); ?>
                <td class="center" style="width: 25%">
                    <div class="input-group ">
                        <?php echo $person['person']['last_name'] . ', ' . $person['person']['first_name']; ?>
                    </div>
                </td>
                <td style="width: 35%">
                    <div class="input-group">
                        <?php echo $this->Form->control('email' . '-' . $row, [
                            'name' => 'email',
                            'label' => false,
                            'class' => 'form-control',
                            'style' => 'width: 450px;',
                            'default' => $person['person']['email']]); ?>
                    </div>
                </td>
                <td style="width: 25%">
                    <div class="input-group">
                        <?php echo $this->Form->control('phone' . '-' . $row, [
                            'name' => 'phone',
                            'label' => false,
                            'class' => 'form-control phone-input',
                            'default' => $person['person']['phone']]); ?>
                    </div>
                </td>
                <td class="center" style="width: 5%">
                    <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                        ['action' => 'edit', $person['person']['id']], ['escape' => false]); ?>
                </td>
            </tr>
            <?php $row++; endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>
<script>
    $(document).ready(function(){
        $('.phone-input').on('input', function() {
            var number = $(this).val().replace(/[^\d]/g, '');
            if (number.length > 6) {
                number = '(' + number.substring(0, 3) + ') ' + number.substring(3, 6) + '-' + number.substring(6, 10);
            } else if (number.length > 3) {
                number = '(' + number.substring(0, 3) + ') ' + number.substring(3);
            } else {
                number = number ? '(' + number : '';
            }
            $(this).val(number);
        }).on('blur', function() {
            // If the value is just "(", clear the field.
            if($(this).val() === '(') {
                $(this).val('');
            }
        });
    });
</script>