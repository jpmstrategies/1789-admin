<?php
$enable_tusa = $integrations['enable_tusa'];
?>
<?php if (!empty($people)): ?>
    <?php echo $this->Form->create($people, ['class' => 'form-bulkedit']); ?>
    <table id="IntegrationTable" class="table table-striped vcenter">
        <thead class="header">
        <tr>
            <th class="center">Name</th>
            <?php if ($enable_tusa === 'yes') : ?>
                <th class="center"><?php echo __('TUSA Candidate'); ?></th>
            <?php endif; ?>
            <th class="center"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $row = 0;
        foreach ($people as $person):
            ?>
            <tr>
                <?php echo $this->Form->control('id' . '-integrations-' . $row, [
                    'name' => 'id',
                    'label' => false,
                    'type' => 'hidden',
                    'default' => $person['person']['id']]); ?>
                <td>
                    <?php echo $person['person']['last_name'] . ', ' . $person['person']['first_name']; ?>
                </td>
                <?php if ($enable_tusa === 'yes') : ?>
                    <td><?php echo $this->Form->control('ttx_candidate_slug' . '-' . $row, [
                            'name' => 'ttx_candidate_slug',
                            'label' => false,
                            'class' => 'custom-select ttx-candidate-select',
                            'type' => 'select',
                            'style' => 'width: 100%',
                            'empty' => '',
                            'options' => [
                                $person['person']['ttx_candidate_slug'] => $person['person']['ttx_candidate_slug'],
                                '' => ''],
                            'selected' => $person['person']['ttx_candidate_slug']]); ?>
                    </td>
                <?php endif; ?>
                <td class="center">
                    <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                        ['action' => 'edit', $person['person']['id']], ['escape' => false]); ?>
                </td>
            </tr>
            <?php $row++; endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>
