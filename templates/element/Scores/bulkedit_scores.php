<?php if (!empty($scores)): ?>
    <?php echo $this->Form->create($scores, ['class' => 'form-bulkedit']); ?>
    <?php $tenant_org = isset($org['org_tenant_id']) ? true : false ?>
    <?php if ($tenant_org) : ?>
        <div class="alert alert-primary">
            The scores for this organization cannot be edited since they are managed by the configured tenant.
        </div>
    <?php endif ?>
    <table id="ScoresTable" class="table table-striped vcenter">
        <thead class="header">
        <tr>
            <th class="center"><?php echo __('Name'); ?></th>
            <th class="center"><?php echo __('Score'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $row = 0;
        foreach ($scores as $score): ?>
            <tr>
                <?php echo $this->Form->control('id' . '-' . $row, [
                    'name' => 'id',
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'default' => $score['id']]); ?>
                <td><?php echo sprintf('%s, %s', $score->People['last_name'], $score->People['first_name']); ?></td>
                <td><?php echo $this->Form->control('display_text' . '-' . $row, [
                        'name' => 'display_text',
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'text',
                        'disabled' => $tenant_org,
                        'default' => $score['display_text']]); ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
<?php endif; ?>