<div class="page-content container">
    <div class="row">
        <div class="col-md-7 offset-md-3">
            &nbsp;<?php use Cake\Core\Configure;

            echo $this->Flash->render(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7 offset-md-3">
            <div class="login-wrapper">
                <!-- start SHOW_ACCEPT -->
                <?php if ($status === 'SHOW_ACCEPT') : ?>
                    <div class="box">
                        <div class="content-wrap">
                            <div style="margin-bottom: 25px;">
                                <h2>Hi <?php echo $account['user']['email']; ?></h2>
                                <?php
                                if (!empty($account) && Configure::read('users.self_signup_enabled') !== 1) :
                                    ?>
                                    <h4>
                                        <small class="text-muted">You've been invited to manage <b><?php
                                                $tenant = $account['tenant']['name'];
                                                if (strtolower(substr((string)$tenant, -1)) === 's') {
                                                    echo '<b>' . $account['tenant']['name'] . '</b>\'';
                                                } else {
                                                    echo '<b>' . $account['tenant']['name'] . '</b>\'s';
                                                } ?>
                                            </b>
                                            account.
                                        </small>
                                    </h4>
                                <?php endif; ?>
                            </div>

                            <div class="col-md-8 offset-md-2">
                                <div class="signup">
                                    <?= $this->Html->link(__('Accept Invite'),
                                        [
                                            'controller' => 'accounts',
                                            'action' => 'invite',
                                            $account['security_code'],
                                            'ACTIVE'],
                                        ['class' => 'btn btn-lg btn-primary btn-block']);
                                    ?>
                                    <?= $this->Html->link(__('Decline Invite'),
                                        [
                                            'controller' => 'accounts',
                                            'action' => 'invite',
                                            $account['security_code'],
                                            'DISABLED'],
                                        ['class' => 'btn btn-lg btn-outline-danger btn-block']);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end SHOW_ACCEPT -->
                    <!-- start SHOW_LOGOUT -->
                <?php else : ?>
                    <div class="box">
                        <div class="content-wrap">
                            <div style="margin-bottom: 25px;">
                                <h2>This invitation is for <?php echo $account['user']['email']; ?></h2>
                            </div>

                            <div class="col-md-8 offset-md-2">
                                <?= $this->Html->link(__('Log out and try the invitation link again.'),
                                    ['controller' => 'users', 'action' => 'logout', 'invite'],
                                    ['escape' => false]);
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- end SHOW_LOGOUT -->
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
