<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account[]|\Cake\Collection\CollectionInterface $accounts
 */
?>
<div class="accounts index columns col col-lg-5">

    <h3><?= __('Your Accounts') ?></h3>
    <?php if (!empty($accounts)) : ?>

        <p class="text-justify">Easily switch between accounts by clicking one below:</p>
        <div class="list-group">
            <?php foreach ($accounts as $account): ?>
                <?php
                $activeClass = ($currentTenantId === $account->tenant_id ? ' active' : '');
                echo $this->Form->postLink(__($account->tenant_name)
                    , ['controller' => 'Accounts', 'action' => 'setActiveAccount', $account->tenant_id]
                    , ['escape' => false, 'class' => 'list-group-item list-group-item-action' . $activeClass]
                )
                ?>
            <?php endforeach; ?>
        </div>
    <?php else : ?>
        You have no active accounts, please contact an administrator if you think this is a mistake.
    <?php endif; ?>
</div>
