<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account $account
 */
?>
<div class="accounts form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= __('Invite Member') ?></h1>
            </div>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-9">
            <?= $this->Form->create($account) ?>
            <div class="form-group">
                <?php
                echo $this->Form->control('email', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->AccountRole->control('role', ['class' => 'custom-select', 'placeholder' => '']);
                ?>
                <small class="form-text text-muted">User role will not be able to add or remove other users.</small>
            </div>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>
        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->