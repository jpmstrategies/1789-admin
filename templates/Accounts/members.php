<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account[]|\Cake\Collection\CollectionInterface $accounts
 */

use Cake\I18n\FrozenTime;

?>
<div class="accounts index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Members') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span class="octicon octicon-person-add"></span>&nbsp;&nbsp;Invite User')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <ul class="navbar-nav mr-auto">
                <li>
                    <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;Invite Multiple Users')
                        , ['action' => 'bulkAdd']
                        , ['escape' => false, 'class' => 'nav-link'])
                    ?>
                </li>
                <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-desktop-download"></span>&nbsp;&nbsp;Export Users'),
                        ['action' => 'exportCsv'], ['escape' => false, 'class' => 'nav-link']); ?></li>
            </ul>
            <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('created') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('user_id') ?>
                    </th>
                    <?php
                    if (isset($userFields)) {
                        foreach ($userFields as $userField) {
                            ?>
                            <th scope="col"><?php echo $userField['description']; ?></th>
                            <?php
                        }
                    }
                    ?>
                    <th scope="col">
                        <?= $this->Paginator->sort('status') ?>
                    </th>
                    <th scope="col" class="actions" style="width: 120px"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($accounts as $account): ?>
                    <tr>
                        <td>
                            <?php echo $this->Time->format($account->created, 'MM/dd/yyyy') ?>
                        </td>
                        <td>
                            <?= h($account->user->email) ?>
                        </td>
                        <?php
                        if (isset($userFields)) {
                            foreach ($userFields as $userField) {
                                ?>
                                <td><?php echo $account->user->user_details[$userField['name']] ?? null; ?></td>
                                <?php
                            }
                        }
                        ?>
                        <td>
                            <?php echo $this->AccountStatus->display_text($account->status) ?>
                        </td>
                        <td class="actions">
                            <?php
                            $disableLinks = '';
                            if ($currentUserId === $account->user->id) {
                                $disableLinks = ' disabled';
                            }

                            echo $this->Html->link(__('Edit User')
                                , ['action' => 'edit', $account->id]
                                , [
                                    'class' => 'btn btn-outline-secondary btn-sm' . $disableLinks,
                                    'escape' => false,
                                    'style' => "margin-bottom: 5px"]
                            );
                            echo '<br />';

                            // disabled
                            if ($account->status === 'DISABLED') {
                                echo $this->Form->postLink(__('Enable User')
                                    , ['action' => 'enableUser', $account->id]
                                    , [
                                        'class' => 'btn btn-outline-success btn-sm',
                                        'escape' => false,
                                        'style' => "margin-bottom: 5px"]
                                );
                                echo '<br />';
                                echo $this->Form->postLink(__('Delete User')
                                    , ['action' => 'delete', $account->id]
                                    , [
                                        'class' => 'btn btn-outline-danger btn-sm' . $disableLinks,
                                        'escape' => false,
                                        'confirm' => __('Are you sure you want to delete this user?')]
                                );
                            }

                            // pending
                            if ($account->status === 'PENDING') {
                                echo $this->Form->postLink(__('Resend Invite')
                                    , ['action' => 'resendInvite', $account->id]
                                    , [
                                        'class' => 'btn btn-outline-primary btn-sm',
                                        'escape' => false,
                                        'style' => "margin-bottom: 5px"]
                                );
                                echo '<br />';
                                echo $this->Form->postLink(__('Cancel Invite')
                                    , ['action' => 'delete', $account->id]
                                    , [
                                        'class' => 'btn btn-outline-danger btn-sm' . $disableLinks,
                                        'escape' => false,
                                        'confirm' => __('Are you sure you want to cancel this invitation? (You can always send another invitation)')]
                                );
                            }

                            // active
                            if ($account->status === 'ACTIVE') {
                                echo $this->Form->postLink(__('Disable User')
                                    , ['action' => 'disableUser', $account->id]
                                    , [
                                        'class' => 'btn btn-outline-danger btn-sm' . $disableLinks,
                                        'escape' => false,
                                        'confirm' => __('Are you sure you want to revoke access for {0}?', $account->user->email)]
                                );
                            }

                            // review
                            if ($account->status === 'REVIEW') {
                                echo $this->Form->postLink(__('Approve User')
                                    , ['action' => 'approveUser', $account->id]
                                    , [
                                        'class' => 'btn btn-outline-success btn-sm',
                                        'escape' => false,
                                        'style' => "margin-bottom: 5px"]
                                );
                                echo '<br />';

                                echo $this->Form->postLink(__('Disable User')
                                    , ['action' => 'declineUser', $account->id]
                                    , [
                                        'class' => 'btn btn-outline-danger btn-sm',
                                        'escape' => false]
                                );
                            }

                            // approved
                            if ($account->status === 'APPROVED') {
                                echo $this->Form->postLink(__('Disable User')
                                    , ['action' => 'declineUser', $account->id]
                                    , [
                                        'class' => 'btn btn-outline-danger btn-sm',
                                        'escape' => false]
                                );
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
