<?php
// Import 3rd party libraries
$this->Html->script('View/Legislators/add_edit.js?v=20210311', ['block' => true]);
?>
<div class="legislators form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Admin Edit Legislator'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="titles">
                <div class="card">
                    <div class="card-header">Titles</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete'),
                                    ['title' => 'delete', $legislator->id],
                                    [
                                        'escape' => false,
                                        'class' => 'nav-link'], __('Are you sure you want to delete # {0}?',
                                        $legislator->id)); ?></li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Legislators'),
                                    ['title' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?php echo $this->Form->create($legislator); ?>

            <div class="form-group">
                <?php echo $this->Form->control('id', ['class' => 'form-control', 'placeholder' => 'Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('legislative_session_id',
                    ['class' => 'form-control', 'placeholder' => 'Legislative Session Id']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('person_id',
                    [
                        'class' => 'select2-drop-mask',
                        'label' => 'Person',
                        'empty' => '',
                        'options' => $people,
                        'between' => '<br/>',
                        'selected' => $legislator['person_id']]); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('chamber_id',
                    ['class' => 'form-control', 'placeholder' => 'Chamber']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('title_id',
                    ['class' => 'form-control', 'placeholder' => 'Title', 'empty' => true]); ?>
                <small class="form-text text-muted">
                    <?php if (sizeof($titles) === 0) : ?>
                        Optional Feature: No titles are configured for this chamber. These can be configured by clicking
                        <?php echo $this->Html->link(__('here'),
                            ['controller' => 'Titles', 'title' => 'index']); ?>.
                    <?php else : ?>
                        Optional: Select a title based on the Chamber. Add more options
                        <?php echo $this->Html->link(__('here'),
                            ['controller' => 'Titles', 'title' => 'index']); ?>.
                    <?php endif; ?>
                </small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('party_id', ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('district',
                    ['class' => 'form-control', 'placeholder' => 'District', 'type' => 'text']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('journal_name',
                    ['class' => 'form-control', 'placeholder' => 'Journal Name']); ?>
                <small class="form-text text-muted">The text as it is used for mapping votes during import.</small>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('term_starts', [
                    'name' => 'term_starts',
                    'label' => 'Term Starts',
                    'class' => 'form-control',
                    'type' => 'date']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('term_ends', [
                    'name' => 'term_ends',
                    'label' => 'Term Ends',
                    'class' => 'form-control',
                    'type' => 'date']); ?>
            </div>
            <div class="form-group">
                <?php
                if (sizeof($grades->toArray()) > 0) {
                    $type = '';
                    $empty = true;
                } else {
                    $type = 'hidden';
                    $empty = false;
                }
                echo $this->Form->control('grade_id',
                    ['class' => 'form-control', 'placeholder' => 'Grade', 'type' => $type, 'empty' => true]);
                ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->control('grade_number',
                    ['class' => 'form-control', 'placeholder' => 'Number Grade', 'type' => 'text']); ?>
            </div>
            <div class="form-check">
                <?php echo $this->Form->control('is_enabled', ['label' => 'Enabled', 'class' => 'form-check-input']); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->submit(__('Submit'), ['class' => 'btn btn-primary']); ?>
            </div>

            <?php echo $this->Form->end() ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#person-id').select2({
            theme: "bootstrap4",
            placeholder: "- select -",
            allowClear: true,
            width: "100%"
        });
    });
</script>
