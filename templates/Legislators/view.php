<div class="legislators view">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?php echo __('Legislator'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp&nbsp;Edit Legislator'),
                                    ['action' => 'edit', $legislator['id']],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>&nbsp;&nbsp;Delete Legislator'),
                                    ['action' => 'delete', $legislator['id']],
                                    [
                                        'escape' => false,
                                        'class' => 'nav-link'], __('Are you sure you want to delete # {0}?',
                                        $legislator['id'])); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp&nbsp;List Legislators'),
                                    ['action' => 'index'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Legislator'),
                                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Legislative Session'),
                                    ['controller' => 'LegislativeSessions', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                            <li class="nav-item"><?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp&nbsp;New Person'),
                                    ['controller' => 'People', 'action' => 'add'],
                                    ['escape' => false, 'class' => 'nav-link']); ?> </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <table class="table table-striped table-sm">
                <tbody>
                <tr>
                    <th><?php echo __('Legislative Session'); ?></th>
                    <td>
                        <?php echo $this->Html->link($legislator['legislative_session']['name'], [
                            'controller' => 'legislative_sessions',
                            'action' => 'view',
                            $legislator['legislative_session']['id']]); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Person'); ?></th>
                    <td>
                        <?php echo $this->Html->link($legislator['person']['full_name'],
                            ['controller' => 'People', 'action' => 'view', $legislator['person']['id']]); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Chamber'); ?></th>
                    <td>
                        <?php echo $this->Html->link($legislator['chamber']['name'], [
                            'controller' => 'chambers',
                            'action' => 'view',
                            $legislator['chamber']['id']]); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Title'); ?></th>
                    <td>
                        <?php
                        if (isset($legislator['title']['title_long'])) {
                            echo $this->Html->link($legislator['title']['title_long'], [
                                'controller' => 'titles',
                                'action' => 'view',
                                $legislator['title']['id']]);
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Party'); ?></th>
                    <td>
                        <?php echo $this->Html->link($legislator['party']['name'], [
                            'controller' => 'parties',
                            'action' => 'view',
                            $legislator['party']['id']]); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('District'); ?></th>
                    <td>
                        <?php echo h($legislator['district']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Journal Name'); ?></th>
                    <td>
                        <?php echo h($legislator['journal_name']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Term Starts'); ?></th>
                    <td>
                        <?php echo $this->Time->format($legislator['term_starts'], 'MM/dd/yyyy'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Term Ends'); ?></th>
                    <td>
                        <?php echo $this->Time->format($legislator['term_ends'], 'MM/dd/yyyy'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Grade'); ?></th>
                    <td>
                        <?php
                        if (isset($legislator['grade']['name'])) {
                            echo $this->Html->link($legislator['grade']['name'], [
                                'controller' => 'grades',
                                'action' => 'view',
                                $legislator['grade']['id']]);
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Number Grade'); ?></th>
                    <td>
                        <?php echo h($legislator['grade_number']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Enabled'); ?></th>
                    <td>
                        <?php echo $this->Enable->display_text($legislator['is_enabled']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Created'); ?></th>
                    <td>
                        <?php echo h($legislator['created']); ?>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Modified'); ?></th>
                    <td>
                        <?php echo h($legislator['modified']); ?>
                        &nbsp;
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- end col md 9 -->
    </div>
</div>
