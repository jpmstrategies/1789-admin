<div class="legislators index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?php echo __('Legislators'); ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <?php echo $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legislator'),
                    ['action' => 'add'], ['escape' => false, 'class' => 'nav-link']); ?>
            </li>
            <li class="nav-item">
                <?php echo $this->Html->link(__('<span class="octicon octicon-pencil"></span>&nbsp;&nbsp;Bulk Edit Legislators'),
                    ['action' => 'bulkedit', 1], ['escape' => false, 'class' => 'nav-link']); ?>
            </li>
            <li class="nav-item">
                <?php echo $this->Html->link(__('<span class="octicon octicon-desktop-download"></span>&nbsp;&nbsp;CSV Export'),
                    ['action' => 'exportCsv'], ['escape' => false, 'class' => 'nav-link']); ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end row -->
    <div class="row">
        <!-- end col md 3 -->
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('LegislativeSessions.name', 'Legislative Session'); ?></th>
                    <th><?php echo $this->Paginator->sort('People.full_name', 'Person'); ?></th>
                    <th><?php echo $this->Paginator->sort('Chambers.name', 'Chamber'); ?></th>
                    <th><?php echo $this->Paginator->sort('Parties.name', 'Party'); ?></th>
                    <th><?php echo $this->Paginator->sort('Legislators.district', 'District'); ?></th>
                    <th><?php echo $this->Paginator->sort('Legislators.is_enabled', 'Enabled'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($legislators as $legislator): ?>
                    <tr>
                        <td>
                            <?php echo $this->Html->link($legislator['legislative_session']['name'], [
                                'controller' => 'legislative_sessions',
                                'action' => 'view',
                                $legislator['legislative_session']['id']]); ?>
                        </td>
                        <td>
                            <?php echo $this->Html->link($legislator['person']['full_name'],
                                ['controller' => 'People', 'action' => 'view', $legislator['person']['id']]); ?>
                        </td>
                        <td>
                            <?php echo $this->Html->link($legislator['chamber']['name'], [
                                'controller' => 'chambers',
                                'action' => 'view',
                                $legislator['chamber']['id']]); ?>
                        </td>
                        <td>
                            <?php echo $this->Html->link($legislator['party']['name'], [
                                'controller' => 'parties',
                                'action' => 'view',
                                $legislator['party']['id']]); ?>
                        </td>
                        <td><?php echo h($legislator['district']); ?>&nbsp;</td>
                        <td><?php echo $this->Enable->display_text($legislator['is_enabled']); ?>&nbsp;</td>
                        <td class="actions">
                            <?php echo $this->Html->link('<span class="octicon octicon-search"></span>',
                                ['action' => 'view', $legislator['id']],
                                ['escape' => false]); ?>
                            <?php echo $this->Html->link('<span class="octicon octicon-pencil"></span>',
                                ['action' => 'edit', $legislator['id']],
                                ['escape' => false]); ?>
                            <?php echo $this->Form->postLink('<span class="octicon octicon-trashcan"></span>',
                                ['action' => 'delete', $legislator['id']], ['escape' => false],
                                __('Are you sure you want to delete # {0}?', $legislator['id'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->Element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div><!-- end containing of content -->
