<?php

use Cake\Routing\Router;

$this->Html->script('View/bulkedit.js?v=20220816', ['block' => true]);
$this->Html->css('bulkedit.css?v=20220909', ['block' => true]);

?>
<div class="ratings view">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-header">
                <h1><?php echo __('Legislator Bulk Edit'); ?></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <?php echo $this->Form->create(null, ['class' => 'form-horizontal', 'id' => 'FilterAdminBulkeditForm']); ?>
            <input type="hidden" name="_csrfToken" value="<?php echo $_COOKIE['csrfToken']; ?>"/>
            <input type="hidden" name="url"
                   value="<?php echo Router::url(['controller' => 'Legislators', 'action' => 'bulkedit'], true); ?>">
            <input type="hidden" name="saveUrl"
                   value="<?php echo Router::url([
                       'controller' => 'Legislators',
                       'action' => 'bulk-update-row'], true); ?>">
            <div class="row">
                <div class="form-group form-group-sm col-sm-9">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('session',
                                [
                                    'label' => false,
                                    'class' => 'custom-select',
                                    'placeholder' => 'Legislative Session Id',
                                    'default' => $selectedFilters['session']]); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('chamber', [
                                'label' => false,
                                'name' => 'chamber',
                                'class' => 'custom-select',
                                'default' => $selectedFilters['chamber']]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<div class="related row">
    <div class="col-xl-12">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab"
                   aria-controls="details"
                   aria-selected="true">Individual Details</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="term-tab" data-toggle="tab" href="#terms" role="tab"
                   aria-controls="terms" aria-selected="false">Term Details</a>
            </li>
            <?php if ($currentTenantType !== 'D' && $currentTenantType !== 'F') : ?>
                <li class="nav-item">
                    <a class="nav-link" id="scores-tab" data-toggle="tab" href="#scores" role="tab"
                       aria-controls="scores" aria-selected="false">Scores</a>
                </li>
                <?php if ($currentTenantConfigurations['use_criteria'] === 'yes') : ?>
                    <li class="nav-item">
                        <a class="nav-link" id="criteria-tab" data-toggle="tab" href="#criteria" role="tab"
                           aria-controls="criteria" aria-selected="false">Criteria Scores</a>
                    </li>
                <?php endif ?>
                <li class="nav-item">
                    <a class="nav-link" id="metrics-tab" data-toggle="tab" href="#metrics" role="tab"
                       aria-controls="metrics" aria-selected="false">Metrics</a>
                </li>
            <?php endif ?>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                <?php echo $this->element('Legislators/bulkedit_details'); ?>
            </div>
            <div class="tab-pane fade" id="terms" role="tabpanel" aria-labelledby="terms-tab">
                <?php echo $this->element('Legislators/bulkedit_terms'); ?>
            </div>
            <div class="tab-pane fade" id="scores" role="tabpanel" aria-labelledby="scores-tab">
                <?php
                if (empty($calculatedScores['scores'])) {
                    echo "<div class='alert alert-warning'>Please check back later.</div>";
                } else {
                    $exportURL = Router::url(['controller' => 'Legislators', 'action' => 'exportScoreCsv',], true);
                    $exportURL .= '/' . $selectedFilters['session'];
                    $exportURL .= '/' . $selectedFilters['chamber'];

                    echo $this->element('Legislators/bulkedit_scores', [
                        'exportURL' => $exportURL
                    ]);
                }
                ?>
            </div>
            <?php
            if ($currentTenantConfigurations['use_criteria'] === 'yes') :
                ?>
                <div class="tab-pane fade" id="criteria" role="tabpanel" aria-labelledby="criteria-tab">
                    <?php
                    if (empty($calculatedScores['use_criteria'])) {
                        echo "<div class='alert alert-warning'>Please check back later.</div>";
                    } else {
                        $exportURL = Router::url(['controller' => 'Legislators', 'action' => 'exportScoreCsv',], true);
                        $exportURL .= '/' . $selectedFilters['session'];
                        $exportURL .= '/' . $selectedFilters['chamber'];

                        echo $this->element('Legislators/bulkedit_scores_criteria', [
                            'selectedFilters' => $selectedFilters,
                            'exportURL' => $exportURL
                        ]);
                    }
                    ?>
                </div>
            <?php endif; ?>
            <div class="tab-pane fade" id="metrics" role="tabpanel" aria-labelledby="metrics-tab">
                <?php
                echo $this->element('Legislators/bulkedit_metrics');
                ?>
            </div>
        </div>
    </div>
    <!-- end col md 12 -->
</div>
<script type="application/javascript">
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        let targetTableId = (e.target.id === 'scores-tab' ? 'ScoresTable' : 'DetailsTable');
        $('table#' + targetTableId).fixedHeader();
    })
</script>
