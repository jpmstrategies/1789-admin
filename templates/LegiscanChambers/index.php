<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanChamber[]|\Cake\Collection\CollectionInterface $legiscanChambers
 */
?>
<div class="legiscanChambers index">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?= __('Legiscan Chambers') ?></h1>
            </div>
        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
        <ul class="navbar-nav mr-auto">
            <li>
                <?= $this->Html->link(__('<span
                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Chamber')
                    , ['action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link'])
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Chamber
                                ')
                    , ['controller' => 'Chambers', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Subject
                                ')
                    , ['controller' => 'LegiscanBillSubjects', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Vote Detail
                                ')
                    , ['controller' => 'LegiscanBillVoteDetails', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Vote
                                ')
                    , ['controller' => 'LegiscanBillVotes', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
            <li>
                <?= $this->Html->link(__('<span
                                        class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill
                                ')
                    , ['controller' => 'LegiscanBills', 'action' => 'add']
                    , ['escape' => false, 'class' => 'nav-link']
                )
                ?>
            </li>
        </ul>
        <?php /* TODO add a search filter one day
        <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
        </form>
        */ ?>
    </nav>
    <!-- end nav -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">
                        <?= $this->Paginator->sort('body_abbr') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('body_short') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('body_name') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('role_id') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('role_abbr') ?>
                    </th>
                    <th scope="col">
                        <?= $this->Paginator->sort('role_name') ?>
                    </th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($legiscanChambers as $legiscanChamber): ?>
                    <tr>
                        <td>
                            <?= h($legiscanChamber->body_abbr) ?>
                        </td>
                        <td>
                            <?= h($legiscanChamber->body_short) ?>
                        </td>
                        <td>
                            <?= h($legiscanChamber->body_name) ?>
                        </td>
                        <td>
                            <?= $this->Number->format($legiscanChamber->role_id) ?>
                        </td>
                        <td>
                            <?= h($legiscanChamber->role_abbr) ?>
                        </td>
                        <td>
                            <?= h($legiscanChamber->role_name) ?>
                        </td>
                        <td class="actions">
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-search"></span>')
                                , ['action' => 'view', $legiscanChamber->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Html->link(__('<span class="octicon octicon-pencil"></span>')
                                , ['action' => 'edit', $legiscanChamber->id]
                                , ['escape' => false]
                            )
                            ?>
                            <?=
                            $this->Form->postLink(__('<span class="octicon octicon-trashcan"></span>')
                                , ['action' => 'delete', $legiscanChamber->id]
                                , [
                                    'escape' => false,
                                    'confirm' => __('Are you sure you want to delete # {0}?', $legiscanChamber->id)]
                            )
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php
            // Pagination element
            $params = $this->Paginator->params();
            if ($params['pageCount'] > 1) {
                ?>
                <?php echo $this->element('paging'); ?>
            <?php } ?>

        </div>
        <!-- end col md 12 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->
