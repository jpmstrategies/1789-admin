<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LegiscanChamber $legiscanChamber
 */
?>
<div class="legiscanChambers form">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1><?= __('Edit Legiscan Chamber') ?></h1>
            </div>
        </div>
        <!-- end col lg 12 -->
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-md-3">
            <div class="actions">
                <div class="card">
                    <div class="card-header">Actions</div>
                    <div class="card-body">
                        <ul class="nav nav-pills flex-column">
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Chamber
                                                ')
                                    , ['controller' => 'Chambers', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Subject
                                                ')
                                    , ['controller' => 'LegiscanBillSubjects', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Vote Detail
                                                ')
                                    , ['controller' => 'LegiscanBillVoteDetails', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill Vote
                                                ')
                                    , ['controller' => 'LegiscanBillVotes', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-plus"></span>&nbsp;&nbsp;New Legiscan Bill
                                                ')
                                    , ['controller' => 'LegiscanBills', 'action' => 'add']
                                    , ['escape' => false, 'class' => 'nav-link']
                                )
                                ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__('<span class="octicon octicon-list-unordered"></span>&nbsp;&nbsp;List Legiscan Chambers
                                ')
                                    , ['action' => 'index']
                                    , ['escape' => false, 'class' => 'nav-link'])
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- end body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end actions -->
        </div>
        <!-- end col md 3 -->
        <div class="col-md-9">
            <?= $this->Form->create($legiscanChamber) ?>
            <div class="form-group">
                <?php
                echo $this->Form->control('state', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('body_abbr', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('body_short', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('body_name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('role_id', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('role_abbr', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->control('role_name', ['class' => 'form-control', 'placeholder' => '']);
                ?>
            </div>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
            <?php echo $this->Html->link(
                'Cancel',
                ['controller' => 'legiscanChambers', 'action' => 'index', '_full' => true],
                ['class' => 'btn btn-secondary', 'id' => 'cancel']
            ); ?>
            <?= $this->Form->end() ?>
        </div>
        <!-- end col md 9 -->
    </div>
    <!-- end row -->
</div>
<!-- end containing of content -->