<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;

/**
 * App\Controller\ConfigurationOptionsController Test Case
 *
 * @uses \App\Controller\ConfigurationOptionsController
 */
class ConfigurationOptionsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected $configurationOptionsTable;

    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->configurationOptionsTable = TableRegistry::getTableLocator()->get('ConfigurationOptions');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */

    public function testIndexAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/configuration-options/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/configuration-options/index');
        $this->assertAccessDenied();
    }

    public function testIndexAsUser()
    {
        $this->setUserAuth();
        $this->get('/configuration-options/index');
        $this->assertAccessDenied();
    }

    public function testIndexAsPublicUser()
    {
        $this->get('/configuration-options/index');
        $this->assertLoginRedirect('/configuration-options/index');
    }

    /**
     * Test method: view
     */

    public function testViewAsSuperAdmin()
    {
        $id = $this->createTestRecord();

        $this->setSuperAdminAuth();
        $this->get('/configuration-options/view/' . $id);
        $this->assertResponseOk();

        $this->deleteTestRecord($id);
    }

    public function testViewAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/configuration-options/view/7587ddfb-4c58-40ce-a5c6-7767de2434af');
        $this->assertAccessDenied();
    }

    public function testViewAsUser()
    {
        $this->setUserAuth();
        $this->get('/configuration-options/view/7587ddfb-4c58-40ce-a5c6-7767de2434af');
        $this->assertAccessDenied();
    }

    public function testViewAsPublicUser()
    {
        $this->get('/configuration-options/view/7587ddfb-4c58-40ce-a5c6-7767de2434af');
        $this->assertLoginRedirect('/configuration-options/view/7587ddfb-4c58-40ce-a5c6-7767de2434af');
    }

    /**
     * Test method: add
     */

    public function testAddAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/configuration-options/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/configuration-options/add');
        $this->assertAccessDenied();
    }

    public function testAddAsUser()
    {
        $this->setUserAuth();
        $this->get('/configuration-options/add');
        $this->assertAccessDenied();
    }

    public function testAddAsPublicUser()
    {
        $this->get('/configuration-options/add');
        $this->assertLoginRedirect('/configuration-options/add');
    }

    public function testAddPostAsSuperAdminWithValidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/configuration-options/add', [
            'configuration_type_id' => '4f6788eb-d218-4ea9-8a25-0a9420ac6db6',
            'name' => 'JPM Testing Add',
            'value' => 'JPM',
            'sort_order' => 1,
        ]);
        $this->assertFlashMessage('The configuration option has been created.');
        $this->assertRedirect(['action' => 'index']);
    }

    public function testAddPostAsSuperAdminWithInvalidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/configuration-options/add', [
            'sort_order' => 'F'
        ]);
        $this->assertFlashMessage('The configuration option could not be created. Please, try again.');
    }

    /**
     * Test method: edit
     */

    public function testEditAsSuperAdmin()
    {
        $id = $this->createTestRecord();

        $this->setSuperAdminAuth();
        $this->get('/configuration-options/edit/' . $id);
        $this->assertResponseOk();

        $this->deleteTestRecord($id);
    }

    public function testEditAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/configuration-options/edit/7587ddfb-4c58-40ce-a5c6-7767de2434af');
        $this->assertAccessDenied();
    }

    public function testEditAsUser()
    {
        $this->setUserAuth();
        $this->get('/configuration-options/edit/7587ddfb-4c58-40ce-a5c6-7767de2434af');
        $this->assertAccessDenied();
    }

    public function testEditAsPublicUser()
    {
        $this->get('/configuration-options/edit/7587ddfb-4c58-40ce-a5c6-7767de2434af');
        $this->assertLoginRedirect('/configuration-options/edit/7587ddfb-4c58-40ce-a5c6-7767de2434af');
    }

    public function testEditPostAsSuperAdminWithValidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $id = $this->createTestRecord();
        $this->post('/configuration-options/edit/' . $id, [
            'name' => 'JPM Testing' . random_int(0, 500)
        ]);
        $this->assertFlashMessage('The configuration option has been saved.');
        $this->assertRedirect(['action' => 'index']);

        $this->deleteTestRecord($id);
    }

    public function testEditPostAsSuperAdminWithInvalidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $id = $this->createTestRecord();
        $this->post('/configuration-options/edit/' . $id, [
            'sort_order' => 'F'
        ]);
        $this->assertFlashMessage('The configuration option could not be saved. Please, try again.');

        $this->deleteTestRecord($id);
    }

    /**
     * Test method: delete
     */
    public function testDeleteAsSuperAdminSuccess()
    {
        $id = $this->createTestRecord();

        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/configuration-options/delete/' . $id);
        $this->assertFlashMessage('The configuration option has been deleted.');
        $this->assertRedirect(['action' => 'index']);
    }

    public function testDeleteAsSuperAdminFailure()
    {
        // this should fail due to cascading deletes disabled
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $model = $this->getMockForModel('ConfigurationOptions', ['delete']);
        $model->expects($this->once())
            ->method('delete')
            ->will($this->returnValue(false));

        $id = $this->createTestRecord();
        $this->post('/configuration-options/delete/' . $id);
        $this->assertFlashMessage('The configuration option could not be deleted. Please, try again.');
        $this->assertRedirect(['action' => 'index']);

        $this->deleteTestRecord($id);
    }

    public function testDeleteAsAdmin()
    {
        $this->enableCsrfToken();
        $this->setAdminAuth();
        $this->post('/configuration-options/delete/7587ddfb-4c58-40ce-a5c6-7767de2434af');
        $this->assertAccessDenied();
    }

    public function testDeleteAsUser()
    {
        $this->enableCsrfToken();
        $this->setUserAuth();
        $this->post('/configuration-options/delete/7587ddfb-4c58-40ce-a5c6-7767de2434af');
        $this->assertAccessDenied();
    }

    public function testDeleteAsPublicUser()
    {
        $this->enableCsrfToken();
        $this->post('/configuration-options/delete/7587ddfb-4c58-40ce-a5c6-7767de2434af');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $account = $this->configurationOptionsTable->newEntity([
            'configuration_type_id' => '4f6788eb-d218-4ea9-8a25-0a9420ac6db6',
            'name' => 'JPM Testing Add' . random_int(0, 500),
            'value' => 'JPM',
            'sort_order' => 1,
        ]);
        $result = $this->configurationOptionsTable->save($account);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->configurationOptionsTable->get($id);
        return $this->configurationOptionsTable->delete($entity);
    }
}
