<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;

/**
 * App\Controller\Admin\BillsController Test Case
 *
 * @uses \App\Controller\BillsController
 */
class BillsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $BillsTable;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->BillsTable = TableRegistry::getTableLocator()->get('Bills');
        $this->loadRoutes();
    }
    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/Bills/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/Bills/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/Bills/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/Bills/index');
        $this->assertLoginRedirect('/Bills/index');
    }

    /**
     * Test method: view
     */



    public function testViewAsPublicUser(): void
    {
        $this->get('/Bills/view/1');
        $this->assertLoginRedirect('/Bills/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/Bills/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/Bills/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/Bills/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/Bills/add');
        $this->assertLoginRedirect('/Bills/add');
    }

    /**
     * Test method: edit
     */



    public function testEditAsPublicUser(): void
    {
        $this->get('/Bills/edit/1');
        $this->assertLoginRedirect('/Bills/edit/1');
    }

    /**
     * Test method: delete
     */



    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/Bills/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->BillsTable->newEntity([]);
        $result = $this->BillsTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->BillsTable->get($id);
        return $this->BillsTable->delete($entity);
    }
}
