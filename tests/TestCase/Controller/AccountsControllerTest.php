<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;

/**
 * App\Controller\AccountsController Test Case
 *
 * @uses \App\Controller\AccountsController
 */
class AccountsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected $accountsTable;

    public function setUp(): void
    {
        parent::setUp();
        $this->accountsTable = TableRegistry::getTableLocator()->get('Accounts');
        $this->loadRoutes();
    }

    /**
     * controller function: index
     */
    public function testIndexSuperAdminHasAccess(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/accounts');
        $this->assertResponseOk();
    }

    public function testIndexAdminSingleTenantRedirect(): void
    {
        $this->setAdminAuth();
        $this->get('/accounts');
        $this->assertRedirect(['controller' => 'Pages', 'action' => 'home', 'plugin' => false]);
    }

    public function testIndexUserSingleTenantRedirect(): void
    {
        $this->setUserAuth();
        $this->get('/accounts');
        $this->assertRedirect(['controller' => 'Pages', 'action' => 'home', 'plugin' => false]);
    }

    public function testIndexUserSingleTenantCustomRedirect(): void
    {
        Configure::write('users.default_login_redirect', [
            'controller' => 'Pages',
            'action' => 'test',
            'plugin' => false]);

        $this->setUserAuth();
        $this->get('/accounts');
        $this->assertRedirect(['controller' => 'Pages', 'action' => 'test', 'plugin' => false]);
    }

    /**
     * controller function: setActiveAccount
     */
    public function testSetActiveAccountAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->enableCsrfToken();
        unset($_SESSION['Account']);
        $this->assertSessionNotHasKey('Account');
        $this->post('/accounts/set-active-account/6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertSessionHasKey('Account');
        $this->assertSessionHasKey('Configurations');
        $this->assertFlashMessage('Successfully switched memberships!');
        $this->assertRedirect(['controller' => 'Pages', 'action' => 'home', 'plugin' => false]);
    }

    public function testSetActiveAccountAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->enableCsrfToken();
        unset($_SESSION['Account']);
        $this->assertSessionNotHasKey('Account');
        $this->post('/accounts/set-active-account/6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertSessionHasKey('Account');
        $this->assertSessionHasKey('Configurations');
        $this->assertFlashMessage('Successfully switched memberships!');
        $this->assertRedirect(['controller' => 'Pages', 'action' => 'home', 'plugin' => false]);
    }

    public function testSetActiveAccountAsUser(): void
    {
        $this->setUserAuth();
        $this->enableCsrfToken();
        unset($_SESSION['Account']);
        $this->assertSessionNotHasKey('Account');
        $this->post('/accounts/set-active-account/6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertSessionHasKey('Account');
        $this->assertSessionHasKey('Configurations');
        $this->assertFlashMessage('Successfully switched memberships!');
        $this->assertRedirect(['controller' => 'Pages', 'action' => 'home', 'plugin' => false]);
    }

    public function testSetActiveAccountAsUserCustomRedirect(): void
    {
        Configure::write('users.default_login_redirect', [
            'controller' => 'Pages',
            'action' => 'test',
            'plugin' => false]);

        $this->setUserAuth();
        $this->enableCsrfToken();
        unset($_SESSION['Account']);
        $this->assertSessionNotHasKey('Account');
        $this->post('/accounts/set-active-account/6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertSessionHasKey('Account');
        $this->assertSessionHasKey('Configurations');
        $this->assertFlashMessage('Successfully switched memberships!');
        $this->assertRedirect(Configure::read('users.default_login_redirect'));
    }

    public function testSetActiveAccountAsUserUrlRedirect(): void
    {
        $this->setUserAuth();
        $this->enableCsrfToken();
        unset($_SESSION['Account']);
        $this->assertSessionNotHasKey('Account');
        $this->post('/accounts/set-active-account/6f84587a-1d88-11ec-8296-d2ea1fd9e5ee?redirect=/users/view');
        $this->assertSessionHasKey('Account');
        $this->assertSessionHasKey('Configurations');
        $this->assertFlashMessage('Successfully switched memberships!');
        $this->assertRedirect(['controller' => 'Users', 'action' => 'view', 'plugin' => false]);
    }

    public function testSetActiveEnforceTenantOwnership()
    {
        $this->setUserAuth();
        $this->enableCsrfToken();
        $this->post('/accounts/set-active-account/6f845abe-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertFlashMessage('Access denied');
    }

    public function testSetActiveNoTenant()
    {
        $this->setUserAuth();
        $this->enableCsrfToken();
        $this->post('/accounts/set-active-account/');
        $this->assertRedirect(['controller' => 'accounts', 'action' => 'index']);
    }

    /**
     * controller function: members
     */
    public function testMembersListingSuperAdminHasAccess(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/accounts/members');
        $this->assertResponseOk();
    }

    public function testMembersListingAdminHasAccess(): void
    {
        $this->setAdminAuth();
        $this->get('/accounts/members');
        $this->assertResponseOk();
    }

    public function testMemberUserNoAccess(): void
    {
        $this->setUserAuth();
        $this->get('/accounts/members');
        $this->assertAccessDenied();
    }

    /**
     * controller function: add
     */
    public function testAddUserNoAccess(): void
    {
        $this->setUserAuth();
        $this->get('/accounts/add');
        $this->assertAccessDenied();
    }

    public function testAddForm(): void
    {
        $this->setAdminAuth();
        $this->get('/accounts/add');
        $this->assertResponseOk();
    }

    public function testAddNewAccountExistingUser(): void
    {
        $this->setAdminAuth();
        $this->enableCsrfToken();

        $email = 'test@jpmstrategies.com';
        $this->post('/accounts/add', [
            'email' => $email,
            'role' => 'U'
        ]);
        $this->assertFlashMessage('Invitation sent to ' . $email);
    }

    public function testAddNewAccountExistingUserFailure(): void
    {
        $this->setSuperAdminAuth();
        $this->enableCsrfToken();

        $model = $this->getMockForModel('Accounts', ['save']);
        $model->expects($this->once())
            ->method('save')
            ->will($this->returnValue(false));

        $email = 'dev@jpmstrategies.com';
        $this->post('/accounts/add', [
            'email' => $email,
            'role' => 'U'
        ]);
        $this->assertFlashMessage('The invite was not successful. Please, try again.');
    }

    public function testAddNewAccountNewUser(): void
    {
        $this->setAdminAuth();
        $this->enableCsrfToken();

        $email = 'ut@jpmstrategies.com';
        $this->post('/accounts/add', [
            'email' => $email,
            'role' => 'U'
        ]);
        $this->assertFlashMessage('Invitation sent to ' . $email);
    }

    public function testAddNewAccountNewUserFailure(): void
    {
        $this->setAdminAuth();
        $this->enableCsrfToken();

        $model = $this->getMockForModel('Users', ['save']);
        $model->expects($this->once())
            ->method('save')
            ->will($this->returnValue(false));

        $email = 'failure@jpmstrategies.com';
        $this->post('/accounts/add', [
            'email' => $email,
            'role' => 'U'
        ]);
        $this->assertFlashMessage('The invite was not successful. Please, try again.');
    }

    public function testAddNewAccountAlreadyMember(): void
    {
        $this->setAdminAuth();
        $this->enableCsrfToken();

        $email = 'jeff.morris@outlook.com';
        $this->post('/accounts/add', [
            'email' => $email,
            'role' => 'U'
        ]);
        $this->assertFlashMessage($email . ' is already a member!');
    }

    /**
     * controller function: bulkAdd
     */
    public function testBulkAddUserNoAccess(): void
    {
        $this->setUserAuth();
        $this->get('/accounts/bulk-add');
        $this->assertAccessDenied();
    }

    public function testBulkAddUserAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->enableCsrfToken();
        $this->get('/accounts/bulk-add');
        $this->assertResponseOk();
    }

    public function testBulkAddUserAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->enableCsrfToken();
        $this->get('/accounts/bulk-add');
        $this->assertResponseOk();
    }

    public function testBulkAddUserValidPostAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->enableCsrfToken();
        $this->post('/accounts/bulk-add', [
            'role' => 'U',
            'emails' => 'bulk@test.com']);
        $this->assertFlashMessage('Bulk invite successful. Email sending can take up to 15 minutes to complete.');
    }

    public function testBulkAddUserValidPostMultipleAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->enableCsrfToken();
        $this->post('/accounts/bulk-add', [
            'role' => 'U',
            'emails' => 'bulk1@test.com\r\nbulk2@test.com']);
        $this->assertFlashMessage('Bulk invite successful. Email sending can take up to 15 minutes to complete.');
    }

    public function testBulkAddUserInvalidPostAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->enableCsrfToken();
        $this->post('/accounts/bulk-add', [
            'role' => 'U',
            'emails' => 'bulk']);
        $this->assertFlashMessage('Invalid email address: bulk');
    }

    public function testBulkAddUserValidPostAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->enableCsrfToken();
        $this->post('/accounts/bulk-add', [
            'role' => 'U',
            'emails' => 'bulk@test.com']);
        $this->assertFlashMessage('Bulk invite successful. Email sending can take up to 15 minutes to complete.');
    }

    public function testBulkAddUserValidPostMultipleAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->enableCsrfToken();
        $this->post('/accounts/bulk-add', [
            'role' => 'U',
            'emails' => 'bulk1@test.com\r\nbulk2@test.com']);
        $this->assertFlashMessage('Bulk invite successful. Email sending can take up to 15 minutes to complete.');
    }

    public function testBulkAddUserInvalidPostAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->enableCsrfToken();
        $this->post('/accounts/bulk-add', [
            'role' => 'U',
            'emails' => 'bulk']);
        $this->assertFlashMessage('Invalid email address: bulk');
    }

    /**
     * controller function: addMember
     */
    /**
     * controller function: edit
     */
    public function testEditUserNoAccess(): void
    {
        $this->setUserAuth();
        $this->get('/accounts/edit/1b3ddfda-2dae-4f3c-a2c3-eec5ce719457');
        $this->assertAccessDenied();
    }

    public function testEditAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->enableCsrfToken();

        // test editing record from your tenant id
        $this->get('/accounts/edit/b043e329-f489-4ab2-8c49-183340312e92');
        $this->assertResponseOk();

        $this->post('/accounts/edit/b043e329-f489-4ab2-8c49-183340312e92', [
            'role' => 'A',
        ]);
        $this->assertFlashMessage('The member has been saved.');

        // test editing record from different tenant id
        $this->get('/accounts/edit/3597a073-fd9c-4b13-917a-8f947249dedc');
        $this->assertFlashMessage('Access denied');

        $this->post('/accounts/edit/3597a073-fd9c-4b13-917a-8f947249dedc', [
            'role' => 'U',
        ]);
        $this->assertFlashMessage('Access denied');
    }

    public function testEditAsAdminFailure(): void
    {
        $this->setAdminAuth();
        $this->enableCsrfToken();
        $this->enableRetainFlashMessages();

        $model = $this->getMockForModel('Accounts', ['save']);
        $model->expects($this->once())
            ->method('save')
            ->will($this->returnValue(false));

        $this->post('/accounts/edit/b043e329-f489-4ab2-8c49-183340312e92', [
            'role' => 'A',
        ]);
        $this->assertFlashMessage('The member could not be saved. Please, try again.');
    }

    public function testEditOwnRecordPrevented(): void
    {
        $this->setAdminAuth();
        $this->get('/accounts/edit/1b3ddfda-2dae-4f3c-a2c3-eec5ce719457');
        $this->assertFlashMessage('You cannot manage your own membership!');
    }

    /**
     * controller function: delete
     */
    public function testDeleteUserNoAccess(): void
    {
        $this->setUserAuth();
        $this->enableCsrfToken();
        $this->post('/accounts/delete/1b3ddfda-2dae-4f3c-a2c3-eec5ce719457');
        $this->assertAccessDenied();
    }

    public function testDeleteAsAdmin(): void
    {
        // add record for testing
        $id = $this->createTestRecord();

        $this->setAdminAuth();
        $this->enableCsrfToken();
        $this->post('/accounts/delete/' . $id);
        $this->assertFlashMessage('The membership has been revoked.');
        $this->assertRedirect(['action' => 'members']);
    }

    public function testDeleteAsAdminFailure(): void
    {
        // add record for testing
        $id = $this->createTestRecord();

        $this->setAdminAuth();
        $this->enableCsrfToken();

        $model = $this->getMockForModel('Accounts', ['delete']);
        $model->expects($this->once())
            ->method('delete')
            ->will($this->returnValue(false));

        $this->post('/accounts/delete/' . $id);
        $this->assertFlashMessage('The membership could not be revoked. Please, try again.');
        $this->assertRedirect(['action' => 'members']);

        $this->deleteTestRecord($id);
    }

    public function testDeleteOwnRecordPrevented(): void
    {
        $this->setAdminAuth();
        $this->enableCsrfToken();
        $this->post('/accounts/delete/1b3ddfda-2dae-4f3c-a2c3-eec5ce719457');
        $this->assertFlashMessage('You cannot manage your own membership!');
    }

    /**
     * controller function: invite
     */

    /**
     * controller function: resendInvite
     */
    public function testResendUserNoAccess(): void
    {
        $this->setUserAuth();
        $this->get('/accounts/resendInvite/1b3ddfda-2dae-4f3c-a2c3-eec5ce719457');
        $this->assertAccessDenied();
    }

    public function testResendInviteAsAdmin(): void
    {
        $id = $this->createTestRecord();

        $this->setAdminAuth();
        $this->get('/accounts/resendInvite/' . $id);
        $this->assertMailSubjectContains('been invited to join and manage');
        $this->deleteTestRecord($id);
    }

    /**
     * controller function: sendMemberInvite
     */
    public function testInvalidInvite(): void
    {
        // test user provides an invalid security code
        $this->get('/accounts/verify/asdfasdf');
        $this->assertFlashMessage('Invalid or expired invite token. Please check your email or try again.');
    }

    public function testValidInviteExistingUser(): void
    {
        // test user already has an active account, redirect to login
        $this->setPublicAuth();
        $this->get('/accounts/verify/9bed23f4-f544-11eb-9a03-0242ac130003');
        $this->assertRedirect(['controller' => 'users', 'action' => 'finishSignup', 'plugin' => false]);
        $this->assertSession(true, 'Invite.user.is_enabled');
    }

    public function testValidInviteNewUser(): void
    {
        $id = $this->createTestRecord();

        // test user does not have an active account, redirect to signup
        $this->get('/accounts/verify/fb33fd26-f547-11eb-9a03-0242ac130003');
        $this->assertRedirect(['controller' => 'users', 'action' => 'finishSignup', 'plugin' => false]);
        $this->assertSession(false, 'Invite.user.is_enabled');

        $this->deleteTestRecord($id);
    }

    public function testValidInviteUserLoggedIn(): void
    {
        // test correct user logged in
        $this->setUserAuth();
        $this->get('/accounts/verify/9bed23f4-f544-11eb-9a03-0242ac130003');
        $this->assertRedirect(['controller' => 'users', 'action' => 'finishSignup', 'plugin' => false]);
        $this->assertSessionHasKey('Auth.User');
    }

    public function testValidInviteUserLoggedInWrongUser(): void
    {
        $id = $this->createTestRecord();

        // test wrong user logged in
        $this->setAdminAuth();
        $this->get('/accounts/verify/fb33fd26-f547-11eb-9a03-0242ac130003');
        $this->assertRedirect(['controller' => 'users', 'action' => 'finishSignup', 'plugin' => false]);
        $this->deleteTestRecord($id);
    }

    public function testValidInviteSuccessSaveSuccess(): void
    {
        $id = $this->createTestRecord();

        // delete invite after successful signup redirect
        $this->get('/accounts/invite/fb33fd26-f547-11eb-9a03-0242ac130003/A');
        $this->assertSessionNotHasKey('Invite');

        $this->deleteTestRecord($id);
    }

    public function testValidInviteSuccessSaveFailure(): void
    {
        $this->enableRetainFlashMessages();
        $id = $this->createTestRecord();

        $model = $this->getMockForModel('Accounts', ['save']);
        $model->expects($this->once())
            ->method('save')
            ->will($this->returnValue(false));

        // delete invite after successful signup redirect
        $this->get('/accounts/invite/fb33fd26-f547-11eb-9a03-0242ac130003/A');
        $this->assertFlashMessage('Invitation was not saved. Please, try again.');

        $this->deleteTestRecord($id);
    }

    /**
     * Test method: exportCsv
     */
    public function testExportAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/accounts/export-csv');
        $this->assertResponseOk();
    }

    public function testExportAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/accounts/export-csv');
        $this->assertResponseOk();
    }

    public function testExportAsUser()
    {
        $this->setUserAuth();
        $this->get('/accounts/export-csv');
        $this->assertAccessDenied();
    }

    public function testExportAsPublicUser()
    {
        $this->get('/accounts/export-csv');
        $this->assertLoginRedirect('/accounts/export-csv');
    }

    public function testExportAsSuperAdminWithCustomFields()
    {
        $this->setSuperAdminAuth();
        $customFieldId = $this->createCustomField();

        $this->get('/accounts/export-csv');
        $this->assertResponseOk();

        $this->deleteCustomField($customFieldId);
    }

    public function testExportAsAdminWithCustomFields()
    {
        $this->setAdminAuth();
        $customFieldId = $this->createCustomField();

        $this->get('/accounts/export-csv');
        $this->assertResponseOk();

        $this->deleteCustomField($customFieldId);
    }

    public function testVerifyNoSecurityCode()
    {
        $this->get('/accounts/invite/invalid-security-code');
        $this->assertFlashMessage('Invalid or expired invite token. Please check your email or try again.');
        $this->assertRedirect(['controller' => 'users', 'action' => 'login', 'plugin' => false]);

        $this->get('/accounts/verify/invalid-security-code');
        $this->assertFlashMessage('Invalid or expired invite token. Please check your email or try again.');
        $this->assertRedirect(['controller' => 'users', 'action' => 'login', 'plugin' => false]);
    }

    private function createTestRecord()
    {
        $account = $this->accountsTable->newEntity([
            'tenant_id' => '6f84587a-1d88-11ec-8296-d2ea1fd9e5ee',
            'user_id' => '84f04c46-1d88-11ec-8296-d2ea1fd9e5ee',
            'role' => 'U',
            'status' => 'PENDING',
            'security_code' => 'fb33fd26-f547-11eb-9a03-0242ac130003'
        ]);
        $result = $this->accountsTable->save($account);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->accountsTable->get($id);
        return $this->accountsTable->delete($entity);
    }
}
