<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\AddressTypesController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\AddressTypesController Test Case
 *
 * @uses \App\Controller\AddressTypesController
 */
class AddressTypesControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $AddressTypesTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->AddressTypesTable = TableRegistry::getTableLocator()->get('AddressTypes');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/AddressTypes/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/AddressTypes/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/AddressTypes/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/AddressTypes/index');
        $this->assertLoginRedirect('/AddressTypes/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/AddressTypes/view/1');
        $this->assertLoginRedirect('/AddressTypes/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/AddressTypes/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/AddressTypes/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/AddressTypes/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/AddressTypes/add');
        $this->assertLoginRedirect('/AddressTypes/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/AddressTypes/edit/1');
        $this->assertLoginRedirect('/AddressTypes/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/AddressTypes/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->AddressTypesTable->newEntity([]);
        $result = $this->AddressTypesTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->AddressTypesTable->get($id);
        return $this->AddressTypesTable->delete($entity);
    }
}
