<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\GradesController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\GradesController Test Case
 *
 * @uses \App\Controller\GradesController
 */
class GradesControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $GradesTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->GradesTable = TableRegistry::getTableLocator()->get('Grades');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/Grades/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/Grades/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/Grades/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/Grades/index');
        $this->assertLoginRedirect('/Grades/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/Grades/view/1');
        $this->assertLoginRedirect('/Grades/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/Grades/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/Grades/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/Grades/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/Grades/add');
        $this->assertLoginRedirect('/Grades/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/Grades/edit/1');
        $this->assertLoginRedirect('/Grades/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/Grades/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->GradesTable->newEntity([]);
        $result = $this->GradesTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->GradesTable->get($id);
        return $this->GradesTable->delete($entity);
    }
}
