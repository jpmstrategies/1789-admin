<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\ConfigurationsController;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Laminas\Diactoros\UploadedFile;

/**
 * App\Controller\ConfigurationsController Test Case
 *
 * @uses \App\Controller\ConfigurationsController
 */
class ConfigurationsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected $configurationsTable;

    public function setUp(): void
    {
        parent::setUp();
        $this->configurationsTable = TableRegistry::getTableLocator()->get('Configurations');
        $this->loadRoutes();
    }

    /**
     * Test method: bulkUpdateRow
     */

    public function testBulkUpdateRowNonPostReturnsResponseCode405()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->get('/configurations/bulk-update-row');
        $this->assertResponseCode(405);
    }

    public function testBulkUpdateRowAsSuperAdminWithValidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $results = $this->configurationsTable->find('all', ['currentTenantId' => '6f84587a-1d88-11ec-8296-d2ea1fd9e5ee'])->first();
        $this->assertNotNull($results['id']);

        $this->post('/configurations/bulk-update-row', [
            'id' => strval($results['id']),
            'value' => '55'
        ]);
        $this->assertResponseOk();
    }

    public function testBulkUpdateRowAsSuperAdminWithFailure()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $results = $this->configurationsTable->find('all', ['currentTenantId' => '6f84587a-1d88-11ec-8296-d2ea1fd9e5ee'])->first();
        $this->assertNotNull($results['id']);

        $model = $this->getMockForModel('Configurations', ['save']);
        $model->expects($this->once())
            ->method('save')
            ->will($this->returnValue(false));
        $this->post('/configurations/bulk-update-row', [
            'id' => strval($results['id']),
            'value' => '55'
        ]);
        $this->assertResponseCode(400);
    }

    public function testBulkUpdateRowAsAdminWithValidPayload()
    {
        $this->enableCsrfToken();
        $this->setAdminAuth();

        $results = $this->configurationsTable->find('all', ['currentTenantId' => '6f84587a-1d88-11ec-8296-d2ea1fd9e5ee'])->first();
        $this->assertNotNull($results['id']);

        $this->post('/configurations/bulk-update-row', [
            'id' => strval($results['id']),
            'value' => '55'
        ]);
        $this->assertResponseOk();
    }

    public function testBulkUpdateRowAsUser()
    {
        $this->enableCsrfToken();
        $this->setUserAuth();
        $this->post('/configurations/bulk-update-row', [
            'id' => 'ec946a6e-3c76-45e7-ac11-0e6d235df311',
            'value' => '55'
        ]);
        $this->assertAccessDenied();
    }

    public function testBulkUpdateRowAsPublicUser()
    {
        $this->enableCsrfToken();
        $this->post('/configurations/bulk-update-row', [
            'id' => 'ec946a6e-3c76-45e7-ac11-0e6d235df311',
            'value' => '55'
        ]);
        $this->assertLoginRedirect(null);
    }

    /**
     * Test method: getTenantConfigurations
     */

    public function testGetTenantConfigurations()
    {
        $configurationsController = new ConfigurationsController();
        $options = $configurationsController->getTenantConfigurations('6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertNotEmpty($options);
    }

    /**
     * Test method: index
     */

    public function testIndexAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/configurations/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/configurations/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser()
    {
        $this->setUserAuth();
        $this->get('/configurations/index');
        $this->assertAccessDenied();
    }

    public function testIndexAsPublicUser()
    {
        $this->get('/configurations/index');
        $this->assertLoginRedirect('/configurations/index');
    }

    /**
     * Test method: export
     */
    public function testExportAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/configurations/export');
        $this->assertResponseOk();
    }

    public function testExportAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/configurations/export');
        $this->assertAccessDenied();
    }

    public function testExportAsUser()
    {
        $this->setUserAuth();
        $this->get('/configurations/export');
        $this->assertAccessDenied();
    }

    public function testExportAsPublicUser()
    {
        $this->get('/configurations/export');
        $this->assertLoginRedirect('/configurations/export');
    }

    /**
     * Test method: import
     */

    public function testImportGetAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/configurations/import');
        $this->assertResponseOk();
    }

    public function testImportGetAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/configurations/import');
        $this->assertAccessDenied();
    }

    public function testImportGetAsUser()
    {
        $this->setUserAuth();
        $this->get('/configurations/import');
        $this->assertAccessDenied();
    }

    public function testImportGetAsPublicUser()
    {
        $this->get('/configurations/import');
        $this->assertLoginRedirect('/configurations/import');
    }

    public function testImportPostAsSuperAdminWithSuccess()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $configurationUpload = new UploadedFile(
            'tests/data/configurations.json',
            247,
            0,
            'configurations.json',
            'application/json'
        );

        $this->configRequest([
            'files' => [
                'file' => $configurationUpload,
            ],
        ]);

        $this->post('/configurations/import', [
            'file' => $configurationUpload
        ]);
        $this->assertFlashMessage('Invalid configuration: Invalid Configuration');
        $this->assertFlashMessage('Bulk import was successful.');
        $this->assertRedirect(['controller' => 'Configurations', 'action' => 'index']);

        $results = $this->configurationsTable->find()
            ->where(['value' => 'IMPORT TEST']);
        $this->assertGreaterThan(0, sizeof($results->toArray()));
    }

    public function testImportPostAsSuperAdminWithFailure()
    {
        $this->enableRetainFlashMessages();
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $configurationUpload = new UploadedFile(
            'tests/data/configurations.json',
            247,
            0,
            'configurations.json',
            'application/json'
        );

        $this->configRequest([
            'files' => [
                'file' => $configurationUpload,
            ],
        ]);

        $model = $this->getMockForModel('Configurations', ['saveManyOrFail']);
        $model->expects($this->once())
            ->method('saveManyOrFail')
            ->will(static::throwException(new \Exception()));
        $this->post('/configurations/import', [
            'file' => $configurationUpload
        ]);
        $this->assertFlashMessage('The configurations could not be imported. Please, try again.');

        // make sure no updates happened
        $results = $this->configurationsTable->find()
            ->where(['value' => 'IMPORT TEST']);
        $this->assertEquals(0, sizeof($results->toArray()));

        // make sure configuration records exist since they are bulk deleted
        $results = $this->configurationsTable->find();
        $this->assertGreaterThan(0, sizeof($results->toArray()));
    }

    public function testImportPostAsAdmin()
    {
        $this->enableCsrfToken();
        $this->setAdminAuth();
        $this->post('/configurations/import');
        $this->assertAccessDenied();
    }

    public function testImportPostAsUser()
    {
        $this->enableCsrfToken();
        $this->setUserAuth();
        $this->post('/configurations/import');
        $this->assertAccessDenied();
    }

    public function testImportPostAsPublicUser()
    {
        $this->enableCsrfToken();
        $this->post('/configurations/import');
        $this->assertLoginRedirect(null);
    }

}
