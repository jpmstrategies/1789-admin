<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestTrait;

/**
 * App\Controller\PagesController Test Case
 *
 * @uses \App\Controller\PagesController
 */
class PagesControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    /**
     * Test method: display
     */

    public function testDisplayAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/pages/display');
        $this->assertResponseOk();
    }

    public function testDisplayAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/pages/display');
        $this->assertResponseOk();
    }

    public function testDisplayAsUser()
    {
        $this->setUserAuth();
        $this->get('/pages/display');
        $this->assertResponseOk();
    }

    public function testDisplayAsPublicUser()
    {
        $this->get('/pages/display');
        $this->assertLoginRedirect('/pages/display');
    }

    /**
     * Test method: terms
     */

    public function testTermsAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/terms');
        $this->assertResponseOk();
    }

    public function testTermsAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/terms');
        $this->assertResponseOk();
    }

    public function testTermsAsUser()
    {
        $this->setUserAuth();
        $this->get('/terms');
        $this->assertResponseOk();
    }

    public function testTermsAsPublicUser()
    {
        $this->get('/terms');
        $this->assertResponseOk();
    }

    /**
     * Test method: privacyPolicy
     */

    public function testPrivacyPolicyAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/privacy-policy');
        $this->assertResponseOk();
    }

    public function testPrivacyPolicyAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/privacy-policy');
        $this->assertResponseOk();
    }

    public function testPrivacyPolicyAsUser()
    {
        $this->setUserAuth();
        $this->get('/privacy-policy');
        $this->assertResponseOk();
    }

    public function testPrivacyPolicyAsPublicUser()
    {
        $this->get('/privacy-policy');
        $this->assertResponseOk();
    }

    /**
     * Test method: cookiePolicy
     */

    public function testCookiePolicyAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/cookie-policy');
        $this->assertResponseOk();
    }

    public function testCookiePolicyAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/cookie-policy');
        $this->assertResponseOk();
    }

    public function testCookiePolicyAsUser()
    {
        $this->setUserAuth();
        $this->get('/cookie-policy');
        $this->assertResponseOk();
    }

    public function testCookiePolicyAsPublicUser()
    {
        $this->get('/cookie-policy');
        $this->assertResponseOk();
    }

}
