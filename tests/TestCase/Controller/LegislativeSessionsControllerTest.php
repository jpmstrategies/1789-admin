<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\LegislativeSessionsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\LegislativeSessionsController Test Case
 *
 * @uses \App\Controller\LegislativeSessionsController
 */
class LegislativeSessionsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $LegislativeSessionsTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->LegislativeSessionsTable = TableRegistry::getTableLocator()->get('LegislativeSessions');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/LegislativeSessions/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/LegislativeSessions/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/LegislativeSessions/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/LegislativeSessions/index');
        $this->assertLoginRedirect('/LegislativeSessions/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/LegislativeSessions/view/1');
        $this->assertLoginRedirect('/LegislativeSessions/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/LegislativeSessions/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/LegislativeSessions/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/LegislativeSessions/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/LegislativeSessions/add');
        $this->assertLoginRedirect('/LegislativeSessions/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/LegislativeSessions/edit/1');
        $this->assertLoginRedirect('/LegislativeSessions/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/LegislativeSessions/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->LegislativeSessionsTable->newEntity([]);
        $result = $this->LegislativeSessionsTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->LegislativeSessionsTable->get($id);
        return $this->LegislativeSessionsTable->delete($entity);
    }
}
