<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\OrganizationsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\OrganizationsController Test Case
 *
 * @uses \App\Controller\OrganizationsController
 */
class OrganizationsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $OrganizationsTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->OrganizationsTable = TableRegistry::getTableLocator()->get('Organizations');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/Organizations/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/Organizations/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/Organizations/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/Organizations/index');
        $this->assertLoginRedirect('/Organizations/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/Organizations/view/1');
        $this->assertLoginRedirect('/Organizations/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/Organizations/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/Organizations/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/Organizations/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/Organizations/add');
        $this->assertLoginRedirect('/Organizations/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/Organizations/edit/1');
        $this->assertLoginRedirect('/Organizations/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/Organizations/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->OrganizationsTable->newEntity([]);
        $result = $this->OrganizationsTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->OrganizationsTable->get($id);
        return $this->OrganizationsTable->delete($entity);
    }
}
