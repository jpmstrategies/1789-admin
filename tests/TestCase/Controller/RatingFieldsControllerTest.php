<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\RatingFieldsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\RatingFieldsController Test Case
 *
 * @uses \App\Controller\RatingFieldsController
 */
class RatingFieldsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $RatingFieldsTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->RatingFieldsTable = TableRegistry::getTableLocator()->get('RatingFields');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/RatingFields/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/RatingFields/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/RatingFields/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/RatingFields/index');
        $this->assertLoginRedirect('/RatingFields/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/RatingFields/view/1');
        $this->assertLoginRedirect('/RatingFields/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/RatingFields/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/RatingFields/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/RatingFields/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/RatingFields/add');
        $this->assertLoginRedirect('/RatingFields/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/RatingFields/edit/1');
        $this->assertLoginRedirect('/RatingFields/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/RatingFields/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->RatingFieldsTable->newEntity([]);
        $result = $this->RatingFieldsTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->RatingFieldsTable->get($id);
        return $this->RatingFieldsTable->delete($entity);
    }
}
