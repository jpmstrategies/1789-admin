<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;

/**
 * App\Controller\ConfigurationTypesController Test Case
 *
 * @uses \App\Controller\ConfigurationTypesController
 */
class ConfigurationTypesControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected $configurationTypesTable;

    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->configurationTypesTable = TableRegistry::getTableLocator()->get('ConfigurationTypes');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */

    public function testIndexAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/configuration-types/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/configuration-types/index');
        $this->assertAccessDenied();
    }

    public function testIndexAsUser()
    {
        $this->setUserAuth();
        $this->get('/configuration-types/index');
        $this->assertAccessDenied();
    }

    public function testIndexAsPublicUser()
    {
        $this->get('/configuration-types/index');
        $this->assertLoginRedirect('/configuration-types/index');
    }

    /**
     * Test method: view
     */

    public function testViewAsSuperAdmin()
    {
        $this->setSuperAdminAuth();

        $id = $this->createTestRecord();
        $this->get('/configuration-types/view/' . $id);
        $this->assertResponseOk();

        $this->deleteTestRecord($id);
    }

    public function testViewAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/configuration-types/view/b1e85392-4fd9-4734-bbe4-db3137adb9ad');
        $this->assertAccessDenied();
    }

    public function testViewAsUser()
    {
        $this->setUserAuth();
        $this->get('/configuration-types/view/b1e85392-4fd9-4734-bbe4-db3137adb9ad');
        $this->assertAccessDenied();
    }

    public function testViewAsPublicUser()
    {
        $this->get('/configuration-types/view/b1e85392-4fd9-4734-bbe4-db3137adb9ad');
        $this->assertLoginRedirect('/configuration-types/view/b1e85392-4fd9-4734-bbe4-db3137adb9ad');
    }

    /**
     * Test method: insertConfigurations
     */

    public function testInsertConfigurationsAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/configuration-types/insert-configurations/1/tenant');
        $this->assertRedirect(['action' => 'index']);
    }

    public function testInsertConfigurationsAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/configuration-types/insert-configurations/1/tenant');
        $this->assertRedirect(['action' => 'index']);
    }

    public function testInsertConfigurationsAsUser()
    {
        $this->setUserAuth();
        $this->get('/configuration-types/insert-configurations/1/tenant');
        $this->assertRedirect(['action' => 'index']);
    }

    public function testInsertConfigurationsAsPublicUser()
    {
        $this->get('/configuration-types/insert-configurations/b1e85392-4fd9-4734-bbe4-db3137adb9ad');
        $this->assertLoginRedirect('/configuration-types/insert-configurations/b1e85392-4fd9-4734-bbe4-db3137adb9ad');
    }

    /**
     * Test method: add
     */

    public function testAddAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/configuration-types/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/configuration-types/add');
        $this->assertAccessDenied();
    }

    public function testAddAsUser()
    {
        $this->setUserAuth();
        $this->get('/configuration-types/add');
        $this->assertAccessDenied();
    }

    public function testAddAsPublicUser()
    {
        $this->get('/configuration-types/add');
        $this->assertLoginRedirect('/configuration-types/add');
    }

    public function testAddPostAsSuperAdminWithValidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/configuration-types/add', [
            'scope' => 'U',
            'name' => 'JPM Testing',
            'description' => 'Lorem ipsum dolor sit amet',
            'data_type' => 'text',
            'default_value' => 'JPM',
            'tab' => 'admin',
            'is_editable' => 1,
            'is_enabled' => 1,
            'is_public' => 1,
        ]);
        $this->assertFlashMessage('The configuration type has been created.');
        $this->assertRedirect(['action' => 'index']);
    }

    public function testAddPostAsSuperAdminWithInvalidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/configuration-types/add', [
            'is_enabled' => 'F'
        ]);
        $this->assertFlashMessage('The configuration type could not be created. Please, try again.');
    }

    /**
     * Test method: edit
     */

    public function testEditAsSuperAdmin()
    {
        $this->setSuperAdminAuth();

        $id = $this->createTestRecord();
        $this->get('/configuration-types/edit/' . $id);
        $this->assertResponseOk();

        $this->deleteTestRecord($id);
    }

    public function testEditAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/configuration-types/edit/b1e85392-4fd9-4734-bbe4-db3137adb9ad');
        $this->assertAccessDenied();
    }

    public function testEditAsUser()
    {
        $this->setUserAuth();
        $this->get('/configuration-types/edit/b1e85392-4fd9-4734-bbe4-db3137adb9ad');
        $this->assertAccessDenied();
    }

    public function testEditAsPublicUser()
    {
        $this->get('/configuration-types/edit/b1e85392-4fd9-4734-bbe4-db3137adb9ad');
        $this->assertLoginRedirect('/configuration-types/edit/b1e85392-4fd9-4734-bbe4-db3137adb9ad');
    }

    public function testEditPostAsSuperAdminWithValidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $id = $this->createTestRecord();
        $this->post('/configuration-types/edit/' . $id, [
            'name' => 'JPM Testing' . random_int(0, 500)
        ]);
        $this->assertFlashMessage('The configuration type has been saved.');
        $this->assertRedirect(['action' => 'index']);

        $this->deleteTestRecord($id);
    }

    public function testEditPostAsSuperAdminWithInvalidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $id = $this->createTestRecord();
        $this->post('/configuration-types/edit/' . $id, [
            'is_enabled' => 'F'
        ]);
        $this->assertFlashMessage('The configuration type could not be saved. Please, try again.');

        $this->deleteTestRecord($id);
    }

    /**
     * Test method: delete
     */
    public function testDeleteAsSuperAdminSuccess()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $id = $this->createTestRecord();
        $this->post('/configuration-types/delete/' . $id);
        $this->assertFlashMessage('The configuration type has been deleted.');
        $this->assertRedirect(['action' => 'index']);
    }

    public function testDeleteAsSuperAdminFailure()
    {
        // this should fail due to cascading deletes disabled
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $model = $this->getMockForModel('ConfigurationTypes', ['delete']);
        $model->expects($this->once())
            ->method('delete')
            ->will($this->returnValue(false));

        $id = $this->createTestRecord();
        $this->post('/configuration-types/delete/' . $id);
        $this->assertFlashMessage('The configuration type could not be deleted. Please, try again.');
        $this->assertRedirect(['action' => 'index']);

        $this->deleteTestRecord($id);
    }

    public function testDeleteAsSuperAdminSaveCatch()
    {
        // this should fail due to cascading deletes disabled
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $model = $this->getMockForModel('ConfigurationTypes', ['delete']);
        $model->expects($this->once())
            ->method('delete')
            ->will(static::throwException(new \Exception()));

        $id = $this->createTestRecord();
        $this->post('/configuration-types/delete/' . $id);
        $this->assertFlashMessage('The configuration type you are trying to delete is associated with other records. Please, try again after deleting the related records.');
        $this->assertRedirect(['action' => 'index']);

        $this->deleteTestRecord($id);
    }

    public function testDeleteAsAdmin()
    {
        $this->enableCsrfToken();
        $this->setAdminAuth();
        $this->post('/configuration-types/delete/b1e85392-4fd9-4734-bbe4-db3137adb9ad');
        $this->assertAccessDenied();
    }

    public function testDeleteAsUser()
    {
        $this->enableCsrfToken();
        $this->setUserAuth();
        $this->post('/configuration-types/delete/b1e85392-4fd9-4734-bbe4-db3137adb9ad');
        $this->assertAccessDenied();
    }

    public function testDeleteAsPublicUser()
    {
        $this->enableCsrfToken();
        $this->post('/configuration-types/delete/b1e85392-4fd9-4734-bbe4-db3137adb9ad');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $account = $this->configurationTypesTable->newEntity([
            'scope' => 'U',
            'name' => 'JPM Testing' . random_int(0, 500),
            'description' => 'Lorem ipsum dolor sit amet',
            'data_type' => 'text',
            'default_value' => 'JPM',
            'tab' => 'admin',
            'is_editable' => 1,
            'is_enabled' => 1,
            'is_public' => 1,
            'sort_order' => 1
        ]);
        $result = $this->configurationTypesTable->save($account);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->configurationTypesTable->get($id);
        return $this->configurationTypesTable->delete($entity);
    }
}
