<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\ActionTypesController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\ActionTypesController Test Case
 *
 * @uses \App\Controller\ActionTypesController
 */
class ActionTypesControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $ActionTypesTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->ActionTypesTable = TableRegistry::getTableLocator()->get('ActionTypes');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/ActionTypes/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/ActionTypes/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/ActionTypes/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/ActionTypes/index');
        $this->assertLoginRedirect('/ActionTypes/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/ActionTypes/view/1');
        $this->assertLoginRedirect('/ActionTypes/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/ActionTypes/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/ActionTypes/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/ActionTypes/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/ActionTypes/add');
        $this->assertLoginRedirect('/ActionTypes/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/ActionTypes/edit/1');
        $this->assertLoginRedirect('/ActionTypes/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/ActionTypes/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->ActionTypesTable->newEntity([]);
        $result = $this->ActionTypesTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->ActionTypesTable->get($id);
        return $this->ActionTypesTable->delete($entity);
    }
}
