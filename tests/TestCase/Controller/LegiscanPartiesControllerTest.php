<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\LegiscanPartiesController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\LegiscanPartiesController Test Case
 *
 * @uses \App\Controller\LegiscanPartiesController
 */
class LegiscanPartiesControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $LegiscanPartiesTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->LegiscanPartiesTable = TableRegistry::getTableLocator()->get('LegiscanParties');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/LegiscanParties/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/LegiscanParties/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/LegiscanParties/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/LegiscanParties/index');
        $this->assertLoginRedirect('/LegiscanParties/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/LegiscanParties/view/1');
        $this->assertLoginRedirect('/LegiscanParties/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/LegiscanParties/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/LegiscanParties/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/LegiscanParties/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/LegiscanParties/add');
        $this->assertLoginRedirect('/LegiscanParties/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/LegiscanParties/edit/1');
        $this->assertLoginRedirect('/LegiscanParties/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/LegiscanParties/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->LegiscanPartiesTable->newEntity([]);
        $result = $this->LegiscanPartiesTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->LegiscanPartiesTable->get($id);
        return $this->LegiscanPartiesTable->delete($entity);
    }
}
