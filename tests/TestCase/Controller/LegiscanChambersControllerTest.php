<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\LegiscanChambersController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\LegiscanChambersController Test Case
 *
 * @uses \App\Controller\LegiscanChambersController
 */
class LegiscanChambersControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $LegiscanChambersTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->LegiscanChambersTable = TableRegistry::getTableLocator()->get('LegiscanChambers');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/LegiscanChambers/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/LegiscanChambers/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/LegiscanChambers/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/LegiscanChambers/index');
        $this->assertLoginRedirect('/LegiscanChambers/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/LegiscanChambers/view/1');
        $this->assertLoginRedirect('/LegiscanChambers/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/LegiscanChambers/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/LegiscanChambers/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/LegiscanChambers/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/LegiscanChambers/add');
        $this->assertLoginRedirect('/LegiscanChambers/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/LegiscanChambers/edit/1');
        $this->assertLoginRedirect('/LegiscanChambers/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/LegiscanChambers/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->LegiscanChambersTable->newEntity([]);
        $result = $this->LegiscanChambersTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->LegiscanChambersTable->get($id);
        return $this->LegiscanChambersTable->delete($entity);
    }
}
