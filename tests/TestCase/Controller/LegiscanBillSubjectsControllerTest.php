<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\LegiscanBillSubjectsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\LegiscanBillSubjectsController Test Case
 *
 * @uses \App\Controller\LegiscanBillSubjectsController
 */
class LegiscanBillSubjectsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $LegiscanBillSubjectsTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->LegiscanBillSubjectsTable = TableRegistry::getTableLocator()->get('LegiscanBillSubjects');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/LegiscanBillSubjects/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/LegiscanBillSubjects/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/LegiscanBillSubjects/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/LegiscanBillSubjects/index');
        $this->assertLoginRedirect('/LegiscanBillSubjects/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/LegiscanBillSubjects/view/1');
        $this->assertLoginRedirect('/LegiscanBillSubjects/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/LegiscanBillSubjects/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/LegiscanBillSubjects/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/LegiscanBillSubjects/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/LegiscanBillSubjects/add');
        $this->assertLoginRedirect('/LegiscanBillSubjects/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/LegiscanBillSubjects/edit/1');
        $this->assertLoginRedirect('/LegiscanBillSubjects/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/LegiscanBillSubjects/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->LegiscanBillSubjectsTable->newEntity([]);
        $result = $this->LegiscanBillSubjectsTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->LegiscanBillSubjectsTable->get($id);
        return $this->LegiscanBillSubjectsTable->delete($entity);
    }
}
