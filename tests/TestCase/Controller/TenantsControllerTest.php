<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;

/**
 * App\Controller\TenantsController Test Case
 *
 * @uses \App\Controller\TenantsController
 */
class TenantsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected $tenantsTable;

    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->tenantsTable = TableRegistry::getTableLocator()->get('Tenants');
    }

    /**
     * Test method: index
     */

    public function testIndexAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/tenants/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/tenants/index');
        $this->assertAccessDenied();
    }

    public function testIndexAsUser()
    {
        $this->setUserAuth();
        $this->get('/tenants/index');
        $this->assertAccessDenied();
    }

    public function testIndexAsPublicUser()
    {
        $this->get('/tenants/index');
        $this->assertLoginRedirect('/tenants/index');
    }

    /**
     * Test method: view
     */

    public function testViewAsSuperAdmin()
    {
        $this->setSuperAdminAuth();

        $id = $this->createTestRecord();
        $this->get('/tenants/view/' . $id);
        $this->assertResponseOk();

        $this->deleteTestRecord($id);
    }

    public function testViewAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/tenants/view/6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertAccessDenied();
    }

    public function testViewAsUser()
    {
        $this->setUserAuth();
        $this->get('/tenants/view/6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertAccessDenied();
    }

    public function testViewAsPublicUser()
    {
        $this->get('/tenants/view/6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertLoginRedirect('/tenants/view/6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
    }

    /**
     * Test method: add
     */

    public function testAddAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/tenants/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/tenants/add');
        $this->assertAccessDenied();
    }

    public function testAddAsUser()
    {
        $this->setUserAuth();
        $this->get('/tenants/add');
        $this->assertAccessDenied();
    }

    public function testAddAsPublicUser()
    {
        $this->get('/tenants/add');
        $this->assertLoginRedirect('/tenants/add');
    }

    public function testAddPostAsSuperAdminWithValidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/tenants/add', [
            'name' => 'JPM Testing' . random_int(0, 500),
            'domain_name' => random_int(0, 500),
            'id_public' => random_int(0, 500),
        ]);
        $this->assertFlashMessage('The tenant has been created.');
        $this->assertRedirect(['action' => 'index']);
    }

    public function testAddPostAsSuperAdminWithInvalidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/tenants/add', [
            'is_enabled' => 'F'
        ]);
        $this->assertFlashMessage('The tenant could not be created. Please, try again.');
    }

    /**
     * Test method: edit
     */

    public function testEditAsSuperAdmin()
    {
        $this->setSuperAdminAuth();

        $id = $this->createTestRecord();
        $this->get('/tenants/edit/' . $id);
        $this->assertResponseOk();

        $this->deleteTestRecord($id);
    }

    public function testEditAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/tenants/edit/6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertAccessDenied();
    }

    public function testEditAsUser()
    {
        $this->setUserAuth();
        $this->get('/tenants/edit/6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertAccessDenied();
    }

    public function testEditAsPublicUser()
    {
        $this->get('/tenants/edit/6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertLoginRedirect('/tenants/edit/6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
    }

    public function testEditPostAsSuperAdminWithValidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $id = $this->createTestRecord();
        $this->post('/tenants/edit/' . $id, [
            'name' => 'JPM Testing' . random_int(0, 500),
            'domain_name' => random_int(0, 500),
            'id_public' => random_int(0, 500),
        ]);
        $this->assertFlashMessage('The tenant has been saved.');
        $this->assertRedirect(['action' => 'index']);

        $this->deleteTestRecord($id);
    }

    public function testEditPostAsSuperAdminWithInvalidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $id = $this->createTestRecord();
        $this->post('/tenants/edit/' . $id, [
            'is_enabled' => 'F'
        ]);
        $this->assertFlashMessage('The tenant could not be saved. Please, try again.');

        $this->deleteTestRecord($id);
    }

    private function createTestRecord()
    {
        $account = $this->tenantsTable->newEntity([
            'name' => 'JPM Testing' . random_int(0, 500),
            'domain_name' => random_int(0, 500),
            'id_public' => random_int(0, 500),
        ]);
        $result = $this->tenantsTable->save($account);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->tenantsTable->get($id);
        return $this->tenantsTable->delete($entity);
    }
}
