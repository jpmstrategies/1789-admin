<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\BillEnumsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use App\Test\TestCase\Controller\BaseJpmControllerTest;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\BillEnumsController Test Case
 *
 * @uses \App\Controller\BillEnumsController
 */
class BillEnumsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $BillEnumsTable;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->BillEnumsTable = TableRegistry::getTableLocator()->get('BillEnums');
        $this->loadRoutes();
    }
    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/BillEnums/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/BillEnums/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/BillEnums/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/BillEnums/index');
        $this->assertLoginRedirect('/BillEnums/index');
    }

    /**
     * Test method: view
     */



    public function testViewAsPublicUser(): void
    {
        $this->get('/BillEnums/view/1');
        $this->assertLoginRedirect('/BillEnums/view/1');
    }

    private function createTestRecord()
    {
        $record = $this->BillEnumsTable->newEntity([]);
        $result = $this->BillEnumsTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->BillEnumsTable->get($id);
        return $this->BillEnumsTable->delete($entity);
    }
}
