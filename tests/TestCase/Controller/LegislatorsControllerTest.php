<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\LegislatorsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\LegislatorsController Test Case
 *
 * @uses \App\Controller\LegislatorsController
 */
class LegislatorsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $LegislatorsTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->LegislatorsTable = TableRegistry::getTableLocator()->get('Legislators');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/Legislators/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/Legislators/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/Legislators/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/Legislators/index');
        $this->assertLoginRedirect('/Legislators/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/Legislators/view/1');
        $this->assertLoginRedirect('/Legislators/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/Legislators/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/Legislators/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/Legislators/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/Legislators/add');
        $this->assertLoginRedirect('/Legislators/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/Legislators/edit/1');
        $this->assertLoginRedirect('/Legislators/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/Legislators/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->LegislatorsTable->newEntity([]);
        $result = $this->LegislatorsTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->LegislatorsTable->get($id);
        return $this->LegislatorsTable->delete($entity);
    }
}
