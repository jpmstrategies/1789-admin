<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\AccountsController;
use App\Controller\UsersController;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;

/**
 * App\Controller\UsersController Test Case
 *
 * @uses \App\Controller\UsersController
 */
class UsersControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected $usersTable;
    protected $configurationTypesTable;

    protected $signupDomainsTable;
    protected $accountsTable;
    protected $accountsController;

    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->accountsController = new AccountsController();
        $this->accountsTable = TableRegistry::getTableLocator()->get('Accounts');
        $this->usersTable = TableRegistry::getTableLocator()->get('Users');
        $this->configurationTypesTable = TableRegistry::getTableLocator()->get('ConfigurationTypes');
        $this->signupDomainsTable = TableRegistry::getTableLocator()->get('SignupDomains');
        $this->loadRoutes();
    }

    /**
     * Test method: checkSession
     */
    public function testCheckSessionAsPublicUser()
    {
        $this->get('/users/check-session');
        $this->assertLoginRedirect('/users/check-session');
    }

    public function testCheckSessionAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/users/check-session');
        $this->assertResponseOk();
    }

    public function testCheckSessionAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/users/check-session');
        $this->assertResponseOk();
    }

    public function testCheckSessionAsUser()
    {
        $this->setUserAuth();
        $this->get('/users/check-session');
        $this->assertResponseOk();
    }

    /**
     * Test method: login
     */
    public function testLoginAsPublicUser()
    {
        $this->get('/users/login');
        $this->assertResponseOk();
    }

    public function testLoginWithInvalidCredentials()
    {
        $this->enableCsrfToken();
        $this->post('/users/login', [
            'email' => 'jeff@jpmstrategies.com',
            'password' => 'test'
        ]);
        $this->assertFlashMessage('The Email and Password don\'t match our records, please check them and try signing in again.');
        $this->assertSessionNotHasKey('Auth');
    }

    public function testLoginSuccessOneTenant()
    {
        // the first tenant should be automatically selected and bypass the tenant selection
        $this->enableCsrfToken();
        $this->post('/users/login', [
            'email' => 'el.jefe42@gmail.com',
            'password' => 'jpmtesting'
        ]);
        $this->assertSessionHasKey('Account');
        $this->assertFlashMessage('Sign in successful');
        $this->assertRedirect(['controller' => 'Pages', 'action' => 'home', 'plugin' => false]);
    }

    public function testLoginSuccessOneTenantCustomRedirect()
    {
        // the first tenant should be automatically selected and bypass the tenant selection
        Configure::write('users.default_login_redirect', [
            'controller' => 'Pages',
            'action' => 'test',
            'plugin' => false]);

        $this->enableCsrfToken();
        $this->post('/users/login', [
            'email' => 'el.jefe42@gmail.com',
            'password' => 'jpmtesting'
        ]);
        $this->assertSessionHasKey('Account');
        $this->assertFlashMessage('Sign in successful');
        $this->assertRedirect(['controller' => 'Pages', 'action' => 'test', 'plugin' => false]);
    }

    public function testLoginSuccessOneTenantUrlRedirect()
    {
        $this->enableCsrfToken();
        $this->post('/users/login?redirect=/users/view', [
            'email' => 'el.jefe42@gmail.com',
            'password' => 'jpmtesting'
        ]);
        $this->assertSessionHasKey('Account');
        $this->assertFlashMessage('Sign in successful');
        $this->assertRedirect(['controller' => 'Users', 'action' => 'view', 'plugin' => false]);
    }

    public function testLoginSuccessMultipleTenants()
    {
        // user should be redirected to pick a tenant to login
        $this->enableCsrfToken();
        $this->post('/users/login', [
            'email' => 'multi-tenant@jpmstrategies.com',
            'password' => 'jpmtesting'
        ]);
        $this->assertSessionHasKey('Account');
        $this->assertFlashMessage('Sign in successful');
        $this->assertRedirect(['controller' => 'accounts', 'action' => 'index', 'plugin' => false]);
    }

    public function testLoginSuccessMultipleTenantsUrlRedirect()
    {
        // user should be redirected to pick a tenant to login
        $this->enableCsrfToken();
        $this->post('/users/login?redirect=/users/view', [
            'email' => 'multi-tenant@jpmstrategies.com',
            'password' => 'jpmtesting'
        ]);
        $this->assertSessionHasKey('Account');
        $this->assertFlashMessage('Sign in successful');
        $this->assertRedirect([
            'controller' => 'accounts',
            'action' => 'index',
            '?' => ['redirect' => '/users/view'],
            'plugin' => false]);
    }

    public function testLoginWithInviteRedirect()
    {
        $this->enableCsrfToken();
        $this->setUserInvite(true);
        $this->post('/users/login', [
            'email' => 'jeff.morris@outlook.com',
            'password' => 'jpmtesting'
        ]);
        $this->assertSessionHasKey('Auth');
        $this->assertFlashMessage('Sign in successful');
        $this->assertRedirect([
            'controller' => 'accounts',
            'action' => 'invite',
            '9bed23f4-f544-11eb-9a03-0242ac130003']);
    }

    /**
     * Test method: loginSetAccount
     */

    /**
     * Test method: logout
     */

    public function testLogoutWithValidSession()
    {
        // user should get redirected to the login page with no auth set
        $this->setUserAuth();
        $this->get('/users/logout');
        $this->assertRedirect(['controller' => 'users', 'action' => 'login']);
        $this->assertSessionNotHasKey('Auth');
    }

    public function testLogoutWithUserSource()
    {
        $this->setUserAuth();
        $this->get('/users/logout?source=user');
        $this->assertRedirect(['controller' => 'users', 'action' => 'login', '?' => ['source' => 'user']]);
        $this->assertFlashMessage('You have successfully logged out.');
        $this->assertSessionNotHasKey('Auth');
    }

    public function testLogoutWithTimeoutSource()
    {
        $this->setUserAuth();
        $this->get('/users/logout?source=timeout');
        $this->assertRedirect(['controller' => 'users', 'action' => 'login', '?' => ['source' => 'timeout']]);
        $this->assertFlashMessage('Your browser session has expired to protect your information.');
        $this->assertSessionNotHasKey('Auth');
    }

    public function testLogoutWithInviteRedirectEnabledUser()
    {
        // user should get redirected to the login page
        $this->setUserInvite(true);
        $this->get('/users/logout/invite');
        $this->assertRedirect(['controller' => 'users', 'action' => 'login']);
        $this->assertSessionHasKey('Invite');
    }

    public function testLogoutWithInviteRedirectDisabledUser()
    {
        // user should get redirected to the signup page
        $this->setUserInvite(false);
        $this->get('/users/logout/invite');
        $this->assertRedirect(['controller' => 'users', 'action' => 'finishSignup']);
        $this->assertSessionHasKey('Invite');
    }

    /**
     * Test method: index
     */

    public function testIndexAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/users/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/users/index');
        $this->assertAccessDenied();
    }

    public function testIndexAsUser()
    {
        $this->setUserAuth();
        $this->get('/users/index');
        $this->assertAccessDenied();
    }

    public function testIndexAsPublicUser()
    {
        $this->get('/users/index');
        $this->assertLoginRedirect('/users/index');
    }

    /**
     * Test method: view
     */

    public function testViewAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/users/view');
        $this->assertResponseOk();
    }

    public function testViewAsSuperAdminWithId()
    {
        // test the super admin user can see any record
        $this->setSuperAdminAuth();
        $this->get('/users/view/84f04b10-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertResponseContains('el.jefe42@gmail.com');
        $this->assertResponseOk();
    }

    public function testViewAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/users/view');
        $this->assertResponseOk();
    }

    public function testViewAsAdminWithId()
    {
        // test the admin user can only see their record
        $this->setAdminAuth();
        $this->get('/users/view/3');
        $this->assertResponseContains('el.jefe42@gmail.com');
        $this->assertResponseOk();
    }

    public function testViewAsUser()
    {
        $this->setUserAuth();
        $this->get('/users/view');
        $this->assertResponseOk();
    }

    public function testViewAsUserWithId()
    {
        // test the non-admin user can only see their record
        $this->setUserAuth();
        $this->get('/users/view/2');
        $this->assertResponseContains('jeff.morris@outlook.com');
        $this->assertResponseOk();
    }

    public function testViewAsPublicUser()
    {
        $this->get('/users/view');
        $this->assertLoginRedirect('/users/view');
    }

    /**
     * Test method: add
     */

    public function testAddGetAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/users/add');
        $this->assertResponseOk();
    }

    public function testAddPostAsSuperAdminWithValidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/users/add', [
            'email' => 'add@apptest.com',
            'password' => 'jpmtesting',
            'password_confirm' => 'jpmtesting'
        ]);
        $this->assertRedirect(['action' => 'index']);
        $this->assertFlashMessage('The user has been saved.');
    }

    public function testAddPostAsSuperAdminWithInvalidPayload()
    {
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/users/add', [
            'email' => 'add@apptest.com',
            'password' => 'test',
            'password_confirm' => 'test'
        ]);
        $this->assertFlashMessage('The user could not be saved. Please, try again.');
    }

    public function testAddAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/users/add');
        $this->assertAccessDenied();
    }

    public function testAddAsUser()
    {
        $this->setUserAuth();
        $this->get('/users/add');
        $this->assertAccessDenied();
    }

    public function testAddAsPublicUser()
    {
        $this->get('/users/add');
        $this->assertLoginRedirect('/users/add');
    }

    /**
     * Test method: signup (self signup disabled)
     */
    public function testSignupAsPublicUserSelfSignupDisabled()
    {
        $this->get('/users/signup');
        $this->assertRedirect([
            'controller' => 'users',
            'action' => 'login',
            'plugin' => false
        ]);
    }

    public function testSignupPostSelfSignupDisabled()
    {
        $this->enableCsrfToken();
        $this->post('/users/signup', [
            'email' => 'success@a.gov'
        ]);
        $this->assertRedirect([
            'controller' => 'users',
            'action' => 'login',
            'plugin' => false
        ]);
    }

    /**
     * Test method: signup (self signup enabled)
     */

    public function enableSelfSignup()
    {
        Configure::write('users.self_signup_enabled', 1);
        Configure::write('users.self_signup_tenant', '6f84587a-1d88-11ec-8296-d2ea1fd9e5ee');
    }

    public function disableSelfSignup()
    {
        Configure::write('users.self_signup_enabled', null);
        Configure::write('users.self_signup_tenant', null);
    }

    public function testSignupAsPublicUserSelfSignupEnabled()
    {
        $this->enableSelfSignup();

        $this->get('/users/signup');
        $this->assertResponseOk();

        $this->disableSelfSignup();
    }

    public function testSignupPostInvalidEmail()
    {
        $this->enableSelfSignup();

        $this->enableCsrfToken();
        $this->post('/users/signup', [
            'email' => 'abc'
        ]);
        $this->assertFlashMessage('Please enter a validate email address.');

        $this->disableSelfSignup();
    }

    public function testSignupPostEmailRegexSuccessSaveFailure()
    {
        $this->enableSelfSignup();

        $this->enableCsrfToken();

        $model = $this->getMockForModel('Accounts', ['save']);
        $model->expects($this->once())
            ->method('save')
            ->will($this->returnValue(false));

        $this->post('/users/signup', [
            'email' => 'failure@abc.gov'
        ]);

        $this->assertFlashMessage('Signup was not successful. Please, try again.');

        $this->disableSelfSignup();
        $this->getTableLocator()->clear();
    }

    public function testSignupPostPendingAccountExists()
    {
        $this->enableSelfSignup();

        $this->enableCsrfToken();
        $this->post('/users/signup', [
            'email' => 'pending@a.gov'
        ]);
        $this->assertFlashMessage('Signup was successful! Please check your email.');

        $this->post('/users/signup', [
            'email' => 'pending@a.gov'
        ]);
        $this->assertFlashMessage('You already have an account. Please check your email to finish setup.');

        $this->disableSelfSignup();
    }

    public function testSignupPostActiveAccountExists()
    {
        $this->enableSelfSignup();

        $this->enableCsrfToken();
        $this->post('/users/signup', [
            'email' => 'active-user@jpmstrategies.com'
        ]);

        $this->assertRedirect([
            'controller' => 'users',
            'action' => 'login',
            'plugin' => false
        ]);

        $this->disableSelfSignup();
    }

    /**
     * Test method: finish-signup
     */
    public function testFinishSignupGetAsPublicUser()
    {
        $this->get('/users/finish-signup');
        $this->assertRedirect(['controller' => 'users', 'action' => 'login', 'plugin' => false]);
    }

    public function testFinishSignupGetWithEnabledUser()
    {
        $this->enableCsrfToken();
        $this->setUserInvite(true);
        $this->get('/users/finish-signup');
        $this->assertRedirect(['controller' => 'users', 'action' => 'login', 'plugin' => false]);
    }

    public function testFinishSignupGetWithDisabledUser()
    {
        $this->enableCsrfToken();
        $this->setUserInvite(false);
        $this->get('/users/finish-signup');
        $this->assertResponseOk();
    }

    public function testFinishSignupPostWithNoInvite()
    {
        $this->enableCsrfToken();
        $this->post('/users/finish-signup');
        $this->assertRedirect(['controller' => 'users', 'action' => 'login', 'plugin' => false]);
    }

    public function testFinishSignupPostExistingUser()
    {
        $this->enableCsrfToken();
        $this->setUserInvite(true);
        $this->post('/users/finish-signup');
        $this->assertRedirect(['controller' => 'users', 'action' => 'login', 'plugin' => false]);
    }

    public function testFinishSignupPostNewUser()
    {
        $this->enableCsrfToken();
        $this->setUserInvite(false);
        $this->post('/users/finish-signup');
        $this->assertMailSubjectContains('Get Started with');
        $this->assertRedirect([
            'controller' => 'accounts',
            'action' => 'invite',
            '9bed23f4-f544-11eb-9a03-0242ac130003']);
    }

    public function testFinishSignupPostNewUserSelfSignupEnabled()
    {
        Configure::write('users.self_signup_enabled', 1);

        $this->enableCsrfToken();
        $this->setUserInvite(false);
        $this->post('/users/finish-signup');
        $this->assertRedirect([
            'controller' => 'accounts',
            'action' => 'invite',
            '9bed23f4-f544-11eb-9a03-0242ac130003',
            'ACTIVE']);
    }

    /*
     * Custom User Fields
     */

    private function postFinishSignupCustomFields($ct, $assertFailure = false)
    {
        $this->enableCsrfToken();
        $this->setUserInvite(false);
        $this->post('/users/finish-signup', [
            'email' => 'jeff.morris@outlook.com',
            'password' => 'jpmtesting',
            'password_confirm' => 'jpmtesting',
            'ct' => $ct
        ]);
        if ($assertFailure) {
            $this->assertFlashMessage('User sign up failed. Please, try again.');
        } else {
            $this->assertRedirect([
                'controller' => 'accounts',
                'action' => 'invite',
                '9bed23f4-f544-11eb-9a03-0242ac130003',
                'ACTIVE']);
        }
    }

    public function testFinishSignupPostCustomFieldsServerSideValidationFailure()
    {
        Configure::write('users.self_signup_enabled', 1);

        // create custom fields (this should force the `first_name` field to fail)
        $customFieldId = $this->createCustomField(['required_regex' => '[0-9]+']);

        $this->postFinishSignupCustomFields(['first_name' => 'Jeff'], true);

        // delete custom fields
        $this->deleteCustomField($customFieldId);
    }

    public function testFinishSignupPostCustomFieldsServerSideValidationFailureBlankField()
    {
        Configure::write('users.self_signup_enabled', 1);

        // create custom fields
        $customFieldId = $this->createCustomField();

        $this->postFinishSignupCustomFields([], true);

        // delete custom fields
        $this->deleteCustomField($customFieldId);
    }

    public function testFinishSignupPostCustomFieldsValidationSuccessWithValidField()
    {
        Configure::write('users.self_signup_enabled', 1);

        // create custom fields
        $customFieldId = $this->createCustomField();

        $this->postFinishSignupCustomFields(['first_name' => 'Jeff']);

        // validate custom field saved
        $user = $this->usersTable->find('all', ['conditions' => ['email' => 'jeff.morris@outlook.com']])->first();
        $this->assertEquals('Jeff', $user['user_details']['first_name']);

        // delete custom fields
        $this->deleteCustomField($customFieldId);
    }

    public function testFinishSignupPostCustomFieldsValidationSuccessWithInvalidField()
    {
        Configure::write('users.self_signup_enabled', 1);

        // create custom fields
        $customFieldId = $this->createCustomField();

        $this->postFinishSignupCustomFields(['first_name' => 'Jeff', 'invalid_field' => 'should-not-save']);

        // validate custom field saved
        $user = $this->usersTable->find('all', ['conditions' => ['email' => 'jeff.morris@outlook.com']])->first();
        $this->assertEquals('Jeff', $user['user_details']['first_name']);
        $this->assertArrayNotHasKey('invalid_field', $user['user_details']);

        // delete custom fields
        $this->deleteCustomField($customFieldId);
    }

    public function testFinishSignupPostCustomFieldsValidateOnlyUserDetailFieldsSaved()
    {
        Configure::write('users.self_signup_enabled', 1);

        // create custom fields
        $customFieldId = $this->createCustomField();

        // create non-user-detail field
        $nonUserField = $this->createCustomField([
            'name' => 'non_user_field',
            'tab' => 'admin',
        ]);

        $this->postFinishSignupCustomFields(['first_name' => 'Jeff', 'non_user_field' => 'should-not-save']);

        // validate custom field saved
        $user = $this->usersTable->find('all', ['conditions' => ['email' => 'jeff.morris@outlook.com']])->first();
        $this->assertEquals('Jeff', $user['user_details']['first_name']);
        $this->assertArrayNotHasKey('non_user_field', $user['user_details']);

        // delete custom fields
        $this->deleteCustomField($customFieldId);

        // delete non-user-detail field
        $this->deleteCustomField($nonUserField);
    }

    public function testFinishSignupPostNoCustomFieldsSuccess()
    {
        Configure::write('users.self_signup_enabled', 1);

        $this->postFinishSignupCustomFields(['first_name' => 'Jeff']);

        // validate custom field saved
        $user = $this->usersTable->find('all', ['conditions' => ['email' => 'jeff.morris@outlook.com']])->first();
        $this->assertEquals([], $user['user_details']);
    }

    public function testGetUserDetailsOnlyDataParameter()
    {
        $userDetails = (new UsersController())->getUserDetails(['first_name' => 'jeff']);
        $this->assertArrayHasKey('validation_failed', $userDetails);
    }

    /**
     * Test method: edit
     */

    // TODO validate that fields only get stripped for non-super admin actions

    public function testEditGetAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/users/edit');
        $this->assertResponseOk();
    }

    public function testEditPostAsSuperAdminWithValidPayload()
    {
        // test that a super admin can change their email address
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/users/edit', [
            'email' => 'test@test.com'
        ]);
        $this->assertRedirect(['action' => 'view']);

        // reset record to original value after testing
        $this->post('/users/edit', [
            'email' => 'jeff@jpmstrategies.com'
        ]);
    }

    public function testEditPostAsSuperAdminSaveFailure()
    {
        // test that a super admin can change their email address
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $model = $this->getMockForModel('Users', ['save']);
        $model->expects($this->once())
            ->method('save')
            ->will($this->returnValue(false));
        $this->post('/users/edit', [
            'email' => 'test@test.com'
        ]);
        $this->assertFlashMessage('The user could not be saved. Please, try again.');
    }

    public function testEditGetAsSuperAdminWithDifferentUserId()
    {
        // test the super admin user can see any record
        $this->setSuperAdminAuth();
        $this->get('/users/edit/84f04b10-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertResponseContains('el.jefe42@gmail.com');
        $this->assertResponseOk();
    }

    public function testEditPostAsSuperAdminWithDifferentUserId()
    {
        // test that a super admin can change any user's email address
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/users/edit/84f04b10-1d88-11ec-8296-d2ea1fd9e5ee', [
            'email' => 'test@test.com'
        ]);
        $this->assertRedirect(['action' => 'index']);

        // reset record to original value after testing
        $this->post('/users/edit/84f04b10-1d88-11ec-8296-d2ea1fd9e5ee', [
            'email' => 'el.jefe42@gmail.com'
        ]);
    }

    public function testEditGetAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/users/edit');
        $this->assertResponseOk();
    }

    public function testEditPostAsAdminWithValidPayload()
    {
        $this->enableCsrfToken();
        $this->setAdminAuth();
        $this->post('/users/edit', [
            'email' => 'test@test.com'
        ]);
        $this->assertRedirect(['action' => 'view']);
    }

    public function testEditGetAsAdminWithDifferentUserId()
    {
        // test the admin user can only see their record
        $this->setAdminAuth();
        $this->get('/users/edit/3');
        $this->assertResponseContains('el.jefe42@gmail.com');
        $this->assertResponseOk();
    }

    public function testEditPostAsAdminWithDifferentUserId()
    {
        $this->enableCsrfToken();
        $this->setAdminAuth();
        $this->post('/users/edit/3', [
            'email' => 'test@test.com'
        ]);
        $this->assertRedirect(['action' => 'view']);
    }

    public function testEditGetAsUser()
    {
        $this->setUserAuth();
        $this->get('/users/edit');
        $this->assertResponseOk();
    }

    public function testEditPostAsUserWithValidPayload()
    {
        $this->enableCsrfToken();
        $this->setUserAuth();
        $this->post('/users/edit', [
            'email' => 'test@test.com'
        ]);
        $this->assertRedirect(['action' => 'view']);
    }

    public function testEditGetAsUserWithDifferentUserId()
    {
        // test the non-admin user can only see their record
        $this->setUserAuth();
        $this->get('/users/edit/2');
        $this->assertResponseContains('jeff.morris@outlook.com');
        $this->assertResponseOk();
    }

    public function testEditPostAsUserWithDifferentUserId()
    {
        $this->enableCsrfToken();
        $this->setUserAuth();
        $this->post('/users/edit/2', [
            'email' => 'test@test.com'
        ]);
        $this->assertRedirect(['action' => 'view']);
    }

    public function testEditGetAsPublicUser()
    {
        $this->get('/users/edit');
        $this->assertLoginRedirect('/users/edit');
    }

    public function testEditPostAsPublicUser()
    {
        $this->enableCsrfToken();
        $this->post('/users/edit', [
            'email' => 'test@test.com'
        ]);
        $this->assertLoginRedirect(null);
    }

    private function postEditProfileCustomFields($ct, $assertFailure = false)
    {
        $this->enableCsrfToken();
        $this->setUserAuth();
        $this->post('/users/edit', [
            'email' => 'jeff.morris@outlook.com',
            'ct' => $ct
        ]);
        if ($assertFailure) {
            $this->assertFlashMessage('The user could not be saved. Please, try again.');
        } else {
            $this->assertRedirect([
                'controller' => 'users',
                'action' => 'view',
            ]);
        }
    }

    public function testEditProfilePostCustomFieldsServerSideValidationFailure()
    {
        // create custom fields (this should force the `first_name` field to fail)
        $customFieldId = $this->createCustomField(['required_regex' => '[0-9]+']);

        $this->postEditProfileCustomFields(['first_name' => 'Jeff'], true);

        // delete custom fields
        $this->deleteCustomField($customFieldId);
    }

    public function testEditProfilePostCustomFieldsServerSideValidationFailureBlankField()
    {
        // create custom fields
        $customFieldId = $this->createCustomField();

        $this->postEditProfileCustomFields([], true);

        // delete custom fields
        $this->deleteCustomField($customFieldId);
    }

    public function testEditProfilePostCustomFieldsValidationSuccessWithValidField()
    {
        // create custom fields
        $customFieldId = $this->createCustomField();

        $this->postEditProfileCustomFields(['first_name' => 'Jeff']);

        // validate custom field saved
        $user = $this->usersTable->find('all', ['conditions' => ['email' => 'jeff.morris@outlook.com']])->first();
        $this->assertEquals('Jeff', $user['user_details']['first_name']);

        // delete custom fields
        $this->deleteCustomField($customFieldId);
    }

    public function testEditProfilePostCustomFieldsValidationSuccessWithInvalidField()
    {
        // create custom fields
        $customFieldId = $this->createCustomField();

        $this->postEditProfileCustomFields(['first_name' => 'Jeff', 'invalid_field' => 'should-not-save']);

        // validate custom field saved
        $user = $this->usersTable->find('all', ['conditions' => ['email' => 'jeff.morris@outlook.com']])->first();
        $this->assertEquals('Jeff', $user['user_details']['first_name']);
        $this->assertArrayNotHasKey('invalid_field', $user['user_details']);

        // delete custom fields
        $this->deleteCustomField($customFieldId);
    }

    public function testEditProfilePostCustomFieldsValidateOnlyUserDetailFieldsSaved()
    {
        // create custom fields
        $customFieldId = $this->createCustomField();

        // create non-user-detail field
        $nonUserField = $this->createCustomField([
            'name' => 'non_user_field',
            'tab' => 'admin',
        ]);

        $this->postEditProfileCustomFields(['first_name' => 'Jeff', 'non_user_field' => 'should-not-save']);

        // validate custom field saved
        $user = $this->usersTable->find('all', ['conditions' => ['email' => 'jeff.morris@outlook.com']])->first();
        $this->assertEquals('Jeff', $user['user_details']['first_name']);
        $this->assertArrayNotHasKey('non_user_field', $user['user_details']);

        // delete custom fields
        $this->deleteCustomField($customFieldId);

        // delete non-user-detail field
        $this->deleteCustomField($nonUserField);
    }

    public function testEditProfilePostNoCustomFieldsSuccess()
    {
        $this->postEditProfileCustomFields(['first_name' => 'Jeff']);

        // validate custom field saved
        $user = $this->usersTable->find('all', ['conditions' => ['email' => 'jeff.morris@outlook.com']])->first();
        $this->assertEquals([], $user['user_details']);
    }

    /**
     * Test method: editPassword
     */

    // TODO validate that fields only get stripped for non-super admin actions

    public function testEditPasswordGetAsSuperAdmin()
    {
        $this->setSuperAdminAuth();
        $this->get('/users/edit-password');
        $this->assertResponseOk();
    }

    public function testEditPasswordPostAsSuperAdminWithValidPayload()
    {
        // test that a super admin can change their email address
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/users/edit-password', [
            'current_password' => 'jpmtesting',
            'password_update' => 'jpmtesting2',
            'password_update_confirm' => 'jpmtesting2',
        ]);
        $this->assertRedirect(['action' => 'view']);

        // reset record to original value after testing
        $this->post('/users/edit-password', [
            'current_password' => 'jpmtesting2',
            'password_update' => 'jpmtesting',
            'password_update_confirm' => 'jpmtesting',
        ]);
    }

    public function testEditPasswordPostAsSuperAdminWithInvalidPayload()
    {
        // test that a super admin can change their email address
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/users/edit-password', [
            'current_password' => uniqid(),
            'password_update' => 'jpmtesting',
            'password_update_confirm' => 'jpmtesting',
        ]);
        $this->assertFlashMessage('The user could not be saved. Please, try again.');
    }

    public function testEditPasswordGetAsSuperAdminWithDifferentUserId()
    {
        // test the super admin user can see any record
        $this->setSuperAdminAuth();
        $this->get('/users/edit-password/84f04b10-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertResponseContains('el.jefe42@gmail.com');
        $this->assertResponseOk();
    }

    public function testEditPasswordPostAsSuperAdminWithDifferentUserId()
    {
        // test that a super admin can change any user's email address
        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/users/edit-password/84f04b10-1d88-11ec-8296-d2ea1fd9e5ee', [
            'current_password' => 'jpmtesting',
            'password_update' => 'jpmtesting2',
            'password_update_confirm' => 'jpmtesting2',
        ]);
        $this->assertRedirect(['action' => 'index']);

        // reset record to original value after testing
        $this->post('/users/edit-password/84f04b10-1d88-11ec-8296-d2ea1fd9e5ee', [
            'current_password' => 'jpmtesting2',
            'password_update' => 'jpmtesting',
            'password_update_confirm' => 'jpmtesting',
        ]);
    }

    public function testEditPasswordGetAsAdmin()
    {
        $this->setAdminAuth();
        $this->get('/users/edit-password');
        $this->assertResponseOk();
    }

    public function testEditPasswordPostAsAdminWithValidPayload()
    {
        $this->enableCsrfToken();
        $this->setAdminAuth();
        $this->post('/users/edit-password', [
            'current_password' => 'jpmtesting',
            'password_update' => 'jpmtesting2',
            'password_update_confirm' => 'jpmtesting2',
        ]);
        $this->assertRedirect(['action' => 'view']);

        // reset record to original value after testing
        $this->post('/users/edit-password', [
            'current_password' => 'jpmtesting2',
            'password_update' => 'jpmtesting',
            'password_update_confirm' => 'jpmtesting',
        ]);
    }

    public function testEditPasswordPostAsAdminWithInvalidPayload()
    {
        $this->enableCsrfToken();
        $this->setAdminAuth();
        $this->post('/users/edit-password', [
            'current_password' => uniqid(),
            'password_update' => 'jpmtesting',
            'password_update_confirm' => 'jpmtesting',
        ]);
        $this->assertFlashMessage('The user could not be saved. Please, try again.');
    }

    public function testEditPasswordGetAsAdminWithDifferentUserId()
    {
        // test the admin user can only see their record
        $this->setAdminAuth();
        $this->get('/users/edit-password/84f04c1e-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertResponseContains('el.jefe42@gmail.com');
        $this->assertResponseOk();
    }

    public function testEditPasswordPostAsAdminWithDifferentUserId()
    {
        $this->enableCsrfToken();
        $this->setAdminAuth();
        $this->post('/users/edit-password/84f04c1e-1d88-11ec-8296-d2ea1fd9e5ee', [
            'current_password' => 'jpmtesting',
            'password_update' => 'jpmtesting2',
            'password_update_confirm' => 'jpmtesting2',
        ]);
        $this->assertRedirect(['action' => 'view']);

        // reset record to original value after testing
        $this->post('/users/edit-password/84f04c1e-1d88-11ec-8296-d2ea1fd9e5ee', [
            'current_password' => 'jpmtesting2',
            'password_update' => 'jpmtesting',
            'password_update_confirm' => 'jpmtesting',
        ]);
    }

    public function testEditPasswordGetAsUser()
    {
        $this->setUserAuth();
        $this->get('/users/edit-password');
        $this->assertResponseOk();
    }

    public function testEditPasswordPostAsUserWithValidPayload()
    {
        $this->enableCsrfToken();
        $this->setUserAuth();
        $this->post('/users/edit-password', [
            'current_password' => 'jpmtesting',
            'password_update' => 'jpmtesting2',
            'password_update_confirm' => 'jpmtesting2',
        ]);
        $this->assertRedirect(['action' => 'view']);

        // reset record to original value after testing
        $this->post('/users/edit-password', [
            'current_password' => 'jpmtesting2',
            'password_update' => 'jpmtesting',
            'password_update_confirm' => 'jpmtesting',
        ]);
    }

    public function testEditPasswordPostAsUserWithInvalidPayload()
    {
        $this->enableCsrfToken();
        $this->setUserAuth();
        $this->post('/users/edit-password', [
            'current_password' => uniqid(),
            'password_update' => 'jpmtesting',
            'password_update_confirm' => 'jpmtesting',
        ]);
        $this->assertFlashMessage('The user could not be saved. Please, try again.');
    }

    public function testEditPasswordGetAsUserWithDifferentUserId()
    {
        // test the non-admin user can only see their record
        $this->setUserAuth();
        $this->get('/users/edit-password/84f04bec-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertResponseContains('jeff.morris@outlook.com');
        $this->assertResponseOk();
    }

    public function testEditPasswordPostAsUserWithDifferentUserId()
    {
        $this->enableCsrfToken();
        $this->setAdminAuth();
        $this->post('/users/edit-password/84f04bec-1d88-11ec-8296-d2ea1fd9e5ee', [
            'current_password' => 'jpmtesting',
            'password_update' => 'jpmtesting2',
            'password_update_confirm' => 'jpmtesting2',
        ]);
        $this->assertRedirect(['action' => 'view']);

        // reset record to original value after testing
        $this->post('/users/edit-password/84f04bec-1d88-11ec-8296-d2ea1fd9e5ee', [
            'current_password' => 'jpmtesting2',
            'password_update' => 'jpmtesting',
            'password_update_confirm' => 'jpmtesting',
        ]);
    }

    public function testEditPasswordGetAsPublicUser()
    {
        $this->get('/users/edit-password');
        $this->assertLoginRedirect('/users/edit-password');
    }

    public function testEditPasswordPostAsPublicUser()
    {
        $this->enableCsrfToken();
        $this->post('/users/edit-password', [
            'current_password' => 'jpmtesting',
            'password_update' => 'jpmtesting2',
            'password_update_confirm' => 'jpmtesting2',
        ]);
        $this->assertLoginRedirect(null);
    }

    /**
     * Test method: delete
     */
    public function testDeleteAsSuperAdminSuccess()
    {
        // add record for testing
        $id = $this->createTestRecord();

        $this->enableCsrfToken();
        $this->setSuperAdminAuth();
        $this->post('/users/delete/' . $id);
        $this->assertFlashMessage('The user has been deleted.');
        $this->assertRedirect(['action' => 'index']);
    }

    public function testDeleteAsSuperAdminFailure()
    {
        // add record for testing
        $id = $this->createTestRecord();

        $this->enableCsrfToken();
        $this->setSuperAdminAuth();

        $model = $this->getMockForModel('Users', ['delete']);
        $model->expects($this->once())
            ->method('delete')
            ->will($this->returnValue(false));
        $this->post('/users/delete/' . $id);
        $this->assertFlashMessage('The user could not be deleted. Please, try again.');
        $this->assertRedirect(['action' => 'index']);
    }

    public function testDeleteAsAdmin()
    {
        $this->enableCsrfToken();
        $this->setAdminAuth();
        $this->post('/users/delete/84f04b10-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertAccessDenied();
    }

    public function testDeleteAsUser()
    {
        $this->enableCsrfToken();
        $this->setUserAuth();
        $this->post('/users/delete/84f04b10-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertAccessDenied();
    }

    public function testDeleteAsPublicUser()
    {
        $this->enableCsrfToken();
        $this->post('/users/delete/84f04b10-1d88-11ec-8296-d2ea1fd9e5ee');
        $this->assertLoginRedirect();
    }

    /**
     * Test method: forgotPassword
     */
    public function testForgotPasswordGetAsPublicUser()
    {
        $this->get('/users/forgot-password');
        $this->assertResponseOk();
    }

    public function testForgotPasswordPostAsPublicUserWithValidEmail()
    {
        $this->enableCsrfToken();
        $email = 'jeff.morris@outlook.com';
        $newPassword = 'jpmtesting42';

        // request reset token
        $this->post('/users/forgot-password', [
            'email' => $email
        ]);
        $this->assertFlashMessage('Copy the reset code from your email and paste it below.');
        $this->assertMailSubjectContains('Instructions for changing your password');

        $user = $this->usersTable->find('all')
            ->where(['email' => $email])
            ->first();
        $originalPassword = $user->password;

        // test reset with invalid payload
        $this->post('/users/reset-password/' . $user->security_code, [
            'security_code' => $user->security_code,
            'password' => $newPassword,
            'password_confirm' => 'jpmtesting3',
        ]);
        $this->assertFlashMessage('The password could not be updated. Please, try again.');

        // test reset with valid payload
        $this->post('/users/reset-password/' . $user->security_code, [
            'security_code' => $user->security_code,
            'password' => $newPassword,
            'password_confirm' => $newPassword,
        ]);
        $this->assertMailSubjectContains('Your password has been changed');
        $this->assertFlashMessage('Your password has been updated.');
        $this->assertRedirect(['action' => 'login']);

        // make sure the password hashes changed, and the security_code reset
        $user = $this->usersTable->find('all')
            ->where(['email' => $email])
            ->first();
        $this->assertNull($user->security_code);
        $this->assertTextNotEquals($originalPassword, $user->password);

        // test login with new credentials
        $this->post('/users/login', [
            'email' => $email,
            'password' => $newPassword
        ]);
        // not sure why this information is missing for the session, so we will validate the redirect instead
        // $this->assertSessionHasKey('Account');
        // $this->assertFlashMessage('Sign in successful');
        $this->assertRedirect(['controller' => 'Pages', 'action' => 'home', 'plugin' => false]);

        // reset record to original value after testing
        $this->setSuperAdminAuth();
        $this->post('/users/edit-password/' . $user->id, [
            'current_password' => $newPassword,
            'password_update' => 'jpmtesting',
            'password_update_confirm' => 'jpmtesting',
        ]);
        $this->assertFlashMessage('The user has been saved.');
        // test login with old credentials
        $this->post('/users/login', [
            'email' => $email,
            'password' => $newPassword
        ]);
        $this->assertFlashMessage('The Email and Password don\'t match our records, please check them and try signing in again.');
    }

    public function testForgotPasswordPostAsPublicUserWithInvalidEmail()
    {
        $this->enableCsrfToken();
        $this->post('/users/forgot-password', [
            'email' => 'password@test.com'
        ]);
        $this->assertFlashMessage('Copy the reset code from your email and paste it below.');
    }

    /**
     * Test method: resetPassword
     */
    public function testResetPasswordAsPublicUser()
    {
        $this->get('/users/reset-password');
        $this->assertResponseOk();
    }

    public function testResetPasswordInvalidSecurityCode()
    {
        $this->enableCsrfToken();
        $this->post('/users/reset-password', [
            'security_code' => 'invalid-security-code'
        ]);
        $this->assertFlashMessage('Invalid or expired security token. Please check your email or try again.');
        $this->assertRedirect(['action' => 'forgot-password']);
    }

    public function setupSelfSignupUserApproval()
    {
        $this->enableSelfSignup();
        $this->enableCsrfToken();

        // create signup domain to enforce pending email
        $record = $this->signupDomainsTable->newEntity([
            'name' => 'Only Federal Domains',
            'regex' => '.gov',
        ]);
        $signupDomain = $this->signupDomainsTable->save($record);

        // create user
        $this->post('/users/signup', [
            'email' => 'test@whitelist.com'
        ]);
        $this->assertFlashMessage('Signup was successful! Please check your email.');

        $user = $this->usersTable->find('all')
            ->where(['email' => 'test@whitelist.com'])
            ->first();

        $account = $this->accountsTable->find('all')
            ->where(['user_id' => $user->id])
            ->first();

        $this->get('/accounts/verify/' . $account->security_code);
        $this->assertRedirect(['controller' => 'users', 'action' => 'finishSignup', 'plugin' => false]);

        $this->session([
            'Invite' => (object)[
                'id' => $account->id,
                'invited_by_user_id' => $account->invited_by_user_id,
                'security_code' => $account->security_code,
                'user' => (object)[
                    'id' => $user->id,
                    'email' => $user->email,
                    'is_enabled' => (bool)$user->is_enabled
                ],
            ]
        ]);

        // validate user email address and finish signup
        $this->post('/users/finish-signup/', [
            'id' => $user->id,
            'email' => 'test@whitelist.com',
        ]);
        $this->assertFlashMessage('Signup was successful! Your request has been received and is pending approval.');

        return [
            'user' => $user,
            'account' => $account,
            'signupDomain' => $signupDomain
        ];
    }

    public function testSelfInviteUserNeedsApprovalSuccess()
    {
        $setup = $this->setupSelfSignupUserApproval();

        $account = $this->accountsTable->find('all')
            ->where(['id' => $setup['account']->id])
            ->first();
        $this->assertEquals('REVIEW', $account->status);

        $this->cleanupSelfSignupUserApproval($setup);
    }

    public function testSelfInviteUserNeedsApprovalSuccessAdminApproval()
    {
        $setup = $this->setupSelfSignupUserApproval();

        $account = $this->accountsTable->find('all')
            ->where(['id' => $setup['account']->id])
            ->first();
        $this->assertEquals('REVIEW', $account->status);

        // approve user as admin
        $this->setAdminAuth();
        $this->post('/accounts/approve-user/' . $setup['account']->id);

        $account = $this->accountsTable->find('all')
            ->where(['id' => $setup['account']->id])
            ->first();
        $this->assertEquals('APPROVED', $account->status);

        // user should be able to finish signup process
        $this->setPublicAuth();
        $this->get('/accounts/invite/' . $setup['account']->security_code);

        $this->cleanupSelfSignupUserApproval($setup);
    }

    public function testSelfInviteUserNeedsApprovalSuccessAdminDecline()
    {
        $setup = $this->setupSelfSignupUserApproval();

        $account = $this->accountsTable->find('all')
            ->where(['id' => $setup['account']->id])
            ->first();
        $this->assertEquals('REVIEW', $account->status);

        // approve user as admin
        $this->setAdminAuth();
        $this->post('/accounts/decline-user/' . $setup['account']->id);

        $account = $this->accountsTable->find('all')
            ->where(['id' => $setup['account']->id])
            ->first();
        $this->assertEquals('DISABLED', $account->status);

        $this->cleanupSelfSignupUserApproval($setup);
    }

    public function testSelfInviteUserNeedsApprovalSuccessAdminApprovalToggleStatuses()
    {
        $setup = $this->setupSelfSignupUserApproval();

        $account = $this->accountsTable->find('all')
            ->where(['id' => $setup['account']->id])
            ->first();
        $this->assertEquals('REVIEW', $account->status);

        // approve user as admin
        $this->setAdminAuth();
        $this->post('/accounts/approve-user/' . $setup['account']->id);

        $account = $this->accountsTable->find('all')
            ->where(['id' => $setup['account']->id])
            ->first();
        $this->assertEquals('APPROVED', $account->status);

        // user should be able to finish signup process
        $this->setPublicAuth();
        $this->get('/accounts/invite/' . $setup['account']->security_code);

        // disable user as admin
        $this->setAdminAuth();
        $this->post('/accounts/disable-user/' . $setup['account']->id);

        $account = $this->accountsTable->find('all')
            ->where(['id' => $setup['account']->id])
            ->first();
        $this->assertEquals('DISABLED', $account->status);

        // enable user as admin
        $this->setAdminAuth();
        $this->post('/accounts/enable-user/' . $setup['account']->id);

        $account = $this->accountsTable->find('all')
            ->where(['id' => $setup['account']->id])
            ->first();
        $this->assertEquals('ACTIVE', $account->status);

        $this->cleanupSelfSignupUserApproval($setup);
    }

    public function cleanupSelfSignupUserApproval($setup)
    {
        // delete test account
        $entity = $this->accountsTable->get($setup['account']->id);
        $this->accountsTable->delete($entity);

        //delete test user
        $entity = $this->usersTable->get($setup['user']->id);
        $this->usersTable->delete($entity);

        // delete test signup domain
        $entity = $this->signupDomainsTable->get($setup['signupDomain']->id);
        $this->signupDomainsTable->delete($entity);

        $this->disableSelfSignup();
    }

    private function setUserInvite($isEnabled)
    {
        $this->session([
            'Invite' => (object)[
                'id' => '3597a073-fd9c-4b13-917a-8f947249dedc',
                'security_code' => '9bed23f4-f544-11eb-9a03-0242ac130003',
                'user' => (object)[
                    'id' => '84f04c1e-1d88-11ec-8296-d2ea1fd9e5ee',
                    'email' => 'jeff.morris@outlook.com',
                    'is_enabled' => (bool)$isEnabled
                ],
                'tenant' => (object)[
                    'name' => 'JPM Strategies'
                ]
            ]
        ]);
    }

    private function createTestRecord()
    {
        $account = $this->usersTable->newEntity([
            'email' => 'delete@apptest.com',
            'password' => 'jpmtesting',
            'password_confirm' => 'jpmtesting'
        ]);
        $result = $this->usersTable->save($account);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->usersTable->get($id);
        return $this->usersTable->delete($entity);
    }
}
