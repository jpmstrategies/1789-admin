<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\RatingTypesController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\RatingTypesController Test Case
 *
 * @uses \App\Controller\RatingTypesController
 */
class RatingTypesControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $RatingTypesTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->RatingTypesTable = TableRegistry::getTableLocator()->get('RatingTypes');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/RatingTypes/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/RatingTypes/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/RatingTypes/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/RatingTypes/index');
        $this->assertLoginRedirect('/RatingTypes/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/RatingTypes/view/1');
        $this->assertLoginRedirect('/RatingTypes/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/RatingTypes/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/RatingTypes/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/RatingTypes/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/RatingTypes/add');
        $this->assertLoginRedirect('/RatingTypes/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/RatingTypes/edit/1');
        $this->assertLoginRedirect('/RatingTypes/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/RatingTypes/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->RatingTypesTable->newEntity([]);
        $result = $this->RatingTypesTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->RatingTypesTable->get($id);
        return $this->RatingTypesTable->delete($entity);
    }
}
