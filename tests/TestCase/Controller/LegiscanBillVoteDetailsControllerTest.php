<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\LegiscanBillVoteDetailsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\LegiscanBillVoteDetailsController Test Case
 *
 * @uses \App\Controller\LegiscanBillVoteDetailsController
 */
class LegiscanBillVoteDetailsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $LegiscanBillVoteDetailsTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->LegiscanBillVoteDetailsTable = TableRegistry::getTableLocator()->get('LegiscanBillVoteDetails');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/LegiscanBillVoteDetails/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/LegiscanBillVoteDetails/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/LegiscanBillVoteDetails/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/LegiscanBillVoteDetails/index');
        $this->assertLoginRedirect('/LegiscanBillVoteDetails/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/LegiscanBillVoteDetails/view/1');
        $this->assertLoginRedirect('/LegiscanBillVoteDetails/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/LegiscanBillVoteDetails/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/LegiscanBillVoteDetails/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/LegiscanBillVoteDetails/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/LegiscanBillVoteDetails/add');
        $this->assertLoginRedirect('/LegiscanBillVoteDetails/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/LegiscanBillVoteDetails/edit/1');
        $this->assertLoginRedirect('/LegiscanBillVoteDetails/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/LegiscanBillVoteDetails/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->LegiscanBillVoteDetailsTable->newEntity([]);
        $result = $this->LegiscanBillVoteDetailsTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->LegiscanBillVoteDetailsTable->get($id);
        return $this->LegiscanBillVoteDetailsTable->delete($entity);
    }
}
