<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\LegiscanSubjectsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\LegiscanSubjectsController Test Case
 *
 * @uses \App\Controller\LegiscanSubjectsController
 */
class LegiscanSubjectsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $LegiscanSubjectsTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->LegiscanSubjectsTable = TableRegistry::getTableLocator()->get('LegiscanSubjects');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/LegiscanSubjects/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/LegiscanSubjects/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/LegiscanSubjects/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/LegiscanSubjects/index');
        $this->assertLoginRedirect('/LegiscanSubjects/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/LegiscanSubjects/view/1');
        $this->assertLoginRedirect('/LegiscanSubjects/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/LegiscanSubjects/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/LegiscanSubjects/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/LegiscanSubjects/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/LegiscanSubjects/add');
        $this->assertLoginRedirect('/LegiscanSubjects/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/LegiscanSubjects/edit/1');
        $this->assertLoginRedirect('/LegiscanSubjects/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/LegiscanSubjects/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->LegiscanSubjectsTable->newEntity([]);
        $result = $this->LegiscanSubjectsTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->LegiscanSubjectsTable->get($id);
        return $this->LegiscanSubjectsTable->delete($entity);
    }
}
