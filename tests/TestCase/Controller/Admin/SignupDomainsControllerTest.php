<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\SignupDomainsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use App\Test\TestCase\Controller\BaseJpmControllerTest;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\Admin\SignupDomainsController Test Case
 *
 * @uses \App\Controller\Admin\SignupDomainsController
 */
class SignupDomainsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $SignupDomainsTable;

    protected $signupDomainsController;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->SignupDomainsTable = TableRegistry::getTableLocator()->get('SignupDomains');
        $this->signupDomainsController = new SignupDomainsController();
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */

    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/admin/SignupDomains/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/admin/SignupDomains/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/admin/SignupDomains/index');
        $this->assertRedirect(['controller' => 'pages', 'action' => 'home', 'prefix' => false]);
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/admin/SignupDomains/index');
        $this->assertLoginRedirect('/admin/SignupDomains/index');
    }

    /**
     * Test method: view
     */

    public function testViewAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $id = $this->createTestRecord();
        $this->get('/admin/SignupDomains/view/' . $id);
        $this->assertResponseOk();

        $this->deleteTestRecord($id);
    }

    public function testViewAsAdmin(): void
    {
        $this->setAdminAuth();
        $id = $this->createTestRecord();
        $this->get('/admin/SignupDomains/view/' . $id);
        $this->assertResponseOk();

        $this->deleteTestRecord($id);
    }

    public function testViewAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/admin/SignupDomains/view/1');
        $this->assertRedirect(['controller' => 'pages', 'action' => 'home', 'prefix' => false]);
    }

    public function testViewAsPublicUser(): void
    {
        $this->get('/admin/SignupDomains/view/1');
        $this->assertLoginRedirect('/admin/SignupDomains/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/admin/SignupDomains/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/admin/SignupDomains/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/admin/SignupDomains/add');
        $this->assertRedirect(['controller' => 'pages', 'action' => 'home', 'prefix' => false]);
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/admin/SignupDomains/add');
        $this->assertLoginRedirect('/admin/SignupDomains/add');
    }

    /**
     * Test method: edit
     */

    public function testEditAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $id = $this->createTestRecord();
        $this->get('/admin/SignupDomains/edit/' . $id);
        $this->assertResponseOk();

        $this->deleteTestRecord($id);
    }

    public function testEditAsAdmin(): void
    {
        $this->setAdminAuth();
        $id = $this->createTestRecord();
        $this->get('/admin/SignupDomains/edit/' . $id);
        $this->assertResponseOk();

        $this->deleteTestRecord($id);
    }

    public function testEditAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/admin/SignupDomains/edit/1');
        $this->assertRedirect(['controller' => 'pages', 'action' => 'home', 'prefix' => false]);
    }

    public function testEditAsPublicUser(): void
    {
        $this->get('/admin/SignupDomains/edit/1');
        $this->assertLoginRedirect('/admin/SignupDomains/edit/1');
    }

    /**
     * Test method: delete
     */

    public function testDeleteAsSuperAdminUser(): void
    {
        $id = $this->createTestRecord();

        $this->setSuperAdminAuth();
        $this->enableCsrfToken();
        $this->post('/admin/SignupDomains/delete/' . $id);
        $this->assertRedirect(['action' => 'index']);
    }

    public function testDeleteAsAdminUser(): void
    {
        $id = $this->createTestRecord();

        $this->setAdminAuth();
        $this->enableCsrfToken();
        $this->post('/admin/SignupDomains/delete/' . $id);
        $this->assertRedirect(['action' => 'index']);
    }

    public function testDeleteAsUser(): void
    {
        $this->setUserAuth();
        $this->enableCsrfToken();
        $this->post('/admin/SignupDomains/delete/1');
        $this->assertRedirect(['controller' => 'pages', 'action' => 'home', 'prefix' => false]);
    }

    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/admin/SignupDomains/delete/1');
        $this->assertLoginRedirect();
    }

    public function testCheckSignupDomainEmpty()
    {
        $results = $this->signupDomainsController->checkSignupDomain('test@test.com');
        $this->assertEquals(true, $results);
    }

    public function testCheckSignupDomainValidEmail()
    {
        $domain1 = $this->createTestRecord([
            'name' => 'Test 1',
            'regex' => '.gov',
        ]);
        $domain2 = $this->createTestRecord([
            'name' => 'Test 2',
            'regex' => 'no-at.com',
        ]);
        $domain3 = $this->createTestRecord([
            'name' => 'Test 3',
            'regex' => '@at.com',
        ]);
        $domain4 = $this->createTestRecord([
            'name' => 'Test 4',
            'regex' => '.main-domain.com',
        ]);
        $domain5 = $this->createTestRecord([
            'name' => 'Test 5',
            'regex' => 'office.agency.com',
        ]);

        $results = $this->signupDomainsController->checkSignupDomain('john@test.gov');
        $this->assertEquals(true, $results);

        $results = $this->signupDomainsController->checkSignupDomain('john@test.test.gov');
        $this->assertEquals(true, $results);

        $results = $this->signupDomainsController->checkSignupDomain('john@no-at.com');
        $this->assertEquals(true, $results);

        $results = $this->signupDomainsController->checkSignupDomain('john@sub.no-at.com');
        $this->assertEquals(true, $results);

        $results = $this->signupDomainsController->checkSignupDomain('john@at.com');
        $this->assertEquals(true, $results);

        $results = $this->signupDomainsController->checkSignupDomain('john@test.main-domain.com');
        $this->assertEquals(true, $results);

        $results = $this->signupDomainsController->checkSignupDomain('john@office.agency.com');
        $this->assertEquals(true, $results);

        // test no partial matches
        $results = $this->signupDomainsController->checkSignupDomain('john@main-domain.com');
        $this->assertEquals(false, $results);

        $results = $this->signupDomainsController->checkSignupDomain('john@before-no-at.com');
        $this->assertEquals(false, $results);

        $results = $this->signupDomainsController->checkSignupDomain('john@invalid.com');
        $this->assertEquals(false, $results);

        $results = $this->signupDomainsController->checkSignupDomain('john@sub.at.com');
        $this->assertEquals(false, $results);

        $results = $this->signupDomainsController->checkSignupDomain('john@agency.com');
        $this->assertEquals(false, $results);

        // test full TLD enforcement (no partial matches)
        $results = $this->signupDomainsController->checkSignupDomain('john@no-at.commmerical');
        $this->assertEquals(false, $results);

        $results = $this->signupDomainsController->checkSignupDomain('john@at.commmerical');
        $this->assertEquals(false, $results);

        $results = $this->signupDomainsController->checkSignupDomain('john@test.govervnment');
        $this->assertEquals(false, $results);

        $this->deleteTestRecord($domain1);
        $this->deleteTestRecord($domain2);
        $this->deleteTestRecord($domain3);
        $this->deleteTestRecord($domain4);
        $this->deleteTestRecord($domain5);
    }

    public function createTestRecord($data = [])
    {
        if (empty($data)) {
            $data = [
                'name' => 'All Domains',
                'regex' => '*',
            ];
        }

        $record = $this->SignupDomainsTable->newEntity($data);
        $result = $this->SignupDomainsTable->save($record);
        return $result['id'];
    }

    public function deleteTestRecord($id)
    {
        $entity = $this->SignupDomainsTable->get($id);
        return $this->SignupDomainsTable->delete($entity);
    }
}
