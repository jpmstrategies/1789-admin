<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\PositionsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\PositionsController Test Case
 *
 * @uses \App\Controller\PositionsController
 */
class PositionsControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $PositionsTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->PositionsTable = TableRegistry::getTableLocator()->get('Positions');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/Positions/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/Positions/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/Positions/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/Positions/index');
        $this->assertLoginRedirect('/Positions/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/Positions/view/1');
        $this->assertLoginRedirect('/Positions/view/1');
    }

    /**
     * Test method: add
     */
    public function testAddAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/Positions/add');
        $this->assertResponseOk();
    }

    public function testAddAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/Positions/add');
        $this->assertResponseOk();
    }

    public function testAddAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/Positions/add');
        $this->assertResponseOk();
    }

    public function testAddAsPublicUser(): void
    {
        $this->get('/Positions/add');
        $this->assertLoginRedirect('/Positions/add');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/Positions/edit/1');
        $this->assertLoginRedirect('/Positions/edit/1');
    }

    /**
     * Test method: delete
     */


    public function testDeleteAsPublicUser(): void
    {
        $this->enableCsrfToken();
        $this->post('/Positions/delete/1');
        $this->assertLoginRedirect();
    }

    private function createTestRecord()
    {
        $record = $this->PositionsTable->newEntity([]);
        $result = $this->PositionsTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->PositionsTable->get($id);
        return $this->PositionsTable->delete($entity);
    }
}
