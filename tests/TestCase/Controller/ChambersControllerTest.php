<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\ChambersController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\ChambersController Test Case
 *
 * @uses \App\Controller\ChambersController
 */
class ChambersControllerTest extends BaseJpmControllerTest
{
    use IntegrationTestTrait;

    protected Table $ChambersTable;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->enableRetainFlashMessages();
        $this->ChambersTable = TableRegistry::getTableLocator()->get('Chambers');
        $this->loadRoutes();
    }

    /**
     * Test method: index
     */
    public function testIndexAsSuperAdmin(): void
    {
        $this->setSuperAdminAuth();
        $this->get('/Chambers/index');
        $this->assertResponseOk();
    }

    public function testIndexAsAdmin(): void
    {
        $this->setAdminAuth();
        $this->get('/Chambers/index');
        $this->assertResponseOk();
    }

    public function testIndexAsUser(): void
    {
        $this->setUserAuth();
        $this->get('/Chambers/index');
        $this->assertResponseOk();
    }

    public function testIndexAsPublicUser(): void
    {
        $this->get('/Chambers/index');
        $this->assertLoginRedirect('/Chambers/index');
    }

    /**
     * Test method: view
     */


    public function testViewAsPublicUser(): void
    {
        $this->get('/Chambers/view/1');
        $this->assertLoginRedirect('/Chambers/view/1');
    }

    /**
     * Test method: edit
     */


    public function testEditAsPublicUser(): void
    {
        $this->get('/Chambers/edit/1');
        $this->assertLoginRedirect('/Chambers/edit/1');
    }

    private function createTestRecord()
    {
        $record = $this->ChambersTable->newEntity([]);
        $result = $this->ChambersTable->save($record);
        return $result['id'];
    }

    private function deleteTestRecord($id)
    {
        $entity = $this->ChambersTable->get($id);
        return $this->ChambersTable->delete($entity);
    }
}
