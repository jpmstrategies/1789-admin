<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\EmailTrait;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\AccountsController Test Case
 *
 * @uses \App\Controller\AccountsController
 */
abstract class BaseJpmControllerTest extends TestCase
{
    use IntegrationTestTrait;
    use EmailTrait;

    protected $configurationTypesTable;

    public function setUp(): void
    {
        parent::setUp();

        $this->configurationTypesTable = TableRegistry::getTableLocator()->get('ConfigurationTypes');

        Configure::write('users.self_signup_enabled', 0);
        Configure::write('users.default_login_redirect', null);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->getTableLocator()->clear();
    }

    /**
     * setup auth for user roles
     */
    public function setSuperAdminAuth()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => '84f04bec-1d88-11ec-8296-d2ea1fd9e5ee',
                    'email' => 'jeff@jpmstrategies.com',
                    'is_super' => true,
                ],
            ],
            'Account' => [
                'numberOfTenants' => 1,
                'currentTenantId' => '6f84587a-1d88-11ec-8296-d2ea1fd9e5ee',
                'currentTenantName' => 'JPM Strategies',
                'currentTenantRole' => 'A',
                'currentTenantState' => 'TX',
            ],
        ]);
    }

    public function setAdminAuth()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => '84f04b10-1d88-11ec-8296-d2ea1fd9e5ee',
                    'email' => 'el.jefe42@gmail.com',
                    'is_super' => false,
                ],
            ],
            'Account' => [
                'numberOfTenants' => 1,
                'currentTenantId' => '6f84587a-1d88-11ec-8296-d2ea1fd9e5ee',
                'currentTenantName' => 'JPM Strategies',
                'currentTenantRole' => 'A',
                'currentTenantState' => 'TX',
            ],
        ]);
    }

    public function setUserAuth()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => '84f04c1e-1d88-11ec-8296-d2ea1fd9e5ee',
                    'email' => 'jeff.morris@outlook.com',
                    'is_super' => false,
                ],
            ],
            'Account' => [
                'numberOfTenants' => 1,
                'currentTenantId' => '6f84587a-1d88-11ec-8296-d2ea1fd9e5ee',
                'currentTenantName' => 'JPM Strategies',
                'currentTenantRole' => 'U',
                'currentTenantState' => 'TX',
            ],
        ]);
    }

    public function setPublicAuth()
    {
        $this->session([
            'Auth' => [],
        ]);
    }

    public function setUserMultiTenantAuth()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => '52ca0771-f3a7-4d49-ab74-d5f5c2696128',
                    'email' => 'multi-tenant@jpmstrategies.com',
                    'is_super' => false,
                ],
            ],
            'Account' => [
                'numberOfTenants' => 2,
                'currentTenantId' => '6f84587a-1d88-11ec-8296-d2ea1fd9e5ee',
                'currentTenantName' => 'JPM Strategies',
                'currentTenantRole' => 'U',
                'currentTenantState' => 'TX',
            ],
        ]);
    }

    public function assertAccessDenied()
    {
        $this->assertFlashMessage('Access denied');
        $this->assertRedirect(['controller' => 'Pages', 'action' => 'home', 'plugin' => false, 'prefix' => false]);
    }

    public function assertLoginRedirect($redirectPath = null)
    {
        $params = [
            'controller' => 'users',
            'action' => 'login',
            'plugin' => false,
            'prefix' => false
        ];

        if (isset($redirectPath)) {
            $params['?'] = ['redirect' => $redirectPath];
        }
        $this->assertRedirect($params);
        $this->assertFlashMessage('You are not authorized to access that location.');
    }

    public function createCustomField($overrideValues = [])
    {
        $data = [
            'scope' => '',
            'name' => 'first_name',
            'description' => 'First Name',
            'data_type' => 'text',
            'default_value' => '',
            'tab' => 'user_details',
            'is_editable' => 1,
            'is_enabled' => 1,
            'is_public' => 1,
            'is_required' => '1',
            'required_regex' => '.*',
            'sort_order' => 1
        ];
        foreach ($overrideValues as $key => $value) {
            $data[$key] = $value;
        }
        $configurationType = $this->configurationTypesTable->newEntity($data);
        $result = $this->configurationTypesTable->save($configurationType);
        return $result['id'];
    }

    public function deleteCustomField($id): bool
    {
        $entity = $this->configurationTypesTable->get($id);
        return $this->configurationTypesTable->delete($entity);
    }
}
