<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\RatingFieldHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\RatingFieldHelper Test Case
 */
class RatingFieldHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\RatingFieldHelper
     */
    protected $RatingFieldHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->RatingFieldHelper = new RatingFieldHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->RatingFieldHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->RatingFieldHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->RatingFieldHelper->display_text('B');
        $this->assertStringContainsString('Boolean', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->RatingFieldHelper->display_text('Z');
        $this->assertStringContainsString('Rating Field Type', $result);
    }
}
