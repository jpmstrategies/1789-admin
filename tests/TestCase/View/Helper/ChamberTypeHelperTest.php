<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\ChamberTypeHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\ChamberTypeHelper Test Case
 */
class ChamberTypeHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\ChamberTypeHelper
     */
    protected $ChamberTypeHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->ChamberTypeHelper = new ChamberTypeHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->ChamberTypeHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->ChamberTypeHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->ChamberTypeHelper->display_text('L');
        $this->assertStringContainsString('State Lower Chamber', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->ChamberTypeHelper->display_text('Z');
        $this->assertStringContainsString('Unknown Chamber Type', $result);
    }
}
