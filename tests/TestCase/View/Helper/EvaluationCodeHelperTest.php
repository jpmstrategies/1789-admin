<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\EvaluationCodeHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\EvaluationCodeHelper Test Case
 */
class EvaluationCodeHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\EvaluationCodeHelper
     */
    protected $EvaluationCodeHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->EvaluationCodeHelper = new EvaluationCodeHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->EvaluationCodeHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->EvaluationCodeHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->EvaluationCodeHelper->display_text('N');
        $this->assertStringContainsString('Not Applicable', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->EvaluationCodeHelper->display_text('Z');
        $this->assertStringContainsString('Unknown Evaluation Code', $result);
    }
}
