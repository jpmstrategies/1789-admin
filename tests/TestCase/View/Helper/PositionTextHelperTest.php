<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\PositionTextHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\PositionTextHelper Test Case
 */
class PositionTextHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\PositionTextHelper
     */
    protected $PositionTextHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->PositionTextHelper = new PositionTextHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->PositionTextHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->PositionTextHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->PositionTextHelper->display_text('U');
        $this->assertStringContainsString('Unknown', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->PositionTextHelper->display_text('Z');
        $this->assertStringContainsString('Unknown Position Text', $result);
    }
}
