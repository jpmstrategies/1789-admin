<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\MethodHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\MethodHelper Test Case
 */
class MethodHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\MethodHelper
     */
    protected $MethodHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->MethodHelper = new MethodHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->MethodHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->MethodHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->MethodHelper->display_text('S');
        $this->assertStringContainsString('Standard', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->MethodHelper->display_text('Z');
        $this->assertStringContainsString('Unknown Method', $result);
    }

    public function testHelpTextValid(): void
    {
        $result = $this->MethodHelper->help_text('S');
        $this->assertStringContainsString('Tracks positive and negative vote evaluations', $result);
    }

    public function testHelpTextInvalid(): void
    {
        $result = $this->MethodHelper->help_text('Z');
        $this->assertStringContainsString('Unknown Method', $result);

        $result = $this->MethodHelper->help_text('');
        $this->assertStringContainsString('Unknown Method', $result);
    }
}
