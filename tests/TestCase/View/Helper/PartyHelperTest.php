<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\PartyHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\PartyHelper Test Case
 */
class PartyHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\PartyHelper
     */
    protected $PartyHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->PartyHelper = new PartyHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->PartyHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->PartyHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->PartyHelper->display_text('U');
        $this->assertStringContainsString('Unknown', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->PartyHelper->display_text('Z');
        $this->assertStringContainsString('Unknown Party', $result);
    }
}
