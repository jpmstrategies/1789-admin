<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\EnableHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\EnableHelper Test Case
 */
class EnableHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\EnableHelper
     */
    protected $EnableHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->EnableHelper = new EnableHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->EnableHelper);

        parent::tearDown();
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->EnableHelper->display_text(1);
        $this->assertStringContainsString('Yes', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->EnableHelper->display_text(0);
        $this->assertStringContainsString('No', $result);
    }
}
