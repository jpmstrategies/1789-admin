<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\PositionHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\PositionHelper Test Case
 */
class PositionHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\PositionHelper
     */
    protected $PositionHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->PositionHelper = new PositionHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->PositionHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->PositionHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->PositionHelper->display_text('U');
        $this->assertStringContainsString('Unknown', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->PositionHelper->display_text('Z');
        $this->assertStringContainsString('Unknown Position', $result);
    }
}
