<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\AddressTypeHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\AddressTypeHelper Test Case
 */
class AddressTypeHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\AddressTypeHelper
     */
    protected $AddressTypeHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->AddressTypeHelper = new AddressTypeHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->AddressTypeHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->AddressTypeHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->AddressTypeHelper->display_text('C');
        $this->assertStringContainsString('Capitol', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->AddressTypeHelper->display_text('Z');
        $this->assertStringContainsString('Unknown Address Type', $result);
    }
}
