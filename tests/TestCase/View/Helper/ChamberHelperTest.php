<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\ChamberHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\ChamberHelper Test Case
 */
class ChamberHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\ChamberHelper
     */
    protected $ChamberHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->ChamberHelper = new ChamberHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->ChamberHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->ChamberHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->ChamberHelper->display_text('U');
        $this->assertStringContainsString('Upper Chamber', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->ChamberHelper->display_text('Z');
        $this->assertStringContainsString('Unknown Chamber', $result);
    }

    public function testTitleShortLower(): void
    {
        $result = $this->ChamberHelper->title_short('L');
        $this->assertStringContainsString('Rep.', $result);
    }

    public function testTitleShortUpper(): void
    {
        $result = $this->ChamberHelper->title_short('U');
        $this->assertStringContainsString('Sen.', $result);
    }

    public function testTitleShortInvalid(): void
    {
        $result = $this->ChamberHelper->title_short('Z');
        $this->assertStringContainsString('Unknown Title', $result);
    }

    public function testTitleLongLower(): void
    {
        $result = $this->ChamberHelper->title_Long('L');
        $this->assertStringContainsString('State Rep.', $result);
    }

    public function testTitleLongUpper(): void
    {
        $result = $this->ChamberHelper->title_Long('U');
        $this->assertStringContainsString('State Sen.', $result);
    }

    public function testTitleLongInvalid(): void
    {
        $result = $this->ChamberHelper->title_Long('Z');
        $this->assertStringContainsString('Unknown Title', $result);
    }
}
