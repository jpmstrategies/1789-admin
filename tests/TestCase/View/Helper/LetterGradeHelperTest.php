<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\LetterGradeHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\LetterGradeHelper Test Case
 */
class LetterGradeHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\LetterGradeHelper
     */
    protected $LetterGradeHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->LetterGradeHelper = new LetterGradeHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->LetterGradeHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->LetterGradeHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->LetterGradeHelper->display_text('N/A');
        $this->assertStringContainsString('N/A', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->LetterGradeHelper->display_text('Z');
        $this->assertStringContainsString('Unknown Letter Grade', $result);
    }
}
