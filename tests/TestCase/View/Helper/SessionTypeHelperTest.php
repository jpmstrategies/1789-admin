<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\SessionTypeHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\SessionTypeHelper Test Case
 */
class SessionTypeHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\SessionTypeHelper
     */
    protected $SessionTypeHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->SessionTypeHelper = new SessionTypeHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->SessionTypeHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->SessionTypeHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->SessionTypeHelper->display_text('R');
        $this->assertStringContainsString('Regular', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->SessionTypeHelper->display_text('invalid');
        $this->assertStringContainsString('Session Type', $result);
    }
}
