<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\ScoreTypeHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\ScoreTypeHelper Test Case
 */
class ScoreTypeHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\ScoreTypeHelper
     */
    protected $ScoreTypeHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->ScoreTypeHelper = new ScoreTypeHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->ScoreTypeHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->ScoreTypeHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->ScoreTypeHelper->display_text('N');
        $this->assertStringContainsString('Negative', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->ScoreTypeHelper->display_text('invalid');
        $this->assertStringContainsString('Score Type', $result);
    }
}
