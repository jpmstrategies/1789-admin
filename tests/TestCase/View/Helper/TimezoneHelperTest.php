<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\TimezoneHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\TimezoneHelper Test Case
 */
class TimezoneHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\TimezoneHelper
     */
    protected $TimezoneHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->TimezoneHelper = new TimezoneHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->TimezoneHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->TimezoneHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->TimezoneHelper->display_text('America/Chicago');
        $this->assertStringContainsString('America/Chicago', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->TimezoneHelper->display_text('America/Dallas');
        $this->assertStringContainsString('Unknown Timezone', $result);
    }
}
