<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\AccountStatusHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\AccountStatusHelper Test Case
 */
class AccountStatusHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\AccountStatusHelper
     */
    protected $AccountStatusHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->AccountStatusHelper = new AccountStatusHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->AccountStatusHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->AccountStatusHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->AccountStatusHelper->display_text('PENDING');
        $this->assertStringContainsString('Invite Pending', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->AccountStatusHelper->display_text('Z');
        $this->assertStringContainsString('Unknown Account Status', $result);
    }
}
