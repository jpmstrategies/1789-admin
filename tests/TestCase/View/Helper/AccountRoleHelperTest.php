<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\AccountRoleHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\AccountRoleHelper Test Case
 */
class AccountRoleHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\AccountRoleHelper
     */
    protected $AccountRoleHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->AccountRoleHelper = new AccountRoleHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->AccountRoleHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->AccountRoleHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->AccountRoleHelper->display_text('A');
        $this->assertStringContainsString('Admin', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->AccountRoleHelper->display_text('Z');
        $this->assertStringContainsString('Unknown Account Role', $result);
    }
}
