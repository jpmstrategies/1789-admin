<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\ConfigurationTabHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\ConfigurationTabHelper Test Case
 */
class ConfigurationTabHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\ConfigurationTabHelper
     */
    protected $ConfigurationTabHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->ConfigurationTabHelper = new ConfigurationTabHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->ConfigurationTabHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->ConfigurationTabHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->ConfigurationTabHelper->display_text('admin');
        $this->assertStringContainsString('Super Admin', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->ConfigurationTabHelper->display_text('test');
        $this->assertStringContainsString('Unknown Configuration Tab', $result);
    }

    public function testGetOptions(): void
    {
        $result = $this->ConfigurationTabHelper->get_options();
        $this->assertCount(9, $result);
    }
}
