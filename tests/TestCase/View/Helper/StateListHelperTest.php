<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\StateListHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\StateListHelper Test Case
 */
class StateListHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\StateListHelper
     */
    protected $StateListHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->StateListHelper = new StateListHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->StateListHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->StateListHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->StateListHelper->display_text('TX');
        $this->assertStringContainsString('Texas', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->StateListHelper->display_text('ZZ');
        $this->assertStringContainsString('Unknown State', $result);
    }
}
