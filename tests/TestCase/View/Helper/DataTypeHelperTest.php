<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\DataTypeHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\DataTypeHelper Test Case
 */
class DataTypeHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\DataTypeHelper
     */
    protected $DataTypeHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->DataTypeHelper = new DataTypeHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->DataTypeHelper);

        parent::tearDown();
    }

    public function testControl(): void
    {
        $result = $this->DataTypeHelper->control('test-input');
        $this->assertStringContainsString('<div class="input select">', $result);
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->DataTypeHelper->display_text('text');
        $this->assertStringContainsString('Text', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->DataTypeHelper->display_text('test');
        $this->assertStringContainsString('Unknown Data Type', $result);
    }

    public function testGetOptions(): void
    {
        $result = $this->DataTypeHelper->get_options();
        $this->assertCount(4, $result);
    }
}
