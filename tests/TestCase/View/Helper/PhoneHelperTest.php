<?php
declare(strict_types=1);

namespace App\Test\TestCase\View\Helper;

use App\View\Helper\PhoneHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\PhoneHelper Test Case
 */
class PhoneHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\PhoneHelper
     */
    protected $PhoneHelper;

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->PhoneHelper = new PhoneHelper($view);
    }

    /**
     * tearDown method
     */
    public function tearDown(): void
    {
        unset($this->PhoneHelper);

        parent::tearDown();
    }

    public function testDisplayTextValid(): void
    {
        $result = $this->PhoneHelper->display_text('888-888-8888');
        $this->assertStringContainsString('(888) 888-8888', $result);
    }

    public function testDisplayTextInvalid(): void
    {
        $result = $this->PhoneHelper->display_text('test');
        $this->assertStringContainsString('test', $result);
    }
}
