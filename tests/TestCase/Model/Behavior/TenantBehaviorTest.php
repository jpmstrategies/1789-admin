<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Behavior;

use App\Event\TenantListener;
use App\Model\Behavior\TenantBehavior;
use Cake\Event\EventManager;
use Cake\ORM\Table;
use Cake\TestSuite\TestCase;
use Exception;

/**
 * App\Model\Behavior\TenantBehavior Test Case
 */
class TenantBehaviorTest extends TestCase
{
    protected $fixtures = [
        'app.BehaviorArticles',
        'app.BehaviorTenants',
    ];
    protected $articlesTable;
    protected $tenantsTable;

    public function setUp(): void
    {
        parent::setUp();

        $this->tenantsTable = $this->getTableLocator()->get('app.BehaviorTenants');
        $this->articlesTable = $this->getTableLocator()->get('app.BehaviorArticles');
        $this->articlesTable->addBehavior('Tenant', [
            'foreign_key_field' => 'tenant_id',
            'sync_table' => 'BehaviorTenants'
        ]);

        $tenantListener = new TenantListener('a19cf524-1d97-11ec-9621-0242ac130002');
        EventManager::instance()->on($tenantListener);

        $this->resetSyncPendingDatetime();
    }

    public function testDefaultConfig(): void
    {
        $table = new Table(['table' => 'BehaviorArticles']);
        $TenantBehavior = new TenantBehavior($table);

        $this->assertEquals('tenant_id', $TenantBehavior->getConfig('foreign_key_field'));
        $this->assertEquals('Tenants', $TenantBehavior->getConfig('sync_table'));
    }

    public function testOverrideConfig(): void
    {
        $table = new Table(['table' => 'BehaviorArticles']);
        $TenantBehavior = new TenantBehavior($table, [
            'foreign_key_table' => 'Tenants',
            'foreign_key_field' => 'tenant_id',
            'sync_table' => 'Tenants'
        ]);

        $this->assertEquals('Tenants', $TenantBehavior->getConfig('foreign_key_table'));
        $this->assertEquals('tenant_id', $TenantBehavior->getConfig('foreign_key_field'));
        $this->assertEquals('Tenants', $TenantBehavior->getConfig('sync_table'));
    }

    /**
     * Test method: beforeFind
     */
    public function testBeforeFind(): void
    {
        $result = $this->articlesTable->find('all');
        $this->assertTextContains('Articles.tenant_id =', $result->sql());

        $this->articlesTable->removeBehavior('Tenant');
        $result = $this->articlesTable->find('all');
        $this->assertTextNotContains('Articles.tenant_id =', $result->sql());
    }

    /**
     * Test method: beforeSave
     */
    public function testSave(): void
    {
        $entity = $this->articlesTable->get('a4980246-2cc2-11ee-be56-0242ac120002');

        // test paranoid check is never triggered
        $entity->tenant_id = '8780599c-1d97-11ec-9621-0242ac130002';
        $result = $this->articlesTable->save($entity);
        $this->assertEquals('a19cf524-1d97-11ec-9621-0242ac130002', $result['tenant_id']);

        // test update with correct ownership
        $entity->tenant_id = 'a19cf524-1d97-11ec-9621-0242ac130002';
        $entity->title = 'testing update';
        $result = $this->articlesTable->save($entity);
        $this->assertEquals('a19cf524-1d97-11ec-9621-0242ac130002', $result['tenant_id']);
        $this->assertEquals('testing update', $result['title']);

        $this->checkSyncPendingDatetime('a19cf524-1d97-11ec-9621-0242ac130002');
    }

    /**
     * Test method: beforeDelete
     */
    public function testDelete(): void
    {
        $entity = $this->articlesTable->get('a4980246-2cc2-11ee-be56-0242ac120002');

        // test paranoid check of ownership
        try {
            $entity->tenant_id = 3;
            $this->articlesTable->delete($entity);
            throw new Exception("delete test should not get this far");
        } catch (Exception $e) {
            $this->assertTextContains('does not own', $e->getMessage());
        }

        // test delete with correct ownership
        $entity->tenant_id = 'a19cf524-1d97-11ec-9621-0242ac130002';
        $result = $this->articlesTable->delete($entity);
        $this->assertTrue($result);

        $this->checkSyncPendingDatetime('a19cf524-1d97-11ec-9621-0242ac130002');
    }

    function resetSyncPendingDatetime()
    {
        $this->tenantsTable->query()
            ->update()
            ->set(['sync_pending' => null])
            ->execute();
    }

    function checkSyncPendingDatetime($tenantId)
    {
        $results = $this->tenantsTable->query()
            ->where(['id' => $tenantId])
            ->first();
        $this->assertNotNull($results['sync_pending']);
    }
}