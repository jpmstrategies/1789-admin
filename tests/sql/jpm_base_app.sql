drop database test_1789_cornerstone;

create database test_1789_cornerstone;

use test_1789_cornerstone;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `accounts`;

DROP TABLE IF EXISTS `behavior_articles`;

DROP TABLE IF EXISTS `behavior_tenants`;

DROP TABLE IF EXISTS `configurations`;

DROP TABLE IF EXISTS `configuration_options`;

DROP TABLE IF EXISTS `configuration_types`;

DROP TABLE IF EXISTS `users`;

DROP TABLE IF EXISTS `tenants`;

DROP TABLE IF EXISTS `constants`;

DROP TABLE IF EXISTS `logs`;

DROP TABLE IF EXISTS `phinxlog`;

DROP TABLE IF EXISTS `sessions`;

DROP TABLE IF EXISTS `queue_phinxlog`;

DROP TABLE IF EXISTS `queue_processes`;

DROP TABLE IF EXISTS `queued_jobs`;

DROP TABLE IF EXISTS `signup_domains`;

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts`
(
    `id`                 char(36)     NOT NULL,
    `tenant_id`          char(36)     NOT NULL,
    `user_id`            char(36)     NOT NULL,
    `invited_by_user_id` char(36)     null,
    `role`               char(1)      NOT NULL DEFAULT 'U',
    `status`             varchar(255) NOT NULL DEFAULT 'PENDING',
    `security_code`      varchar(255)          DEFAULT NULL,
    `modified`           timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`            timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `tenant_id`, `user_id`, `role`, `status`, `security_code`, `modified`, `created`)
VALUES ('1b3ddfda-2dae-4f3c-a2c3-eec5ce719457', '6f84587a-1d88-11ec-8296-d2ea1fd9e5ee',
        '84f04b10-1d88-11ec-8296-d2ea1fd9e5ee', 'A', 'ACTIVE', null, '2020-08-19 18:12:53', '2020-08-19 18:10:17'),
       ('b043e329-f489-4ab2-8c49-183340312e92', '6f84587a-1d88-11ec-8296-d2ea1fd9e5ee',
        '84f04c1e-1d88-11ec-8296-d2ea1fd9e5ee', 'U', 'ACTIVE', null, '2020-08-19 18:15:06', '2020-08-19 18:13:58'),
       ('3597a073-fd9c-4b13-917a-8f947249dedc', '6f845a78-1d88-11ec-8296-d2ea1fd9e5ee',
        '84f04c1e-1d88-11ec-8296-d2ea1fd9e5ee', 'U', 'PENDING', '9bed23f4-f544-11eb-9a03-0242ac130003',
        '2020-08-19 18:15:06', '2020-08-19 18:13:58'),
       ('40aa7fba-1810-4c3c-91f4-84063c60bc92', '6f84587a-1d88-11ec-8296-d2ea1fd9e5ee',
        '20f18694-280b-4fa6-84f5-0f9d86f1aeea', 'U', 'ACTIVE', null,
        '2020-08-19 18:15:06', '2020-08-19 18:13:58'),
       ('d00f2bd6-e45d-405e-924a-cc243e5b409e', '6f845a78-1d88-11ec-8296-d2ea1fd9e5ee',
        '20f18694-280b-4fa6-84f5-0f9d86f1aeea', 'U', 'ACTIVE', null,
        '2020-08-19 18:15:06', '2020-08-19 18:13:58'),
       ('f64e7c18-2b33-11ee-be56-0242ac120002', '6f84587a-1d88-11ec-8296-d2ea1fd9e5ee',
        'b1959871-198b-49e6-bb2e-171c9486fc8c', 'U', 'ACTIVE', null,
        '2020-08-19 18:15:06', '2020-08-19 18:13:58');

-- --------------------------------------------------------

--
-- Table structure for table `behavior_articles`
--

CREATE TABLE `behavior_articles`
(
    `id`        int(11) UNSIGNED NOT NULL,
    `tenant_id` int(11) UNSIGNED NOT NULL,
    `title`     varchar(255)     NOT NULL DEFAULT 'U',
    `modified`  timestamp        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`   timestamp        NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `behavior_tenants`
--

CREATE TABLE `behavior_tenants`
(
    `id`           int(11) UNSIGNED NOT NULL,
    `name`         varchar(100)     NOT NULL,
    `phone`        varchar(20)               DEFAULT NULL,
    `sync_pending` datetime                  DEFAULT NULL,
    `is_enabled`   tinyint(1)       NOT NULL DEFAULT '0',
    `modified`     timestamp        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`      timestamp        NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE `configurations`
(
    `id`                    char(36)  NOT NULL,
    `tenant_id`             char(36)  NOT NULL,
    `configuration_type_id` char(36)  NOT NULL,
    `value`                 longtext,
    `modified`              timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`               timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `configuration_options`
--

CREATE TABLE `configuration_options`
(
    `id`                    char(36)     NOT NULL,
    `configuration_type_id` char(36)     NOT NULL,
    `name`                  varchar(100) NOT NULL,
    `value`                 longtext,
    `sort_order`            int(11)      NOT NULL,
    `modified`              timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`               timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Dumping data for table `configuration_options`
--

INSERT INTO `configuration_options` (`id`, `configuration_type_id`, `name`, `value`, `sort_order`, `modified`,
                                     `created`)
VALUES ('7587ddfb-4c58-40ce-a5c6-7767de2434af', '4f6788eb-d218-4ea9-8a25-0a9420ac6db6', 'Test Option', 'Value 1', 1,
        '2021-09-24 19:52:09', '2021-09-24 19:52:09');

-- --------------------------------------------------------

--
-- Table structure for table `configuration_types`
--

CREATE TABLE `configuration_types`
(
    `id`            char(36)     NOT NULL,
    `scope`         char(1)      NOT NULL,
    `name`          varchar(100) NOT NULL,
    `description`   varchar(255)          DEFAULT NULL,
    `data_type`     char(10)     NOT NULL,
    `default_value` longtext,
    `tab`           char(10)     NOT NULL,
    `is_editable`   tinyint(1)   NOT NULL DEFAULT '0',
    `is_enabled`    tinyint(1)   NOT NULL DEFAULT '0',
    `is_public`     tinyint(1)   NOT NULL DEFAULT '1',
    `modified`      timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`       timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Dumping data for table `configuration_types`
--

INSERT INTO `configuration_types` (`id`, `scope`, `name`, `description`, `data_type`, `default_value`, `tab`,
                                   `is_editable`, `is_enabled`, `is_public`, `modified`, `created`)
VALUES ('4f6788eb-d218-4ea9-8a25-0a9420ac6db6', 'U', 'Test Non-Admin Configuration', 'Test Non-Admin', 'text', NULL,
        'site',
        1, 1, 1, '2021-09-24 19:52:09', '2021-09-24 19:52:09'),
       ('b1e85392-4fd9-4734-bbe4-db3137adb9ad', 'U', 'Test Admin Configuration', 'Test Configuration Type', 'text',
        NULL,
        'admin', 1, 1, 1, '2021-09-24 19:52:09', '2021-09-24 19:52:09');

-- --------------------------------------------------------

--
-- Table structure for table `constants`
--

CREATE TABLE `constants`
(
    `code`     smallint(5) UNSIGNED NOT NULL,
    `scope`    varchar(255)         NOT NULL,
    `property` varchar(255)         NOT NULL,
    `text`     varchar(255)         NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Dumping data for table `constants`
--

INSERT INTO `constants` (`code`, `scope`, `property`, `text`)
VALUES (100, 'Method', 'SMS', 'SMS'),
       (101, 'Method', 'VOICE', 'Voice'),
       (200, 'Direction', 'INBOUND', 'Inbound'),
       (201, 'Direction', 'OUTBOUND', 'Outbound'),
       (300, 'SubscriptionStatus', 'SUBSCRIBED', 'Subscribed'),
       (301, 'SubscriptionStatus', 'UNSUBSCRIBED', 'Unsubscribed'),
       (302, 'SubscriptionStatus', 'INVALID_PHONE', 'Invalid Phone'),
       (303, 'SubscriptionStatus', 'FAILED', 'Failed'),
       (400, 'BroadcastStatus', 'DRAFT', 'Draft'),
       (401, 'BroadcastStatus', 'SCHEDULED', 'Scheduled'),
       (402, 'BroadcastStatus', 'BROADCASTING', 'Broadcasting'),
       (403, 'BroadcastStatus', 'SENT', 'Sent'),
       (404, 'BroadcastStatus', 'SENT_WITH_ERRORS', 'Sent With Errors'),
       (500, 'SystemAction', 'NONE', 'None'),
       (501, 'SystemAction', 'SUBSCRIBE', 'Subscribe'),
       (502, 'SystemAction', 'UNSUBSCRIBE', 'Unsubscribe'),
       (600, 'OutboundStatus', 'SUBSCRIBED', 'Subscribed'),
       (601, 'OutboundStatus', 'UNSUBSCRIBED', 'Unsubscribed'),
       (602, 'OutboundStatus', 'INVALID_PHONE', 'Invalid Phone'),
       (603, 'OutboundStatus', 'QUEUED', 'Queued'),
       (604, 'OutboundStatus', 'SENDING', 'Sending'),
       (605, 'OutboundStatus', 'SENT', 'Sent'),
       (606, 'OutboundStatus', 'DELIVERED', 'Delivered'),
       (607, 'OutboundStatus', 'UNDELIVERED', 'Undelivered'),
       (608, 'OutboundStatus', 'FAILED', 'Failed'),
       (609, 'OutboundStatus', 'UNKNOWN', 'Unknown'),
       (700, 'InboundStatus', 'RECEIVED', 'Received');

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE `tenants`
(
    `id`           char(36)     NOT NULL,
    `name`         varchar(100) NOT NULL,
    `sync_pending` datetime              DEFAULT NULL,
    `is_enabled`   tinyint(1)   NOT NULL DEFAULT '0',
    `modified`     timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`      timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

alter table `tenants`
    add `domain_name` varchar(50)  NOT NULL after `name`,
    add `id_public`   varchar(8)   NOT NULL after `domain_name`,
    add `state`       varchar(2) DEFAULT NULL after `id_public`,
    add `type`        char(1)      NOT NULL DEFAULT 'S' after `state`,
    add `data_source` varchar(255) NOT NULL DEFAULT 'jpm' after `type`,
    add `layout`      varchar(50)  NOT NULL DEFAULT 'default' after `data_source`;

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`id`, `name`, `domain_name`, `id_public`, `state`, `type`, `data_source`, `layout`,
                       `sync_pending`, `is_enabled`, `modified`, `created`)
values ('6f84587a-1d88-11ec-8296-d2ea1fd9e5ee', 'JPM Strategies', '1.com', '', null, 'S', 'jpm', 'default',
        '2023-07-26 15:14:36', 1, '2023-07-26 15:14:36', '2020-08-19 00:23:49'),
       ('6f845a78-1d88-11ec-8296-d2ea1fd9e5ee', 'YCT', '2.com', '', null, 'S', 'jpm', 'default', null, 1,
        '2020-08-19 00:35:23', '2020-08-19 00:35:23'),
       ('6f845abe-1d88-11ec-8296-d2ea1fd9e5ee', 'Extra Tenant', '3.com', '', null, 'S', 'jpm', 'default', null, 1,
        '2021-08-18 23:33:12', '2021-08-18 23:33:12'),
       ('6f845afa-1d88-11ec-8296-d2ea1fd9e5ee', 'Broadcasts Tenant 1', '4.com', '', null, 'S', 'jpm', 'default', null,
        1, '2021-08-18 23:33:16', '2021-08-18 23:33:16'),
       ('6f845b36-1d88-11ec-8296-d2ea1fd9e5ee', 'Broadcast Assigner Tenant', '5.com', '', null, 'S', 'jpm', 'default',
        null, 1, '2021-08-18 23:33:16', '2021-08-18 23:33:16'),
       ('6f845b72-1d88-11ec-8296-d2ea1fd9e5ee', 'Broadcaster Tenant', '6.com', '', null, 'S', 'jpm', 'default', null, 1,
        '2021-08-18 23:33:16', '2021-08-18 23:33:16'),
       ('6f845ba4-1d88-11ec-8296-d2ea1fd9e5ee', 'Message Parser Tenant', '7.com', '', null, 'S', 'jpm', 'default', null,
        1, '2021-08-18 23:33:16', '2021-08-18 23:33:16'),
       ('6f845be0-1d88-11ec-8296-d2ea1fd9e5ee', 'Broadcasts Tenant 2', '8.com', '', null, 'S', 'jpm', 'default', null,
        1, '2021-08-18 23:33:16', '2021-08-18 23:33:16'),
       ('6f845c12-1d88-11ec-8296-d2ea1fd9e5ee', 'BroadcastPoller Tenant', '9.com', '', null, 'S', 'jpm', 'default',
        null, 1, '2021-08-18 23:33:16', '2021-08-18 23:33:16'),
       ('6f845a14-1d88-11ec-8296-d2ea1fd9e5ee', 'SMS Receive Tenant', '10.com', '', null, 'S', 'jpm', 'default', null,
        1, '2021-08-18 23:33:17', '2021-08-18 23:33:17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users`
(
    `id`            char(36)     NOT NULL,
    `email`         varchar(100) NOT NULL,
    `password`      varchar(64)  NOT NULL,
    `security_code` varchar(255)          DEFAULT NULL,
    `is_enabled`    tinyint(1)   NOT NULL DEFAULT '0',
    `is_super`      tinyint(1)   NOT NULL DEFAULT '0',
    `modified`      timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`       timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Dumping data for table `users`
--

-- password = jpmtesting
INSERT INTO `users` (`id`, `email`, `password`, `security_code`, `is_enabled`, `is_super`, `modified`, `created`)
VALUES ('84f04b10-1d88-11ec-8296-d2ea1fd9e5ee', 'el.jefe42@gmail.com',
        '$2y$10$/RRsqdESTaVg.bUGLSVp9.JePXkRodDayEnWlNXYUeDW.nt6jQbvy', NULL, 1, 0,
        '2020-08-19 18:09:28', '2015-05-30 20:15:34'),
       ('84f04bec-1d88-11ec-8296-d2ea1fd9e5ee', 'jeff@jpmstrategies.com',
        '$2y$10$/RRsqdESTaVg.bUGLSVp9.JePXkRodDayEnWlNXYUeDW.nt6jQbvy',
        '101782f0-c0fe-4d1f-b80a-26fec546a80c', 1, 1, '2020-08-19 00:46:12', '2018-10-01 00:18:49'),
       ('84f04c1e-1d88-11ec-8296-d2ea1fd9e5ee', 'jeff.morris@outlook.com',
        '$2y$10$/RRsqdESTaVg.bUGLSVp9.JePXkRodDayEnWlNXYUeDW.nt6jQbvy', NULL, 1, 0,
        '2020-08-19 18:12:33', '2020-08-19 18:10:17'),
       ('84f04c46-1d88-11ec-8296-d2ea1fd9e5ee', 'dev@jpmstrategies.com',
        '$2y$10$/RRsqdESTaVg.bUGLSVp9.JePXkRodDayEnWlNXYUeDW.nt6jQbvy', NULL, 0, 0,
        '2020-08-19 18:12:33', '2020-08-19 18:10:17'),
       ('84f04c64-1d88-11ec-8296-d2ea1fd9e5ee', 'test@jpmstrategies.com',
        '$2y$10$/RRsqdESTaVg.bUGLSVp9.JePXkRodDayEnWlNXYUeDW.nt6jQbvy', NULL, 0, 0,
        '2020-08-19 18:12:33', '2020-08-19 18:10:17'),
       ('20f18694-280b-4fa6-84f5-0f9d86f1aeea', 'multi-tenant@jpmstrategies.com',
        '$2y$10$/RRsqdESTaVg.bUGLSVp9.JePXkRodDayEnWlNXYUeDW.nt6jQbvy', NULL, 1, 0,
        '2020-08-19 18:12:33', '2020-08-19 18:10:17'),
       ('b1959871-198b-49e6-bb2e-171c9486fc8c', 'active-user@jpmstrategies.com',
        '$2y$10$/RRsqdESTaVg.bUGLSVp9.JePXkRodDayEnWlNXYUeDW.nt6jQbvy', NULL, 1, 0,
        '2020-08-19 18:12:33', '2020-08-19 18:10:17');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs`
(
    `id`       char(36)    NOT NULL,
    `type`     varchar(50) NOT NULL,
    `message`  mediumtext  NOT NULL,
    `created`  datetime     DEFAULT NULL,
    `ip`       varchar(50)  DEFAULT NULL,
    `hostname` varchar(50)  DEFAULT NULL,
    `uri`      varchar(255) DEFAULT NULL,
    `refer`    varchar(255) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE `phinxlog`
(
    `version`        bigint(20) NOT NULL,
    `migration_name` varchar(100)        DEFAULT NULL,
    `start_time`     timestamp  NULL     DEFAULT NULL,
    `end_time`       timestamp  NULL     DEFAULT NULL,
    `breakpoint`     tinyint(1) NOT NULL DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions`
(
    `id`       char(40) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
    `created`  datetime         DEFAULT CURRENT_TIMESTAMP,
    `modified` datetime         DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `data`     blob,
    `expires`  int(10) UNSIGNED DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_accounts` (`tenant_id`, `user_id`),
    ADD KEY `XFK2_accounts` (`user_id`);

--
-- Indexes for table `behavior_articles`
--
ALTER TABLE `behavior_articles`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `behavior_tenants`
--
ALTER TABLE `behavior_tenants`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_tenants` (`name`),
    ADD UNIQUE KEY `XUN2_tenants` (`phone`);

--
-- Indexes for table `configurations`
--
ALTER TABLE `configurations`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_configurations` (`tenant_id`, `configuration_type_id`),
    ADD KEY `XFK1_configuration` (`configuration_type_id`);

--
-- Indexes for table `configuration_options`
--
ALTER TABLE `configuration_options`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_configuration_options` (`configuration_type_id`, `name`);

--
-- Indexes for table `configuration_types`
--
ALTER TABLE `configuration_types`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_configuration_types` (`scope`, `name`);

--
-- Indexes for table `constants`
--
ALTER TABLE `constants`
    ADD PRIMARY KEY (`code`),
    ADD UNIQUE KEY `XUN1_constants` (`scope`, `property`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
    ADD PRIMARY KEY (`id`),
    ADD KEY `type` (`type`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenants`
--
ALTER TABLE `tenants`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_tenants` (`name`),
    ADD UNIQUE KEY `XUN2_tenants` (`domain_name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_users` (`email`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
    ADD CONSTRAINT `XFK1_accounts` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK2_accounts` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `configurations`
--
ALTER TABLE `configurations`
    ADD CONSTRAINT `XFK1_configuration` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK2_configuration` FOREIGN KEY (`configuration_type_id`) REFERENCES `configuration_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `configuration_options`
--
ALTER TABLE `configuration_options`
    ADD CONSTRAINT `XFK1_configuration_options` FOREIGN KEY (`configuration_type_id`) REFERENCES `configuration_types` (`id`);

-- --------------------------------------------------------

--
-- Populate configurations for all tenant
--
INSERT INTO `configurations` (id, configuration_type_id, tenant_id, value, created, modified)
SELECT uuid()            as id,
       ct.id             as configuration_type_id,
       t.id              as tenant_id,
       'ORIGINAL-RECORD' as value,
       NOW()             as created,
       NOW()             as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
            ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;

-- --------------------------------------------------------

-- 2022-11-17-user-details.sql
alter table configuration_types
    add column `is_required`      tinyint(1)   not null default 0 after `is_public`,
    add column `required_message` varchar(255) null after `is_required`,
    add column `required_regex`   varchar(255) null after `required_message`,
    add column `field_mask`       varchar(255) null after `required_regex`,
    add column `sort_order`       int          not null after `required_regex`;

alter table configuration_types
    modify column `scope` varchar(255) null,
    modify column `tab` varchar(255) null;

alter table users
    add column `user_details` json null after `security_code`;

-- 2023-03-02-cakephp-queue.sql
create table queue_phinxlog
(
    version        bigint               not null
        primary key,
    migration_name varchar(100)         null,
    start_time     timestamp            null,
    end_time       timestamp            null,
    breakpoint     tinyint(1) default 0 not null
)
    collate = utf8mb4_unicode_ci;

create table queue_processes
(
    id        int auto_increment
        primary key,
    pid       varchar(40)          not null,
    created   datetime             not null,
    modified  datetime             not null,
    terminate tinyint(1) default 0 not null,
    server    varchar(90)          null,
    workerkey varchar(45)          not null,
    constraint pid
        unique (pid, server),
    constraint workerkey
        unique (workerkey)
)
    collate = utf8mb4_unicode_ci;

create table queued_jobs
(
    id              int auto_increment
        primary key,
    job_task        varchar(90)   not null,
    data            text          null,
    job_group       varchar(255)  null,
    reference       varchar(255)  null,
    created         datetime      not null,
    notbefore       datetime      null,
    fetched         datetime      null,
    completed       datetime      null,
    progress        float         null,
    failed          int default 0 not null,
    failure_message text          null,
    workerkey       varchar(45)   null,
    status          varchar(255)  null,
    priority        int default 5 not null
)
    collate = utf8mb4_unicode_ci;

-- 2023-03-04-user-signup-improvements.sql
create table signup_domains
(
    id       int unsigned auto_increment
        primary key,
    name     varchar(255)                        not null,
    regex    varchar(255)                        not null,
    modified timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
    created  timestamp default CURRENT_TIMESTAMP not null,
    constraint XUK1_signup_domains
        unique (regex)
) charset = utf8mb3;

-- ========================================================
-- Application Specific Schema
-- ========================================================

-- 1789 tables
DROP TABLE IF EXISTS `ratings_priorities`;

DROP TABLE IF EXISTS `records`;

DROP TABLE IF EXISTS `ratings`;

DROP TABLE IF EXISTS `actions`;

DROP TABLE IF EXISTS `action_types`;

DROP TABLE IF EXISTS `addresses`;

DROP TABLE IF EXISTS `address_types`;

DROP TABLE IF EXISTS `scores`;

DROP TABLE IF EXISTS `legislators`;

DROP TABLE IF EXISTS `titles`;

DROP TABLE IF EXISTS `chambers`;

DROP TABLE IF EXISTS `people_tags`;

DROP TABLE IF EXISTS `people`;

DROP TABLE IF EXISTS `grades`;

DROP TABLE IF EXISTS `organizations`;

DROP TABLE IF EXISTS `legislative_sessions`;

DROP TABLE IF EXISTS `parties`;

DROP TABLE IF EXISTS `positions`;

DROP TABLE IF EXISTS `tenants_bills`;

DROP TABLE IF EXISTS `priorities`;

DROP TABLE IF EXISTS `tags`;

DROP TABLE IF EXISTS `rating_fields`;

DROP TABLE IF EXISTS `rating_types`;

-- Legiscan tables
DROP TABLE IF EXISTS `legiscan_actions`;

DROP TABLE IF EXISTS `legiscan_bill_subjects`;

DROP TABLE IF EXISTS `legiscan_bill_vote_details`;

DROP TABLE IF EXISTS `legiscan_bill_votes`;

DROP TABLE IF EXISTS `legiscan_bills`;

DROP TABLE IF EXISTS `legiscan_chambers`;

DROP TABLE IF EXISTS `legiscan_parties`;

DROP TABLE IF EXISTS `legiscan_sessions`;

DROP TABLE IF EXISTS `legiscan_subjects`;

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE `actions`
(
    `id`                 char(36)                         NOT NULL,
    `tenant_id`          char(36)                         NOT NULL,
    `rating_type_id`     char(36)                         NOT NULL,
    `action_type_id`     char(36)            DEFAULT NULL,
    `name`               varchar(25) CHARACTER SET latin1 NOT NULL,
    `description`        varchar(45)                      NOT NULL,
    `denom_value`        decimal(5, 2)       DEFAULT NULL,
    `multiplier_support` decimal(5, 2)       DEFAULT NULL,
    `multiplier_oppose`  decimal(5, 2)       DEFAULT NULL,
    `legiscan_action_id` tinyint(3) UNSIGNED DEFAULT NULL,
    `created`            datetime            DEFAULT NULL,
    `modified`           datetime            DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `action_types`
--

CREATE TABLE `action_types`
(
    `id`              char(36) NOT NULL,
    `tenant_id`       char(36) NOT NULL,
    `name`            char(50) NOT NULL,
    `icon_text`       varchar(10)       DEFAULT NULL,
    `export_text`     varchar(5)        DEFAULT NULL,
    `css_color`       char(7)  NOT NULL,
    `sort_order`      int(11)  NOT NULL,
    `evaluation_code` char(1)  NOT NULL DEFAULT 'N',
    `score_type`      char(1)  NOT NULL DEFAULT 'Z',
    `created`         datetime          DEFAULT NULL,
    `modified`        datetime          DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses`
(
    `id`              char(36) NOT NULL,
    `tenant_id`       char(36) NOT NULL,
    `person_id`       char(36) NOT NULL,
    `address_type_id` char(36) NOT NULL,
    `capitol_address` varchar(100) DEFAULT NULL,
    `full_address`    text,
    `address_line_1`  varchar(50)  DEFAULT NULL,
    `address_line_2`  varchar(50)  DEFAULT NULL,
    `city`            varchar(64)  DEFAULT NULL,
    `state`           char(2)      DEFAULT NULL,
    `zip`             varchar(10)  DEFAULT NULL,
    `phone`           varchar(15)  DEFAULT NULL,
    `fax`             varchar(15)  DEFAULT NULL,
    `created`         datetime     DEFAULT NULL,
    `modified`        datetime     DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `address_types`
--

CREATE TABLE `address_types`
(
    `id`        char(36) NOT NULL,
    `tenant_id` char(36) NOT NULL,
    `name`      char(50) NOT NULL,
    `created`   datetime DEFAULT NULL,
    `modified`  datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `chambers`
--

CREATE TABLE `chambers`
(
    `id`                         char(36)            NOT NULL,
    `tenant_id`                  char(36)            NOT NULL,
    `type`                       char(1)             NOT NULL,
    `name`                       varchar(100)        NOT NULL,
    `title_short`                varchar(100)                 DEFAULT NULL,
    `title_long`                 varchar(100)                 DEFAULT NULL,
    `presiding_legislator_short` varchar(100)                 DEFAULT NULL,
    `presiding_legislator_long`  varchar(100)                 DEFAULT NULL,
    `slug`                       varchar(255)        NOT NULL,
    `legiscan_chamber_id`        tinyint(3) UNSIGNED          DEFAULT NULL,
    `is_enabled`                 tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
    `is_default`                 tinyint(1)          NOT NULL DEFAULT '0',
    `sort_order`                 int(11)                      DEFAULT NULL,
    `created`                    datetime                     DEFAULT NULL,
    `modified`                   datetime                     DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades`
(
    `id`          char(36)    NOT NULL,
    `tenant_id`   char(36)    NOT NULL,
    `name`        char(50)    NOT NULL,
    `css_color`   char(7)     NOT NULL,
    `sort_order`  int(11)     NOT NULL,
    `lower_range` smallint(6) NOT NULL,
    `upper_range` smallint(6) NOT NULL,
    `created`     datetime DEFAULT NULL,
    `modified`    datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `legiscan_actions`
-- (See below for the actual view)
--
CREATE TABLE `legiscan_actions`
(
    `id`          tinyint(3) unsigned,
    `description` varchar(24)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `legiscan_bills`
-- (See below for the actual view)
--
CREATE TABLE `legiscan_bills`
(
    `id`                                    mediumint(8) unsigned,
    `state`                                 char(2),
    `bill_number`                           varchar(10),
    `legiscan_status_id`                    tinyint(3) unsigned,
    `status_desc`                           varchar(24),
    `status_date`                           date,
    `title`                                 text,
    `description`                           text,
    `legiscan_bill_type_id`                 tinyint(3) unsigned,
    `bill_type_name`                        varchar(64),
    `bill_type_abbr`                        varchar(4),
    `legiscan_chamber_id`                   tinyint(3) unsigned,
    `body_abbr`                             char(1),
    `body_short`                            varchar(16),
    `body_name`                             varchar(128),
    `legiscan_current_chamber_id`           tinyint(3) unsigned,
    `current_body_abbr`                     char(1),
    `current_body_short`                    varchar(16),
    `current_body_name`                     varchar(128),
    `legiscan_pending_committee_id`         smallint(5) unsigned,
    `legiscan_pending_committee_chamber_id` tinyint(3) unsigned,
    `pending_committee_body_abbr`           char(1),
    `pending_committee_body_short`          varchar(16),
    `pending_committee_body_name`           varchar(128),
    `pending_committee_name`                varchar(128),
    `legiscan_url`                          varchar(255),
    `state_url`                             varchar(255),
    `change_hash`                           char(32),
    `created`                               timestamp,
    `updated`                               timestamp,
    `legiscan_state_id`                     tinyint(3) unsigned,
    `state_name`                            varchar(64),
    `legiscan_session_id`                   smallint(5) unsigned,
    `session_year_start`                    smallint(5) unsigned,
    `session_year_end`                      smallint(5) unsigned,
    `session_prefile`                       tinyint(3) unsigned,
    `session_sine_die`                      tinyint(3) unsigned,
    `session_prior`                         tinyint(3) unsigned,
    `session_special`                       tinyint(3) unsigned,
    `session_tag`                           varchar(32),
    `session_title`                         varchar(64),
    `session_name`                          varchar(64),
    `has_bill_vote_details`                 int(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `legiscan_bill_subjects`
-- (See below for the actual view)
--
CREATE TABLE `legiscan_bill_subjects`
(
    `state`                         char(2),
    `id`                            varchar(17),
    `legiscan_subject_id`           mediumint(8) unsigned,
    `legiscan_bill_id`              mediumint(8) unsigned,
    `bill_number`                   varchar(10),
    `subject_name`                  varchar(128),
    `legiscan_state_id`             tinyint(3) unsigned,
    `state_name`                    varchar(64),
    `legiscan_session_id`           smallint(5) unsigned,
    `legiscan_chamber_id`           tinyint(3) unsigned,
    `legiscan_current_chamber_id`   tinyint(3) unsigned,
    `legsican_bill_type_id`         tinyint(3) unsigned,
    `legsican_status_id`            tinyint(3) unsigned,
    `legsican_pending_committee_id` smallint(5) unsigned
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `legiscan_bill_votes`
-- (See below for the actual view)
--
CREATE TABLE `legiscan_bill_votes`
(
    `id`                            mediumint(8) unsigned,
    `state`                         char(2),
    `legiscan_bill_id`              mediumint(8) unsigned,
    `bill_number`                   varchar(10),
    `roll_call_date`                date,
    `roll_call_desc`                varchar(255),
    `legiscan_bill_vote_chamber_id` tinyint(3) unsigned,
    `roll_call_body_abbr`           char(1),
    `roll_call_body_short`          varchar(16),
    `roll_call_body_name`           varchar(128),
    `yea`                           smallint(5) unsigned,
    `nay`                           smallint(5) unsigned,
    `nv`                            smallint(5) unsigned,
    `absent`                        smallint(5) unsigned,
    `total`                         smallint(5) unsigned,
    `passed`                        tinyint(3) unsigned,
    `legiscan_url`                  varchar(255),
    `state_url`                     varchar(255),
    `legiscan_state_id`             tinyint(3) unsigned,
    `state_name`                    varchar(64),
    `legiscan_session_id`           smallint(5) unsigned,
    `legiscan_chamber_id`           tinyint(3) unsigned,
    `legiscan_current_chamber_id`   tinyint(3) unsigned,
    `legiscan_bill_type_id`         tinyint(3) unsigned,
    `legiscan_status_id`            tinyint(3) unsigned,
    `legiscan_pending_committee_id` smallint(5) unsigned,
    `has_bill_vote_details`         int(1),
    `created`                       timestamp,
    `updated`                       timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `legiscan_bill_vote_details`
-- (See below for the actual view)
--
CREATE TABLE `legiscan_bill_vote_details`
(
    `id`                            varchar(14),
    `state`                         char(2),
    `legiscan_bill_id`              mediumint(8) unsigned,
    `legiscan_bill_vote_id`         mediumint(8) unsigned,
    `legiscan_bill_vote_chamber_id` tinyint(3) unsigned,
    `legiscan_people_id`            smallint(5) unsigned,
    `bill_number`                   varchar(10),
    `legiscan_vote_id`              tinyint(3) unsigned,
    `vote_desc`                     varchar(24),
    `legiscan_party_id`             tinyint(3) unsigned,
    `party_abbr`                    char(1),
    `legiscan_role_id`              tinyint(3) unsigned,
    `role_abbr`                     char(3),
    `role_name`                     varchar(64),
    `name`                          varchar(128),
    `first_name`                    varchar(32),
    `middle_name`                   varchar(32),
    `last_name`                     varchar(32),
    `suffix`                        varchar(32),
    `nickname`                      varchar(32),
    `ballotpedia`                   varchar(64),
    `followthemoney_eid`            bigint(20) unsigned,
    `votesmart_id`                  mediumint(8) unsigned,
    `opensecrets_id`                char(9),
    `knowwho_pid`                   mediumint(8) unsigned,
    `person_hash`                   char(8),
    `person_district`               varchar(9),
    `legiscan_people_chamber_id`    tinyint(3) unsigned,
    `legiscan_state_id`             tinyint(3) unsigned,
    `state_name`                    varchar(64),
    `legiscan_session_id`           smallint(5) unsigned,
    `legiscan_chamber_id`           tinyint(3) unsigned,
    `legiscan_current_chamber_id`   tinyint(3) unsigned,
    `legiscan_bill_type_id`         tinyint(3) unsigned,
    `legiscan_status_id`            tinyint(3) unsigned,
    `legiscan_pending_committee_id` smallint(5) unsigned
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `legiscan_chambers`
-- (See below for the actual view)
--
CREATE TABLE `legiscan_chambers`
(
    `state`      char(2),
    `id`         tinyint(3) unsigned,
    `body_abbr`  char(1),
    `body_short` varchar(16),
    `body_name`  varchar(128),
    `role_id`    tinyint(3) unsigned,
    `role_abbr`  varchar(3),
    `role_name`  varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `legiscan_parties`
-- (See below for the actual view)
--
CREATE TABLE `legiscan_parties`
(
    `state`       char(2),
    `id`          tinyint(3) unsigned,
    `party_abbr`  char(1),
    `party_short` char(3),
    `party_name`  varchar(32)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `legiscan_sessions`
-- (See below for the actual view)
--
CREATE TABLE `legiscan_sessions`
(
    `state`         char(2),
    `id`            smallint(5) unsigned,
    `year_start`    smallint(5) unsigned,
    `year_end`      smallint(5) unsigned,
    `prefile`       tinyint(3) unsigned,
    `sine_die`      tinyint(3) unsigned,
    `prior`         tinyint(3) unsigned,
    `special`       tinyint(3) unsigned,
    `session_name`  varchar(64),
    `session_title` varchar(64),
    `session_tag`   varchar(32),
    `import_date`   date,
    `import_hash`   char(32)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `legiscan_subjects`
-- (See below for the actual view)
--
CREATE TABLE `legiscan_subjects`
(
    `state`        char(2),
    `id`           mediumint(8) unsigned,
    `subject_name` varchar(128)
);

-- --------------------------------------------------------

--
-- Table structure for table `legislative_sessions`
--

CREATE TABLE `legislative_sessions`
(
    `id`                     char(36)            NOT NULL,
    `tenant_id`              char(36)            NOT NULL,
    `name`                   varchar(100)        NOT NULL,
    `year`                   char(4)             NOT NULL,
    `legislature_number`     smallint(5)         NOT NULL,
    `session_type`           char(1)             NOT NULL DEFAULT 'R',
    `special_session_number` smallint(5)         NOT NULL DEFAULT '0',
    `speaker_journal_name`   varchar(25)                  DEFAULT NULL,
    `lt_gov_journal_name`    varchar(25)                  DEFAULT NULL,
    `slug`                   varchar(255)        NOT NULL,
    `legiscan_session_id`    smallint(5) UNSIGNED         DEFAULT NULL,
    `state_pk`               varchar(45)                  DEFAULT NULL,
    `os_pk`                  smallint(5) UNSIGNED         DEFAULT NULL,
    `created`                datetime                     DEFAULT NULL,
    `modified`               datetime                     DEFAULT NULL,
    `is_enabled`             tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
    `show_scores`            tinyint(1)          NOT NULL DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `legislators`
--

CREATE TABLE `legislators`
(
    `id`                     char(36)            NOT NULL,
    `tenant_id`              char(36)            NOT NULL,
    `legislative_session_id` char(36)            NOT NULL,
    `person_id`              char(36)            NOT NULL,
    `chamber_id`             char(36)            NOT NULL,
    `party_id`               char(36)            NOT NULL,
    `district`               varchar(255)                 DEFAULT NULL,
    `journal_name`           varchar(25)                  DEFAULT NULL,
    `title_id`               char(36)                     DEFAULT NULL,
    `term_starts`            date                         DEFAULT NULL,
    `term_ends`              date                         DEFAULT NULL,
    `grade_id`               char(36)                     DEFAULT NULL,
    `grade_number`           decimal(5, 2)                DEFAULT NULL,
    `override_text`          varchar(10)                  DEFAULT NULL,
    `created`                datetime                     DEFAULT NULL,
    `modified`               datetime                     DEFAULT NULL,
    `is_enabled`             tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations`
(
    `id`                          char(36)     NOT NULL,
    `tenant_id`                   char(36)     NOT NULL,
    `org_tenant_id`               char(36)     DEFAULT NULL,
    `sync_legislative_session_id` char(36)     DEFAULT NULL,
    `name`                        varchar(100) NOT NULL,
    `description`                 text,
    `website`                     varchar(100) DEFAULT NULL,
    `image_url`                   varchar(100) DEFAULT NULL,
    `sort_order`                  int(11)      DEFAULT NULL,
    `created`                     datetime     DEFAULT NULL,
    `modified`                    datetime     DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `parties`
--

CREATE TABLE `parties`
(
    `id`                char(36)            NOT NULL,
    `tenant_id`         char(36)            NOT NULL,
    `name`              char(40)            NOT NULL,
    `abbreviation`      char(15)            NOT NULL,
    `css_color`         char(7)             NOT NULL,
    `legiscan_party_id` tinyint(3) UNSIGNED          DEFAULT NULL,
    `is_enabled`        tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
    `created`           datetime                     DEFAULT NULL,
    `modified`          datetime                     DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people`
(
    `id`                 char(36)            NOT NULL,
    `tenant_id`          char(36)            NOT NULL,
    `full_name`          varchar(100)        NOT NULL,
    `nickname`           varchar(45)                  DEFAULT NULL,
    `salutation`         varchar(3)                   DEFAULT NULL,
    `first_name`         varchar(45)                  DEFAULT NULL,
    `middle_name`        varchar(45)                  DEFAULT NULL,
    `initials`           varchar(10)                  DEFAULT NULL,
    `last_name`          varchar(45)                  DEFAULT NULL,
    `suffix`             char(3)                      DEFAULT NULL,
    `bio`                longtext,
    `committee_list`     longtext,
    `image_url`          varchar(255)                 DEFAULT NULL,
    `ballotpedia_url`    varchar(255)                 DEFAULT NULL,
    `first_elected`      char(4)                      DEFAULT NULL,
    `district_city`      varchar(64)                  DEFAULT NULL,
    `grade_id`           char(36)                     DEFAULT NULL,
    `grade_number`       decimal(5, 2)                DEFAULT NULL,
    `slug`               varchar(255)        NOT NULL,
    `legiscan_people_id` smallint(5) UNSIGNED         DEFAULT NULL,
    `os_pk`              char(10)                     DEFAULT NULL,
    `state_pk`           varchar(45)                  DEFAULT NULL,
    `email`              varchar(100)                 DEFAULT NULL,
    `facebook`           varchar(100)                 DEFAULT NULL,
    `twitter`            varchar(100)                 DEFAULT NULL,
    `created`            datetime                     DEFAULT NULL,
    `modified`           datetime                     DEFAULT NULL,
    `is_enabled`         tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
    `is_retired`         tinyint(1)          NOT NULL DEFAULT '0',
    `ttx_candidate_slug` varchar(255)                 DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `people_tags`
--

CREATE TABLE `people_tags`
(
    `id`        char(36) NOT NULL,
    `person_id` char(36) NOT NULL,
    `tag_id`    char(36) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions`
(
    `id`           char(36) NOT NULL,
    `tenant_id`    char(36) NOT NULL,
    `name`         char(50) NOT NULL,
    `display_code` char(1)  NOT NULL DEFAULT 'N',
    `created`      datetime          DEFAULT NULL,
    `modified`     datetime          DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `priorities`
--

CREATE TABLE `priorities`
(
    `id`          char(36)     NOT NULL,
    `tenant_id`   char(36)     NOT NULL,
    `name`        varchar(255) NOT NULL,
    `description` text,
    `slug`        varchar(255) NOT NULL,
    `sort_order`  int(11)  DEFAULT NULL,
    `created`     datetime DEFAULT NULL,
    `modified`    datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings`
(
    `id`                     char(36)            NOT NULL,
    `tenant_id`              char(36)            NOT NULL,
    `legislative_session_id` char(36)            NOT NULL,
    `rating_type_id`         char(36)            NOT NULL,
    `chamber_id`             char(36)            NOT NULL,
    `name`                   varchar(255)        NOT NULL,
    `description`            longtext,
    `action_id`              char(36)            NOT NULL,
    `position_id`            char(36)            NOT NULL,
    `record_vote`            smallint(5) UNSIGNED         DEFAULT NULL,
    `rating_weight`          decimal(5, 2)                DEFAULT NULL,
    `sort_order`             int(11)                      DEFAULT NULL,
    `slug`                   varchar(255)        NOT NULL,
    `legiscan_bill_vote_id`  mediumint(8) UNSIGNED        DEFAULT NULL,
    `json`                   json                         DEFAULT NULL,
    `created`                datetime                     DEFAULT NULL,
    `modified`               datetime                     DEFAULT NULL,
    `is_enabled`             tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ratings_priorities`
--

CREATE TABLE `ratings_priorities`
(
    `id`          char(36) NOT NULL,
    `rating_id`   char(36) NOT NULL,
    `priority_id` char(36) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rating_fields`
--

CREATE TABLE `rating_fields`
(
    `id`         char(36)     NOT NULL,
    `tenant_id`  char(36)     NOT NULL,
    `name`       varchar(45)  NOT NULL,
    `json_path`  varchar(255) NOT NULL,
    `type`       char(1)      NOT NULL,
    `sort_order` int(11)  DEFAULT NULL,
    `created`    datetime DEFAULT NULL,
    `modified`   datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rating_types`
--

CREATE TABLE `rating_types`
(
    `id`             char(36)    NOT NULL,
    `tenant_id`      char(36)    NOT NULL,
    `name`           varchar(45) NOT NULL,
    `th_detail_name` varchar(20) NOT NULL,
    `th_vote_name`   varchar(20) NOT NULL,
    `method`         char(1)     NOT NULL DEFAULT 'S',
    `sort_order`     int(11)              DEFAULT NULL,
    `created`        datetime             DEFAULT NULL,
    `modified`       datetime             DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `records`
--

CREATE TABLE `records`
(
    `id`            char(36)            NOT NULL,
    `tenant_id`     char(36)            NOT NULL,
    `action_id`     char(36)            NOT NULL,
    `legislator_id` char(36)            NOT NULL,
    `rating_id`     char(36)            NOT NULL,
    `is_chair`      tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
    `has_statement` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
    `created`       datetime                     DEFAULT NULL,
    `modified`      datetime                     DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores`
(
    `id`              char(36) NOT NULL,
    `tenant_id`       char(36) NOT NULL,
    `legislator_id`   char(36) NOT NULL,
    `organization_id` char(36) NOT NULL,
    `display_text`    varchar(10) DEFAULT NULL,
    `created`         datetime    DEFAULT NULL,
    `modified`        datetime    DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags`
(
    `id`          char(36)     NOT NULL,
    `tenant_id`   char(36)     NOT NULL,
    `name`        varchar(255) NOT NULL,
    `description` text,
    `slug`        varchar(255) NOT NULL,
    `css_class`   varchar(255) NOT NULL,
    `sort_order`  int(11)  DEFAULT NULL,
    `created`     datetime DEFAULT NULL,
    `modified`    datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tenants_bills`
--

CREATE TABLE `tenants_bills`
(
    `id`               char(36)              NOT NULL,
    `tenant_id`        char(36)              NOT NULL,
    `legiscan_bill_id` mediumint(8) UNSIGNED NOT NULL,
    `priority_id`      char(36) DEFAULT NULL,
    `status`           varchar(255)          NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles`
(
    `id`          char(36)    NOT NULL,
    `tenant_id`   char(36)    NOT NULL,
    `chamber_id`  char(36)    NOT NULL,
    `branch`      varchar(50) NOT NULL,
    `office`      varchar(50) DEFAULT NULL,
    `title_long`  varchar(50) NOT NULL,
    `title_short` varchar(50) NOT NULL,
    `sort_order`  int(11)     DEFAULT NULL,
    `created`     datetime    DEFAULT NULL,
    `modified`    datetime    DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------
--
-- Indexes for dumped tables
--

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_actions` (`tenant_id`, `rating_type_id`, `name`),
    ADD UNIQUE KEY `XUN2_actions` (`tenant_id`, `rating_type_id`, `description`),
    ADD UNIQUE KEY `XUN3_actions` (`tenant_id`, `legiscan_action_id`),
    ADD KEY `XFK3_actions` (`action_type_id`),
    ADD KEY `FK_rating_type_id` (`rating_type_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `action_types`
--
ALTER TABLE `action_types`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_address_type` (`address_type_id`),
    ADD KEY `FK_person_id` (`person_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `address_types`
--
ALTER TABLE `address_types`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `chambers`
--
ALTER TABLE `chambers`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_chambers` (`tenant_id`, `type`),
    ADD UNIQUE KEY `XUN2_chambers` (`tenant_id`, `slug`),
    ADD UNIQUE KEY `XUN3_chambers` (`tenant_id`, `legiscan_chamber_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `legislative_sessions`
--
ALTER TABLE `legislative_sessions`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN2_legislative_sessions` (`tenant_id`, `slug`),
    ADD UNIQUE KEY `XUN1_legislative_sessions` (`tenant_id`, `year`, `session_type`, `special_session_number`),
    ADD UNIQUE KEY `XUN3_legislative_sessions` (`tenant_id`, `legiscan_session_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `legislators`
--
ALTER TABLE `legislators`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_legislators` (`tenant_id`, `legislative_session_id`, `person_id`, `chamber_id`),
    ADD KEY `XFK7_legislators` (`title_id`),
    ADD KEY `FK_chamber_id` (`chamber_id`),
    ADD KEY `FK_grade_id` (`grade_id`),
    ADD KEY `FK_legislative_session_id` (`legislative_session_id`),
    ADD KEY `FK_party_id` (`party_id`),
    ADD KEY `FK_person_id` (`person_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`),
    ADD KEY `XIE1_legislators` (`tenant_id`, `legislative_session_id`, `chamber_id`, `is_enabled`),
    ADD KEY `XIE2_legislators` (`tenant_id`, `grade_number`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
    ADD PRIMARY KEY (`id`),
    ADD KEY `XFK3_organizations` (`sync_legislative_session_id`),
    ADD KEY `FK_scorecard_id` (`org_tenant_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `parties`
--
ALTER TABLE `parties`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_parties` (`tenant_id`, `name`),
    ADD UNIQUE KEY `XUN2_parties` (`tenant_id`, `legiscan_party_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `UN1_people` (`tenant_id`, `slug`),
    ADD UNIQUE KEY `UN2_people` (`tenant_id`, `state_pk`),
    ADD UNIQUE KEY `XUN3_people` (`tenant_id`, `legiscan_people_id`),
    ADD KEY `FK_grade_id` (`grade_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `people_tags`
--
ALTER TABLE `people_tags`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_people_tags` (`person_id`, `tag_id`),
    ADD KEY `XFK1_people_tags` (`person_id`),
    ADD KEY `XFK2_people_tags` (`tag_id`);

--
-- Indexes for table `phinxlog`
--
ALTER TABLE `phinxlog`
    ADD PRIMARY KEY (`version`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `priorities`
--
ALTER TABLE `priorities`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_priorities` (`tenant_id`, `slug`),
    ADD KEY `XFK1_priorities` (`tenant_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_ratings` (`tenant_id`, `legislative_session_id`, `chamber_id`, `name`),
    ADD UNIQUE KEY `XUN2_ratings` (`tenant_id`, `slug`),
    ADD UNIQUE KEY `XUN3_ratings` (`tenant_id`, `legiscan_bill_vote_id`),
    ADD KEY `FK_action_id` (`action_id`),
    ADD KEY `FK_chamber_id` (`chamber_id`),
    ADD KEY `FK_legislative_session_id` (`legislative_session_id`),
    ADD KEY `FK_position_id` (`position_id`),
    ADD KEY `FK_rating_type_id` (`rating_type_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `ratings_priorities`
--
ALTER TABLE `ratings_priorities`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_ratings_priorities` (`rating_id`, `priority_id`),
    ADD KEY `XFK1_ratings_priorities` (`rating_id`),
    ADD KEY `XFK2_ratings_priorities` (`priority_id`);

--
-- Indexes for table `rating_fields`
--
ALTER TABLE `rating_fields`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_rating_fields` (`tenant_id`, `name`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `rating_types`
--
ALTER TABLE `rating_types`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_rating_types` (`tenant_id`, `name`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `records`
--
ALTER TABLE `records`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_records` (`tenant_id`, `legislator_id`, `rating_id`),
    ADD KEY `FK_action_id` (`action_id`),
    ADD KEY `FK_legislator_id` (`legislator_id`),
    ADD KEY `FK_rating_id` (`rating_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_scores` (`tenant_id`, `legislator_id`, `organization_id`),
    ADD KEY `FK_legislator_id` (`legislator_id`),
    ADD KEY `FK_organization_id` (`organization_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_tags` (`tenant_id`, `slug`),
    ADD KEY `XFK1_tags` (`tenant_id`);

--
-- Indexes for table `tenants_bills`
--
ALTER TABLE `tenants_bills`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_tenants_bills` (`tenant_id`, `legiscan_bill_id`),
    ADD KEY `XFK1_tenants_bills` (`tenant_id`),
    ADD KEY `XFK2_tenants_bills` (`priority_id`),
    ADD KEY `XIE1_tenants_bills` (`legiscan_bill_id`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_titles` (`tenant_id`, `chamber_id`, `branch`, `office`, `title_long`),
    ADD KEY `XFK2_titles` (`chamber_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `actions`
--
ALTER TABLE `actions`
    ADD CONSTRAINT `XFK1_actions` FOREIGN KEY (`rating_type_id`) REFERENCES `rating_types` (`id`),
    ADD CONSTRAINT `XFK2_actions` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`),
    ADD CONSTRAINT `XFK3_actions` FOREIGN KEY (`action_type_id`) REFERENCES `action_types` (`id`);

--
-- Constraints for table `action_types`
--
ALTER TABLE `action_types`
    ADD CONSTRAINT `XFK1_action_types` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
    ADD CONSTRAINT `XFK1_addresses` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK2_addresses` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`),
    ADD CONSTRAINT `XFK3_addresses` FOREIGN KEY (`address_type_id`) REFERENCES `address_types` (`id`);

--
-- Constraints for table `address_types`
--
ALTER TABLE `address_types`
    ADD CONSTRAINT `XFK1_address_types` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`);

--
-- Constraints for table `chambers`
--
ALTER TABLE `chambers`
    ADD CONSTRAINT `XFK1_chambers` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`);

--
-- Constraints for table `grades`
--
ALTER TABLE `grades`
    ADD CONSTRAINT `XFK1_grades` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`);

--
-- Constraints for table `legislative_sessions`
--
ALTER TABLE `legislative_sessions`
    ADD CONSTRAINT `XFK2_legislative_sessions` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`);

--
-- Constraints for table `legislators`
--
ALTER TABLE `legislators`
    ADD CONSTRAINT `XFK1_legislators` FOREIGN KEY (`legislative_session_id`) REFERENCES `legislative_sessions` (`id`),
    ADD CONSTRAINT `XFK2_legislators` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
    ADD CONSTRAINT `XFK3_legislators` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`),
    ADD CONSTRAINT `XFK4_legislators` FOREIGN KEY (`chamber_id`) REFERENCES `chambers` (`id`),
    ADD CONSTRAINT `XFK5_legislators` FOREIGN KEY (`party_id`) REFERENCES `parties` (`id`),
    ADD CONSTRAINT `XFK6_legislators` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`),
    ADD CONSTRAINT `XFK7_legislators` FOREIGN KEY (`title_id`) REFERENCES `titles` (`id`);

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
    ADD CONSTRAINT `XFK1_organizations` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`),
    ADD CONSTRAINT `XFK2_organizations` FOREIGN KEY (`org_tenant_id`) REFERENCES `tenants` (`id`),
    ADD CONSTRAINT `XFK3_organizations` FOREIGN KEY (`sync_legislative_session_id`) REFERENCES `legislative_sessions` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `parties`
--
ALTER TABLE `parties`
    ADD CONSTRAINT `XFK1_parties` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`);

--
-- Constraints for table `people`
--
ALTER TABLE `people`
    ADD CONSTRAINT `XFK1_people` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`),
    ADD CONSTRAINT `XFK2_people` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`);

--
-- Constraints for table `people_tags`
--
ALTER TABLE `people_tags`
    ADD CONSTRAINT `XFK1_people_tags` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`),
    ADD CONSTRAINT `XFK2_people_tags` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`);

--
-- Constraints for table `positions`
--
ALTER TABLE `positions`
    ADD CONSTRAINT `XFK1_positions` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`);

--
-- Constraints for table `priorities`
--
ALTER TABLE `priorities`
    ADD CONSTRAINT `XFK1_priorities` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`);

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
    ADD CONSTRAINT `XFK1_ratings` FOREIGN KEY (`legislative_session_id`) REFERENCES `legislative_sessions` (`id`),
    ADD CONSTRAINT `XFK2_ratings` FOREIGN KEY (`rating_type_id`) REFERENCES `rating_types` (`id`),
    ADD CONSTRAINT `XFK3_ratings` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`),
    ADD CONSTRAINT `XFK4_ratings` FOREIGN KEY (`chamber_id`) REFERENCES `chambers` (`id`),
    ADD CONSTRAINT `XFK5_ratings` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`),
    ADD CONSTRAINT `XFK6_ratings` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`);

--
-- Constraints for table `ratings_priorities`
--
ALTER TABLE `ratings_priorities`
    ADD CONSTRAINT `XFK1_ratings_priorities` FOREIGN KEY (`rating_id`) REFERENCES `ratings` (`id`),
    ADD CONSTRAINT `XFK2_ratings_priorities` FOREIGN KEY (`priority_id`) REFERENCES `priorities` (`id`);

--
-- Constraints for table `rating_fields`
--
ALTER TABLE `rating_fields`
    ADD CONSTRAINT `XFK1_rating_fields` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`);

--
-- Constraints for table `rating_types`
--
ALTER TABLE `rating_types`
    ADD CONSTRAINT `XFK1_rating_types` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`);

--
-- Constraints for table `records`
--
ALTER TABLE `records`
    ADD CONSTRAINT `XFK1_records` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK2_records` FOREIGN KEY (`legislator_id`) REFERENCES `legislators` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK3_records` FOREIGN KEY (`rating_id`) REFERENCES `ratings` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK4_records` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `scores`
--
ALTER TABLE `scores`
    ADD CONSTRAINT `XFK1_scores` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK2_scores` FOREIGN KEY (`legislator_id`) REFERENCES `legislators` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK3_scores` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
    ADD CONSTRAINT `XFK1_tags` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`);

--
-- Constraints for table `tenants_bills`
--
ALTER TABLE `tenants_bills`
    ADD CONSTRAINT `XFK1_tenants_bills` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`),
    ADD CONSTRAINT `XFK2_tenants_bills` FOREIGN KEY (`priority_id`) REFERENCES `priorities` (`id`);

--
-- Constraints for table `titles`
--
ALTER TABLE `titles`
    ADD CONSTRAINT `XFK1_titles` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`),
    ADD CONSTRAINT `XFK2_titles` FOREIGN KEY (`chamber_id`) REFERENCES `chambers` (`id`);
