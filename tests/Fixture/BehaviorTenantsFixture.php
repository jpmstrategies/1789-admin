<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TenantsFixture
 */
class BehaviorTenantsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => [
            'type' => 'string',
            'length' => '36',
            'null' => false,
            'default' => null,
            'comment' => '',
            'precision' => null],
        'name' => [
            'type' => 'string',
            'length' => 100,
            'null' => false,
            'default' => null,
            'collate' => 'utf8_general_ci',
            'comment' => '',
            'precision' => null],
        'sync_pending' => [
            'type' => 'datetime',
            'length' => null,
            'precision' => null,
            'null' => true,
            'default' => null,
            'comment' => ''],
        'is_enabled' => [
            'type' => 'boolean',
            'length' => null,
            'null' => false,
            'default' => '0',
            'comment' => '',
            'precision' => null],
        'modified' => [
            'type' => 'timestamp',
            'length' => null,
            'precision' => null,
            'null' => false,
            'default' => 'CURRENT_TIMESTAMP',
            'comment' => ''],
        'created' => [
            'type' => 'timestamp',
            'length' => null,
            'precision' => null,
            'null' => false,
            'default' => 'CURRENT_TIMESTAMP',
            'comment' => ''],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'XUN1_tenants' => ['type' => 'unique', 'columns' => ['name'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // phpcs:enable

    /**
     * Init method
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => '8780599c-1d97-11ec-9621-0242ac130002',
                'name' => 'Tenant 1',
                'sync_pending' => null,
                'is_enabled' => 1,
                'modified' => 1_627_611_455,
                'created' => 1_627_611_455,
            ],
            [
                'id' => 'a19cf524-1d97-11ec-9621-0242ac130002',
                'name' => 'Tenant 2',
                'sync_pending' => null,
                'is_enabled' => 1,
                'modified' => 1_627_611_455,
                'created' => 1_627_611_455,
            ],
        ];
        parent::init();
    }
}