<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

class BehaviorArticlesFixture extends TestFixture
{
    public $fields = [
        'id' => [
            'type' => 'string',
            'length' => null,
            'unsigned' => true,
            'null' => false,
            'default' => null,
            'comment' => '',
            'autoIncrement' => true,
            'precision' => null],
        'tenant_id' => [
            'type' => 'string',
            'length' => '36',
            'null' => false,
            'default' => null,
            'comment' => '',
            'precision' => null],
        'title' => ['type' => 'string', 'length' => 255],
        'modified' => [
            'type' => 'timestamp',
            'length' => null,
            'precision' => null,
            'null' => false,
            'default' => 'CURRENT_TIMESTAMP',
            'comment' => ''],
        'created' => [
            'type' => 'timestamp',
            'length' => null,
            'precision' => null,
            'null' => false,
            'default' => 'CURRENT_TIMESTAMP',
            'comment' => ''],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
    ];

    public function init(): void
    {
        $this->records = [
            [
                'id' => 'a497fef4-2cc2-11ee-be56-0242ac120002',
                'tenant_id' => '8780599c-1d97-11ec-9621-0242ac130002',
                'title' => 'article 1',
            ],
            [
                'id' => 'a498014c-2cc2-11ee-be56-0242ac120002',
                'tenant_id' => '8780599c-1d97-11ec-9621-0242ac130002',
                'title' => 'article 2',
            ],
            [
                'id' => 'a4980246-2cc2-11ee-be56-0242ac120002',
                'tenant_id' => 'a19cf524-1d97-11ec-9621-0242ac130002',
                'title' => 'article 3',
            ],
        ];
        parent::init();
    }
}