FROM webdevops/php-nginx:8.2-alpine as builder

# Create app directory
# RUN mkdir -p /usr/src/app
WORKDIR /app

# Install app dependencies
RUN mkdir -p ~/.ssh && ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
COPY composer.json composer.lock ./
RUN --mount=type=ssh composer install --no-interaction --no-scripts --no-suggest

FROM webdevops/php-nginx:8.2-alpine as app

## Copy built node modules and binaries without including the toolchain
# RUN mkdir -p /usr/src/app
WORKDIR /app

# util-linux for uuidgen
RUN apk update && apk add --no-cache util-linux

# Copy app source.
COPY . .

# Copy vendor files.
COPY --from=builder /app/vendor vendor/

# Copy docker config.
COPY ./build/php.ini /opt/docker/etc/php/php.ini
COPY ./build/vhost.conf /opt/docker/etc/nginx/vhost.conf
