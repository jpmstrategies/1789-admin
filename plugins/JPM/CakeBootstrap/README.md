# JPM/CakeBootstrap plugin for CakePHP

## Installation

Add the plugin to the "bootstrap" function within `src/Applications.php`:

```
$this->addPlugin('JPM/CakeBootstrap', ['autoload' => false, 'bootstrap' => false, 'routes' => true]);
```

Add the bake configuration to `config/bootstrap.php`

``
Configure::write('Bake.theme', 'JPM/CakeBootstrap');
``

Cake bake as usual:

``
bin/cake bake <type> <table>
``

***

Optionally, you can skip the "config/bootstrap.php" configuration and bake as such:

``
bin/cake bake --theme=JPM/CakeBootstrap <type> <table>
``
