-- use this query to properly update config.php for a database rebuild
select state_name, session_id, year_start, year_end
from (select distinct legiscan_bills.state_name, legiscan_bills.legiscan_session_id
      from ratings
               inner join legiscan_bills on ratings.legiscan_bill_id = legiscan_bills.id
      union
      select distinct legiscan_bill_votes.state_name, legiscan_bill_votes.legiscan_session_id
      from ratings
               inner join legiscan_bill_votes on ratings.legiscan_bill_vote_id = legiscan_bill_votes.id) results
         inner join legiscan.ls_session on results.legiscan_session_id = ls_session.session_id
order by state_name, year_start;
