<?php

namespace App\Event;

use ArrayObject;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\ORM\Entity;
use Cake\ORM\Query;

/**
 * Class LoggedInUserListener
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 */
class TenantListener implements EventListenerInterface
{

    /**
     * Constructor
     *
     */
    public function __construct(
        /**
         * @var $currentTenantId
         */
        protected $currentTenantId
    )
    {
    }

    /**
     * {@inheritDoc}
     */
    public function implementedEvents(): array
    {
        return [
            'Model.beforeFind' => [
                'callable' => 'beforeFind',
                'priority' => -100
            ],
            'Model.beforeSave' => [
                'callable' => 'beforeSave',
                'priority' => -100
            ],
            'Model.beforeDelete' => [
                'callable' => 'beforeDelete',
                'priority' => -100
            ]
        ];
    }

    /**
     * beforeFind callback
     *
     * always filter by tenant_id using current sessions
     *
     * @param Event $event The afterSave event that was fired.
     * @param Query $query The query.
     * @param ArrayObject $options
     * @param $primary
     * @return void
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options, $primary)
    {
        if (empty($options['currentTenantId']) && !empty($this->currentTenantId)) {
            $options['currentTenantId'] = $this->currentTenantId;
        }
    }

    /**
     * Before save listener.
     *
     * @param Event $event The beforeSave event that was fired.
     * @param Entity $entity The entity to be saved.
     * @return void
     */
    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        if (empty($options['currentTenantId']) && !empty($this->currentTenantId)) {
            $options['currentTenantId'] = $this->currentTenantId;
        }
    }

    /**
     * beforeDelete callback
     *
     * always check tenant_id on DELETE operations using current sessions
     *
     * @param Event $event The beforeDelete event that was fired.
     * @param Entity $entity The entity to be deleted.
     * @param ArrayObject $options
     * @return void
     */
    public function beforeDelete(Event $event, Entity $entity, ArrayObject $options)
    {
        if (empty($options['currentTenantId']) && !empty($this->currentTenantId)) {
            $options['currentTenantId'] = $this->currentTenantId;
        }
    }
}