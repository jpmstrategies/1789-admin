<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

class EnableHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Enabled';
        $this->options = ['1' => 'Yes', '0' => 'No'];
    }
}
