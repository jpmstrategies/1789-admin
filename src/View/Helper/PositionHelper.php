<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class PositionHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Position';
        $this->options = ['U' => 'Unknown', 'Y' => 'Support', 'N' => 'Oppose'];
    }
}
