<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class BillTrackingStatusHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Bill Tracking';
        $this->options = [
            'PENDING' => 'No Tracking Status',
            'NO-VOTES' => 'No Votes',
            'FAILED' => 'Failed Introduction',
            'WILL-SCORE' => 'Will Score',
            'IGNORE' => 'Will Not Score',
        ];
    }
}
