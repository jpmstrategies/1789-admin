<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class ScoreTypeHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Score Type';
        $this->options = ['N' => 'Negative', 'P' => 'Positive', 'Z' => 'N/A'];
    }
}
