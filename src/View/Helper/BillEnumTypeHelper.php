<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class BillEnumTypeHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Type';
        $this->options = [
            '' => '',
            'cta' => 'Call to Action',
            'status' => 'Status',
        ];
    }
}
