<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use DateTime;
use DateTimeZone;

/**
 * @property Helper $Form
 */
class TimezoneHelper extends Helper
{
    var $helpers = ['Form'];

    private $options;

    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->options = $this->getTimezones();
    }

    public function control(string $fieldName, array $options = [], array $attributes = []): string
    {
        // override passed options to static list
        $options['options'] = $this->options;

        return $this->Form->control($fieldName, $options);
    }

    public function display_text($input)
    {
        $parts = explode('/', (string)$input);
        $region = $parts[0];
        return $this->options[$region][$input] ?? "Unknown Timezone";
    }

    public function getTimezones(): array
    {
        $zoneIdentifiers = timezone_identifiers_list();
        $zoneLocations = [];

        foreach ($zoneIdentifiers as $zoneIdentifier) {
            $zone = explode('/', $zoneIdentifier);
            $desiredRegions = [
                //'Africa', 'America', 'Antarctica', 'Arctic', 'Asia', 'Atlantic', 'Australia', 'Europe', 'Indian', 'Pacific'
                'America',
            ];
            if (in_array($zone[0], $desiredRegions)) {
                if (isset($zone[1]) != '') {
                    $area = str_replace('_', ' ', $zone[1]);
                    if (!empty($zone[2])) {
                        $area = $area . ' (' . str_replace('_', ' ', $zone[2]) . ')';
                    }
                    $zoneLocations[$zone[0]][$zoneIdentifier] = $zone[0] . '/' . $area;
                }
            }
        }

        $options = [];
        foreach ($zoneLocations as $zoneRegion => $regionAreas) {
            foreach ($regionAreas as $regionArea => $zoneLabel) {
                $currentTimeInZone = new DateTime("now", new DateTimeZone($regionArea));
                $currentTimeDiff = $currentTimeInZone->format('P');
                $options[$zoneRegion][$regionArea] = "$zoneLabel (GMT$currentTimeDiff)";
            }
        }

        return $options;
    }
}
