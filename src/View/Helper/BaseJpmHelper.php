<?php

namespace App\View\Helper;

use Cake\View\Helper;

/**
 * @property Helper $Form
 */
class BaseJpmHelper extends Helper
{
    var $helpers = ['Form'];

    var $friendlyName = '';
    var $options = ['A' => 'Admin', 'U' => 'User'];

    public function control(string $fieldName, array $options = [], array $attributes = []): string
    {
        // override passed options to static list
        $options['options'] = $this->options;

        return $this->Form->control($fieldName, $options);
    }

    public function display_text($input)
    {
        return $this->options[$input] ?? "Unknown " . $this->friendlyName;
    }

    public function get_options()
    {
        return $this->options;
    }
}
