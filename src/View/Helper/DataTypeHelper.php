<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class DataTypeHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Data Type';
        $this->options = ['text' => 'Text', 'textarea' => 'Textarea', 'checkbox' => 'Checkbox', 'select' => 'Select'];
    }
}
