<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class ChamberHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Chamber';
        $this->options = ['U' => 'Upper Chamber', 'L' => 'Lower Chamber'];
    }

    public function title_short($chamber)
    {
        if ($chamber === 'L') {
            $title = "Rep.";
        } elseif ($chamber === 'U') {
            $title = "Sen.";
        } else {
            $title = "Unknown Title";
        }
        return $title;
    }

    public function title_long($chamber)
    {
        if ($chamber === 'L') {
            $title = "State Rep.";
        } elseif ($chamber === 'U') {
            $title = "State Sen.";
        } else {
            $title = "Unknown Title";
        }
        return $title;
    }
}
