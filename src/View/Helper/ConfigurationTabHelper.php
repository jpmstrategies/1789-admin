<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class ConfigurationTabHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Configuration Tab';
        $this->options = [
            'admin' => 'Super Admin',
            'site' => 'Site Settings',
            'home' => 'Home Page',
            'vote' => 'Vote Page',
            'legislator' => 'Legislator Page',
            'social' => 'Social Settings',
            'display' => 'Position Display',
            'grade' => 'Grade Display',
            'user_details' => 'User Details'];
    }
}
