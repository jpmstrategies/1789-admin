<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

class VisibilityHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Visibility';
        $this->options = ['1' => 'Public', '0' => 'Private'];
    }

    public function display_text($input)
    {
        if ($input) {
            $icon = '<div class="octicon octicon-globe"></div>';
            $label = 'Public';
        } else {
            $icon = '<div class="octicon octicon-lock"></div>';
            $label = 'Private';
        }
        echo $label . ' ' . $icon;
    }
}
