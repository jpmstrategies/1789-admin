<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class UserDetailsHelper extends BaseJpmHelper
{

    public function isProfileComplete($userData, $newline = '\n'): bool
    {
        $requiredDetail = ['first_name', 'last_name', 'organization', 'phone'];
        $userDetails = $userData['details'];
        foreach ($requiredDetail as $d) {
            if (!isset($userDetails[$d])) return false;
            if (strlen((string)$userDetails[$d]) === 0) return false;
        }

        return true;
    }

    public function getEmailSignature($userData, $newline = '\n'): ?string
    {
        $userDetails = $userData['details'];
        $emailSignature = $userDetails['first_name'] . ' ' . $userDetails['last_name'] . $newline;
        $emailSignature .= $userDetails['organization'] . $newline;
        $emailSignature .= 'P: ' . $userDetails['phone'] . $newline;
        $emailSignature .= 'E: ' . $userData['email'] . $newline;
        return $emailSignature;
    }

    public function getUserNameAndEmail($userData): string
    {
        $d = $userData['user_details'];
        $name = $d['first_name'] . ' ' . $d['last_name'];
        $email = $userData['email'];
        return $this->formatNameAndEmail($name, $email);
    }

    public function formatNameAndEmail($name, $email): string
    {
        return htmlspecialchars($name . ' <' . $email . '>');
    }
}
