<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class MethodHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Method';
        $this->options = ['S' => 'Standard', 'B' => 'Bonus', 'P' => 'Party'];
    }

    public $help = [
        'S' => 'Tracks positive and negative vote evaluations.',
        'B' => 'Only tracks positive vote evaluations (i.e. a legislator is not penalized). <b>Custom formula required.</b>',
        'P' => 'Tracks based on a legislator party affiliation. <b>Custom formula required.</b>',
    ];

    function help_text($method)
    {
        if (isset($this->options[$method])) {
            $help = '<i>' . $this->options[$method] . '</i>: ' . $this->help[$method];
        } else {
            $help = "Unknown Method";
        }
        return $help;
    }
}
