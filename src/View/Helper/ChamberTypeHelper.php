<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class ChamberTypeHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Chamber Type';
        $this->options = [
            'L' => 'State Lower Chamber',
            'U' => 'State Upper Chamber',
            'C' => 'US House',
            'E' => 'State SBOE',
            'W' => 'Statewide'];
    }
}
