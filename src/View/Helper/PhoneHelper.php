<?php

namespace App\View\Helper;

use Cake\View\Helper;

/**
 * @property Helper $Form
 */
class PhoneHelper extends Helper
{
    var $helpers = ['Form'];

    function display_text($input)
    {
        $pattern = '~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~';
        preg_match($pattern, (string)$input, $matches);

        return $matches ? preg_replace($pattern, '($1) $2-$3', (string)$input) : $input;
    }
}
