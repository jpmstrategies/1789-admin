<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class LetterGradeHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Letter Grade';
        $this->options = [
            'N/A' => 'N/A',
            'F-' => 'F-',
            'F' => 'F',
            'F+' => 'F+',
            'D-' => 'D-',
            'D' => 'D',
            'D+' => 'D+',
            'C-' => 'C-',
            'C' => 'C',
            'C+' => 'C+',
            'B-' => 'B-',
            'B' => 'B',
            'B+' => 'B+',
            'A-' => 'A-',
            'A' => 'A',
            'A+' => 'A+'];
    }
}
