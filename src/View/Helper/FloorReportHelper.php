<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

class FloorReportHelper extends Helper
{

    var $helpers = ['Form'];

    private $colors = [
        'Yes' => '#4CAF50', // A brighter shade of green for "Yes"
        'Neutral' => '#FFC107', // A more vibrant shade of amber for "Neutral"
        'Vote No; Amend' => '#F57C00', // A deeper shade of orange for "Vote No; Amend"
        'Vote Yes; Amend' => '#1976D2', // A shade of blue for "Vote Yes; Amend", differentiating from a straightforward "Yes"
        'No' => '#C62828'  // A darker red for "No", emphasizing the negative stance
    ];

    function formattedLink($ratingName, $positionName, $url): string
    {
        $color = $this->colors[$positionName];

        // Bold NO votes
        $formattedText = contains($positionName, 'No')
            ? "<strong>$ratingName - $positionName</strong>"
            : "$ratingName - $positionName";

        // Consolidated HTML construction with corrected 'styleclass' to 'class' (assuming it was a typo)
        // If 'styleclass' was meant to add additional styles, merge it with the 'style' attribute
        $html = <<<HTML
            <div style="font-size: 14pt; font-family: Georgia, 'Times New Roman', Times, serif; color: #b55188; padding-bottom: 1px">
                <a href="$url" style="color: $color; font-family: Arial, Helvetica, sans-serif; font-size: 10pt; text-decoration:none" target="_blank">
                    $formattedText
                </a>
            </div>
            HTML;

        return $html;
    }
}
