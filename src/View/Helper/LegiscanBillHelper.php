<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

class LegiscanBillHelper extends Helper
{

    var $helpers = ['Form'];

    function getBillName($ratingName): string
    {
        // check if not string
        if (!is_string($ratingName)) {
            return 'Unknown Bill: ' . $ratingName;
        }

        preg_match('/([A-Z]+)([0-9]+)/', $ratingName, $matches);

        if (empty($matches)) {
            return $ratingName;
        }

        $billType = $matches[1];
        $billNumber = ltrim($matches[2], '0');

        return $billType . ' ' . $billNumber;
    }
}
