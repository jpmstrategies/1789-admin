<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class PositionTextHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Position Text';
        $this->options = [
            'U' => 'Unknown',
            'S' => 'Support',
            'O' => 'Oppose',
            'SA' => 'Support Amendment',
            'OM' => 'Support Amendment (Oppose Motion to Table)',
            'OA' => 'Oppose Amendment',
            'SM' => 'Oppose Amendment (Support Motion to Table)'];
    }
}
