<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class BillEnumBranchHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Branch';
        $this->options = [
            '' => '',
            'exe' => 'Executive',
            'leg' => 'Legislative'
        ];
    }
}
