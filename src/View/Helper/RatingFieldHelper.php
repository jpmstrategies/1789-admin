<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class RatingFieldHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Rating Field Type';
        $this->options = [
            'B' => 'Boolean',
            'J' => 'Journal Name Link',
            'L' => 'Link',
            'R' => 'Roll Call',
            'T' => 'Text'];
    }
}
