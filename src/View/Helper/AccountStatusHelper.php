<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class AccountStatusHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Account Status';
        $this->options = ['PENDING' => 'Invite Pending', 'ACTIVE' => 'Active', 'DISABLED' => 'Disabled', 'REVIEW' => 'Pending Review', 'APPROVED' => 'Approved'];
    }
}

