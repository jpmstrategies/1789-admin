<?php

namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper;

/**
 * @property Helper $Form
 */
class S3Helper extends Helper
{
    public function getFullUrl($pdf_url)
    {
        return Configure::read('App.s3_url') . '/' . Configure::read('App.s3_prefix') . '/' . $pdf_url;
    }
}
