<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class PartyHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Party';
        $this->options = ['U' => 'Unknown', 'D' => 'Democrat', 'I' => 'Independent', 'R' => 'Republican'];
    }
}
