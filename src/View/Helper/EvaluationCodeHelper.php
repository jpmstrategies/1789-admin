<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * @property Helper $Form
 */
class EvaluationCodeHelper extends BaseJpmHelper
{
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);

        $this->friendlyName = 'Evaluation Code';
        $this->options = ['N' => 'Not Applicable/Neutral', 'F' => 'Favor', 'O' => 'Oppose', 'E' => 'Stance Evaluation'];
    }
}
