<?php
declare(strict_types=1);

namespace App\Service;

use Cake\Core\Configure;
use Aws\S3\S3Client;
use Aws\Exception\AwsException;

class R2Service
{
    protected $s3Client;
    protected $bucketName;
    protected $baseUrl;

    public function __construct()
    {
        $this->s3Client = new S3Client([
            'version' => 'latest',
            'region'  => Configure::read('App.r2_region'),
            'endpoint' => Configure::read('App.r2_endpoint'),
            'credentials' => [
                'key'    => Configure::read('App.r2_key_id'),
                'secret' => Configure::read('App.r2_access_key'),
            ],
        ]);

        $this->bucketName = Configure::read('App.r2_bucket'); // Replace with your bucket name
        $this->baseUrl = Configure::read('App.r2_base_url');
    }

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * Generate a presigned URL for a given operation.
     *
     * @param string $key Object key in the bucket.
     * @param int $expiryMinutes Expiration time in minutes for the signed URL.
     * @return string Presigned URL.
     */
    public function generateSignedPutUrl(string $key, int $expiryMinutes = 15): string
    {
        try {
            $cmd = $this->s3Client->getCommand('PutObject', [
                'Bucket' => $this->bucketName,
                'Key'    => $key,
                // Maybe require ContentType
            ]);

            $request = $this->s3Client->createPresignedRequest($cmd, "+{$expiryMinutes} minutes");

            return (string)$request->getUri();
        } catch (AwsException $e) {
            throw new \RuntimeException("Error generating signed URL: " . $e->getMessage());
        }
    }

    /**
     * Retrieve metadata for an object (HEAD request).
     *
     * @param string $key Object key in the bucket.
     * @return array Metadata of the object.
     */
    public function headObject(string $key): array
    {
        try {
            $result = $this->s3Client->headObject([
                'Bucket' => $this->bucketName,
                'Key'    => $key,
            ]);

            return $result->toArray();
        } catch (AwsException $e) {
            throw new \RuntimeException("Error retrieving object metadata: " . $e->getMessage());
        }
    }

    /**
     * Delete an object from the bucket.
     *
     * @param string $key Object key in the bucket.
     * @return bool True if deletion is successful.
     */
    public function deleteObject(string $key): bool
    {
        try {
            $this->s3Client->deleteObject([
                'Bucket' => $this->bucketName,
                'Key'    => $key,
            ]);

            return true;
        } catch (AwsException $e) {
            throw new \RuntimeException("Error deleting object: " . $e->getMessage());
        }
    }

    /**
     * Upload an object to the bucket.
     *
     * @param string $key Object key in the bucket.
     * @param mixed $body Content of the object.
     * @param array $options Additional options for the upload.
     * @return array Result of the upload.
     */
    public function putObject(string $key, $body, array $options = []): array
    {
        try {
            $params = array_merge([
                'Bucket' => $this->bucketName,
                'Key'    => $key,
                'Body'   => $body,
            ], $options);

            $result = $this->s3Client->putObject($params);

            return $result->toArray();
        } catch (AwsException $e) {
            throw new \RuntimeException("Error uploading object: " . $e->getMessage());
        }
    }
}
