<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Datasource\Exception\RecordNotFoundException;
use Exception;

/**
 * BillEnums Controller
 *
 * @property \App\Model\Table\BillEnumsTable $BillEnums
 * @method \App\Model\Entity\BillEnum[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BillEnumsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $paginationSettings = [
            'contain' => ['ParentBillEnums'],
            'order' => [
                'BillEnums.type' => 'ASC',
                'BillEnums.parent_id' => 'ASC',
                'BillEnums.sort_order' => 'ASC'
            ]
        ];

        // If the user is not a super admin, only show statuses and CTAs
        if (!$this->Auth->user('is_super')) {
            $paginationSettings['conditions'] = ['BillEnums.type IN' => ['status', 'cta']];
        }

        $this->paginate = $paginationSettings;
        $billEnums = $this->paginate($this->BillEnums);

        $this->set(compact('billEnums'));
    }

    /**
     * View method
     *
     * @param string|null $id Bill Enum id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $billEnum = $this->BillEnums->get($id, [
            'contain' => ['Tenants', 'ParentBillEnums', 'ChildBillEnums'],
        ]);

        $this->set(compact('billEnum'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $billEnum = $this->BillEnums->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->replaceEmptyWithNull($this->request->getData());

            $billEnum = $this->BillEnums->patchEntity($billEnum, $data);
            if ($this->BillEnums->save($billEnum)) {
                $this->Flash->success(__('The bill enum has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bill enum could not be saved. Please, try again.'));
        }
        $parentBillEnums = $this->BillEnums->find('list', [
            'conditions' => ['type' => 'stage'],
            'limit' => 200
        ]);
        $this->set(compact('billEnum', 'parentBillEnums'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bill Enum id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $billEnum = $this->BillEnums->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->replaceEmptyWithNull($this->request->getData());

            $billEnum = $this->BillEnums->patchEntity($billEnum, $data);
            if ($this->BillEnums->save($billEnum)) {
                $this->Flash->success(__('The bill enum has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bill enum could not be saved. Please, try again.'));
        }
        $parentBillEnums = $this->BillEnums->find('list', [
            'conditions' => ['type' => 'stage'],
            'limit' => 200
        ]);
        $this->set(compact('billEnum', 'parentBillEnums'));
    }

    function replaceEmptyWithNull($data)
    {
        // Replace empty values with null
        $fieldsToNullify = ['parent_id', 'branch'];
        foreach ($fieldsToNullify as $field) {
            if (empty($data[$field])) {
                $data[$field] = null;
            }
        }
        return $data;
    }

    // Fetch the relevant statuses based on the selected stage
    public function getStatusesByStage()
    {
        $this->request->allowMethod(['ajax', 'get']);

        $stageId = $this->request->getQuery('stage_id');
        $statuses = $this->BillEnums->find('list', [
            'conditions' => [
                'type' => 'status',
                'parent_id' => $stageId  // Assuming the statuses are linked by parent_id
            ]
        ])->toArray();

        $this->autoRender = false;

        $this->response = $this->response->withType('application/json')
            ->withStringBody(json_encode($statuses));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bill Enum id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        try {
            // Fetch the bill enum or throw a RecordNotFoundException if it doesn't exist
            $billEnum = $this->BillEnums->get($id);

            // Check if there are any records where id = parent_id
            $relatedRecords = $this->BillEnums->find()
                ->where(['parent_id' => $id])
                ->count();

            if ($relatedRecords > 0) {
                // If any related records exist, throw an error and do not delete
                $this->Flash->error(__('The bill enum cannot be deleted because it has related child records.'));
            } elseif ($billEnum->id === $billEnum->parent_id) {
                // If the bill enum's id equals its parent_id, throw an error
                $this->Flash->error(__('The bill enum cannot be deleted because it is related to itself.'));
            } else {
                // Attempt to delete the entity if no related records are found
                if ($this->BillEnums->delete($billEnum)) {
                    $this->Flash->success(__('The bill enum has been deleted.'));
                } else {
                    $this->Flash->error(__('The bill enum could not be deleted. Please, try again.'));
                }
            }
        } catch (RecordNotFoundException $e) {
            // Handle the case where the bill enum wasn't found
            $this->Flash->error(__('The bill enum could not be found.'));
        } catch (Exception $e) {
            // Catch any other exceptions that may occur
            $this->Flash->error(__('An error occurred. Please, try again.'));
        }

        // Redirect to the index page after attempting the delete
        return $this->redirect(['action' => 'index']);
    }

}