<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;


use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Event\EventInterface;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow(['terms', 'privacyPolicy', 'cookiePolicy', 'cacheInvalidate']);
    }

    public function display()
    {

    }

    public function terms()
    {

    }

    public function privacyPolicy()
    {

    }

    public function cookiePolicy()
    {

    }

    public function cacheInvalidate(): \Cake\Http\Response
    {
        $this->request->allowMethod(['get']);

        $purgeCache = Cache::clear('query_cache');
        $purgeCloudflare = $this->purgeCloudflare();
        if ($purgeCache && isset($purgeCloudflare->success) && $purgeCloudflare->success) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode(['status' => 'ok']))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'status' => 'warn',
                    'cake' => ['success' => $purgeCache],
                    'cloudflare' => $purgeCloudflare]))
                ->withStatus(400);
        }

    }

    public function purgeCloudflare()
    {
        $apiZone = Configure::read('cloudflare.api_zone');
        $apiKey = Configure::read('cloudflare.api_key');

        if ($apiZone && $apiKey) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.cloudflare.com/client/v4/zones/' . $apiZone . '/purge_cache');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: Bearer ' . $apiKey,
                'Content-Type: application/json'
            ]);
            curl_setopt($ch, CURLOPT_POSTFIELDS, '{"purge_everything":true}');
            curl_setopt($ch, CURLOPT_FAILONERROR, true);

            $response = curl_exec($ch);
            if (curl_errno($ch)) {
                $results = ['success' => false, 'message' => curl_error($ch)];
            } else {
                $results = json_decode($response);
            }

            curl_close($ch);
        } else {
            $results = ['success' => true, 'message' => 'Skipping Cloudflare purge'];
        }

        return $results;
    }
}
