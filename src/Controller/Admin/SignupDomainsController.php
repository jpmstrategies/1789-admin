<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * SignupDomains Controller
 *
 * @property \App\Model\Table\SignupDomainsTable $SignupDomains
 * @method \App\Model\Entity\SignupDomain[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SignupDomainsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        if ($this->isUserNonAdmin()) return null;
        $signupDomains = $this->paginate($this->SignupDomains);

        $this->set(compact('signupDomains'));
    }

    /**
     * View method
     *
     * @param string|null $id Signup Domain id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if ($this->isUserNonAdmin()) return null;
        $signupDomain = $this->SignupDomains->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('signupDomain'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->isUserNonAdmin()) return null;
        $signupDomain = $this->SignupDomains->newEmptyEntity();
        if ($this->request->is('post')) {
            $signupDomain = $this->SignupDomains->patchEntity($signupDomain, $this->request->getData());
            if ($this->SignupDomains->save($signupDomain)) {
                $this->Flash->success(__('The signup domain has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The signup domain could not be saved. Please, try again.'));
        }
        $this->set(compact('signupDomain'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Signup Domain id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->isUserNonAdmin()) return null;
        $signupDomain = $this->SignupDomains->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $signupDomain = $this->SignupDomains->patchEntity($signupDomain, $this->request->getData());
            if ($this->SignupDomains->save($signupDomain)) {
                $this->Flash->success(__('The signup domain has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The signup domain could not be saved. Please, try again.'));
        }
        $this->set(compact('signupDomain'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Signup Domain id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        if ($this->isUserNonAdmin()) return null;
        $signupDomain = $this->SignupDomains->get($id);
        if ($this->SignupDomains->delete($signupDomain)) {
            $this->Flash->success(__('The signup domain has been deleted.'));
        } else {
            $this->Flash->error(__('The signup domain could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function checkSignupDomain($inputEmail): bool
    {
        $validEmail = false;
        $signupDomains = $this->SignupDomains->find('list', [
            'keyField' => 'name',
            'valueField' => 'regex'
        ])->toArray();

        if (!empty($signupDomains)) {
            [$inputUsername, $inputDomain] = explode('@', (string)$inputEmail);
            foreach ($signupDomains as $domain => $databaseRegex) {
                $databaseRegex = str_replace('.', '\.', (string)$databaseRegex);

                if (str_starts_with($databaseRegex, '\.')) {
                    $matchRegex = '/.*' . $databaseRegex . '$/';
                } elseif (str_starts_with($databaseRegex, '@')) {
                    $matchRegex = '/^' . str_replace('@', '', $databaseRegex) . '$/';
                } else {
                    $matchRegex = '/(^|.*\.)' . $databaseRegex . '$/';
                }

                if (preg_match($matchRegex, $inputDomain)) {
                    $validEmail = true;
                    break;
                }
            }
        } else {
            $validEmail = true;
        }

        return $validEmail;
    }
}