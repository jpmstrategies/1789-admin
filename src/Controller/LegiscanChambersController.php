<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * LegiscanChambers Controller
 *
 * @property \App\Model\Table\LegiscanChambersTable $LegiscanChambers
 * @method \App\Model\Entity\LegiscanChamber[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LegiscanChambersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $legiscanChambers = $this->paginate($this->LegiscanChambers);

        $this->set(compact('legiscanChambers'));
    }

    /**
     * View method
     *
     * @param string|null $id Legiscan Chamber id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $legiscanChamber = $this->LegiscanChambers->get($id, [
            'contain' => [
                'Chambers',
                'LegiscanBillSubjects',
                'LegiscanBillVoteDetails',
                'LegiscanBillVotes',
                'LegiscanBills'],
        ]);

        $this->set(compact('legiscanChamber'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $legiscanChamber = $this->LegiscanChambers->newEmptyEntity();
        if ($this->request->is('post')) {
            $legiscanChamber = $this->LegiscanChambers->patchEntity($legiscanChamber, $this->request->getData());
            if ($this->LegiscanChambers->save($legiscanChamber)) {
                $this->Flash->success(__('The legiscan chamber has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan chamber could not be saved. Please, try again.'));
        }
        $this->set(compact('legiscanChamber'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Legiscan Chamber id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $legiscanChamber = $this->LegiscanChambers->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $legiscanChamber = $this->LegiscanChambers->patchEntity($legiscanChamber, $this->request->getData());
            if ($this->LegiscanChambers->save($legiscanChamber)) {
                $this->Flash->success(__('The legiscan chamber has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan chamber could not be saved. Please, try again.'));
        }
        $this->set(compact('legiscanChamber'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Legiscan Chamber id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $legiscanChamber = $this->LegiscanChambers->get($id);
        if ($this->LegiscanChambers->delete($legiscanChamber)) {
            $this->Flash->success(__('The legiscan chamber has been deleted.'));
        } else {
            $this->Flash->error(__('The legiscan chamber could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}