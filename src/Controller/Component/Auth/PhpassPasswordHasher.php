<?php

namespace App\app\Controller\Component\Auth;

use App\Controller\Component\Auth\AbstractPasswordHasher;
use Cake\Core\App;

App::import('Vendor', 'PHPass/class-phpass'); //<--this exists and defines PasswordHash class

class PhpassPasswordHasher extends AbstractPasswordHasher
{
    public function hash($password)
    {
        // http://codex.wordpress.org/Function_Reference/wp_hash_password
        $PasswordHash = new PasswordHash(8, true);

        return $PasswordHash->HashPassword($password);
    }

    public function check($password, $hashedPassword)
    {
        // http://codex.wordpress.org/Function_Reference/wp_hash_password
        $PasswordHash = new PasswordHash(8, true);

        // stuff here
        return $PasswordHash->CheckPassword($password, $hashedPassword);
    }
}