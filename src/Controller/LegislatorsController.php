<?php

namespace App\Controller;


use Cake\Http\Client;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\I18n\FrozenTime;

/**
 * Legislators Controller
 *
 * @property \App\Model\Table\LegislatorsTable $Legislators
 *
 * @method \App\Model\Entity\Legislator[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LegislatorsController extends AppController
{

    public function __construct(ServerRequest $request = null, Response $response = null, $name = null, \Cake\Event\EventManager $eventManager = null)
    {
        parent::__construct($request, $response, $name, $eventManager);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $session = $this->request->getSession();
        $currentTenantDataSource = $session->read('Account.currentTenantDataSource');
        if ($currentTenantDataSource === 'legiscan') {
            $this->Flash->success(__('Note: The data below is curated weekly on Monday from LegiScan meaning you cannot add new records.'));
        }
        $this->paginate = [
            'contain' => ['Chambers', 'LegislativeSessions', 'Parties', 'People'],
            'fields' => [
                'Legislators.id',
                'Legislators.district',
                'Legislators.grade_number',
                'Legislators.is_enabled',
                'LegislativeSessions.id',
                'LegislativeSessions.name',
                'People.id',
                'People.full_name',
                'Chambers.id',
                'Chambers.name',
                'Parties.id',
                'Parties.name'
            ],
            'sortableFields' => [
                'LegislativeSessions.name',
                'People.full_name',
                'Chambers.name',
                'Parties.name',
                'Legislators.district',
                'Legislators.is_enabled'
            ]
        ];
        $legislators = $this->paginate($this->Legislators);

        $this->set(compact('legislators'));
    }

    /**
     * View method
     *
     * @param string|null $id Legislator id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $legislator = $this->Legislators->get($id, [
            'contain' => ['Chambers', 'LegislativeSessions', 'Parties', 'People', 'Grades', 'Titles'],
            'fields' => [
                'Legislators.id',
                'Legislators.legislative_session_id',
                'Legislators.person_id',
                'Legislators.district',
                'Legislators.journal_name',
                'Legislators.term_starts',
                'Legislators.term_ends',
                'Legislators.grade_number',
                'Legislators.created',
                'Legislators.modified',
                'Legislators.is_enabled',
                'LegislativeSessions.id',
                'LegislativeSessions.name',
                'People.id',
                'People.full_name',
                'Chambers.id',
                'Chambers.name',
                'Parties.id',
                'Parties.name',
                'Grades.id',
                'Grades.name',
                'Grades.css_color',
                'Titles.id',
                'Titles.title_long'
            ]
        ]);

        $this->set('legislator', $legislator);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        $currentTenantDataSource = $session->read('Account.currentTenantDataSource');
        if ($currentTenantDataSource === 'legiscan') {
            $this->Flash->error(__('This functionality is not available when using LegiScan as the primary data source!'));
            return $this->redirect(['action' => 'index']);
        }

        $legislator = $this->Legislators->newEmptyEntity();
        if ($this->request->is('post')) {
            $legislator = $this->Legislators->patchEntity($legislator, $this->request->getData());
            if ($this->Legislators->save($legislator)) {
                $this->Flash->success(__('The legislator has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legislator could not be saved. Please, try again.'));
        }
        $chambers = $this->Legislators->Chambers->find('list', ['limit' => '200']);
        $legislativeSessions = $this->Legislators->LegislativeSessions->find('list', ['limit' => '200']);
        $parties = $this->Legislators->Parties->find('list', ['limit' => 500]);
        $people = $this->getPeople();
        $grades = $this->Legislators->Grades->find('list', ['limit' => 500]);
        $titles = $this->getTitlesByChamber();

        $this->set(compact('legislator', 'chambers', 'legislativeSessions', 'parties', 'people', 'grades', 'titles'));
    }

    private function getPeople()
    {
        $people = $this->Legislators->People->find('list');

        $clean = [];
        foreach ($people->toArray() as $key => $name) {
            $clean[$key] = $name;
        }
        return $clean;
    }

    /**
     * Edit method
     *
     * @param string|null $id Legislator id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $legislator = $this->Legislators->get($id, [
            'contain' => [],
            'fields' => [
                'Legislators.id',
                'Legislators.legislative_session_id',
                'Legislators.person_id',
                'Legislators.chamber_id',
                'Legislators.party_id',
                'Legislators.district',
                'Legislators.journal_name',
                'Legislators.title_id',
                'Legislators.term_starts',
                'Legislators.term_ends',
                'Legislators.grade_id',
                'Legislators.grade_number',
                'Legislators.created',
                'Legislators.modified',
                'Legislators.is_enabled'
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $legislator = $this->Legislators->patchEntity($legislator, $this->request->getData());
            if ($this->Legislators->save($legislator)) {
                $this->Flash->success(__('The legislator has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legislator could not be saved. Please, try again.'));
        }
        $chambers = $this->Legislators->Chambers->find('list', ['limit' => '200']);
        $legislativeSessions = $this->Legislators->LegislativeSessions->find('list', ['limit' => '200']);
        $parties = $this->Legislators->Parties->find('list', ['limit' => 500]);
        $people = $this->getPeople();
        $grades = $this->Legislators->Grades->find('list', ['limit' => 500]);
        $titles = $this->getTitlesByChamber();

        $this->set(compact('legislator', 'chambers', 'legislativeSessions', 'parties', 'people', 'grades', 'titles'));
    }

    public function getTitlesByChamber()
    {
        $return = [];

        $titles = $this->Legislators->Titles->find('all',
            [
                'contain' => ['Chambers'],
                'fields' => [
                    'Titles.id',
                    'Titles.title_long',
                    'Chambers.name'
                ]
            ])->all();

        foreach ($titles as $title) {

            $id = $title->id;
            $name = $title->title_long;
            $chamber = $title->chamber->name;

            $return[$chamber][$id] = $name;
        }

        return $return;
    }

    /**
     * Delete method
     *
     * @param string|null $id Legislator id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        try {
            $legislator = $this->Legislators->get($id);
            if ($this->Legislators->delete($legislator)) {
                $this->Flash->success(__('The legislator has been deleted.'));
            } else {
                $this->Flash->error(__('The legislator could not be deleted. Please, try again.'));
            }
        } catch (\Exception) {
            $this->Flash->error(__('The legislator you are trying to delete is associated with other records. Please, try again after deleting the related records.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function getCalculatedScores($sessionId, $chamberId)
    {
        $calculatedScores = [];
        $sessions = $this->Legislators->LegislativeSessions->find()
            ->select(['slug'])
            ->where(['id' => $sessionId])
            ->first();
        $sessionSlug = $sessions['slug'];

        $chambers = $this->Legislators->Chambers->find()
            ->select(['slug'])
            ->where(['id' => $chamberId])
            ->first();
        $chamberSlug = $chambers['slug'];

        if ($sessionSlug && $chamberSlug) {
            // get tenant domain
            $currentTenantDomain = $this->getRequest()->getSession()->read('Account.currentTenantDomain');
            $isLocal = (str_contains((string)$currentTenantDomain, 'localhost') ? 1 : 0);

            if ($isLocal) {
                $apiUrl = 'http://' . $currentTenantDomain . ":3001/api/legislators";
                $clientOptions = ['ssl_verify_peer' => false, 'ssl_verify_peer_name' => false];
            } else {
                $apiUrl = 'https://' . $currentTenantDomain . "/api/legislators";
                $clientOptions = [];
            }

            $apiUrl .= "?session=$sessionSlug&chamber=$chamberSlug&includeDisabled=true&disableOverrideScores=true";
            $apiUrl .= "?t=" . strtotime("now");

            $json = [];

            $currentTenantType = $this->request->getSession()->read('Account.currentTenantType');
            if ($currentTenantType === 'D' || $currentTenantType === 'F') {
                return [];
            }

            try {
                $http = new Client();
                $response = $http->get($apiUrl, [], $clientOptions);
                $json = $response->getJson();
            } catch (\Exception) {
                $this->Flash->error(__('The Legislator Score API is not responding. Please, try again later.'));
            }

            if (empty($json)) {
                return [];
            }

            // criteria scoring
            $currentTenantConfigurations = $this->request->getSession()->read('Configurations');
            if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes') {
                // criteria scoring
                $criteria = (new CriteriasController())->parseCalculatedScores(
                    $json['results'], 'criteriaVotingBreakdown', 'legislatorId'
                );
                $calculatedScores['use_criteria'] = $criteria;
            }

            // default scoring
            $calculatedScores['partyVotingSummary'] = [];
            if (isset($json['partyVotingSummary'])) {
                $calculatedScores['partyVotingSummary'] = $json['partyVotingSummary'];
            }
            foreach ($json['results'] as $legislator) {
                $id = $legislator['legislatorId'];
                $calculatedScores['scores'][$id] = [
                    'score' => ($legislator['votingBreakdown']['score'] ?? 'N/A'),
                    'grade' => ($legislator['votingBreakdown']['grade']['name'] ?? 'N/A')
                ];
            }
        }

        return $calculatedScores;
    }

    /**
     * bulkedit method
     *
     * @return void
     */
    public function bulkedit()
    {
        $selectedFilters = [];
        $sessionId = (!empty($this->request->getParam('sessionId')) ? $this->request->getParam('sessionId') : null);
        $chamber = (!empty($this->request->getParam('chamber')) ? $this->request->getParam('chamber') : null);

        if (empty($chamber)) {
            $currentTenantId = $this->getRequest()->getSession()->read('Account.currentTenantId');
            $chamber = (new ChambersController())->getDefault($currentTenantId);
        }

        // Filter options.
        $sessions = $this->Legislators->LegislativeSessions->find('list');
        $chambers = $this->Legislators->Chambers->find('list');
        $parties = $this->Legislators->Parties->find('list');
        $grades = $this->Legislators->Grades->find('list', [
            'order' => [
                'Grades.sort_order' => 'desc'
            ]
        ]);
        $titles = $this->Legislators->Titles->find('list', [
            'conditions' => [
                'Titles.chamber_id' => $chamber
            ]
        ]);

        // get first key from list, if not set
        if (empty($sessionId)) {
            $sessionId = key($sessions->toArray());
        }

        // Get selected filters.
        $selectedFilters['session'] = $sessionId;
        $selectedFilters['chamber'] = $chamber;

        // Legislators.
        $legislators = $this->Legislators->find('all', [
            'contain' => ['LegislativeSessions', 'People'],
            'fields' => [
                'People.first_name',
                'People.last_name',
                'Legislators.id',
                'Legislators.chamber_id',
                'Legislators.legislative_session_id',
                'Legislators.person_id',
                'Legislators.party_id',
                'Legislators.district',
                'Legislators.journal_name',
                'Legislators.grade_id',
                'Legislators.grade_number',
                'Legislators.override_text',
                'Legislators.term_starts',
                'Legislators.term_ends',
                'Legislators.title_id',
                'Legislators.is_enabled'],
            'conditions' => ['Legislators.legislative_session_id' => $sessionId, 'Legislators.chamber_id' => $chamber],
            'order' => ['Legislators.journal_name']])->all();

        $session = $this->request->getSession();
        $currentTenantType = $session->read('Account.currentTenantType');

        if ($currentTenantType !== 'D' && $currentTenantType !== 'F') {
            $calculatedScores = $this->getCalculatedScores($sessionId, $chamber);
        } else {
            $calculatedScores = [];
        }

        $this->set(compact('selectedFilters', 'sessions', 'legislators', 'chambers', 'parties', 'grades', 'calculatedScores', 'titles'));
    }

    public function exportCsv()
    {
        $records = $this->Legislators->find('all', [
            'contain' => [
                'People',
                'LegislativeSessions',
                'Chambers',
                'Parties'],
            'fields' => [
                'legislator_id' => 'Legislators.id',
                'person_id' => 'Legislators.person_id',
                'type' => '\'member\'',
                'term' => 'LegislativeSessions.name',
                'district' => 'Legislators.district',
                'chamber' => 'CASE WHEN Chambers.type = \'U\' THEN \'upper\' ELSE \'lower\' END',
                'party' => 'Parties.name',
                'first_name' => 'People.first_name',
                'last_name' => 'People.last_name',
                'journal_name' => 'Legislators.journal_name']]);

        $header = [
            'legislator_id',
            'person_id',
            'type',
            'term',
            'district',
            'chamber',
            'party',
            'first_name',
            'last_name',
            'journal_name'];

        // use the CSV view
        $filename = 'Legislators (as of ' . FrozenTime::now()->i18nFormat('yyyy-MM-dd') . ').csv';
        $this->setResponse($this->getResponse()->withDownload($filename));
        $this->viewBuilder()
            ->setClassName('CsvView.Csv')
            ->setOptions(['serialize' => 'records', 'header' => $header]);

        $this->set(compact('records'));
    }

    public function exportScoreCsv($sessionId, $chamber)
    {
        $currentTenantConfigurations = $this->request->getSession()->read('Configurations');

        $currentTenantType = $this->request->getSession()->read('Account.currentTenantType');
        if ($currentTenantType === 'D' || $currentTenantType === 'F') {
            return null;
        }

        // Legislators.
        $legislators = $this->Legislators->find('all', [
            'contain' => ['LegislativeSessions', 'People', 'Parties', 'Chambers', 'Grades'],
            'fields' => [
                'Legislators.id',
                'Legislators.district',
                'Grades.name',
                'Legislators.grade_number',
                'Legislators.override_text',
                'People.first_name',
                'People.last_name',
                'Parties.name',
                'Chambers.name'],
            'conditions' => ['Legislators.legislative_session_id' => $sessionId, 'Legislators.chamber_id' => $chamber],
            'order' => ['Legislators.journal_name']])->all();

        $calculatedScores = $this->getCalculatedScores($sessionId, $chamber);

        $criterias = $calculatedScores['use_criteria']['criteria'] ?? [];
        $scores = $calculatedScores['scores'];

        $records = [];
        foreach ($legislators as $legislator) {
            $legislatorId = $legislator['id'];
            $record['legislator_id'] = $legislatorId;
            $record['district'] = $legislator['district'];
            $record['chamber'] = $legislator['chamber']['name'];
            $record['party'] = $legislator['party']['name'];
            $record['first_name'] = $legislator['person']['first_name'];
            $record['last_name'] = $legislator['person']['last_name'];

            // default scoring
            $record['calculated_score'] = $scores[$legislatorId]['score'] ?? '';
            $record['override_score'] = $legislator['grade_number'];
            $record['calculated_grade'] = $scores[$legislatorId]['grade'] ?? '';
            $record['override_grade'] = $legislator['grade']['name'] ?? '';
            $record['override_text'] = $legislator['override_text'];

            // criteria scoring
            if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes') {
                foreach ($criterias as $criteria) {
                    $criteriaId = $criteria['id'];
                    $criteriaName = $criteria['name'];

                    $partyMatch = $calculatedScores['use_criteria']['scores'][$legislatorId][$criteriaId]['partyMatch'] ?? '';
                    if ($partyMatch === 'n/a' || $partyMatch === 'true') {
                        $record[$criteriaName] = $calculatedScores['use_criteria']['scores'][$legislatorId][$criteriaId]['score'] ?? '';
                    } else {
                        $record[$criteriaName] = 'n/a';
                    }
                }
            }

            $records[] = $record;
        }

        $header = [
            'Legislator ID',
            'District',
            'Chamber',
            'Party',
            'First Name',
            'Last Name'
        ];

        // default scoring
        array_push($header, 'Calculated Score');
        array_push($header, 'Override Score');
        array_push($header, 'Calculated Grade');
        array_push($header, 'Override Grade');
        array_push($header, 'Override Text');

        // criteria scoring
        if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes') {
            foreach ($criterias as $criteria) {
                array_push($header, $criteria['name']);
            }
        }

        // use the CSV view
        $filename = 'Legislator Scores (as of ' . FrozenTime::now()->i18nFormat('yyyy-MM-dd') . ').csv';
        $this->setResponse($this->getResponse()->withDownload($filename));
        $this->viewBuilder()
            ->setClassName('CsvView.Csv')
            ->setOptions(['serialize' => 'records', 'header' => $header]);

        $this->set(compact('records'));
    }

    public function bulkUpdateRow()
    {
        $this->viewBuilder()->disableAutoLayout();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $record = $this->Legislators->get($data['id']);

            if (!empty($data['is_enabled'])) {
                $data['is_enabled'] = empty($data['is_enabled']) ? false : true;
            }

            $record = $this->Legislators->patchEntity($record, $data);
            if ($this->Legislators->save($record)) {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($data))
                    ->withStatus(200);
            } else {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($record->getErrors()))
                    ->withStatus(400);
            }
        }
    }
}
