<?php

namespace App\Controller;


/**
 * AddressTypes Controller
 *
 * @property \App\Model\Table\AddressTypesTable $AddressTypes
 *
 * @method \App\Model\Entity\AddressType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AddressTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tenants']
        ];
        $addressTypes = $this->paginate($this->AddressTypes);

        $this->set(compact('addressTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Address Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $addressType = $this->AddressTypes->get($id, [
            'contain' => []
        ]);

        $this->set('addressType', $addressType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $addressType = $this->AddressTypes->newEmptyEntity();
        if ($this->request->is('post')) {
            $addressType = $this->AddressTypes->patchEntity($addressType, $this->request->getData());
            if ($this->AddressTypes->save($addressType)) {
                $this->Flash->success(__('The address type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The address type could not be saved. Please, try again.'));
        }
        $tenants = $this->AddressTypes->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('addressType', 'tenants'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Address Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $addressType = $this->AddressTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $addressType = $this->AddressTypes->patchEntity($addressType, $this->request->getData());
            if ($this->AddressTypes->save($addressType)) {
                $this->Flash->success(__('The address type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The address type could not be saved. Please, try again.'));
        }
        $tenants = $this->AddressTypes->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('addressType', 'tenants'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Address Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        try {
            $addressType = $this->AddressTypes->get($id);

            if ($this->AddressTypes->delete($addressType)) {
                $this->Flash->success(__('The address type has been deleted.'));
            } else {
                $this->Flash->error(__('The address type could not be deleted. Please, try again.'));
            }
        } catch (\Exception) {
            $this->Flash->error(__('The address type you are trying to delete is associated with other records. Please, try again after deleting the related records.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
