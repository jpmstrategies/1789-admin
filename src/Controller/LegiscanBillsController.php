<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * LegiscanBills Controller
 *
 * @property \App\Model\Table\LegiscanBillsTable $LegiscanBills
 * @method \App\Model\Entity\LegiscanBill[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LegiscanBillsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $selectedFilters = [];
        // Get selected filters.
        $selectedFilters['session'] = (!empty($this->request->getQuery('session')) ? $this->request->getQuery('session') : null);
        $selectedFilters['chamber'] = (!empty($this->request->getQuery('chamber')) ? $this->request->getQuery('chamber') : null);
        $selectedFilters['bill'] = (!empty($this->request->getQuery('bill')) ? $this->request->getQuery('bill') : null);
        $selectedFilters['title'] = (!empty($this->request->getQuery('title')) ? $this->request->getQuery('title') : null);
        $selectedFilters['subject'] = (!empty($this->request->getQuery('subject')) ? $this->request->getQuery('subject') : null);
        $selectedFilters['priority'] = (!empty($this->request->getQuery('priority')) ? $this->request->getQuery('priority') : null);
        $selectedFilters['tracking'] = (!empty($this->request->getQuery('tracking')) ? $this->request->getQuery('tracking') : null);
        $selectedFilters['status'] = (!empty($this->request->getQuery('status')) ? $this->request->getQuery('status') : null);
        $selectedFilters['type'] = (!empty($this->request->getQuery('type')) ? $this->request->getQuery('type') : null);
        $selectedFilters['scored'] = $this->request->getQuery('scored');

        // Filter options.
        $sessions = $this->getLegiscanSessions();
        $chambers = $this->LegiscanBills->LegiscanChambers->find('list');

        // get first key from list, if not set
        if (empty($selectedFilters['session'])) {
            $selectedFilters['session'] = key($sessions->toArray());
        }

        $conditions = [];
        $joins = [];

        if (isset($selectedFilters['session'])) {
            $conditions[] = ['LegiscanBills.legiscan_session_id' => $selectedFilters['session']];
        }

        if (isset($selectedFilters['chamber'])) {
            $conditions[] = ['LegiscanBills.legiscan_chamber_id' => $selectedFilters['chamber']];
        }

        if (isset($selectedFilters['bill'])) {
            $conditions[] = $this->getBillSearchCondition($selectedFilters['bill']);
        }

        if (isset($selectedFilters['title'])) {
            $conditions[] = ['LegiscanBills.title LIKE' => '%' . $selectedFilters['title'] . '%'];
        }

        if (isset($selectedFilters['subject'])) {
            $conditions[] = ['LegiscanBillSubjects.subject_name LIKE' => '%' . $selectedFilters['subject'] . '%'];
            $joins[] = [
                'table' => 'legiscan_bill_subjects',
                'alias' => 'LegiscanBillSubjects',
                'type' => 'INNER',
                'conditions' => 'LegiscanBills.id = LegiscanBillSubjects.legiscan_bill_id'
            ];
        }

        if (isset($selectedFilters['priority']) || isset($selectedFilters['tracking'])) {
            if (isset($selectedFilters['priority'])) {
                $conditions[] = ['TenantsBills.priority_id' => $selectedFilters['priority']];
            }
            if (isset($selectedFilters['tracking'])) {
                if ($selectedFilters['tracking'] === 'PENDING') {
                    $conditions[] = ['TenantsBills.status IS NULL'];
                } else {
                    $conditions[] = ['TenantsBills.status' => $selectedFilters['tracking']];
                }
            }
            $joins[] = [
                'table' => 'tenants_bills',
                'alias' => 'TenantsBills',
                'type' => 'LEFT',
                'conditions' => 'LegiscanBills.id = TenantsBills.legiscan_bill_id'
            ];
        }

        if (isset($selectedFilters['status'])) {
            $conditions[] = ['LegiscanBills.legiscan_status_id' => $selectedFilters['status']];
        }

        if (isset($selectedFilters['type'])) {
            $conditions[] = ['LegiscanBills.bill_type_abbr' => $selectedFilters['type']];
        }

        $joins[] = [
            'table' => 'legiscan_bill_votes',
            'alias' => 'LegiscanBillVotes',
            'type' => 'LEFT',
            'conditions' => 'LegiscanBills.id = LegiscanBillVotes.legiscan_bill_id'
        ];

        $fields = [
            'LegiscanBills.id',
            'LegiscanBills.bill_number',
            'LegiscanBills.title',
            'LegiscanBills.legiscan_url',
            'LegiscanBills.state_url',
            'LegiscanBills.has_bill_vote_details'
        ];

        $groupings = [
            'LegiscanBills.id',
            'LegiscanBills.bill_number',
            'LegiscanBills.title',
            'LegiscanBills.legiscan_url',
            'LegiscanBills.state_url',
            'LegiscanBills.has_bill_vote_details',
            'LegiscanBills.status_date',
            'LegiscanBills.bill_type_name'
        ];

        $currentTenantType = $this->request->getSession()->read('Account.currentTenantType');
        if ($currentTenantType === 'F') {
            $fields[] = 'Ratings.id';
            $groupings[] = 'Ratings.id';
            $joins[] = [
                'table' => 'ratings',
                'alias' => 'Ratings',
                'type' => 'LEFT',
                'conditions' => 'LegiscanBills.id = Ratings.legiscan_bill_id'
            ];
        }

        if (isset($selectedFilters['scored'])) {
            if ($selectedFilters['scored']) {
                $conditions[] = ['Ratings.id IS NOT NULL'];
            } else {
                $conditions[] = ['Ratings.id IS NULL'];
            }
        }

        $this->paginate = [
            'contain' => [
                'TenantsBills',
                'LegiscanBillSubjects',
            ],
            'fields' => $fields,
            'conditions' => $conditions,
            'join' => $joins,
            'group' => $groupings,
            'order' => [
                'LegiscanBills.bill_type_name' => 'ASC',
                'LegiscanBills.bill_number' => 'ASC'
            ]
        ];
        $bills = $this->paginate($this->LegiscanBills);
        $priorities = $this->LegiscanBills->TenantsBills->Priorities->find('list');
        $subjects = $this->LegiscanBills->LegiscanBillSubjects->LegiscanSubjects->find('list');
        $statuses = $this->getBillStatuses();
        $billTypes = $this->getBillTypes();

        $this->set(compact('selectedFilters', 'sessions', 'bills', 'chambers', 'priorities', 'subjects',
            'statuses', 'billTypes'));
    }

    public function getLegiscanSessions()
    {
        return $this->LegiscanBills->LegiscanSessions->find('list', [
            'join' => [
                [
                    'table' => 'legislative_sessions',
                    'alias' => 'LegislativeSessions',
                    'type' => 'INNER',
                    'conditions' => 'LegislativeSessions.legiscan_session_id = LegiscanSessions.id'
                ]
            ],
            'order' => ['LegiscanSessions.year_start' => 'desc']
        ]);
    }

    public function getBillStatuses(): array
    {
        $billStatuses = [];
        $results = $this->LegiscanBills->find()
            ->select(['legiscan_status_id', 'status_desc'])->distinct(['legiscan_status_id', 'status_desc'])
            ->order(['legiscan_status_id' => 'ASC'])
            ->toArray();
        foreach ($results as $result) {
            $billStatuses[$result->legiscan_status_id] = $result->status_desc;
        }
        return $billStatuses;
    }

    public function getBillTypes(): array
    {
        $billTypes = [];
        $results = $this->LegiscanBills->find()
            ->select(['bill_type_abbr', 'bill_type_name'])->distinct(['bill_type_abbr', 'bill_type_name'])
            ->order(['bill_type_name' => 'ASC'])
            ->toArray();
        foreach ($results as $result) {
            $billTypes[$result->bill_type_abbr] = $result->bill_type_name;
        }
        return $billTypes;
    }

    /**
     * View method
     *
     * @param string|null $id Legiscan Bill id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $selectedFilters = [
            'bill_uuid' => $this->request->getQuery('bill_uuid') ?: null,
            'bill_chamber' => $this->request->getQuery('bill_chamber') ?: null
        ];

        $bill = $this->LegiscanBills->get($id, [
            'contain' => [
                'TenantsBills',
            ],
            'fields' => [
                'LegiscanBills.id',
                'LegiscanBills.bill_number',
                'LegiscanBills.title',
                'LegiscanBills.legiscan_url',
                'LegiscanBills.state_url',
                'LegiscanBills.has_bill_vote_details'
            ],
            'order' => [
                'LegiscanBills.id' => 'DESC'
            ]
        ]);

        $conditions = ['LegiscanBillVotes.legiscan_bill_id' => $id];
        if ($selectedFilters['bill_chamber']) {
            $conditions[] = ['LegiscanBillVotes.roll_call_body_abbr' => ($selectedFilters['bill_chamber'] === 'L' ? 'H' : 'S')];
        }
        $legiscanBillVotes = $this->LegiscanBills->LegiscanBillVotes->find('all', [
            'contain' => ['Ratings'],
            'conditions' => $conditions,
            'order' => [
                'LegiscanBillVotes.roll_call_date' => 'DESC', 'LegiscanBillVotes.id' => 'DESC'
            ]
        ])->toArray();

        $legiscanVoteGroupsByParty = $this->getVoteGroupsByParty($id);
        $legiscanVoteGroupsByTag = $this->getVoteGroupsByTag($id);

        $this->set(compact('bill', 'legiscanBillVotes', 'legiscanVoteGroupsByParty', 'legiscanVoteGroupsByTag', 'selectedFilters'));
    }

    public function getVoteGroupsByTag($id): array
    {
        $votesByParty = $this->LegiscanBills->LegiscanBillVoteDetails
            ->query()
            ->select([
                'state' => 'LegiscanBillVoteDetails.state',
                'legiscan_bill_id' => 'LegiscanBillVoteDetails.legiscan_bill_id',
                'legiscan_bill_vote_id' => 'LegiscanBillVoteDetails.legiscan_bill_vote_id',
                'group_name' => "ifnull(Tags.name, 'No Tags')",
                'vote_desc' => 'LegiscanBillVoteDetails.vote_desc',
                'group_count' => 'count(distinct People.legiscan_people_id)'
            ])
            ->from('legiscan_bill_vote_details LegiscanBillVoteDetails')
            ->join([
                'table' => 'people',
                'alias' => 'People',
                'type' => 'INNER',
                'conditions' => [
                    'LegiscanBillVoteDetails.legiscan_people_id = People.legiscan_people_id'
                ],
            ])
            ->join([
                'table' => 'people_tags',
                'alias' => 'PeopleTags',
                'type' => 'INNER',
                'conditions' => [
                    'People.id = PeopleTags.person_id'
                ],
            ])
            ->join([
                'table' => 'tags',
                'alias' => 'Tags',
                'type' => 'INNER',
                'conditions' => [
                    'PeopleTags.tag_id = Tags.id'
                ],
            ])
            ->where([
                'LegiscanBillVoteDetails.legiscan_bill_id' => $id
            ])
            ->order(['group_name' => 'ASC', 'vote_desc' => 'ASC'])
            ->group([
                'LegiscanBillVoteDetails.legiscan_bill_id',
                'LegiscanBillVoteDetails.legiscan_bill_vote_id',
                'Tags.name',
                'LegiscanBillVoteDetails.vote_desc'
            ]);
        return $this->getVoteGroup($votesByParty);
    }

    public function getVoteGroupsByParty($id): array
    {
        $votesByParty = $this->LegiscanBills->LegiscanBillVoteDetails
            ->query()
            ->select([
                'state' => 'LegiscanBillVoteDetails.state',
                'legiscan_bill_id' => 'LegiscanBillVoteDetails.legiscan_bill_id',
                'legiscan_bill_vote_id' => 'LegiscanBillVoteDetails.legiscan_bill_vote_id',
                'group_name' => 'LegiscanParties.party_name',
                'vote_desc' => 'LegiscanBillVoteDetails.vote_desc',
                'group_count' => 'count(1)'
            ])
            ->from('legiscan_bill_vote_details LegiscanBillVoteDetails')
            ->join([
                'table' => 'legiscan_parties',
                'alias' => 'LegiscanParties',
                'type' => 'INNER',
                'conditions' => [
                    'LegiscanBillVoteDetails.legiscan_party_id = LegiscanParties.id and LegiscanBillVoteDetails.state = LegiscanParties.state'
                ],
            ])
            ->where([
                'LegiscanBillVoteDetails.legiscan_bill_id' => $id
            ])
            ->order(['group_name' => 'ASC', 'vote_desc' => 'ASC'])
            ->group([
                'LegiscanBillVoteDetails.legiscan_bill_id',
                'LegiscanBillVoteDetails.legiscan_bill_vote_id',
                'LegiscanParties.party_name',
                'LegiscanBillVoteDetails.vote_desc'
            ]);
        return $this->getVoteGroup($votesByParty);
    }

    public function getVoteGroup($records): array
    {
        $legiscanVoteGroups = [];
        foreach ($records as $record) {
            $legiscan_bill_vote_id = $record['legiscan_bill_vote_id'];
            $group_name = $record['group_name'];

            if (!isset($legiscanVoteGroups[$legiscan_bill_vote_id][$group_name])) {
                $legiscanVoteGroups[$legiscan_bill_vote_id][$group_name] = [
                    'Yea' => 0,
                    'Nay' => 0,
                    'Not Voting' => 0,
                    'Absent' => 0];
            }

            $vote_desc = $record['vote_desc'];
            $group_count = $record['group_count'];

            $legiscanVoteGroups[$legiscan_bill_vote_id][$group_name][$vote_desc] = $group_count;
        }
        return $legiscanVoteGroups;
    }

    function getBillName($ratingName): string
    {
        preg_match('/([A-Z]+)([0-9]+)/', $ratingName, $matches);

        if (empty($matches)) {
            return $ratingName;
        }

        $billType = $matches[1];
        $billNumber = ltrim($matches[2], '0');

        return $billType . ' ' . $billNumber;
    }

    function getBillSearchCondition(string $term): array
    {
        // Trim any leading/trailing whitespace
        $term = trim($term);

        if (empty($term)) {
            return [];
        }

        // Use regex to split the term into bill type and number parts
        if (preg_match('/([A-Za-z]+)\s*(\d+)/', $term, $matches)) {
            $billType = strtoupper($matches[1] ?? '');  // Convert bill type to uppercase
            $billNumber = $matches[2] ?? '';  // Bill number

            // If both parts are found, create a LIKE condition
            if (!empty($billType) && !empty($billNumber)) {
                return [
                    'LegiscanBills.bill_number LIKE' => '%' . $billType . '%' . $billNumber . '%'
                ];
            }
        }

        // Fallback to a generic search if no match
        return [
            'LegiscanBills.bill_number LIKE' => '%' . $term . '%'
        ];
    }

    public function search()
    {
        $this->request->allowMethod(['ajax', 'get']);

        $term = $this->request->getQuery('term');
        $cornerstoneSessionId = $this->request->getQuery('session');
        $cornerstoneChamberId = $this->request->getQuery('chamber');

        // Validate session and chamber ID
        if (empty($cornerstoneSessionId) || empty($cornerstoneChamberId)) {
            return $this->respondWithError('Invalid session or chamber ID', 400);
        }

        // Search for bills
        $bills = $this->LegiscanBills->find('all', [
            'conditions' => $this->getBillSearchCondition($term),
            'join' => $this->getJoins($cornerstoneSessionId, $cornerstoneChamberId),
            'fields' => ['id', 'bill_number'],
            'order' => ['LegiscanBills.bill_number' => 'ASC'],
            'limit' => 50
        ]);

        // Format the results for the jQuery Autocomplete response
        $results = $this->formatBillResults($bills);

        // Return the JSON response
        return $this->respondWithJson($results);
    }

    /**
     * Helper function to get the JOIN clauses for the query.
     *
     * @param string $sessionId
     * @param string $chamberId
     * @return array
     */
    private function getJoins(string $sessionId, string $chamberId): array
    {
        return [
            [
                'table' => 'legislative_sessions',
                'alias' => 'LegislativeSessions',
                'type' => 'INNER',
                'conditions' => [
                    'LegiscanBills.legiscan_session_id = LegislativeSessions.legiscan_session_id',
                    'LegislativeSessions.id' => $sessionId
                ]
            ],
            [
                'table' => 'chambers',
                'alias' => 'Chambers',
                'type' => 'INNER',
                'conditions' => [
                    'LegiscanBills.legiscan_chamber_id = Chambers.legiscan_chamber_id',
                    'Chambers.id' => $chamberId
                ]
            ]
        ];
    }

    /**
     * Helper function to format the bill search results.
     *
     * @param \Cake\Datasource\ResultSetInterface $bills
     * @return array
     */
    private function formatBillResults($bills): array
    {
        $results = [];
        foreach ($bills as $bill) {
            $results[] = [
                'legiscan_bill_id' => $bill->id,
                'bill_number' => $this->getBillName($bill->bill_number)
            ];
        }

        return $results;
    }

    /**
     * Helper function to respond with JSON data.
     *
     * @param array $data
     * @param int $status
     * @return \Cake\Http\Response
     */
    private function respondWithJson(array $data, int $status = 200): \Cake\Http\Response
    {
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data))
            ->withStatus($status);
    }

    /**
     * Helper function to respond with an error message.
     *
     * @param string $message
     * @param int $status
     * @return \Cake\Http\Response
     */
    private function respondWithError(string $message, int $status = 400): \Cake\Http\Response
    {
        return $this->respondWithJson(['error' => $message], $status);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $legiscanBill = $this->LegiscanBills->newEmptyEntity();
        if ($this->request->is('post')) {
            $legiscanBill = $this->LegiscanBills->patchEntity($legiscanBill, $this->request->getData());
            if ($this->LegiscanBills->save($legiscanBill)) {
                $this->Flash->success(__('The legiscan bill has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan bill could not be saved. Please, try again.'));
        }
        $legiscanChambers = $this->LegiscanBills->LegiscanChambers->find('list', ['limit' => 200]);
        $legiscanCurrentChambers = $this->LegiscanBills->LegiscanCurrentChambers->find('list', ['limit' => 200]);
        $legiscanSessions = $this->getLegiscanSessions();
        $this->set(compact('legiscanBill', 'legiscanChambers', 'legiscanCurrentChambers', 'legiscanSessions'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Legiscan Bill id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $legiscanBill = $this->LegiscanBills->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $legiscanBill = $this->LegiscanBills->patchEntity($legiscanBill, $this->request->getData());
            if ($this->LegiscanBills->save($legiscanBill)) {
                $this->Flash->success(__('The legiscan bill has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan bill could not be saved. Please, try again.'));
        }
        $legiscanChambers = $this->LegiscanBills->LegiscanChambers->find('list', ['limit' => 200]);
        $legiscanCurrentChambers = $this->LegiscanBills->LegiscanCurrentChambers->find('list', ['limit' => 200]);
        $legiscanSessions = $this->getLegiscanSessions();
        $this->set(compact('legiscanBill', 'legiscanChambers', 'legiscanCurrentChambers', 'legiscanSessions'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Legiscan Bill id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $legiscanBill = $this->LegiscanBills->get($id);
        if ($this->LegiscanBills->delete($legiscanBill)) {
            $this->Flash->success(__('The legiscan bill has been deleted.'));
        } else {
            $this->Flash->error(__('The legiscan bill could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function bulkUpdateRow()
    {
        $this->viewBuilder()->setLayout('ajax');
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            return $this->saveTenantsBillsData($data);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([]))
                ->withStatus(405);
        }
    }

    public function saveTenantsBillsData($data): \Cake\Http\Response
    {
        // rename tracking to status to avoid filter confusion
        if (isset($data['tracking'])) {
            $data['status'] = $data['tracking'];
            unset($data['tracking']);
        }

        if (isset($data['status'])) {
            if ($data['status'] === 'PENDING') {
                $data['status'] = null;
            }
        }

        $record = $this->LegiscanBills->TenantsBills->find('all', [
            'conditions' => [
                'legiscan_bill_id' => $data['legiscan_bill_id']
            ]
        ])->first();

        if (isset($record)) {
            $record = $this->LegiscanBills->TenantsBills->patchEntity($record, $data);
            if ($this->LegiscanBills->TenantsBills->save($record)) {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($data))
                    ->withStatus(200);
            } else {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($record->getErrors()))
                    ->withStatus(400);
            }
        } else {
            $tenantBill = $this->LegiscanBills->TenantsBills->newEmptyEntity();
            $tenantBill = $this->LegiscanBills->TenantsBills->patchEntity($tenantBill, $data);
            if ($this->LegiscanBills->TenantsBills->save($tenantBill)) {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($data))
                    ->withStatus(200);
            } else {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($tenantBill->getErrors()))
                    ->withStatus(400);
            }
        }
    }
}