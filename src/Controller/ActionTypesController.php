<?php

namespace App\Controller;


/**
 * ActionTypes Controller
 *
 * @property \App\Model\Table\ActionTypesTable $ActionTypes
 *
 * @method \App\Model\Entity\ActionType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ActionTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tenants'],
            'conditions' => [
                'ActionTypes.evaluation_code !=' => 'E'
            ],
            'order' => [
                'ActionTypes.sort_order' => 'asc'
            ]
        ];
        $actionTypes = $this->paginate($this->ActionTypes);

        $this->set(compact('actionTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id ActionType id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $actionType = $this->ActionTypes->get($id, [
            'contain' => ['Tenants', 'Actions']
        ]);

        $this->set('actionType', $actionType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $actionType = $this->ActionTypes->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['evaluation_code'] = 'N';

            $actionType = $this->ActionTypes->patchEntity($actionType, $data);
            if ($this->ActionTypes->save($actionType)) {
                $this->Flash->success(__('The Action Type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Action Type could not be saved. Please, try again.'));
        }
        $tenants = $this->ActionTypes->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('actionType', 'tenants'));
    }

    /**
     * Edit method
     *
     * @param string|null $id ActionType id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $actionType = $this->ActionTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $actionType = $this->ActionTypes->patchEntity($actionType, $this->request->getData());
            if ($this->ActionTypes->save($actionType)) {
                $this->Flash->success(__('The Action Type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Action Type could not be saved. Please, try again.'));
        }
        $tenants = $this->ActionTypes->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('actionType', 'tenants'));
    }

    /**
     * Delete method
     *
     * @param string|null $id ActionType id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        try {
            $actionType = $this->ActionTypes->get($id);

            if ($this->ActionTypes->delete($actionType)) {
                $this->Flash->success(__('The Action Type has been deleted.'));
            } else {
                $this->Flash->error(__('The Action Type could not be deleted. Please, try again.'));
            }
        } catch (\Exception) {
            $this->Flash->error(__('The Action Type you are trying to delete is associated with other records. Please, try again after deleting the related records.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
