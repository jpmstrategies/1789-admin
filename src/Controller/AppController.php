<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use App\Event\TenantListener;
use App\Event\TenantStateListener;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\EventInterface;
use Cake\Event\EventManager;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $currentTenantId;
    public $currentTenantRole;
    public $currentTenantName;
    public $currentTenantState;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'], // Added this line
            'authenticate' => [
                'Form' => [
                    'finder' => 'auth',
                    'fields' => ['username' => 'email']
                ]
            ],
            'loginAction' => [
                'controller' => 'users',
                'action' => 'login',
                'plugin' => false,
                'prefix' => false
            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'login',
                'plugin' => false,
                'prefix' => false
            ]
        ]);

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    }

    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);

        // set theme
        $this->viewBuilder()->setLayout('bootstrap');

        if (Configure::read('alerts.enable_sending') !== true) {
            $this->loadModel('Queue.QueuedJobs');
            $this->QueuedJobs->deleteAll([]);

            $this->loadModel('Queue.QueueProcesses');
            $this->QueueProcesses->deleteAll([]);
        }

        // determine role and if logged in
        $user_id = $this->Auth->user('id');
        $logged_in = isset($user_id);

        $this->set('logged_in', $logged_in);

        // determine if Super Admin
        $is_super = $this->Auth->user('is_super');
        $this->set('is_super', $is_super);

        $userData = $this->Auth->user();
        $this->set('userData', $userData);

        // set tenant
        $session = $this->request->getSession();
        $numberOfTenants = $session->read('Account.numberOfTenants');
        $currentTenantId = $session->read('Account.currentTenantId');
        $currentTenantName = $session->read('Account.currentTenantName');
        $currentTenantRole = $session->read('Account.currentTenantRole');
        $currentTenantState = $session->read('Account.currentTenantState');
        $currentTenantIdPublic = $session->read('Account.currentTenantIdPublic');
        $currentTenantDomain = $session->read('Account.currentTenantDomain');
        $currentTenantType = $session->read('Account.currentTenantType');
        $currentTenantDataSource = $session->read('Account.currentTenantDataSource');
        $currentTenantFeatures = $session->read('Account.currentTenantFeatures');
        $currentTenantConfigurations = $session->read('Configurations');

        $this->set('numberOfTenants', $numberOfTenants);
        $this->set('currentTenantId', $currentTenantId);
        $this->set('currentTenantName', $currentTenantName);
        $this->set('currentTenantRole', $currentTenantRole);
        $this->set('currentTenantState', $currentTenantState);
        $this->set('currentTenantDomain', $currentTenantDomain);
        $this->set('currentTenantIdPublic', $currentTenantIdPublic);
        $this->set('currentTenantType', $currentTenantType);
        $this->set('currentTenantDataSource', $currentTenantDataSource);
        $this->set('currentTenantConfigurations', $currentTenantConfigurations);
        $this->set('currentTenantFeatures', $currentTenantFeatures);

        $this->currentTenantId = $currentTenantId;
        $this->currentTenantName = $currentTenantName;
        $this->currentTenantRole = $currentTenantRole;
        $this->currentTenantState = $currentTenantState;

        $tenantListener = new TenantListener($this->currentTenantId);
        EventManager::instance()->on($tenantListener);

        $tenantStateListener = new TenantStateListener($this->currentTenantState);
        EventManager::instance()->on($tenantStateListener);
    }

    public function isUserNonSuperAdmin(): bool
    {
        $is_super = $this->Auth->user('is_super');

        if (!$is_super) {
            $this->Flash->error(__('Access denied'));

            $this->redirect(['controller' => 'Pages', 'action' => 'home', 'plugin' => false, 'prefix' => false]);
            return true;
        }
        return false;
    }

    public function isUserNonAdmin(): bool
    {
        if ($this->currentTenantRole === 'U') {
            $this->Flash->error(__('Access denied'));

            $this->redirect(['controller' => 'Pages', 'action' => 'home', 'plugin' => false, 'prefix' => false]);
            return true;
        }
        return false;
    }

    public function getLoginRedirect(): ?\Cake\Http\Response
    {
        $redirect = $this->request->getQuery('redirect');
        if (isset($redirect)) {
            return $this->redirect($redirect);
        } else if (Configure::read('users.default_login_redirect') !== null) {
            return $this->redirect(Configure::read('users.default_login_redirect'));
        } else {
            return $this->redirect([
                'controller' => 'Pages',
                'action' => 'home',
                'plugin' => false,
                'prefix' => false]);
        }
    }

    public function isAuthorized(): bool
    {
        return true;
    }
}
