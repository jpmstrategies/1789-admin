<?php

namespace App\Controller;


/**
 * Actions Controller
 *
 * @property \App\Model\Table\ActionsTable $Actions
 *
 * @method \App\Model\Entity\Action[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ActionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $session = $this->request->getSession();
        $currentTenantDataSource = $session->read('Account.currentTenantDataSource');
        if ($currentTenantDataSource === 'legiscan') {
            $this->Flash->success(__('Note: The data below is curated weekly on Monday from LegiScan meaning you cannot add new records.'));
        }
        $this->paginate = [
            'contain' => ['ActionTypes', 'RatingTypes'],
            'fields' => [
                'Actions.id',
                'Actions.name',
                'Actions.description',
                'RatingTypes.id',
                'RatingTypes.name',
                'ActionTypes.id',
                'ActionTypes.name'
            ],
            'order' => [
                'RatingTypes.name',
                'ActionTypes.name'
            ]
        ];
        $actions = $this->paginate($this->Actions);

        $this->set(compact('actions'));
    }

    /**
     * View method
     *
     * @param string|null $id Action id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $action = $this->Actions->get($id, [
            'contain' => ['ActionTypes', 'RatingTypes'],
            'fields' => [
                'Actions.id',
                'Actions.name',
                'Actions.description',
                'Actions.denom_value',
                'Actions.multiplier_support',
                'Actions.multiplier_oppose',
                'Actions.created',
                'Actions.modified',
                'RatingTypes.id',
                'RatingTypes.name',
                'ActionTypes.id',
                'ActionTypes.name'
            ]
        ]);

        $this->set('action', $action);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        $currentTenantDataSource = $session->read('Account.currentTenantDataSource');
        if ($currentTenantDataSource === 'legiscan') {
            $this->Flash->error(__('This functionality is not available when using LegiScan as the primary data source!'));
            return $this->redirect(['action' => 'index']);
        }

        $action = $this->Actions->newEmptyEntity();
        if ($this->request->is('post')) {
            $action = $this->Actions->patchEntity($action, $this->request->getData());
            if ($this->Actions->save($action)) {
                $this->Flash->success(__('The action has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The action could not be saved. Please, try again.'));
        }
        $ratingTypes = $this->Actions->RatingTypes->find('list', ['limit' => 200]);
        $actionTypes = $this->Actions->ActionTypes->find('list', [
                'conditions' => [
                    'ActionTypes.evaluation_code IN' => ['E', 'N']
                ],
                'limit' => 200
            ]
        );
        $this->set(compact('action', 'ratingTypes', 'actionTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Action id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $action = $this->Actions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $action = $this->Actions->patchEntity($action, $this->request->getData());
            if ($this->Actions->save($action)) {
                $this->Flash->success(__('The action has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The action could not be saved. Please, try again.'));
        }
        $ratingTypes = $this->Actions->RatingTypes->find('list', ['limit' => 200]);
        $actionTypes = $this->Actions->ActionTypes->find('list', [
                'conditions' => [
                    'ActionTypes.evaluation_code IN' => ['E', 'N']
                ],
                'limit' => 200
            ]
        );
        $this->set(compact('action', 'ratingTypes', 'actionTypes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Action id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        try {
            $action = $this->Actions->get($id);

            if ($this->Actions->delete($action)) {
                $this->Flash->success(__('The action has been deleted.'));
            } else {
                $this->Flash->error(__('The action could not be deleted. Please, try again.'));
            }
        } catch (\Exception) {
            $this->Flash->error(__('The action you are trying to delete is associated with other records. Please, try again after deleting the related records.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
