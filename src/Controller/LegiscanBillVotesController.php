<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * LegiscanBillVotes Controller
 *
 * @property \App\Model\Table\LegiscanBillVotesTable $LegiscanBillVotes
 * @method \App\Model\Entity\LegiscanBillVote[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LegiscanBillVotesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'LegiscanBills',
                'LegiscanSessions',
                'LegiscanChambers',
                'LegiscanCurrentChambers',
                'LegiscanVoteChambers'],
        ];
        $legiscanBillVotes = $this->paginate($this->LegiscanBillVotes);

        $this->set(compact('legiscanBillVotes'));
    }

    /**
     * View method
     *
     * @param string|null $id Legiscan Bill Vote id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $legiscanBillVote = $this->LegiscanBillVotes->get($id, [
            'contain' => [
                'LegiscanBills',
                'LegiscanSessions',
                'LegiscanChambers',
                'LegiscanCurrentChambers',
                'LegiscanVoteChambers',
                'LegiscanBillVoteDetails',
                'Ratings'],
        ]);

        $this->set(compact('legiscanBillVote'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $legiscanBillVote = $this->LegiscanBillVotes->newEmptyEntity();
        if ($this->request->is('post')) {
            $legiscanBillVote = $this->LegiscanBillVotes->patchEntity($legiscanBillVote, $this->request->getData());
            if ($this->LegiscanBillVotes->save($legiscanBillVote)) {
                $this->Flash->success(__('The legiscan bill vote has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan bill vote could not be saved. Please, try again.'));
        }
        $legiscanBills = $this->LegiscanBillVotes->LegiscanBills->find('list', ['limit' => 200]);
        $legiscanSessions = $this->LegiscanBillVotes->LegiscanSessions->find('list', ['limit' => 200]);
        $legiscanChambers = $this->LegiscanBillVotes->LegiscanChambers->find('list', ['limit' => 200]);
        $legiscanCurrentChambers = $this->LegiscanBillVotes->LegiscanCurrentChambers->find('list', ['limit' => 200]);
        $legiscanVoteChambers = $this->LegiscanBillVotes->LegiscanVoteChambers->find('list', ['limit' => 200]);
        $this->set(compact('legiscanBillVote', 'legiscanBills', 'legiscanSessions', 'legiscanChambers', 'legiscanCurrentChambers', 'legiscanVoteChambers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Legiscan Bill Vote id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $legiscanBillVote = $this->LegiscanBillVotes->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $legiscanBillVote = $this->LegiscanBillVotes->patchEntity($legiscanBillVote, $this->request->getData());
            if ($this->LegiscanBillVotes->save($legiscanBillVote)) {
                $this->Flash->success(__('The legiscan bill vote has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan bill vote could not be saved. Please, try again.'));
        }
        $legiscanBills = $this->LegiscanBillVotes->LegiscanBills->find('list', ['limit' => 200]);
        $legiscanSessions = $this->LegiscanBillVotes->LegiscanSessions->find('list', ['limit' => 200]);
        $legiscanChambers = $this->LegiscanBillVotes->LegiscanChambers->find('list', ['limit' => 200]);
        $legiscanCurrentChambers = $this->LegiscanBillVotes->LegiscanCurrentChambers->find('list', ['limit' => 200]);
        $legiscanVoteChambers = $this->LegiscanBillVotes->LegiscanVoteChambers->find('list', ['limit' => 200]);
        $this->set(compact('legiscanBillVote', 'legiscanBills', 'legiscanSessions', 'legiscanChambers', 'legiscanCurrentChambers', 'legiscanVoteChambers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Legiscan Bill Vote id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $legiscanBillVote = $this->LegiscanBillVotes->get($id);
        if ($this->LegiscanBillVotes->delete($legiscanBillVote)) {
            $this->Flash->success(__('The legiscan bill vote has been deleted.'));
        } else {
            $this->Flash->error(__('The legiscan bill vote could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}