<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\ORM\TableRegistry;

/**
 * Organizations Controller
 *
 * @property \App\Model\Table\OrganizationsTable $Organizations
 * @method \App\Model\Entity\Organization[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrganizationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tenants'],
            'fields' => [
                'Organizations.id',
                'Organizations.name',
                'Organizations.sort_order',
                'Tenants.name',
                'SyncSession.id',
                'SyncSession.name'
            ],
            'join' => [
                'alias' => 'SyncSession',
                'table' => 'legislative_sessions',
                'type' => 'LEFT',
                'conditions' => '`SyncSession`.`id` = `Organizations`.`sync_legislative_session_id`'
            ]
        ];
        $organizations = $this->paginate($this->Organizations);

        $this->set(compact('organizations'));
    }

    /**
     * View method
     *
     * @param string|null $id Organization id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $organization = $this->Organizations->get($id, [
            'contain' => ['Tenants'],
            'fields' => [
                'Organizations.id',
                'Organizations.tenant_id',
                'Organizations.org_tenant_id',
                'Organizations.sync_legislative_session_id',
                'Organizations.name',
                'Organizations.description',
                'Organizations.website',
                'Organizations.image_url',
                'Organizations.sort_order',
                'Organizations.created',
                'Organizations.modified',
                'Tenants.name',
                'SyncSession.name'
            ],
            'join' => [
                'alias' => 'SyncSession',
                'table' => 'legislative_sessions',
                'type' => 'LEFT',
                'conditions' => '`SyncSession`.`id` = `Organizations`.`sync_legislative_session_id`'
            ]
        ]);

        $this->set(compact('organization'));
    }

    private function getTenantsByState()
    {
        $session = $this->request->getSession();
        $currentTenantState = $session->read('Account.currentTenantState');
        $currentTenant = $session->read('Account.currentTenantId');
        return $this->Organizations->Tenants->find('list', [
            'conditions' => ['state' => $currentTenantState, 'id !=' => $currentTenant],
            'limit' => 200
        ]);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $organization = $this->Organizations->newEmptyEntity();
        if ($this->request->is('post')) {
            $organization = $this->Organizations->patchEntity($organization, $this->request->getData());
            if ($this->Organizations->save($organization)) {
                $this->Flash->success(__('The organization has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The organization could not be saved. Please, try again.'));
        }
        $tenants = $this->getTenantsByState();
        $this->set(compact('organization', 'tenants'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Organization id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $organization = $this->Organizations->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $organization = $this->Organizations->patchEntity($organization, $this->request->getData());
            if ($this->Organizations->save($organization)) {
                $this->Flash->success(__('The organization has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The organization could not be saved. Please, try again.'));
        }
        $tenants = $this->getTenantsByState();
        $this->set(compact('organization', 'tenants'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Organization id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $organization = $this->Organizations->get($id);
        if ($this->Organizations->delete($organization)) {
            $this->Flash->success(__('The organization has been deleted.'));
        } else {
            $this->Flash->error(__('The organization could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
