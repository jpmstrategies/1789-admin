<?php

namespace App\Controller;


/**
 * Addresses Controller
 *
 * @property \App\Model\Table\AddressesTable $Addresses
 *
 * @method \App\Model\Entity\Address[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AddressesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['People', 'AddressTypes'],
            'fields' => [
                'Addresses.id',
                'Addresses.capitol_address',
                'Addresses.full_address',
                'People.id',
                'People.full_name',
                'AddressTypes.id',
                'AddressTypes.name'
            ],
            'order' => [
                'People.last_name ASC',
                'People.first_name ASC'
            ],
            'sortableFields' => [
                'People.full_name',
                'AddressTypes.name',
                'Addresses.full_address',
                'Addresses.capitol_address'
            ]
        ];
        $addresses = $this->paginate($this->Addresses);

        $this->set(compact('addresses'));
    }

    /**
     * View method
     *
     * @param string|null $id Address id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $address = $this->Addresses->get($id, [
            'contain' => ['People', 'AddressTypes'],
            'fields' => [
                'Addresses.id',
                'Addresses.capitol_address',
                'Addresses.full_address',
                'Addresses.phone',
                'Addresses.fax',
                'Addresses.created',
                'Addresses.modified',
                'People.id',
                'People.full_name',
                'AddressTypes.id',
                'AddressTypes.name'
            ]
        ]);

        $this->set('address', $address);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $address = $this->Addresses->newEmptyEntity();
        if ($this->request->is('post')) {
            $address = $this->Addresses->patchEntity($address, $this->request->getData());
            if ($this->Addresses->save($address)) {
                $this->Flash->success(__('The address has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The address could not be saved. Please, try again.'));
        }
        $people = $this->Addresses->People->find('list', ['limit' => 500]);
        $addressTypes = $this->Addresses->AddressTypes->find('list', ['limit' => 500]);
        $this->set(compact('address', 'people', 'addressTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Address id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $address = $this->Addresses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $address = $this->Addresses->patchEntity($address, $this->request->getData());
            if ($this->Addresses->save($address)) {
                $this->Flash->success(__('The address has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The address could not be saved. Please, try again.'));
        }
        $people = $this->Addresses->People->find('list', ['limit' => 500]);
        $addressTypes = $this->Addresses->AddressTypes->find('list', ['limit' => 500]);
        $this->set(compact('address', 'people', 'addressTypes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Address id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $address = $this->Addresses->get($id);
        if ($this->Addresses->delete($address)) {
            $this->Flash->success(__('The address has been deleted.'));
        } else {
            $this->Flash->error(__('The address could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
