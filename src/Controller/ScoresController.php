<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\ORM\Locator\TableLocator;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;


/**
 * Scores Controller
 *
 * @property \App\Model\Table\ScoresTable $Scores
 * @method \App\Model\Entity\Score[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ScoresController extends AppController
{
    private readonly \Cake\ORM\Locator\TableLocator $tableLocatorClass;
    private readonly \Cake\Datasource\ConnectionInterface $connection;

    public function __construct(ServerRequest $request = null, Response $response = null, $name = null, \Cake\Event\EventManager $eventManager = null)
    {
        $this->tableLocatorClass = new TableLocator();
        $this->connection = ConnectionManager::get('default');

        parent::__construct($request, $response, $name, $eventManager);
    }

    public function bulkedit()
    {
        $selectedFilters = [];
        $sessionId = (!empty($this->request->getParam('sessionId')) ? $this->request->getParam('sessionId') : null);
        $chamber = (!empty($this->request->getParam('chamber')) ? $this->request->getParam('chamber') : null);
        $orgId = (!empty($this->request->getParam('orgId')) ? $this->request->getParam('orgId') : null);

        if (empty($chamber)) {
            $currentTenantId = $this->getRequest()->getSession()->read('Account.currentTenantId');
            $chamber = (new ChambersController())->getDefault($currentTenantId);
        }

        // Filter options.
        $oLegislativeSessionsTable = $this->tableLocatorClass->get('LegislativeSessions');
        $sessions = $oLegislativeSessionsTable->find('list');
        $oChambersTable = $this->tableLocatorClass->get('Chambers');
        $chambers = $oChambersTable->find('list');
        $organizations = $this->Scores->Organizations->find('list', [
            'order' => 'Organizations.name'
        ]);

        // get first key from list, if not set
        if (empty($sessionId)) {
            $sessionId = key($sessions->toArray());
        }
        if (empty($orgId)) {
            $orgId = key($organizations->toArray());
        }

        // Get selected filters.
        $selectedFilters['session'] = $sessionId;
        $selectedFilters['chamber'] = $chamber;
        $selectedFilters['orgId'] = $orgId;

        $currentTenantId = $this->getRequest()->getSession()->read('Account.currentTenantId');
        if ($this->insertScores($currentTenantId, 'tenant')) {
            // Legislators.
            $scores = $this->Scores->find('all', [
                'contain' => [],
                'fields' => ['People.first_name', 'People.last_name', 'Scores.id', 'Scores.display_text'],
                'conditions' => [
                    'Legislators.legislative_session_id' => $sessionId,
                    'Legislators.chamber_id' => $chamber,
                    'Scores.organization_id' => $orgId],
                'join' => [
                    'Legislators' => [
                        'table' => 'legislators',
                        'type' => 'INNER',
                        'foreignKey' => false,
                        'conditions' => 'Scores.legislator_id = Legislators.id'],
                    'People' => [
                        'table' => 'people',
                        'type' => 'INNER',
                        'foreignKey' => false,
                        'conditions' => 'Legislators.person_id = People.id'],
                ],
                'order' => ['People.last_name', 'People.first_name']])->all();

            $org = $this->Scores->Organizations->find('all', [
                    'conditions' => [
                        'id' => $orgId
                    ]
                ]
            )->first();
        }

        $this->set(compact('selectedFilters', 'sessions', 'scores', 'chambers', 'organizations', 'org'));
    }

    public function bulkUpdateRow()
    {
        $this->viewBuilder()->disableAutoLayout();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $record = $this->Scores->get($data['id']);

            $record = $this->Scores->patchEntity($record, $data);
            if ($this->Scores->save($record)) {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($data))
                    ->withStatus(200);
            } else {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($record->getErrors()))
                    ->withStatus(400);
            }
        }
    }

    public function insertScores($id = null, $idType = null)
    {
        $query = '
            insert into scores (id, tenant_id, legislator_id, organization_id, display_text, created, modified)
            select uuid(), t.id, l.id, o.id, null, now(), now()
            from tenants t
                     join organizations o on t.id = o.tenant_id
                     join legislators l on t.id = l.tenant_id
                     left join scores s on t.id = s.tenant_id and o.id = s.organization_id and l.id = s.legislator_id
            where s.id is null';

        if ($id !== null && $idType === 'tenant') $query .= " and t.id = '$id'";
        $results = $this->connection->query($query);

        if ($results) {
            $this->Flash->success(__('The related scores have been created.'));
            return true;
        } else {
            $this->Flash->error(__('The related scores data could not be saved. Please, try again.'));
            return false;
        }
    }
}
