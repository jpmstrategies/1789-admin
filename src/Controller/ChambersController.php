<?php

namespace App\Controller;

/**
 * Chambers Controller
 *
 * @property \App\Model\Table\ChambersTable $Chambers
 *
 * @method \App\Model\Entity\Chamber[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ChambersController extends AppController
{

    public function getDefault($tenantId)
    {
        $defaultChamber = $this->Chambers->find('all', ['conditions' => [
            'Chambers.tenant_id' => $tenantId,
            'Chambers.is_default' => true
        ]])->toArray();

        return $defaultChamber[0]['id'];
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $session = $this->request->getSession();
        $currentTenantDataSource = $session->read('Account.currentTenantDataSource');
        if ($currentTenantDataSource === 'legiscan') {
            $this->Flash->success(__('Note: The data below is curated weekly on Monday from LegiScan meaning you cannot add new records.'));
        }
        $this->paginate = [
            'contain' => [],
            'fields' => [
                'Chambers.id',
                'Chambers.type',
                'Chambers.name',
                'Chambers.is_enabled',
                'Chambers.is_default',
                'Chambers.sort_order'
            ],
            'order' => [
                'Chambers.sort_order' => 'ASC'
            ]
        ];
        $chambers = $this->paginate($this->Chambers);

        $this->set(compact('chambers'));
    }

    public function getChambers()
    {
        return $this->Chambers->find('list');
    }

    /**
     * View method
     *
     * @param string|null $id Chamber id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $chamber = $this->Chambers->get($id, [
            'contain' => []
        ]);

        $this->set('chamber', $chamber);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    /*
    public function add()
    {
        $chamber = $this->Chambers->newEmptyEntity();
        if ($this->request->is('post')) {
            $chamber = $this->Chambers->patchEntity($chamber, $this->request->getData());
            if ($this->Chambers->save($chamber)) {
                $this->Flash->success(__('The chamber has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The chamber could not be saved. Please, try again.'));
        }
        $tenants = $this->Chambers->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('chamber', 'tenants'));
    }
    */

    /**
     * Edit method
     *
     * @param string|null $id Chamber id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $chamber = $this->Chambers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $data = $this->request->getData();

            if ($data['is_default']) {

                $currentTenantId = $this->getRequest()->getSession()->read('Account.currentTenantId');

                // set other chamber default to false
                $this->Chambers->updateAll(
                    [  // fields
                        'is_default' => false
                    ],
                    [  // conditions
                        'tenant_id' => $currentTenantId,
                        'id !=' => $id
                    ]
                );

                $data['sort_order'] = 1;

                // enable chamber
                $data['is_enabled'] = true;
            } else {
                if ($data['sort_order'] == 1) {
                    $this->Flash->error(__('Only the default chamber can have a sort order of 1.'));
                    unset($data['sort_order']);
                }
            }

            $chamber = $this->Chambers->patchEntity($chamber, $data);

            if ($this->Chambers->save($chamber)) {
                $this->Flash->success(__('The chamber has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The chamber could not be saved. Please, try again.'));
        }
        $tenants = $this->Chambers->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('chamber', 'tenants'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Chamber id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /*
    public function delete($id = null)
    {

        // we are not going to allow deletes
        // but if we did, ensure at least one exists
        $this->request->allowMethod(['post', 'delete']);
        $chamber = $this->Chambers->get($id);
        if ($this->Chambers->delete($chamber)) {
            $this->Flash->success(__('The chamber has been deleted.'));
        } else {
            $this->Flash->error(__('The chamber could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }*/
}
