<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Actions Controller
 *
 * @property \App\Model\Table\ActionsTable $Actions
 * @method \App\Model\Entity\Action[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ActionsController extends AppController
{
    /**
     * Initialize controller
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->request->allowMethod(['get']);

        $limit = $this->request->getQuery('limit');
        if ($limit === 'all') {
            $actions = $this->Actions->find('all')->toArray();
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'info' => [
                        'limit' => 'all',
                        'offset' => 0,
                        'total' => sizeof($actions)
                    ],
                    'results' => $actions,
                ]))
                ->withStatus(200);
        } else {
            $actions = $this->paginate($this->Actions);
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'info' => [
                        'limit' => $this->Paginator->getPagingParams()['Actions']['perPage'],
                        'offset' => $this->Paginator->getPagingParams()['Actions']['start'] - 1,
                        'total' => $this->Paginator->getPagingParams()['Actions']['count']
                    ],
                    'results' => $actions,
                ]))
                ->withStatus(200);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Action id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $action = $this->Actions->get($id);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['action' => $action]))
            ->withStatus(200);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);
        $action = $this->Actions->newEntity($this->request->getData());
        if ($this->Actions->save($action)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                    'action' => $action
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($action->getErrors()))
                ->withStatus(400);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Action id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        $this->request->allowMethod(['patch', 'put']);
        $action = $this->Actions->get($id);
        $action = $this->Actions->patchEntity($action, $this->request->getData());
        if ($this->Actions->save($action)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                    'action' => $action
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($action->getErrors()))
                ->withStatus(400);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Action id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id)
    {
        $this->request->allowMethod(['delete']);
        $action = $this->Actions->get($id);
        if ($this->Actions->delete($action)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($action->getErrors()))
                ->withStatus(400);
        }
    }
}