<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * LegislativeSessions Controller
 *
 * @property \App\Model\Table\LegislativeSessionsTable $LegislativeSessions
 * @method \App\Model\Entity\LegislativeSession[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LegislativeSessionsController extends AppController
{
    /**
     * Initialize controller
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->request->allowMethod(['get']);

        $limit = $this->request->getQuery('limit');
        if ($limit === 'all') {
            $legislativeSessions = $this->LegislativeSessions->find('all')->toArray();
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'info' => [
                        'limit' => 'all',
                        'offset' => 0,
                        'total' => sizeof($legislativeSessions)
                    ],
                    'results' => $legislativeSessions,
                ]))
                ->withStatus(200);
        } else {
            $legislativeSessions = $this->paginate($this->LegislativeSessions);
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'info' => [
                        'limit' => $this->Paginator->getPagingParams()['LegislativeSessions']['perPage'],
                        'offset' => $this->Paginator->getPagingParams()['LegislativeSessions']['start'] - 1,
                        'total' => $this->Paginator->getPagingParams()['LegislativeSessions']['count']
                    ],
                    'results' => $legislativeSessions,
                ]))
                ->withStatus(200);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Legislative Session id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $legislativeSession = $this->LegislativeSessions->get($id);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['legislativeSession' => $legislativeSession]))
            ->withStatus(200);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);
        $legislativeSession = $this->LegislativeSessions->newEntity($this->request->getData());
        if ($this->LegislativeSessions->save($legislativeSession)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                    'legislativeSession' => $legislativeSession
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($legislativeSession->getErrors()))
                ->withStatus(400);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Legislative Session id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        $this->request->allowMethod(['patch', 'put']);
        $legislativeSession = $this->LegislativeSessions->get($id);
        $legislativeSession = $this->LegislativeSessions->patchEntity($legislativeSession, $this->request->getData());
        if ($this->LegislativeSessions->save($legislativeSession)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                    'legislativeSession' => $legislativeSession
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($legislativeSession->getErrors()))
                ->withStatus(400);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Legislative Session id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id)
    {
        $this->request->allowMethod(['delete']);
        $legislativeSession = $this->LegislativeSessions->get($id);
        if ($this->LegislativeSessions->delete($legislativeSession)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($legislativeSession->getErrors()))
                ->withStatus(400);
        }
    }
}