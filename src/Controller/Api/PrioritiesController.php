<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Priorities Controller
 *
 * @property \App\Model\Table\PrioritiesTable $Priorities
 * @method \App\Model\Entity\Priority[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PrioritiesController extends AppController
{
    /**
     * Initialize controller
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->request->allowMethod(['get']);

        $limit = $this->request->getQuery('limit');
        if ($limit === 'all') {
            $priorities = $this->Priorities->find('all')->toArray();
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'info' => [
                        'limit' => 'all',
                        'offset' => 0,
                        'total' => sizeof($priorities)
                    ],
                    'results' => $priorities,
                ]))
                ->withStatus(200);
        } else {
            $priorities = $this->paginate($this->Priorities);
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'info' => [
                        'limit' => $this->Paginator->getPagingParams()['Priorities']['perPage'],
                        'offset' => $this->Paginator->getPagingParams()['Priorities']['start'] - 1,
                        'total' => $this->Paginator->getPagingParams()['Priorities']['count']
                    ],
                    'results' => $priorities,
                ]))
                ->withStatus(200);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Priority id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $priority = $this->Priorities->get($id);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['priority' => $priority]))
            ->withStatus(200);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);
        $priority = $this->Priorities->newEntity($this->request->getData());
        if ($this->Priorities->save($priority)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                    'priority' => $priority
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($priority->getErrors()))
                ->withStatus(400);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Priority id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        $this->request->allowMethod(['patch', 'put']);
        $priority = $this->Priorities->get($id);
        $priority = $this->Priorities->patchEntity($priority, $this->request->getData());
        if ($this->Priorities->save($priority)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                    'priority' => $priority
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($priority->getErrors()))
                ->withStatus(400);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Priority id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id)
    {
        $this->request->allowMethod(['delete']);
        $priority = $this->Priorities->get($id);
        if ($this->Priorities->delete($priority)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($priority->getErrors()))
                ->withStatus(400);
        }
    }
}