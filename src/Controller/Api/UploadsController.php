<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;
use App\Service\R2Service;
use Cake\Utility\Text;

/**
 * Uploads Controller
 *
 * @property \App\Model\Table\UploadsTable $Uploads
 * @method \App\Model\Entity\Chamber[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UploadsController extends AppController
{
    /**
     * Initialize controller
     */
    public function initialize(): void
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
//    public function index()
//    {
//        $this->request->allowMethod(['get']);
//
//        $limit = $this->request->getQuery('limit');
//        if ($limit === 'all') {
//            $Uploads = $this->Uploads->find('all')->toArray();
//            return $this->response->withType('application/json')
//                ->withStringBody(json_encode([
//                    'info' => [
//                        'limit' => 'all',
//                        'offset' => 0,
//                        'total' => sizeof($Uploads)
//                    ],
//                    'results' => $Uploads,
//                ]))
//                ->withStatus(200);
//        } else {
//            $Uploads = $this->paginate($this->Uploads);
//            return $this->response->withType('application/json')
//                ->withStringBody(json_encode([
//                    'info' => [
//                        'limit' => $this->Paginator->getPagingParams()['Uploads']['perPage'],
//                        'offset' => $this->Paginator->getPagingParams()['Uploads']['start'] - 1,
//                        'total' => $this->Paginator->getPagingParams()['Uploads']['count']
//                    ],
//                    'results' => $Uploads,
//                ]))
//                ->withStatus(200);
//        }
//    }

    /**
     * View method
     *
     * @param string|null $id Chamber id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
//    public function view($id)
//    {
//        $this->request->allowMethod(['get']);
//        $chamber = $this->Uploads->get($id);
//        return $this->response->withType('application/json')
//            ->withStringBody(json_encode(['chamber' => $chamber]))
//            ->withStatus(200);
//    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function getSignedUpload(R2Service $r2Service)
    {
        $this->request->allowMethod(['get']);
        $key = $this->getRequest()->getQuery('key');

        if (empty($key)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'error',
                    'error' => 'Key is required'
                ]))
                ->withStatus(400);
        }

        $uuid = Text::uuid();
        $uploadPath = $uuid . '/' . $key;
        $r2Key = 'uploads/' . $uploadPath;
        $presignedUrl = $r2Service->generateSignedPutUrl($r2Key);

        return $this->response->withType('application/json')
            ->withStringBody(json_encode([
                'message' => 'success',
                'uploadUrl' => $presignedUrl,
                'uploadPath' => $r2Key,
                'uuid' => $uuid
            ]))
            ->withStatus(200);
    }

    /**
     * Edit method
     *
     * @param string|null $id Chamber id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
//    public function edit($id)
//    {
//        $this->request->allowMethod(['patch', 'put']);
//        $chamber = $this->Uploads->get($id);
//        $chamber = $this->Uploads->patchEntity($chamber, $this->request->getData());
//        if ($this->Uploads->save($chamber)) {
//            return $this->response->withType('application/json')
//                ->withStringBody(json_encode([
//                    'message' => 'success',
//                    'chamber' => $chamber
//                ]))
//                ->withStatus(200);
//        } else {
//            return $this->response->withType('application/json')
//                ->withStringBody(json_encode($chamber->getErrors()))
//                ->withStatus(400);
//        }
//    }

    /**
     * Delete method
     *
     * @param string|null $id Chamber id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
//    public function delete($id)
//    {
//        $this->request->allowMethod(['delete']);
//        $chamber = $this->Uploads->get($id);
//        if ($this->Uploads->delete($chamber)) {
//            return $this->response->withType('application/json')
//                ->withStringBody(json_encode([
//                    'message' => 'success',
//                ]))
//                ->withStatus(200);
//        } else {
//            return $this->response->withType('application/json')
//                ->withStringBody(json_encode($chamber->getErrors()))
//                ->withStatus(400);
//        }
//    }
}