<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * AddressTypes Controller
 *
 * @property \App\Model\Table\AddressTypesTable $AddressTypes
 * @method \App\Model\Entity\AddressType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AddressTypesController extends AppController
{
    /**
     * Initialize controller
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->request->allowMethod(['get']);

        $limit = $this->request->getQuery('limit');
        if ($limit === 'all') {
            $addressTypes = $this->AddressTypes->find('all')->toArray();
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'info' => [
                        'limit' => 'all',
                        'offset' => 0,
                        'total' => sizeof($addressTypes)
                    ],
                    'results' => $addressTypes,
                ]))
                ->withStatus(200);
        } else {
            $addressTypes = $this->paginate($this->AddressTypes);
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'info' => [
                        'limit' => $this->Paginator->getPagingParams()['AddressTypes']['perPage'],
                        'offset' => $this->Paginator->getPagingParams()['AddressTypes']['start'] - 1,
                        'total' => $this->Paginator->getPagingParams()['AddressTypes']['count']
                    ],
                    'results' => $addressTypes,
                ]))
                ->withStatus(200);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Address Type id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $addressType = $this->AddressTypes->get($id);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['addressType' => $addressType]))
            ->withStatus(200);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);
        $addressType = $this->AddressTypes->newEntity($this->request->getData());
        if ($this->AddressTypes->save($addressType)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                    'addressType' => $addressType
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($addressType->getErrors()))
                ->withStatus(400);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Address Type id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        $this->request->allowMethod(['patch', 'put']);
        $addressType = $this->AddressTypes->get($id);
        $addressType = $this->AddressTypes->patchEntity($addressType, $this->request->getData());
        if ($this->AddressTypes->save($addressType)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                    'addressType' => $addressType
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($addressType->getErrors()))
                ->withStatus(400);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Address Type id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id)
    {
        $this->request->allowMethod(['delete']);
        $addressType = $this->AddressTypes->get($id);
        if ($this->AddressTypes->delete($addressType)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($addressType->getErrors()))
                ->withStatus(400);
        }
    }
}