<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * ActionTypes Controller
 *
 * @property \App\Model\Table\ActionTypesTable $ActionTypes
 * @method \App\Model\Entity\ActionType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ActionTypesController extends AppController
{
    /**
     * Initialize controller
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->request->allowMethod(['get']);

        $limit = $this->request->getQuery('limit');
        if ($limit === 'all') {
            $actionTypes = $this->ActionTypes->find('all')->toArray();
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'info' => [
                        'limit' => 'all',
                        'offset' => 0,
                        'total' => sizeof($actionTypes)
                    ],
                    'results' => $actionTypes,
                ]))
                ->withStatus(200);
        } else {
            $actionTypes = $this->paginate($this->ActionTypes);
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'info' => [
                        'limit' => $this->Paginator->getPagingParams()['ActionTypes']['perPage'],
                        'offset' => $this->Paginator->getPagingParams()['ActionTypes']['start'] - 1,
                        'total' => $this->Paginator->getPagingParams()['ActionTypes']['count']
                    ],
                    'results' => $actionTypes,
                ]))
                ->withStatus(200);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Action Type id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $actionType = $this->ActionTypes->get($id);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['actionType' => $actionType]))
            ->withStatus(200);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);
        $actionType = $this->ActionTypes->newEntity($this->request->getData());
        if ($this->ActionTypes->save($actionType)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                    'actionType' => $actionType
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($actionType->getErrors()))
                ->withStatus(400);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Action Type id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        $this->request->allowMethod(['patch', 'put']);
        $actionType = $this->ActionTypes->get($id);
        $actionType = $this->ActionTypes->patchEntity($actionType, $this->request->getData());
        if ($this->ActionTypes->save($actionType)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                    'actionType' => $actionType
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($actionType->getErrors()))
                ->withStatus(400);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Action Type id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id)
    {
        $this->request->allowMethod(['delete']);
        $actionType = $this->ActionTypes->get($id);
        if ($this->ActionTypes->delete($actionType)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($actionType->getErrors()))
                ->withStatus(400);
        }
    }
}