<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * Legislators Controller
 *
 * @property \App\Model\Table\LegislatorsTable $Legislators
 * @method \App\Model\Entity\Legislator[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LegislatorsController extends AppController
{
    /**
     * Initialize controller
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Paginator');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->request->allowMethod(['get']);

        $limit = $this->request->getQuery('limit');
        if ($limit === 'all') {
            $legislators = $this->Legislators->find('all')->toArray();
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'info' => [
                        'limit' => 'all',
                        'offset' => 0,
                        'total' => sizeof($legislators)
                    ],
                    'results' => $legislators,
                ]))
                ->withStatus(200);
        } else {
            $legislators = $this->paginate($this->Legislators);
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'info' => [
                        'limit' => $this->Paginator->getPagingParams()['Legislators']['perPage'],
                        'offset' => $this->Paginator->getPagingParams()['Legislators']['start'] - 1,
                        'total' => $this->Paginator->getPagingParams()['Legislators']['count']
                    ],
                    'results' => $legislators,
                ]))
                ->withStatus(200);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Legislator id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $legislator = $this->Legislators->get($id);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['legislator' => $legislator]))
            ->withStatus(200);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);
        $legislator = $this->Legislators->newEntity($this->request->getData());
        if ($this->Legislators->save($legislator)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                    'legislator' => $legislator
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($legislator->getErrors()))
                ->withStatus(400);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Legislator id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        $this->request->allowMethod(['patch', 'put']);
        $legislator = $this->Legislators->get($id);
        $legislator = $this->Legislators->patchEntity($legislator, $this->request->getData());
        if ($this->Legislators->save($legislator)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                    'legislator' => $legislator
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($legislator->getErrors()))
                ->withStatus(400);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Legislator id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id)
    {
        $this->request->allowMethod(['delete']);
        $legislator = $this->Legislators->get($id);
        if ($this->Legislators->delete($legislator)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'success',
                ]))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($legislator->getErrors()))
                ->withStatus(400);
        }
    }
}