<?php

namespace App\Controller;


use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\Routing\Router;

/**
 * Ratings Controller
 *
 * @property \App\Model\Table\RatingsTable $Ratings
 *
 * @method \App\Model\Entity\Rating[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RatingsController extends AppController
{

    public function __construct(ServerRequest $request = null, Response $response = null, $name = null, \Cake\Event\EventManager $eventManager = null)
    {
        parent::__construct($request, $response, $name, $eventManager);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Chambers', 'LegislativeSessions', 'Actions', 'Positions'],
            'fields' => [
                'Ratings.id',
                'Ratings.name',
                'LegislativeSessions.id',
                'LegislativeSessions.name',
                'Chambers.id',
                'Chambers.name',
                'Actions.id',
                'Actions.name',
                'Positions.id',
                'Positions.name'
            ],
            'order' => [
                'Chambers.name' => 'ASC',
                'Ratings.sort_order' => 'ASC'
            ],
            'sortableFields' => [
                'LegislativeSessions.name',
                'Chambers.name',
                'Ratings.name',
                'Actions.name'
            ]
        ];
        $ratings = $this->paginate($this->Ratings);

        $this->set(compact('ratings'));
    }

    /**
     * View method
     *
     * @param string|null $id Rating id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $rating = $this->Ratings->get($id, [
            'contain' => ['Actions', 'Chambers', 'LegislativeSessions', 'RatingTypes', 'Positions'],
            'fields' => [
                'Ratings.id',
                'Ratings.legislative_session_id',
                'Ratings.name',
                'Ratings.description',
                'Ratings.fiscal_note',
                'Ratings.digest',
                'Ratings.vote_recommendation_notes',
                'Ratings.vote_references',
                'Ratings.record_vote',
                'Ratings.rating_weight',
                'Ratings.slug',
                'Ratings.legiscan_bill_id',
                'Ratings.legiscan_bill_vote_id',
                'Ratings.created',
                'Ratings.modified',
                'Ratings.is_enabled',
                'Ratings.sort_order',
                'LegislativeSessions.id',
                'LegislativeSessions.name',
                'RatingTypes.id',
                'RatingTypes.name',
                'Chambers.id',
                'Chambers.name',
                'Actions.id',
                'Actions.name',
                'Actions.description',
                'Positions.id',
                'Positions.name'
            ]
        ]);

        $records = $this->Ratings->Records->find('all', [
            'contain' => ['Actions', 'Legislators'],
            'fields' => [
                'Records.id',
                'Records.is_chair',
                'Actions.description',
                'Legislators.journal_name'
            ],
            'conditions' => [
                'Records.rating_id' => $id
            ],
            'order' => [
                'Legislators.journal_name' => 'ASC'
            ]
        ]);

        $criterias = $this->fetchTable('Criterias')->find('all', [
            'order' => [
                'Criterias.sort_order' => 'ASC'
            ]
        ])->all();

        $criteriaDetails = $this->getCriteriaDetails();
        $ratingCriterias = $this->getRatingCriteriaDetailMapping($rating->legiscan_bill_id);

        $floorReports = $this->Ratings->FloorReports->find('all', [
            'fields' => [
                'FloorReports.id',
                'FloorReports.report_date',
                'FloorReports.is_approved',
            ],
            'join' => [
                [
                    'table' => 'floor_reports_ratings',
                    'alias' => 'FloorReportsRatings',
                    'type' => 'INNER',
                    'conditions' => [
                        'FloorReportsRatings.floor_report_id = FloorReports.id',
                        'FloorReportsRatings.rating_id' => $id
                    ]
                ]
            ],
            'order' => [
                'FloorReports.report_date' => 'ASC'
            ],
        ])->toArray();
        $this->set(compact('rating', 'records', 'criterias', 'criteriaDetails', 'ratingCriterias', 'floorReports'));
    }

    public function getCriteriaDetails() : array
    {
        $results = $this->fetchTable('CriteriaDetails')->find('all')->all();

        $criteriaDetails = [];
        foreach($results as $result) {
            $criteriaDetails[$result->id] = $result->name;
        }

        return $criteriaDetails;
    }

    public function getRatingCriteriaDetailMapping($legiscan_bill_id): array
    {
        if(empty($legiscan_bill_id)) {
            return [];
        }
        $results = $this->Ratings->LegiscanBills->RatingCriterias->find('all', [
            'conditions' => [
                'RatingCriterias.legiscan_bill_id' => $legiscan_bill_id
            ]
        ])->all();

        $criteriaToDetails = [];
        foreach ($results as $result) {
            $criteriaToDetails[$result->criteria_id] = $result->criteria_detail_id;
        }

        return $criteriaToDetails;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $billChamber = (!empty($this->request->getQuery('bill_chamber')) ? $this->request->getQuery('bill_chamber') : null);
        $billUUID = (!empty($this->request->getQuery('bill_uuid')) ? $this->request->getQuery('bill_uuid') : null);
        $legiscanBillId = (!empty($this->request->getQuery('legiscan_bill_id')) ? $this->request->getQuery('legiscan_bill_id') : null);
        $legiscanRollCallId = (!empty($this->request->getQuery('legiscan_bill_vote_id')) ? $this->request->getQuery('legiscan_bill_vote_id') : null);
        $rating = $this->Ratings->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $rating = $this->Ratings->patchEntity($rating, $data);
            if ($this->Ratings->save($rating)) {
                $this->Flash->success(__('The rating has been saved.'));
                $data['bill_chamber'] = $billChamber;
                $data['bill_uuid'] = $billUUID;
                return $this->postRatingSave($rating, $data, ['controller' => 'Records', 'action' => 'bulkImport', $rating->id]);
            }
            $this->Flash->error(__('The rating could not be saved. Please, try again.'));
        }
        $actions = $this->getActionsByType();
        $chambers = $this->Ratings->Chambers->find('list', ['limit' => 200]);
        $legislativeSessions = $this->Ratings->LegislativeSessions->find('list', ['limit' => 200]);
        $ratingTypes = $this->Ratings->RatingTypes->find('list', ['limit' => 200]);
        $positions = $this->Ratings->Positions->find('list');

        if (isset($legiscanRollCallId)) {
            $legiscanBillVote = $this->fetchTable('LegiscanBillVotes')->get($legiscanRollCallId);
            $legiscanChamber = $this->Ratings->Chambers->find('all', [
                'conditions' => [
                    'Chambers.legiscan_chamber_id' => $legiscanBillVote['legiscan_bill_vote_chamber_id']
                ]
            ])->first();
            $defaultChamberId = $legiscanChamber['id'];

            $legiscanSession = $this->Ratings->LegislativeSessions->find('all', [
                'conditions' => [
                    'LegislativeSessions.legiscan_session_id' => $legiscanBillVote['legiscan_session_id']
                ]
            ])->first();
            $defaultSessionId = $legiscanSession['id'];

            $legiscanBill = $this->fetchTable('LegiscanBills')->get($legiscanBillVote['legiscan_bill_id']);
        } else if (isset($legiscanBillId)) {
            $legiscanBillVote = [];
            $legiscanBill = $this->fetchTable('LegiscanBills')->get($legiscanBillId);
            $legiscanChamber = $this->Ratings->Chambers->find('all', [
                'conditions' => [
                    'Chambers.legiscan_chamber_id' => $legiscanBill['legiscan_chamber_id']
                ]
            ])->first();
            $defaultChamberId = $legiscanChamber['id'];

            $legiscanSession = $this->Ratings->LegislativeSessions->find('all', [
                'conditions' => [
                    'LegislativeSessions.legiscan_session_id' => $legiscanBill['legiscan_session_id']
                ]
            ])->first();
            $defaultSessionId = $legiscanSession['id'];
        } else {
            $legiscanBillVote = [];
            $defaultChamberId = null;
            $defaultSessionId = null;
            $legiscanBill = [];
            $legiscanSession = [];
        }
        $this->set(compact('rating', 'chambers', 'legislativeSessions', 'ratingTypes', 'actions', 'positions',
            'legiscanBillVote', 'defaultChamberId', 'defaultSessionId', 'legiscanBill', 'legiscanSession'));
    }

    private function postRatingSave($rating, $data, $defaultRedirect): \Cake\Http\Response
    {
        // Link Rating to Bill Tracking record
        if (isset($data['bill_chamber']) && isset($data['bill_uuid'])) {
            $billChamber = $data['bill_chamber'];
            $billUUID = $data['bill_uuid'];
            // update Bill record
            $bill = $this->fetchTable('Bills')->find('all', [
                'conditions' => [
                    'Bills.id' => $billUUID
                ]
            ])->first();
            $field = ($billChamber === 'L' ? 'house_rating_id' : 'senate_rating_id');
            $bill[$field] = $rating->id;
            if($this->fetchTable('Bills')->save($bill)) {
                $this->Flash->success(__('Successfully linked Rating to Bill Tracking record!'));
            } else {
                $this->Flash->error(__('Failed to link Rating to Bill Tracking record.'));
            }
        }

        // LegiScan Rating tenants
        if (isset($data['legiscan_bill_vote_id'])) {
            if ($this->upsertLegiscanVotes($rating->id)) {
                if(isset($data['bill_uuid'])) {
                    $this->saveBillVoteText($billChamber, $billUUID, $data['legiscan_bill_vote_id'], $rating->id);
                }
                $this->Flash->success(__('Successfully imported LegiScan record votes!'));
                return $this->redirect(['controller' => 'Records', 'action' => 'bulkedit', $rating->id]);
            } else {
                $this->Flash->warning(__('No LegiScan record votes found.'));
                return $this->redirect(['controller' => 'Records', 'action' => 'bulkImport', $rating->id]);
            }
        }
        // Floor Report tenants
        else if (isset($data['legiscan_bill_id'])) {
            $editCriteriaUrl = Router::url(['controller' => 'rating-criterias', 'action' => 'bulkedit',], true);
            $editCriteriaUrl .= '/' . $rating->legislative_session_id;
            $editCriteriaUrl .= '/' . $rating->id;
            return $this->redirect($editCriteriaUrl);
        } else {
            return $this->redirect($defaultRedirect);
        }
    }

    public function saveBillVoteText($billChamber, $billUUID, $legiscanVoteId, $ratingId): void
    {
        // Fetch the bill
        $bill = $this->fetchTable('Bills')->get($billUUID);

        // Fetch the Legiscan Bill Vote
        $legiscanBillVote = $this->fetchTable('LegiscanBillVotes')->get($legiscanVoteId);
        $voteDescription = $legiscanBillVote->roll_call_desc;

        // Fetch records based on rating ID and contain required associations
        $records = $this->fetchTable('Records')->find('all', [
            'contain' => ['Legislators', 'Actions'],
            'conditions' => ['Records.rating_id' => $ratingId],
            'order' => ['Legislators.journal_name' => 'ASC']
        ])->all();

        // Group records by action name
        $groupedRecords = $this->groupRecordsByAction($records);

        // Generate vote text
        $voteText = $this->generateVoteText($groupedRecords, $voteDescription);

        // Assign vote text to the appropriate field based on the bill chamber
        $this->assignVoteTextToBill($bill, $billChamber, $voteText);

        // Save the bill and provide feedback
        $this->saveBillWithFeedback($bill);
    }

    private function groupRecordsByAction($records): array
    {
        $groupedRecords = [];
        foreach ($records as $record) {
            $actionName = $record->action->description;
            $groupedRecords[$actionName][] = $record;
        }
        return $groupedRecords;
    }

    private function generateVoteText(array $groupedRecords, string $voteDescription): string
    {
        $voteText = '<u>' . $voteDescription . '</u><br />';
        foreach ($groupedRecords as $actionName => $actionRecords) {
            $legislatorNames = array_map(function ($record) {
                return $record->legislator->journal_name;
            }, $actionRecords);
            $voteText .= sprintf("<b>%s</b>: %s<br /><br />", $actionName, implode('; ', $legislatorNames));
        }

        // remove trailing <br /><br />
        $voteText = substr($voteText, 0, -12);

        return trim($voteText);
    }


    private function assignVoteTextToBill($bill, $billChamber, $voteText): void
    {
        if ($billChamber === 'L') {
            $bill->house_rating_votes = $voteText;
        } else {
            $bill->senate_rating_votes = $voteText;
        }
    }

    private function saveBillWithFeedback($bill): void
    {
        if ($this->fetchTable('Bills')->save($bill)) {
            $this->Flash->success(__('Successfully saved Bill Vote Text!'));
        } else {
            $this->Flash->error(__('Failed to save Bill Vote Text.'));
        }
    }

    private function upsertLegiscanVotes($ratingId): bool
    {
        // delete previous records
        $this->Ratings->Records->deleteAll(['Records.rating_id' => $ratingId]);

        // find record votes
        $selectQuery = $this->Ratings
            ->query()
            ->select([
                'id' => 'UUID()',
                'tenant_id' => 'Tenants.id',
                'action_id' => 'Actions.id',
                'legislator_id' => 'Legislators.id',
                'rating_id' => 'Ratings.id',
                'is_chair' => '0',
                'has_statement' => '0',
                'created' => 'NOW()',
                'modified' => 'NOW()'
            ])
            ->from('ratings Ratings')
            ->join([
                'table' => 'legiscan_bill_vote_details',
                'alias' => 'LegiscanBillVoteDetails',
                'type' => 'INNER',
                'conditions' => [
                    'Ratings.legiscan_bill_vote_id = LegiscanBillVoteDetails.legiscan_bill_vote_id'
                ],
            ])
            ->join([
                'table' => 'rating_types',
                'alias' => 'RatingTypes',
                'type' => 'INNER',
                'conditions' => [
                    'RatingTypes.id = Ratings.rating_type_id'
                ],
            ])
            ->join([
                'table' => 'actions',
                'alias' => 'Actions',
                'type' => 'INNER',
                'conditions' => [
                    'Actions.legiscan_action_id = LegiscanBillVoteDetails.legiscan_vote_id',
                    'Actions.rating_type_id = RatingTypes.id'
                ],
            ])
            ->join([
                'table' => 'people',
                'alias' => 'People',
                'type' => 'INNER',
                'conditions' => [
                    'People.legiscan_people_id = LegiscanBillVoteDetails.legiscan_people_id'
                ],
            ])
            ->join([
                'table' => 'legislators',
                'alias' => 'Legislators',
                'type' => 'INNER',
                'conditions' => [
                    'Legislators.person_id = People.id',
                    'Legislators.chamber_id = Ratings.chamber_id'
                ],
            ])
            ->join([
                'table' => 'legislative_sessions',
                'alias' => 'LegislativeSessions',
                'type' => 'INNER',
                'conditions' => [
                    'LegislativeSessions.legiscan_session_id = LegiscanBillVoteDetails.legiscan_session_id',
                    'LegislativeSessions.id = Legislators.legislative_session_id',
                    'LegislativeSessions.id = Ratings.legislative_session_id'
                ],
            ])
            ->join([
                'table' => 'tenants',
                'alias' => 'Tenants',
                'type' => 'INNER',
                'conditions' => [
                    'Tenants.id = Actions.tenant_id',
                    'Tenants.id = Ratings.tenant_id',
                    'Tenants.id = People.tenant_id',
                    'Tenants.id = Legislators.tenant_id',
                    'Tenants.id = LegislativeSessions.tenant_id'
                ],
            ])
            ->where([
                'Ratings.id' => $ratingId
            ]);

        // insert new records
        $insertQuery = $this->Ratings->Records->query();
        $insertQuery->insert([
            'id',
            'tenant_id',
            'action_id',
            'legislator_id',
            'rating_id',
            'is_chair',
            'has_statement',
            'created',
            'modified'
        ])
            ->values($selectQuery)
            ->execute();

        // validate records exist
        $recordCheck = $this->Ratings->Records->find('all', [
            'conditions' => [
                'Records.rating_id' => $ratingId
            ]
        ]);

        return $recordCheck->count() > 0;
    }

    /**
     * Edit method
     *
     * @param string|null $id Rating id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $rating = $this->Ratings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $rating = $this->Ratings->patchEntity($rating, $data);
            if ($this->Ratings->save($rating)) {
                $this->Flash->success(__('The rating has been saved.'));
                return $this->postRatingSave($rating, $data, ['controller' => 'Ratings', 'action' => 'view', $rating->id]);
            }
            $this->Flash->error(__('The rating could not be saved. Please, try again.'));
        }
        $actions = $this->getActionsByType();
        $chambers = $this->Ratings->Chambers->find('list', ['limit' => 200]);
        $legislativeSessions = $this->Ratings->LegislativeSessions->find('list', ['limit' => 200]);
        $ratingTypes = $this->Ratings->RatingTypes->find('list', ['limit' => 200]);
        $positions = $this->Ratings->Positions->find('list');
        $this->set(compact('rating', 'chambers', 'legislativeSessions', 'ratingTypes', 'actions', 'positions'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Rating id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rating = $this->Ratings->get($id, ['contain' => ['Chambers']]);

        // Unlink Rating from Bill Tracking record
        $ratingChamberType = $rating->chamber->type;
        $legiscanBillId = $rating->legiscan_bill_id;

        if (isset($ratingChamberType) && isset($legiscanBillId)) {
            // update Bill record
            $bill = $this->fetchTable('Bills')->find('all', [
                'conditions' => [
                    'Bills.legiscan_bill_id' => $legiscanBillId
                ]
            ])->first();
            $chamber_name = ($ratingChamberType === 'L' ? 'house' : 'senate');
            $field = strtolower($chamber_name . '_rating_id');
            $bill[$field] = null;
            if ($this->fetchTable('Bills')->save($bill)) {
                $this->Flash->success(__('Successfully unlinked Rating from Bill Tracking record!'));
            } else {
                $this->Flash->error(__('Failed to unlink Rating from Bill Tracking record.'));
            }
        }

        if ($this->Ratings->delete($rating)) {
            $this->Flash->success(__('The rating has been deleted.'));
        } else {
            $this->Flash->error(__('The rating could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * admin_bulkedit method
     *
     * @return void
     */
    public function bulkedit()
    {
        $selectedFilters = [];
        $sessionId = (!empty($this->request->getParam('sessionId')) ? $this->request->getParam('sessionId') : null);
        $chamber = (!empty($this->request->getParam('chamber')) ? $this->request->getParam('chamber') : null);

        if (empty($chamber)) {
            $currentTenantId = $this->getRequest()->getSession()->read('Account.currentTenantId');
            $chamber = (new ChambersController())->getDefault($currentTenantId);
        }

        // Filter options.
        $sessions = $this->Ratings->LegislativeSessions->find('list', ['limit' => '200']);
        $chambers = $this->Ratings->Chambers->find('list');
        $actions = $this->getActionsByType();
        $positions = $this->Ratings->Positions->find('list');
        $priorities = $this->Ratings->Priorities->find('list');

        // get first key from list, if not set
        if (empty($sessionId)) {
            $sessionId = key($sessions->toArray());
        }

        // Get selected filters.
        $selectedFilters['session'] = $sessionId;
        $selectedFilters['chamber'] = $chamber;

        // Ratings.
        $ratingsPriorities = $this->getRatingsPriorities();
        $ratings = $this->Ratings->find('all', [
            'contain' => ['RatingTypes'],
            'fields' => [
                'Ratings.id',
                'Ratings.rating_type_id',
                'Ratings.name',
                'Ratings.action_id',
                'Ratings.position_id',
                'Ratings.record_vote',
                'Ratings.slug',
                'Ratings.sort_order',
                'Ratings.is_enabled',
                'RatingTypes.name',
                'Ratings.rating_weight'],
            'conditions' => ['Ratings.legislative_session_id' => $sessionId, 'Ratings.chamber_id' => $chamber],
            'order' => ['RatingTypes.sort_order', 'Ratings.sort_order']])->all();

        // generate Vote Grid Export url
        $downloadUrl = Router::url([
            'controller' => 'ratings',
            'action' => 'export-grid',
            '?' => [
                'sessionId' => $sessionId
            ]
        ]);

        $this->set(compact('selectedFilters', 'sessions', 'ratings', 'chambers', 'actions', 'positions', 'downloadUrl', 'ratingsPriorities', 'priorities'));
    }

    public function getRatingsPriorities(): array
    {
        $allRatingsPriorities = [];
        $priorities = $this->Ratings->Priorities->find('all', [
            'contain' => [
                'RatingsPriorities'
            ]
        ])->toArray();

        foreach ($priorities as $priority) {
            if (is_array($priority['ratings_priorities'])) {
                foreach ($priority['ratings_priorities'] as $ratingPrioirties) {
                    $rating_id = $ratingPrioirties['rating_id'];
                    $allRatingsPriorities[$rating_id][] = $ratingPrioirties['priority_id'];
                }
            }
        }
        return $allRatingsPriorities;
    }

    public function getActionsByType()
    {

        $return = [];

        $actions = $this->Ratings->Actions->find('all',
            [
                'contain' => ['RatingTypes', 'ActionTypes'],
                'fields' => [
                    'Actions.id',
                    'Actions.name',
                    'Actions.description',
                    'RatingTypes.name'
                ],
                'conditions' => [
                    'ActionTypes.evaluation_code' => 'E'
                ]
            ])->all();

        foreach ($actions as $action) {

            $id = $action->id;
            $name = $action->name;
            $description = $action->description;
            $rating_type_name = $action->rating_type->name;

            $return[$rating_type_name][$id] = $name . ' - ' . $description;
        }

        return $return;
    }

    public function bulkUpdateRow()
    {
        $this->viewBuilder()->disableAutoLayout();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $record = $this->Ratings->get($data['id']);

            if (!empty($data['is_enabled'])) {
                $data['is_enabled'] = empty($data['is_enabled']) ? false : true;
            }

            if (isset($data['ratings_priorities'])) {
                $ratingsPriorities = [];

                if (is_array($data['ratings_priorities'])) {
                    foreach ($data['ratings_priorities'] as $tag) {
                        $ratingsPriorities[] = $tag;
                    }
                }

                unset($data['ratings_priorities']);
                $data['priorities'] = [
                    '_ids' => $ratingsPriorities,
                ];
            }

            $record = $this->Ratings->patchEntity($record, $data, [
                'associated' => ['Priorities' => ['onlyIds' => true]],
            ]);
            if ($this->Ratings->save($record)) {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($data))
                    ->withStatus(200);
            } else {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($record->getErrors()))
                    ->withStatus(400);
            }
        }
    }

    public function getRatingsAjax()
    {
        $this->viewBuilder()->setLayout('ajax');

        $chamber = $this->request->getQuery('chamber');
        if (empty($chamber)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'error' => [
                        'code' => '400',
                        'message' => 'chamber is a required parameter.']]))
                ->withStatus(400);
        }

        $inputSessionId = $this->request->getQuery('legislative_session_id');
        if (empty($inputSessionId)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'error' => [
                        'code' => '400',
                        'message' => 'legislative_session_id is a required parameter.']]))
                ->withStatus(400);
        }

        $data = $this->Ratings->find('list', [
            'conditions' => [
                'Ratings.chamber_id' => $chamber,
                'Ratings.legislative_session_id' => $inputSessionId],
            'fields' => ['Ratings.id', 'Ratings.name'],
            'order' => ['Ratings.sort_order' => 'asc']]);

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data))
            ->withStatus(200);
    }

    public function exportGrid()
    {
        $selectedFilters = [];
        $sessionId = (!empty($this->request->getQuery('sessionId')) ? $this->request->getQuery('sessionId') : null);

        // Filter options.
        $sessions = $this->Ratings->LegislativeSessions->find('list', ['limit' => '200']);

        // get first key from list, if not set
        if (empty($sessionId)) {
            $sessionId = key($sessions->toArray());
        }

        $selectedFilters['session'] = $sessionId;

        // generate Vote Grid Export url
        $currentTenantDomain = $this->getRequest()->getSession()->read('Account.currentTenantDomain');
        $isLocal = (str_contains((string)$currentTenantDomain, 'localhost') ? 1 : 0);

        $downloadPath = "/downloads/vote-grid";
        $downloadUrl = 'https://' . $currentTenantDomain . $downloadPath;
        if ($isLocal) {
            $downloadUrl = 'http://' . $currentTenantDomain . ':3001' . $downloadPath;
        }

        $this->set(compact('sessions', 'selectedFilters', 'downloadUrl'));
    }

    public function bulkLegiscanVoteImport() : void
    {
        if ($this->request->is('post')) {
            $sessionId = $this->request->getData('session');
            $this->processBulkLegiscanVoteImport($sessionId);
        } else {
            $selectedFilters = [];
            $sessionId = $this->request->getQuery('sessionId') ?? null;

            // Filter options.
            $sessions = $this->Ratings->LegislativeSessions->find('list', ['limit' => 200])->toArray();

            // Get first key from list, if not set
            if (empty($sessionId)) {
                $sessionId = key($sessions);
            }

            $selectedFilters['session'] = $sessionId;

            // Generate Vote Grid Export URL
            $currentTenantDomain = $this->getRequest()->getSession()->read('Account.currentTenantDomain');
            $isLocal = str_contains((string)$currentTenantDomain, 'localhost');

            $downloadPath = "/downloads/vote-grid";
            $downloadUrl = ($isLocal ? 'http://' . $currentTenantDomain . ':3001' : 'https://' . $currentTenantDomain) . $downloadPath;

            $this->set(compact('sessions', 'selectedFilters', 'downloadUrl'));
        }
    }

    public function processBulkLegiscanVoteImport($sessionId = null)
    {
        if (!$this->request->is('post')) {
            $this->Flash->error(__('Invalid request.'));
            return $this->redirect(['action' => 'bulkLegiscanVoteImport']);
        }

        if (!$sessionId) {
            $this->Flash->error(__('Session ID is required.'));
            return $this->redirect(['action' => 'bulkLegiscanVoteImport']);
        }

        $this->viewBuilder()->disableAutoLayout();
        $this->autoRender = false;

        $ratings = $this->Ratings->find('all', [
            'conditions' => ['Ratings.legislative_session_id' => $sessionId]
        ]);

        $logOutput[] = 'Bulk LegiScan Vote Import - ' . date('Y-m-d H:i:s') . "\n";
        $logOutput[] = 'Legislative Session ID: ' . $sessionId . "\n";

        $successCount = 0;
        $failureCount = 0;
        foreach ($ratings as $rating) {
            $status = $this->upsertLegiscanVotes($rating->id);
            if ($status) {
                $logOutput[] = 'Rating ID: ' . $rating->id . ' - Successfully imported LegiScan record votes!';
                $successCount++;
            } else {
                $logOutput[] = 'Rating ID: ' . $rating->id . ' - No LegiScan record votes found.';
                $failureCount++;
            }
        }

        $logFilePath = LOGS . 'bulk_legiscan_vote_import_' . date('Ymd_His') . '.log';
        file_put_contents($logFilePath, implode("\n", $logOutput));

        if ($successCount > 0) {
            $this->Flash->success(__('Successfully imported LegiScan record votes for {0} ratings.', $successCount));
        }

        if ($failureCount > 0) {
            $this->Flash->warning(__('No LegiScan record votes found for {0} ratings.', $failureCount));
        }

        return $this->redirect(['controller' => 'Ratings', 'action' => 'bulkLegiscanVoteImport', '?' => ['sessionId' => $sessionId]]);
    }

    public function getRatingsList($sessionId, $chamberId)
    {
        return $this->Ratings->find('list', [
            'fields' => ['Ratings.id', 'Ratings.name'],
            'conditions' => ['Ratings.chamber_id' => $chamberId, 'Ratings.legislative_session_id' => $sessionId],
            'order' => ['Ratings.name']]);
    }
}
