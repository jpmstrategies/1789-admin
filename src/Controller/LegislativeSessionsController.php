<?php

namespace App\Controller;


/**
 * LegislativeSessions Controller
 *
 * @property \App\Model\Table\LegislativeSessionsTable $LegislativeSessions
 *
 * @method \App\Model\Entity\LegislativeSession[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LegislativeSessionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $session = $this->request->getSession();
        $currentTenantDataSource = $session->read('Account.currentTenantDataSource');
        if ($currentTenantDataSource === 'legiscan') {
            $this->Flash->success(__('Note: The data below is curated weekly on Monday from LegiScan meaning you cannot add new records.'));
        }
        $this->paginate = [
            'contain' => [],
            'fields' => [
                'LegislativeSessions.id',
                'LegislativeSessions.name',
                'LegislativeSessions.legislature_number',
                'LegislativeSessions.session_type',
                'LegislativeSessions.special_session_number',
                'LegislativeSessions.is_enabled',
                'LegislativeSessions.show_scores',
            ]
        ];
        $legislativeSessions = $this->paginate($this->LegislativeSessions);

        $this->set(compact('legislativeSessions'));
    }

    /**
     * View method
     *
     * @param string|null $id Legislative Session id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $legislativeSession = $this->LegislativeSessions->get($id, [
            'contain' => [],
            'fields' => [
                'LegislativeSessions.id',
                'LegislativeSessions.name',
                'LegislativeSessions.year',
                'LegislativeSessions.legislature_number',
                'LegislativeSessions.session_type',
                'LegislativeSessions.special_session_number',
                'LegislativeSessions.speaker_journal_name',
                'LegislativeSessions.lt_gov_journal_name',
                'LegislativeSessions.slug',
                'LegislativeSessions.state_pk',
                'LegislativeSessions.os_pk',
                'LegislativeSessions.created',
                'LegislativeSessions.modified',
                'LegislativeSessions.is_enabled',
                'LegislativeSessions.show_scores',
            ]
        ]);

        $this->set('legislativeSession', $legislativeSession);
    }

    public function getLegislativeSessions()
    {
        return $this->LegislativeSessions->find('list', ['limit' => '200']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        $currentTenantDataSource = $session->read('Account.currentTenantDataSource');
        if ($currentTenantDataSource === 'legiscan') {
            $this->Flash->error(__('This functionality is not available when using LegiScan as the primary data source!'));
            return $this->redirect(['action' => 'index']);
        }

        $legislativeSession = $this->LegislativeSessions->newEmptyEntity();
        if ($this->request->is('post')) {
            $legislativeSession = $this->LegislativeSessions->patchEntity($legislativeSession, $this->request->getData());
            if ($this->LegislativeSessions->save($legislativeSession)) {
                $this->Flash->success(__('The legislative session has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legislative session could not be saved. Please, try again.'));
        }
        $this->set(compact('legislativeSession'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Legislative Session id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $legislativeSession = $this->LegislativeSessions->get($id, [
            'contain' => [],
            'fields' => [
                'LegislativeSessions.id',
                'LegislativeSessions.name',
                'LegislativeSessions.year',
                'LegislativeSessions.legislature_number',
                'LegislativeSessions.session_type',
                'LegislativeSessions.special_session_number',
                'LegislativeSessions.speaker_journal_name',
                'LegislativeSessions.lt_gov_journal_name',
                'LegislativeSessions.slug',
                'LegislativeSessions.state_pk',
                'LegislativeSessions.os_pk',
                'LegislativeSessions.created',
                'LegislativeSessions.modified',
                'LegislativeSessions.is_enabled',
                'LegislativeSessions.show_scores',
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $legislativeSession = $this->LegislativeSessions->patchEntity($legislativeSession, $this->request->getData());
            if ($this->LegislativeSessions->save($legislativeSession)) {
                $this->Flash->success(__('The legislative session has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legislative session could not be saved. Please, try again.'));
        }
        $this->set(compact('legislativeSession'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Legislative Session id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        try {
            $legislativeSession = $this->LegislativeSessions->get($id);

            if ($this->LegislativeSessions->delete($legislativeSession)) {
                $this->Flash->success(__('The legislative session has been deleted.'));
            } else {
                $this->Flash->error(__('The legislative session could not be deleted. Please, try again.'));
            }
        } catch (\Exception) {
            $this->Flash->error(__('The legislative session you are trying to delete is associated with other records. Please, try again after deleting the related records.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
