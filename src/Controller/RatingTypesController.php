<?php

namespace App\Controller;


/**
 * RatingTypes Controller
 *
 * @property \App\Model\Table\RatingTypesTable $RatingTypes
 *
 * @method \App\Model\Entity\RatingType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RatingTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [],
            'fields' => [
                'RatingTypes.id',
                'RatingTypes.name',
                'RatingTypes.sort_order',
                'RatingTypes.method',
            ],
            'order' => [
                'RatingTypes.sort_order'
            ],
            'sortableFields' => [
                'RatingTypes.name',
                'RatingTypes.sort_order',
                'RatingTypes.method'
            ]
        ];
        $ratingTypes = $this->paginate($this->RatingTypes);

        $this->set(compact('ratingTypes'));
    }

    public function getRatingTypes()
    {
        return $this->RatingTypes->find('list');
    }

    /**
     * View method
     *
     * @param string|null $id Rating Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ratingType = $this->RatingTypes->get($id, [
            'contain' => [],
            'fields' => [
                'RatingTypes.id',
                'RatingTypes.name',
                'RatingTypes.th_detail_name',
                'RatingTypes.th_vote_name',
                'RatingTypes.sort_order',
                'RatingTypes.created',
                'RatingTypes.modified',
                'RatingTypes.method',
            ]
        ]);

        $this->set('ratingType', $ratingType);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ratingType = $this->RatingTypes->newEmptyEntity();
        if ($this->request->is('post')) {
            $ratingType = $this->RatingTypes->patchEntity($ratingType, $this->request->getData());
            if ($this->RatingTypes->save($ratingType)) {
                $this->Flash->success(__('The rating type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rating type could not be saved. Please, try again.'));
        }
        $this->set(compact('ratingType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Rating Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $ratingType = $this->RatingTypes->get($id, [
            'contain' => [],
            'fields' => [
                'RatingTypes.id',
                'RatingTypes.name',
                'RatingTypes.th_detail_name',
                'RatingTypes.th_vote_name',
                'RatingTypes.sort_order',
                'RatingTypes.created',
                'RatingTypes.modified',
                'RatingTypes.method',
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ratingType = $this->RatingTypes->patchEntity($ratingType, $this->request->getData());
            if ($this->RatingTypes->save($ratingType)) {
                $this->Flash->success(__('The rating type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The rating type could not be saved. Please, try again.'));
        }
        $this->set(compact('ratingType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Rating Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        try {
            $ratingType = $this->RatingTypes->get($id);
            if ($this->RatingTypes->delete($ratingType)) {
                $this->Flash->success(__('The rating type has been deleted.'));
            } else {
                $this->Flash->error(__('The rating type could not be deleted. Please, try again.'));
            }
        } catch (\Exception) {
            $this->Flash->error(__('The rating type you are trying to delete is associated with other records. Please, try again after deleting the related records.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
