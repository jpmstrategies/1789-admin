<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * LegiscanBillVoteDetails Controller
 *
 * @property \App\Model\Table\LegiscanBillVoteDetailsTable $LegiscanBillVoteDetails
 * @method \App\Model\Entity\LegiscanBillVoteDetail[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LegiscanBillVoteDetailsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'LegiscanBills',
                'LegiscanBillVotes',
                'LegiscanParties',
                'LegiscanSessions',
                'LegiscanChambers',
                'LegiscanCurrentChambers'],
        ];
        $legiscanBillVoteDetails = $this->paginate($this->LegiscanBillVoteDetails);

        $this->set(compact('legiscanBillVoteDetails'));
    }

    /**
     * View method
     *
     * @param string|null $id Legiscan Bill Vote Detail id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $legiscanBillVoteDetail = $this->LegiscanBillVoteDetails->get($id, [
            'contain' => [
                'LegiscanBills',
                'LegiscanBillVotes',
                'LegiscanParties',
                'LegiscanSessions',
                'LegiscanChambers',
                'LegiscanCurrentChambers'],
        ]);

        $this->set(compact('legiscanBillVoteDetail'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $legiscanBillVoteDetail = $this->LegiscanBillVoteDetails->newEmptyEntity();
        if ($this->request->is('post')) {
            $legiscanBillVoteDetail = $this->LegiscanBillVoteDetails->patchEntity($legiscanBillVoteDetail, $this->request->getData());
            if ($this->LegiscanBillVoteDetails->save($legiscanBillVoteDetail)) {
                $this->Flash->success(__('The legiscan bill vote detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan bill vote detail could not be saved. Please, try again.'));
        }
        $legiscanBills = $this->LegiscanBillVoteDetails->LegiscanBills->find('list', ['limit' => 200]);
        $legiscanBillVotes = $this->LegiscanBillVoteDetails->LegiscanBillVotes->find('list', ['limit' => 200]);
        $legiscanParties = $this->LegiscanBillVoteDetails->LegiscanParties->find('list', ['limit' => 200]);
        $legiscanSessions = $this->LegiscanBillVoteDetails->LegiscanSessions->find('list', ['limit' => 200]);
        $legiscanChambers = $this->LegiscanBillVoteDetails->LegiscanChambers->find('list', ['limit' => 200]);
        $legiscanCurrentChambers = $this->LegiscanBillVoteDetails->LegiscanCurrentChambers->find('list', ['limit' => 200]);
        $this->set(compact('legiscanBillVoteDetail', 'legiscanBills', 'legiscanBillVotes', 'legiscanParties', 'legiscanSessions', 'legiscanChambers', 'legiscanCurrentChambers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Legiscan Bill Vote Detail id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $legiscanBillVoteDetail = $this->LegiscanBillVoteDetails->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $legiscanBillVoteDetail = $this->LegiscanBillVoteDetails->patchEntity($legiscanBillVoteDetail, $this->request->getData());
            if ($this->LegiscanBillVoteDetails->save($legiscanBillVoteDetail)) {
                $this->Flash->success(__('The legiscan bill vote detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan bill vote detail could not be saved. Please, try again.'));
        }
        $legiscanBills = $this->LegiscanBillVoteDetails->LegiscanBills->find('list', ['limit' => 200]);
        $legiscanBillVotes = $this->LegiscanBillVoteDetails->LegiscanBillVotes->find('list', ['limit' => 200]);
        $legiscanParties = $this->LegiscanBillVoteDetails->LegiscanParties->find('list', ['limit' => 200]);
        $legiscanSessions = $this->LegiscanBillVoteDetails->LegiscanSessions->find('list', ['limit' => 200]);
        $legiscanChambers = $this->LegiscanBillVoteDetails->LegiscanChambers->find('list', ['limit' => 200]);
        $legiscanCurrentChambers = $this->LegiscanBillVoteDetails->LegiscanCurrentChambers->find('list', ['limit' => 200]);
        $this->set(compact('legiscanBillVoteDetail', 'legiscanBills', 'legiscanBillVotes', 'legiscanParties', 'legiscanSessions', 'legiscanChambers', 'legiscanCurrentChambers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Legiscan Bill Vote Detail id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $legiscanBillVoteDetail = $this->LegiscanBillVoteDetails->get($id);
        if ($this->LegiscanBillVoteDetails->delete($legiscanBillVoteDetail)) {
            $this->Flash->success(__('The legiscan bill vote detail has been deleted.'));
        } else {
            $this->Flash->error(__('The legiscan bill vote detail could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}