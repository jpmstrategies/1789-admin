<?php

namespace App\Controller;


use Cake\I18n\FrozenTime;

/**
 * RatingFields Controller
 *
 * @property \App\Model\Table\RatingFieldsTable $RatingFields
 *
 * @method \App\Model\Entity\RatingField[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RatingFieldsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [],
            'fields' => [
                'RatingFields.id',
                'RatingFields.name',
                'RatingFields.json_path',
                'RatingFields.sort_order'
            ],
            'order' => [
                'RatingFields.sort_order'
            ]
        ];
        $ratingFields = $this->paginate($this->RatingFields);

        $this->set(compact('ratingFields'));
    }

    /**
     * View method
     *
     * @param string|null $id Rating Field id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ratingField = $this->RatingFields->get($id, [
            'contain' => [],
            'fields' => [
                'RatingFields.id',
                'RatingFields.name',
                'RatingFields.json_path',
                'RatingFields.type',
                'RatingFields.sort_order',
                'RatingFields.created',
                'RatingFields.modified',
            ]
        ]);

        $this->set('ratingField', $ratingField);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ratingField = $this->RatingFields->newEmptyEntity();
        if ($this->request->is('post')) {
            $ratingField = $this->RatingFields->patchEntity($ratingField, $this->request->getData());
            if ($this->RatingFields->save($ratingField)) {
                $this->Flash->success(__('The Rating Field has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Rating Field could not be saved. Please, try again.'));
        }
        $this->set(compact('ratingField'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Rating Field id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $ratingField = $this->RatingFields->get($id, [
            'contain' => [],
            'fields' => [
                'RatingFields.id',
                'RatingFields.name',
                'RatingFields.json_path',
                'RatingFields.type',
                'RatingFields.sort_order',
                'RatingFields.created',
                'RatingFields.modified',
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ratingField = $this->RatingFields->patchEntity($ratingField, $this->request->getData());
            if ($this->RatingFields->save($ratingField)) {
                $this->Flash->success(__('The Rating Field has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Rating Field could not be saved. Please, try again.'));
        }
        $this->set(compact('ratingField'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Rating Field id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        try {
            $ratingField = $this->RatingFields->get($id);
            if ($this->RatingFields->delete($ratingField)) {
                $this->Flash->success(__('The Rating Field has been deleted.'));
            } else {
                $this->Flash->error(__('The Rating Field could not be deleted. Please, try again.'));
            }
        } catch (\Exception) {
            $this->Flash->error(__('The Rating Field you are trying to delete is associated with other records. Please, try again after deleting the related records.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function export()
    {
        $results = $this->RatingFields->find('all', [
            'contain' => ['Tenants'],
            'fields' => [
                'tenantId' => 'Tenants.id_public',
                'tenantName' => 'Tenants.name',
                'name' => 'RatingFields.name',
                'jsonPath' => 'RatingFields.json_path',
                'type' => 'RatingFields.type',
                'sortOrder' => 'RatingFields.sort_order']])->toArray();

        $ratingFields = [];
        foreach ($results as $result) {

            $temp = [];

            $temp['name'] = $result['name'];
            $temp['jsonPath'] = $result['jsonPath'];
            $temp['type'] = $result['type'];
            $temp['sortOrder'] = $result['sortOrder'];

            array_push($ratingFields, $temp);
        }

        $exports = [];
        $exports['tenantId'] = $this->getRequest()->getSession()->read('Account.currentTenantId');
        $exports['tenantName'] = $this->getRequest()->getSession()->read('Account.currentTenantName');
        $exports['exportUTC'] = FrozenTime::now()->i18nFormat('yyyy-MM-dd hh:mm:ss');
        $exports['ratingFields'] = $ratingFields;

        // use the Json view
        $this->viewBuilder()->setClassName('Json');
        $this->set(compact('exports'));
        $this->viewBuilder()->setOption('serialize', ['exports']);

        // setup filename
        // todo not sure why downloads don't contain the content
        $this->response->withDownload(FrozenTime::now()->i18nFormat('yyyy-MM-dd') . '_rating_fields.json');
    }

    public function import()
    {
        if ($this->request->is('post')) {
            $attachment = $this->request->getUploadedFile('file');
            $tempFileLocation = $attachment->getStream()->getMetadata('uri');
            $uploadData = json_decode(file_get_contents($tempFileLocation), true);
            $updates = $this->parseImportUpload($uploadData);

            if (sizeof($updates) > 0) {

                $sCurrentTenantId = $this->getRequest()->getSession()->read('Account.currentTenantId');

                // delete existing records
                $this->RatingFields->deleteAll(['RatingFields.tenant_id' => $sCurrentTenantId]);

                /// add new records
                $oRatingFieldTable = $this->RatingFields;
                $oNewEntities = $oRatingFieldTable->newEntities($updates);

                $bBulkSaveSuccessful = true;
                $oRatingFieldTable->getConnection()->transactional(function () use ($oRatingFieldTable, $oNewEntities) {
                    foreach ($oNewEntities as $aEntity) {
                        $bSaveSuccessful = $oRatingFieldTable->save($aEntity, ['atomic' => false]);

                        if (!$bSaveSuccessful) {
                            $oRatingFieldTable = false;
                        }
                    }
                });

                if ($bBulkSaveSuccessful) {
                    $this->Flash->success(__('Bulk import was successful.'),
                        ['class' => 'alert alert-success']);
                    return $this->redirect(['controller' => 'RatingFields', 'action' => 'index']);
                } else {
                    $this->Flash->error(__('The Rating Fields could not be imported. Please, try again.'),
                        ['class' => 'alert alert-danger']);
                }
            }
        }
    }

    public function parseImportUpload($uploadData)
    {
        $updates = [];
        foreach ($uploadData['exports']['ratingFields'] as $ratingField) {

            array_push($updates, [
                'name' => $ratingField['name'],
                'json_path' => $ratingField['jsonPath'],
                'type' => $ratingField['type'],
                'sort_order' => $ratingField['sortOrder']]);
        }

        return $updates;
    }
}