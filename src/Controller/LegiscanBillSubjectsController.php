<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * LegiscanBillSubjects Controller
 *
 * @property \App\Model\Table\LegiscanBillSubjectsTable $LegiscanBillSubjects
 * @method \App\Model\Entity\LegiscanBillSubject[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LegiscanBillSubjectsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'LegiscanSubjects',
                'LegiscanBills',
                'LegiscanSessions',
                'LegiscanChambers',
                'LegiscanCurrentChambers'],
        ];
        $legiscanBillSubjects = $this->paginate($this->LegiscanBillSubjects);

        $this->set(compact('legiscanBillSubjects'));
    }

    /**
     * View method
     *
     * @param string|null $id Legiscan Bill Subject id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $legiscanBillSubject = $this->LegiscanBillSubjects->get($id, [
            'contain' => [
                'LegiscanSubjects',
                'LegiscanBills',
                'LegiscanSessions',
                'LegiscanChambers',
                'LegiscanCurrentChambers'],
        ]);

        $this->set(compact('legiscanBillSubject'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $legiscanBillSubject = $this->LegiscanBillSubjects->newEmptyEntity();
        if ($this->request->is('post')) {
            $legiscanBillSubject = $this->LegiscanBillSubjects->patchEntity($legiscanBillSubject, $this->request->getData());
            if ($this->LegiscanBillSubjects->save($legiscanBillSubject)) {
                $this->Flash->success(__('The legiscan bill subject has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan bill subject could not be saved. Please, try again.'));
        }
        $legiscanSubjects = $this->LegiscanBillSubjects->LegiscanSubjects->find('list', ['limit' => 200]);
        $legiscanBills = $this->LegiscanBillSubjects->LegiscanBills->find('list', ['limit' => 200]);
        $legiscanSessions = $this->LegiscanBillSubjects->LegiscanSessions->find('list', ['limit' => 200]);
        $legiscanChambers = $this->LegiscanBillSubjects->LegiscanChambers->find('list', ['limit' => 200]);
        $legiscanCurrentChambers = $this->LegiscanBillSubjects->LegiscanCurrentChambers->find('list', ['limit' => 200]);
        $this->set(compact('legiscanBillSubject', 'legiscanSubjects', 'legiscanBills', 'legiscanSessions', 'legiscanChambers', 'legiscanCurrentChambers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Legiscan Bill Subject id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $legiscanBillSubject = $this->LegiscanBillSubjects->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $legiscanBillSubject = $this->LegiscanBillSubjects->patchEntity($legiscanBillSubject, $this->request->getData());
            if ($this->LegiscanBillSubjects->save($legiscanBillSubject)) {
                $this->Flash->success(__('The legiscan bill subject has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan bill subject could not be saved. Please, try again.'));
        }
        $legiscanSubjects = $this->LegiscanBillSubjects->LegiscanSubjects->find('list', ['limit' => 200]);
        $legiscanBills = $this->LegiscanBillSubjects->LegiscanBills->find('list', ['limit' => 200]);
        $legiscanSessions = $this->LegiscanBillSubjects->LegiscanSessions->find('list', ['limit' => 200]);
        $legiscanChambers = $this->LegiscanBillSubjects->LegiscanChambers->find('list', ['limit' => 200]);
        $legiscanCurrentChambers = $this->LegiscanBillSubjects->LegiscanCurrentChambers->find('list', ['limit' => 200]);
        $this->set(compact('legiscanBillSubject', 'legiscanSubjects', 'legiscanBills', 'legiscanSessions', 'legiscanChambers', 'legiscanCurrentChambers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Legiscan Bill Subject id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $legiscanBillSubject = $this->LegiscanBillSubjects->get($id);
        if ($this->LegiscanBillSubjects->delete($legiscanBillSubject)) {
            $this->Flash->success(__('The legiscan bill subject has been deleted.'));
        } else {
            $this->Flash->error(__('The legiscan bill subject could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}