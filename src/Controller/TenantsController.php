<?php

namespace App\Controller;

use App\Model\Entity\Tenant;
use App\Model\Table\TenantsTable;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Datasource\ResultSetInterface;
use Cake\Http\Response;
use Cake\Utility\Text;

/**
 * Tenants Controller
 *
 * @property TenantsTable $Tenants
 *
 * @method Tenant[]|ResultSetInterface paginate($object = null, array $settings = [])
 */
class TenantsController extends AppController
{
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $tenants = $this->paginate($this->Tenants);

        $this->set(compact('tenants'));
    }

    /**
     * View method
     *
     * @param string|null $id Tenant id.
     * @throws RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $tenant = $this->Tenants->get($id, [
            'contain' => []
        ]);

        $this->set('tenant', $tenant);
    }

    private function getIdPublic($uuid)
    {
        return substr((string)$uuid, 0, 8);
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $tenant = $this->Tenants->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $id = Text::uuid();
            $data['id'] = $id;
            $data['id_public'] = $this->getIdPublic($id);

            $tenant = $this->Tenants->patchEntity($tenant, $data);

            if ($results = $this->Tenants->save($tenant)) {
                $this->Flash->success(__('The tenant has been created.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tenant could not be created. Please, try again.'));
        }

        $this->set(compact('tenant'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Tenant id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $tenant = $this->Tenants->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            // determine public id every time
            $data['id_public'] = $this->getIdPublic($id);

            $tenant = $this->Tenants->patchEntity($tenant, $data);

            if ($this->Tenants->save($tenant)) {
                $this->Flash->success(__('The tenant has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tenant could not be saved. Please, try again.'));
        }

        $this->set(compact('tenant'));
    }

}
