<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * LegiscanParties Controller
 *
 * @property \App\Model\Table\LegiscanPartiesTable $LegiscanParties
 * @method \App\Model\Entity\LegiscanParty[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LegiscanPartiesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $legiscanParties = $this->paginate($this->LegiscanParties);

        $this->set(compact('legiscanParties'));
    }

    /**
     * View method
     *
     * @param string|null $id Legiscan Party id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $legiscanParty = $this->LegiscanParties->get($id, [
            'contain' => ['LegiscanBillVoteDetails', 'Parties'],
        ]);

        $this->set(compact('legiscanParty'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $legiscanParty = $this->LegiscanParties->newEmptyEntity();
        if ($this->request->is('post')) {
            $legiscanParty = $this->LegiscanParties->patchEntity($legiscanParty, $this->request->getData());
            if ($this->LegiscanParties->save($legiscanParty)) {
                $this->Flash->success(__('The legiscan party has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan party could not be saved. Please, try again.'));
        }
        $this->set(compact('legiscanParty'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Legiscan Party id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $legiscanParty = $this->LegiscanParties->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $legiscanParty = $this->LegiscanParties->patchEntity($legiscanParty, $this->request->getData());
            if ($this->LegiscanParties->save($legiscanParty)) {
                $this->Flash->success(__('The legiscan party has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan party could not be saved. Please, try again.'));
        }
        $this->set(compact('legiscanParty'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Legiscan Party id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $legiscanParty = $this->LegiscanParties->get($id);
        if ($this->LegiscanParties->delete($legiscanParty)) {
            $this->Flash->success(__('The legiscan party has been deleted.'));
        } else {
            $this->Flash->error(__('The legiscan party could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}