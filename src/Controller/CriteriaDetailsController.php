<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * CriteriaDetails Controller
 *
 * @property \App\Model\Table\CriteriaDetailsTable $CriteriaDetails
 * @method \App\Model\Entity\CriteriaDetail[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CriteriaDetailsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tenants', 'Criterias'],
            'order' => [
                'Criterias.name' => 'ASC'
            ],
            'sortableFields' => [
                'Criterias.name',
                'CriteriaDetails.name',
                'CriteriaDetails.sort_order',
            ]
        ];
        $criteriaDetails = $this->paginate($this->CriteriaDetails);

        $this->set(compact('criteriaDetails'));
    }

    /**
     * View method
     *
     * @param string|null $id Criteria Detail id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $criteriaDetail = $this->CriteriaDetails->get($id, [
            'contain' => ['Tenants', 'Criterias'],
        ]);

        $this->set(compact('criteriaDetail'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $criteriaDetail = $this->CriteriaDetails->newEmptyEntity();
        if ($this->request->is('post')) {
            $criteriaDetail = $this->CriteriaDetails->patchEntity($criteriaDetail, $this->request->getData());
            if ($this->CriteriaDetails->save($criteriaDetail)) {
                $this->Flash->success(__('The criteria detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The criteria detail could not be saved. Please, try again.'));
        }
        $tenants = $this->CriteriaDetails->Tenants->find('list', ['limit' => 200]);
        $criterias = $this->CriteriaDetails->Criterias->find('list', ['limit' => 200]);
        $this->set(compact('criteriaDetail', 'tenants', 'criterias'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Criteria Detail id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $criteriaDetail = $this->CriteriaDetails->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $criteriaDetail = $this->CriteriaDetails->patchEntity($criteriaDetail, $this->request->getData());
            if ($this->CriteriaDetails->save($criteriaDetail)) {
                $this->Flash->success(__('The criteria detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The criteria detail could not be saved. Please, try again.'));
        }
        $tenants = $this->CriteriaDetails->Tenants->find('list', ['limit' => 200]);
        $criterias = $this->CriteriaDetails->Criterias->find('list', ['limit' => 200]);
        $this->set(compact('criteriaDetail', 'tenants', 'criterias'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Criteria Detail id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $criteriaDetail = $this->CriteriaDetails->get($id);
        if ($this->CriteriaDetails->delete($criteriaDetail)) {
            $this->Flash->success(__('The criteria detail has been deleted.'));
        } else {
            $this->Flash->error(__('The criteria detail could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}