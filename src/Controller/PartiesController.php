<?php

namespace App\Controller;


/**
 * Parties Controller
 *
 * @property \App\Model\Table\PartiesTable $Parties
 *
 * @method \App\Model\Entity\Party[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PartiesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $session = $this->request->getSession();
        $currentTenantDataSource = $session->read('Account.currentTenantDataSource');
        if ($currentTenantDataSource === 'legiscan') {
            $this->Flash->success(__('Note: The data below is curated weekly on Monday from LegiScan meaning you cannot add new records.'));
        }
        $this->paginate = [
            'contain' => ['Tenants']
        ];
        $parties = $this->paginate($this->Parties);

        $this->set(compact('parties'));
    }

    /**
     * View method
     *
     * @param string|null $id Party id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $party = $this->Parties->get($id, [
            'contain' => ['Tenants']
        ]);

        $this->set('party', $party);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        $currentTenantDataSource = $session->read('Account.currentTenantDataSource');
        if ($currentTenantDataSource === 'legiscan') {
            $this->Flash->error(__('This functionality is not available when using LegiScan as the primary data source!'));
            return $this->redirect(['action' => 'index']);
        }

        $party = $this->Parties->newEmptyEntity();
        if ($this->request->is('post')) {
            $party = $this->Parties->patchEntity($party, $this->request->getData());
            if ($this->Parties->save($party)) {
                $this->Flash->success(__('The party has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The party could not be saved. Please, try again.'));
        }
        $tenants = $this->Parties->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('party', 'tenants'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Party id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $party = $this->Parties->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $party = $this->Parties->patchEntity($party, $this->request->getData());
            if ($this->Parties->save($party)) {
                $this->Flash->success(__('The party has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The party could not be saved. Please, try again.'));
        }
        $tenants = $this->Parties->Tenants->find('list', ['limit' => 200]);
        $this->set(compact('party', 'tenants'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Party id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        try {
            $party = $this->Parties->get($id);
            if ($this->Parties->delete($party)) {
                $this->Flash->success(__('The party has been deleted.'));
            } else {
                $this->Flash->error(__('The party could not be deleted. Please, try again.'));
            }
        } catch (\Exception) {
            $this->Flash->error(__('The party you are trying to delete is associated with other records. Please, try again after deleting the related records.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
