<?php

namespace App\Controller;

use App\Model\Entity\Record;
use App\Model\Table\RecordsTable;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Datasource\ResultSetInterface;
use Cake\Event\EventManager;
use Cake\Http\Client;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Records Controller
 *
 * @property RecordsTable $Records
 *
 * @method Record[]|ResultSetInterface paginate($object = null, array $settings = [])
 */
class RecordsController extends AppController
{
    private readonly \Cake\ORM\TableRegistry $tableRegistryClass;

    public function __construct(ServerRequest $request = null, Response $response = null, $name = null, EventManager $eventManager = null)
    {
        parent::__construct($request, $response, $name, $eventManager);

        $this->tableRegistryClass = new TableRegistry();
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Actions', 'Legislators', 'Ratings'],
            'fields' => [
                'Records.id',
                'Records.is_chair',
                'Records.has_statement',
                'Actions.id',
                'Actions.description',
                'Legislators.id',
                'Legislators.journal_name',
                'Ratings.id',
                'Ratings.name',
            ]
        ];
        $records = $this->paginate($this->Records);

        $this->set(compact('records'));
    }

    /**
     * View method
     *
     * @param string|null $id Record id.
     * @return void
     * @throws RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $record = $this->Records->get($id, [
            'contain' => ['Actions', 'Legislators', 'Ratings'],
            'fields' => [
                'Records.id',
                'Records.is_chair',
                'Records.has_statement',
                'Records.created',
                'Records.modified',
                'Actions.id',
                'Actions.name',
                'Legislators.id',
                'Legislators.journal_name',
                'Ratings.id',
                'Ratings.name',
            ]
        ]);

        $this->set('record', $record);
    }

    /**
     * Edit method
     *
     * @param string|null $id Record id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $record = $this->Records->get($id, [
            'contain' => ['Ratings'],
            'fields' => [
                'Records.id',
                'Records.action_id',
                'Records.legislator_id',
                'Records.rating_id',
                'Records.is_chair',
                'Records.has_statement',
                'Ratings.chamber_id',
                'Ratings.legislative_session_id'
            ]
        ]);

        // get legislative session
        $chamber = $record->toArray()['rating']['chamber_id'];
        $inputSessionId = $record->toArray()['rating']['legislative_session_id'];

        if ($this->request->is(['patch', 'post', 'put'])) {
            $record = $this->Records->patchEntity($record, $this->request->getData());
            if ($this->Records->save($record)) {
                $this->Flash->success(__('The record has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The record could not be saved. Please, try again.'));
        }
        $actions = $this->Records->Actions->find('list', ['limit' => 500]);
        $legislators = $this->Records->Legislators->find('list', [
            'conditions' => [
                'Legislators.chamber_id' => $chamber,
                'Legislators.legislative_session_id' => $inputSessionId
            ],
            'limit' => 500
        ]);
        $ratings = $this->Records->Ratings->find('list', [
            'conditions' => [
                'Ratings.chamber_id' => $chamber,
                'Ratings.legislative_session_id' => $inputSessionId
            ],
            'limit' => 500
        ]);
        $this->set(compact('record', 'actions', 'legislators', 'ratings'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Record id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $record = $this->Records->get($id);
        if ($this->Records->delete($record)) {
            $this->Flash->success(__('The record has been deleted.'));
        } else {
            $this->Flash->error(__('The record could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function exportCsv($ratingId = null)
    {
        if (!empty($ratingId)) {
            $ratingCheck = $this->Records->Ratings->find('all', [
                'fields' => ['id', 'slug', 'record_vote'],
                'conditions' => ['id' => $ratingId]])->first();

            $slug = $ratingCheck->slug;

            if (empty($ratingCheck['id'])) {
                $this->Flash->error(__('Invalid rating selected. Please, try again.'));
                return $this->redirect(['controller' => 'ratings', 'action' => 'index']);
            }

        } else {
            $this->Flash->error(__('Invalid rating selected. Please, try again.'));
            return $this->redirect(['controller' => 'ratings', 'action' => 'index']);
        }

        // get tenant domain
        $currentTenantDomain = $this->getRequest()->getSession()->read('Account.currentTenantDomain');
        $isLocal = (str_contains((string)$currentTenantDomain, 'localhost') ? 1 : 0);

        if ($isLocal) {
            $apiUrl = 'http://' . $currentTenantDomain . ':3001/api/votes/' . $slug;
            $clientOptions = ['ssl_verify_peer' => false, 'ssl_verify_peer_name' => false];
        } else {
            $apiUrl = 'https://' . $currentTenantDomain . '/api/votes/' . $slug;
            $clientOptions = [];
        }

        $http = new Client();
        $response = $http->get($apiUrl, [], $clientOptions);
        $json = $response->getJson();

        $exportResults = [];
        if (isset($json['results'])) {
            $legislators = $json['results'][0]['legislatorListing'];
            foreach ($legislators as $legislator) {
                array_push($exportResults, [
                    'last_name' => $legislator['personLastName'],
                    'first_name' => $legislator['personFirstName'],
                    'district' => $legislator['legislatorDistrict'],
                    'party' => $legislator['partyAbbreviation'],
                    'journal_name' => $legislator['journalName'],
                    'export_text' => (
                    $legislator['evaluatedActionType']['actionExportText'] === '' ? 'Export Text Not Configured' :
                        $legislator['evaluatedActionType']['actionExportText']
                    )]);
            }
        }

        $header = ['Last Name', 'First Name', 'District', 'Party', 'Journal Name', 'Vote'];
        $extract = ['last_name', 'first_name', 'district', 'party', 'journal_name', 'export_text'];

        // use the CSV view
        $filename = $ratingCheck->slug . ' (as of ' . FrozenTime::now()->i18nFormat('yyyy-MM-dd') . ').csv';
        $this->setResponse($this->getResponse()->withDownload($filename));
        $this->viewBuilder()
            ->setClassName('CsvView.Csv')
            ->setOptions(['serialize' => 'exportResults', 'header' => $header, 'extract' => $extract]);

        $this->set(compact('exportResults'));
    }

    private function getExportFilename($ratingId)
    {
        $results = $this->Records->Ratings->find('all', [
            'contain' => ['LegislativeSessions', 'Chambers'],
            'fields' => ['LegislativeSessions.name', 'Chambers.name', 'Ratings.name'],
            'conditions' => ['Ratings.id' => $ratingId]])->first();

        $session = $results['legislative_session']['name'];
        $chamber = $results['chamber']['name'];
        $rating = $results['name'];

        return $session . ' - ' . $chamber . ' - ' . $rating . '.csv';
    }

    /**
     * bulkedit method
     *
     * @return void
     */
    public function bulkedit()
    {
        $ratingId = (!empty($this->request->getParam('ratingId')) ? $this->request->getParam('ratingId') : null);

        if (!$this->request->is('post')) {
            // Get selected filters for provided rating.
            $selected_options = [
                'contain' => ['LegislativeSessions'],
                'fields' => ['LegislativeSessions.id', 'Ratings.chamber_id', 'Ratings.id']];

            if ($ratingId !== null)
                $selected_options['conditions'] = ['Ratings.id' => $ratingId];

            $selectedFilters = $this->Records->Ratings->find('all', $selected_options)->first();

            if ($ratingId === null)
                $ratingId = $selectedFilters['id'];

            // Filter options.
            $sessions = $this->Records->Ratings->LegislativeSessions->find('list');
            $chambers = $this->Records->Ratings->Chambers->find('list');

            $ratings = $this->get_ratings($selectedFilters['legislative_session']['id'],
                $selectedFilters['chamber_id']);

            // Records.
            $rows = $this->Records->Ratings->find('all', [
                'contain' => [],
                'fields' => [
                    'RatingId' => 'Ratings.id',
                    'RecordId' => 'Records.id',
                    'LegislatorId' => 'Legislators.id',
                    'Legislators.journal_name',
                    'Records.is_chair',
                    'Records.has_statement',
                    'Records.action_id'],
                'join' => [
                    'Legislators' => [
                        'table' => 'legislators',
                        'type' => 'INNER',
                        'foreignKey' => false,
                        'conditions' => [
                            'Ratings.legislative_session_id = Legislators.legislative_session_id',
                            'Ratings.chamber_id = Legislators.chamber_id']],
                    'Records' => [
                        'table' => 'records',
                        'type' => 'LEFT',
                        'foreignKey' => false,
                        'conditions' => ['Legislators.id = Records.legislator_id', 'Ratings.id = Records.rating_id']]],
                'conditions' => ['Ratings.id' => $ratingId],
                'order' => ['Legislators.journal_name']])->all();

            // Actions.
            $actionTemp = $this->Records->Ratings->find('all', [
                'contain' => [],
                'fields' => ['Actions.id', 'Actions.description'],
                'join' => [
                    'RatingTypes' => [
                        'table' => 'rating_types',
                        'type' => 'INNER',
                        'foreignKey' => false,
                        'conditions' => ['Ratings.rating_type_id = RatingTypes.id']],
                    'Actions' => [
                        'table' => 'actions',
                        'type' => 'inner',
                        'foreignKey' => false,
                        'conditions' => ['Actions.rating_type_id = RatingTypes.id']]],
                'conditions' => ['Ratings.id' => $ratingId]])->toList();

            // reformat action array for easier manipulation in the view
            $actions = [];
            foreach ($actionTemp as $action) {

                $actions[$action['Actions']['id']] = $action['Actions']['description'];
            }

            $this->set(compact('selectedFilters', 'sessions', 'chambers', 'ratings', 'rows', 'actions'));
        }
    }

    private function get_ratings($sessionId, $chamberId)
    {
        return (new RatingsController())->getRatingsList($sessionId, $chamberId);
    }

    private function get_actions($ratingId, $reformat = true)
    {
        // get the record_type_id for the rating
        $ratingType = $this->Records->Ratings->get($ratingId, [
            'fields' => [
                'Ratings.rating_type_id'
            ]
        ]);

        // votes types
        $voteTypeQuery = $this->Records->Actions->find('list', [
            'conditions' => ['Actions.rating_type_id' => $ratingType['rating_type_id']],
            'fields' => ['Actions.id', 'Actions.description']]);

        if ($reformat) {
            $voteTypes = [];
            foreach ($voteTypeQuery->toArray() as $voteTypeId => $voteTypeName) {
                $voteTypes[$voteTypeName] = $voteTypeId;
            }
            return $voteTypes;
        } else {
            return $voteTypeQuery;
        }
    }

    public function api()
    {
        $this->viewBuilder()->disableAutoLayout();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $action = $data['action'];
            unset($data['action']);

            switch ($action) {
                case 'update-row':
                    return $this->bulkEditUpdateRow($data);
                case 'fetch-ratings':
                    return $this->bulkEditFetchRatings($data);
            }
        }
    }

    private function bulkEditUpdateRow($data)
    {
        if (empty($data['id'])) {
            unset($data['id']);
            $record = $this->Records->newEmptyEntity();
        } else {
            $record = $this->Records->get($data['id']);
        }

        $data['has_statement'] = !empty($data['has_statement']);
        $data['is_chair'] = !empty($data['is_chair']);

        if ($data['is_chair']) {
            $currentTenantId = $this->getRequest()->getSession()->read('Account.currentTenantId');

            $this->Records->updateAll(['is_chair' => false], [
                'rating_id' => $data['rating_id'],
                'tenant_id' => $currentTenantId]);
        }

        $record = $this->Records->patchEntity($record, $data);
        if ($this->Records->save($record)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($data))
                ->withStatus(200);
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode($record->getErrors()))
                ->withStatus(400);
        }
    }

    private function bulkEditFetchRatings($data)
    {
        $ratings = $this->get_ratings($data['session'], $data['chamber']);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($ratings))
            ->withStatus(200);
    }

    public function bulkImport($id = null)
    {
        $currentTenantState = $this->getRequest()->getSession()->read('Account.currentTenantState');
        if ($this->request->is('post')) {

            $aFormData = $this->request->getData();

            if (!isset($aFormData['rating_id']) || !isset($aFormData['action_id'])) {
                $this->Flash->error('Please select a Rating and Action');
            } else {
                $sFormRatingId = $aFormData['rating_id'];
                $sFormChamberId = $aFormData['chamber'];
                $sChamberType = $this->getChamberType($sFormChamberId);
                $jsonDetails = $this->getVoteJsonByState($aFormData, $currentTenantState, $sChamberType);
                $this->saveVoteJson($sFormRatingId, $jsonDetails);

                $json = json_decode((string)$jsonDetails);

                if ($json->status === 'failure') {
                    $this->Flash->error('API Error: ' . $json->message);
                } else {

                    // save off identifier
                    if (isset($json->data->details->identifier)) {

                        $identifier = $json->data->details->identifier;

                        $oRatingTable = $this->tableRegistryClass->getTableLocator()->get('Ratings');
                        $rating = $oRatingTable->get($sFormRatingId); // Return article with id = $id (primary_key of row which need to get updated)
                        $rating->record_vote = $identifier;
                        if ($oRatingTable->save($rating)) {
                            $this->Flash->success(__('Identifier save successful.'));
                        } else {
                            $this->Flash->error(__('The identifier could not be saved. Please, try again.'));
                        }
                    }

                    if (strlen((string)$jsonDetails) > 2) {
                        $aMemberVotes = $this->processVoteJson($jsonDetails, $aFormData, $sChamberType);

                        if (sizeof($aMemberVotes) > 0) {

                            $sCurrentTenantId = $this->getRequest()->getSession()->read('Account.currentTenantId');

                            // delete existing records
                            $this->Records->deleteAll([
                                'Records.rating_id' => $sFormRatingId,
                                'Records.tenant_id' => $sCurrentTenantId]);

                            /// add new records
                            $oRecordTable = $this->tableRegistryClass->getTableLocator()->get('Records');
                            $oNewEntities = $oRecordTable->newEntities($aMemberVotes);

                            $bBulkSaveSuccessful = true;
                            try {
                                $oRecordTable->getConnection()->transactional(function () use ($oRecordTable, $oNewEntities) {
                                    foreach ($oNewEntities as $aEntity) {
                                        $bSaveSuccessful = $oRecordTable->save($aEntity, ['atomic' => false]);

                                        if (!$bSaveSuccessful) {
                                            $oRecordTable = false;
                                        }
                                    }
                                });
                            } catch (Exception $e) {
                                $this->Flash->error('Bulk Save Error: ' . $e);
                            }

                            if ($bBulkSaveSuccessful) {
                                $this->Flash->success(__('Bulk import was successful.'));
                                return $this->redirect([
                                    'controller' => 'Records',
                                    'action' => 'bulkedit',
                                    $sFormRatingId]);
                            } else {
                                $this->Flash->error(__('The records could not be imported. Please, try again.'));
                            }
                        }
                    }
                }
            }
        }

        $legislativeSessions = $this->Records->Ratings->LegislativeSessions->find('list');
        $chambers = $this->Records->Ratings->Chambers->find('list');

        // Get selected filters for provided rating.
        if ($id !== null) {
            $selectedFilters = $this->Records->Ratings->find('all', [
                'fields' => [
                    'Ratings.legislative_session_id',
                    'Ratings.chamber_id',
                    'Ratings.id'],
                'conditions' => ['Ratings.id' => $id]])->first();
        } else {
            $selectedFilters = [];
        }
        $this->set(compact('id', 'legislativeSessions', 'selectedFilters', 'chambers', 'currentTenantState'));
    }

    public function __tx_parse_raw($sFormRawVotes, $sChamberType)
    {
        $aMemberVotes = [];

        // only perform transformations if they provide a record vote
        if (strlen((string)$sFormRawVotes) > 0) {
            // change ascent characters
            $rvt_e_ascent = preg_replace('/e´/m', 'é', (string)$sFormRawVotes);
            $rvt_n_ascent = preg_replace('/n˜/m', 'ñ', $rvt_e_ascent);
            $rvt_a_ascent = preg_replace('/a´/m', 'á', $rvt_n_ascent);
            $rvt_i_ascent = preg_replace('/ı´/m', 'í', $rvt_a_ascent);

            // replace "ii"
            $replace_ii = preg_replace('/ii/', ' ', $rvt_i_ascent);

            // Senate journal only issues
            if ($sChamberType === "U") {

                // fix VanideiPutte
                $fix_vdp = preg_replace("/VanideiPutte/m", "Van De Putte", $replace_ii);

                // (Senate) replace ":"
                $replace_colon = preg_replace('/ ?: ?/', ' — ', $fix_vdp);

                // (Senate) change commas to semicolons
                $replace_semicolon = preg_replace('/,/', ';', $replace_colon);

                // rewrite "Absent-excused" to match House
                $replace_a_ex = preg_replace('/Absent-excused/', 'Absent, Excused', $replace_semicolon);

            } else {

                // when a line doesn't end with a semicolon, let's do some magic to joint commas and two last names together
                // i.e. Martinez Fisher
                $fix_line_endings = preg_replace("/(.*[^;,.])\r\n/m", "$1 ", $replace_ii);

                $replace_a_ex = $fix_line_endings;
            }

            // (Senate) remove spaces after semicolons
            $rvt_remove_space = preg_replace('/; /m', ';', $replace_a_ex);

            // change dashes after vote type to *
            $rvt_fix_dashes = preg_replace('/ — /m', '*', $rvt_remove_space);

            // replace yes -> yea
            $rvt_replace_no = preg_replace('/(No|no)\*/m', 'Nays*', $rvt_fix_dashes);

            // replace no -> nay
            $rvt_replace_yes = preg_replace('/(Yes|yes)\*/', 'Yeas*', $rvt_replace_no);

            // replace present, not voting -> Present, not voting
            $rvt_replace_pnv = preg_replace('/present, not voting\*/', 'Present, not voting*', $rvt_replace_yes);

            // replace Present-not voting -> Present, not voting
            $rvt_replace_pnv2 = preg_replace('/Present-not voting\*/', 'Present, not voting*', $rvt_replace_pnv);

            // separate lines by vote type
            $rvt_add_separator = preg_replace("/\n(.*)\*/m", "|$1*", $rvt_replace_pnv2);

            // remove all the new lines
            $rvt_remove_lines = preg_replace("/\n/m", "", $rvt_add_separator);

            // split by |
            $rvt_array = explode('|', $rvt_remove_lines);

            // loop through each SOV
            foreach ($rvt_array as $rvt) {

                preg_match("/.*Record.*/m", $rvt, $summary_check);

                if (!$summary_check) {

                    // find trailing periods, except when part of an initial => then remove
                    $rvt_no_end_period = preg_replace("/^(.*)([a-z])[ .\/?-]*/m", "$1$2", $rvt);

                    // split into vote type => journal_name
                    $final_split = explode('*', $rvt_no_end_period);

                    $vote_type = rtrim(ltrim($final_split[0], ' '), ' ');
                    $journal_name = $final_split[1];

                    $aMemberVotes[$vote_type] = $journal_name;
                }

            }
        }

        return $aMemberVotes;
    }

    public function __tx_get_json($aFormData, $sChamberType)
    {

        $aMemberVotes = [];

        $sFormSessionId = $aFormData['legislative_session_id'];
        $sFormChamberId = $aFormData['chamber'];

        // need to clean up the Texas votes first before putting into the correct JSON format
        $sFormRawVotes = $aFormData['record_vote_text'];
        $aInputMemberVotes = $this->__tx_parse_raw($sFormRawVotes, $sChamberType);

        // legislators
        $oLegislatorQuery = $this->Records->Legislators->find('list', [
            'conditions' => 'Legislators.legislative_session_id=\'' . $sFormSessionId . '\' AND Legislators.chamber_id = \'' . $sFormChamberId . '\'',
            'fields' => ['journal_name', 'id']]);

        // build legislator array for easier parsing
        $aLegislators = [];
        foreach ($oLegislatorQuery->toArray() as $sLegislatorId => $sJournalName) {
            $aLegislators[$sJournalName] = $sLegislatorId;
        }

        // only perform transformations if they provide a record vote
        if (sizeof($aInputMemberVotes) > 0) {

            foreach ($aInputMemberVotes as $sVoteTypeOriginal => $sJournalNames) {

                // remove new lines and spaces
                $sRemoveNewLines = str_replace(["\r", "\n"], '', (string)$sJournalNames);

                $aNames = explode(';', $sRemoveNewLines);

                foreach ($aNames as $sJournalNameOriginal) {

                    // remove double spaces
                    $sRemoveDoubleSpaces = preg_replace('/\s\s+/', ' ', $sJournalNameOriginal);

                    // fix issue when there isn't a space after commas
                    $sFixCommaNoSpace = preg_replace('/(.*,)([A-Z].*)/', '$1 $2', $sRemoveDoubleSpaces);

                    // add period at end of journal name if it's an initial
                    $sAddPeriod = preg_replace('/(.*[A-Z)])$/', '$1.', $sFixCommaNoSpace);

                    preg_match("/(.*)\(C\).?/", $sAddPeriod, $bChairMatch);

                    if ($bChairMatch) {
                        // replace chair indicator (C) -> "-1"
                        $sJournalNameFinal = preg_replace('/(.*)\(C\).?/', '$1', $sAddPeriod);
                        $this->Flash->success(__('Setting "' . $sJournalNameFinal . '" as "Chair"'));
                        $bChairFlag = 1;
                    } else {
                        $sJournalNameFinal = $sAddPeriod;
                        $bChairFlag = 0;
                    }

                    // rewrite Mr. Speaker to Speaker vote if not 'Present' or 'Present, not voting'
                    if (strtolower($sJournalNameFinal) === 'mr. speaker' && ($sVoteTypeOriginal === 'Present, not voting' || $sVoteTypeOriginal === 'Present')) {
                        $this->Flash->success(__('Changing "Mr. Speaker" vote to "Speaker" from "' . $sVoteTypeOriginal . '"'));
                        $sVoteTypeFinal = 'Speaker';
                    } else {
                        $sVoteTypeFinal = $sVoteTypeOriginal;
                    }

                    $sJournalNameFinal = trim($sJournalNameFinal);

                    // check to see if the journal name exists in the area
                    if (array_key_exists($sJournalNameFinal, $aLegislators) || str_contains($sJournalNameFinal, 'Speaker')) {
                        array_push($aMemberVotes, [
                            'name' => $sJournalNameFinal,
                            'voteType' => $sVoteTypeFinal,
                            'is_chair' => $bChairFlag,
                            'has_statement' => 0]);
                    } else {
                        $this->Flash->error(__('Unknown Journal Name: "' . $sJournalNameFinal . '"'));
                    }
                }
            }
        }

        $aVoteBreakdown = $this->__tx_vote_breakdown($aMemberVotes);

        return json_encode([
            'status' => 'success',
            'data' => ['rollCall' => $aMemberVotes, 'voteCounts' => $aVoteBreakdown, 'speaker' => null]]);
    }

    public function __tx_vote_breakdown($aMemberVotes)
    {
        $aVoteBreakdown = [];

        foreach ($aMemberVotes as $aMemberVote) {
            $sVoteType = $aMemberVote['voteType'];

            if (array_key_exists($sVoteType, $aVoteBreakdown)) {
                $aVoteBreakdown[$sVoteType] = $aVoteBreakdown[$sVoteType] + 1;
            } else {
                $aVoteBreakdown[$sVoteType] = 1;
            }
        }

        return $aVoteBreakdown;
    }

    public function processVoteJson($jsonDetails, $aFormData, $sChamberType)
    {
        $aRecords = [];

        $sFormSessionId = $aFormData['legislative_session_id'];
        $sFormDefaultActionId = $aFormData['action_id'];
        $sFormChamberId = $aFormData['chamber'];
        $sFormRatingId = $aFormData['rating_id'];

        $sLeaderJournalName = $this->getChamberLeader($aFormData, $sChamberType);

        // legislators
        $oLegislatorQuery = $this->Records->Legislators->find('list', [
            'conditions' => 'Legislators.legislative_session_id=\'' . $sFormSessionId . '\' AND Legislators.chamber_id = \'' . $sFormChamberId . '\'',
            'fields' => ['journal_name', 'id']]);

        // lower case all legislator names for string matching
        $aLegislators = [];
        foreach ($oLegislatorQuery->toArray() as $sLegislatorId => $sJournalName) {
            // lower case and remove commas and periods from names to get more precise matches
            $sJournalNameLower = str_replace(',', '', str_replace('.', '', strtolower((string)$sJournalName)));
            $aLegislators[$sJournalNameLower] = $sLegislatorId;
        }

        $aLegislatorsMissing = $aLegislators;

        // only perform transformations if they provide a record vote
        $aMemberVotes = json_decode((string)$jsonDetails)->data->rollCall;
        if (sizeof($aMemberVotes) > 0) {
            // votes types
            $voteTypes = $this->get_actions($sFormRatingId);

            foreach ($aMemberVotes as $aMemberVote) {
                // lower case and remove commas and periods from names to get more precise matches
                $sJournalName = str_replace(',', '', str_replace('.', '', strtolower((string)$aMemberVote->name)));

                // replace Leader with configured journal name from legislative_sessions table
                $sJournalName = preg_replace('/(mr speaker|mr president)/i', (string)$sLeaderJournalName, $sJournalName);

                if (strtolower((string)$sLeaderJournalName) === strtolower($sJournalName)) {
                    $this->Flash->success(__('Successfully replaced Chamber Leader "journal name" with "' . $sLeaderJournalName . '"'));
                }

                $sVoteType = $aMemberVote->voteType;
                $bChair = ($aMemberVote->is_chair ?? 0);
                $bStatement = ($aMemberVote->has_statement ?? 0);

                // check to see if the journal name exists in the area
                if (array_key_exists($sJournalName, $aLegislators)) {

                    $aRecord = [];

                    $sActionId = $voteTypes[$sVoteType];
                    $sLegislatorId = $aLegislators[$sJournalName];

                    $aRecord['action_id'] = $sActionId;
                    $aRecord['legislator_id'] = $sLegislatorId;
                    $aRecord['rating_id'] = $sFormRatingId;
                    $aRecord['is_chair'] = $bChair;
                    $aRecord['has_statement'] = $bStatement;

                    array_push($aRecords, $aRecord);

                    // remove legislator so they don't get set as N/A
                    unset($aLegislatorsMissing[$sJournalName]);
                } else {
                    $this->Flash->error(__('Unknown Journal Name: "' . $sJournalName . '"'));
                }
            }
        }

        // set missing legislators as "N/A" automatically
        if (isset($sFormDefaultActionId)) {
            foreach ($aLegislatorsMissing as $sLegislatorId) {
                $aRecord = [];

                $aRecord['action_id'] = $sFormDefaultActionId;
                $aRecord['legislator_id'] = $sLegislatorId;
                $aRecord['rating_id'] = $sFormRatingId;
                $aRecord['is_chair'] = 0;
                $aRecord['has_statement'] = 0;

                array_push($aRecords, $aRecord);
            }
        }

        return $aRecords;
    }

    public function getVoteJsonByState($aFormData, $sTenantState, $sChamberType)
    {

        $sTenantState = strtolower((string)$sTenantState);

        $sFormSessionId = $aFormData['legislative_session_id'];

        $oSessionDetails = $this->Records->Ratings->LegislativeSessions->find('all', [
            'contains' => [''],
            'fields' => [
                'LegislativeSessions.state_pk'
            ],
            'conditions' => [
                'LegislativeSessions.id' => $sFormSessionId
            ]])->first();

        $sSessionPk = $oSessionDetails->state_pk;

        if (strtolower($sTenantState) === 'tx') {

            $jsonVote = $this->__tx_get_json($aFormData, $sChamberType);

        } else {
            $http = new Client();

            // strip comma for accidental copy/paste for multiple votes scenario in Indiana
            $identifier = str_replace(',', '', trim((string)$aFormData['record_vote_text']));
            $sChamber = ($sChamberType === 'U' ? 'upper' : 'lower');

            if ($identifier === '') {
                $this->Flash->error(__('No identifier provided. Setting all votes to selected default action'));
            }

            $apiUrl = Configure::read('App.jpm_api_url') . '/api/v1/scrapers/' . $sTenantState . '/vote/?chamber=' . $sChamber . '&identifier=' . $identifier . '&session=' . $sSessionPk;
            $response = $http->get(
                $apiUrl,
                []
            // uncomment for dev only
            //,['ssl_verify_peer' => false, 'ssl_verify_peer_name' => false]
            );

            $jsonVote = json_encode($response->getJson());
        }

        return $jsonVote;
    }

    public function getActionsAjax()
    {
        $this->viewBuilder()->setLayout('ajax');

        $rating_id = $this->request->getQuery('rating_id');
        if (empty($rating_id)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'error' => [
                        'code' => '400',
                        'message' => 'rating_id is a required parameter.']]))
                ->withStatus(400);
        }

        $data = $this->get_actions($rating_id, false);

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data))
            ->withStatus(200);
    }

    public function saveVoteJson($sFormRatingId, $jsonDetails)
    {
        $ratingsTable = $this->tableRegistryClass->getTableLocator()->get('Ratings');
        $rating = $ratingsTable->get($sFormRatingId);

        if ($rating) {
            $rating->json = $jsonDetails;
            return $ratingsTable->save($rating);
        } else {
            return false;
        }
    }

    public function getChamberLeader($aFormData, $sChamberType)
    {
        $sFormSessionId = $aFormData['legislative_session_id'];

        // replace "Mr. Speaker" || "Mr. President" with session speaker
        if ($sChamberType === "U") {
            $oUpperChamberLeader = $this->Records->Legislators->LegislativeSessions->find('all', [
                'conditions' => ['LegislativeSessions.id' => $sFormSessionId],
                'fields' => ['LegislativeSessions.lt_gov_journal_name']])->first();
            $sLeaderJournalName = $oUpperChamberLeader['lt_gov_journal_name'];

        } else {
            $oLowerChamberLeader = $this->Records->Legislators->LegislativeSessions->find('all', [
                'conditions' => ['LegislativeSessions.id' => $sFormSessionId],
                'fields' => ['LegislativeSessions.speaker_journal_name']])->first();
            $sLeaderJournalName = $oLowerChamberLeader['speaker_journal_name'];
        }

        if (strlen((string)$sLeaderJournalName) > 0) {
            $this->Flash->success(__('Replacement for Chamber Leader "journal name" enabled: ' . $sLeaderJournalName));
        } else {
            $this->Flash->error(__('Replacement for Chamber Leader "journal name" is not configured.'));
            $sLeaderJournalName = 'Unknown Leader Journal Name';
        }

        return strtolower((string)$sLeaderJournalName);
    }

    public function getChamberType($chamberId)
    {
        $oChamberDetails = $this->Records->Ratings->Chambers->find('all', [
            'contains' => [''],
            'fields' => [
                'Chambers.type'
            ],
            'conditions' => [
                'Chambers.id' => $chamberId
            ]])->first();

        return $oChamberDetails->type;
    }
}
