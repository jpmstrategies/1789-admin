<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * LegiscanSessions Controller
 *
 * @property \App\Model\Table\LegiscanSessionsTable $LegiscanSessions
 * @method \App\Model\Entity\LegiscanSession[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LegiscanSessionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $legiscanSessions = $this->paginate($this->LegiscanSessions);

        $this->set(compact('legiscanSessions'));
    }

    /**
     * View method
     *
     * @param string|null $id Legiscan Session id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $legiscanSession = $this->LegiscanSessions->get($id, [
            'contain' => [
                'LegiscanBillSubjects',
                'LegiscanBillVoteDetails',
                'LegiscanBillVotes',
                'LegiscanBills',
                'LegislativeSessions'],
        ]);

        $this->set(compact('legiscanSession'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $legiscanSession = $this->LegiscanSessions->newEmptyEntity();
        if ($this->request->is('post')) {
            $legiscanSession = $this->LegiscanSessions->patchEntity($legiscanSession, $this->request->getData());
            if ($this->LegiscanSessions->save($legiscanSession)) {
                $this->Flash->success(__('The legiscan session has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan session could not be saved. Please, try again.'));
        }
        $this->set(compact('legiscanSession'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Legiscan Session id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $legiscanSession = $this->LegiscanSessions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $legiscanSession = $this->LegiscanSessions->patchEntity($legiscanSession, $this->request->getData());
            if ($this->LegiscanSessions->save($legiscanSession)) {
                $this->Flash->success(__('The legiscan session has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan session could not be saved. Please, try again.'));
        }
        $this->set(compact('legiscanSession'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Legiscan Session id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $legiscanSession = $this->LegiscanSessions->get($id);
        if ($this->LegiscanSessions->delete($legiscanSession)) {
            $this->Flash->success(__('The legiscan session has been deleted.'));
        } else {
            $this->Flash->error(__('The legiscan session could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}