<?php

namespace App\Controller;


use Cake\Http\Client;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\I18n\FrozenTime;

/**
 * People Controller
 *
 * @property \App\Model\Table\PeopleTable $People
 *
 * @method \App\Model\Entity\Person[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PeopleController extends AppController
{

    public function __construct(ServerRequest $request = null, Response $response = null, $name = null, \Cake\Event\EventManager $eventManager = null)
    {
        parent::__construct($request, $response, $name, $eventManager);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $session = $this->request->getSession();
        $currentTenantDataSource = $session->read('Account.currentTenantDataSource');
        if ($currentTenantDataSource === 'legiscan') {
            $this->Flash->success(__('Note: The data below is curated weekly on Monday from LegiScan meaning you cannot add new records.'));
        }
        $this->paginate = [
            'contain' => [],
            'fields' => [
                'People.id',
                'People.full_name',
                'People.first_name',
                'People.last_name',
                'People.is_enabled',
            ]
        ];
        $people = $this->paginate($this->People);

        $this->set(compact('people'));
    }

    /**
     * View method
     *
     * @param string|null $id Person id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $person = $this->People->get($id, [
            'contain' => ['Grades'],
            'fields' => [
                'People.id',
                'People.full_name',
                'People.nickname',
                'People.salutation',
                'People.first_name',
                'People.middle_name',
                'People.initials',
                'People.last_name',
                'People.suffix',
                'People.bio',
                'People.committee_list',
                'People.image_url',
                'People.ballotpedia_url',
                'People.first_elected',
                'People.district_city',
                'People.grade_number',
                'People.slug',
                'People.state_pk',
                'People.os_pk',
                'People.created',
                'People.modified',
                'People.is_enabled',
                'People.is_retired',
                'People.email',
                'People.phone',
                'People.facebook',
                'People.twitter',
                'Grades.id',
                'Grades.name'
            ]
        ]);

        $addresses = $this->People->Addresses->find('all', [
            'contain' => ['AddressTypes'],
            'fields' => [
                'Addresses.id',
                'Addresses.capitol_address',
                'Addresses.full_address',
                'Addresses.phone',
                'AddressTypes.id',
                'AddressTypes.name'
            ],
            'conditions' => [
                'Addresses.person_id' => $id
            ]
        ])->all();

        $legislators = $this->People->Legislators->find('all', [
            'contain' => ['Chambers', 'LegislativeSessions', 'Parties', 'Grades'],
            'fields' => [
                'LegislativeSessions.id',
                'LegislativeSessions.name',
                'Legislators.id',
                'Legislators.district',
                'Legislators.journal_name',
                'Legislators.grade_number',
                'Legislators.is_enabled',
                'Chambers.id',
                'Chambers.name',
                'Parties.id',
                'Parties.name',
                'Grades.id',
                'Grades.name'
            ],
            'conditions' => [
                'Legislators.person_id' => $id
            ]
        ])->all();

        $this->set(compact('person', 'addresses', 'legislators'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        $currentTenantDataSource = $session->read('Account.currentTenantDataSource');
        if ($currentTenantDataSource === 'legiscan') {
            $this->Flash->error(__('This functionality is not available when using LegiScan as the primary data source!'));
            return $this->redirect(['action' => 'index']);
        }

        $person = $this->People->newEmptyEntity();
        if ($this->request->is('post')) {
            $person = $this->People->patchEntity($person, $this->request->getData());
            if ($this->People->save($person)) {
                $this->Flash->success(__('The person has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The person could not be saved. Please, try again.'));
        }
        $grades = $this->People->Grades->find('list');
        $this->set(compact('person', 'grades'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Person id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $person = $this->People->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            if ($data['state_pk'] === '') {
                $data['state_pk'] = null;
            }

            $person = $this->People->patchEntity($person, $data);
            if ($this->People->save($person)) {
                $this->Flash->success(__('The person has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The person could not be saved. Please, try again.'));
        }
        $grades = $this->People->Grades->find('list');
        $this->set(compact('person', 'grades'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Person id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        try {
            $person = $this->People->get($id);
            if ($this->People->delete($person)) {
                $this->Flash->success(__('The person has been deleted.'));
            } else {
                $this->Flash->error(__('The person could not be deleted. Please, try again.'));
            }
        } catch (\Exception) {
            $this->Flash->error(__('The person you are trying to delete is associated with other records. Please, try again after deleting the related records.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function getCalculatedScores($sessionId, $chamberId): array
    {
        $calculatedScores = [];
        $sessions = $this->People->Legislators->LegislativeSessions->find()
            ->select(['slug'])
            ->where(['id' => $sessionId])
            ->first();
        $sessionSlug = $sessions['slug'];

        $chambers = $this->People->Legislators->Chambers->find()
            ->select(['slug'])
            ->where(['id' => $chamberId])
            ->first();
        $chamberSlug = $chambers['slug'];

        if ($sessionSlug && $chamberSlug) {
            // get tenant domain
            $currentTenantDomain = $this->getRequest()->getSession()->read('Account.currentTenantDomain');
            $isLocal = (str_contains((string)$currentTenantDomain, 'localhost') ? 1 : 0);

            if ($isLocal) {
                $apiUrl = 'http://' . $currentTenantDomain . ":3001/api/people/";
                $clientOptions = ['ssl_verify_peer' => false, 'ssl_verify_peer_name' => false];
            } else {
                $apiUrl = 'https://' . $currentTenantDomain . "/api/people/";
                $clientOptions = [];
            }

            $apiUrl .= "?sessionSlug=$sessionSlug&chamberSlug=$chamberSlug&includeDisabled=true&disableOverrideScores=true";
            $apiUrl .= "?t=" . strtotime("now");

            $currentTenantType = $this->request->getSession()->read('Account.currentTenantType');
            if ($currentTenantType === 'D' || $currentTenantType === 'F') {
                return [];
            }

            $json = [];
            try {
                $http = new Client();
                $response = $http->get($apiUrl, [], $clientOptions);
                $json = $response->getJson();
            } catch (\Exception) {
                $this->Flash->error(__('The Person Score API is not responding. Please, try again later.'));
            }

            if (empty($json)) {
                return [];
            }

            // criteria scoring
            $currentTenantConfigurations = $this->request->getSession()->read('Configurations');
            if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes') {
                // criteria scoring
                $criteria = (new CriteriasController())->parseCalculatedScores(
                    $json, 'criteriaCareerScores', 'personId'
                );
                $calculatedScores['use_criteria'] = $criteria;
            }

            // default scoring
            foreach ($json as $person) {
                $id = $person['personId'];
                $calculatedScores['scores'][$id] = [
                    'score' => ($person['careerScore']['score'] ?? null),
                    'grade' => ($person['careerScore']['grade']['name'] ?? null)
                ];
            }
        }

        return $calculatedScores;
    }

    /**
     * admin_bulkedit method
     *
     * @return void
     */
    public function bulkedit()
    {
        $selectedFilters = [];
        $sessionId = (!empty($this->request->getParam('sessionId')) ? $this->request->getParam('sessionId') : null);
        $chamber = (!empty($this->request->getParam('chamber')) ? $this->request->getParam('chamber') : null);

        if (empty($chamber)) {
            $currentTenantId = $this->getRequest()->getSession()->read('Account.currentTenantId');
            $chamber = (new ChambersController())->getDefault($currentTenantId);
        }

        // Filter options.
        $sessions = $this->People->Legislators->LegislativeSessions->find('list', ['limit' => '200']);
        $chambers = $this->People->Legislators->Chambers->find('list');
        $grades = $this->People->Grades->find('list', [
            'order' => [
                'Grades.sort_order' => 'desc'
            ]]);
        $tags = $this->People->Tags->find('list', [
            'order' => [
                'Tags.sort_order' => 'desc'
            ]]);

        // get first key from list, if not set
        if (empty($sessionId)) {
            $sessionId = key($sessions->toArray());
        }

        // Get selected filters.
        $selectedFilters['session'] = $sessionId;
        $selectedFilters['chamber'] = $chamber;

        // People.
        $peopleTags = $this->getPeopleTags();
        $people = $this->People->Legislators->find('all', [
            'contain' => ['People'],
            'fields' => [
                'People.id',
                'People.full_name',
                'People.first_name',
                'People.last_name',
                'People.first_elected',
                'People.district_city',
                'People.grade_id',
                'People.grade_number',
                'People.email',
                'People.phone',
                'People.facebook',
                'People.twitter',
                'People.slug',
                'People.ttx_candidate_slug',
                'People.is_enabled'],
            'conditions' => ['Legislators.legislative_session_id' => $sessionId, 'Legislators.chamber_id' => $chamber],
            'order' => ['People.last_name' => 'asc', 'People.first_name' => 'asc']]);

        $session = $this->request->getSession();
        $currentTenantType = $session->read('Account.currentTenantType');

        $calculatedScores = [];
        if ($currentTenantType !== 'D' && $currentTenantType !== 'F') {
            $calculatedScores = $this->getCalculatedScores($sessionId, $chamber);
        }

        $this->set(compact('selectedFilters', 'sessions', 'people', 'chambers', 'grades', 'calculatedScores',
            'tags', 'peopleTags'));
    }

    public function getPeopleTags(): array
    {
        $allPeopleTags = [];
        $tags = $this->People->Tags->find('all', [
            'contain' => [
                'PeopleTags'
            ]
        ])->toArray();

        foreach ($tags as $tag) {
            if (is_array($tag['people_tags'])) {
                foreach ($tag['people_tags'] as $peopleTag) {
                    $person_id = $peopleTag['person_id'];
                    $allPeopleTags[$person_id][] = $peopleTag['tag_id'];
                }
            }
        }
        return $allPeopleTags;
    }

    public function exportCsv()
    {
        $records = $this->People->find('all', [
            'contain' => [],
            'fields' => [
                'leg_id' => 'People.id',
                'full_name' => 'People.full_name',
                'first_name' => 'People.first_name',
                'middle_name' => 'People.middle_name',
                'last_name' => 'People.last_name',
                'suffixes' => 'People.suffix',
                'nickname' => 'People.nickname',
                'email' => 'People.email',
                'phone' => 'People.phone',
                'facebook' => 'People.facebook',
                'twitter' => 'People.twitter',
                'active' => 'CASE WHEN People.is_retired = 1 THEN \'TRUE\' ELSE \'FALSE\' END',
                'created_at' => 'People.created',
                'updated_at' => 'People.modified',
                'image_url' => 'People.image_url',
                'salutation' => 'People.salutation',
                'initials' => 'People.initials',
                'first_elected' => 'People.first_elected',
                'district_city' => 'People.district_city',
                'grade_number' => 'CASE WHEN People.grade_number IS NULL THEN \'\' ELSE People.grade_number END',
                'slug' => 'People.slug',
                'enabled' => 'CASE WHEN People.is_enabled = 1 THEN \'TRUE\' ELSE \'FALSE\' END']]);

        $header = [
            'leg_id',
            'full_name',
            'first_name',
            'middle_name',
            'last_name',
            'suffixes',
            'nickname',
            'email',
            'phone',
            'facebook',
            'twitter',
            'active',
            'created_at',
            'updated_at',
            'image_url',
            'salutation',
            'initials',
            'first_elected',
            'district_city',
            'grade_number',
            'slug',
            'is_enabled'];

        // use the CSV view
        $filename = 'People (as of ' . FrozenTime::now()->i18nFormat('yyyy-MM-dd') . ').csv';
        $this->setResponse($this->getResponse()->withDownload($filename));
        $this->viewBuilder()
            ->setClassName('CsvView.Csv')
            ->setOptions(['serialize' => 'records', 'header' => $header]);

        $this->set(compact('records'));
    }

    public function exportScoreCsv($sessionId, $chamber)
    {
        $currentTenantConfigurations = $this->request->getSession()->read('Configurations');

        $currentTenantType = $this->request->getSession()->read('Account.currentTenantType');
        if ($currentTenantType === 'D' || $currentTenantType === 'F') {
            return null;
        }

        $grades = $this->People->Grades->find('list')->toArray();
        $people = $this->People->Legislators->find('all', [
            'contain' => ['People'],
            'fields' => [
                'People.id',
                'People.first_name',
                'People.last_name',
                'People.grade_id',
                'People.grade_number',
            ],
            'conditions' => ['Legislators.legislative_session_id' => $sessionId, 'Legislators.chamber_id' => $chamber],
            'order' => ['People.last_name' => 'asc', 'People.first_name' => 'asc']])->all();

        $calculatedScores = $this->getCalculatedScores($sessionId, $chamber);

        $criterias = $calculatedScores['use_criteria']['criteria'] ?? [];
        $scores = $calculatedScores['scores'];

        $records = [];
        foreach ($people as $person) {
            $personId = $person['person']['id'];
            $record['person_id'] = $personId;
            $record['first_name'] = $person['person']['first_name'];
            $record['last_name'] = $person['person']['last_name'];

            // default scoring
            $record['calculated_score'] = $scores[$personId]['score'] ?? '';
            $record['override_score'] = $person['person']['grade_number'];
            $record['calculated_grade'] = $scores[$personId]['grade'] ?? '';
            $record['override_grade'] = $grades[$person['person']['grade_id']] ?? '';

            // criteria scoring
            if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes') {
                foreach ($criterias as $criteria) {
                    $criteriaId = $criteria['id'];
                    $criteriaName = $criteria['name'];

                    $partyMatch = $calculatedScores['use_criteria']['scores'][$personId][$criteriaId]['partyMatch'] ?? '';
                    if ($partyMatch === 'n/a' || $partyMatch === 'true') {
                        $record[$criteriaName] = $calculatedScores['use_criteria']['scores'][$personId][$criteriaId]['score'] ?? '';
                    } else {
                        $record[$criteriaName] = 'n/a';
                    }
                }
            }

            $records[] = $record;
        }

        $header = [
            'Person ID',
            'First Name',
            'Last Name',
        ];

        // default scoring
        array_push($header, 'Career Calculated Score');
        array_push($header, 'Career Override Score');
        array_push($header, 'Career Calculated Grade');
        array_push($header, 'Career Override Grade');

        // criteria scoring
        if (isset($currentTenantConfigurations['use_criteria']) && $currentTenantConfigurations['use_criteria'] === 'yes') {
            foreach ($criterias as $criteria) {
                array_push($header, $criteria['name']);
            }
        }

        // use the CSV view
        $filename = 'Career Scores (as of ' . FrozenTime::now()->i18nFormat('yyyy-MM-dd') . ').csv';
        $this->setResponse($this->getResponse()->withDownload($filename));
        $this->viewBuilder()
            ->setClassName('CsvView.Csv')
            ->setOptions(['serialize' => 'records', 'header' => $header]);

        $this->set(compact('records'));
    }

    public function bulkUpdateRow()
    {
        $this->viewBuilder()->disableAutoLayout();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $record = $this->People->get($data['id']);

            if (!empty($data['is_enabled'])) {
                $data['is_enabled'] = empty($data['is_enabled']) ? false : true;
            }

            if (isset($data['people_tags'])) {
                $peopleTags = [];

                if (is_array($data['people_tags'])) {
                    foreach ($data['people_tags'] as $tag) {
                        $peopleTags[] = $tag;
                    }
                }

                unset($data['people_tags']);
                $data['tags'] = [
                    '_ids' => $peopleTags,
                ];
            }

            $record = $this->People->patchEntity($record, $data, [
                'associated' => ['Tags' => ['onlyIds' => true]],
            ]);

            if ($this->People->save($record)) {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($data))
                    ->withStatus(200);
            } else {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($record->getErrors()))
                    ->withStatus(400);
            }
        }
    }
}
