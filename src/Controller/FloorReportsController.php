<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Client;

/**
 * FloorReports Controller
 *
 * @property \App\Model\Table\FloorReportsTable $FloorReports
 * @method \App\Model\Entity\FloorReport[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FloorReportsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tenants', 'LegislativeSessions', 'Chambers'],
        ];
        $floorReports = $this->paginate($this->FloorReports);

        $this->set(compact('floorReports'));
    }

    /**
     * View method
     *
     * @param string|null $id Floor Report id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $floorReport = $this->FloorReports->get($id, [
            'contain' => ['Tenants', 'LegislativeSessions', 'Chambers', 'Ratings'],
        ]);
        $legiscanSessions = $this->getLegiscanSessions();

        $this->set(compact('floorReport', 'legiscanSessions'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $floorReport = $this->FloorReports->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data = $this->prepareFloorReportData($data);
            $floorReport = $this->FloorReports->patchEntity($floorReport, $data, [
                'associated' => ['Ratings'],
            ]);
            if ($this->FloorReports->save($floorReport)) {
                $this->Flash->success(__('The floor report has been saved.'));

                return $this->redirect(['action' => 'view', $floorReport->id]);
            }
            $this->Flash->error(__('The floor report could not be saved. Please, try again.'));
        }
        $legislativeSessions = $this->FloorReports->LegislativeSessions->find('list', ['limit' => 200]);
        $chambers = $this->FloorReports->Chambers->find('list', ['limit' => 200]);
        $ratings = $this->FloorReports->Ratings->find('list', ['limit' => 200]);
        $this->set(compact('floorReport', 'legislativeSessions', 'chambers', 'ratings'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Floor Report id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $floorReport = $this->FloorReports->get($id, [
            'contain' => ['Ratings'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            $import_bills = $data['import_bills'] ?? [];
            if ($import_bills) {
                $data = $this->prepareFloorReportData($data);
            } else {
                $logEntry = sprintf(
                    "<strong>Import Log for %s</strong><br /><br />Skipping import of bills.",
                    date('m/d/Y h:i:s A'));
                $data['import_log'] = $logEntry . "<br />--<br />" . $data['import_log'];
            }

            $floorReport = $this->FloorReports->patchEntity($floorReport, $data, [
                'associated' => ['Ratings'],
            ]);
            if ($this->FloorReports->save($floorReport)) {
                $this->Flash->success(__('The floor report has been saved.'));

                return $this->redirect(['action' => 'view', $floorReport->id]);
            }
            $this->Flash->error(__('The floor report could not be saved. Please, try again.'));
        }
        $legislativeSessions = $this->FloorReports->LegislativeSessions->find('list', ['limit' => 200]);
        $chambers = $this->FloorReports->Chambers->find('list', ['limit' => 200]);
        $ratings = $this->FloorReports->Ratings->find('list', ['limit' => 200]);
        $this->set(compact('floorReport', 'legislativeSessions', 'chambers', 'ratings'));
    }

    /**
     * Email Export method
     *
     * @param string|null $id Floor Report id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function emailExport($id = null)
    {
        $this->viewBuilder()->setLayout('ajax');
        $floorReport = $this->FloorReports->get($id, [
            'contain' => ['Tenants', 'LegislativeSessions', 'Chambers', 'Ratings'],
        ]);
        $legislativeSessions = $this->FloorReports->LegislativeSessions->find('all', ['limit' => 200])->toArray();
        $positions = $this->FloorReports->Ratings->Positions->find('all', ['limit' => 200])->toArray();

        $this->set(compact('floorReport', 'legislativeSessions', 'positions'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Floor Report id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $floorReport = $this->FloorReports->get($id);
        if ($this->FloorReports->delete($floorReport)) {
            $this->Flash->success(__('The floor report has been deleted.'));
        } else {
            $this->Flash->error(__('The floor report could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function bulkSetRatingStatus($floorReportId = null, $status = null)
    {
        $this->request->allowMethod(['post']);
        $floorReport = $this->FloorReports->get($floorReportId, [
            'contain' => ['Ratings'],
        ]);
        $ratings = $floorReport->ratings;
        foreach ($ratings as $rating) {
            $rating->is_enabled = $status;
            $this->FloorReports->Ratings->save($rating);
        }
        $this->Flash->success(__('The ratings have been updated.'));
        return $this->redirect(['action' => 'view', $floorReport->id]);
    }

    private function prepareFloorReportData(array $data): array
    {
        $reportData = $this->fetchAndParseReport($data['report_url'], $data['legislative_session_id']);
        $data['import_log'] = $this->updateImportLog($reportData['notFound'], count($reportData['matches']), $data['import_log'] ?? '');
        $data['ratings'] = $reportData['matches'];

        return $data;
    }

    private function fetchAndParseReport(string $reportUrl, string $legislativeSessionId): array
    {
        if (empty($reportUrl) || empty($legislativeSessionId)) {
            return ['matches' => [], 'notFound' => []];
        }

        $floorReportHtml = $this->fetchReportHtml($reportUrl);
        return $this->parseReportHtml($floorReportHtml, $legislativeSessionId);
    }

    private function fetchReportHtml(string $url): string
    {
        $clientOptions = $this->determineClientOptions();
        try {
            $http = new Client();
            $response = $http->get($url, [], $clientOptions);
            return $response->isOk() ? $response->getStringBody() : '';
        } catch (\Exception $e) {
            // Consider logging the error properly.
            return '';
        }
    }

    private function determineClientOptions(): array
    {
        $isLocal = str_contains($this->getRequest()->getSession()->read('Account.currentTenantDomain'), 'localhost');
        return $isLocal ? ['ssl_verify_peer' => false, 'ssl_verify_peer_name' => false] : [];
    }

    private function parseReportHtml(string $html, string $sessionID): array
    {
        $results = ['matches' => [], 'notFound' => []];
        preg_match_all('/Bill=([A-Z]*)[ 0]*([0-9]*)/', $html, $importBills);
        [$matches, $billTypes, $billNumbers] = $importBills;

        $ratings = $this->getRatingsList($sessionID);

        foreach ($matches as $i => $match) {
            $billName = $billTypes[$i] . $billNumbers[$i];
            if (isset($ratings[$billName])) {
                $results['matches'][] = ['id' => $ratings[$billName], '_joinData' => ['sort_order' => $i]];
            } else {
                $results['notFound'][] = ['bill_name' => $billName, 'position' => $i];
            }
        }

        return $results;
    }

    private function getRatingsList(string $legislativeSessionId): array
    {
        $ratings = $this->FloorReports->Ratings->find()
            ->select(['id', 'name'])
            ->where(['legislative_session_id' => $legislativeSessionId])
            ->toArray();

        $ratingList = [];

        foreach ($ratings as $rating) {
            $legiscanName = str_replace(' ', '', $rating->name);
            $ratingList[$legiscanName] = $rating->id;
        }

        return $ratingList;
    }

    private function updateImportLog(array $notFoundBills, int $foundCount, string $existingImportLog = ''): string
    {
        $logEntry = sprintf(
            "<strong>Import Log for %s</strong><br /><br />Success: %d bill(s) found.<br /><br />Error: %d bill(s) not found:<br /><br />",
            date('m/d/Y h:i:s A'), $foundCount, count($notFoundBills)
        );

        foreach ($notFoundBills as $details) {
            $logEntry .= sprintf("%s at position %d<br />", $details['bill_name'], $details['position']);
        }

        return $logEntry . "<br />--<br />" . $existingImportLog;
    }

    private function getLegiscanSessions()
    {
        return $this->FloorReports->Ratings->LegiscanBills->LegiscanSessions->find('list', [
            'order' => ['LegiscanSessions.year_start' => 'asc']
        ])->toArray();
    }
}