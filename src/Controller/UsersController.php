<?php

namespace App\Controller;

use App\Controller\Admin\SignupDomainsController;
use Cake\Core\Configure;
use Cake\Event\EventInterface;
use Cake\Http\Cookie\Cookie;
use Cake\Http\Cookie\CookieCollection;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Mailer;
use Cake\Routing\Router;
use Cake\Utility\Text;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    // todo add back
    // , 'Security', 'Session'

    private $routerClass;
    private $textClass;

    public function __construct(ServerRequest $request = null, Response $response = null, $name = null, \Cake\Event\EventManager $eventManager = null)
    {
        parent::__construct($request, $response, $name, $eventManager);

        $this->routerClass = new Router();
        $this->textClass = new Text();
    }

    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow(['signup', 'login', 'logout', 'finishSignup', 'forgotPassword', 'resetPassword']);
    }

    public function checkSession()
    {
        $this->viewBuilder()->disableAutoLayout();
        $data = ['timeout' => 0];
        if ($this->Auth->user()) {
            $session = $this->request->getSession();
            $data = ['timeout' => $session->read('Config.time')];
        }
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data))
            ->withStatus(200);
    }

    /**
     * admin_login function.
     *
     * @access public
     * @return void
     */
    public function login()
    {

        $this->set('controllerStylesheet', 'public');

        $session = $this->request->getSession();

        $inviteSession = $session->read('Invite');

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            if ($user) {
                $this->Auth->setUser($user);

                // check for inviteSession
                if (empty($inviteSession)) {
                    // no invitation, so continue as normal
                    $this->loginSetAccount($this->request->getAttribute('params'));
                } else {
                    // invitation, so redirect to member-setup

                    $this->redirect([
                        'controller' => 'accounts',
                        'action' => 'invite',
                        $inviteSession->security_code,
                        'plugin' => false]);
                }

                $this->Flash->success(__('Sign in successful'));
            } else {
                $originalRequest = $this->request->getParsedBody();
                unset($originalRequest['password']);
                $this->request = $this->request->withParsedBody($originalRequest);

                $this->Flash->error(__('The Email and Password don\'t match our records, please check them and try signing in again.'));
            }
        }

        $this->set(compact('inviteSession'));
    }

    public function loginSetAccount($params)
    {
        // get accounts
        $userId = $this->Auth->user('id');
        $accounts = (new AccountsController())->getAccounts($userId);

        $user = $this->Users->get($userId);

        $session = $this->request->getSession();
        $session->write('Auth.User.details', $user['user_details']);

        // set logged in cookie for Cloudflare rule bypass
        $getCookieTimeout = Configure::read('Session.timeout') ? Configure::read('Session.timeout') * 60 : ini_get('session.gc_maxlifetime');
        $cookie = new Cookie(
            '_cs_logged_in',
            '1',
            new FrozenTime('+' . $getCookieTimeout . ' seconds'),
            '/',
            '',
            false,
            false
        );
        $cookies = new CookieCollection([$cookie]);
        $this->response = $this->response->withCookieCollection($cookies);

        // if one, set as default and redirect to homepage
        if (sizeof($accounts) === 1) {
            (new AccountsController())->setTenantSession($accounts[0]->tenant_id, $userId);

            return $this->getLoginRedirect();
        }

        // if more than one, redirect to account switcher
        if (isset($params['?']['redirect'])) {
            return $this->redirect([
                'controller' => 'accounts',
                'action' => 'index',
                '?' => ['redirect' => $params['?']['redirect']],
                'plugin' => false,
                'prefix' => false]);
        } else {
            return $this->redirect([
                'controller' => 'accounts',
                'action' => 'index',
                'plugin' => false,
                'prefix' => false]);
        }
    }

    /**
     * admin_logout function.
     *
     * @access public
     * @param null $action
     * @return void
     */
    public function logout($action = null)
    {
        $logoutRedirectURL = $this->Auth->logout();

        if ($action === 'invite') {
            // get invite from sessions
            $session = $this->request->getSession();
            $invite = $session->read('Invite');

            $this->request->getSession()->destroy();

            $session->write('Invite', $invite);

            if ($invite->user->is_enabled) {
                $this->redirect($this->Auth->logout());
            } else {
                $this->redirect(['controller' => 'users', 'action' => 'finishSignup', 'plugin' => false]);
            }

        } else {
            // remove logged in cookie for Cloudflare rule bypass
            $expiredCookie = new Cookie(
                '_cs_logged_in',
                '0',
                new FrozenTime('-1 hour'),
                '/',
                '',
                false,
                false
            );
            $this->response = $this->response->withExpiredCookie($expiredCookie);
            $this->request->getSession()->destroy();
        }

        $parameters = $this->request->getAttribute('params');
        if (isset($parameters['?'])) {
            $logoutRedirectURL .= '?' . http_build_query($parameters['?']);

            if (isset($parameters['?']['source'])) {
                switch ($parameters['?']['source']) {
                    case 'user':
                        $this->Flash->success(__('You have successfully logged out.'));
                        break;
                    case 'timeout':
                        $this->Flash->warning(__('Your browser session has expired to protect your information.'));
                        break;
                }
            }
        }

        $this->redirect($logoutRedirectURL);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $this->paginate = [
            'contain' => [],
            'fields' => [
                'Users.id',
                'Users.email',
                'Users.user_details',
                'Users.is_enabled',
            ]
        ];
        $users = $this->paginate($this->Users);

        $userFields = (new ConfigurationTypesController())->getUserFields();

        $this->set(compact('users', 'userFields'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $is_super = $this->Auth->user('is_super');

        if (!$is_super || $id === null) {
            $id = $this->Auth->user('id');
            $usingSession = true;
        } else {
            $usingSession = false;
        }

        $user = $this->Users->get($id, [
            'contain' => [],
            'fields' => [
                'Users.id',
                'Users.email',
                'Users.password',
                'Users.is_enabled',
                'Users.is_super',
                'Users.created',
                'Users.modified',
                'Users.user_details',
            ]
        ]);

        $userFields = (new ConfigurationTypesController())->getUserFields();

        $this->set(compact('user', 'usingSession', 'userFields'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function signup()
    {
        $this->set('controllerStylesheet', 'public');

        if (Configure::read('users.self_signup_enabled')) {
            $user = $this->Users->newEmptyEntity();
            if ($this->request->is('post')) {
                $data = $this->request->getData();
                $inputEmail = $data['email'];

                $emailValidate = true;
                if (!filter_var($inputEmail, FILTER_VALIDATE_EMAIL)) {
                    $emailValidate = false;
                    $this->Flash->error(__('Please enter a validate email address.'));
                }

                if ($emailValidate) {
                    $addMemberResults = (new AccountsController())->addMember($inputEmail, Configure::read('users.self_signup_tenant'), 'U', true, false);

                    switch ($addMemberResults) {
                        case "ACTIVE_ACCOUNT_EXISTS":
                            $this->Flash->warning(__('You already have an account. Please sign in or reset your password.'));
                            return $this->redirect(['controller' => 'users', 'action' => 'login', 'plugin' => false]);
                        case "PENDING_ACCOUNT_EXISTS":
                            $this->Flash->warning(__('You already have an account. Please check your email to finish setup.'));
                            break;
                        case "USER_ACCOUNT_CREATED":
                            $this->Flash->success(__('Signup was successful! Please check your email.'));
                            break;
                        default:
                            $this->Flash->error(__('Signup was not successful. Please, try again.'));
                    }
                }
            }
            $this->set(compact('user'));
        } else {
            return $this->redirect(['controller' => 'users', 'action' => 'login', 'plugin' => false]);
        }
    }

    public function finishSignup()
    {
        $this->set('controllerStylesheet', 'public');

        $userDetails = ['validation_failed' => [], 'decoded' => [], 'encoded' => json_encode([])];
        $session = $this->request->getSession();
        $inviteSession = $session->read('Invite');

        if (!empty($inviteSession)) {
            if ($inviteSession->user->is_enabled) {

                //user already signed up, redirect to login
                $this->Flash->success('Account already exists. Please, try signing in or <a href="' . $this->routerClass->url([
                        'controller' => 'users',
                        'action' => 'forgot-password']) . '">Forgot password?</a>', ['escape' => false]);

                $this->routerClass->url(['controller' => 'users', 'action' => 'forgotPassword'], true);

                return $this->redirect(['controller' => 'users', 'action' => 'login', 'plugin' => false]);
            }

            $user = $this->Users->get($inviteSession->user->id, [
                'contain' => [],
                'fields' => [
                    'Users.id',
                    'Users.email',
                    'Users.is_enabled'
                ]
            ]);

            $userFields = (new ConfigurationTypesController())->getUserFields();
            $userFieldOptions = (new ConfigurationTypesController())->getUserFieldOptions();

            if ($this->request->is(['patch', 'post', 'put'])) {
                $data = $this->request->getData();

                $data['is_enabled'] = 1;
                $data['is_super'] = 0;

                // prevent non-su from changing these fields
                unset($data['email']);
                unset($data['security_code']);

                // configuration-type fields
                $userDetails = $this->getUserDetails($data, $userFields);

                $customFieldsValidated = true;
                if (empty($userDetails['validation_failed'])) {
                    $data['user_details'] = $userDetails['decoded'];
                } else {
                    $customFieldsValidated = false;
                }

                // set user defaults
                $user = $this->Users->patchEntity($user, $data);
                if ($customFieldsValidated && $this->Users->save($user)) {

                    $emailSignupAllowed = (new SignupDomainsController())->checkSignupDomain($user->email);
                    if ($emailSignupAllowed || isset($inviteSession->invited_by_user_id)) {

                        // auto-login
                        $this->Auth->setUser($user->toArray());

                        // send welcome email
                        $this->sendWelcome();

                        // auto-accept invites for self-signups
                        $selfSignupEnabled = Configure::read('users.self_signup_enabled');
                        if (isset($selfSignupEnabled) && $selfSignupEnabled) {
                            $status = 'ACTIVE';
                        } else {
                            $status = '';
                        }

                        $this->Flash->success(__('User sign up was successful.'));

                        // redirect to invite setup
                        return $this->redirect([
                            'controller' => 'accounts',
                            'action' => 'invite',
                            $inviteSession->security_code,
                            $status,
                            'plugin' => false]);
                    } else {
                        $account = $this->Users->Accounts->find('all', [
                            'contain' => [],
                            'fields' => [
                                'Accounts.id',
                            ],
                            'conditions' => [
                                'Accounts.security_code' => $inviteSession->security_code,
                            ]
                        ])->first();

                        $account->status = 'REVIEW';

                        if ($this->Users->Accounts->save($account)) {
                            $this->Flash->success(__('Signup was successful! Your request has been received and is pending approval.'));
                            $this->sendPendingApprovalEmail($user->email);
                            $session->delete('Invite');
                        } else {
                            // @codeCoverageIgnoreStart
                            $this->Flash->error(__('Signup was not successful. Please, try again.'));
                            // @codeCoverageIgnoreEnd
                        }

                        return $this->redirect([
                            'controller' => 'users',
                            'action' => 'login',
                            'plugin' => false]);
                    }
                } else {
                    $this->Flash->error(__('User sign up failed. Please, try again.'));
                }
            }
        } else {
            // something not right, redirect to login page
            return $this->redirect(['controller' => 'users', 'action' => 'login', 'plugin' => false]);
        }

        $this->set(compact('user', 'inviteSession', 'userFields', 'userFieldOptions', 'userDetails'));
    }

    private function sendPendingApprovalEmail($inputEmail): bool
    {
        $mailer = new Mailer();
        $mailer
            ->setEmailFormat('both')
            ->setTo($inputEmail)
            ->setSubject('[Cornerstone] Account Pending')
            ->viewBuilder()
            ->setTemplate('Users/pending_approval');

        $mailer->send();
        return true;
    }

    public function getUserDetails($data, $userFields = null): array
    {
        $errors = [];
        $validCustomFields = [];
        if (!isset($userFields)) {
            $userFields = (new ConfigurationTypesController())->getUserFields();
        }

        // verify it is a valid custom field before saving
        foreach ($userFields as $userField) {
            $ct = $data['ct'] ?? [];

            // validate is_required fields exist
            if ($userField->is_required === true) {
                if (!isset($ct[$userField->name])) {
                    $errors[] = $userField->name;
                    continue;
                }
            }

            // validate using defined regex
            if (isset($ct[$userField->name])) {
                $validCustomFields[$userField->name] = $ct[$userField->name];

                if (preg_match('/' . $userField->required_regex . '/', (string)$ct[$userField->name], $matches, PREG_OFFSET_CAPTURE) === 1)
                    continue;
                $errors[] = $userField->name;
            }
        }

        return [
            'validation_failed' => $errors,
            'decoded' => $validCustomFields,
            'encoded' => json_encode($validCustomFields)];
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $is_super = $this->Auth->user('is_super');

        if (!$is_super || $id === null) {
            $id = $this->Auth->user('id');
            $usingSession = true;
        } else {
            $usingSession = false;
        }

        $user = $this->Users->get($id, [
            'contain' => [],
            'fields' => [
                'Users.id',
                'Users.email',
                'Users.is_enabled',
                'Users.is_super',
                'Users.user_details',
            ]
        ]);
        $userDetails = [
            'validation_failed' => [],
            'decoded' => $user->user_details,
            'encoded' => json_encode($user->user_details)];
        $userFields = (new ConfigurationTypesController())->getUserFields();
        $userFieldOptions = (new ConfigurationTypesController())->getUserFieldOptions();
        if ($this->request->is(['patch', 'post', 'put'])) {

            $data = $this->request->getData();

            // prevent non-su from changing these fields
            if (!$is_super) {
                unset($data['email']);
                unset($data['password']);
                unset($data['security_code']);
                unset($data['is_enabled']);
                unset($data['is_super']);
            }

            // configuration-type fields
            $userDetails = $this->getUserDetails($data, $userFields);

            $customFieldsValidated = true;
            if (empty($userDetails['validation_failed'])) {
                $data['user_details'] = $userDetails['decoded'];
            } else {
                $customFieldsValidated = false;
            }

            // set user defaults
            $user = $this->Users->patchEntity($user, $data);
            if ($customFieldsValidated && $this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                $session = $this->request->getSession();
                $session->write('Auth.User.details', $userDetails['decoded']);

                if ($usingSession) {
                    return $this->redirect(['action' => 'view']);
                } else {
                    return $this->redirect(['action' => 'index']);
                }
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user', 'usingSession', 'userFields', 'userDetails', 'userFieldOptions'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function editPassword($id = null)
    {
        // TODO in theory a super admin should not need the current password to reset it
        $is_super = $this->Auth->user('is_super');

        if (!$is_super || $id === null) {
            $id = $this->Auth->user('id');
            $usingSession = true;
        } else {
            $usingSession = false;
        }

        $user = $this->Users->get($id, [
            'contain' => [],
            'fields' => [
                'Users.id',
                'Users.password',
                'Users.email'
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $data = $this->request->getData();

            // prevent non-su from changing these fields
            if (!$is_super) {
                unset($data['email']);
                unset($data['password']);
                unset($data['security_code']);
                unset($data['is_enabled']);
                unset($data['is_super']);
            }

            $user = $this->Users->patchEntity($user, $data);

            if (!empty($user->password_update) && !empty($user->password_update_confirm)) {
                $user->password = $user->password_update;
            }

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                if ($usingSession) {
                    return $this->redirect(['action' => 'view']);
                } else {
                    return $this->redirect(['action' => 'index']);
                }
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user', 'usingSession'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if ($this->isUserNonSuperAdmin()) return null;
        $this->request->allowMethod(['post', 'delete']);

        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function forgotPassword()
    {
        $this->set('controllerStylesheet', 'public');

        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $user = $this->Users->find('all')
                ->where(['email' => $data['email']])
                ->first();

            if (!is_null($user)) {
                $security_code = $this->textClass->uuid();

                if ($this->Users->updateAll(['security_code' => $security_code], ['id' => $user->id])) {
                    $this->sendResetEmail($user, $security_code);

                }
            }
            $this->Flash->success(__('Copy the reset code from your email and paste it below.'));
            $this->redirect(['action' => 'resetPassword']);
        }
    }

    public function resetPassword($security_code = null)
    {
        $this->set('controllerStylesheet', 'public');

        $user = [];
        if ($this->request->is('post')) {

            $request = $this->request->getData();

            // Clear passkey and timeout
            $security_code = $request['security_code'];

            $user = $this->Users->find('all')
                ->where(['security_code' => $security_code])
                ->first();

            if (!$user) {
                $this->Flash->error('Invalid or expired security token. Please check your email or try again.');
                $this->redirect(['action' => 'forgot-password']);
            } else {
                // remove security code
                $request['security_code'] = null;

                $user = $this->Users->patchEntity($user, $request);
                $results = $this->Users->save($user);
                if ($results) {

                    // notify the users about password reset
                    $this->sendResetDone($user->id);

                    $this->Flash->success(__('Your password has been updated.'));
                    return $this->redirect(['action' => 'login']);
                } else {
                    $this->Flash->error(__('The password could not be updated. Please, try again.'));
                }
            }
        }

        $this->set(compact('user', 'security_code'));
    }

    public function sendWelcome()
    {
        // inviting user information
        $currentUserId = $this->Auth->user('id');

        $user = $this->Users->get($currentUserId, [
            'contain' => [],
            'fields' => [
                'Users.email'
            ]
        ]);

        $mailer = new Mailer();
        $mailer
            ->setEmailFormat('both')
            ->setTo($user->email)
            ->setSubject('[Cornerstone] Get Started with Cornerstone!')
            ->setViewVars(['user' => $user])
            ->viewBuilder()
            ->setTemplate('Users/welcome');

        return $mailer->send();
    }

    private function sendResetEmail($user, $security_code)
    {

        $url = $this->routerClass->url([
                'controller' => 'users',
                'action' => 'reset-password'], true) . '/' . $security_code;

        $mailer = new Mailer();
        $mailer
            ->setEmailFormat('both')
            ->setTo($user->email, $user->first_name)
            ->setSubject('[Cornerstone] Instructions for changing your password')
            ->setViewVars(['url' => $url, 'email' => $user->email, 'security_code' => $security_code])
            ->viewBuilder()
            ->setTemplate('Users/reset_password');

        if ($mailer->send()) {
            $this->Flash->success(__('We have sent an email to ' . $user->email . ' with further instructions.'));
        } else {
            // @codeCoverageIgnoreStart
            $this->Flash->error(__('Error sending email: '));
            // @codeCoverageIgnoreEnd
        }
    }

    private function sendResetDone($id)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
            'fields' => [
                'Users.email'
            ]
        ]);

        $ip = $this->request->clientIp();

        $mailer = new Mailer();
        $mailer
            ->setEmailFormat('both')
            ->setTo($user->email)
            ->setSubject('[Cornerstone] Your password has been changed')
            ->setViewVars(['user' => $user, 'ip' => $ip])
            ->viewBuilder()
            ->setTemplate('Users/reset_password_done');

        if (!$mailer->send()) {
            // @codeCoverageIgnoreStart
            $this->Flash->error(__('Error sending email: '));
            // @codeCoverageIgnoreEnd
        }
    }

}
