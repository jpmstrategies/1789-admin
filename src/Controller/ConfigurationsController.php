<?php

namespace App\Controller;

use App\Model\Entity\Configuration;
use App\Model\Table\ConfigurationOptionsTable;
use App\Model\Table\ConfigurationsTable;
use Cake\Datasource\ResultSetInterface;
use Cake\Http\Response;
use Cake\I18n\FrozenTime;
use Exception;

/**
 * Configurations Controller
 *
 * @property ConfigurationsTable $Configurations
 * @property ConfigurationOptionsTable $ConfigurationOptions
 *
 * @method Configuration[]|ResultSetInterface paginate($object = null, array $settings = [])
 */
class ConfigurationsController extends AppController
{

    public function bulkUpdateRow(): ?Response
    {
        $this->viewBuilder()->disableAutoLayout();

        if ($this->isUserNonAdmin()) return null;

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $record = $this->Configurations->get($data['id']);
            $record = $this->Configurations->patchEntity($record, $data);
            if ($this->Configurations->save($record)) {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($data))
                    ->withStatus(200);
            } else {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode($record->getErrors()))
                    ->withStatus(400);
            }
        }
        return $this->response->withType('application/json')
            ->withStatus(405);
    }

    private function getConfigurationOptions(): array
    {
        $query = $this->Configurations->ConfigurationTypes->ConfigurationOptions->find('all', [
            'currentTenantId' => $this->currentTenantId,
            'order' => [
                'ConfigurationOptions.configuration_type_id' => 'ASC',
                'ConfigurationOptions.sort_order' => 'ASC'
            ],
        ]);

        $configurationOptions = [];
        foreach ($query->toArray() as $configurationOption) {
            $configurationTypeId = $configurationOption['configuration_type_id'];
            $name = $configurationOption['name'];
            $value = $configurationOption['value'];

            if (!isset($configurationOptions[$configurationTypeId])) {
                $configurationOptions[$configurationTypeId] = [];
                $configurationOptions[$configurationTypeId][''] = '-- Select --';
            }

            $configurationOptions[$configurationTypeId][$value] = $name;
        }

        return $configurationOptions;
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        if ($this->isUserNonAdmin()) return null;

        $currentTab = $this->request->getQuery('tab');

        // insert missing configuration data
        $configurationTypeController = new ConfigurationTypesController();
        $configurationTypeController->insertConfigurations($this->currentTenantId, 'tenant');
        $configurations = $this->Configurations->find('all', [
            'contain' => ['ConfigurationTypes'],
            'fields' => [
                'ConfigurationTypes.id',
                'ConfigurationTypes.name',
                'ConfigurationTypes.description',
                'ConfigurationTypes.data_type',
                'ConfigurationTypes.tab',
                'ConfigurationTypes.default_value',
                'ConfigurationTypes.is_editable',
                'ConfigurationTypes.is_required',
                'ConfigurationTypes.required_message',
                'ConfigurationTypes.required_regex',
                'ConfigurationTypes.field_mask',
                'Configurations.id',
                'Configurations.value'
            ],
            'conditions' => [
                'ConfigurationTypes.is_enabled' => true
            ],
            'order' => [
                'ConfigurationTypes.name' => 'ASC'
            ],
        ]);
        $configurationOptions = $this->getConfigurationOptions();

        $this->set(compact('configurations', 'configurationOptions', 'currentTab'));
    }

    public function getTenantConfigurations($tenantId): array
    {
        $configurationResults = $this->Configurations->find('all', [
            'currentTenantId' => $tenantId,
            'contain' => ['ConfigurationTypes'],
            'fields' => [
                'ConfigurationTypes.name',
                'Configurations.value'
            ],
            'conditions' => [
                'ConfigurationTypes.is_enabled' => true,
                'ConfigurationTypes.tab' => 'admin',
            ]
        ])->toArray();

        $tenantConfigurations = [];
        foreach ($configurationResults as $configuration) {
            $key = $configuration['configuration_type']->name;
            $value = $configuration['value'];

            $tenantConfigurations[$key] = $value;
        }

        return $tenantConfigurations;
    }

    public function export()
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $configurationResults = $this->Configurations->find('all', [
            'contain' => ['ConfigurationTypes', 'Tenants'],
            'fields' => [
                'tenantName' => 'Tenants.name',
                'configurationName' => 'ConfigurationTypes.name',
                'configurationValue' => 'Configurations.value'],
            'conditions' => [
                'ConfigurationTypes.is_enabled' => true
            ]])->toArray();

        $configurations = [];
        foreach ($configurationResults as $configurationResult) {

            $temp = [];

            $configurationName = $configurationResult['configurationName'];
            $configurationValue = $configurationResult['configurationValue'];

            $temp['name'] = $configurationName;
            $temp['value'] = $configurationValue;

            array_push($configurations, $temp);
        }

        $exports = [];
        $exports['tenantName'] = $this->currentTenantName;
        $exports['exportUTC'] = FrozenTime::now()->i18nFormat('yyyy-MM-dd hh:mm:ss');
        $exports['configurations'] = $configurations;

        // use the Json view
        $this->viewBuilder()->setClassName('Json');
        $this->set(compact('exports'));
        $this->viewBuilder()->setOption('serialize', ['exports']);

        // setup filename
        // todo not sure why downloads don't contain the content
        $this->response->withDownload(FrozenTime::now()->i18nFormat('yyyy-MM-dd') . '_configurations.json');
    }

    public function import()
    {
        if ($this->isUserNonSuperAdmin()) return null;

        if ($this->request->is('post')) {
            $attachment = $this->request->getUploadedFile('file');
            $tempFileLocation = $attachment->getStream()->getMetadata('uri');
            $uploadData = json_decode(file_get_contents($tempFileLocation), true);
            $updates = $this->parseConfigurationUpload($uploadData);

            if (sizeof($updates) > 0) {
                // delete existing records
                $this->Configurations->deleteAll(['Configurations.tenant_id' => $this->currentTenantId]);

                try {
                    /// add new records
                    $oNewEntities = $this->Configurations->newEntities($updates);
                    $this->Configurations->saveManyOrFail($oNewEntities);
                    $this->Flash->success(__('Bulk import was successful.'));
                    return $this->redirect(['controller' => 'Configurations', 'action' => 'index']);
                } catch (Exception) {
                    // insert configurations again since they were bulk deleted above
                    $configurationTypeController = new ConfigurationTypesController();
                    $configurationTypeController->insertConfigurations($this->currentTenantId, 'tenant');

                    $this->Flash->error(__('The configurations could not be imported. Please, try again.'));
                }
            }
        }
    }

    private function parseConfigurationUpload($configurations): array
    {
        $configurationTypeResults = $this->Configurations->find('all', [
            'contain' => ['ConfigurationTypes'],
            'fields' => [
                'configurationTypeId' => 'ConfigurationTypes.id',
                'configurationName' => 'ConfigurationTypes.name'],
            'conditions' => [
                'ConfigurationTypes.is_enabled' => true
            ]])->toArray();

        $configurationTypes = [];
        foreach ($configurationTypeResults as $configurationTypeResult) {

            $configurationTypes[$configurationTypeResult['configurationName']] = $configurationTypeResult['configurationTypeId'];
        }

        $updates = [];
        foreach ($configurations['exports']['configurations'] as $configuration) {

            $configurationName = $configuration['name'];
            $configurationValue = $configuration['value'];

            if (isset($configurationTypes[$configurationName])) {
                array_push($updates, [
                    'configuration_type_id' => $configurationTypes[$configurationName],
                    'value' => $configurationValue]);
            } else {
                $this->Flash->error(__('Invalid configuration: ' . $configurationName));
            }
        }

        return $updates;
    }
}
