<?php

namespace App\Controller;


use App\Controller\Admin\SignupDomainsController;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Event\EventInterface;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Mailer;
use Cake\ORM\Locator\TableLocator;
use Cake\Routing\Router;
use Cake\Utility\Text;

/**
 * Accounts Controller
 *
 * @property \App\Model\Table\AccountsTable $Accounts
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\Account[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AccountsController extends AppController
{

    private $routerClass;
    private $tableLocatorClass;
    private $textClass;

    public function __construct(ServerRequest $request = null, Response $response = null, $name = null, \Cake\Event\EventManager $eventManager = null)
    {
        parent::__construct($request, $response, $name, $eventManager);

        $this->routerClass = new Router();
        $this->tableLocatorClass = new TableLocator();
        $this->textClass = new Text();
    }

    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow(['invite', 'verify']);
    }

    /**
     * Index method
     *
     * @return Response|void
     */
    public function index()
    {
        $accounts = $this->getAccounts();

        // redirect to dashboard because there is only one tenant to select
        if (sizeof($accounts) === 1) {
            return $this->getLoginRedirect();
        }

        $this->set(compact('accounts'));
    }

    public function getAccounts($userId = null, $tenantId = null): array
    {
        $conditions = ['Tenants.is_enabled' => TRUE];

        if (isset($tenantId)) {
            $conditions = ['Tenants.id' => $tenantId];
        }

        $is_super = $this->Auth->user('is_super');
        if ($is_super) {
            $accounts = $this->Accounts->Tenants->find('all', [
                'fields' => [
                    // super admin should always have tenant admin permissions
                    'account_role' => "'A'",
                    'tenant_id' => 'Tenants.id',
                    'tenant_name' => 'Tenants.name',
                    'tenant_state' => 'Tenants.state',
                    'tenant_domain' => 'Tenants.domain_name',
                    'tenant_type' => 'Tenants.type',
                    'data_source' => 'Tenants.data_source',
                    'has_tracking' => 'Tenants.has_tracking',
                    'id_public' => 'Tenants.id_public'
                ],
                'conditions' => $conditions,
                'order' => ['tenant_name' => 'ASC']
            ])->toArray();
        } else {
            if (empty($userId)) {
                $userId = $this->Auth->user('id');
            }

            $conditions['Accounts.user_id'] = $userId;
            $conditions['Accounts.status'] = 'ACTIVE';
            $accounts = $this->Accounts->find('all', [
                'contain' => ['Tenants'],
                'fields' => [
                    'account_role' => 'Accounts.role',
                    'tenant_id' => 'Tenants.id',
                    'tenant_name' => 'Tenants.name',
                    'tenant_state' => 'Tenants.state',
                    'tenant_domain' => 'Tenants.domain_name',
                    'tenant_type' => 'Tenants.type',
                    'data_source' => 'Tenants.data_source',
                    'has_tracking' => 'Tenants.has_tracking'
                ],
                'conditions' => $conditions,
                'order' => ['tenant_name' => 'ASC']
            ])->toArray();
        }

        if (empty($tenantId)) {
            // only set if not a specific tenant selection
            $session = $this->request->getSession();
            $session->write('Account.numberOfTenants', sizeOf($accounts));
        }

        return $accounts;
    }

    /**
     * setActiveAccount method
     *
     * @return void
     */
    public function setActiveAccount()
    {
        $this->request->allowMethod(['post']);

        // get parameter
        $pass = $this->request->getParam('pass');

        if (isset($pass[0])) {
            // set session
            $this->setTenantSession($pass[0]);

            $this->getLoginRedirect();

            $this->Flash->success(__('Successfully switched memberships!'));
        }

        // default to showing account switch
        $this->redirect(['controller' => 'accounts', 'action' => 'index']);
    }

    public function setTenantSession($tenantId, $userId = null)
    {
        $accounts = $this->getAccounts($userId, $tenantId);

        if (!empty($accounts[0])) {
            $tenantAccount = $accounts[0];

            $session = $this->request->getSession();
            $session->write('Account.currentTenantId', $tenantAccount->tenant_id);
            $session->write('Account.currentTenantRole', $tenantAccount->account_role);
            $session->write('Account.currentTenantName', $tenantAccount->tenant_name);
            $session->write('Account.currentTenantState', $tenantAccount->tenant_state);
            $session->write('Account.currentTenantDomain', $tenantAccount->tenant_domain);
            $session->write('Account.currentTenantIdPublic', $tenantAccount->id_public);
            $session->write('Account.currentTenantType', $tenantAccount->tenant_type);
            $session->write('Account.currentTenantDataSource', $tenantAccount->data_source);
            $session->write('Account.currentTenantFeatures', [
                'has_tracking' => $tenantAccount->has_tracking
            ]);

            // get configurations
            $tenantConfigurations = (new ConfigurationsController())->getTenantConfigurations($tenantAccount->tenant_id);
            $session->write('Configurations', $tenantConfigurations);
        } else {
            $this->Flash->error(__('Access denied'));
        }
    }

    /**
     * Index method
     *
     * @return void
     */
    public function members()
    {
        if ($this->isUserNonAdmin()) return null;

        $currentUserId = $this->Auth->user('id');

        $this->paginate = [
            'contain' => ['Users'],
            'fields' => [
                'Users.id',
                'Users.email',
                'Users.user_details',
                'Accounts.id',
                'Accounts.role',
                'Accounts.status',
                'Accounts.created',
            ],
            'conditions' => [
                'Accounts.tenant_id' => $this->currentTenantId,
                'Users.is_super' => FALSE
            ],
            'order' => [
                'Accounts.created' => 'desc'
            ]
        ];
        $accounts = $this->paginate($this->Accounts);

        $userFields = (new ConfigurationTypesController())->getUserFields();

        $this->set(compact('accounts', 'currentUserId', 'userFields'));
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->isUserNonAdmin()) return null;

        $account = $this->Accounts->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $inputEmail = $data['email'];
            $inputRole = $data['role'];

            $bAddMember = $this->addMember($inputEmail, $this->currentTenantId, $inputRole, false, false);

            match ($bAddMember) {
                "ACCOUNT_ADDED" => $this->Flash->success(__('Invitation sent to ' . $inputEmail)),
                "ACTIVE_ACCOUNT_EXISTS" => $this->Flash->error(__($inputEmail . ' is already a member!')),
                "USER_ACCOUNT_CREATED" => $this->Flash->success(__('Invitation sent to ' . $inputEmail)),
                default => $this->Flash->error(__('The invite was not successful. Please, try again.')),
            };

            $this->redirect(['controller' => 'accounts', 'action' => 'members', 'plugin' => false]);
        }

        $this->set(compact('account'));
    }

    public function bulkAdd()
    {
        if ($this->isUserNonAdmin()) return null;

        $account = $this->Accounts->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $inputRole = $data['role'];

            $inputEmails = explode("\r\n", trim((string)$data['emails']));

            foreach (array_unique($inputEmails) as $inputEmail) {
                if (filter_var($inputEmail, FILTER_VALIDATE_EMAIL)) {
                    $this->addMember($inputEmail, $this->currentTenantId, $inputRole, false, true);
                } else {
                    $this->Flash->error(__('Invalid email address: ' . $inputEmail));
                }
            }

            $this->Flash->success(__('Bulk invite successful. Email sending can take up to 15 minutes to complete.'));

            $this->redirect(['controller' => 'accounts', 'action' => 'members', 'plugin' => false]);
        }

        $this->set(compact('account'));
    }

    public function addMember($inputUserEmail, $inputTenantId, $inputAccountRole, $bSelfInvite, $bAddToQueue)
    {
        $security_code = $this->textClass->uuid();
        $existingUser = $this->Accounts->Users->find('all', [
            'contains' => [''],
            'fields' => [
                'Users.id'
            ],
            'conditions' => [
                'Users.email' => $inputUserEmail
            ]
        ])->toArray();

        if (sizeof($existingUser) === 1) {

            $uid = $existingUser[0]['id'];

            $accountCheck = $this->Accounts->find('all', [
                'contains' => [],
                'conditions' => [
                    'Accounts.user_id' => $uid,
                    'Accounts.tenant_id' => $inputTenantId
                ]
            ])->first();

            if (empty($accountCheck)) {

                $accountsTable = $this->tableLocatorClass->get('Accounts');
                $account = $accountsTable->newEmptyEntity();

                $account->user_id = $uid;
                $account->tenant_id = $inputTenantId;
                $account->role = $inputAccountRole;
                $account->status = 'PENDING';
                $account->security_code = $security_code;

                if ($this->Auth->user('id')) {
                    $account->invited_by_user_id = $this->Auth->user('id');
                }

                if ($this->Accounts->save($account)) {
                    $this->sendMemberInvite($inputUserEmail, $inputTenantId, $bSelfInvite, $bAddToQueue);
                    return 'ACCOUNT_ADDED';
                } else {
                    return 'ACCOUNT_CREATION_FAILED';
                }
            } else {
                //already added
                switch ($accountCheck['status']) {
                    case 'PENDING':
                        $this->sendMemberInvite($inputUserEmail, $inputTenantId, $bSelfInvite, $bAddToQueue);
                        return 'PENDING_ACCOUNT_EXISTS';
                    case 'ACTIVE':
                        return 'ACTIVE_ACCOUNT_EXISTS';
                    default:
                        // @codeCoverageIgnoreStart
                        throw new NotFoundException(__('Account creation failed'));
                    // @codeCoverageIgnoreEnd
                }
            }
            // otherwise add new record
        } else {

            // create new user
            $usersTable = $this->tableLocatorClass->get('Users');
            $user = $usersTable->newEmptyEntity();

            $user->email = $inputUserEmail;
            $user->password = $this->textClass->uuid();
            $user->is_enable = 0;
            $user->is_super = 0;

            $userAccount = $usersTable->Accounts->newEmptyEntity();
            $userAccount->tenant_id = $inputTenantId;
            $userAccount->role = $inputAccountRole;
            $userAccount->status = 'PENDING';
            $userAccount->security_code = $security_code;

            if ($this->Auth->user('id')) {
                $userAccount->invited_by_user_id = $this->Auth->user('id');
            }

            $user->accounts = [$userAccount];

            if ($this->Accounts->Users->save($user)) {
                $this->sendMemberInvite($inputUserEmail, $inputTenantId, $bSelfInvite, $bAddToQueue);
                return 'USER_ACCOUNT_CREATED';
            } else {
                return 'USER_ACCOUNT_FAILED';
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Account id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        $account = $this->Accounts->get($id, [
            'contain' => ['Users'],
            'fields' => [
                'Accounts.id',
                'Accounts.tenant_id',
                'Accounts.user_id',
                'Accounts.role',
                'Users.email',
                'Users.user_details',
            ]
        ]);
        if ($this->isUserNonAdmin() || $this->isUserRecordOwner($account->user_id) || $this->isNotUserTenant($account->tenant_id)) return null;

        $userDetails = [
            'validation_failed' => [],
            'decoded' => $account->user->user_details,
            'encoded' => json_encode($account->user->user_details)];
        $userFields = (new ConfigurationTypesController())->getUserFields();
        $userFieldOptions = (new ConfigurationTypesController())->getUserFieldOptions();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            // unset fields they can't modify
            unset($data['Users']);
            unset($data['user_id']);
            unset($data['security_code']);

            // configuration-type fields
            $userDetails = (new UsersController())->getUserDetails($data, $userFields);

            $customFieldsValidated = true;
            $userSaveResults = false;
            if (empty($userDetails['validation_failed'])) {
                $user = $this->Accounts->Users->get($account->user_id);
                $user->user_details = $userDetails['decoded'];
                $userSaveResults = $this->Accounts->Users->save($user);
            } else {
                $customFieldsValidated = false;
            }

            $account = $this->Accounts->patchEntity($account, $data);
            if ($customFieldsValidated && $userSaveResults && $this->Accounts->save($account)) {
                $this->Flash->success(__('The member has been saved.'));

                return $this->redirect(['action' => 'members']);
            } else {
                $this->Flash->error(__('The member could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('account', 'userFields', 'userFieldOptions', 'userDetails'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Account id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $account = $this->Accounts->get($id);
        if ($this->isUserNonAdmin() || $this->isUserRecordOwner($account->user_id) || $this->isNotUserTenant($account->tenant_id)) return null;

        if ($this->Accounts->delete($account)) {
            $this->Flash->success(__('The membership has been revoked.'));
        } else {
            $this->Flash->error(__('The membership could not be revoked. Please, try again.'));
        }
        return $this->redirect(['action' => 'members']);
    }

    public function verify($security_code = null)
    {
        $this->set('controllerStylesheet', 'public');

        // validate the id is valid
        $account = $this->Accounts->find('all', [
            'contain' => ['Tenants', 'Users'],
            'fields' => [
                'Accounts.id',
                'Accounts.status',
                'Accounts.security_code',
                'Accounts.invited_by_user_id',
                'Tenants.id',
                'Tenants.name',
                'Users.id',
                'Users.email',
                'Users.is_enabled'
            ],
            'conditions' => [
                'Accounts.security_code' => $security_code,
                'Accounts.status IN' => ['PENDING', 'APPROVED']
            ]
        ])->first();

        // if no results
        if (empty($account)) {
            $this->request->getSession()->destroy();
            $this->Flash->error('Invalid or expired invite token. Please check your email or try again.');
            return $this->redirect(['controller' => 'users', 'action' => 'login', 'plugin' => false]);

        } else {
            if ($this->Auth->user('id') !== $account->user->id) {
                $this->request->getSession()->destroy();
            }

            // write to session
            $session = $this->request->getSession();
            $session->write('Invite', $account);

            // redirect existing user to signin page
            return $this->redirect([
                'controller' => 'users',
                'action' => 'finishSignup',
                'plugin' => false]);
        }
    }

    public function invite($security_code = null, $status = null)
    {
        $this->set('controllerStylesheet', 'public');

        // validate the id is valid
        $account = $this->Accounts->find('all', [
            'contain' => ['Tenants', 'Users'],
            'fields' => [
                'Accounts.id',
                'Accounts.status',
                'Accounts.security_code',
                'Accounts.invited_by_user_id',
                'Tenants.id',
                'Tenants.name',
                'Users.id',
                'Users.email',
                'Users.is_enabled'
            ],
            'conditions' => [
                'Accounts.security_code' => $security_code,
                'Accounts.status IN' => ['PENDING', 'APPROVED']
            ]
        ])->first();

        // if no results
        if (empty($account)) {
            $this->Flash->error('Invalid or expired invite token. Please check your email or try again.');
            return $this->redirect(['controller' => 'users', 'action' => 'login', 'plugin' => false]);

        } else {
            if (empty($status)) {
                $status = 'SHOW_ACCEPT';
            }
            $startingStatus = $account->status;
            // for users that go through the approval process, we want to auto-accept the invite
            if ($account->status === 'APPROVED' && Configure::read('users.self_signup_enabled')) {
                $status = 'ACTIVE';
            }

            if ($status !== 'SHOW_ACCEPT') {
                $data = ['id' => $account['id'], 'security_code' => null, 'status' => $status];

                $account = $this->Accounts->patchEntity($account, $data);
                if ($this->Accounts->save($account)) {
                    $this->Flash->success(__('Invitation was processed!'));

                    $user = $this->Accounts->Users->get($account->user->id, [
                        'contain' => [],
                        'fields' => [
                            'Users.id',
                            'Users.email',
                            'Users.is_enabled'
                        ]
                    ]);

                    $usersController = new UsersController();

                    // send welcome email if user had to go through approval process
                    if ($startingStatus === 'APPROVED') {
                        $usersController->sendWelcome();
                    }

                    return $usersController->loginSetAccount(null);
                } else {
                    $this->Flash->error(__('Invitation was not saved. Please, try again.'));
                }
            }
        }

        $this->set(compact('status', 'account'));
    }

    public function resendInvite($id)
    {
        if ($this->isUserNonAdmin()) return null;

        $user = $this->Accounts->get($id, [
            'contain' => ['Users'],
            'fields' => [
                'Users.email'
            ],
            'conditions' => [
                'Accounts.tenant_id' => $this->currentTenantId
            ]
        ]);

        if (sizeof($user->toArray()) === 1) {
            $userEmail = $user['user']['email'];
            $this->sendMemberInvite($userEmail, $this->currentTenantId, false, false);
            $this->Flash->success(__('An invitation has been re-sent to ' . $userEmail));
        } else {
            // @codeCoverageIgnoreStart
            $this->Flash->error(__('The invitation failed to send. Please, try again.'));
            // @codeCoverageIgnoreEnd
        }
        return $this->redirect(['action' => 'members']);

    }

    public function exportCsv()
    {
        if ($this->isUserNonAdmin()) return null;

        $userFields = (new ConfigurationTypesController())->getUserFields();

        $query = $this->Accounts->find('all', ['contain' => ['Users'], 'fields' => [
            'Users.id',
            'Users.email',
            'Users.user_details',
            'Users.created',
            'Users.modified'
        ], 'conditions' => [
            'Accounts.tenant_id' => $this->currentTenantId,
            'Users.is_super' => FALSE
        ]]);
        $accounts = $query->toArray();

        $records = [];
        foreach ($accounts as $account) {
            $userDetails = $account['user']->user_details;
            $record = ['user_id' => $account['user']['id'], 'email' => $account['user']['email'], 'created' => $account['user']['created'], 'modified' => $account['user']['modified']];

            foreach ($userFields as $userField) {
                $record[$userField['name']] = $userDetails[$userField['name']] ?? null;
            }

            $records[] = $record;
        }

        $header = ['user_id', 'email', 'created', 'modified'];

        foreach ($userFields as $userField) {
            $header[] = $userField['name'];
        }

        // use the CSV view
        $filename = 'Users (as of ' . FrozenTime::now()->i18nFormat('yyyy-MM-dd') . ').csv';
        $this->setResponse($this->getResponse()->withDownload($filename));
        $this->viewBuilder()
            ->setClassName('CsvView.Csv')
            ->setOptions(['serialize' => 'records', 'header' => $header]);

        $this->set(compact('records'));
    }

    public function approveUser($id)
    {
        if ($this->isUserNonAdmin()) return null;

        if ($this->request->is('post')) {
            $account = $this->Accounts->find('all', [
                'contain' => ['Users'],
                'fields' => [
                    'Accounts.id',
                    'Accounts.security_code',
                    'Users.email'
                ],
                'conditions' => [
                    'Accounts.tenant_id' => $this->currentTenantId,
                    'Accounts.id' => $id,
                ]
            ])->first();

            $userEmail = $account['user']['email'];
            $securityCode = $account['security_code'];

            $account->status = 'APPROVED';

            if ($this->Accounts->save($account)) {
                $this->sendApprovedEmail($userEmail, $securityCode);
                $this->Flash->success(__('User has been approved. An email has been sent to them with further instructions.'));
            } else {
                // @codeCoverageIgnoreStart
                $this->Flash->error(__('The user could not be approved. Please, try again.'));
                // @codeCoverageIgnoreEnd
            }
        }
        return $this->redirect(['action' => 'members']);
    }

    public function declineUser($id)
    {
        if ($this->isUserNonAdmin()) return null;

        if ($this->request->is('post')) {
            $account = $this->Accounts->find('all', [
                'contain' => ['Users'],
                'fields' => [
                    'Accounts.id',
                    'Users.email'
                ],
                'conditions' => [
                    'Accounts.tenant_id' => $this->currentTenantId,
                    'Accounts.id' => $id,
                ]
            ])->first();

            $userEmail = $account['user']['email'];

            $account->status = 'DISABLED';

            if ($this->Accounts->save($account)) {
                $this->sendDeclinedEmail($userEmail);
                $this->Flash->success(__('User has been declined and they have been notified via email.'));
            } else {
                // @codeCoverageIgnoreStart
                $this->Flash->error(__('The user could not be declined. Please, try again.'));
                // @codeCoverageIgnoreEnd
            }
        }
        return $this->redirect(['action' => 'members']);
    }

    public function disableUser($id)
    {
        if ($this->isUserNonAdmin()) return null;

        if ($this->request->is('post')) {
            $account = $this->Accounts->find('all', [
                'contain' => ['Users'],
                'fields' => [
                    'Accounts.id',
                ],
                'conditions' => [
                    'Accounts.tenant_id' => $this->currentTenantId,
                    'Accounts.id' => $id,
                ]
            ])->first();

            $account->status = 'DISABLED';

            if ($this->Accounts->save($account)) {
                $this->Flash->success(__('User has been disabled.'));
            } else {
                // @codeCoverageIgnoreStart
                $this->Flash->error(__('The user could not be disabled. Please, try again.'));
                // @codeCoverageIgnoreEnd
            }
        }
        return $this->redirect(['action' => 'members']);
    }

    public function enableUser($id)
    {
        if ($this->isUserNonAdmin()) return null;

        if ($this->request->is('post')) {
            $account = $this->Accounts->find('all', [
                'fields' => [
                    'Accounts.id',
                ],
                'conditions' => [
                    'Accounts.tenant_id' => $this->currentTenantId,
                    'Accounts.id' => $id,
                ]
            ])->first();


            $account->status = 'ACTIVE';

            if ($this->Accounts->save($account)) {
                $this->Flash->success(__('User has been enabled.'));
            } else {
                // @codeCoverageIgnoreStart
                $this->Flash->error(__('The user could not be enabled. Please, try again.'));
                // @codeCoverageIgnoreEnd
            }
        }
        return $this->redirect(['action' => 'members']);
    }

    private function sendMemberInvite($inputEmail, $inputTenantId, $bSelfInvite, $bAddToQueue): bool
    {
        // find account
        $account = $this->Accounts->find('all', [
            'contain' => ['Users'],
            'fields' => [
                'Accounts.id',
                'Accounts.security_code'
            ],
            'conditions' => [
                'Accounts.tenant_id' => $inputTenantId,
                'Users.email' => $inputEmail
            ]
        ])->first();

        $url = $this->routerClass->url([
                'controller' => 'accounts',
                'action' => 'verify'], true) . '/' . $account['security_code'];

        // tenant information
        $tenants = $this->tableLocatorClass->get('Tenants');
        $tenant = $tenants->get($inputTenantId, [
            'contain' => [],
            'fields' => [
                'Tenants.name'
            ]
        ]);

        // inviting user information
        $mailer = new Mailer();
        if ($bSelfInvite) {
            $mailer
                ->setEmailFormat('both')
                ->setTo($inputEmail)
                ->setSubject('[Cornerstone] Verify your email address')
                ->setViewVars(['url' => $url])
                ->viewBuilder()
                ->setTemplate('Accounts/verify_email');
        } else {
            $currentUserId = $this->Auth->user('id');
            $users = $this->tableLocatorClass->get('Users');
            $user = $users->get($currentUserId, [
                'contain' => [],
                'fields' => [
                    'Users.email',
                    'Users.is_super'
                ]
            ]);

            $mailer
                ->setEmailFormat('both')
                ->setTo($inputEmail)
                ->setSubject('[Cornerstone] You\'ve been invited to join and manage an account on Cornerstone')
                ->setViewVars(['tenant' => $tenant, 'user' => $user, 'url' => $url])
                ->viewBuilder()
                ->setTemplate('Accounts/user_invite');
        }

        if ($bAddToQueue) {
            $this->loadModel('Queue.QueuedJobs')->createJob(
                'Queue.Email',
                ['settings' => $mailer]
            );
        } else {
            $mailer->send();
        }
        return true;
    }

    private function sendApprovedEmail($inputEmail, $securityCode): bool
    {
        $url = $this->routerClass->url([
                'controller' => 'accounts',
                'action' => 'verify'], true) . '/' . $securityCode;

        $mailer = new Mailer();
        $mailer
            ->setEmailFormat('both')
            ->setTo($inputEmail)
            ->setSubject('[Cornerstone] Account Approved')
            ->setViewVars(['url' => $url])
            ->viewBuilder()
            ->setTemplate('Accounts/user_approved');

        $mailer->send();
        return true;
    }

    private function sendDeclinedEmail($inputEmail): bool
    {
        $mailer = new Mailer();
        $mailer
            ->setEmailFormat('both')
            ->setTo($inputEmail)
            ->setSubject('[Cornerstone] Your Account')
            ->viewBuilder()
            ->setTemplate('Accounts/user_declined');

        $mailer->send();
        return true;
    }

    private function isUserRecordOwner($userId): bool
    {
        $currentUserId = $this->Auth->user('id');

        if ($currentUserId === $userId) {
            $this->Flash->error(__('You cannot manage your own membership!'));

            $this->redirect(['controller' => 'Pages', 'action' => 'home', 'plugin' => false, 'prefix' => false]);
            return true;
        }
        return false;
    }

    private function isNotUserTenant($tenantId): bool
    {
        if ($this->currentTenantId !== $tenantId) {
            $this->Flash->error(__('Access denied'));

            $this->redirect(['controller' => 'Pages', 'action' => 'home', 'plugin' => false, 'prefix' => false]);
            return true;
        } else {
            return false;
        }
    }
}
