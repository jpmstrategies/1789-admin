<?php

namespace App\Controller;

use App\Model\Entity\ConfigurationOption;
use App\Model\Table\ConfigurationOptionsTable;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Datasource\ResultSetInterface;
use Cake\Http\Response;

/**
 * ConfigurationOptions Controller
 *
 * @property ConfigurationOptionsTable $ConfigurationOptions
 *
 * @method ConfigurationOption[]|ResultSetInterface paginate($object = null, array $settings = [])
 */
class ConfigurationOptionsController extends AppController
{

    /**
     * Index method
     *
     * @return void|null
     */
    public function index()
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $this->paginate = [
            'contain' => ['ConfigurationTypes'],
            'order' => [
                'ConfigurationOptions.configuration_type_id' => 'ASC',
                'ConfigurationOptions.sort_order' => 'ASC'
            ]
        ];
        $configurationOptions = $this->paginate($this->ConfigurationOptions);

        $this->set(compact('configurationOptions'));
    }

    /**
     * View method
     *
     * @param string|null $id Configuration Option id.
     * @return void|null
     * @throws RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $configurationOption = $this->ConfigurationOptions->get($id, [
            'contain' => ['ConfigurationTypes']
        ]);

        $this->set('configurationOption', $configurationOption);
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $configurationOption = $this->ConfigurationOptions->newEmptyEntity();
        if ($this->request->is('post')) {
            $configurationOption = $this->ConfigurationOptions->patchEntity($configurationOption, $this->request->getData());
            if ($this->ConfigurationOptions->save($configurationOption)) {
                $this->Flash->success(__('The configuration option has been created.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The configuration option could not be created. Please, try again.'));
        }
        $configurationTypes = $this->ConfigurationOptions->ConfigurationTypes->find('list', ['limit' => 200]);
        $this->set(compact('configurationOption', 'configurationTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Configuration Option id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $configurationOption = $this->ConfigurationOptions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $configurationOption = $this->ConfigurationOptions->patchEntity($configurationOption, $this->request->getData());
            if ($this->ConfigurationOptions->save($configurationOption)) {
                $this->Flash->success(__('The configuration option has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The configuration option could not be saved. Please, try again.'));
        }
        $configurationTypes = $this->ConfigurationOptions->ConfigurationTypes->find('list', ['limit' => 200]);
        $this->set(compact('configurationOption', 'configurationTypes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Configuration Option id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $this->request->allowMethod(['post', 'delete']);
        $configurationOption = $this->ConfigurationOptions->get($id);
        if ($this->ConfigurationOptions->delete($configurationOption)) {
            $this->Flash->success(__('The configuration option has been deleted.'));
        } else {
            $this->Flash->error(__('The configuration option could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
