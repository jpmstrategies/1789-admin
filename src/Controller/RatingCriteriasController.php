<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Routing\Router;
use Exception;

/**
 * RatingCriterias Controller
 *
 * @property \App\Model\Table\RatingCriteriasTable $RatingCriterias
 * @method \App\Model\Entity\RatingCriteria[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RatingCriteriasController extends AppController
{
    /**
     * admin_bulkedit method
     *
     * @return void
     */
    public function bulkedit()
    {
        $selectedFilters = [];
        $sessionId = (!empty($this->request->getParam('sessionId')) ? $this->request->getParam('sessionId') : null);
        $ratingId = (!empty($this->request->getParam('ratingId')) ? $this->request->getParam('ratingId') : null);

        if (empty($chamberId)) {
            $currentTenantId = $this->getRequest()->getSession()->read('Account.currentTenantId');
        }

        // Filter options.
        $sessions = (new LegislativeSessionsController())->getLegislativeSessions();
        $criterias = $this->RatingCriterias->Criterias->find('list');
        $ratingTypes = (new RatingTypesController())->getRatingTypes();
        $actions = (new RatingsController())->getActionsByType();

        // get first key from list, if not set
        if (empty($sessionId)) {
            $sessionId = key($sessions->toArray());
        }

        // Get selected filters.
        $selectedFilters['session'] = $sessionId;
        $selectedFilters['ratingId'] = $ratingId;

        $conditions = ['Ratings.legislative_session_id' => $sessionId];
        if (isset($ratingId)) {
            $conditions['Ratings.id'] = $ratingId;
        }

        // Ratings.
        $ratings = $this->RatingCriterias->LegiscanBills->find('all', [
            'fields' => [
                'LegiscanBills__id' => 'DISTINCT LegiscanBills.id',
                'LegiscanBills.bill_type_name',
                'LegiscanBills.bill_number',
                'LegiscanBills.title',
                'LegiscanBills.description',
                'LegiscanBills.legiscan_url',
                'LegiscanBills.state_url',
                'TenantsBills.notes',
                'RatingTypes.id',
                'RatingTypes.name',
            ],
            'conditions' => $conditions,
            'join' => [
                $this->getLegiscanBillVotesJoin(),
                $this->getRatingsJoin(),
                [
                    'table' => 'rating_types',
                    'alias' => 'RatingTypes',
                    'type' => 'INNER',
                    'conditions' => 'Ratings.rating_type_id = RatingTypes.id'
                ],
                [
                    'table' => 'tenants_bills',
                    'alias' => 'TenantsBills',
                    'type' => 'LEFT',
                    'conditions' => 'TenantsBills.legiscan_bill_id = LegiscanBills.id'
                ]
            ],
            'order' => ['LegiscanBills.bill_type_name' => 'ASC', 'LegiscanBills.bill_number' => 'ASC']])->all();
        $ratingCriterias = $this->getRatingCriterasByRatingId($sessionId);
        $criteriaDetails = $this->getCriteriaDetailsByCriteriaId();

        $this->set(compact('selectedFilters', 'sessions', 'ratings', 'actions',
            'criterias', 'ratingTypes', 'ratingCriterias', 'criteriaDetails'));
    }

    private function getLegiscanBillVotesJoin() {
        $currentTenantType = $this->request->getSession()->read('Account.currentTenantType');
        return [
            'table' => 'legiscan_bill_votes',
            'alias' => 'LegiscanBillVotes',
            'type' => ($currentTenantType === 'F' ? 'LEFT' : 'INNER'),
            'conditions' => 'LegiscanBills.id = LegiscanBillVotes.legiscan_bill_id'
        ];
    }

    private function getRatingsJoin() {
        $currentTenantType = $this->request->getSession()->read('Account.currentTenantType');
        if ($currentTenantType === 'F') {
            $ratingsJoin = [
                'table' => 'ratings',
                'alias' => 'Ratings',
                'type' => 'INNER',
                'conditions' => 'LegiscanBills.id = Ratings.legiscan_bill_id'
            ];
        } else {
            $ratingsJoin = [
                'table' => 'ratings',
                'alias' => 'Ratings',
                'type' => 'INNER',
                'conditions' => 'LegiscanBillVotes.id = Ratings.legiscan_bill_vote_id'
            ];
        }
        return $ratingsJoin;
    }

    public function getRatingCriterasByRatingId($sessionId): array
    {

        $currentTenantType = $this->request->getSession()->read('Account.currentTenantType');
        $ratingCriteriasResults = $this->RatingCriterias->LegiscanBills->find('all', [
            'fields' => [
                'RatingCriterias.legiscan_bill_id',
                'RatingCriterias.criteria_id',
                'RatingCriterias.criteria_detail_id',
                'RatingCriterias.action_id'
            ],
            'join' => [
                $this->getLegiscanBillVotesJoin(),
                $this->getRatingsJoin(),
                [
                    'table' => 'rating_criterias',
                    'alias' => 'RatingCriterias',
                    'type' => 'INNER',
                    'conditions' => 'LegiscanBills.id = RatingCriterias.legiscan_bill_id'
                ],
            ],
            'conditions' => ['Ratings.legislative_session_id' => $sessionId]
        ])->all();

        $ratingCriteras = [];
        foreach ($ratingCriteriasResults as $ratingCriterias) {
            $legiscan_bill_id = $ratingCriterias['RatingCriterias']['legiscan_bill_id'];
            $action_id = $ratingCriterias['RatingCriterias']['action_id'];
            $criteria_id = $ratingCriterias['RatingCriterias']['criteria_id'];
            $criteria_detail_id = $ratingCriterias['RatingCriterias']['criteria_detail_id'];

            $ratingCriteras[$legiscan_bill_id][$criteria_id] = [
                'rating_criteria_id' => $legiscan_bill_id,
                'action_id' => $action_id,
                'criteria_detail_id' => $criteria_detail_id
            ];
        }

        return $ratingCriteras;
    }

    public function getCriteriaDetailsByCriteriaId(): array
    {
        $criteriaDetails = [];
        $results = $this->RatingCriterias->Criterias->CriteriaDetails->find('all')->all();

        foreach ($results as $criteriaDetail) {
            $criteriaDetails[$criteriaDetail['criteria_id']][$criteriaDetail['id']] = $criteriaDetail['name'];
        }

        return $criteriaDetails;
    }

    public function bulkUpdateRow()
    {
        $this->viewBuilder()->disableAutoLayout();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (isset($data['notes'])) {
                return (new LegiscanBillsController())->saveTenantsBillsData($data);
            } else {
                return $this->saveRatingCriterias($data);
            }
        } else {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([]))
                ->withStatus(405);
        }
    }

    public function saveRatingCriterias($data): \Cake\Http\Response
    {
        $legiscan_bill_id = $data['legiscan_bill_id'];

        $insertData = [];
        foreach ($data['criterias'] as $criteria_id => $criterias) {
            foreach ($criterias as $criteria) {
                $action_id = $criteria['action-id'];
                $criteria_detail_id = $criteria['criteria-detail-id'];

                if ($action_id === '0') {
                    $action_id = null;
                }

                if ($criteria_detail_id === '0') {
                    $criteria_detail_id = null;
                }

                if (!empty($action_id) || !empty($criteria_detail_id)) {
                    $insertData[] = [
                        'legiscan_bill_id' => $legiscan_bill_id,
                        'criteria_id' => $criteria_id,
                        'criteria_detail_id' => $criteria_detail_id,
                        'action_id' => $action_id
                    ];
                }
            }
        }

        if (empty($insertData)) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'failure',
                    'data' => $data
                ]))
                ->withStatus(400);
        }

        // delete existing records
        $this->RatingCriterias->deleteAll(['RatingCriterias.legiscan_bill_id' => $legiscan_bill_id]);

        try {
            /// add new records
            $oNewEntities = $this->RatingCriterias->newEntities($insertData);
            $results = $this->RatingCriterias->saveManyOrFail($oNewEntities);

            if ($results) {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode([
                        'message' => 'success'
                    ]))
                    ->withStatus(200);
            } else {
                return $this->response->withType('application/json')
                    ->withStringBody(json_encode([
                        'message' => 'failure',
                        'data' => $data
                    ]))
                    ->withStatus(400);
            }
        } catch (Exception $e) {
            return $this->response->withType('application/json')
                ->withStringBody(json_encode([
                    'message' => 'exception',
                    'error' => $e->getMessage(),
                    'data' => $data
                ]))
                ->withStatus(400);
        }
    }
}