<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
use App\Service\R2Service;

/**
 * Bills Controller
 *
 * @property \App\Model\Table\BillsTable $Bills
 * @method \App\Model\Entity\Bill[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BillsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $conditions = [];
        $selectedFilters = [
            'session' => $this->request->getQuery('session') ?: null,
            'chamber' => $this->request->getQuery('chamber') ?: null,
            'bill' => $this->request->getQuery('bill') ?: null,
        ];

        // Add condition for chamber if selected
        if (!empty($selectedFilters['chamber'])) {
            $conditions[] = ['Bills.filed_chamber_id' => $selectedFilters['chamber']];
        }

        $sessions = $this->Bills->LegislativeSessions->find('list', ['limit' => 200]);
        $chambers = $this->Bills->FiledChamber->find('list');

        // Set session to the first session if none is selected
        if (empty($selectedFilters['session'])) {
            $selectedFilters['session'] = key($sessions->toArray());
        }

        $conditions[] = ['Bills.legislative_session_id' => $selectedFilters['session']];

        if (isset($selectedFilters['bill'])) {
            $conditions[] = ['Bills.bill_number LIKE' => '%' . $selectedFilters['bill'] . '%'];
        }

        $this->paginate = [
            'contain' => [
                'LegislativeSessions',
                'CurrentBillStage',
                'CurrentBillStatus',
                'CurrentBillCta',
                'CurrentBillProgress',
                'CurrentBillPhaseIcon',
                'CurrentBillRatingIcon',
                'FiledChamber',
                'CurrentChamber',
                'HouseRating',
                'SenateRating',
                'LegiscanBill',
                'Uploads'
            ],
            'fields' => [
                'Bills.id',
                'Bills.bill_number',
                'Bills.official_title',
                'Bills.legiscan_bill_id',
                'Bills.is_enabled',
                'CurrentBillStage.id',
                'CurrentBillStage.name',
                'CurrentBillStatus.id',
                'CurrentBillStatus.name',
                'CurrentBillCta.id',
                'CurrentBillCta.name',
                'CurrentBillProgress.id',
                'CurrentBillProgress.name',
                'CurrentBillPhaseIcon.id',
                'CurrentBillPhaseIcon.name',
                'CurrentBillRatingIcon.id',
                'CurrentBillRatingIcon.name',
                'FiledChamber.id',
                'FiledChamber.name',
                'CurrentChamber.id',
                'CurrentChamber.name',
                'HouseRating.id',
                'HouseRating.name',
                'SenateRating.id',
                'SenateRating.name',
                'LegiscanBill.bill_number',
                'LegiscanBill.legiscan_url',
                'Uploads.path',
            ],
            'conditions' => $conditions,
            'order' => ['Bills.bill_number' => 'ASC']
        ];
        $bills = $this->paginate($this->Bills);

        $this->set(compact('bills', 'sessions', 'chambers', 'selectedFilters'));
    }

    /**
     * View method
     *
     * @param string|null $id Bill id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bill = $this->Bills->get($id, [
            'contain' => [
                'LegislativeSessions',
                'CurrentBillStage',
                'CurrentBillStatus',
                'CurrentBillCta',
                'CurrentBillProgress',
                'CurrentBillPhaseIcon',
                'CurrentBillRatingIcon',
                'FiledChamber',
                'CurrentChamber',
                'HouseRating',
                'SenateRating',
                'LegiscanBill',
                'Uploads'
            ],
        ]);

        $this->set(compact('bill'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bill = $this->Bills->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            // save uploaded image and link to bill
            if (!empty($data['upload_path'])) {
                $upload = $this->Bills->Uploads->newEmptyEntity();
                $upload = $this->Bills->Uploads->patchEntity($upload, [
                    'path' => $data['upload_path'],
                    'mime' => $data['upload_mime'],
                    'width' => $data['upload_width'],
                    'height' => $data['upload_height'],
                ]);

                if ($uploadSave = $this->Bills->Uploads->save($upload)) {
                    $data['upload_id'] = $uploadSave->id;
                    $this->Flash->success(__('The image has been uploaded.'));
                } else {
                    $this->Flash->error(__('The image upload failed. Please, try again.'));
                }
            }

            $bill = $this->Bills->patchEntity($bill, $data);
            if ($this->Bills->save($bill)) {
                $this->Flash->success(__('The bill has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bill could not be saved. Please, try again.'));
        }
        $legislativeSessions = $this->Bills->LegislativeSessions->find('list', ['limit' => 200]);
        $currentBillStage = $this->Bills->CurrentBillStage->find('list', ['limit' => 200]);
        $currentBillCta = $this->Bills->CurrentBillCta->find('list', ['limit' => 200]);
        $currentBillProgress = $this->Bills->CurrentBillProgress->find('list', ['limit' => 200]);
        $currentBillPhaseIcon = $this->Bills->CurrentBillPhaseIcon->find('list', ['limit' => 200]);
        $currentBillRatingIcon = $this->Bills->CurrentBillRatingIcon->find('list', ['limit' => 200]);
        $filedChamber = $this->Bills->FiledChamber->find('list', ['limit' => 200]);
        $currentChamber = $this->Bills->CurrentChamber->find('list', ['limit' => 200]);
        $this->set(compact('bill', 'legislativeSessions', 'currentBillStage', 'currentBillCta', 'currentBillProgress', 'currentBillPhaseIcon', 'currentBillRatingIcon', 'filedChamber', 'currentChamber'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bill id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bill = $this->Bills->get($id, [
            'contain' => ['Uploads'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            // delete uploaded image
            if(!empty($data['delete_upload'])) {
                $data['upload_id'] = null;
            }

            // save uploaded image and link to bill
            if (!empty($data['upload_path'])) {
                $upload = $this->Bills->Uploads->newEmptyEntity();
                $upload = $this->Bills->Uploads->patchEntity($upload, [
                    'path' => $data['upload_path'],
                    'mime' => $data['upload_mime'],
                    'width' => $data['upload_width'],
                    'height' => $data['upload_height'],
                ]);

                if ($uploadSave = $this->Bills->Uploads->save($upload)) {
                    $data['upload_id'] = $uploadSave->id;
                    $this->Flash->success(__('The image has been uploaded.'));
                } else {
                    $this->Flash->error(__('The image upload failed. Please, try again.'));
                }
            }

            $bill = $this->Bills->patchEntity($bill, $data);
            if ($this->Bills->save($bill)) {
                $this->Flash->success(__('The bill has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bill could not be saved. Please, try again.'));
        }
        $legislativeSessions = $this->Bills->LegislativeSessions->find('list', ['limit' => 200]);
        $currentBillStage = $this->Bills->CurrentBillStage->find('list', ['limit' => 200]);
        $currentBillCta = $this->Bills->CurrentBillCta->find('list', ['limit' => 200]);
        $currentBillProgress = $this->Bills->CurrentBillProgress->find('list', ['limit' => 200]);
        $currentBillPhaseIcon = $this->Bills->CurrentBillPhaseIcon->find('list', ['limit' => 200]);
        $currentBillRatingIcon = $this->Bills->CurrentBillRatingIcon->find('list', ['limit' => 200]);
        $filedChamber = $this->Bills->FiledChamber->find('list', ['limit' => 200]);
        $currentChamber = $this->Bills->CurrentChamber->find('list', ['limit' => 200]);

        if ($bill->legiscan_bill_id) {
            $legiscanBill = $this->Bills->LegiscanBill->find('all', [
                'conditions' => ['id' => $bill->legiscan_bill_id]
            ])->first();
        } else {
            $legiscanBill = null;
        }

        $this->set(compact('bill', 'legislativeSessions', 'currentBillStage', 'currentBillCta', 'currentBillProgress', 'currentBillPhaseIcon', 'currentBillRatingIcon', 'filedChamber', 'currentChamber', 'legiscanBill'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bill id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bill = $this->Bills->get($id);
        if ($this->Bills->delete($bill)) {
            $this->Flash->success(__('The bill has been deleted.'));
        } else {
            $this->Flash->error(__('The bill could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function linkRating()
    {
        $ratingId = $this->request->getQuery('rating_id');
        $billChamber = $this->request->getQuery('bill_chamber');
        $billUuid = $this->request->getQuery('bill_uuid');
        $legiscanVoteId = $this->request->getQuery('legiscan_vote_id');

        $bill = $this->Bills->find('all', [
            'conditions' => [
                'id' => $billUuid
            ]
        ])->first();

        if ($billChamber === 'L') {
            $bill->house_rating_id = $ratingId;
        } else {
            $bill->senate_rating_id = $ratingId;
        }

        // populate Vote Text
        $ratingsController = new RatingsController();
        $ratingsController->saveBillVoteText($billChamber, $billUuid, $legiscanVoteId, $ratingId);

        if ($this->Bills->save($bill)) {
            $this->Flash->success(__('The bill has been linked to the rating.'));
        } else {
            $this->Flash->error(__('The bill could not be linked to the rating. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function unlinkRating() {
        $billUuid = $this->request->getQuery('bill_uuid');
        $billChamber = $this->request->getQuery('bill_chamber');

        $bill = $this->Bills->find('all', [
            'conditions' => [
                'id' => $billUuid
            ]
        ])->first();

        if ($billChamber === 'L') {
            $bill->house_rating_id = null;
        } else {
            $bill->senate_rating_id = null;
        }

        if ($this->Bills->save($bill)) {
            $this->Flash->success(__('The bill has been unlinked from the rating.'));
        } else {
            $this->Flash->error(__('The bill could not be unlinked from the rating. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function exportCsv()
    {
        $conditions = [];
        $selectedFilters = [
            'session' => $this->request->getQuery('session') ?: null,
            'chamber' => $this->request->getQuery('chamber') ?: null,
        ];

        // Add condition for session if selected
        if (!empty($selectedFilters['session'])) {
            $conditions[] = ['Bills.legislative_session_id' => $selectedFilters['session']];
        }

        // Add condition for chamber if selected
        if (!empty($selectedFilters['chamber'])) {
            $conditions[] = ['Bills.filed_chamber_id' => $selectedFilters['chamber']];
        }

        $records = $this->Bills->find('all', [
            'contain' => [
                'LegislativeSessions',
                'CurrentBillStage',
                'CurrentBillStatus',
                'CurrentBillCta',
                'CurrentBillProgress',
                'CurrentBillPhaseIcon',
                'CurrentBillRatingIcon',
                'FiledChamber',
                'CurrentChamber',
                'LegiscanBill'
            ],
            'fields' => [
                'legislative_session' => 'LegislativeSessions.name',
                'bill' => 'Bills.bill_number',
                'official_title' => 'Bills.official_title',
                'explainer_title' => 'Bills.explainer_title',
                'current_chamber' => 'CurrentChamber.name',
                'current_stage' => 'CurrentBillStage.name',
                'current_status' => 'CurrentBillStatus.name',
                'current_progress' => 'CurrentBillProgress.name',
                'phase_icon' => 'CurrentBillPhaseIcon.name',
                'rating_icon' => 'CurrentBillRatingIcon.name',
                'current_cta' => 'CurrentBillCta.name',
                'current_cta_url' => 'Bills.current_bill_cta_url',
                'legiscan_url' => 'LegiscanBill.legiscan_url',
                ],
            'conditions' => $conditions,
            'order' => ['Bills.bill_number' => 'ASC']
        ]);

        $header = [
            'Legislative Session',
            'Bill Number',
            'Official Title',
            'Explainer Title',
            'Current Chamber',
            'Current Stage',
            'Current Status',
            'Current Progress',
            'Current Phase Icon',
            'Current Rating Icon',
            'Current CTA',
            'Current CTA URL',
            'Legiscan URL'
        ];

        // use the CSV view
        $filename = 'Bills (as of ' . FrozenTime::now()->i18nFormat('yyyy-MM-dd') . ').csv';
        $this->setResponse($this->getResponse()->withDownload($filename));
        $this->viewBuilder()
            ->setClassName('CsvView.Csv')
            ->setOptions(['serialize' => 'records', 'header' => $header]);

        $this->set(compact('records'));
    }
}