<?php

namespace App\Controller;


use App\Model\Entity\ConfigurationType;
use App\Model\Table\ConfigurationTypesTable;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Datasource\ResultSetInterface;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\ORM\Locator\TableLocator;
use Exception;


/**
 * ConfigurationTypes Controller
 *
 * @property ConfigurationTypesTable $ConfigurationTypes
 *
 * @method ConfigurationType[]|ResultSetInterface paginate($object = null, array $settings = [])
 */
class ConfigurationTypesController extends AppController
{
    private $tableLocatorClass;

    public function __construct(ServerRequest $request = null, Response $response = null, $name = null, \Cake\Event\EventManager $eventManager = null)
    {
        parent::__construct($request, $response, $name, $eventManager);

        $this->tableLocatorClass = new TableLocator();
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $this->paginate = [
            'order' => [
                'name' => 'ASC'
            ]
        ];
        $configurationTypes = $this->paginate($this->ConfigurationTypes);

        $this->set(compact('configurationTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Configuration Type id.
     * @return void
     * @throws RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $configurationType = $this->ConfigurationTypes->get($id, [
            'contain' => []
        ]);

        $this->set('configurationType', $configurationType);
    }

    public function insertConfigurations($id = null, $idType = null)
    {
        $is_super = $this->Auth->user('is_super');

        $selectQuery = $this->ConfigurationTypes
            ->query()
            ->select([
                'id' => 'UUID()',
                'configuration_type_id' => 'ConfigurationTypes.id',
                'tenant_id' => 'Tenants.id',
                'value' => 'ConfigurationTypes.default_value',
                'created' => 'NOW()',
                'modified' => 'NOW()'
            ])
            ->from('configuration_types ConfigurationTypes')
            ->join([
                'table' => 'tenants',
                'alias' => 'Tenants',
                'type' => 'CROSS',
                'conditions' => '1=1',
            ])
            ->join([
                'table' => 'configurations',
                'alias' => 'Configurations',
                'type' => 'LEFT',
                'conditions' => [
                    'ConfigurationTypes.id = Configurations.configuration_type_id',
                    'Tenants.id = Configurations.tenant_id'
                ],
            ]);

        // when no ID, do an upsert for all configuration types and tenants
        if (empty($idType)) {
            if (!empty($id)) {
                $selectQuery->where(['Configurations.id IS NULL', 'ConfigurationTypes.id' => $id]);
            } else {
                $selectQuery->where(['Configurations.id IS NULL']);
            }
        } else {
            if ($idType === 'tenant' && !empty($id)) {
                $selectQuery->where(['Configurations.id IS NULL', 'Tenants.id' => $id]);
            }
        }

        $configurations = $this->tableLocatorClass->get('Configurations');
        $insertQuery = $configurations->query();
        $results = $insertQuery->insert([
            'id',
            'configuration_type_id',
            'tenant_id',
            'value',
            'created',
            'modified'
        ])
            ->values($selectQuery)
            ->execute();

        if ($is_super) {
            if ($results) {
                $this->Flash->success(__('The related configurations have been created.'));
            } else {
                // @codeCoverageIgnoreStart
                $this->Flash->error(__('The related configuration data could not be saved. Please, try again.'));
                // @codeCoverageIgnoreEnd
            }
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $configurationType = $this->ConfigurationTypes->newEmptyEntity();
        if ($this->request->is('post')) {
            $configurationType = $this->ConfigurationTypes->patchEntity($configurationType, $this->request->getData());
            if ($results = $this->ConfigurationTypes->save($configurationType)) {
                // add configurations for every tenant
                $results = $this->insertConfigurations($results->id);
                if ($results) {
                    $this->Flash->success(__('The configuration type has been created.'));
                    return $this->redirect(['action' => 'index']);
                }
            }
            $this->Flash->error(__('The configuration type could not be created. Please, try again.'));
        }

        $this->set(compact('configurationType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Configuration Type id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     */
    public function edit($id = null)
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $configurationType = $this->ConfigurationTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $configurationType = $this->ConfigurationTypes->patchEntity($configurationType, $this->request->getData());
            if ($this->ConfigurationTypes->save($configurationType)) {
                $this->Flash->success(__('The configuration type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The configuration type could not be saved. Please, try again.'));
        }

        $this->set(compact('configurationType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Configuration Type id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if ($this->isUserNonSuperAdmin()) return null;

        $this->request->allowMethod(['post', 'delete']);

        $configurationType = $this->ConfigurationTypes->get($id);
        try {
            if ($this->ConfigurationTypes->delete($configurationType)) {
                $this->Flash->success(__('The configuration type has been deleted.'));
            } else {
                $this->Flash->error(__('The configuration type could not be deleted. Please, try again.'));
            }
        } catch (Exception) {
            $this->Flash->error(__('The configuration type you are trying to delete is associated with other records. Please, try again after deleting the related records.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getUserFields(): array
    {
        return $this->ConfigurationTypes->find('all', [
            'conditions' => [
                'ConfigurationTypes.tab' => 'user_details'
            ]])->toArray();
    }

    public function getUserFieldOptions(): array
    {
        $databaseOptions = $this->ConfigurationTypes->ConfigurationOptions->find('all')->toArray();

        $options = [];
        foreach ($databaseOptions as $option) {
            $configuration_type = $option['configuration_type_id'];
            $name = $option['name'];
            $value = $option['value'];

            $options[$configuration_type][$value] = $name;
        }
        return $options;
    }
}
