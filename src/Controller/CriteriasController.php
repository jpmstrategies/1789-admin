<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Criterias Controller
 *
 * @property \App\Model\Table\CriteriasTable $Criterias
 * @method \App\Model\Entity\Criteria[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CriteriasController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Parties'],
            'fields' => [
                'Criterias.id',
                'Criterias.name',
                'Criterias.sort_order',
                'Criterias.method',
                'Parties.id',
                'Parties.name'
            ],
            'order' => [
                'Criterias.sort_order'
            ],
            'sortableFields' => [
                'Criterias.name',
                'Criterias.sort_order',
                'Criterias.method'
            ]
        ];
        $criterias = $this->paginate($this->Criterias);

        $this->set(compact('criterias'));
    }

    /**
     * View method
     *
     * @param string|null $id Criteria id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $criteria = $this->Criterias->get($id, [
            'contain' => ['Parties'],
            'fields' => [
                'Criterias.id',
                'Criterias.name',
                'Criterias.th_detail_name',
                'Criterias.th_vote_name',
                'Criterias.sort_order',
                'Criterias.created',
                'Criterias.modified',
                'Criterias.method',
                'Parties.id',
                'Parties.name'
            ]
        ]);

        $this->set(compact('criteria'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $criteria = $this->Criterias->newEmptyEntity();
        if ($this->request->is('post')) {
            $criteria = $this->Criterias->patchEntity($criteria, $this->request->getData());
            if ($this->Criterias->save($criteria)) {
                $this->Flash->success(__('The criteria has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The criteria could not be saved. Please, try again.'));
        }
        $parties = $this->Criterias->Parties->find('list', ['limit' => 200]);
        $this->set(compact('criteria', 'parties'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Criteria id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $criteria = $this->Criterias->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $criteria = $this->Criterias->patchEntity($criteria, $this->request->getData());
            if ($this->Criterias->save($criteria)) {
                $this->Flash->success(__('The criteria has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The criteria could not be saved. Please, try again.'));
        }
        $parties = $this->Criterias->Parties->find('list', ['limit' => 200]);
        $this->set(compact('criteria', 'parties'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Criteria id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        try {
            $ratingType = $this->Criterias->get($id);
            if ($this->Criterias->delete($ratingType)) {
                $this->Flash->success(__('The criteria has been deleted.'));
            } else {
                $this->Flash->error(__('The criteria could not be deleted. Please, try again.'));
            }
        } catch (\Exception) {
            $this->Flash->error(__('The criteria you are trying to delete is associated with other records. Please, try again after deleting the related records.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getCriteriaDetails(): array
    {
        $criterias = $this->Criterias->find('all')->toArray();

        $criteriaDetails = [];
        foreach ($criterias as $criteria) {
            $criteriaId = $criteria['id'];
            $criteriaDetails[$criteriaId] = [
                'id' => $criteria['id'],
                'name' => $criteria['name'],
                'party_id' => $criteria['party_id'],
            ];
        }
        return $criteriaDetails;
    }

    public function parseCalculatedScores($apiJson, $breakdownType, $entityPropertyId): array
    {
        $calculatedScores = [];
        $criteriaDetails = $this->getCriteriaDetails();

        $calculatedScores['criteria'] = $criteriaDetails;
        foreach ($apiJson as $legislatorOrPerson) {
            $legislatorOrPersonId = $legislatorOrPerson[$entityPropertyId];
            $legislatorPartyId = $legislatorOrPerson['legislatorPartyId'];
            foreach ($legislatorOrPerson[$breakdownType] as $criteriaId => $criteria) {
                $score = ($criteria['score'] ?? 'N/A');
                $grade = ($criteria['grade']['name'] ?? 'N/A');
                $partyMatch = 'n/a';

                if (isset($criteriaDetails[$criteriaId]['party_id'])) {
                    $criteriaPartyId = $criteriaDetails[$criteriaId]['party_id'];
                    if ($criteriaPartyId === $legislatorPartyId) {
                        $partyMatch = 'true';
                    } else {
                        $partyMatch = 'false';
                    }
                }

                $calculatedScores['scores'][$legislatorOrPersonId][$criteriaId] = [
                    'score' => $score,
                    'grade' => $grade,
                    'partyMatch' => $partyMatch
                ];
            }
        }

        return $calculatedScores;
    }
}