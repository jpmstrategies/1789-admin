<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * LegiscanSubjects Controller
 *
 * @property \App\Model\Table\LegiscanSubjectsTable $LegiscanSubjects
 * @method \App\Model\Entity\LegiscanSubject[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LegiscanSubjectsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $legiscanSubjects = $this->paginate($this->LegiscanSubjects);

        $this->set(compact('legiscanSubjects'));
    }

    /**
     * View method
     *
     * @param string|null $id Legiscan Subject id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $legiscanSubject = $this->LegiscanSubjects->get($id, [
            'contain' => ['LegiscanBillSubjects'],
        ]);

        $this->set(compact('legiscanSubject'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $legiscanSubject = $this->LegiscanSubjects->newEmptyEntity();
        if ($this->request->is('post')) {
            $legiscanSubject = $this->LegiscanSubjects->patchEntity($legiscanSubject, $this->request->getData());
            if ($this->LegiscanSubjects->save($legiscanSubject)) {
                $this->Flash->success(__('The legiscan subject has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan subject could not be saved. Please, try again.'));
        }
        $this->set(compact('legiscanSubject'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Legiscan Subject id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $legiscanSubject = $this->LegiscanSubjects->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $legiscanSubject = $this->LegiscanSubjects->patchEntity($legiscanSubject, $this->request->getData());
            if ($this->LegiscanSubjects->save($legiscanSubject)) {
                $this->Flash->success(__('The legiscan subject has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The legiscan subject could not be saved. Please, try again.'));
        }
        $this->set(compact('legiscanSubject'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Legiscan Subject id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $legiscanSubject = $this->LegiscanSubjects->get($id);
        if ($this->LegiscanSubjects->delete($legiscanSubject)) {
            $this->Flash->success(__('The legiscan subject has been deleted.'));
        } else {
            $this->Flash->error(__('The legiscan subject could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}