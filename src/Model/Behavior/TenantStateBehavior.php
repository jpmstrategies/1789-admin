<?php

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Exception;

// Inspiration: https://github.com/UseMuffin/Footprint
class TenantStateBehavior extends Behavior
{

    /**
     * Keeping a reference to the table in order to,
     * be able to retrieve table/model attributes
     *
     * @var Table
     */
    protected $_table;

    /**
     * Default config
     *
     * @var array
     */
    protected $_defaultConfig = [
        'foreign_key_table',
        'foreign_key_field'
    ];

    /**
     * Constructor
     *
     *
     * @param Table $table The table this behavior is attached to.
     * @param array $config The config for this behavior.
     * @throws Exception
     */
    public function __construct(Table $table, array $config = [])
    {
        parent::__construct($table, $config);

        $this->_table = $table;

        if (isset($config['foreign_key_table'])) {
            $this->setConfig('foreign_key_table', $config['foreign_key_table']);
        } else {
            $this->setConfig('foreign_key_table', $this->_table->getAlias());
        }

        if (isset($config['foreign_key_field'])) {
            $this->setConfig('foreign_key_field', $config['foreign_key_field']);
        } else {
            $this->setConfig('foreign_key_field', 'state');
        }
    }

    /**
     * beforeFind callback
     *
     * always filter by state using current sessions
     *
     * @param Event $event The afterSave event that was fired.
     * @param Query $query The query.
     * @param $primary
     * @return void
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options, $primary)
    {
        $qualifiedFieldName = $this->getConfig('foreign_key_table') . '.' . $this->getConfig('foreign_key_field');
        $query->where([$qualifiedFieldName => $options['currentTenantState']]);
    }

    /**
     * beforeSave callback
     *
     * always add state to INSERT or UPDATE operations using current sessions
     *
     * @param Event $event The beforeSave event that was fired.
     * @param Entity $entity The entity to be saved.
     * @return void
     * @throws Exception
     */
    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        //blind overwrite, preventing user from providing explicit value
        $field = $this->getConfig('foreign_key_field');
        $entity->{$field} = $options['currentTenantState'];

        $this->checkEntityOwnership($entity, $options['currentTenantState']);
    }

    /**
     * beforeDelete callback
     *
     * always check state on DELETE operations using current sessions
     *
     * @param Event $event The beforeDelete event that was fired.
     * @param Entity $entity The entity to be deleted.
     * @return void
     * @throws Exception
     */
    public function beforeDelete(Event $event, Entity $entity, ArrayObject $options)
    {
        $this->checkEntityOwnership($entity, $options['currentTenantState']);
    }

    /**
     *
     * check the current tenant has permissions to the record before performing any operation
     *
     * @param Entity $entity The entity record for the operation
     * @throws Exception
     */
    private function checkEntityOwnership(Entity $entity, $currentTenantState)
    {
        $field = $this->getConfig('foreign_key_field');

        //paranoid check of ownership
        if (isset($entity->state) && $entity->{$field} != $currentTenantState) { //current tenant is NOT owner
            throw new Exception('Tenant->state:' . $currentTenantState . ' does not own ' . $this->getConfig('foreign_key_table') . '->' . $field . ':' . $entity->id);
        }
    }
}