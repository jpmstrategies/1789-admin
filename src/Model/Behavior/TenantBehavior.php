<?php

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Exception;

// Inspiration: https://github.com/UseMuffin/Footprint
class TenantBehavior extends Behavior
{

    /**
     * Keeping a reference to the table in order to,
     * be able to retrieve table/model attributes
     *
     * @var Table
     */
    protected $_table;

    /**
     * Default config
     *
     * @var array
     */
    protected $_defaultConfig = [
        'foreign_key_table',
        'foreign_key_field',
        'sync_table'
    ];

    /**
     * Constructor
     *
     *
     * @param Table $table The table this behavior is attached to.
     * @param array $config The config for this behavior.
     * @throws Exception
     */
    public function __construct(Table $table, array $config = [])
    {
        parent::__construct($table, $config);

        $this->_table = $table;
        if (isset($config['foreign_key_table'])) {
            $this->setConfig('foreign_key_table', $config['foreign_key_table']);
        } else {
            $this->setConfig('foreign_key_table', $this->_table->getAlias());
        }

        if (isset($config['foreign_key_field'])) {
            $this->setConfig('foreign_key_field', $config['foreign_key_field']);
        } else {
            $this->setConfig('foreign_key_field', 'tenant_id');
        }

        if (isset($config['sync_table'])) {
            $this->setConfig('sync_table', $config['sync_table']);
        } else {
            $this->setConfig('sync_table', 'Tenants');
        }
    }

    /**
     * beforeFind callback
     *
     * always filter by tenant_id using current sessions
     *
     * @param Event $event The afterSave event that was fired.
     * @param Query $query The query.
     * @param ArrayObject $options
     * @param $primary
     * @return void
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options, $primary)
    {
        $qualifiedFieldName = $this->getConfig('foreign_key_table') . '.' . $this->getConfig('foreign_key_field');
        $query->where([$qualifiedFieldName => $options['currentTenantId']]);
    }

    /**
     * beforeSave callback
     *
     * always add tenant_id to INSERT or UPDATE operations using current sessions
     *
     * @param Event $event The beforeSave event that was fired.
     * @param Entity $entity The entity to be saved.
     * @param ArrayObject $options
     * @return void
     * @throws Exception
     */
    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        //blind overwrite, preventing user from providing explicit value
        $field = $this->getConfig('foreign_key_field');
        $entity->{$field} = $options['currentTenantId'];

        $this->checkEntityOwnership($entity, $options['currentTenantId']);
    }

    /**
     * afterSave callback
     *
     * always update tenants.sync_pending after INSERT or UPDATE operations
     *
     * @param Event $event The afterSave event that was fired.
     * @param Entity $entity The entity that was saved.
     * @param ArrayObject $options
     * @return void
     * @throws Exception
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $this->updateSyncPendingDatetime($options['currentTenantId']);
    }

    /**
     * beforeDelete callback
     *
     * always check tenant_id on DELETE operations using current sessions
     *
     * @param Event $event The beforeDelete event that was fired.
     * @param Entity $entity The entity to be deleted.
     * @param ArrayObject $options
     * @return void
     * @throws Exception
     */
    public function beforeDelete(Event $event, Entity $entity, ArrayObject $options)
    {
        $this->checkEntityOwnership($entity, $options['currentTenantId']);
    }

    /**
     * afterDelete callback
     *
     * always update tenants.sync_pending after DELETE operations
     *
     * @param Event $event The afterDelete event that was fired.
     * @param Entity $entity The entity that was deleted.
     * @param ArrayObject $options
     * @return void
     * @throws Exception
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options)
    {
        $this->updateSyncPendingDatetime($options['currentTenantId']);
    }

    /**
     *
     * check the current tenant has permissions to the record before performing any operation
     *
     * @param Entity $entity The entity record for the operation
     * @throws Exception
     */
    private function checkEntityOwnership(Entity $entity, $currentTenantId)
    {
        $field = $this->getConfig('foreign_key_field');

        //paranoid check of ownership
        if (isset($entity->tenant_id) && $entity->{$field} != $currentTenantId) { //current tenant is NOT owner
            throw new Exception('Tenant->id:' . $currentTenantId . ' does not own ' . $this->getConfig('foreign_key_table') . '->' . $field . ':' . $entity->id);
        }
    }

    /**
     *
     * set tenants.sync_pending after INSERT, UPDATE and DELETE operations
     *
     */
    private function updateSyncPendingDatetime($currentTenantId)
    {
        $tenants = TableRegistry::getTableLocator()->get($this->getConfig('sync_table'));
        $query = $tenants->query();
        $query
            ->update()
            ->set(['sync_pending' => $query->func()->now()])
            ->where(['id' => $currentTenantId])
            ->execute();
    }
}