<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FloorReportsRatings Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\FloorReportsTable&\Cake\ORM\Association\BelongsTo $FloorReports
 * @property \App\Model\Table\RatingsTable&\Cake\ORM\Association\BelongsTo $Ratings
 *
 * @method \App\Model\Entity\FloorReportsRating newEmptyEntity()
 * @method \App\Model\Entity\FloorReportsRating newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\FloorReportsRating[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FloorReportsRating get($primaryKey, $options = [])
 * @method \App\Model\Entity\FloorReportsRating findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\FloorReportsRating patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FloorReportsRating[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\FloorReportsRating|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FloorReportsRating saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FloorReportsRating[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\FloorReportsRating[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\FloorReportsRating[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\FloorReportsRating[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FloorReportsRatingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('floor_reports_ratings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tenant');
        $this->addBehavior('ADmad/Sequence.Sequence', [
            'sequenceField' => 'sort_order', // Field to use to store integer sequence. Default "position".
            'scope' => ['tenant_id'], // Array of field names to use for grouping records. Default [].
            'startAt' => 1, // Initial value for sequence. Default 1.
        ]);

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('FloorReports', [
            'foreignKey' => 'floor_report_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Ratings', [
            'foreignKey' => 'rating_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('floor_report_id')
            ->notEmptyString('floor_report_id');

        $validator
            ->uuid('rating_id')
            ->notEmptyString('rating_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);
        $rules->add($rules->existsIn(['floor_report_id'], 'FloorReports'), ['errorField' => 'floor_report_id']);
        $rules->add($rules->existsIn(['rating_id'], 'Ratings'), ['errorField' => 'rating_id']);

        return $rules;
    }

    // set default sorting if one isn't provided and sort_order exists
    public function beforeFind($event, Query $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !(is_countable($order) ? count($order) : 0)) {
            $query->order([$this->getAlias() . '.sort_order' => 'ASC']);
        }
    }
}
