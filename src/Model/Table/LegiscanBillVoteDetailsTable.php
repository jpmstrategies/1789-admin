<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LegiscanBillVoteDetails Model
 *
 * @property \App\Model\Table\LegiscanBillsTable&\Cake\ORM\Association\BelongsTo $LegiscanBills
 * @property \App\Model\Table\LegiscanBillVotesTable&\Cake\ORM\Association\BelongsTo $LegiscanBillVotes
 * @property \App\Model\Table\LegiscanPartiesTable&\Cake\ORM\Association\BelongsTo $LegiscanParties
 * @property \App\Model\Table\LegiscanSessionsTable&\Cake\ORM\Association\BelongsTo $LegiscanSessions
 * @property \App\Model\Table\LegiscanChambersTable&\Cake\ORM\Association\BelongsTo $LegiscanChambers
 *
 * @method \App\Model\Entity\LegiscanBillVoteDetail newEmptyEntity()
 * @method \App\Model\Entity\LegiscanBillVoteDetail newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBillVoteDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBillVoteDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\LegiscanBillVoteDetail findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\LegiscanBillVoteDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBillVoteDetail[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBillVoteDetail|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanBillVoteDetail saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanBillVoteDetail[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanBillVoteDetail[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanBillVoteDetail[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanBillVoteDetail[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class LegiscanBillVoteDetailsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('legiscan_bill_vote_details');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('TenantState');

        $this->belongsTo('LegiscanBills', [
            'foreignKey' => 'legiscan_bill_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegiscanBillVotes', [
            'foreignKey' => 'legiscan_bill_vote_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegiscanParties', [
            'foreignKey' => 'legiscan_party_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegiscanSessions', [
            'foreignKey' => 'legiscan_session_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegiscanChambers', [
            'className' => 'LegiscanChambers',
            'foreignKey' => 'legiscan_chamber_id',
            'joinType' => 'INNER',
            'propertyName' => 'origination_chamber'
        ]);
        $this->belongsTo('LegiscanCurrentChambers', [
            'className' => 'LegiscanChambers',
            'foreignKey' => 'legiscan_current_chamber_id',
            'joinType' => 'INNER',
            'propertyName' => 'current_chamber'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('id')
            ->maxLength('id', 14)
            ->notEmptyString('id');

        $validator
            ->scalar('state')
            ->maxLength('state', 2)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->nonNegativeInteger('legiscan_bill_id')
            ->requirePresence('legiscan_bill_id', 'create')
            ->notEmptyString('legiscan_bill_id');

        $validator
            ->nonNegativeInteger('legiscan_bill_vote_id')
            ->requirePresence('legiscan_bill_vote_id', 'create')
            ->notEmptyString('legiscan_bill_vote_id');

        $validator
            ->requirePresence('legiscan_bill_vote_chamber_id', 'create')
            ->notEmptyString('legiscan_bill_vote_chamber_id');

        $validator
            ->requirePresence('legiscan_people_id', 'create')
            ->notEmptyString('legiscan_people_id');

        $validator
            ->scalar('bill_number')
            ->maxLength('bill_number', 10)
            ->requirePresence('bill_number', 'create')
            ->notEmptyString('bill_number');

        $validator
            ->notEmptyString('legiscan_vote_id');

        $validator
            ->scalar('vote_desc')
            ->maxLength('vote_desc', 24)
            ->requirePresence('vote_desc', 'create')
            ->notEmptyString('vote_desc');

        $validator
            ->notEmptyString('legiscan_party_id');

        $validator
            ->scalar('party_abbr')
            ->maxLength('party_abbr', 1)
            ->requirePresence('party_abbr', 'create')
            ->notEmptyString('party_abbr');

        $validator
            ->requirePresence('legiscan_role_id', 'create')
            ->notEmptyString('legiscan_role_id');

        $validator
            ->scalar('role_abbr')
            ->maxLength('role_abbr', 3)
            ->requirePresence('role_abbr', 'create')
            ->notEmptyString('role_abbr');

        $validator
            ->scalar('role_name')
            ->maxLength('role_name', 64)
            ->requirePresence('role_name', 'create')
            ->notEmptyString('role_name');

        $validator
            ->scalar('name')
            ->maxLength('name', 128)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 32)
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name');

        $validator
            ->scalar('middle_name')
            ->maxLength('middle_name', 32)
            ->requirePresence('middle_name', 'create')
            ->notEmptyString('middle_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 32)
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name');

        $validator
            ->scalar('suffix')
            ->maxLength('suffix', 32)
            ->requirePresence('suffix', 'create')
            ->notEmptyString('suffix');

        $validator
            ->scalar('nickname')
            ->maxLength('nickname', 32)
            ->requirePresence('nickname', 'create')
            ->notEmptyString('nickname');

        $validator
            ->scalar('ballotpedia')
            ->maxLength('ballotpedia', 64)
            ->allowEmptyString('ballotpedia');

        $validator
            ->notEmptyString('followthemoney_eid');

        $validator
            ->nonNegativeInteger('votesmart_id')
            ->notEmptyString('votesmart_id');

        $validator
            ->scalar('opensecrets_id')
            ->maxLength('opensecrets_id', 9)
            ->allowEmptyString('opensecrets_id');

        $validator
            ->nonNegativeInteger('knowwho_pid')
            ->notEmptyString('knowwho_pid');

        $validator
            ->scalar('person_hash')
            ->maxLength('person_hash', 8)
            ->requirePresence('person_hash', 'create')
            ->notEmptyString('person_hash');

        $validator
            ->scalar('person_district')
            ->maxLength('person_district', 9)
            ->allowEmptyString('person_district');

        $validator
            ->requirePresence('legiscan_people_chamber_id', 'create')
            ->notEmptyString('legiscan_people_chamber_id');

        $validator
            ->requirePresence('legiscan_state_id', 'create')
            ->notEmptyString('legiscan_state_id');

        $validator
            ->scalar('state_name')
            ->maxLength('state_name', 64)
            ->requirePresence('state_name', 'create')
            ->notEmptyString('state_name');

        $validator
            ->requirePresence('legiscan_session_id', 'create')
            ->notEmptyString('legiscan_session_id');

        $validator
            ->requirePresence('legiscan_chamber_id', 'create')
            ->notEmptyString('legiscan_chamber_id');

        $validator
            ->requirePresence('legiscan_current_chamber_id', 'create')
            ->notEmptyString('legiscan_current_chamber_id');

        $validator
            ->requirePresence('legiscan_bill_type_id', 'create')
            ->notEmptyString('legiscan_bill_type_id');

        $validator
            ->requirePresence('legiscan_status_id', 'create')
            ->notEmptyString('legiscan_status_id');

        $validator
            ->requirePresence('legiscan_pending_committee_id', 'create')
            ->notEmptyString('legiscan_pending_committee_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['legiscan_bill_id'], 'LegiscanBills'), ['errorField' => 'legiscan_bill_id']);
        $rules->add($rules->existsIn(['legiscan_bill_vote_id'], 'LegiscanBillVotes'), ['errorField' => 'legiscan_bill_vote_id']);
        $rules->add($rules->existsIn(['legiscan_party_id'], 'LegiscanParties'), ['errorField' => 'legiscan_party_id']);
        $rules->add($rules->existsIn(['legiscan_session_id'], 'LegiscanSessions'), ['errorField' => 'legiscan_session_id']);
        $rules->add($rules->existsIn(['legiscan_chamber_id'], 'LegiscanChambers'), ['errorField' => 'legiscan_chamber_id']);

        return $rules;
    }
}
