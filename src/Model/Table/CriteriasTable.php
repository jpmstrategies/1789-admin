<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Criterias Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\PartiesTable&\Cake\ORM\Association\BelongsTo $Parties
 * @property \App\Model\Table\CriteriaDetailsTable&\Cake\ORM\Association\HasMany $CriteriaDetails
 * @property \App\Model\Table\RatingCriteriasTable&\Cake\ORM\Association\HasMany $RatingCriterias
 *
 * @method \App\Model\Entity\Criteria newEmptyEntity()
 * @method \App\Model\Entity\Criteria newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Criteria[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Criteria get($primaryKey, $options = [])
 * @method \App\Model\Entity\Criteria findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Criteria patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Criteria[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Criteria|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Criteria saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Criteria[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Criteria[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Criteria[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Criteria[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CriteriasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('criterias');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tenant');
        $this->addBehavior('ADmad/Sequence.Sequence', [
            'sequenceField' => 'sort_order', // Field to use to store integer sequence. Default "position".
            'scope' => ['tenant_id'], // Array of field names to use for grouping records. Default [].
            'startAt' => 1, // Initial value for sequence. Default 1.
        ]);

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Parties', [
            'foreignKey' => 'party_id',
        ]);
        $this->hasMany('CriteriaDetails', [
            'foreignKey' => 'criteria_id',
        ]);
        $this->hasMany('RatingCriterias', [
            'foreignKey' => 'criteria_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('name')
            ->maxLength('name', 45)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('th_detail_name')
            ->maxLength('th_detail_name', 45)
            ->requirePresence('th_detail_name', 'create')
            ->notEmptyString('th_detail_name');

        $validator
            ->scalar('th_vote_name')
            ->maxLength('th_vote_name', 45)
            ->requirePresence('th_vote_name', 'create')
            ->notEmptyString('th_vote_name');

        $validator
            ->scalar('method')
            ->maxLength('method', 1)
            ->notEmptyString('method');

        $validator
            ->uuid('party_id')
            ->allowEmptyString('party_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);
        $rules->add($rules->existsIn(['party_id'], 'Parties'), ['errorField' => 'party_id']);

        // leave off tenant_id because validation doesn't get trigger and this combination should be unique across tenants
        $rules->add($rules->isUnique(['party_id']), [
            'errorField' => 'party_id',
            'message' => 'A criteria already exists for this party.',
        ]);

        return $rules;
    }

    // set default sorting if one isn't provided and sort_order exists
    public function beforeFind($event, Query $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !(is_countable($order) ? count($order) : 0)) {
            $query->order([$this->getAlias() . '.sort_order' => 'ASC']);
        }
    }
}
