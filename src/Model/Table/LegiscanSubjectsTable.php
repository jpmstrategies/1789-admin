<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LegiscanSubjects Model
 *
 * @property \App\Model\Table\LegiscanBillSubjectsTable&\Cake\ORM\Association\HasMany $LegiscanBillSubjects
 *
 * @method \App\Model\Entity\LegiscanSubject newEmptyEntity()
 * @method \App\Model\Entity\LegiscanSubject newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanSubject[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanSubject get($primaryKey, $options = [])
 * @method \App\Model\Entity\LegiscanSubject findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\LegiscanSubject patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanSubject[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanSubject|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanSubject saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanSubject[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanSubject[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanSubject[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanSubject[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class LegiscanSubjectsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('legiscan_subjects');
        $this->setDisplayField('subject_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('TenantState');

        $this->hasMany('LegiscanBillSubjects', [
            'foreignKey' => 'legiscan_subject_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('state')
            ->maxLength('state', 2)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->nonNegativeInteger('id')
            ->requirePresence('id', 'create')
            ->notEmptyString('id');

        $validator
            ->scalar('subject_name')
            ->maxLength('subject_name', 128)
            ->requirePresence('subject_name', 'create')
            ->notEmptyString('subject_name');

        return $validator;
    }
}
