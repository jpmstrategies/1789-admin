<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TenantsBills Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\LegiscanBillsTable&\Cake\ORM\Association\BelongsTo $LegiscanBills
 * @property \App\Model\Table\PrioritiesTable&\Cake\ORM\Association\BelongsTo $Priorities
 *
 * @method \App\Model\Entity\TenantsBill newEmptyEntity()
 * @method \App\Model\Entity\TenantsBill newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\TenantsBill[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TenantsBill get($primaryKey, $options = [])
 * @method \App\Model\Entity\TenantsBill findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\TenantsBill patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TenantsBill[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\TenantsBill|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TenantsBill saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TenantsBill[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\TenantsBill[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\TenantsBill[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\TenantsBill[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class TenantsBillsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('tenants_bills');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Tenant');

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegiscanBills', [
            'foreignKey' => 'legiscan_bill_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Priorities', [
            'foreignKey' => 'priority_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('legiscan_bill_id')
            ->notEmptyString('legiscan_bill_id');

        $validator
            ->uuid('priority_id')
            ->allowEmptyString('priority_id');

        $validator
            ->scalar('notes')
            ->allowEmptyString('notes');

        $validator
            ->scalar('status')
            ->maxLength('status', 255)
            ->allowEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);
        $rules->add($rules->existsIn(['legiscan_bill_id'], 'LegiscanBills'), ['errorField' => 'legiscan_bill_id']);
        $rules->add($rules->existsIn(['priority_id'], 'Priorities'), ['errorField' => 'priority_id']);

        return $rules;
    }
}
