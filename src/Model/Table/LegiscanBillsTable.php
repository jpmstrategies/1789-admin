<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LegiscanBills Model
 *
 * @property \App\Model\Table\LegiscanChambersTable&\Cake\ORM\Association\BelongsTo $LegiscanChambers
 * @property \App\Model\Table\LegiscanSessionsTable&\Cake\ORM\Association\BelongsTo $LegiscanSessions
 * @property \App\Model\Table\LegiscanBillSubjectsTable&\Cake\ORM\Association\HasMany $LegiscanBillSubjects
 * @property \App\Model\Table\LegiscanBillVoteDetailsTable&\Cake\ORM\Association\HasMany $LegiscanBillVoteDetails
 * @property \App\Model\Table\LegiscanBillVotesTable&\Cake\ORM\Association\HasMany $LegiscanBillVotes
 * @property \App\Model\Table\TenantsBillsTable&\Cake\ORM\Association\HasMany $TenantsBills
 *
 * @method \App\Model\Entity\LegiscanBill newEmptyEntity()
 * @method \App\Model\Entity\LegiscanBill newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBill[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBill get($primaryKey, $options = [])
 * @method \App\Model\Entity\LegiscanBill findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\LegiscanBill patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBill[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBill|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanBill saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanBill[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanBill[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanBill[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanBill[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LegiscanBillsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('legiscan_bills');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('TenantState');

        $this->belongsTo('LegiscanChambers', [
            'className' => 'LegiscanChambers',
            'foreignKey' => 'legiscan_chamber_id',
            'joinType' => 'INNER',
            'propertyName' => 'origination_chamber'
        ]);
        $this->belongsTo('LegiscanCurrentChambers', [
            'className' => 'LegiscanChambers',
            'foreignKey' => 'legiscan_current_chamber_id',
            'joinType' => 'INNER',
            'propertyName' => 'current_chamber'
        ]);
        $this->belongsTo('LegiscanSessions', [
            'foreignKey' => 'legiscan_session_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('LegiscanBillSubjects', [
            'foreignKey' => 'legiscan_bill_id',
        ]);
        $this->hasMany('LegiscanBillVoteDetails', [
            'foreignKey' => 'legiscan_bill_id',
        ]);
        $this->hasMany('LegiscanBillVotes', [
            'foreignKey' => 'legiscan_bill_id',
        ]);
        $this->hasMany('RatingCriterias', [
            'foreignKey' => 'legiscan_bill_id',
        ]);
        $this->hasMany('TenantsBills', [
            'foreignKey' => 'legiscan_bill_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->requirePresence('id', 'create')
            ->notEmptyString('id');

        $validator
            ->scalar('state')
            ->maxLength('state', 2)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->scalar('bill_number')
            ->maxLength('bill_number', 10)
            ->requirePresence('bill_number', 'create')
            ->notEmptyString('bill_number');

        $validator
            ->requirePresence('legiscan_status_id', 'create')
            ->notEmptyString('legiscan_status_id');

        $validator
            ->scalar('status_desc')
            ->maxLength('status_desc', 24)
            ->allowEmptyString('status_desc');

        $validator
            ->date('status_date')
            ->allowEmptyDate('status_date');

        $validator
            ->scalar('title')
            ->requirePresence('title', 'create')
            ->notEmptyString('title');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->requirePresence('legiscan_bill_type_id', 'create')
            ->notEmptyString('legiscan_bill_type_id');

        $validator
            ->scalar('bill_type_name')
            ->maxLength('bill_type_name', 64)
            ->requirePresence('bill_type_name', 'create')
            ->notEmptyString('bill_type_name');

        $validator
            ->scalar('bill_type_abbr')
            ->maxLength('bill_type_abbr', 4)
            ->requirePresence('bill_type_abbr', 'create')
            ->notEmptyString('bill_type_abbr');

        $validator
            ->requirePresence('legiscan_chamber_id', 'create')
            ->notEmptyString('legiscan_chamber_id');

        $validator
            ->scalar('body_abbr')
            ->maxLength('body_abbr', 1)
            ->requirePresence('body_abbr', 'create')
            ->notEmptyString('body_abbr');

        $validator
            ->scalar('body_short')
            ->maxLength('body_short', 16)
            ->allowEmptyString('body_short');

        $validator
            ->scalar('body_name')
            ->maxLength('body_name', 128)
            ->requirePresence('body_name', 'create')
            ->notEmptyString('body_name');

        $validator
            ->requirePresence('legiscan_current_chamber_id', 'create')
            ->notEmptyString('legiscan_current_chamber_id');

        $validator
            ->scalar('current_body_abbr')
            ->maxLength('current_body_abbr', 1)
            ->requirePresence('current_body_abbr', 'create')
            ->notEmptyString('current_body_abbr');

        $validator
            ->scalar('current_body_short')
            ->maxLength('current_body_short', 16)
            ->allowEmptyString('current_body_short');

        $validator
            ->scalar('current_body_name')
            ->maxLength('current_body_name', 128)
            ->requirePresence('current_body_name', 'create')
            ->notEmptyString('current_body_name');

        $validator
            ->requirePresence('legiscan_pending_committee_id', 'create')
            ->notEmptyString('legiscan_pending_committee_id');

        $validator
            ->allowEmptyString('legiscan_pending_committee_chamber_id');

        $validator
            ->scalar('pending_committee_body_abbr')
            ->maxLength('pending_committee_body_abbr', 1)
            ->allowEmptyString('pending_committee_body_abbr');

        $validator
            ->scalar('pending_committee_body_short')
            ->maxLength('pending_committee_body_short', 16)
            ->allowEmptyString('pending_committee_body_short');

        $validator
            ->scalar('pending_committee_body_name')
            ->maxLength('pending_committee_body_name', 128)
            ->allowEmptyString('pending_committee_body_name');

        $validator
            ->scalar('pending_committee_name')
            ->maxLength('pending_committee_name', 128)
            ->allowEmptyString('pending_committee_name');

        $validator
            ->scalar('legiscan_url')
            ->maxLength('legiscan_url', 255)
            ->requirePresence('legiscan_url', 'create')
            ->notEmptyString('legiscan_url');

        $validator
            ->scalar('state_url')
            ->maxLength('state_url', 255)
            ->requirePresence('state_url', 'create')
            ->notEmptyString('state_url');

        $validator
            ->scalar('change_hash')
            ->maxLength('change_hash', 32)
            ->requirePresence('change_hash', 'create')
            ->notEmptyString('change_hash');

        $validator
            ->requirePresence('legiscan_state_id', 'create')
            ->notEmptyString('legiscan_state_id');

        $validator
            ->scalar('state_name')
            ->maxLength('state_name', 64)
            ->requirePresence('state_name', 'create')
            ->notEmptyString('state_name');

        $validator
            ->requirePresence('legiscan_session_id', 'create')
            ->notEmptyString('legiscan_session_id');

        $validator
            ->requirePresence('session_year_start', 'create')
            ->notEmptyString('session_year_start');

        $validator
            ->requirePresence('session_year_end', 'create')
            ->notEmptyString('session_year_end');

        $validator
            ->notEmptyFile('session_prefile');

        $validator
            ->notEmptyString('session_sine_die');

        $validator
            ->notEmptyString('session_prior');

        $validator
            ->requirePresence('session_special', 'create')
            ->notEmptyString('session_special');

        $validator
            ->scalar('session_tag')
            ->maxLength('session_tag', 32)
            ->requirePresence('session_tag', 'create')
            ->notEmptyString('session_tag');

        $validator
            ->scalar('session_title')
            ->maxLength('session_title', 64)
            ->requirePresence('session_title', 'create')
            ->notEmptyString('session_title');

        $validator
            ->scalar('session_name')
            ->maxLength('session_name', 64)
            ->requirePresence('session_name', 'create')
            ->notEmptyString('session_name');

        $validator
            ->integer('has_bill_vote_details')
            ->notEmptyString('has_bill_vote_details');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['legiscan_chamber_id'], 'LegiscanChambers'), ['errorField' => 'legiscan_chamber_id']);
        $rules->add($rules->existsIn(['legiscan_session_id'], 'LegiscanSessions'), ['errorField' => 'legiscan_session_id']);

        return $rules;
    }
}
