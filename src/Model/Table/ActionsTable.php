<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Actions Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\RatingTypesTable&\Cake\ORM\Association\BelongsTo $RatingTypes
 * @property \App\Model\Table\ActionTypesTable&\Cake\ORM\Association\BelongsTo $ActionTypes
 * @property \App\Model\Table\LegiscanActionsTable&\Cake\ORM\Association\BelongsTo $LegiscanActions
 * @property \App\Model\Table\RatingCriteriasTable&\Cake\ORM\Association\HasMany $RatingCriterias
 * @property \App\Model\Table\RatingsTable&\Cake\ORM\Association\HasMany $Ratings
 * @property \App\Model\Table\RecordsTable&\Cake\ORM\Association\HasMany $Records
 *
 * @method \App\Model\Entity\Action newEmptyEntity()
 * @method \App\Model\Entity\Action newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Action[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Action get($primaryKey, $options = [])
 * @method \App\Model\Entity\Action findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Action patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Action[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Action|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Action saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Action[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Action[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Action[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Action[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ActionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('actions');
        $this->setDisplayField('description');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tenant');

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('RatingTypes', [
            'foreignKey' => 'rating_type_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ActionTypes', [
            'foreignKey' => 'action_type_id',
        ]);
        $this->belongsTo('LegiscanActions', [
            'foreignKey' => 'legiscan_action_id',
        ]);
        $this->hasMany('RatingCriterias', [
            'foreignKey' => 'action_id',
        ]);
        $this->hasMany('Ratings', [
            'foreignKey' => 'action_id',
        ]);
        $this->hasMany('Records', [
            'foreignKey' => 'action_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('rating_type_id')
            ->requirePresence('rating_type_id', 'create')
            ->notEmptyString('rating_type_id');

        $validator
            ->uuid('action_type_id')
            ->allowEmptyString('action_type_id');

        $validator
            ->scalar('name')
            ->maxLength('name', 25)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->maxLength('description', 45)
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->decimal('denom_value')
            ->allowEmptyString('denom_value');

        $validator
            ->decimal('multiplier_support')
            ->allowEmptyString('multiplier_support');

        $validator
            ->decimal('multiplier_oppose')
            ->allowEmptyString('multiplier_oppose');

        $validator
            ->allowEmptyString('legiscan_action_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);
        $rules->add($rules->existsIn(['rating_type_id'], 'RatingTypes'), ['errorField' => 'rating_type_id']);
        $rules->add($rules->existsIn(['action_type_id'], 'ActionTypes'), ['errorField' => 'action_type_id']);
        $rules->add($rules->existsIn(['legiscan_action_id'], 'LegiscanActions'), ['errorField' => 'legiscan_action_id']);

        return $rules;
    }
}
