<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LegiscanChambers Model
 *
 * @property \App\Model\Table\ChambersTable&\Cake\ORM\Association\HasMany $Chambers
 * @property \App\Model\Table\LegiscanBillSubjectsTable&\Cake\ORM\Association\HasMany $LegiscanBillSubjects
 * @property \App\Model\Table\LegiscanBillVoteDetailsTable&\Cake\ORM\Association\HasMany $LegiscanBillVoteDetails
 * @property \App\Model\Table\LegiscanBillVotesTable&\Cake\ORM\Association\HasMany $LegiscanBillVotes
 * @property \App\Model\Table\LegiscanBillsTable&\Cake\ORM\Association\HasMany $LegiscanBills
 *
 * @method \App\Model\Entity\LegiscanChamber newEmptyEntity()
 * @method \App\Model\Entity\LegiscanChamber newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanChamber[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanChamber get($primaryKey, $options = [])
 * @method \App\Model\Entity\LegiscanChamber findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\LegiscanChamber patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanChamber[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanChamber|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanChamber saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanChamber[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanChamber[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanChamber[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanChamber[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class LegiscanChambersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('legiscan_chambers');
        $this->setDisplayField('body_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('TenantState');

        $this->hasMany('Chambers', [
            'foreignKey' => 'legiscan_chamber_id',
        ]);
        $this->hasMany('LegiscanBillSubjects', [
            'foreignKey' => 'legiscan_chamber_id',
        ]);
        $this->hasMany('LegiscanBillVoteDetails', [
            'foreignKey' => 'legiscan_chamber_id',
        ]);
        $this->hasMany('LegiscanBillVotes', [
            'foreignKey' => 'legiscan_chamber_id',
        ]);
        $this->hasMany('LegiscanBills', [
            'foreignKey' => 'legiscan_chamber_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('state')
            ->maxLength('state', 2)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->requirePresence('id', 'create')
            ->notEmptyString('id');

        $validator
            ->scalar('body_abbr')
            ->maxLength('body_abbr', 1)
            ->requirePresence('body_abbr', 'create')
            ->notEmptyString('body_abbr');

        $validator
            ->scalar('body_short')
            ->maxLength('body_short', 16)
            ->allowEmptyString('body_short');

        $validator
            ->scalar('body_name')
            ->maxLength('body_name', 128)
            ->requirePresence('body_name', 'create')
            ->notEmptyString('body_name');

        $validator
            ->requirePresence('role_id', 'create')
            ->notEmptyString('role_id');

        $validator
            ->scalar('role_abbr')
            ->maxLength('role_abbr', 3)
            ->allowEmptyString('role_abbr');

        $validator
            ->scalar('role_name')
            ->maxLength('role_name', 15)
            ->allowEmptyString('role_name');

        return $validator;
    }
}
