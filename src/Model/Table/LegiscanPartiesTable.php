<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LegiscanParties Model
 *
 * @property \App\Model\Table\LegiscanBillVoteDetailsTable&\Cake\ORM\Association\HasMany $LegiscanBillVoteDetails
 * @property \App\Model\Table\PartiesTable&\Cake\ORM\Association\HasMany $Parties
 *
 * @method \App\Model\Entity\LegiscanParty newEmptyEntity()
 * @method \App\Model\Entity\LegiscanParty newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanParty[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanParty get($primaryKey, $options = [])
 * @method \App\Model\Entity\LegiscanParty findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\LegiscanParty patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanParty[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanParty|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanParty saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanParty[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanParty[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanParty[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanParty[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class LegiscanPartiesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('legiscan_parties');
        $this->setDisplayField('party_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('TenantState');

        $this->hasMany('LegiscanBillVoteDetails', [
            'foreignKey' => 'legiscan_party_id',
        ]);
        $this->hasMany('Parties', [
            'foreignKey' => 'legiscan_party_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('state')
            ->maxLength('state', 2)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->requirePresence('id', 'create')
            ->notEmptyString('id');

        $validator
            ->scalar('party_abbr')
            ->maxLength('party_abbr', 1)
            ->requirePresence('party_abbr', 'create')
            ->notEmptyString('party_abbr');

        $validator
            ->scalar('party_short')
            ->maxLength('party_short', 3)
            ->requirePresence('party_short', 'create')
            ->notEmptyString('party_short');

        $validator
            ->scalar('party_name')
            ->maxLength('party_name', 32)
            ->requirePresence('party_name', 'create')
            ->notEmptyString('party_name');

        return $validator;
    }
}
