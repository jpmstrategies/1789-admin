<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Uploads Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\BillsTable&\Cake\ORM\Association\HasMany $Bills
 *
 * @method \App\Model\Entity\Upload newEmptyEntity()
 * @method \App\Model\Entity\Upload newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Upload[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Upload get($primaryKey, $options = [])
 * @method \App\Model\Entity\Upload findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Upload patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Upload[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Upload|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Upload saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Upload[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Upload[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Upload[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Upload[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UploadsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('uploads');
    $this->setDisplayField('id');
        $this->setPrimaryKey('id');

    $this->addBehavior('Timestamp');
            $this->addBehavior('Tenant');
                            
        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Bills', [
            'foreignKey' => 'upload_id',
        ]);
}

    /**
    * Default validation rules.
    *
    * @param \Cake\Validation\Validator $validator Validator instance.
    */
    public function validationDefault(Validator $validator): Validator
    {
                                                                $validator
                                                        ->scalar('path')
                                                        ->maxLength('path', 255)
                                                        ->requirePresence('path', 'create')
                                                                                                    ->notEmptyString('path');
                
                                                                        $validator
                                                        ->scalar('mime')
                                                        ->maxLength('mime', 255)
                                                        ->requirePresence('mime', 'create')
                                                                                                    ->notEmptyString('mime');
                
                                                                        $validator
                                                        ->requirePresence('width', 'create')
                                                                                                    ->notEmptyString('width');
                
                                                                        $validator
                                                        ->requirePresence('height', 'create')
                                                                                                    ->notEmptyString('height');
                
                            return $validator;
    }

    /**
    * Returns a rules checker object that will be used for validating
    * application integrity.
    *
    * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
    */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
            $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);
    
    return $rules;
    }
                    }
