<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Organizations Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\LegislativeSessionsTable&\Cake\ORM\Association\BelongsTo $LegislativeSessions
 * @property \App\Model\Table\ScoresTable&\Cake\ORM\Association\HasMany $Scores
 *
 * @method \App\Model\Entity\Organization newEmptyEntity()
 * @method \App\Model\Entity\Organization newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Organization[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Organization get($primaryKey, $options = [])
 * @method \App\Model\Entity\Organization findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Organization patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Organization[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Organization|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Organization saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Organization[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Organization[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Organization[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Organization[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrganizationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('organizations');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tenant');
        $this->addBehavior('ADmad/Sequence.Sequence', [
            'sequenceField' => 'sort_order', // Field to use to store integer sequence. Default "position".
            'scope' => ['tenant_id'], // Array of field names to use for grouping records. Default [].
            'startAt' => 1, // Initial value for sequence. Default 1.
        ]);

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Tenants', [
            'foreignKey' => 'org_tenant_id',
        ]);
        $this->belongsTo('LegislativeSessions', [
            'foreignKey' => 'sync_legislative_session_id',
        ]);
        $this->hasMany('Scores', [
            'foreignKey' => 'organization_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('org_tenant_id')
            ->allowEmptyString('org_tenant_id');

        $validator
            ->uuid('sync_legislative_session_id')
            ->allowEmptyString('sync_legislative_session_id');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('website')
            ->maxLength('website', 100)
            ->allowEmptyString('website');

        $validator
            ->scalar('image_url')
            ->maxLength('image_url', 100)
            ->allowEmptyFile('image_url');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);
        $rules->add($rules->existsIn(['org_tenant_id'], 'Tenants'), ['errorField' => 'org_tenant_id']);
        $rules->add($rules->existsIn(['sync_legislative_session_id'], 'LegislativeSessions'), ['errorField' => 'sync_legislative_session_id']);

        return $rules;
    }

    // set default sorting if one isn't provided and sort_order exists
    public function beforeFind($event, Query $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !(is_countable($order) ? count($order) : 0)) {
            $query->order([$this->getAlias() . '.sort_order' => 'ASC']);
        }
    }
}
