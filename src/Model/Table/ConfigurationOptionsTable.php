<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ConfigurationOptions Model
 *
 * @property \App\Model\Table\ConfigurationTypesTable&\Cake\ORM\Association\BelongsTo $ConfigurationTypes
 *
 * @method \App\Model\Entity\ConfigurationOption newEmptyEntity()
 * @method \App\Model\Entity\ConfigurationOption newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ConfigurationOption[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ConfigurationOption get($primaryKey, $options = [])
 * @method \App\Model\Entity\ConfigurationOption findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ConfigurationOption patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ConfigurationOption[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ConfigurationOption|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConfigurationOption saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConfigurationOption[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ConfigurationOption[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ConfigurationOption[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ConfigurationOption[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ConfigurationOptionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('configuration_options');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('ADmad/Sequence.Sequence', [
            'sequenceField' => 'sort_order', // Field to use to store integer sequence. Default "position".
            'scope' => ['configuration_type_id'], // Array of field names to use for grouping records. Default [].
            'startAt' => 1, // Initial value for sequence. Default 1.
        ]);

        $this->belongsTo('ConfigurationTypes', [
            'foreignKey' => 'configuration_type_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('value')
            ->maxLength('value', 4_294_967_295)
            ->allowEmptyString('value');

        $validator
            ->integer('sort_order')
            ->notEmptyString('sort_order');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['configuration_type_id'], 'ConfigurationTypes'), ['errorField' => 'configuration_type_id']);

        return $rules;
    }

    // set default sorting if one isn't provided
    public function beforeFind($event, Query $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !(is_countable($order) ? count($order) : 0)) {
            $query->order([$this->getAlias() . '.sort_order' => 'ASC']);
        }
    }
}
