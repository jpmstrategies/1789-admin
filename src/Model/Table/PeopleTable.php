<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * People Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\GradesTable&\Cake\ORM\Association\BelongsTo $Grades
 * @property \App\Model\Table\AddressesTable&\Cake\ORM\Association\HasMany $Addresses
 * @property \App\Model\Table\LegislatorsTable&\Cake\ORM\Association\HasMany $Legislators
 * @property \App\Model\Table\TagsTable&\Cake\ORM\Association\BelongsToMany $Tags
 *
 * @method \App\Model\Entity\Person newEmptyEntity()
 * @method \App\Model\Entity\Person newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Person[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Person get($primaryKey, $options = [])
 * @method \App\Model\Entity\Person findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Person patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Person[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Person|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Person saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Person[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Person[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Person[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Person[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PeopleTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('people');
        $this->setDisplayField('full_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tenant');

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Grades', [
            'foreignKey' => 'grade_id',
        ]);
        $this->hasMany('Addresses', [
            'foreignKey' => 'person_id',
        ]);
        $this->hasMany('Legislators', [
            'foreignKey' => 'person_id',
        ]);
        $this->belongsToMany('Tags', [
            'foreignKey' => 'person_id',
            'targetForeignKey' => 'tag_id',
            'joinTable' => 'people_tags',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('full_name')
            ->maxLength('full_name', 100)
            ->requirePresence('full_name', 'create')
            ->notEmptyString('full_name');

        $validator
            ->scalar('nickname')
            ->maxLength('nickname', 45)
            ->allowEmptyString('nickname');

        $validator
            ->scalar('salutation')
            ->maxLength('salutation', 3)
            ->allowEmptyString('salutation');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 45)
            ->allowEmptyString('first_name');

        $validator
            ->scalar('middle_name')
            ->maxLength('middle_name', 45)
            ->allowEmptyString('middle_name');

        $validator
            ->scalar('initials')
            ->maxLength('initials', 10)
            ->allowEmptyString('initials');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 45)
            ->allowEmptyString('last_name');

        $validator
            ->scalar('suffix')
            ->maxLength('suffix', 3)
            ->allowEmptyString('suffix');

        $validator
            ->scalar('bio')
            ->maxLength('bio', 4_294_967_295)
            ->allowEmptyString('bio');

        $validator
            ->scalar('committee_list')
            ->maxLength('committee_list', 4_294_967_295)
            ->allowEmptyString('committee_list');

        $validator
            ->scalar('image_url')
            ->maxLength('image_url', 255)
            ->allowEmptyString('image_url');

        $validator
            ->scalar('ballotpedia_url')
            ->maxLength('ballotpedia_url', 255)
            ->allowEmptyString('ballotpedia_url');

        $validator
            ->scalar('first_elected')
            ->maxLength('first_elected', 4)
            ->allowEmptyString('first_elected');

        $validator
            ->scalar('district_city')
            ->maxLength('district_city', 64)
            ->allowEmptyString('district_city');

        $validator
            ->uuid('grade_id')
            ->allowEmptyString('grade_id');

        $validator
            ->decimal('grade_number')
            ->allowEmptyString('grade_number');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->requirePresence('slug', 'create')
            ->add('slug', 'validFormat', [
                //regex via https://stackoverflow.com/a/19256344/103297
                'rule' => ['custom', '/^[A-z0-9]+(?:-[A-z0-9]+)*$/'],
                'message' => 'Only lower case alphanumeric and dashes allowed.'
            ]);

        $validator
            ->allowEmptyString('legiscan_people_id');

        $validator
            ->scalar('os_pk')
            ->maxLength('os_pk', 10)
            ->allowEmptyString('os_pk');

        $validator
            ->scalar('state_pk')
            ->maxLength('state_pk', 45)
            ->allowEmptyString('state_pk');

        $validator
            ->email('email')
            ->allowEmptyString('email');

        $validator
            ->scalar('facebook')
            ->maxLength('facebook', 50, 'Max of 50 characters allowed')
            ->regex('facebook', '/^[A-Za-z0-9_\.]{1,100}$/', 'Invalid username format')
            ->allowEmptyString('facebook');

        $validator
            ->scalar('twitter')
            ->maxLength('twitter', 15, 'Max of 15 characters allowed')
            ->regex('twitter', '/^[A-Za-z0-9_]{1,15}$/', 'Invalid handle format')
            ->allowEmptyString('twitter');

        $validator
            ->boolean('is_enabled')
            ->notEmptyString('is_enabled');

        $validator
            ->boolean('is_retired')
            ->notEmptyString('is_retired');

        $validator
            ->scalar('ttx_candidate_slug')
            ->maxLength('ttx_candidate_slug', 255)
            ->allowEmptyString('ttx_candidate_slug');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 15)
            ->allowEmptyString('phone');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);
        $rules->add($rules->existsIn(['grade_id'], 'Grades'), ['errorField' => 'grade_id']);

        return $rules;
    }
}
