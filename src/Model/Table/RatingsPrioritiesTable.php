<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RatingsPriorities Model
 *
 * @property \App\Model\Table\RatingsTable&\Cake\ORM\Association\BelongsTo $Ratings
 * @property \App\Model\Table\PrioritiesTable&\Cake\ORM\Association\BelongsTo $Priorities
 *
 * @method \App\Model\Entity\RatingsPriority newEmptyEntity()
 * @method \App\Model\Entity\RatingsPriority newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\RatingsPriority[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RatingsPriority get($primaryKey, $options = [])
 * @method \App\Model\Entity\RatingsPriority findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\RatingsPriority patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RatingsPriority[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\RatingsPriority|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RatingsPriority saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RatingsPriority[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RatingsPriority[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\RatingsPriority[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RatingsPriority[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class RatingsPrioritiesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('ratings_priorities');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Ratings', [
            'foreignKey' => 'rating_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Priorities', [
            'foreignKey' => 'priority_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('rating_id')
            ->requirePresence('rating_id', 'create')
            ->notEmptyString('rating_id');

        $validator
            ->uuid('priority_id')
            ->requirePresence('priority_id', 'create')
            ->notEmptyString('priority_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['rating_id'], 'Ratings'), ['errorField' => 'rating_id']);
        $rules->add($rules->existsIn(['priority_id'], 'Priorities'), ['errorField' => 'priority_id']);

        return $rules;
    }
}
