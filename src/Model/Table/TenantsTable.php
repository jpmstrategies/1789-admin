<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tenants Model
 *
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\HasMany $Accounts
 * @property \App\Model\Table\ActionTypesTable&\Cake\ORM\Association\HasMany $ActionTypes
 * @property \App\Model\Table\ActionsTable&\Cake\ORM\Association\HasMany $Actions
 * @property \App\Model\Table\AddressTypesTable&\Cake\ORM\Association\HasMany $AddressTypes
 * @property \App\Model\Table\AddressesTable&\Cake\ORM\Association\HasMany $Addresses
 * @property \App\Model\Table\ChambersTable&\Cake\ORM\Association\HasMany $Chambers
 * @property \App\Model\Table\ConfigurationsTable&\Cake\ORM\Association\HasMany $Configurations
 * @property \App\Model\Table\GradesTable&\Cake\ORM\Association\HasMany $Grades
 * @property \App\Model\Table\LegislativeSessionsTable&\Cake\ORM\Association\HasMany $LegislativeSessions
 * @property \App\Model\Table\LegislatorsTable&\Cake\ORM\Association\HasMany $Legislators
 * @property \App\Model\Table\OrganizationsTable&\Cake\ORM\Association\HasMany $Organizations
 * @property \App\Model\Table\PartiesTable&\Cake\ORM\Association\HasMany $Parties
 * @property \App\Model\Table\PeopleTable&\Cake\ORM\Association\HasMany $People
 * @property \App\Model\Table\PositionsTable&\Cake\ORM\Association\HasMany $Positions
 * @property \App\Model\Table\RatingFieldsTable&\Cake\ORM\Association\HasMany $RatingFields
 * @property \App\Model\Table\RatingTypesTable&\Cake\ORM\Association\HasMany $RatingTypes
 * @property \App\Model\Table\RatingsTable&\Cake\ORM\Association\HasMany $Ratings
 * @property \App\Model\Table\RecordsTable&\Cake\ORM\Association\HasMany $Records
 * @property \App\Model\Table\ScoresTable&\Cake\ORM\Association\HasMany $Scores
 *
 * @method \App\Model\Entity\Tenant newEmptyEntity()
 * @method \App\Model\Entity\Tenant newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Tenant[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tenant get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tenant findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Tenant patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tenant[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tenant|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tenant saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tenant[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Tenant[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Tenant[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Tenant[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TenantsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('tenants');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Accounts', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('ActionTypes', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Actions', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('AddressTypes', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Addresses', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Chambers', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Configurations', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Grades', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('LegislativeSessions', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Legislators', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Organizations', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Parties', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('People', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Positions', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Priorities', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('RatingFields', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('RatingTypes', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Ratings', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Records', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Scores', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Tags', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('TenantsBills', [
            'foreignKey' => 'tenant_id',
        ]);
        $this->hasMany('Titles', [
            'foreignKey' => 'tenant_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('domain_name')
            ->maxLength('domain_name', 50)
            ->requirePresence('domain_name', 'create')
            ->notEmptyString('domain_name')
            ->add('domain_name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('id_public')
            ->maxLength('id_public', 8)
            ->requirePresence('id_public', 'create')
            ->notEmptyString('id_public');

        $validator
            ->scalar('state')
            ->maxLength('state', 2)
            ->allowEmptyString('state');

        $validator
            ->scalar('type')
            ->maxLength('type', 1)
            ->notEmptyString('type');

        $validator
            ->scalar('data_source')
            ->maxLength('data_source', 255)
            ->notEmptyString('data_source');

        $validator
            ->scalar('layout')
            ->maxLength('layout', 50)
            ->notEmptyString('layout');

        $validator
            ->dateTime('sync_pending')
            ->allowEmptyDateTime('sync_pending');

        $validator
            ->boolean('is_enabled')
            ->notEmptyString('is_enabled');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['name']), ['errorField' => 'name']);
        $rules->add($rules->isUnique(['domain_name']), ['errorField' => 'domain_name']);

        return $rules;
    }
}
