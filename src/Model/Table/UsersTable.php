<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Authentication\PasswordHasher\DefaultPasswordHasher;

/**
 * Users Model
 *
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\HasMany $Accounts
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Accounts', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('password')
            ->minLength('password', 8, 'Password must be between 8 and 20 characters.')
            ->maxLength('password', 20, 'Password cannot exceed 20 characters.')
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->scalar('password_confirm')
            ->minLength('password_confirm', 8, 'Password must be between 8 and 20 characters.')
            ->maxLength('password_confirm', 20, 'Password cannot exceed 20 characters.');

        $validator->add('password_confirm_confirm', [
            'compare' => [
                'rule' => ['compareWith', 'password'],
                'message' => 'Those password didn\'t match. Try Again.'
            ]
        ]);

        $validator
            ->add('current_password', 'custom', [
                'rule' => function ($value, $context) {
                    $user = $this->get($context['data']['id']);
                    if ($user) {
                        if ((new DefaultPasswordHasher())->check($value, $user->password)) {
                            return true;
                        }
                    }
                    return false;
                },
                'message' => 'Please enter your current password.',
            ])
            ->notEmptyString('current_password');

        $validator->add('password_confirm', [
            'compare' => [
                'rule' => ['compareWith', 'password'],
                'message' => 'Those password didn\'t match. Try Again.'
            ]
        ]);

        $validator
            ->scalar('password_update')
            ->minLength('password_update', 8, 'Password must be between 8 and 20 characters.')
            ->maxLength('password_update', 20, 'Password cannot exceed 20 characters.');

        $validator->add('password_update_confirm', [
            'compare' => [
                'rule' => ['compareWith', 'password_update'],
                'message' => 'Those password didn\'t match. Try Again.'
            ]
        ]);

        $validator
            ->scalar('security_code')
            ->maxLength('security_code', 255)
            ->allowEmptyString('security_code');

        $validator
            ->allowEmptyString('user_details');

        $validator
            ->boolean('is_enabled')
            ->notEmptyString('is_enabled');

        $validator
            ->boolean('is_super')
            ->notEmptyString('is_super');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['email']), ['errorField' => 'email']);

        return $rules;
    }

    public function findAuth(\Cake\ORM\Query $query)
    {
        $query
            ->select(['id', 'email', 'password', 'is_super'])
            ->where(['Users.is_enabled' => 1]);

        return $query;
    }
}
