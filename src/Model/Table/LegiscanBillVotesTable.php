<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LegiscanBillVotes Model
 *
 * @property \App\Model\Table\LegiscanBillsTable&\Cake\ORM\Association\BelongsTo $LegiscanBills
 * @property \App\Model\Table\LegiscanSessionsTable&\Cake\ORM\Association\BelongsTo $LegiscanSessions
 * @property \App\Model\Table\LegiscanChambersTable&\Cake\ORM\Association\BelongsTo $LegiscanChambers
 * @property \App\Model\Table\LegiscanBillVoteDetailsTable&\Cake\ORM\Association\HasMany $LegiscanBillVoteDetails
 * @property \App\Model\Table\RatingsTable&\Cake\ORM\Association\HasMany $Ratings
 *
 * @method \App\Model\Entity\LegiscanBillVote newEmptyEntity()
 * @method \App\Model\Entity\LegiscanBillVote newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBillVote[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBillVote get($primaryKey, $options = [])
 * @method \App\Model\Entity\LegiscanBillVote findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\LegiscanBillVote patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBillVote[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBillVote|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanBillVote saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanBillVote[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanBillVote[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanBillVote[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanBillVote[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LegiscanBillVotesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('legiscan_bill_votes');
        $this->setDisplayField('roll_call_desc');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('TenantState');

        $this->belongsTo('LegiscanBills', [
            'foreignKey' => 'legiscan_bill_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegiscanSessions', [
            'foreignKey' => 'legiscan_session_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegiscanChambers', [
            'className' => 'LegiscanChambers',
            'foreignKey' => 'legiscan_chamber_id',
            'joinType' => 'INNER',
            'propertyName' => 'origination_chamber'
        ]);
        $this->belongsTo('LegiscanCurrentChambers', [
            'className' => 'LegiscanChambers',
            'foreignKey' => 'legiscan_current_chamber_id',
            'joinType' => 'INNER',
            'propertyName' => 'current_chamber'
        ]);
        $this->belongsTo('LegiscanVoteChambers', [
            'className' => 'LegiscanChambers',
            'foreignKey' => 'legiscan_bill_vote_chamber_id',
            'joinType' => 'INNER',
            'propertyName' => 'vote_chamber'
        ]);
        $this->hasMany('LegiscanBillVoteDetails', [
            'foreignKey' => 'legiscan_bill_vote_id',
        ]);
        $this->hasMany('Ratings', [
            'foreignKey' => 'legiscan_bill_vote_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->requirePresence('id', 'create')
            ->notEmptyString('id');

        $validator
            ->scalar('state')
            ->maxLength('state', 2)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->nonNegativeInteger('legiscan_bill_id')
            ->requirePresence('legiscan_bill_id', 'create')
            ->notEmptyString('legiscan_bill_id');

        $validator
            ->scalar('bill_number')
            ->maxLength('bill_number', 10)
            ->requirePresence('bill_number', 'create')
            ->notEmptyString('bill_number');

        $validator
            ->date('roll_call_date')
            ->allowEmptyDate('roll_call_date');

        $validator
            ->scalar('roll_call_desc')
            ->maxLength('roll_call_desc', 255)
            ->requirePresence('roll_call_desc', 'create')
            ->notEmptyString('roll_call_desc');

        $validator
            ->requirePresence('legiscan_bill_vote_chamber_id', 'create')
            ->notEmptyString('legiscan_bill_vote_chamber_id');

        $validator
            ->scalar('roll_call_body_abbr')
            ->maxLength('roll_call_body_abbr', 1)
            ->requirePresence('roll_call_body_abbr', 'create')
            ->notEmptyString('roll_call_body_abbr');

        $validator
            ->scalar('roll_call_body_short')
            ->maxLength('roll_call_body_short', 16)
            ->allowEmptyString('roll_call_body_short');

        $validator
            ->scalar('roll_call_body_name')
            ->maxLength('roll_call_body_name', 128)
            ->requirePresence('roll_call_body_name', 'create')
            ->notEmptyString('roll_call_body_name');

        $validator
            ->notEmptyString('yea');

        $validator
            ->notEmptyString('nay');

        $validator
            ->notEmptyString('nv');

        $validator
            ->notEmptyString('absent');

        $validator
            ->notEmptyString('total');

        $validator
            ->notEmptyString('passed');

        $validator
            ->scalar('legiscan_url')
            ->maxLength('legiscan_url', 255)
            ->requirePresence('legiscan_url', 'create')
            ->notEmptyString('legiscan_url');

        $validator
            ->scalar('state_url')
            ->maxLength('state_url', 255)
            ->requirePresence('state_url', 'create')
            ->notEmptyString('state_url');

        $validator
            ->requirePresence('legiscan_state_id', 'create')
            ->notEmptyString('legiscan_state_id');

        $validator
            ->scalar('state_name')
            ->maxLength('state_name', 64)
            ->requirePresence('state_name', 'create')
            ->notEmptyString('state_name');

        $validator
            ->requirePresence('legiscan_session_id', 'create')
            ->notEmptyString('legiscan_session_id');

        $validator
            ->requirePresence('legiscan_chamber_id', 'create')
            ->notEmptyString('legiscan_chamber_id');

        $validator
            ->requirePresence('legiscan_current_chamber_id', 'create')
            ->notEmptyString('legiscan_current_chamber_id');

        $validator
            ->requirePresence('legiscan_bill_type_id', 'create')
            ->notEmptyString('legiscan_bill_type_id');

        $validator
            ->requirePresence('legiscan_status_id', 'create')
            ->notEmptyString('legiscan_status_id');

        $validator
            ->requirePresence('legiscan_pending_committee_id', 'create')
            ->notEmptyString('legiscan_pending_committee_id');

        $validator
            ->integer('has_bill_vote_details')
            ->notEmptyString('has_bill_vote_details');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['legiscan_bill_id'], 'LegiscanBills'), ['errorField' => 'legiscan_bill_id']);
        $rules->add($rules->existsIn(['legiscan_session_id'], 'LegiscanSessions'), ['errorField' => 'legiscan_session_id']);
        $rules->add($rules->existsIn(['legiscan_chamber_id'], 'LegiscanChambers'), ['errorField' => 'legiscan_chamber_id']);

        return $rules;
    }
}
