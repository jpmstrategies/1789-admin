<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LegiscanActions Model
 *
 * @property \App\Model\Table\ActionsTable&\Cake\ORM\Association\HasMany $Actions
 *
 * @method \App\Model\Entity\LegiscanAction newEmptyEntity()
 * @method \App\Model\Entity\LegiscanAction newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanAction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanAction get($primaryKey, $options = [])
 * @method \App\Model\Entity\LegiscanAction findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\LegiscanAction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanAction[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanAction|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanAction saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanAction[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanAction[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanAction[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanAction[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class LegiscanActionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('legiscan_actions');
        $this->setPrimaryKey('id');
        $this->setDisplayField('description');

        $this->hasMany('Actions', [
            'foreignKey' => 'legiscan_action_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence('id', 'create')
            ->notEmptyString('id');

        $validator
            ->scalar('description')
            ->maxLength('description', 24)
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        return $validator;
    }
}
