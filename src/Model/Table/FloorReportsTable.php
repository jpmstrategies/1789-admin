<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FloorReports Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\LegislativeSessionsTable&\Cake\ORM\Association\BelongsTo $LegislativeSessions
 * @property \App\Model\Table\ChambersTable&\Cake\ORM\Association\BelongsTo $Chambers
 * @property \App\Model\Table\RatingsTable&\Cake\ORM\Association\BelongsToMany $Ratings
 *
 * @method \App\Model\Entity\FloorReport newEmptyEntity()
 * @method \App\Model\Entity\FloorReport newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\FloorReport[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FloorReport get($primaryKey, $options = [])
 * @method \App\Model\Entity\FloorReport findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\FloorReport patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FloorReport[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\FloorReport|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FloorReport saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FloorReport[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\FloorReport[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\FloorReport[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\FloorReport[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FloorReportsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('floor_reports');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tenant');

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegislativeSessions', [
            'foreignKey' => 'legislative_session_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Chambers', [
            'foreignKey' => 'chamber_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsToMany('Ratings', [
            'foreignKey' => 'floor_report_id',
            'targetForeignKey' => 'rating_id',
            'joinTable' => 'floor_reports_ratings',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('legislative_session_id')
            ->notEmptyString('legislative_session_id');

        $validator
            ->uuid('chamber_id')
            ->notEmptyString('chamber_id');

        $validator
            ->date('report_date')
            ->allowEmptyDate('report_date');

        $validator
            ->scalar('report_url')
            ->maxLength('report_url', 255)
            ->allowEmptyString('report_url');

        $validator
            ->scalar('import_log')
            ->allowEmptyString('import_log');

        $validator
            ->boolean('is_approved')
            ->notEmptyString('is_approved');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);
        $rules->add($rules->existsIn(['legislative_session_id'], 'LegislativeSessions'), ['errorField' => 'legislative_session_id']);
        $rules->add($rules->existsIn(['chamber_id'], 'Chambers'), ['errorField' => 'chamber_id']);

        return $rules;
    }


    // set default sorting if one isn't provided and sort_order exists
    public function beforeFind($event, Query $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !(is_countable($order) ? count($order) : 0)) {
            $query->order([$this->getAlias() . '.report_date' => 'DESC']);
        }
    }
}
