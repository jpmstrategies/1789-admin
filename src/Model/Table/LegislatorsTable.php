<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Legislators Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\LegislativeSessionsTable&\Cake\ORM\Association\BelongsTo $LegislativeSessions
 * @property \App\Model\Table\PeopleTable&\Cake\ORM\Association\BelongsTo $People
 * @property \App\Model\Table\ChambersTable&\Cake\ORM\Association\BelongsTo $Chambers
 * @property \App\Model\Table\PartiesTable&\Cake\ORM\Association\BelongsTo $Parties
 * @property \App\Model\Table\TitlesTable&\Cake\ORM\Association\BelongsTo $Titles
 * @property \App\Model\Table\GradesTable&\Cake\ORM\Association\BelongsTo $Grades
 * @property \App\Model\Table\RecordsTable&\Cake\ORM\Association\HasMany $Records
 * @property \App\Model\Table\ScoresTable&\Cake\ORM\Association\HasMany $Scores
 *
 * @method \App\Model\Entity\Legislator newEmptyEntity()
 * @method \App\Model\Entity\Legislator newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Legislator[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Legislator get($primaryKey, $options = [])
 * @method \App\Model\Entity\Legislator findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Legislator patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Legislator[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Legislator|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Legislator saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Legislator[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Legislator[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Legislator[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Legislator[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LegislatorsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('legislators');
        $this->setDisplayField('journal_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tenant');

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegislativeSessions', [
            'foreignKey' => 'legislative_session_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('People', [
            'foreignKey' => 'person_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Chambers', [
            'foreignKey' => 'chamber_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Parties', [
            'foreignKey' => 'party_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Titles', [
            'foreignKey' => 'title_id',
        ]);
        $this->belongsTo('Grades', [
            'foreignKey' => 'grade_id',
        ]);
        $this->hasMany('Records', [
            'foreignKey' => 'legislator_id',
        ]);
        $this->hasMany('Scores', [
            'foreignKey' => 'legislator_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('legislative_session_id')
            ->requirePresence('legislative_session_id', 'create')
            ->notEmptyString('legislative_session_id');

        $validator
            ->uuid('person_id')
            ->requirePresence('person_id', 'create')
            ->notEmptyString('person_id');

        $validator
            ->uuid('chamber_id')
            ->requirePresence('chamber_id', 'create')
            ->notEmptyString('chamber_id');

        $validator
            ->uuid('party_id')
            ->requirePresence('party_id', 'create')
            ->notEmptyString('party_id');

        $validator
            ->allowEmptyString('district');

        $validator
            ->scalar('journal_name')
            ->maxLength('journal_name', 25)
            ->allowEmptyString('journal_name');

        $validator
            ->uuid('title_id')
            ->allowEmptyString('title_id');

        $validator
            ->date('term_starts')
            ->allowEmptyDate('term_starts');

        $validator
            ->date('term_ends')
            ->allowEmptyDate('term_ends');

        $validator
            ->uuid('grade_id')
            ->allowEmptyString('grade_id');

        $validator
            ->decimal('grade_number')
            ->allowEmptyString('grade_number');

        $validator
            ->scalar('override_text')
            ->maxLength('override_text', 10)
            ->allowEmptyString('override_text');

        $validator
            ->boolean('is_enabled')
            ->notEmptyString('is_enabled');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);
        $rules->add($rules->existsIn(['legislative_session_id'], 'LegislativeSessions'), ['errorField' => 'legislative_session_id']);
        $rules->add($rules->existsIn(['person_id'], 'People'), ['errorField' => 'person_id']);
        $rules->add($rules->existsIn(['chamber_id'], 'Chambers'), ['errorField' => 'chamber_id']);
        $rules->add($rules->existsIn(['party_id'], 'Parties'), ['errorField' => 'party_id']);
        $rules->add($rules->existsIn(['title_id'], 'Titles'), ['errorField' => 'title_id']);
        $rules->add($rules->existsIn(['grade_id'], 'Grades'), ['errorField' => 'grade_id']);

        return $rules;
    }
}
