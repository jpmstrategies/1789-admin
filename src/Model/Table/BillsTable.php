<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bills Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\LegislativeSessionsTable&\Cake\ORM\Association\BelongsTo $LegislativeSessions
 * @property \App\Model\Table\ChambersTable&\Cake\ORM\Association\BelongsTo $FiledChamber
 * @property \App\Model\Table\ChambersTable&\Cake\ORM\Association\BelongsTo $CurrentChamber
 * @property \App\Model\Table\BillEnumsTable&\Cake\ORM\Association\BelongsTo $CurrentBillStage
 * @property \App\Model\Table\BillEnumsTable&\Cake\ORM\Association\BelongsTo $CurrentBillStatus
 * @property \App\Model\Table\BillEnumsTable&\Cake\ORM\Association\BelongsTo $CurrentBillCta
 * @property \App\Model\Table\BillEnumsTable&\Cake\ORM\Association\BelongsTo $currentBillProgress
 * @property \App\Model\Table\BillEnumsTable&\Cake\ORM\Association\BelongsTo $CurrentBillPhaseIcon
 * @property \App\Model\Table\BillEnumsTable&\Cake\ORM\Association\BelongsTo $CurrentBillRatingIcon
 * @property \App\Model\Table\RatingsTable&\Cake\ORM\Association\BelongsTo $HouseRating
 * @property \App\Model\Table\RatingsTable&\Cake\ORM\Association\BelongsTo $SenateRating
 *
 * @method \App\Model\Entity\Bill newEmptyEntity()
 * @method \App\Model\Entity\Bill newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Bill[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Bill get($primaryKey, $options = [])
 * @method \App\Model\Entity\Bill findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Bill patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Bill[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Bill|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bill saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bill[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Bill[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Bill[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Bill[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BillsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('bills');
        $this->setDisplayField('bill_number');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tenant');

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegislativeSessions', [
            'foreignKey' => 'legislative_session_id',
            'joinType' => 'INNER',
        ]);

        // Add associations for 'current' fields mapping to BillEnums
        $this->belongsTo('CurrentBillStage', [
            'className' => 'BillEnums',
            'foreignKey' => 'current_bill_stage_id',
            'conditions' => ['CurrentBillStage.type' => 'stage'],
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('CurrentBillStatus', [
            'className' => 'BillEnums',
            'foreignKey' => 'current_bill_status_id',
            'conditions' => ['CurrentBillStatus.type' => 'status'],
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('CurrentBillCta', [
            'className' => 'BillEnums',
            'foreignKey' => 'current_bill_cta_id',
            'conditions' => ['CurrentBillCta.type' => 'cta'],
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('CurrentBillProgress', [
            'className' => 'BillEnums',
            'foreignKey' => 'current_bill_progress_id',
            'conditions' => ['CurrentBillProgress.type' => 'progress'],
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('CurrentBillPhaseIcon', [
            'className' => 'BillEnums',
            'foreignKey' => 'current_bill_phase_icon_id',
            'conditions' => ['CurrentBillPhaseIcon.type' => 'phase-icon'],
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('CurrentBillRatingIcon', [
            'className' => 'BillEnums',
            'foreignKey' => 'current_bill_rating_icon_id',
            'conditions' => ['CurrentBillRatingIcon.type' => 'rating-icon'],
            'joinType' => 'LEFT',
        ]);

        // Chambers associations
        $this->belongsTo('FiledChamber', [
            'className' => 'Chambers',
            'foreignKey' => 'filed_chamber_id',
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('CurrentChamber', [
            'className' => 'Chambers',
            'foreignKey' => 'current_chamber_id',
            'joinType' => 'LEFT',
        ]);

        // Ratings associations
        $this->belongsTo('HouseRating', [
            'className' => 'Ratings',
            'foreignKey' => 'house_rating_id',
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('SenateRating', [
            'className' => 'Ratings',
            'foreignKey' => 'senate_rating_id',
            'joinType' => 'LEFT',
        ]);

        // LegiScan Bills association
        $this->belongsTo('LegiscanBill', [
            'className' => 'LegiscanBills',
            'foreignKey' => 'legiscan_bill_id',
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('Uploads', [
            'foreignKey' => 'upload_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('legislative_session_id')
            ->notEmptyString('legislative_session_id');

        $validator
            ->uuid('filed_chamber_id')
            ->notEmptyFile('filed_chamber_id');

        $validator
            ->scalar('bill_number')
            ->maxLength('bill_number', 45)
            ->requirePresence('bill_number', 'create')
            ->notEmptyString('bill_number');

        $validator
            ->scalar('official_title')
            ->maxLength('official_title', 255)
            ->requirePresence('official_title', 'create')
            ->notEmptyString('official_title');

        $validator
            ->scalar('explainer_title')
            ->maxLength('explainer_title', 255)
            ->requirePresence('explainer_title', 'create')
            ->notEmptyString('explainer_title');

        $validator
            ->scalar('description')
            ->maxLength('description', 4294967295)
            ->allowEmptyString('description');

        $validator
            ->scalar('notes')
            ->maxLength('notes', 4294967295)
            ->allowEmptyString('notes');

        $validator
            ->scalar('sponsors')
            ->maxLength('sponsors', 255)
            ->allowEmptyString('sponsors');

        $validator
            ->nonNegativeInteger('legiscan_bill_id')
            ->allowEmptyString('legiscan_bill_id');

        $validator
            ->uuid('current_chamber_id')
            ->allowEmptyString('current_chamber_id');

        $validator
            ->uuid('current_bill_stage_id')
            ->allowEmptyString('current_bill_stage_id');

        $validator
            ->uuid('current_bill_status_id')
            ->allowEmptyString('current_bill_status_id');

        $validator
            ->uuid('current_bill_cta_id')
            ->allowEmptyString('current_bill_cta_id');

        $validator
            ->add('current_bill_cta_url', 'validUrl', [
                'rule' => ['custom', '/^(https?:\/\/)([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/i'],
                'message' => 'The provided URL must start with http or https and be a valid URL.'
            ])
            ->allowEmptyString('current_bill_cta_url');

        $validator
            ->uuid('current_bill_progress_id')
            ->allowEmptyString('current_bill_progress_id');

        $validator
            ->uuid('current_bill_phase_icon_id')
            ->allowEmptyString('current_bill_phase_icon_id');

        $validator
            ->uuid('current_bill_rating_icon_id')
            ->allowEmptyString('current_bill_rating_icon_id');

        $validator
            ->uuid('house_rating_id')
            ->allowEmptyString('house_rating_id');

        $validator
            ->scalar('house_rating_votes')
            ->maxLength('house_rating_votes', 4294967295)
            ->allowEmptyString('house_rating_votes');

        $validator
            ->uuid('senate_rating_id')
            ->allowEmptyString('senate_rating_id');

        $validator
            ->scalar('senate_rating_votes')
            ->maxLength('senate_rating_votes', 4294967295)
            ->allowEmptyString('senate_rating_votes');

        $validator
            ->uuid('upload_id')
            ->allowEmptyString('upload_id');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->requirePresence('slug', 'create')
            ->notEmptyString('slug');

        $validator
            ->boolean('is_enabled')
            ->notEmptyString('is_enabled');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);
        $rules->add($rules->existsIn(['legislative_session_id'], 'LegislativeSessions'), ['errorField' => 'legislative_session_id']);

        // Rules to ensure Chambers exists
        $rules->add($rules->existsIn(['filed_chamber_id'], 'FiledChamber'), ['errorField' => 'filed_chamber_id']);
        $rules->add($rules->existsIn(['current_chamber_id'], 'CurrentChamber'), ['errorField' => 'current_chamber_id']);

        // Rules to ensure Bill Enums exists
        $rules->add($rules->existsIn(['current_bill_stage_id'], 'CurrentBillStage'), ['errorField' => 'current_bill_stage_id']);
        $rules->add($rules->existsIn(['current_bill_status_id'], 'CurrentBillStatus'), ['errorField' => 'current_bill_status_id']);
        $rules->add($rules->existsIn(['current_bill_cta_id'], 'CurrentBillCta'), ['errorField' => 'current_bill_cta_id']);
        $rules->add($rules->existsIn(['current_bill_progress_id'], 'CurrentBillProgress'), ['errorField' => 'current_bill_progress_id']);
        $rules->add($rules->existsIn(['current_bill_phase_icon_id'], 'CurrentBillPhaseIcon'), ['errorField' => 'current_bill_phase_icon_id']);
        $rules->add($rules->existsIn(['current_bill_rating_icon_id'], 'CurrentBillRatingIcon'), ['errorField' => 'current_bill_rating_icon_id']);

        // Rules to ensure Ratings exists
        $rules->add($rules->existsIn(['house_rating_id'], 'HouseRating'), ['errorField' => 'house_rating_id']);
        $rules->add($rules->existsIn(['senate_rating_id'], 'SenateRating'), ['errorField' => 'senate_rating_id']);

        $rules->add($rules->existsIn(['upload_id'], 'Uploads'), ['errorField' => 'upload_id']);

        return $rules;
    }
}
