<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LegislativeSessions Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\LegislatorsTable&\Cake\ORM\Association\HasMany $Legislators
 * @property \App\Model\Table\RatingsTable&\Cake\ORM\Association\HasMany $Ratings
 *
 * @method \App\Model\Entity\LegislativeSession newEmptyEntity()
 * @method \App\Model\Entity\LegislativeSession newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\LegislativeSession[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LegislativeSession get($primaryKey, $options = [])
 * @method \App\Model\Entity\LegislativeSession findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\LegislativeSession patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LegislativeSession[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\LegislativeSession|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegislativeSession saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegislativeSession[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegislativeSession[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegislativeSession[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegislativeSession[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LegislativeSessionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('legislative_sessions');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tenant');

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegiscanSessions', [
            'foreignKey' => 'legiscan_session_id',
        ]);
        $this->hasMany('Legislators', [
            'foreignKey' => 'legislative_session_id',
        ]);
        $this->hasMany('Ratings', [
            'foreignKey' => 'legislative_session_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('year')
            ->maxLength('year', 4)
            ->requirePresence('year', 'create')
            ->notEmptyString('year');

        $validator
            ->requirePresence('legislature_number', 'create')
            ->notEmptyString('legislature_number');

        $validator
            ->scalar('session_type')
            ->maxLength('session_type', 1)
            ->notEmptyString('session_type');

        $validator
            ->notEmptyString('special_session_number');

        $validator
            ->scalar('speaker_journal_name')
            ->maxLength('speaker_journal_name', 25)
            ->allowEmptyString('speaker_journal_name');

        $validator
            ->scalar('lt_gov_journal_name')
            ->maxLength('lt_gov_journal_name', 25)
            ->allowEmptyString('lt_gov_journal_name');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->requirePresence('slug', 'create')
            ->add('slug', 'validFormat', [
                //regex via https://stackoverflow.com/a/19256344/103297
                'rule' => ['custom', '/^[A-z0-9]+(?:-[A-z0-9]+)*$/'],
                'message' => 'Only lower case alphanumeric and dashes allowed.'
            ])
            ->add('slug', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmptyString('legiscan_session_id');

        $validator
            ->scalar('state_pk')
            ->maxLength('state_pk', 45)
            ->allowEmptyString('state_pk');

        $validator
            ->allowEmptyString('os_pk');

        $validator
            ->boolean('is_enabled')
            ->notEmptyString('is_enabled');

        $validator
            ->boolean('show_scores')
            ->notEmptyString('show_scores');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);

        return $rules;
    }

    // set default sorting if one isn't provided
    public function beforeFind($event, Query $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !(is_countable($order) ? count($order) : 0)) {
            $query->order([$this->getAlias() . '.year' => 'DESC']);
        }
    }
}
