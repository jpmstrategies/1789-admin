<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RatingCriterias Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\CriteriasTable&\Cake\ORM\Association\BelongsTo $Criterias
 * @property \App\Model\Table\CriteriaDetailsTable&\Cake\ORM\Association\BelongsTo $CriteriaDetails
 * @property \App\Model\Table\ActionsTable&\Cake\ORM\Association\BelongsTo $Actions
 *
 * @method \App\Model\Entity\RatingCriteria newEmptyEntity()
 * @method \App\Model\Entity\RatingCriteria newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\RatingCriteria[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RatingCriteria get($primaryKey, $options = [])
 * @method \App\Model\Entity\RatingCriteria findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\RatingCriteria patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RatingCriteria[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\RatingCriteria|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RatingCriteria saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RatingCriteria[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RatingCriteria[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\RatingCriteria[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RatingCriteria[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class RatingCriteriasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('rating_criterias');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Tenant');

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegiscanBills', [
            'foreignKey' => 'legiscan_bill_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Criterias', [
            'foreignKey' => 'criteria_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('CriteriaDetails', [
            'foreignKey' => 'criteria_detail_id',
        ]);
        $this->belongsTo('Actions', [
            'foreignKey' => 'action_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('legiscan_bill_id')
            ->notEmptyString('legiscan_bill_id');

        $validator
            ->uuid('criteria_id')
            ->notEmptyString('criteria_id');

        $validator
            ->uuid('criteria_detail_id')
            ->allowEmptyString('criteria_detail_id');

        $validator
            ->uuid('action_id')
            ->allowEmptyString('action_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['legiscan_bill_id'], 'LegiscanBills'), ['errorField' => 'legiscan_bill_id']);
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);
        $rules->add($rules->existsIn(['criteria_id'], 'Criterias'), ['errorField' => 'criteria_id']);
        $rules->add($rules->existsIn(['criteria_detail_id'], 'CriteriaDetails'), ['errorField' => 'criteria_detail_id']);
        $rules->add($rules->existsIn(['action_id'], 'Actions'), ['errorField' => 'action_id']);

        return $rules;
    }
}
