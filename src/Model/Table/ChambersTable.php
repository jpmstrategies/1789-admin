<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Chambers Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\LegislatorsTable&\Cake\ORM\Association\HasMany $Legislators
 * @property \App\Model\Table\RatingsTable&\Cake\ORM\Association\HasMany $Ratings
 * @property \App\Model\Table\TitlesTable&\Cake\ORM\Association\HasMany $Titles
 *
 * @method \App\Model\Entity\Chamber newEmptyEntity()
 * @method \App\Model\Entity\Chamber newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Chamber[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Chamber get($primaryKey, $options = [])
 * @method \App\Model\Entity\Chamber findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Chamber patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Chamber[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Chamber|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Chamber saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Chamber[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Chamber[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Chamber[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Chamber[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ChambersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('chambers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tenant');
        $this->addBehavior('ADmad/Sequence.Sequence', [
            'sequenceField' => 'sort_order', // Field to use to store integer sequence. Default "position".
            'scope' => ['tenant_id'], // Array of field names to use for grouping records. Default [].
            'startAt' => 1, // Initial value for sequence. Default 1.
        ]);

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegiscanChambers', [
            'foreignKey' => 'legiscan_chamber_id',
        ]);
        $this->hasMany('Legislators', [
            'foreignKey' => 'chamber_id',
        ]);
        $this->hasMany('Ratings', [
            'foreignKey' => 'chamber_id',
        ]);
        $this->hasMany('Titles', [
            'foreignKey' => 'chamber_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('type')
            ->maxLength('type', 1)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('title_short')
            ->maxLength('title_short', 100)
            ->allowEmptyString('title_short');

        $validator
            ->scalar('title_long')
            ->maxLength('title_long', 100)
            ->allowEmptyString('title_long');

        $validator
            ->scalar('presiding_legislator_short')
            ->maxLength('presiding_legislator_short', 100)
            ->allowEmptyString('presiding_legislator_short');

        $validator
            ->scalar('presiding_legislator_long')
            ->maxLength('presiding_legislator_long', 100)
            ->allowEmptyString('presiding_legislator_long');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->requirePresence('slug', 'create')
            ->add('slug', 'validFormat', [
                //regex via https://stackoverflow.com/a/19256344/103297
                'rule' => ['custom', '/^[A-z0-9]+(?:-[A-z0-9]+)*$/'],
                'message' => 'Only lower case alphanumeric and dashes allowed.'
            ]);

        $validator
            ->allowEmptyString('legiscan_chamber_id');

        $validator
            ->boolean('is_enabled')
            ->notEmptyString('is_enabled');

        $validator
            ->boolean('is_default')
            ->notEmptyString('is_default');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);

        return $rules;
    }

    // set default sorting if one isn't provided and sort_order exists
    public function beforeFind($event, Query $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !(is_countable($order) ? count($order) : 0)) {
            $query->order([$this->getAlias() . '.sort_order' => 'ASC']);
        }
    }
}
