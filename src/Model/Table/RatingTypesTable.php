<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RatingTypes Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\ActionsTable&\Cake\ORM\Association\HasMany $Actions
 * @property \App\Model\Table\RatingsTable&\Cake\ORM\Association\HasMany $Ratings
 *
 * @method \App\Model\Entity\RatingType newEmptyEntity()
 * @method \App\Model\Entity\RatingType newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\RatingType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RatingType get($primaryKey, $options = [])
 * @method \App\Model\Entity\RatingType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\RatingType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RatingType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\RatingType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RatingType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RatingType[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RatingType[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\RatingType[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RatingType[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RatingTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('rating_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tenant');
        $this->addBehavior('ADmad/Sequence.Sequence', [
            'sequenceField' => 'sort_order', // Field to use to store integer sequence. Default "position".
            'scope' => ['tenant_id'], // Array of field names to use for grouping records. Default [].
            'startAt' => 1, // Initial value for sequence. Default 1.
        ]);

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Actions', [
            'foreignKey' => 'rating_type_id',
        ]);
        $this->hasMany('Ratings', [
            'foreignKey' => 'rating_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 45)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('th_detail_name')
            ->maxLength('th_detail_name', 20)
            ->requirePresence('th_detail_name', 'create')
            ->notEmptyString('th_detail_name');

        $validator
            ->scalar('th_vote_name')
            ->maxLength('th_vote_name', 20)
            ->requirePresence('th_vote_name', 'create')
            ->notEmptyString('th_vote_name');

        $validator
            ->scalar('method')
            ->maxLength('method', 1)
            ->notEmptyString('method');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['name']));
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);

        return $rules;
    }

    // set default sorting if one isn't provided and sort_order exists
    public function beforeFind($event, Query $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !(is_countable($order) ? count($order) : 0)) {
            $query->order([$this->getAlias() . '.sort_order' => 'ASC']);
        }
    }
}
