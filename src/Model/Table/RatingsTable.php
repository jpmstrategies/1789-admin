<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ratings Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\LegislativeSessionsTable&\Cake\ORM\Association\BelongsTo $LegislativeSessions
 * @property \App\Model\Table\RatingTypesTable&\Cake\ORM\Association\BelongsTo $RatingTypes
 * @property \App\Model\Table\ChambersTable&\Cake\ORM\Association\BelongsTo $Chambers
 * @property \App\Model\Table\ActionsTable&\Cake\ORM\Association\BelongsTo $Actions
 * @property \App\Model\Table\PositionsTable&\Cake\ORM\Association\BelongsTo $Positions
 * @property \App\Model\Table\LegiscanBillsTable&\Cake\ORM\Association\BelongsTo $LegiscanBills
 * @property \App\Model\Table\LegiscanBillVotesTable&\Cake\ORM\Association\BelongsTo $LegiscanBillVotes
 * @property \App\Model\Table\RecordsTable&\Cake\ORM\Association\HasMany $Records
 * @property \App\Model\Table\FloorReportsTable&\Cake\ORM\Association\BelongsToMany $FloorReports
 * @property \App\Model\Table\PrioritiesTable&\Cake\ORM\Association\BelongsToMany $Priorities
 *
 * @method \App\Model\Entity\Rating newEmptyEntity()
 * @method \App\Model\Entity\Rating newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Rating[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Rating get($primaryKey, $options = [])
 * @method \App\Model\Entity\Rating findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Rating patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Rating[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Rating|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rating saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rating[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Rating[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Rating[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Rating[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RatingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('ratings');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tenant');
        $this->addBehavior('ADmad/Sequence.Sequence', [
            'sequenceField' => 'sort_order',
            // Field to use to store integer sequence. Default "position".
            'scope' => ['tenant_id', 'legislative_session_id', 'chamber_id', 'rating_type_id'],
            // Array of field names to use for grouping records. Default [].
            'startAt' => 1,
            // Initial value for sequence. Default 1.
        ]);

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegislativeSessions', [
            'foreignKey' => 'legislative_session_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('RatingTypes', [
            'foreignKey' => 'rating_type_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Chambers', [
            'foreignKey' => 'chamber_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Actions', [
            'foreignKey' => 'action_id',
        ]);
        $this->belongsTo('Positions', [
            'foreignKey' => 'position_id',
        ]);
        $this->belongsTo('LegiscanBills', [
            'foreignKey' => 'legiscan_bill_id',
        ]);
        $this->belongsTo('LegiscanBillVotes', [
            'foreignKey' => 'legiscan_bill_vote_id',
        ]);
        $this->hasMany('Records', [
            'foreignKey' => 'rating_id',
        ]);
        $this->belongsToMany('FloorReports', [
            'foreignKey' => 'rating_id',
            'targetForeignKey' => 'floor_report_id',
            'joinTable' => 'floor_reports_ratings',
        ]);
        $this->belongsToMany('Priorities', [
            'foreignKey' => 'rating_id',
            'targetForeignKey' => 'priority_id',
            'joinTable' => 'ratings_priorities',
        ]);

        // Bills associations
        $this->hasMany('HouseBills', [
            'className' => 'Bills',
            'foreignKey' => 'house_rating_id',
            'joinType' => 'LEFT',
        ]);
        $this->hasMany('SenateBills', [
            'className' => 'Bills',
            'foreignKey' => 'senate_rating_id',
            'joinType' => 'LEFT',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('legislative_session_id')
            ->notEmptyString('legislative_session_id');

        $validator
            ->uuid('rating_type_id')
            ->notEmptyString('rating_type_id');

        $validator
            ->uuid('chamber_id')
            ->notEmptyString('chamber_id');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->maxLength('description', 4_294_967_295)
            ->allowEmptyString('description');

        $validator
            ->scalar('fiscal_note')
            ->allowEmptyString('fiscal_note');

        $validator
            ->scalar('digest')
            ->allowEmptyString('digest');

        $validator
            ->scalar('vote_recommendation_notes')
            ->allowEmptyString('vote_recommendation_notes');

        $validator
            ->scalar('vote_references')
            ->allowEmptyString('vote_references');

        $validator
            ->uuid('action_id')
            ->allowEmptyString('action_id');

        $validator
            ->uuid('position_id')
            ->allowEmptyString('position_id');

        $validator
            ->allowEmptyString('record_vote');

        $validator
            ->decimal('rating_weight')
            ->allowEmptyString('rating_weight');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->requirePresence('slug', 'create')
            ->add('slug', 'validFormat', [
                //regex via https://stackoverflow.com/a/19256344/103297
                'rule' => ['custom', '/^[A-z0-9]+(?:-[A-z0-9]+)*$/'],
                'message' => 'Only lower case alphanumeric and dashes allowed.'
            ]);

        $validator
            ->nonNegativeInteger('legiscan_bill_id')
            ->allowEmptyString('legiscan_bill_id');

        $validator
            ->nonNegativeInteger('legiscan_bill_vote_id')
            ->allowEmptyString('legiscan_bill_vote_id');

        $validator
            ->allowEmptyString('json');

        $validator
            ->boolean('is_enabled')
            ->notEmptyString('is_enabled');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);
        $rules->add($rules->existsIn(['legislative_session_id'], 'LegislativeSessions'), ['errorField' => 'legislative_session_id']);
        $rules->add($rules->existsIn(['rating_type_id'], 'RatingTypes'), ['errorField' => 'rating_type_id']);
        $rules->add($rules->existsIn(['chamber_id'], 'Chambers'), ['errorField' => 'chamber_id']);
        $rules->add($rules->existsIn(['action_id'], 'Actions'), ['errorField' => 'action_id']);
        $rules->add($rules->existsIn(['position_id'], 'Positions'), ['errorField' => 'position_id']);
        $rules->add($rules->existsIn(['legiscan_bill_id'], 'LegiscanBills'), ['errorField' => 'legiscan_bill_id']);
        $rules->add($rules->existsIn(['legiscan_bill_vote_id'], 'LegiscanBillVotes'), ['errorField' => 'legiscan_bill_vote_id']);

        // leave off tenant_id because validation doesn't get trigger and this combination should be unique across tenants
        $rules->add($rules->isUnique(['legislative_session_id', 'chamber_id', 'name']), [
            'errorField' => 'name',
            'message' => 'This rating name is already in use for this legislative session and chamber.',
        ]);

        return $rules;
    }

    // set default sorting if one isn't provided and sort_order exists
    public function beforeFind($event, Query $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !(is_countable($order) ? count($order) : 0)) {
            $query->order([$this->getAlias() . '.sort_order' => 'ASC']);
        }
    }
}
