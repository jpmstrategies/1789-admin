<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ConfigurationTypes Model
 *
 * @property \App\Model\Table\ConfigurationOptionsTable&\Cake\ORM\Association\HasMany $ConfigurationOptions
 * @property \App\Model\Table\ConfigurationsTable&\Cake\ORM\Association\HasMany $Configurations
 *
 * @method \App\Model\Entity\ConfigurationType newEmptyEntity()
 * @method \App\Model\Entity\ConfigurationType newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ConfigurationType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ConfigurationType get($primaryKey, $options = [])
 * @method \App\Model\Entity\ConfigurationType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ConfigurationType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ConfigurationType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ConfigurationType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConfigurationType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConfigurationType[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ConfigurationType[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ConfigurationType[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ConfigurationType[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ConfigurationTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('configuration_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('ADmad/Sequence.Sequence', [
            'sequenceField' => 'sort_order', // Field to use to store integer sequence. Default "position".
            'scope' => ['name'], // Array of field names to use for grouping records. Default [].
            'startAt' => 1, // Initial value for sequence. Default 1.
        ]);

        $this->hasMany('ConfigurationOptions', [
            'foreignKey' => 'configuration_type_id',
        ]);
        $this->hasMany('Configurations', [
            'foreignKey' => 'configuration_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('scope')
            ->maxLength('scope', 255)
            ->allowEmptyString('scope');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        $validator
            ->scalar('data_type')
            ->maxLength('data_type', 10)
            ->requirePresence('data_type', 'create')
            ->notEmptyString('data_type');

        $validator
            ->scalar('default_value')
            ->maxLength('default_value', 4_294_967_295)
            ->allowEmptyString('default_value');

        $validator
            ->scalar('tab')
            ->maxLength('tab', 255)
            ->requirePresence('tab', 'create')
            ->notEmptyString('tab');

        $validator
            ->boolean('is_editable')
            ->notEmptyString('is_editable');

        $validator
            ->boolean('is_enabled')
            ->notEmptyString('is_enabled');

        $validator
            ->boolean('is_public')
            ->notEmptyString('is_public');

        $validator
            ->boolean('is_required')
            ->notEmptyString('is_required');

        $validator
            ->scalar('required_message')
            ->maxLength('required_message', 255)
            ->allowEmptyString('required_message');

        $validator
            ->scalar('required_regex')
            ->maxLength('required_regex', 255)
            ->allowEmptyString('required_regex');

        $validator
            ->scalar('field_mask')
            ->maxLength('field_mask', 255)
            ->allowEmptyString('field_mask');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['name']), ['errorField' => 'name']);

        return $rules;
    }

    // set default sorting if one isn't provided and sort_order exists
    public function beforeFind($event, Query $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !(is_countable($order) ? count($order) : 0)) {
            $query->order([$this->getAlias() . '.sort_order' => 'ASC']);
        }
    }
}
