<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LegiscanSessions Model
 *
 * @property \App\Model\Table\LegiscanBillSubjectsTable&\Cake\ORM\Association\HasMany $LegiscanBillSubjects
 * @property \App\Model\Table\LegiscanBillVoteDetailsTable&\Cake\ORM\Association\HasMany $LegiscanBillVoteDetails
 * @property \App\Model\Table\LegiscanBillVotesTable&\Cake\ORM\Association\HasMany $LegiscanBillVotes
 * @property \App\Model\Table\LegiscanBillsTable&\Cake\ORM\Association\HasMany $LegiscanBills
 * @property \App\Model\Table\LegislativeSessionsTable&\Cake\ORM\Association\HasMany $LegislativeSessions
 *
 * @method \App\Model\Entity\LegiscanSession newEmptyEntity()
 * @method \App\Model\Entity\LegiscanSession newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanSession[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanSession get($primaryKey, $options = [])
 * @method \App\Model\Entity\LegiscanSession findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\LegiscanSession patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanSession[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanSession|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanSession saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanSession[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanSession[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanSession[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanSession[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class LegiscanSessionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('legiscan_sessions');
        $this->setDisplayField('session_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('TenantState');

        $this->hasMany('LegiscanBillSubjects', [
            'foreignKey' => 'legiscan_session_id',
        ]);
        $this->hasMany('LegiscanBillVoteDetails', [
            'foreignKey' => 'legiscan_session_id',
        ]);
        $this->hasMany('LegiscanBillVotes', [
            'foreignKey' => 'legiscan_session_id',
        ]);
        $this->hasMany('LegiscanBills', [
            'foreignKey' => 'legiscan_session_id',
        ]);
        $this->hasMany('LegislativeSessions', [
            'foreignKey' => 'legiscan_session_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('state')
            ->maxLength('state', 2)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->requirePresence('id', 'create')
            ->notEmptyString('id');

        $validator
            ->requirePresence('year_start', 'create')
            ->notEmptyString('year_start');

        $validator
            ->requirePresence('year_end', 'create')
            ->notEmptyString('year_end');

        $validator
            ->notEmptyFile('prefile');

        $validator
            ->notEmptyString('sine_die');

        $validator
            ->notEmptyString('prior');

        $validator
            ->requirePresence('special', 'create')
            ->notEmptyString('special');

        $validator
            ->scalar('session_name')
            ->maxLength('session_name', 64)
            ->requirePresence('session_name', 'create')
            ->notEmptyString('session_name');

        $validator
            ->scalar('session_title')
            ->maxLength('session_title', 64)
            ->requirePresence('session_title', 'create')
            ->notEmptyString('session_title');

        $validator
            ->scalar('session_tag')
            ->maxLength('session_tag', 32)
            ->requirePresence('session_tag', 'create')
            ->notEmptyString('session_tag');

        $validator
            ->date('import_date')
            ->allowEmptyDate('import_date');

        $validator
            ->scalar('import_hash')
            ->maxLength('import_hash', 32)
            ->allowEmptyString('import_hash');

        return $validator;
    }
}
