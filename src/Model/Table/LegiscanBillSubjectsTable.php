<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LegiscanBillSubjects Model
 *
 * @property \App\Model\Table\LegiscanSubjectsTable&\Cake\ORM\Association\BelongsTo $LegiscanSubjects
 * @property \App\Model\Table\LegiscanBillsTable&\Cake\ORM\Association\BelongsTo $LegiscanBills
 * @property \App\Model\Table\LegiscanSessionsTable&\Cake\ORM\Association\BelongsTo $LegiscanSessions
 * @property \App\Model\Table\LegiscanChambersTable&\Cake\ORM\Association\BelongsTo $LegiscanChambers
 *
 * @method \App\Model\Entity\LegiscanBillSubject newEmptyEntity()
 * @method \App\Model\Entity\LegiscanBillSubject newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBillSubject[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBillSubject get($primaryKey, $options = [])
 * @method \App\Model\Entity\LegiscanBillSubject findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\LegiscanBillSubject patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBillSubject[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\LegiscanBillSubject|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanBillSubject saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LegiscanBillSubject[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanBillSubject[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanBillSubject[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\LegiscanBillSubject[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class LegiscanBillSubjectsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('legiscan_bill_subjects');
        $this->setPrimaryKey('id');
        $this->setDisplayField('subject_name');

        $this->addBehavior('TenantState');

        $this->belongsTo('LegiscanSubjects', [
            'foreignKey' => 'legiscan_subject_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegiscanBills', [
            'foreignKey' => 'legiscan_bill_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegiscanSessions', [
            'foreignKey' => 'legiscan_session_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('LegiscanChambers', [
            'className' => 'LegiscanChambers',
            'foreignKey' => 'legiscan_chamber_id',
            'joinType' => 'INNER',
            'propertyName' => 'origination_chamber'
        ]);
        $this->belongsTo('LegiscanCurrentChambers', [
            'className' => 'LegiscanChambers',
            'foreignKey' => 'legiscan_current_chamber_id',
            'joinType' => 'INNER',
            'propertyName' => 'current_chamber'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('state')
            ->maxLength('state', 2)
            ->requirePresence('state', 'create')
            ->notEmptyString('state');

        $validator
            ->scalar('id')
            ->maxLength('id', 17)
            ->notEmptyString('id');

        $validator
            ->nonNegativeInteger('legiscan_subject_id')
            ->requirePresence('legiscan_subject_id', 'create')
            ->notEmptyString('legiscan_subject_id');

        $validator
            ->nonNegativeInteger('legiscan_bill_id')
            ->requirePresence('legiscan_bill_id', 'create')
            ->notEmptyString('legiscan_bill_id');

        $validator
            ->scalar('bill_number')
            ->maxLength('bill_number', 10)
            ->requirePresence('bill_number', 'create')
            ->notEmptyString('bill_number');

        $validator
            ->scalar('subject_name')
            ->maxLength('subject_name', 128)
            ->requirePresence('subject_name', 'create')
            ->notEmptyString('subject_name');

        $validator
            ->requirePresence('legiscan_state_id', 'create')
            ->notEmptyString('legiscan_state_id');

        $validator
            ->scalar('state_name')
            ->maxLength('state_name', 64)
            ->requirePresence('state_name', 'create')
            ->notEmptyString('state_name');

        $validator
            ->requirePresence('legiscan_session_id', 'create')
            ->notEmptyString('legiscan_session_id');

        $validator
            ->requirePresence('legiscan_chamber_id', 'create')
            ->notEmptyString('legiscan_chamber_id');

        $validator
            ->requirePresence('legiscan_current_chamber_id', 'create')
            ->notEmptyString('legiscan_current_chamber_id');

        $validator
            ->requirePresence('legsican_bill_type_id', 'create')
            ->notEmptyString('legsican_bill_type_id');

        $validator
            ->requirePresence('legsican_status_id', 'create')
            ->notEmptyString('legsican_status_id');

        $validator
            ->requirePresence('legsican_pending_committee_id', 'create')
            ->notEmptyString('legsican_pending_committee_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['legiscan_subject_id'], 'LegiscanSubjects'), ['errorField' => 'legiscan_subject_id']);
        $rules->add($rules->existsIn(['legiscan_bill_id'], 'LegiscanBills'), ['errorField' => 'legiscan_bill_id']);
        $rules->add($rules->existsIn(['legiscan_session_id'], 'LegiscanSessions'), ['errorField' => 'legiscan_session_id']);
        $rules->add($rules->existsIn(['legiscan_chamber_id'], 'LegiscanChambers'), ['errorField' => 'legiscan_chamber_id']);

        return $rules;
    }
}
