<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BillEnums Model
 *
 * @property \App\Model\Table\TenantsTable&\Cake\ORM\Association\BelongsTo $Tenants
 * @property \App\Model\Table\BillEnumsTable&\Cake\ORM\Association\BelongsTo $ParentBillEnums
 * @property \App\Model\Table\BillEnumsTable&\Cake\ORM\Association\HasMany $ChildBillEnums
 *
 * @method \App\Model\Entity\BillEnum newEmptyEntity()
 * @method \App\Model\Entity\BillEnum newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\BillEnum[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BillEnum get($primaryKey, $options = [])
 * @method \App\Model\Entity\BillEnum findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\BillEnum patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BillEnum[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\BillEnum|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BillEnum saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BillEnum[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\BillEnum[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\BillEnum[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\BillEnum[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BillEnumsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('bill_enums');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tenant');
        $this->addBehavior('ADmad/Sequence.Sequence', [
            'sequenceField' => 'sort_order', // Field to use to store integer sequence. Default "position".
            'scope' => ['tenant_id', 'type', 'parent_id'], // Array of field names to use for grouping records. Default [].
            'startAt' => 1, // Initial value for sequence. Default 1.
        ]);

        $this->belongsTo('Tenants', [
            'foreignKey' => 'tenant_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ParentBillEnums', [
            'className' => 'BillEnums',
            'foreignKey' => 'parent_id',
        ]);
        $this->hasMany('ChildBillEnums', [
            'className' => 'BillEnums',
            'foreignKey' => 'parent_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('type')
            ->requirePresence('type', 'create')
            ->notEmptyString('type')
            ->inList('type', ['stage', 'status', 'cta', 'progress', 'phase-icon', 'rating-icon'], 'Invalid type provided.');

        $validator
            ->uuid('parent_id')
            ->allowEmptyString('parent_id');

        $validator
            ->scalar('name')
            ->maxLength('name', 45)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        $validator
            ->scalar('css_class')
            ->maxLength('css_class', 255)
            ->allowEmptyString('css_class');

        $validator
            ->scalar('branch')
            ->allowEmptyString('branch')
            ->inList('branch', ['exe', 'leg'], 'Invalid branch type provided.');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['tenant_id'], 'Tenants'), ['errorField' => 'tenant_id']);
        $rules->add($rules->existsIn(['parent_id'], 'ParentBillEnums'), ['errorField' => 'parent_id']);

        return $rules;
    }

    // set default sorting if one isn't provided and sort_order exists
    public function beforeFind($event, Query $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !(is_countable($order) ? count($order) : 0)) {
            $query->order([$this->getAlias() . '.sort_order' => 'ASC']);
        }
    }
}
