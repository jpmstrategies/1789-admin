<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Score Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $legislator_id
 * @property string $organization_id
 * @property string|null $display_text
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Legislator $legislator
 * @property \App\Model\Entity\Organization $organization
 */
class Score extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'legislator_id' => 1,
        'organization_id' => 1,
        'display_text' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'legislator' => 1,
        'organization' => 1,
    ];
}
