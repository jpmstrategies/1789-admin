<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Configuration Entity
 *
 * @property string $id
 * @property string $configuration_type_id
 * @property string $tenant_id
 * @property string|null $value
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\ConfigurationType $configuration_type
 * @property \App\Model\Entity\Tenant $tenant
 */
class Configuration extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'configuration_type_id' => 1,
        'value' => 1,
        'modified' => 1,
        'created' => 1,
        'configuration_type' => 1,
        'tenant' => 1,
    ];
}
