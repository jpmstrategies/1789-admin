<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BillEnum Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $type
 * @property string|null $parent_id
 * @property string $name
 * @property string|null $description
 * @property string|null $css_class
 * @property string|null $branch
 * @property int|null $sort_order
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\BillEnum $parent_bill_enum
 * @property \App\Model\Entity\BillEnum[] $child_bill_enums
 */
class BillEnum extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'type' => 1,
        'parent_id' => 1,
        'name' => 1,
        'description' => 1,
        'css_class' => 1,
        'branch' => 1,
        'sort_order' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'parent_bill_enum' => 1,
        'child_bill_enums' => 1,
    ];
}
