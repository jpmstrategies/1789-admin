<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Action Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $rating_type_id
 * @property string|null $action_type_id
 * @property string $name
 * @property string $description
 * @property string|null $denom_value
 * @property string|null $multiplier_support
 * @property string|null $multiplier_oppose
 * @property int|null $legiscan_action_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\RatingType $rating_type
 * @property \App\Model\Entity\ActionType $action_type
 * @property \App\Model\Entity\LegiscanAction $legiscan_action
 * @property \App\Model\Entity\RatingCriteria[] $rating_criterias
 * @property \App\Model\Entity\Rating[] $ratings
 * @property \App\Model\Entity\Record[] $records
 */
class Action extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'rating_type_id' => 1,
        'action_type_id' => 1,
        'name' => 1,
        'description' => 1,
        'denom_value' => 1,
        'multiplier_support' => 1,
        'multiplier_oppose' => 1,
        'legiscan_action_id' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'rating_type' => 1,
        'action_type' => 1,
        'legiscan_action' => 1,
        'rating_criterias' => 1,
        'ratings' => 1,
        'records' => 1,
    ];
}
