<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CriteriaDetail Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $criteria_id
 * @property string $name
 * @property int|null $sort_order
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Criteria $criteria
 * @property \App\Model\Entity\RatingCriteria[] $rating_criterias
 */
class CriteriaDetail extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'criteria_id' => 1,
        'name' => 1,
        'sort_order' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'criteria' => 1,
        'rating_criterias' => 1,
    ];
}
