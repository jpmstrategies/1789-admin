<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LegiscanBillSubject Entity
 *
 * @property string $state
 * @property string $id
 * @property int $legiscan_subject_id
 * @property int $legiscan_bill_id
 * @property string $bill_number
 * @property string $subject_name
 * @property int $legiscan_state_id
 * @property string $state_name
 * @property int $legiscan_session_id
 * @property int $legiscan_chamber_id
 * @property int $legiscan_current_chamber_id
 * @property int $legsican_bill_type_id
 * @property int $legsican_status_id
 * @property int $legsican_pending_committee_id
 *
 * @property \App\Model\Entity\LegiscanSubject $legiscan_subject
 * @property \App\Model\Entity\LegiscanBill $legiscan_bill
 * @property \App\Model\Entity\LegiscanSession $legiscan_session
 * @property \App\Model\Entity\LegiscanChamber $legiscan_chamber
 */
class LegiscanBillSubject extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'state' => 1,
        'id' => 1,
        'legiscan_subject_id' => 1,
        'legiscan_bill_id' => 1,
        'bill_number' => 1,
        'subject_name' => 1,
        'legiscan_state_id' => 1,
        'state_name' => 1,
        'legiscan_session_id' => 1,
        'legiscan_chamber_id' => 1,
        'legiscan_current_chamber_id' => 1,
        'legsican_bill_type_id' => 1,
        'legsican_status_id' => 1,
        'legsican_pending_committee_id' => 1,
        'legiscan_subject' => 1,
        'legiscan_bill' => 1,
        'legiscan_session' => 1,
        'legiscan_chamber' => 1,
    ];
}
