<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Person Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $full_name
 * @property string|null $nickname
 * @property string|null $salutation
 * @property string|null $first_name
 * @property string|null $middle_name
 * @property string|null $initials
 * @property string|null $last_name
 * @property string|null $suffix
 * @property string|null $bio
 * @property string|null $committee_list
 * @property string|null $image_url
 * @property string|null $ballotpedia_url
 * @property string|null $first_elected
 * @property string|null $district_city
 * @property string|null $grade_id
 * @property string|null $grade_number
 * @property string $slug
 * @property int|null $legiscan_people_id
 * @property string|null $os_pk
 * @property string|null $state_pk
 * @property string|null $email
 * @property string $phone
 * @property string|null $facebook
 * @property string|null $twitter
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property bool $is_enabled
 * @property bool $is_retired
 * @property string|null $ttx_candidate_slug
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Grade $grade
 * @property \App\Model\Entity\Address[] $addresses
 * @property \App\Model\Entity\Legislator[] $legislators
 * @property \App\Model\Entity\Tag[] $tags
 */
class Person extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'full_name' => 1,
        'nickname' => 1,
        'salutation' => 1,
        'first_name' => 1,
        'middle_name' => 1,
        'initials' => 1,
        'last_name' => 1,
        'suffix' => 1,
        'bio' => 1,
        'committee_list' => 1,
        'image_url' => 1,
        'ballotpedia_url' => 1,
        'first_elected' => 1,
        'district_city' => 1,
        'grade_id' => 1,
        'grade_number' => 1,
        'slug' => 1,
        'legiscan_people_id' => 1,
        'os_pk' => 1,
        'state_pk' => 1,
        'email' => 1,
        'phone' => 1,
        'facebook' => 1,
        'twitter' => 1,
        'created' => 1,
        'modified' => 1,
        'is_enabled' => 1,
        'is_retired' => 1,
        'ttx_candidate_slug' => 1,
        'tenant' => 1,
        'grade' => 1,
        'addresses' => 1,
        'legislators' => 1,
        'tags' => 1,
    ];
}
