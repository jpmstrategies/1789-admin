<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LegislativeSession Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $name
 * @property string $year
 * @property int $legislature_number
 * @property string $session_type
 * @property int $special_session_number
 * @property string|null $speaker_journal_name
 * @property string|null $lt_gov_journal_name
 * @property string $slug
 * @property int|null $legiscan_session_id
 * @property string|null $state_pk
 * @property int|null $os_pk
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property bool $is_enabled
 * @property bool $show_scores
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Legislator[] $legislators
 * @property \App\Model\Entity\Rating[] $ratings
 */
class LegislativeSession extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'name' => 1,
        'year' => 1,
        'legislature_number' => 1,
        'session_type' => 1,
        'special_session_number' => 1,
        'speaker_journal_name' => 1,
        'lt_gov_journal_name' => 1,
        'slug' => 1,
        'legiscan_session_id' => 1,
        'state_pk' => 1,
        'os_pk' => 1,
        'created' => 1,
        'modified' => 1,
        'is_enabled' => 1,
        'show_scores' => 1,
        'tenant' => 1,
        'legislators' => 1,
        'ratings' => 1,
    ];
}
