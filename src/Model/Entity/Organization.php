<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Organization Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string|null $org_tenant_id
 * @property string|null $sync_legislative_session_id
 * @property string $name
 * @property string|null $description
 * @property string|null $website
 * @property string|null $image_url
 * @property int|null $sort_order
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\LegislativeSession $legislative_session
 * @property \App\Model\Entity\Score[] $scores
 */
class Organization extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'org_tenant_id' => 1,
        'sync_legislative_session_id' => 1,
        'name' => 1,
        'description' => 1,
        'website' => 1,
        'image_url' => 1,
        'sort_order' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'legislative_session' => 1,
        'scores' => 1,
    ];
}
