<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TenantsBill Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property int $legiscan_bill_id
 * @property string|null $priority_id
 * @property string|null $notes
 * @property string|null $status
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\LegiscanBill $legiscan_bill
 * @property \App\Model\Entity\Priority $priority
 */
class TenantsBill extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'legiscan_bill_id' => 1,
        'priority_id' => 1,
        'notes' => 1,
        'status' => 1,
        'tenant' => 1,
        'legiscan_bill' => 1,
        'priority' => 1,
    ];
}
