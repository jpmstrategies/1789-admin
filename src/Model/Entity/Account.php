<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Account Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $user_id
 * @property string|null $invited_by_user_id
 * @property string $role
 * @property string $status
 * @property string|null $security_code
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\User $user
 */
class Account extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'user_id' => 1,
        'invited_by_user_id' => 1,
        'role' => 1,
        'status' => 1,
        'security_code' => 1,
        'modified' => 1,
        'created' => 1,
        'tenant' => 1,
        'user' => 1,
    ];
}
