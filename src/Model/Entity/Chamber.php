<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Chamber Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $type
 * @property string $name
 * @property string|null $title_short
 * @property string|null $title_long
 * @property string|null $presiding_legislator_short
 * @property string|null $presiding_legislator_long
 * @property string $slug
 * @property int|null $legiscan_chamber_id
 * @property bool $is_enabled
 * @property bool $is_default
 * @property int|null $sort_order
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Legislator[] $legislators
 * @property \App\Model\Entity\Rating[] $ratings
 * @property \App\Model\Entity\Title[] $titles
 */
class Chamber extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'type' => 1,
        'name' => 1,
        'title_short' => 1,
        'title_long' => 1,
        'presiding_legislator_short' => 1,
        'presiding_legislator_long' => 1,
        'slug' => 1,
        'legiscan_chamber_id' => 1,
        'is_enabled' => 1,
        'is_default' => 1,
        'sort_order' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'legislators' => 1,
        'ratings' => 1,
        'titles' => 1,
    ];
}
