<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RatingsPriority Entity
 *
 * @property string $id
 * @property string $rating_id
 * @property string $priority_id
 *
 * @property \App\Model\Entity\Rating $rating
 * @property \App\Model\Entity\Priority $priority
 */
class RatingsPriority extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'rating_id' => 1,
        'priority_id' => 1,
        'rating' => 1,
        'priority' => 1,
    ];
}
