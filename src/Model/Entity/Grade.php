<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Grade Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $name
 * @property string $css_color
 * @property int $sort_order
 * @property int $lower_range
 * @property int $upper_range
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Legislator[] $legislators
 * @property \App\Model\Entity\Person[] $people
 */
class Grade extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'name' => 1,
        'css_color' => 1,
        'sort_order' => 1,
        'lower_range' => 1,
        'upper_range' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'legislators' => 1,
        'people' => 1,
    ];
}
