<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RatingCriteria Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property int $legiscan_bill_id
 * @property string $criteria_id
 * @property string|null $criteria_detail_id
 * @property string|null $action_id
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Rating $rating
 * @property \App\Model\Entity\Criteria $criteria
 * @property \App\Model\Entity\CriteriaDetail $criteria_detail
 * @property \App\Model\Entity\Action $action
 */
class RatingCriteria extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'legiscan_bill_id' => 1,
        'criteria_id' => 1,
        'criteria_detail_id' => 1,
        'action_id' => 1,
        'tenant' => 1,
        'rating' => 1,
        'criteria' => 1,
        'criteria_detail' => 1,
        'action' => 1,
    ];
}
