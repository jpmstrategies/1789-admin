<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConfigurationOption Entity
 *
 * @property string $id
 * @property string $configuration_type_id
 * @property string $name
 * @property string|null $value
 * @property int $sort_order
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\ConfigurationType $configuration_type
 */
class ConfigurationOption extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'configuration_type_id' => true,
        'name' => true,
        'value' => true,
        'sort_order' => true,
        'created' => true,
        'modified' => true,
        'configuration_type' => true,
    ];
}
