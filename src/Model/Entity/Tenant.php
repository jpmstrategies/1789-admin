<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tenant Entity
 *
 * @property string $id
 * @property string $name
 * @property string $domain_name
 * @property string $id_public
 * @property string|null $state
 * @property string $type
 * @property string $data_source
 * @property string $layout
 * @property \Cake\I18n\FrozenTime|null $sync_pending
 * @property bool $is_enabled
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Account[] $accounts
 * @property \App\Model\Entity\ActionType[] $action_types
 * @property \App\Model\Entity\Action[] $actions
 * @property \App\Model\Entity\AddressType[] $address_types
 * @property \App\Model\Entity\Address[] $addresses
 * @property \App\Model\Entity\Chamber[] $chambers
 * @property \App\Model\Entity\Configuration[] $configurations
 * @property \App\Model\Entity\Grade[] $grades
 * @property \App\Model\Entity\LegislativeSession[] $legislative_sessions
 * @property \App\Model\Entity\Legislator[] $legislators
 * @property \App\Model\Entity\Organization[] $organizations
 * @property \App\Model\Entity\Party[] $parties
 * @property \App\Model\Entity\Person[] $people
 * @property \App\Model\Entity\Position[] $positions
 * @property \App\Model\Entity\RatingField[] $rating_fields
 * @property \App\Model\Entity\RatingType[] $rating_types
 * @property \App\Model\Entity\Rating[] $ratings
 * @property \App\Model\Entity\Record[] $records
 * @property \App\Model\Entity\Score[] $scores
 */
class Tenant extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => 1,
        'domain_name' => 1,
        'id_public' => 1,
        'state' => 1,
        'type' => 1,
        'data_source' => 1,
        'layout' => 1,
        'sync_pending' => 1,
        'is_enabled' => 1,
        'created' => 1,
        'modified' => 1,
        'accounts' => 1,
        'action_types' => 1,
        'actions' => 1,
        'address_types' => 1,
        'addresses' => 1,
        'chambers' => 1,
        'configurations' => 1,
        'grades' => 1,
        'legislative_sessions' => 1,
        'legislators' => 1,
        'organizations' => 1,
        'parties' => 1,
        'people' => 1,
        'positions' => 1,
        'rating_fields' => 1,
        'rating_types' => 1,
        'ratings' => 1,
        'records' => 1,
        'scores' => 1,
    ];
}
