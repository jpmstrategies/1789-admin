<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LegiscanSession Entity
 *
 * @property string $state
 * @property int $id
 * @property int $year_start
 * @property int $year_end
 * @property int $prefile
 * @property int $sine_die
 * @property int $prior
 * @property int $special
 * @property string $session_name
 * @property string $session_title
 * @property string $session_tag
 * @property \Cake\I18n\FrozenDate|null $import_date
 * @property string|null $import_hash
 *
 * @property \App\Model\Entity\LegiscanBillSubject[] $legiscan_bill_subjects
 * @property \App\Model\Entity\LegiscanBillVoteDetail[] $legiscan_bill_vote_details
 * @property \App\Model\Entity\LegiscanBillVote[] $legiscan_bill_votes
 * @property \App\Model\Entity\LegiscanBill[] $legiscan_bills
 * @property \App\Model\Entity\LegislativeSession[] $legislative_sessions
 */
class LegiscanSession extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'state' => 1,
        'id' => 1,
        'year_start' => 1,
        'year_end' => 1,
        'prefile' => 1,
        'sine_die' => 1,
        'prior' => 1,
        'special' => 1,
        'session_name' => 1,
        'session_title' => 1,
        'session_tag' => 1,
        'import_date' => 1,
        'import_hash' => 1,
        'legiscan_bill_subjects' => 1,
        'legiscan_bill_vote_details' => 1,
        'legiscan_bill_votes' => 1,
        'legiscan_bills' => 1,
        'legislative_sessions' => 1,
    ];
}
