<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LegiscanBillVoteDetail Entity
 *
 * @property string $id
 * @property string $state
 * @property int $legiscan_bill_id
 * @property int $legiscan_bill_vote_id
 * @property int $legiscan_bill_vote_chamber_id
 * @property int $legiscan_people_id
 * @property string $bill_number
 * @property int $legiscan_vote_id
 * @property string $vote_desc
 * @property int $legiscan_party_id
 * @property string $party_abbr
 * @property int $legiscan_role_id
 * @property string $role_abbr
 * @property string $role_name
 * @property string $name
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $suffix
 * @property string $nickname
 * @property string|null $ballotpedia
 * @property int $followthemoney_eid
 * @property int $votesmart_id
 * @property string|null $opensecrets_id
 * @property int $knowwho_pid
 * @property string $person_hash
 * @property string|null $person_district
 * @property int $legiscan_people_chamber_id
 * @property int $legiscan_state_id
 * @property string $state_name
 * @property int $legiscan_session_id
 * @property int $legiscan_chamber_id
 * @property int $legiscan_current_chamber_id
 * @property int $legiscan_bill_type_id
 * @property int $legiscan_status_id
 * @property int $legiscan_pending_committee_id
 *
 * @property \App\Model\Entity\LegiscanBill $legiscan_bill
 * @property \App\Model\Entity\LegiscanBillVote $legiscan_bill_vote
 * @property \App\Model\Entity\LegiscanParty $legiscan_party
 * @property \App\Model\Entity\LegiscanSession $legiscan_session
 * @property \App\Model\Entity\LegiscanChamber $legiscan_chamber
 */
class LegiscanBillVoteDetail extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => 1,
        'state' => 1,
        'legiscan_bill_id' => 1,
        'legiscan_bill_vote_id' => 1,
        'legiscan_bill_vote_chamber_id' => 1,
        'legiscan_people_id' => 1,
        'bill_number' => 1,
        'legiscan_vote_id' => 1,
        'vote_desc' => 1,
        'legiscan_party_id' => 1,
        'party_abbr' => 1,
        'legiscan_role_id' => 1,
        'role_abbr' => 1,
        'role_name' => 1,
        'name' => 1,
        'first_name' => 1,
        'middle_name' => 1,
        'last_name' => 1,
        'suffix' => 1,
        'nickname' => 1,
        'ballotpedia' => 1,
        'followthemoney_eid' => 1,
        'votesmart_id' => 1,
        'opensecrets_id' => 1,
        'knowwho_pid' => 1,
        'person_hash' => 1,
        'person_district' => 1,
        'legiscan_people_chamber_id' => 1,
        'legiscan_state_id' => 1,
        'state_name' => 1,
        'legiscan_session_id' => 1,
        'legiscan_chamber_id' => 1,
        'legiscan_current_chamber_id' => 1,
        'legiscan_bill_type_id' => 1,
        'legiscan_status_id' => 1,
        'legiscan_pending_committee_id' => 1,
        'legiscan_bill' => 1,
        'legiscan_bill_vote' => 1,
        'legiscan_party' => 1,
        'legiscan_session' => 1,
        'legiscan_chamber' => 1,
    ];
}
