<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Legislator Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $legislative_session_id
 * @property string $person_id
 * @property string $chamber_id
 * @property string $party_id
 * @property int|null $district
 * @property string|null $journal_name
 * @property string|null $title_id
 * @property \Cake\I18n\FrozenDate|null $term_starts
 * @property \Cake\I18n\FrozenDate|null $term_ends
 * @property string|null $grade_id
 * @property string|null $grade_number
 * @property string|null $override_text
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property bool $is_enabled
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\LegislativeSession $legislative_session
 * @property \App\Model\Entity\Person $person
 * @property \App\Model\Entity\Chamber $chamber
 * @property \App\Model\Entity\Party $party
 * @property \App\Model\Entity\Title $title
 * @property \App\Model\Entity\Grade $grade
 * @property \App\Model\Entity\Record[] $records
 * @property \App\Model\Entity\Score[] $scores
 */
class Legislator extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'legislative_session_id' => 1,
        'person_id' => 1,
        'chamber_id' => 1,
        'party_id' => 1,
        'district' => 1,
        'journal_name' => 1,
        'title_id' => 1,
        'term_starts' => 1,
        'term_ends' => 1,
        'grade_id' => 1,
        'grade_number' => 1,
        'override_text' => 1,
        'created' => 1,
        'modified' => 1,
        'is_enabled' => 1,
        'tenant' => 1,
        'legislative_session' => 1,
        'person' => 1,
        'chamber' => 1,
        'party' => 1,
        'title' => 1,
        'grade' => 1,
        'records' => 1,
        'scores' => 1,
    ];
}
