<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Title Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $chamber_id
 * @property string $branch
 * @property string|null $office
 * @property string $title_long
 * @property string $title_short
 * @property int|null $sort_order
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Chamber $chamber
 * @property \App\Model\Entity\Legislator[] $legislators
 */
class Title extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'chamber_id' => 1,
        'branch' => 1,
        'office' => 1,
        'title_long' => 1,
        'title_short' => 1,
        'sort_order' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'chamber' => 1,
        'legislators' => 1,
    ];
}
