<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Party Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $name
 * @property string $abbreviation
 * @property string $css_color
 * @property int|null $legiscan_party_id
 * @property bool $is_enabled
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Legislator[] $legislators
 */
class Party extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'name' => 1,
        'abbreviation' => 1,
        'css_color' => 1,
        'legiscan_party_id' => 1,
        'is_enabled' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'legislators' => 1,
    ];
}
