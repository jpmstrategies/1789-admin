<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ActionType Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $name
 * @property string|null $icon_text
 * @property string|null $export_text
 * @property string $css_color
 * @property int $sort_order
 * @property string $evaluation_code
 * @property string $score_type
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Action[] $actions
 */
class ActionType extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'name' => 1,
        'icon_text' => 1,
        'export_text' => 1,
        'css_color' => 1,
        'sort_order' => 1,
        'evaluation_code' => 1,
        'score_type' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'actions' => 1,
    ];
}
