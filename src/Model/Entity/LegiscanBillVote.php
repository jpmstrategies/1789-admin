<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LegiscanBillVote Entity
 *
 * @property int $id
 * @property string $state
 * @property int $legiscan_bill_id
 * @property string $bill_number
 * @property \Cake\I18n\FrozenDate|null $roll_call_date
 * @property string $roll_call_desc
 * @property int $legiscan_bill_vote_chamber_id
 * @property string $roll_call_body_abbr
 * @property string|null $roll_call_body_short
 * @property string $roll_call_body_name
 * @property int $yea
 * @property int $nay
 * @property int $nv
 * @property int $absent
 * @property int $total
 * @property int $passed
 * @property string $legiscan_url
 * @property string $state_url
 * @property int $legiscan_state_id
 * @property string $state_name
 * @property int $legiscan_session_id
 * @property int $legiscan_chamber_id
 * @property int $legiscan_current_chamber_id
 * @property int $legiscan_bill_type_id
 * @property int $legiscan_status_id
 * @property int $legiscan_pending_committee_id
 * @property int $has_bill_vote_details
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $updated
 *
 * @property \App\Model\Entity\LegiscanBill $legiscan_bill
 * @property \App\Model\Entity\LegiscanSession $legiscan_session
 * @property \App\Model\Entity\LegiscanChamber $legiscan_chamber
 * @property \App\Model\Entity\LegiscanBillVoteDetail[] $legiscan_bill_vote_details
 * @property \App\Model\Entity\Rating[] $ratings
 */
class LegiscanBillVote extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => 1,
        'state' => 1,
        'legiscan_bill_id' => 1,
        'bill_number' => 1,
        'roll_call_date' => 1,
        'roll_call_desc' => 1,
        'legiscan_bill_vote_chamber_id' => 1,
        'roll_call_body_abbr' => 1,
        'roll_call_body_short' => 1,
        'roll_call_body_name' => 1,
        'yea' => 1,
        'nay' => 1,
        'nv' => 1,
        'absent' => 1,
        'total' => 1,
        'passed' => 1,
        'legiscan_url' => 1,
        'state_url' => 1,
        'legiscan_state_id' => 1,
        'state_name' => 1,
        'legiscan_session_id' => 1,
        'legiscan_chamber_id' => 1,
        'legiscan_current_chamber_id' => 1,
        'legiscan_bill_type_id' => 1,
        'legiscan_status_id' => 1,
        'legiscan_pending_committee_id' => 1,
        'has_bill_vote_details' => 1,
        'created' => 1,
        'updated' => 1,
        'legiscan_bill' => 1,
        'legiscan_session' => 1,
        'legiscan_chamber' => 1,
        'legiscan_bill_vote_details' => 1,
        'ratings' => 1,
    ];
}
