<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LegiscanParty Entity
 *
 * @property string $state
 * @property int $id
 * @property string $party_abbr
 * @property string $party_short
 * @property string $party_name
 *
 * @property \App\Model\Entity\LegiscanBillVoteDetail[] $legiscan_bill_vote_details
 * @property \App\Model\Entity\Party[] $parties
 */
class LegiscanParty extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'state' => 1,
        'id' => 1,
        'party_abbr' => 1,
        'party_short' => 1,
        'party_name' => 1,
        'legiscan_bill_vote_details' => 1,
        'parties' => 1,
    ];
}
