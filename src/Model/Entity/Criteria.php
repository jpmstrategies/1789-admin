<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Criteria Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $name
 * @property string $th_detail_name
 * @property string $th_vote_name
 * @property string $method
 * @property string|null $party_id
 * @property int|null $sort_order
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Party $party
 * @property \App\Model\Entity\CriteriaDetail[] $criteria_details
 * @property \App\Model\Entity\RatingCriteria[] $rating_criterias
 */
class Criteria extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'name' => 1,
        'th_detail_name' => 1,
        'th_vote_name' => 1,
        'method' => 1,
        'party_id' => 1,
        'sort_order' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'party' => 1,
        'criteria_details' => 1,
        'rating_criterias' => 1,
    ];
}
