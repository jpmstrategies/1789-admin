<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LegiscanChamber Entity
 *
 * @property string $state
 * @property int $id
 * @property string $body_abbr
 * @property string|null $body_short
 * @property string $body_name
 * @property int $role_id
 * @property string|null $role_abbr
 * @property string|null $role_name
 *
 * @property \App\Model\Entity\Chamber[] $chambers
 * @property \App\Model\Entity\LegiscanBillSubject[] $legiscan_bill_subjects
 * @property \App\Model\Entity\LegiscanBillVoteDetail[] $legiscan_bill_vote_details
 * @property \App\Model\Entity\LegiscanBillVote[] $legiscan_bill_votes
 * @property \App\Model\Entity\LegiscanBill[] $legiscan_bills
 */
class LegiscanChamber extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'state' => 1,
        'id' => 1,
        'body_abbr' => 1,
        'body_short' => 1,
        'body_name' => 1,
        'role_id' => 1,
        'role_abbr' => 1,
        'role_name' => 1,
        'chambers' => 1,
        'legiscan_bill_subjects' => 1,
        'legiscan_bill_vote_details' => 1,
        'legiscan_bill_votes' => 1,
        'legiscan_bills' => 1,
    ];
}
