<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Address Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $person_id
 * @property string $address_type_id
 * @property string|null $capitol_address
 * @property string|null $full_address
 * @property string|null $address_line_1
 * @property string|null $address_line_2
 * @property string|null $city
 * @property string|null $state
 * @property string|null $zip
 * @property string|null $phone
 * @property string|null $fax
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Person $person
 * @property \App\Model\Entity\AddressType $address_type
 */
class Address extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'person_id' => 1,
        'address_type_id' => 1,
        'capitol_address' => 1,
        'full_address' => 1,
        'address_line_1' => 1,
        'address_line_2' => 1,
        'city' => 1,
        'state' => 1,
        'zip' => 1,
        'phone' => 1,
        'fax' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'person' => 1,
        'address_type' => 1,
    ];
}
