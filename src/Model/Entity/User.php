<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Authentication\PasswordHasher\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $password_confirm
 * @property string $password_update
 * @property string $password_update_confirm
 * @property string|null $security_code
 * @property array|null $user_details
 * @property bool $is_enabled
 * @property bool $is_super
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Account[] $accounts
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => 1,
        'password' => 1,
        'password_confirm' => 1,
        'password_update' => 1,
        'password_update_confirm' => 1,
        'security_code' => 1,
        'user_details' => 1,
        'is_enabled' => 1,
        'is_super' => 1,
        'modified' => 1,
        'created' => 1,
        'accounts' => 1,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'password_confirm',
        'password_update',
        'password_update_confirm'
    ];

    protected function _setPassword(string $password): ?string
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher())->hash($password);
        }
    }

    protected function _getDisplayName()
    {
        return
            $this->_properties['first_name'] .
            ' ' .
            $this->_properties['last_name'];
    }
}
