<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConfigurationType Entity
 *
 * @property string $id
 * @property string|null $scope
 * @property string $name
 * @property string|null $description
 * @property string $data_type
 * @property string|null $default_value
 * @property string $tab
 * @property bool $is_editable
 * @property bool $is_enabled
 * @property bool $is_public
 * @property bool $is_required
 * @property string|null $required_message
 * @property string|null $required_regex
 * @property string|null $field_mask
 * @property int $sort_order
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\ConfigurationOption[] $configuration_options
 * @property \App\Model\Entity\Configuration[] $configurations
 */
class ConfigurationType extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'scope' => 1,
        'name' => 1,
        'description' => 1,
        'data_type' => 1,
        'default_value' => 1,
        'tab' => 1,
        'is_editable' => 1,
        'is_enabled' => 1,
        'is_public' => 1,
        'is_required' => 1,
        'required_message' => 1,
        'required_regex' => 1,
        'field_mask' => 1,
        'sort_order' => 1,
        'modified' => 1,
        'created' => 1,
        'configuration_options' => 1,
        'configurations' => 1,
    ];
}
