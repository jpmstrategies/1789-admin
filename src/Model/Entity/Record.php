<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Record Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $action_id
 * @property string $legislator_id
 * @property string $rating_id
 * @property bool $is_chair
 * @property bool $has_statement
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Action $action
 * @property \App\Model\Entity\Legislator $legislator
 * @property \App\Model\Entity\Rating $rating
 */
class Record extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'action_id' => 1,
        'legislator_id' => 1,
        'rating_id' => 1,
        'is_chair' => 1,
        'has_statement' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'action' => 1,
        'legislator' => 1,
        'rating' => 1,
    ];
}
