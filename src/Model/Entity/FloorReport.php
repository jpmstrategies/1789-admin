<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FloorReport Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $legislative_session_id
 * @property string $chamber_id
 * @property \Cake\I18n\FrozenDate|null $report_date
 * @property string|null $report_url
 * @property string|null $import_log
 * @property bool $is_approved
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\LegislativeSession $legislative_session
 * @property \App\Model\Entity\Chamber $chamber
 * @property \App\Model\Entity\Rating[] $ratings
 */
class FloorReport extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'legislative_session_id' => 1,
        'chamber_id' => 1,
        'report_date' => 1,
        'report_url' => 1,
        'import_log' => 1,
        'is_approved' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'legislative_session' => 1,
        'chamber' => 1,
        'ratings' => 1,
    ];
}
