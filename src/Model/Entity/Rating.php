<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Rating Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $legislative_session_id
 * @property string $rating_type_id
 * @property string $chamber_id
 * @property string $name
 * @property string|null $description
 * @property string|null $fiscal_note
 * @property string|null $digest
 * @property string|null $vote_recommendation_notes
 * @property string|null $vote_references
 * @property string|null $action_id
 * @property string|null $position_id
 * @property int|null $record_vote
 * @property string|null $rating_weight
 * @property int|null $sort_order
 * @property string $slug
 * @property int|null $legiscan_bill_id
 * @property int|null $legiscan_bill_vote_id
 * @property array|null $json
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property bool $is_enabled
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\LegislativeSession $legislative_session
 * @property \App\Model\Entity\RatingType $rating_type
 * @property \App\Model\Entity\Chamber $chamber
 * @property \App\Model\Entity\Action $action
 * @property \App\Model\Entity\Position $position
 * @property \App\Model\Entity\LegiscanBill $legiscan_bill
 * @property \App\Model\Entity\LegiscanBillVote $legiscan_bill_vote
 * @property \App\Model\Entity\RatingCriteria[] $rating_criterias
 * @property \App\Model\Entity\Record[] $records
 * @property \App\Model\Entity\FloorReport[] $floor_reports
 * @property \App\Model\Entity\Priority[] $priorities
 */
class Rating extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'legislative_session_id' => 1,
        'rating_type_id' => 1,
        'chamber_id' => 1,
        'name' => 1,
        'description' => 1,
        'fiscal_note' => 1,
        'digest' => 1,
        'vote_recommendation_notes' => 1,
        'vote_references' => 1,
        'action_id' => 1,
        'position_id' => 1,
        'record_vote' => 1,
        'rating_weight' => 1,
        'sort_order' => 1,
        'slug' => 1,
        'legiscan_bill_id' => 1,
        'legiscan_bill_vote_id' => 1,
        'json' => 1,
        'created' => 1,
        'modified' => 1,
        'is_enabled' => 1,
        'tenant' => 1,
        'legislative_session' => 1,
        'rating_type' => 1,
        'chamber' => 1,
        'action' => 1,
        'position' => 1,
        'legiscan_bill' => 1,
        'legiscan_bill_vote' => 1,
        'rating_criterias' => 1,
        'records' => 1,
        'floor_reports' => 1,
        'priorities' => 1,
    ];
}
