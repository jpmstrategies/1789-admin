<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Upload Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $path
 * @property string $mime
 * @property int $width
 * @property int $height
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Tenant $tenant
 * @property \App\Model\Entity\Bill[] $bills
 */
class Upload extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'path' => 1,
        'mime' => 1,
        'width' => 1,
        'height' => 1,
        'created' => 1,
        'modified' => 1,
        'tenant' => 1,
        'bills' => 1,
    ];
}
