<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LegiscanSubject Entity
 *
 * @property string $state
 * @property int $id
 * @property string $subject_name
 *
 * @property \App\Model\Entity\LegiscanBillSubject[] $legiscan_bill_subjects
 */
class LegiscanSubject extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'state' => 1,
        'id' => 1,
        'subject_name' => 1,
        'legiscan_bill_subjects' => 1,
    ];
}
