<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LegiscanBill Entity
 *
 * @property int $id
 * @property string $state
 * @property string $bill_number
 * @property int $legiscan_status_id
 * @property string|null $status_desc
 * @property \Cake\I18n\FrozenDate|null $status_date
 * @property string $title
 * @property string $description
 * @property int $legiscan_bill_type_id
 * @property string $bill_type_name
 * @property string $bill_type_abbr
 * @property int $legiscan_chamber_id
 * @property string $body_abbr
 * @property string|null $body_short
 * @property string $body_name
 * @property int $legiscan_current_chamber_id
 * @property string $current_body_abbr
 * @property string|null $current_body_short
 * @property string $current_body_name
 * @property int $legiscan_pending_committee_id
 * @property int|null $legiscan_pending_committee_chamber_id
 * @property string|null $pending_committee_body_abbr
 * @property string|null $pending_committee_body_short
 * @property string|null $pending_committee_body_name
 * @property string|null $pending_committee_name
 * @property string $legiscan_url
 * @property string $state_url
 * @property string $change_hash
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $updated
 * @property int $legiscan_state_id
 * @property string $state_name
 * @property int $legiscan_session_id
 * @property int $session_year_start
 * @property int $session_year_end
 * @property int $session_prefile
 * @property int $session_sine_die
 * @property int $session_prior
 * @property int $session_special
 * @property string $session_tag
 * @property string $session_title
 * @property string $session_name
 * @property int $has_bill_vote_details
 *
 * @property \App\Model\Entity\LegiscanChamber $legiscan_chamber
 * @property \App\Model\Entity\LegiscanSession $legiscan_session
 * @property \App\Model\Entity\LegiscanBillSubject[] $legiscan_bill_subjects
 * @property \App\Model\Entity\LegiscanBillVoteDetail[] $legiscan_bill_vote_details
 * @property \App\Model\Entity\LegiscanBillVote[] $legiscan_bill_votes
 * @property \App\Model\Entity\TenantsBill[] $tenants_bills
 */
class LegiscanBill extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => 1,
        'state' => 1,
        'bill_number' => 1,
        'legiscan_status_id' => 1,
        'status_desc' => 1,
        'status_date' => 1,
        'title' => 1,
        'description' => 1,
        'legiscan_bill_type_id' => 1,
        'bill_type_name' => 1,
        'bill_type_abbr' => 1,
        'legiscan_chamber_id' => 1,
        'body_abbr' => 1,
        'body_short' => 1,
        'body_name' => 1,
        'legiscan_current_chamber_id' => 1,
        'current_body_abbr' => 1,
        'current_body_short' => 1,
        'current_body_name' => 1,
        'legiscan_pending_committee_id' => 1,
        'legiscan_pending_committee_chamber_id' => 1,
        'pending_committee_body_abbr' => 1,
        'pending_committee_body_short' => 1,
        'pending_committee_body_name' => 1,
        'pending_committee_name' => 1,
        'legiscan_url' => 1,
        'state_url' => 1,
        'change_hash' => 1,
        'created' => 1,
        'updated' => 1,
        'legiscan_state_id' => 1,
        'state_name' => 1,
        'legiscan_session_id' => 1,
        'session_year_start' => 1,
        'session_year_end' => 1,
        'session_prefile' => 1,
        'session_sine_die' => 1,
        'session_prior' => 1,
        'session_special' => 1,
        'session_tag' => 1,
        'session_title' => 1,
        'session_name' => 1,
        'has_bill_vote_details' => 1,
        'legiscan_chamber' => 1,
        'legiscan_session' => 1,
        'legiscan_bill_subjects' => 1,
        'legiscan_bill_vote_details' => 1,
        'legiscan_bill_votes' => 1,
        'tenants_bills' => 1,
    ];
}
