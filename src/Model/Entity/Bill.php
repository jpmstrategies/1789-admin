<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Bill Entity
 *
 * @property string $id
 * @property string $tenant_id
 * @property string $legislative_session_id
 * @property string $filed_chamber_id
 * @property string $bill_number
 * @property string $official_title
 * @property string $explainer_title
 * @property string|null $description
 * @property string|null $notes
 * @property string|null $sponsors
 * @property int|null $legiscan_bill_id
 * @property string|null $current_chamber_id
 * @property string|null $current_bill_stage_id
 * @property string|null $current_bill_status_id
 * @property string|null $current_bill_cta_id
 * @property string|null $current_bill_cta_url
 * @property string|null $current_bill_progress_id
 * @property string|null $current_bill_phase_icon_id
 * @property string|null $current_bill_rating_icon_id
 * @property string|null $house_rating_id
 * @property string|null $house_rating_votes
 * @property string|null $senate_rating_id
 * @property string|null $senate_rating_votes
 * @property string|null $upload_id
 * @property string $slug
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property bool $is_enabled
 *
 * @property \App\Model\Entity\Tenant[] $tenants
 * @property \App\Model\Entity\LegislativeSession $legislative_session
 * @property \App\Model\Entity\Chamber $chamber
 * @property \App\Model\Entity\LegiscanBill $legiscan_bill
 * @property \App\Model\Entity\BillEnum $bill_enum
 * @property \App\Model\Entity\Rating $rating
 */
class Bill extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tenant_id' => 1,
        'legislative_session_id' => 1,
        'filed_chamber_id' => 1,
        'bill_number' => 1,
        'official_title' => 1,
        'explainer_title' => 1,
        'description' => 1,
        'notes' => 1,
        'sponsors' => 1,
        'legiscan_bill_id' => 1,
        'current_chamber_id' => 1,
        'current_bill_stage_id' => 1,
        'current_bill_status_id' => 1,
        'current_bill_cta_id' => 1,
        'current_bill_cta_url' => 1,
        'current_bill_progress_id' => 1,
        'current_bill_phase_icon_id' => 1,
        'current_bill_rating_icon_id' => 1,
        'house_rating_id' => 1,
        'house_rating_votes' => 1,
        'senate_rating_id' => 1,
        'senate_rating_votes' => 1,
        'upload_id' => 1,
        'slug' => 1,
        'created' => 1,
        'modified' => 1,
        'is_enabled' => 1,
        'tenants' => 1,
        'legislative_session' => 1,
        'chamber' => 1,
        'legiscan_bill' => 1,
        'bill_enum' => 1,
        'rating' => 1,
    ];
}
