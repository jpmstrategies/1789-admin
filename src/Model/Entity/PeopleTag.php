<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PeopleTag Entity
 *
 * @property string $id
 * @property string $person_id
 * @property string $tag_id
 *
 * @property \App\Model\Entity\Person $person
 * @property \App\Model\Entity\Tag $tag
 */
class PeopleTag extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'person_id' => 1,
        'tag_id' => 1,
        'person' => 1,
        'tag' => 1,
    ];
}
