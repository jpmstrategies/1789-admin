INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `created`, `modified`)
VALUES ('8c45c44a-c97f-4088-abab-e741f7a45e93', 'subscribe_link_text', '', 'text', 'Subscribe to Updates', 'legislator',
        0, 1, '2018-10-03 01:11:37', '2020-10-09 17:32:55'),
       ('a4ef937d-902e-4566-9194-f07369638621', 'directory_homepage_link_text',
        'Legislator header link to homepage text', 'text', '', 'admin', 1, 1, '2020-10-09 17:31:41',
        '2020-10-09 17:31:41'),
       ('abd7d03a-7f25-48e1-b011-1aa5b2c224fb', 'header_data_url', 'Customer API Endpoint to pull data for the header',
        'text', '', 'admin', 1, 1, '2020-10-09 17:28:02', '2020-10-09 17:28:02')
on duplicate key update description   = values(description),
                        data_type     = values(data_type),
                        default_value = values(default_value),
                        tab           = values(tab),
                        is_editable   = values(is_editable),
                        is_enabled    = values(is_enabled),
                        modified      = now();

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;

-- Update subscribe_link_text for all tenants to match default.
update configurations
set value = 'Subscribe to Updates'
where configuration_type_id = '8c45c44a-c97f-4088-abab-e741f7a45e93';

-- Add is_public column to hide config values from the client.
alter table configuration_types
	add is_public tinyint(1) default 1 not null after is_enabled;

-- Make es_host_url and es_index_name configs private
update configuration_types
set is_public = 0
where id in ('39003b3f-867d-4752-8ea2-4ac3b917638f', '59f8e2c8-4e6c-45b9-8a55-68c8059076a1');

-- directory_homepage_link_text for TXD
update configurations
set value = 'Texas Directory'
where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1'
and configuration_type_id = 'a4ef937d-902e-4566-9194-f07369638621';

-- header_data_url for TXD
update configurations
set value = 'https://texasscorecard.com/wp-json/texasscorecard/v1/countdown-featured'
where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1'
and configuration_type_id = 'abd7d03a-7f25-48e1-b011-1aa5b2c224fb';
