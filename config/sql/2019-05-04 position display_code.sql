ALTER TABLE `positions`
    ADD `display_code` char(1) NOT NULL DEFAULT 'N' after `name`;

UPDATE `positions`
SET `display_code` = 'F'
WHERE `name` LIKE 'Support';

UPDATE `positions`
SET `display_code` = 'O'
WHERE `name` LIKE 'Oppose';

UPDATE `positions`
SET `display_code` = 'F'
WHERE `name` LIKE '%(Support%';

UPDATE `positions`
SET `display_code` = 'O'
WHERE `name` LIKE '%(Oppose%';