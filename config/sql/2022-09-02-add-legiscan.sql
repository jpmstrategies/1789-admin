# Priorities
create table `priorities`
(
    `id`          char(36)     not null,
    `tenant_id`   char(36)     not null,
    `name`        varchar(255) not null,
    `description` text,
    `slug`        varchar(255) not null,
    `sort_order`  int(11)  default null,
    `created`     datetime default null,
    `modified`    datetime default null
) engine = innodb
  default charset = utf8;

alter table `priorities`
    add primary key (`id`),
    add unique key `XUN1_priorities` (`tenant_id`, `slug`),
    add key `XFK1_priorities` (`tenant_id`);

alter table `priorities`
    add constraint `XFK1_priorities` foreign key (`tenant_id`) references `tenants` (`id`);

create table `ratings_priorities`
(
    `id`          char(36) not null,
    `rating_id`   char(36) not null,
    `priority_id` char(36) not null
) engine = innodb
  default charset = utf8;

alter table `ratings_priorities`
    add primary key (`id`),
    add unique key `XUN1_ratings_priorities` (`rating_id`, `priority_id`),
    add key `XFK1_ratings_priorities` (`rating_id`),
    add key `XFK2_ratings_priorities` (`priority_id`);

alter table `ratings_priorities`
    add constraint `XFK1_ratings_priorities` foreign key (`rating_id`) references `ratings` (`id`),
    add constraint `XFK2_ratings_priorities` foreign key (`priority_id`) references `priorities` (`id`);

# tags
create table `tags`
(
    `id`          char(36)     not null,
    `tenant_id`   char(36)     not null,
    `name`        varchar(255) not null,
    `description` text,
    `slug`        varchar(255) not null,
    `css_class`   varchar(255) not null,
    `sort_order`  int(11)  default null,
    `created`     datetime default null,
    `modified`    datetime default null
) engine = innodb
  default charset = utf8;

alter table `tags`
    add primary key (`id`),
    add unique key `XUN1_tags` (`tenant_id`, `slug`),
    add key `XFK1_tags` (`tenant_id`);

alter table `tags`
    add constraint `XFK1_tags` foreign key (`tenant_id`) references `tenants` (`id`);

create table `people_tags`
(
    `id`        char(36) not null,
    `person_id` char(36) not null,
    `tag_id`    char(36) not null
) engine = innodb
  default charset = utf8;

alter table `people_tags`
    add primary key (`id`),
    add unique key `XUN1_people_tags` (`person_id`, `tag_id`),
    add key `XFK1_people_tags` (`person_id`),
    add key `XFK2_people_tags` (`tag_id`);

alter table `people_tags`
    add constraint `XFK1_people_tags` foreign key (`person_id`) references `people` (`id`),
    add constraint `XFK2_people_tags` foreign key (`tag_id`) references `tags` (`id`);

# bill tracking
create table `tenants_bills`
(
    `id`               char(36)           not null,
    `tenant_id`        char(36)           not null,
    `legiscan_bill_id` mediumint unsigned not null,
    `priority_id`      char(36)           null,
    `status`           varchar(255)       not null
) engine = innodb
  default charset = utf8;

alter table `tenants_bills`
    add primary key (`id`),
    add unique key `XUN1_tenants_bills` (`tenant_id`, `legiscan_bill_id`),
    add key `XFK1_tenants_bills` (`tenant_id`),
    add key `XFK2_tenants_bills` (`priority_id`),
    add key `XIE1_tenants_bills` (`legiscan_bill_id`);

alter table `tenants_bills`
    add constraint `XFK1_tenants_bills` foreign key (`tenant_id`) references `tenants` (`id`),
    add constraint `XFK2_tenants_bills` foreign key (`priority_id`) references `priorities` (`id`);

# existing tables
alter table tenants
    add data_source varchar(255) default 'jpm' not null after type;

alter table legislative_sessions
    add legiscan_session_id smallint unsigned default null after slug,
    add unique key `XUN3_legislative_sessions` (`tenant_id`, `legiscan_session_id`);

alter table people
    add legiscan_people_id smallint unsigned default null after slug,
    add unique key `XUN3_people` (`tenant_id`, `legiscan_people_id`);

alter table ratings
    add legiscan_bill_vote_id mediumint unsigned default null after slug,
    add unique key `XUN3_ratings` (`tenant_id`, `legiscan_bill_vote_id`);

alter table parties
    add legiscan_party_id tinyint unsigned default null after css_color,
    add unique key `XUN2_parties` (`tenant_id`, `legiscan_party_id`);

alter table chambers
    add legiscan_chamber_id tinyint unsigned default null after slug,
    add unique key `XUN3_chambers` (`tenant_id`, `legiscan_chamber_id`);

alter table actions
    add legiscan_action_id tinyint unsigned default null after multiplier_oppose,
    add unique key `XUN3_actions` (`tenant_id`, `legiscan_action_id`);

drop index XUN1_legislative_sessions on legislative_sessions;

alter table legislative_sessions
    add unique key XUN1_legislative_sessions (tenant_id, year, session_type, special_session_number);

alter table legislators
    modify district varchar(255) default null;

GRANT SELECT, SHOW VIEW ON `legiscan`.* TO '1789_cornerstone'@'%';