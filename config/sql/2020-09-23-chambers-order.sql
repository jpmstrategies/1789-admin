ALTER TABLE `chambers`
    ADD COLUMN `sort_order` int(11) DEFAULT NULL after `is_default`;

UPDATE `chambers`
SET chambers.sort_order = 1
WHERE type = 'L';

UPDATE `chambers`
SET chambers.sort_order = 2
WHERE type = 'U';

UPDATE `chambers`
SET chambers.sort_order = 3
WHERE type = 'C';