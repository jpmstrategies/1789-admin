-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 11, 2019 at 03:34 PM
-- Server version: 5.7.25
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `1789_cornerstone`
--

--
-- Dumping data for table `configuration_types`
--

REPLACE INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                    `is_enabled`, `created`, `modified`)
VALUES ('961064d5-55c6-462f-945b-b6dab49c8d35', 'addthis_tool_header',
        'Inline Tool ID from AddThis (Can be blank if first tool configured)', 'text', '', 'social', 1, 1,
        '2019-08-11 20:13:33', '2019-08-11 20:31:07'),
       ('ce1d3c34-7b5b-4195-96e0-e186c23dd5fe', 'addthis_tool_content',
        'Inline Tool ID from AddThis (Can be blank if first tool configured)', 'text', '', 'social', 1, 1,
        '2019-08-11 20:13:47', '2019-08-11 20:31:13'),
       ('e486313a-778c-4ab8-8eb0-d948e9fac9b6', 'addthis_id', 'Public ID for AddThis widget', 'text', '', 'social', 1,
        1, '2019-08-03 16:03:25', '2019-08-03 16:03:35');

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;