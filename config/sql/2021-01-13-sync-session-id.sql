alter table organizations
    add sync_legislative_session_id char(36) null after org_tenant_id;

alter table organizations
    add constraint XFK3_organizations
        foreign key (sync_legislative_session_id) references legislative_sessions (id)
            on delete set null;
