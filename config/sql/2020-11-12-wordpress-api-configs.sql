--
-- Database: `1789_cornerstone`
--

--
-- Dumping data for table `configuration_types`
--

INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `is_public`, `created`, `modified`)
VALUES ('a04fbd31-8b95-4ac8-8798-29e556d0ce58', 'related_articles_source', '', 'select', 'none', 'admin', 1, 1, 1,
        '2020-11-12 22:52:15', '2020-11-12 22:55:27');

--
-- Dumping data for table `configuration_options`
--

INSERT INTO `configuration_options` (`id`, `configuration_type_id`, `name`, `value`, `sort_order`, `created`,
                                     `modified`)
VALUES ('0cd7e651-b9fb-4708-b238-9ae32a0c9d05', 'a04fbd31-8b95-4ac8-8798-29e556d0ce58', 'Wordpress API', 'wpapi', 2,
        '2020-11-12 22:53:01', '2020-11-12 22:53:01'),
       ('8ea9dcac-7552-46b7-bed7-f0b2b344d2ce', 'a04fbd31-8b95-4ac8-8798-29e556d0ce58', 'None', 'none', 1,
        '2020-11-12 22:52:39', '2020-11-12 22:52:39'),
       ('929ec5d3-ea9d-4949-a647-c8f31563ee2a', 'a04fbd31-8b95-4ac8-8798-29e556d0ce58', 'ElasticSearch',
        'elasticsearch', 3, '2020-11-12 22:53:16', '2020-11-12 22:53:16');

-- Delete `es_wp_show_posts`
DELETE
FROM `configurations`
WHERE configuration_type_id IN (
    SELECT id
    FROM `configuration_types`
    WHERE name = 'es_wp_show_posts'
);

DELETE
FROM `configuration_types`
WHERE name = 'es_wp_show_posts';

-- Rename `es_wp_domain_name`
UPDATE `configuration_types`
SET name = 'wp_domain_name',
     description = 'WordPress domain name for Elasticsearch proxy validation or WP API'
WHERE name = 'es_wp_domain_name';

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;