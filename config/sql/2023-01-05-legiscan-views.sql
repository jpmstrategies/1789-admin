drop view if exists 1789_cornerstone.legiscan_bill_sponsors;

create view 1789_cornerstone.legiscan_bill_sponsors as
select concat(b.bill_id, '-', sp.people_id) as id,
       st.state_abbr                               AS state,
       b.bill_id                                   AS legiscan_bill_id,
       sp.people_id                                AS legiscan_people_id,
       b.bill_number                               AS bill_number,
       sp.sponsor_order                            AS sponsor_order,
       sp.sponsor_type_id                          AS sponsor_type_id,
       spt.sponsor_type_desc                       AS sponsor_type_desc,
       p.party_id                                  AS legiscan_party_id,
       pa.party_abbr                               AS party_abbr,
       p.role_id                                   AS legiscan_role_id,
       r.role_abbr                                 AS role_abbr,
       r.role_name                                 AS role_name,
       p.name                                      AS name,
       p.first_name                                AS first_name,
       p.middle_name                               AS middle_name,
       p.last_name                                 AS last_name,
       p.suffix                                    AS suffix,
       p.nickname                                  AS nickname,
       p.ballotpedia                               AS ballotpedia,
       p.followthemoney_eid                        AS followthemoney_eid,
       p.votesmart_id                              AS votesmart_id,
       p.opensecrets_id                            AS opensecrets_id,
       p.knowwho_pid                               AS knowwho_pid,
       p.committee_sponsor_id                      AS committee_sponsor_id,
       c.committee_body_id                         AS committee_sponsor_body_id,
       c.committee_name                            AS committee_sponsor_name,
       p.person_hash                               AS person_hash,
       p.district                                  AS person_district,
       people_body.body_id                         AS legiscan_people_chamber_id,
       b.state_id                                  AS legiscan_state_id,
       st.state_name                               AS state_name,
       b.session_id                                AS legiscan_session_id,
       b.body_id                                   AS legiscan_chamber_id,
       b.current_body_id                           AS legiscan_current_chamber_id,
       b.bill_type_id                              AS legiscan_bill_type_id,
       b.status_id                                 AS legiscan_status_id,
       b.pending_committee_id                      AS legiscan_pending_committee_id
from legiscan.ls_bill b
         join legiscan.ls_bill_sponsor sp on b.bill_id = sp.bill_id
         join legiscan.ls_sponsor_type spt on sp.sponsor_type_id = spt.sponsor_type_id
         join legiscan.ls_people p on sp.people_id = p.people_id
         join legiscan.ls_party pa on p.party_id = pa.party_id
         join legiscan.ls_role r on p.role_id = r.role_id
         join legiscan.ls_state st on b.state_id = st.state_id
         join legiscan.ls_body people_body on people_body.role_id = p.role_id and people_body.state_id = st.state_id
left join legiscan.ls_committee c on p.committee_sponsor_id = c.committee_id
where p.role_id != 3;

