--
-- action_types
--
CREATE TABLE 1789_cornerstone.`action_types` (
                                   `id` char(36) NOT NULL,
                                   `tenant_id` char(36) NOT NULL,
                                   `name` char(50) NOT NULL,
                                   `css_color` char(7) NOT NULL,
                                   `sort_order` int NOT NULL,
                                   `created` datetime DEFAULT NULL,
                                   `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- add primary key
ALTER TABLE 1789_cornerstone.`action_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tenant_id` (`tenant_id`);

-- populate with existing data
INSERT INTO 1789_cornerstone.action_types
SELECT
  uuid() as id,
  t.id as tenant_id,
  results.name as name,
  results.css_color as css_color,
  results.sort_order as sort_order,
  NOW() as created,
  NOW() as modified
FROM
  (
    SELECT 'Y' as name, '#8ebd4e' as css_color, 1 as sort_order
    UNION
    SELECT 'N' as name, '#a5290d' as css_color, 2 as sort_order
    UNION
    SELECT 'A' as name, '#251815' as css_color, 3 as sort_order
    UNION
    SELECT 'S' as name, '#3c4485' as css_color, 4 as sort_order
    UNION
    SELECT 'C' as name, '#000000' as css_color, 5 as sort_order
    UNION
    SELECT 'E' as name, '#7a7a7a' as css_color, 6 as sort_order
    UNION
    SELECT 'P' as name, '#afafaf' as css_color, 7 as sort_order
    UNION
    SELECT 'U' as name, '#7a7a7a' as css_color, 8 as sort_order
  ) results
    CROSS JOIN 1789_cornerstone.tenants t;

-- add foreign key columns
ALTER TABLE 1789_cornerstone.action_types
  ADD CONSTRAINT XFK1_action_types FOREIGN KEY (tenant_id) REFERENCES 1789_cornerstone.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- add foreign key field to legislators
ALTER TABLE 1789_cornerstone.actions ADD COLUMN action_type_id char(36) NULL AFTER rating_type_id;

-- populate foreign key field for legislators
UPDATE 1789_cornerstone.actions a
  INNER JOIN 1789_cornerstone.action_types at ON a.type = at.name AND a.tenant_id = at.tenant_id
SET a.action_type_id = at.id;

-- add foreign key
ALTER TABLE 1789_cornerstone.actions
  ADD CONSTRAINT `XFK3_actions` FOREIGN KEY (`action_type_id`) REFERENCES `action_types`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- drop original type column
ALTER TABLE 1789_cornerstone.actions DROP `type`;

-- give friendly name
UPDATE 1789_cornerstone.action_types SET name = 'Absent' WHERE name = 'A';
UPDATE 1789_cornerstone.action_types SET name = 'Chair' WHERE name = 'C';
UPDATE 1789_cornerstone.action_types SET name = 'Speaker' WHERE name = 'S';
UPDATE 1789_cornerstone.action_types SET name = 'Absent, Excused' WHERE name = 'E';
UPDATE 1789_cornerstone.action_types SET name = 'Nay' WHERE name = 'N';
UPDATE 1789_cornerstone.action_types SET name = 'Present, Not Voting' WHERE name = 'P';
UPDATE 1789_cornerstone.action_types SET name = 'Yea' WHERE name = 'Y';
UPDATE 1789_cornerstone.action_types SET name = 'Not Applicable' WHERE name = 'U';

-- update sort_order
UPDATE 1789_cornerstone.action_types SET sort_order = 1 WHERE name = 'Yea';
UPDATE 1789_cornerstone.action_types SET sort_order = 2 WHERE name = 'Nay';
UPDATE 1789_cornerstone.action_types SET sort_order = 3 WHERE name = 'Absent';
UPDATE 1789_cornerstone.action_types SET sort_order = 4 WHERE name = 'Speaker';
UPDATE 1789_cornerstone.action_types SET sort_order = 5 WHERE name = 'Chair';
UPDATE 1789_cornerstone.action_types SET sort_order = 6 WHERE name = 'Absent, Excused';
UPDATE 1789_cornerstone.action_types SET sort_order = 7 WHERE name = 'Present, Not Voting';
UPDATE 1789_cornerstone.action_types SET sort_order = 8 WHERE name = 'Not Applicable';

ALTER TABLE 1789_cornerstone.action_types ADD `type` CHAR(1) NULL AFTER `tenant_id`;

UPDATE 1789_cornerstone.action_types SET type = 'A' WHERE name = 'Absent';
UPDATE 1789_cornerstone.action_types SET type = 'C' WHERE name = 'Chair';
UPDATE 1789_cornerstone.action_types SET type = 'S' WHERE name = 'Speaker';
UPDATE 1789_cornerstone.action_types SET type = 'E' WHERE name = 'Absent, Excused';
UPDATE 1789_cornerstone.action_types SET type = 'E' WHERE name = 'Excused Absent';
UPDATE 1789_cornerstone.action_types SET type = 'N' WHERE name = 'Nay';
UPDATE 1789_cornerstone.action_types SET type = 'N' WHERE name = 'Anti-Taxpayer';
UPDATE 1789_cornerstone.action_types SET type = 'P' WHERE name = 'Present, Not Voting';
UPDATE 1789_cornerstone.action_types SET type = 'P' WHERE name = 'Present Not Voting';
UPDATE 1789_cornerstone.action_types SET type = 'Y' WHERE name = 'Yea';
UPDATE 1789_cornerstone.action_types SET type = 'Y' WHERE name = 'Pro-Taxpayer';
UPDATE 1789_cornerstone.action_types SET type = 'U' WHERE name = 'Not Applicable';
UPDATE 1789_cornerstone.action_types SET type = 'U' WHERE name = 'N/A';

ALTER TABLE `action_types` ADD `icon_text` VARCHAR(10) NULL AFTER `name`;

UPDATE 1789_cornerstone.action_types SET icon_text = 'Absent' WHERE name = 'Absent';
UPDATE 1789_cornerstone.action_types SET icon_text = 'Chair' WHERE name = 'Chair';
UPDATE 1789_cornerstone.action_types SET icon_text = 'Speaker' WHERE name = 'Speaker';
UPDATE 1789_cornerstone.action_types SET icon_text = 'A-EX' WHERE name = 'Absent, Excused';
UPDATE 1789_cornerstone.action_types SET icon_text = 'A-EX' WHERE name = 'Excused Absent';
UPDATE 1789_cornerstone.action_types SET icon_text = '' WHERE name = 'Nay';
UPDATE 1789_cornerstone.action_types SET icon_text = '' WHERE name = 'Anti-Taxpayer';
UPDATE 1789_cornerstone.action_types SET icon_text = 'PNV' WHERE name = 'Present, Not Voting';
UPDATE 1789_cornerstone.action_types SET icon_text = 'PNV' WHERE name = 'Present Not Voting';
UPDATE 1789_cornerstone.action_types SET icon_text = '' WHERE name = 'Yea';
UPDATE 1789_cornerstone.action_types SET icon_text = '' WHERE name = 'Pro-Taxpayer';
UPDATE 1789_cornerstone.action_types SET icon_text = 'N/A' WHERE name = 'Not Applicable';
UPDATE 1789_cornerstone.action_types SET icon_text = 'N/A' WHERE name = 'N/A';

UPDATE 1789_cornerstone.action_types SET css_color = '#251815' WHERE name = 'Chair';

ALTER TABLE `actions` CHANGE `action_type_id` `action_type_id` CHAR(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE `action_types` CHANGE `type` `type` CHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;