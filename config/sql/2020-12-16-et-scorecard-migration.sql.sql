-- REVOKE ALL PRIVILEGES ON `et\_ratings`.* FROM 'et_ratings_temp'@'%';
-- REVOKE GRANT OPTION ON `et\_ratings`.* FROM 'et_ratings_temp'@'%';
-- GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE VIEW ON `et\_ratings`.* TO 'et_ratings_temp'@'%';

-- add new GUID
ALTER TABLE et_ratings_temp.actions
    ADD COLUMN id_uuid char(36) NULL AFTER id;
ALTER TABLE et_ratings_temp.addresses
    ADD COLUMN id_uuid char(36) NULL AFTER id;
ALTER TABLE et_ratings_temp.legislative_sessions
    ADD COLUMN id_uuid char(36) NULL AFTER id;
ALTER TABLE et_ratings_temp.legislators
    ADD COLUMN id_uuid char(36) NULL AFTER id;
ALTER TABLE et_ratings_temp.logs
    ADD COLUMN id_uuid char(36) NULL AFTER id;
ALTER TABLE et_ratings_temp.people
    ADD COLUMN id_uuid char(36) NULL AFTER id;
ALTER TABLE et_ratings_temp.ratings
    ADD COLUMN id_uuid char(36) NULL AFTER id;
ALTER TABLE et_ratings_temp.rating_types
    ADD COLUMN id_uuid char(36) NULL AFTER id;
ALTER TABLE et_ratings_temp.records
    ADD COLUMN id_uuid char(36) NULL AFTER id;
ALTER TABLE et_ratings_temp.users
    ADD COLUMN id_uuid char(36) NULL AFTER id;

-- populate GUID
UPDATE et_ratings_temp.actions
SET id_uuid = UUID();
UPDATE et_ratings_temp.addresses
SET id_uuid = UUID();
UPDATE et_ratings_temp.legislative_sessions
SET id_uuid = UUID();
UPDATE et_ratings_temp.legislators
SET id_uuid = UUID();
UPDATE et_ratings_temp.logs
SET id_uuid = UUID();
UPDATE et_ratings_temp.people
SET id_uuid = UUID();
UPDATE et_ratings_temp.ratings
SET id_uuid = UUID();
UPDATE et_ratings_temp.rating_types
SET id_uuid = UUID();
UPDATE et_ratings_temp.records
SET id_uuid = UUID();
UPDATE et_ratings_temp.users
SET id_uuid = UUID();

-- set to NOT NULL
ALTER TABLE et_ratings_temp.actions
    MODIFY id_uuid char(36) NOT NULL;
ALTER TABLE et_ratings_temp.addresses
    MODIFY id_uuid char(36) NOT NULL;
ALTER TABLE et_ratings_temp.legislative_sessions
    MODIFY id_uuid char(36) NOT NULL;
ALTER TABLE et_ratings_temp.legislators
    MODIFY id_uuid char(36) NOT NULL;
ALTER TABLE et_ratings_temp.logs
    MODIFY id_uuid char(36) NOT NULL;
ALTER TABLE et_ratings_temp.people
    MODIFY id_uuid char(36) NOT NULL;
ALTER TABLE et_ratings_temp.ratings
    MODIFY id_uuid char(36) NOT NULL;
ALTER TABLE et_ratings_temp.rating_types
    MODIFY id_uuid char(36) NOT NULL;
ALTER TABLE et_ratings_temp.records
    MODIFY id_uuid char(36) NOT NULL;
ALTER TABLE et_ratings_temp.users
    MODIFY id_uuid char(36) NOT NULL;

-- add foreign key GUIDs
ALTER TABLE et_ratings_temp.actions
    ADD COLUMN rating_type_uuid char(36) NULL AFTER rating_type_id;
ALTER TABLE et_ratings_temp.addresses
    ADD COLUMN person_uuid char(36) NULL AFTER person_id;
ALTER TABLE et_ratings_temp.legislators
    ADD COLUMN legislative_session_uuid char(36) NULL AFTER legislative_session_id;
ALTER TABLE et_ratings_temp.legislators
    ADD COLUMN person_uuid char(36) NULL AFTER person_id;
ALTER TABLE et_ratings_temp.ratings
    ADD COLUMN legislative_session_uuid char(36) NULL AFTER legislative_session_id;
ALTER TABLE et_ratings_temp.ratings
    ADD COLUMN rating_type_uuid char(36) NULL AFTER rating_type_id;
ALTER TABLE et_ratings_temp.records
    ADD COLUMN action_uuid char(36) NULL AFTER action_id;
ALTER TABLE et_ratings_temp.records
    ADD COLUMN legislator_uuid char(36) NULL AFTER legislator_id;
ALTER TABLE et_ratings_temp.records
    ADD COLUMN rating_uuid char(36) NULL AFTER rating_id;

--
-- populate foreign keys
--
-- XFK1_actions
UPDATE et_ratings_temp.actions
    INNER JOIN et_ratings_temp.rating_types ON actions.rating_type_id = rating_types.id
SET actions.rating_type_uuid = rating_types.id_uuid;

-- XFK1_addresses
UPDATE et_ratings_temp.addresses
    INNER JOIN et_ratings_temp.people ON addresses.person_id = people.id
SET addresses.person_uuid = people.id_uuid;

-- XFK1_legislators
UPDATE et_ratings_temp.legislators
    INNER JOIN et_ratings_temp.legislative_sessions ON legislators.legislative_session_id = legislative_sessions.id
SET legislators.legislative_session_uuid = legislative_sessions.id_uuid;

-- XFK2_legislators
UPDATE et_ratings_temp.legislators
    INNER JOIN et_ratings_temp.people ON legislators.person_id = people.id
SET legislators.person_uuid = people.id_uuid;

-- XFK1_ratings
UPDATE et_ratings_temp.ratings
    INNER JOIN et_ratings_temp.legislative_sessions ON ratings.legislative_session_id = legislative_sessions.id
SET ratings.legislative_session_uuid = legislative_sessions.id_uuid;

-- XFK2_ratings
UPDATE et_ratings_temp.ratings
    INNER JOIN et_ratings_temp.rating_types ON ratings.rating_type_id = rating_types.id
SET ratings.rating_type_uuid = rating_types.id_uuid;

-- XFK1_records
UPDATE et_ratings_temp.records
    INNER JOIN et_ratings_temp.actions ON records.action_id = actions.id
SET records.action_uuid = actions.id_uuid;

-- XFK2_records
UPDATE et_ratings_temp.records
    INNER JOIN et_ratings_temp.legislators ON records.legislator_id = legislators.id
SET records.legislator_uuid = legislators.id_uuid;

-- XFK3_records
UPDATE et_ratings_temp.records
    INNER JOIN et_ratings_temp.ratings ON records.rating_id = ratings.id
SET records.rating_uuid = ratings.id_uuid;

-- drop foreign key constraints
ALTER TABLE et_ratings_temp.actions
    DROP FOREIGN KEY XFK1_actions;
ALTER TABLE et_ratings_temp.addresses
    DROP FOREIGN KEY XFK1_addresses;
ALTER TABLE et_ratings_temp.legislators
    DROP FOREIGN KEY XFK1_legislators;
ALTER TABLE et_ratings_temp.legislators
    DROP FOREIGN KEY XFK2_legislators;
ALTER TABLE et_ratings_temp.ratings
    DROP FOREIGN KEY XFK1_ratings;
ALTER TABLE et_ratings_temp.ratings
    DROP FOREIGN KEY XFK2_ratings;
ALTER TABLE et_ratings_temp.records
    DROP FOREIGN KEY XFK1_records;
ALTER TABLE et_ratings_temp.records
    DROP FOREIGN KEY XFK2_records;
ALTER TABLE et_ratings_temp.records
    DROP FOREIGN KEY XFK3_records;

-- drop old indexes
DROP INDEX XUN1_actions ON et_ratings_temp.actions;
DROP INDEX XUN2_actions ON et_ratings_temp.actions;
DROP INDEX XIE1_actions ON et_ratings_temp.actions;
DROP INDEX XUN1_addresses ON et_ratings_temp.addresses;
DROP INDEX XFK1_addresses ON et_ratings_temp.addresses;
DROP INDEX XUN1_legislative_sessions ON et_ratings_temp.legislative_sessions;
DROP INDEX XUN2_legislative_sessions ON et_ratings_temp.legislative_sessions;
DROP INDEX XUN1_legislators ON et_ratings_temp.legislators;
DROP INDEX XIE1_legislators ON et_ratings_temp.legislators;
DROP INDEX XIE2_legislators ON et_ratings_temp.legislators;
DROP INDEX XIE3_legislators ON et_ratings_temp.legislators;
DROP INDEX XIE4_legislators ON et_ratings_temp.legislators;
DROP INDEX type ON et_ratings_temp.logs;
DROP INDEX UN1_people ON et_ratings_temp.people;
DROP INDEX XUN1_ratings ON et_ratings_temp.ratings;
DROP INDEX XUN2_ratings ON et_ratings_temp.ratings;
DROP INDEX XIE1_ratings ON et_ratings_temp.ratings;
DROP INDEX XIE2_ratings ON et_ratings_temp.ratings;
DROP INDEX XUN1_rating_types ON et_ratings_temp.rating_types;
DROP INDEX XUN1_records ON et_ratings_temp.records;
DROP INDEX XIE1_records ON et_ratings_temp.records;
DROP INDEX XIE2_records ON et_ratings_temp.records;
DROP INDEX XIE3_records ON et_ratings_temp.records;

-- drop old foreign keys
ALTER TABLE et_ratings_temp.actions
    DROP COLUMN rating_type_id;
ALTER TABLE et_ratings_temp.addresses
    DROP COLUMN person_id;
ALTER TABLE et_ratings_temp.legislators
    DROP COLUMN legislative_session_id;
ALTER TABLE et_ratings_temp.legislators
    DROP COLUMN person_id;
ALTER TABLE et_ratings_temp.ratings
    DROP COLUMN legislative_session_id;
ALTER TABLE et_ratings_temp.ratings
    DROP COLUMN rating_type_id;
ALTER TABLE et_ratings_temp.records
    DROP COLUMN action_id;
ALTER TABLE et_ratings_temp.records
    DROP COLUMN legislator_id;
ALTER TABLE et_ratings_temp.records
    DROP COLUMN rating_id;

-- rename foreign key GUIDs
ALTER TABLE et_ratings_temp.actions
    CHANGE rating_type_uuid rating_type_id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.addresses
    CHANGE person_uuid person_id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.legislators
    CHANGE legislative_session_uuid legislative_session_id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.legislators
    CHANGE person_uuid person_id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.ratings
    CHANGE legislative_session_uuid legislative_session_id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.ratings
    CHANGE rating_type_uuid rating_type_id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.records
    CHANGE action_uuid action_id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.records
    CHANGE legislator_uuid legislator_id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.records
    CHANGE rating_uuid rating_id CHAR(36) NOT NULL;

-- remove primary key AUTO_INCREMENT
ALTER TABLE et_ratings_temp.actions
    CHANGE id id INT(11) UNSIGNED NOT NULL;
ALTER TABLE et_ratings_temp.addresses
    CHANGE id id INT(11) UNSIGNED NOT NULL;
ALTER TABLE et_ratings_temp.legislative_sessions
    CHANGE id id INT(11) UNSIGNED NOT NULL;
ALTER TABLE et_ratings_temp.legislators
    CHANGE id id INT(11) UNSIGNED NOT NULL;
ALTER TABLE et_ratings_temp.logs
    CHANGE id id INT(11) UNSIGNED NOT NULL;
ALTER TABLE et_ratings_temp.people
    CHANGE id id INT(11) UNSIGNED NOT NULL;
ALTER TABLE et_ratings_temp.ratings
    CHANGE id id INT(11) UNSIGNED NOT NULL;
ALTER TABLE et_ratings_temp.rating_types
    CHANGE id id INT(11) UNSIGNED NOT NULL;
ALTER TABLE et_ratings_temp.records
    CHANGE id id INT(11) UNSIGNED NOT NULL;
ALTER TABLE et_ratings_temp.users
    CHANGE id id INT(11) UNSIGNED NOT NULL;

-- drop primary keys
ALTER TABLE et_ratings_temp.actions
    DROP PRIMARY KEY;
ALTER TABLE et_ratings_temp.addresses
    DROP PRIMARY KEY;
ALTER TABLE et_ratings_temp.legislative_sessions
    DROP PRIMARY KEY;
ALTER TABLE et_ratings_temp.legislators
    DROP PRIMARY KEY;
ALTER TABLE et_ratings_temp.logs
    DROP PRIMARY KEY;
ALTER TABLE et_ratings_temp.people
    DROP PRIMARY KEY;
ALTER TABLE et_ratings_temp.ratings
    DROP PRIMARY KEY;
ALTER TABLE et_ratings_temp.rating_types
    DROP PRIMARY KEY;
ALTER TABLE et_ratings_temp.records
    DROP PRIMARY KEY;
ALTER TABLE et_ratings_temp.users
    DROP PRIMARY KEY;

-- drop old primary keys
ALTER TABLE et_ratings_temp.actions
    DROP COLUMN id;
ALTER TABLE et_ratings_temp.addresses
    DROP COLUMN id;
ALTER TABLE et_ratings_temp.legislative_sessions
    DROP COLUMN id;
ALTER TABLE et_ratings_temp.legislators
    DROP COLUMN id;
ALTER TABLE et_ratings_temp.logs
    DROP COLUMN id;
ALTER TABLE et_ratings_temp.people
    DROP COLUMN id;
ALTER TABLE et_ratings_temp.ratings
    DROP COLUMN id;
ALTER TABLE et_ratings_temp.rating_types
    DROP COLUMN id;
ALTER TABLE et_ratings_temp.records
    DROP COLUMN id;
ALTER TABLE et_ratings_temp.users
    DROP COLUMN id;

-- rename primary key GUIDS
ALTER TABLE et_ratings_temp.actions
    CHANGE id_uuid id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.addresses
    CHANGE id_uuid id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.legislative_sessions
    CHANGE id_uuid id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.legislators
    CHANGE id_uuid id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.logs
    CHANGE id_uuid id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.people
    CHANGE id_uuid id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.ratings
    CHANGE id_uuid id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.rating_types
    CHANGE id_uuid id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.records
    CHANGE id_uuid id CHAR(36) NOT NULL;
ALTER TABLE et_ratings_temp.users
    CHANGE id_uuid id CHAR(36) NOT NULL;

-- add new primary keys
ALTER TABLE et_ratings_temp.actions
    ADD PRIMARY KEY (id);
ALTER TABLE et_ratings_temp.addresses
    ADD PRIMARY KEY (id);
ALTER TABLE et_ratings_temp.legislative_sessions
    ADD PRIMARY KEY (id);
ALTER TABLE et_ratings_temp.legislators
    ADD PRIMARY KEY (id);
ALTER TABLE et_ratings_temp.logs
    ADD PRIMARY KEY (id);
ALTER TABLE et_ratings_temp.people
    ADD PRIMARY KEY (id);
ALTER TABLE et_ratings_temp.ratings
    ADD PRIMARY KEY (id);
ALTER TABLE et_ratings_temp.rating_types
    ADD PRIMARY KEY (id);
ALTER TABLE et_ratings_temp.records
    ADD PRIMARY KEY (id);
ALTER TABLE et_ratings_temp.users
    ADD PRIMARY KEY (id);

-- add back original indexes
ALTER TABLE et_ratings_temp.`actions`
    ADD UNIQUE KEY `XUN1_actions` (`rating_type_id`, `name`),
    ADD UNIQUE KEY `XUN2_actions` (`rating_type_id`, `description`),
    ADD KEY `FK_rating_type_id` (`rating_type_id`);

ALTER TABLE et_ratings_temp.`addresses`
    ADD UNIQUE KEY `XUN1_addresses` (`type`, `person_id`),
    ADD KEY `FK_person_id` (`person_id`);

ALTER TABLE et_ratings_temp.`legislative_sessions`
    ADD UNIQUE KEY `XUN1_legislative_sessions` (`legislature_number`, `session_type`, `special_session_number`),
    ADD UNIQUE KEY `XUN2_legislative_sessions` (`slug`);

ALTER TABLE et_ratings_temp.`legislators`
    ADD UNIQUE KEY `XUN1_legislators` (`legislative_session_id`, `person_id`, `chamber`),
    ADD KEY `FK_legislative_session_id` (`legislative_session_id`),
    ADD KEY `FK_person_id` (`person_id`),
    ADD KEY `XIE3_legislators` (`legislative_session_id`, `chamber`, `is_enabled`),
    ADD KEY `XIE4_legislators` (`number_grade`);

ALTER TABLE et_ratings_temp.`logs`
    ADD KEY `type` (`type`);

ALTER TABLE et_ratings_temp.`people`
    ADD UNIQUE KEY `XUN1_people` (`slug`);

ALTER TABLE et_ratings_temp.`ratings`
    ADD UNIQUE KEY `XUN1_ratings` (`legislative_session_id`, `chamber`, `name`),
    ADD UNIQUE KEY `XUN2_ratings` (`slug`),
    ADD KEY `FK_legislative_session_id` (`legislative_session_id`),
    ADD KEY `FK_rating_type_id` (`rating_type_id`);

ALTER TABLE et_ratings_temp.`rating_types`
    ADD UNIQUE KEY `XUN1_rating_types` (`name`);

ALTER TABLE et_ratings_temp.`records`
    ADD UNIQUE KEY `XUN1_records` (`legislator_id`, `rating_id`),
    ADD KEY `FK_action_id` (`action_id`),
    ADD KEY `FK_legislator_id` (`legislator_id`),
    ADD KEY `FK_rating_id` (`rating_id`);

-- unify character sets
ALTER TABLE et_ratings_temp.actions
    CONVERT TO CHARACTER SET utf8;
ALTER TABLE et_ratings_temp.addresses
    CONVERT TO CHARACTER SET utf8;
ALTER TABLE et_ratings_temp.legislative_sessions
    CONVERT TO CHARACTER SET utf8;
ALTER TABLE et_ratings_temp.legislators
    CONVERT TO CHARACTER SET utf8;
ALTER TABLE et_ratings_temp.logs
    CONVERT TO CHARACTER SET utf8;
ALTER TABLE et_ratings_temp.people
    CONVERT TO CHARACTER SET utf8;
ALTER TABLE et_ratings_temp.ratings
    CONVERT TO CHARACTER SET utf8;
ALTER TABLE et_ratings_temp.rating_types
    CONVERT TO CHARACTER SET utf8;
ALTER TABLE et_ratings_temp.records
    CONVERT TO CHARACTER SET utf8;
ALTER TABLE et_ratings_temp.users
    CONVERT TO CHARACTER SET utf8;

-- add back original foreign_key constraints
ALTER TABLE et_ratings_temp.actions
    ADD CONSTRAINT XFK1_actions FOREIGN KEY (rating_type_id) REFERENCES et_ratings_temp.rating_types (id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE et_ratings_temp.addresses
    ADD CONSTRAINT XFK1_addresses FOREIGN KEY (person_id) REFERENCES et_ratings_temp.people (id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE et_ratings_temp.legislators
    ADD CONSTRAINT XFK1_legislators FOREIGN KEY (legislative_session_id) REFERENCES et_ratings_temp.legislative_sessions (id) ON DELETE CASCADE ON UPDATE NO ACTION,
    ADD CONSTRAINT XFK2_legislators FOREIGN KEY (person_id) REFERENCES et_ratings_temp.people (id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE et_ratings_temp.ratings
    ADD CONSTRAINT XFK1_ratings FOREIGN KEY (legislative_session_id) REFERENCES et_ratings_temp.legislative_sessions (id) ON DELETE CASCADE ON UPDATE NO ACTION,
    ADD CONSTRAINT XFK2_ratings FOREIGN KEY (rating_type_id) REFERENCES et_ratings_temp.rating_types (id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE et_ratings_temp.records
    ADD CONSTRAINT XFK1_records FOREIGN KEY (action_id) REFERENCES et_ratings_temp.actions (id) ON DELETE CASCADE ON UPDATE NO ACTION,
    ADD CONSTRAINT XFK2_records FOREIGN KEY (legislator_id) REFERENCES et_ratings_temp.legislators (id) ON DELETE CASCADE ON UPDATE NO ACTION,
    ADD CONSTRAINT XFK3_records FOREIGN KEY (rating_id) REFERENCES et_ratings_temp.ratings (id) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Multi-Tenant support
--
-- add tenant table
CREATE TABLE et_ratings_temp.`tenants`
(
    `id`          char(36)     NOT NULL,
    `name`        varchar(100) NOT NULL,
    `domain_name` varchar(50)  NOT NULL,
    `created`     datetime DEFAULT NULL,
    `modified`    datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE et_ratings_temp.`tenants`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_actions` (`name`),
    ADD UNIQUE KEY `XUN2_actions` (`domain_name`);

-- insert record for ET
SELECT UUID();

INSERT INTO et_ratings_temp.`tenants` (`id`, `name`, `domain_name`, `created`, `modified`)
VALUES ('e59f110e-3045-5598-99a9-9d402d523cf4', 'Empower Texans', 'et.cornerstoneapp.org', NOW(), NOW());

-- add tenant columns
ALTER TABLE et_ratings_temp.actions
    ADD COLUMN tenant_id char(36) NULL after id;
ALTER TABLE et_ratings_temp.addresses
    ADD COLUMN tenant_id char(36) NULL after id;
ALTER TABLE et_ratings_temp.legislative_sessions
    ADD COLUMN tenant_id char(36) NULL after id;
ALTER TABLE et_ratings_temp.legislators
    ADD COLUMN tenant_id char(36) NULL after id;
ALTER TABLE et_ratings_temp.people
    ADD COLUMN tenant_id char(36) NULL after id;
ALTER TABLE et_ratings_temp.ratings
    ADD COLUMN tenant_id char(36) NULL after id;
ALTER TABLE et_ratings_temp.rating_types
    ADD COLUMN tenant_id char(36) NULL after id;
ALTER TABLE et_ratings_temp.records
    ADD COLUMN tenant_id char(36) NULL after id;
ALTER TABLE et_ratings_temp.users
    ADD COLUMN tenant_id char(36) NULL after id;

-- populate tenant columns
UPDATE et_ratings_temp.actions
SET tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE et_ratings_temp.addresses
SET tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE et_ratings_temp.legislative_sessions
SET tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE et_ratings_temp.legislators
SET tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE et_ratings_temp.people
SET tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE et_ratings_temp.ratings
SET tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE et_ratings_temp.rating_types
SET tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE et_ratings_temp.records
SET tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE et_ratings_temp.users
SET tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';

-- change tenant columns to be NOT NULL
ALTER TABLE et_ratings_temp.actions
    MODIFY tenant_id char(36) NOT NULL;
ALTER TABLE et_ratings_temp.addresses
    MODIFY tenant_id char(36) NOT NULL;
ALTER TABLE et_ratings_temp.legislative_sessions
    MODIFY tenant_id char(36) NOT NULL;
ALTER TABLE et_ratings_temp.legislators
    MODIFY tenant_id char(36) NOT NULL;
ALTER TABLE et_ratings_temp.people
    MODIFY tenant_id char(36) NOT NULL;
ALTER TABLE et_ratings_temp.ratings
    MODIFY tenant_id char(36) NOT NULL;
ALTER TABLE et_ratings_temp.rating_types
    MODIFY tenant_id char(36) NOT NULL;
ALTER TABLE et_ratings_temp.records
    MODIFY tenant_id char(36) NOT NULL;
ALTER TABLE et_ratings_temp.users
    MODIFY tenant_id char(36) NOT NULL;

-- add tenant indexes
ALTER TABLE et_ratings_temp.actions
    ADD KEY FK_tenant_id (tenant_id);
ALTER TABLE et_ratings_temp.addresses
    ADD KEY FK_tenant_id (tenant_id);
ALTER TABLE et_ratings_temp.legislative_sessions
    ADD KEY FK_tenant_id (tenant_id);
ALTER TABLE et_ratings_temp.legislators
    ADD KEY FK_tenant_id (tenant_id);
ALTER TABLE et_ratings_temp.people
    ADD KEY FK_tenant_id (tenant_id);
ALTER TABLE et_ratings_temp.ratings
    ADD KEY FK_tenant_id (tenant_id);
ALTER TABLE et_ratings_temp.rating_types
    ADD KEY FK_tenant_id (tenant_id);
ALTER TABLE et_ratings_temp.records
    ADD KEY FK_tenant_id (tenant_id);
ALTER TABLE et_ratings_temp.users
    ADD KEY FK_tenant_id (tenant_id);

-- add tenant foreign keys
ALTER TABLE et_ratings_temp.actions
    ADD CONSTRAINT XFK2_actions FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE et_ratings_temp.addresses
    ADD CONSTRAINT XFK2_addresses FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE et_ratings_temp.legislative_sessions
    ADD CONSTRAINT XFK2_legislative_sessions FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE et_ratings_temp.legislators
    ADD CONSTRAINT XFK3_legislators FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE et_ratings_temp.people
    ADD CONSTRAINT XFK1_people FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE et_ratings_temp.ratings
    ADD CONSTRAINT XFK3_ratings FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE et_ratings_temp.rating_types
    ADD CONSTRAINT XFK1_rating_types FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE et_ratings_temp.records
    ADD CONSTRAINT XFK4_records FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE et_ratings_temp.users
    ADD CONSTRAINT XFK1_users FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- drop old indexes
DROP INDEX XUN1_actions ON et_ratings_temp.actions;
DROP INDEX XUN2_actions ON et_ratings_temp.actions;

DROP INDEX XUN1_addresses ON et_ratings_temp.addresses;

DROP INDEX XUN1_legislative_sessions ON et_ratings_temp.legislative_sessions;
DROP INDEX XUN2_legislative_sessions ON et_ratings_temp.legislative_sessions;

DROP INDEX XUN1_legislators ON et_ratings_temp.legislators;
DROP INDEX XIE3_legislators ON et_ratings_temp.legislators;
DROP INDEX XIE4_legislators ON et_ratings_temp.legislators;

DROP INDEX type ON et_ratings_temp.logs;

DROP INDEX XUN1_people ON et_ratings_temp.people;

DROP INDEX XUN1_ratings ON et_ratings_temp.ratings;
DROP INDEX XUN2_ratings ON et_ratings_temp.ratings;

DROP INDEX XUN1_rating_types ON et_ratings_temp.rating_types;

DROP INDEX XUN1_records ON et_ratings_temp.records;

-- add indexes with tenant
ALTER TABLE et_ratings_temp.`actions`
    ADD UNIQUE KEY `XUN1_actions` (`tenant_id`, `rating_type_id`, `name`),
    ADD UNIQUE KEY `XUN2_actions` (`tenant_id`, `rating_type_id`, `description`);

ALTER TABLE et_ratings_temp.`addresses`
    ADD UNIQUE KEY `XUN1_addresses` (`tenant_id`, `type`, `person_id`);

ALTER TABLE et_ratings_temp.`legislative_sessions`
    ADD UNIQUE KEY `XUN1_legislative_sessions` (`tenant_id`, `legislature_number`, `session_type`,
                                                `special_session_number`),
    ADD UNIQUE KEY `XUN2_legislative_sessions` (`tenant_id`, `slug`);

ALTER TABLE et_ratings_temp.`legislators`
    ADD UNIQUE KEY `XUN1_legislators` (`tenant_id`, `legislative_session_id`, `person_id`, `chamber`),
    ADD KEY `XIE1_legislators` (`tenant_id`, `legislative_session_id`, `chamber`, `is_enabled`),
    ADD KEY `XIE2_legislators` (`tenant_id`, `number_grade`);

ALTER TABLE et_ratings_temp.`logs`
    ADD KEY `type` (`type`);

ALTER TABLE et_ratings_temp.`people`
    ADD UNIQUE KEY `UN1_people` (`tenant_id`, `slug`);

ALTER TABLE et_ratings_temp.`ratings`
    ADD UNIQUE KEY `XUN1_ratings` (`tenant_id`, `legislative_session_id`, `chamber`, `name`),
    ADD UNIQUE KEY `XUN2_ratings` (`tenant_id`, `slug`);

ALTER TABLE et_ratings_temp.`rating_types`
    ADD UNIQUE KEY `XUN1_rating_types` (`tenant_id`, `name`);

ALTER TABLE et_ratings_temp.`records`
    ADD UNIQUE KEY `XUN1_records` (`tenant_id`, `legislator_id`, `rating_id`);

TRUNCATE TABLE et_ratings_temp.logs;

-- =============================================
--
-- START Multi-Tenant changes
--
-- =============================================

--
-- change up users
--

CREATE TABLE et_ratings_temp.`accounts`
(
    `id`        char(36) NOT NULL,
    `tenant_id` char(36) NOT NULL,
    `user_id`   char(36) NOT NULL,
    `created`   datetime DEFAULT NULL,
    `modified`  datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE et_ratings_temp.`accounts`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_accounts` (`tenant_id`, `user_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`),
    ADD KEY `FK_user_id` (`user_id`);

ALTER TABLE et_ratings_temp.accounts
    ADD CONSTRAINT XFK1_accounts FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE et_ratings_temp.accounts
    ADD CONSTRAINT XFK2_accounts FOREIGN KEY (user_id) REFERENCES et_ratings_temp.users (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- populate memberships with current data
INSERT INTO et_ratings_temp.accounts
SELECT UUID() as id, tenant_id, id as user_id, NOW() as created, NOW() as modified
FROM et_ratings_temp.users;

-- drop tenant from users
ALTER TABLE et_ratings_temp.users
    DROP FOREIGN KEY XFK1_users;
ALTER TABLE et_ratings_temp.`users`
    DROP INDEX `FK_tenant_id`;
ALTER TABLE et_ratings_temp.`users`
    DROP `tenant_id`;

--
-- add changes
--
-- CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `chambers`  AS  select 'H' AS `id`,'texas-house' AS `slug`,'State Representative' AS `title`,'State Rep.' AS `titleAbbreviation`,'House District' AS `districtFriendly` union all select 'S' AS `id`,'texas-senate' AS `slug`,'State Senator' AS `title`,'State Sen.' AS `titleAbbreviation`,'Senate District' AS `districtFriendly` ;

DROP VIEW IF EXISTS et_ratings_temp.`chambers`;

CREATE TABLE et_ratings_temp.`chambers`
(
    `id`                         char(36)            NOT NULL,
    `tenant_id`                  char(36)            NOT NULL,
    `type`                       char(1)             NOT NULL,
    `name`                       varchar(100)        NOT NULL,
    `title_short`                varchar(100)        NULL,
    `title_long`                 varchar(100)        NULL,
    `presiding_legislator_short` varchar(100)        NULL,
    `presiding_legislator_long`  varchar(100)        NULL,
    `slug`                       varchar(255)        NOT NULL,
    `is_enabled`                 tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
    `created`                    datetime                     DEFAULT NULL,
    `modified`                   datetime                     DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE et_ratings_temp.`chambers`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_chambers` (`tenant_id`, `type`),
    ADD UNIQUE KEY `XUN2_chambers` (`tenant_id`, `slug`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

ALTER TABLE et_ratings_temp.chambers
    ADD CONSTRAINT XFK1_chambers FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- restore original view
CREATE VIEW et_ratings_temp.`v_chambers` AS
select 'H'                    AS `id`,
       'texas-house'          AS `slug`,
       'State Representative' AS `title`,
       'State Rep.'           AS `titleAbbreviation`,
       'House District'       AS `districtFriendly`
union all
select 'S'               AS `id`,
       'texas-senate'    AS `slug`,
       'State Senator'   AS `title`,
       'State Sen.'      AS `titleAbbreviation`,
       'Senate District' AS `districtFriendly`;

-- populate for each tenant
INSERT INTO et_ratings_temp.chambers
SELECT uuid()              as id,
       t.id                as tenant_id,
       v.id                as type,
       v.districtFriendly  as name,
       v.titleAbbreviation as title_short,
       v.title             as title_long,
       null                as presiding_legislator_short,
       null                as presiding_legislator_long,
       v.slug              as slug,
       1                   as is_enabled,
       now()               as created,
       now()               as modified
FROM et_ratings_temp.v_chambers v
         CROSS JOIN et_ratings_temp.tenants t;

-- drop new view
DROP VIEW IF EXISTS et_ratings_temp.v_chambers;

--
-- create original view with relations to new table
--
CREATE view et_ratings_temp.`v_chambers`
AS
SELECT c.type        as id,
       c.slug        as slug,
       c.title_long  as title,
       c.title_short as titleAbbreviation,
       c.name        as districtFriendly,
       c.tenant_id,
       c.id          as chamber_id
FROM et_ratings_temp.chambers c;

--
-- legislator => chamber changes
--
-- add foreign key field to legislators
ALTER TABLE et_ratings_temp.legislators
    ADD COLUMN chamber_id char(36) NULL AFTER chamber;

-- populate foreign key field for legislators
UPDATE et_ratings_temp.legislators l
    INNER JOIN et_ratings_temp.chambers c ON l.chamber = c.type AND l.tenant_id = c.tenant_id
SET l.chamber_id = c.id;

-- change to NOT NULL for legislators
ALTER TABLE et_ratings_temp.legislators
    MODIFY chamber_id char(36) NOT NULL;

-- add foreign key constraints to legislators
ALTER TABLE et_ratings_temp.legislators
    ADD KEY `FK_chamber_id` (`chamber_id`);

ALTER TABLE et_ratings_temp.legislators
    ADD CONSTRAINT XFK4_legislators FOREIGN KEY (chamber_id) REFERENCES et_ratings_temp.chambers (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- drop original indexes including chamber
ALTER TABLE et_ratings_temp.legislators
    DROP INDEX `XUN1_legislators`;
ALTER TABLE et_ratings_temp.legislators
    DROP INDEX `XIE1_legislators`;

-- add new indexes including chamber_id
ALTER TABLE et_ratings_temp.legislators
    ADD UNIQUE KEY `XUN1_legislators` (`tenant_id`, `legislative_session_id`, `person_id`, `chamber_id`);

ALTER TABLE et_ratings_temp.legislators
    ADD KEY `XIE1_legislators` (`tenant_id`, `legislative_session_id`, `chamber_id`, `is_enabled`);

-- drop original column from legislators
ALTER TABLE et_ratings_temp.legislators
    DROP chamber;

-- change to user U/L
UPDATE et_ratings_temp.chambers
SET type = 'L'
WHERE type = 'H';
UPDATE et_ratings_temp.chambers
SET type = 'U'
WHERE type = 'S';

UPDATE et_ratings_temp.ratings
SET chamber = 'L'
WHERE chamber = 'H';
UPDATE et_ratings_temp.ratings
SET chamber = 'U'
WHERE chamber = 'S';

-- optimize table
OPTIMIZE TABLE et_ratings_temp.`legislators`;

--
-- ratings => chamber changes
--
-- add foreign key field to legislators
ALTER TABLE et_ratings_temp.ratings
    ADD COLUMN chamber_id char(36) NULL AFTER chamber;

-- populate foreign key field for ratings
UPDATE et_ratings_temp.ratings r
    INNER JOIN et_ratings_temp.chambers c ON r.chamber = c.type AND r.tenant_id = c.tenant_id
SET r.chamber_id = c.id;

-- change to NOT NULL for ratings
ALTER TABLE et_ratings_temp.ratings
    MODIFY chamber_id char(36) NOT NULL;

-- add foreign key constraints to ratings
ALTER TABLE et_ratings_temp.ratings
    ADD KEY `FK_chamber_id` (`chamber_id`);

ALTER TABLE et_ratings_temp.ratings
    ADD CONSTRAINT XFK4_ratings FOREIGN KEY (chamber_id) REFERENCES et_ratings_temp.chambers (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- drop original indexes including chamber
ALTER TABLE et_ratings_temp.ratings
    DROP INDEX `XUN1_ratings`;

-- add new indexes including chamber_id
ALTER TABLE et_ratings_temp.ratings
    ADD UNIQUE KEY `XUN1_ratings` (`tenant_id`, `legislative_session_id`, `chamber_id`, `name`);

-- drop original column from ratings
ALTER TABLE et_ratings_temp.ratings
    DROP chamber;

-- optmiize table
OPTIMIZE TABLE et_ratings_temp.`ratings`;

-- add default
ALTER TABLE et_ratings_temp.`chambers`
    ADD `is_default` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_enabled`;

UPDATE et_ratings_temp.chambers
SET is_default = 1
WHERE type = 'L';

ALTER TABLE et_ratings_temp.chambers
    ADD UNIQUE `XUN3_chambers` (`tenant_id`, `is_default`);

--
-- add parties
--

-- drop existing view
DROP VIEW IF EXISTS et_ratings_temp.parties;

CREATE TABLE et_ratings_temp.`parties`
(
    `id`           char(36)            NOT NULL,
    `tenant_id`    char(36)            NOT NULL,
    `name`         char(40)            NOT NULL,
    `abbreviation` char(15)            NOT NULL,
    `is_enabled`   tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
    `created`      datetime                     DEFAULT NULL,
    `modified`     datetime                     DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


-- add primary key
ALTER TABLE et_ratings_temp.`parties`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_parties` (`tenant_id`, `abbreviation`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

-- populate with existing data
INSERT INTO et_ratings_temp.parties
SELECT uuid()                            as id,
       tenant_id,
       concat('Unknown Party - ', party) as name,
       party                             as abbreviation,
       1                                 as is_enabled,
       now()                             as created,
       now()                             as modified
FROM (
         SELECT distinct tenant_id, party
         FROM et_ratings_temp.legislators
         ORDER BY tenant_id, party
     ) results;

-- add foreign key columns
ALTER TABLE et_ratings_temp.parties
    ADD CONSTRAINT XFK1_parties FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- add foreign key field to legislators
ALTER TABLE et_ratings_temp.legislators
    ADD COLUMN party_id char(36) NULL AFTER party;

-- populate foreign key field for legislators
UPDATE et_ratings_temp.legislators l
    INNER JOIN et_ratings_temp.parties p ON l.party = p.abbreviation AND l.tenant_id = p.tenant_id
SET l.party_id = p.id;

-- change to NOT NULL for ratings
ALTER TABLE et_ratings_temp.legislators
    MODIFY party_id char(36) NOT NULL;

-- add foreign key constraints to ratings
ALTER TABLE et_ratings_temp.legislators
    ADD KEY `FK_party_id` (`party_id`);

ALTER TABLE et_ratings_temp.legislators
    ADD CONSTRAINT XFK5_legislators FOREIGN KEY (party_id) REFERENCES et_ratings_temp.parties (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- drop original column from ratings
ALTER TABLE et_ratings_temp.legislators
    DROP party;

-- optmiize table
OPTIMIZE TABLE et_ratings_temp.`legislators`;

-- recreate previous view
CREATE VIEW et_ratings_temp.`v_parties` AS
SELECT abbreviation as id,
       name         as name,
       tenant_id,
       id           as party_id
FROM et_ratings_temp.parties;

--
-- grades
--
CREATE TABLE et_ratings_temp.`grades`
(
    `id`         char(36) NOT NULL,
    `tenant_id`  char(36) NOT NULL,
    `name`       char(50) NOT NULL,
    `css_color`  char(7)  NOT NULL,
    `sort_order` int      NOT NULL,
    `created`    datetime DEFAULT NULL,
    `modified`   datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- add primary key
ALTER TABLE et_ratings_temp.`grades`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

-- populate with existing data
INSERT INTO et_ratings_temp.grades
SELECT uuid()             as id,
       t.id               as tenant_id,
       results.name       as name,
       results.css_color  as css_color,
       results.sort_order as sort_order,
       NOW()              as created,
       NOW()              as modified
FROM (
         SELECT 'F-' as name, '' as css_color, '1' as sort_order
         UNION
         SELECT 'F' as name, '' as css_color, '2' as sort_order
         UNION
         SELECT 'F+' as name, '' as css_color, '3' as sort_order
         UNION
         SELECT 'D-' as name, '' as css_color, '4' as sort_order
         UNION
         SELECT 'D' as name, '' as css_color, '5' as sort_order
         UNION
         SELECT 'D+' as name, '' as css_color, '6' as sort_order
         UNION
         SELECT 'C-' as name, '' as css_color, '7' as sort_order
         UNION
         SELECT 'C' as name, '' as css_color, '8' as sort_order
         UNION
         SELECT 'C+' as name, '' as css_color, '9' as sort_order
         UNION
         SELECT 'B-' as name, '' as css_color, '10' as sort_order
         UNION
         SELECT 'B' as name, '' as css_color, '11' as sort_order
         UNION
         SELECT 'B+' as name, '' as css_color, '12' as sort_order
         UNION
         SELECT 'A-' as name, '' as css_color, '13' as sort_order
         UNION
         SELECT 'A' as name, '' as css_color, '14' as sort_order
         UNION
         SELECT 'A+' as name, '' as css_color, '15' as sort_order
     ) results
         CROSS JOIN et_ratings_temp.tenants t;

-- add foreign key columns
ALTER TABLE et_ratings_temp.grades
    ADD CONSTRAINT XFK1_grades FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- add foreign key field to legislators
ALTER TABLE et_ratings_temp.legislators
    ADD COLUMN grade_id char(36) NULL AFTER letter_grade;

-- UPDATE et_ratings_temp.N/A to be null
-- UPDATE et_ratings_temp.legislators SET letter_grade = null WHERE letter_grade = 'N/A';

-- populate foreign key field for legislators
UPDATE et_ratings_temp.legislators l
    INNER JOIN et_ratings_temp.grades g ON l.letter_grade = g.name AND l.tenant_id = g.tenant_id
SET l.grade_id = g.id;

-- change to NOT NULL for ratings
-- ALTER TABLE et_ratings_temp.legislators MODIFY grade_id char(36) NOT NULL;

-- add foreign key constraints to ratings
ALTER TABLE et_ratings_temp.legislators
    ADD KEY `FK_grade_id` (`grade_id`);

ALTER TABLE et_ratings_temp.legislators
    ADD CONSTRAINT XFK6_legislators FOREIGN KEY (grade_id) REFERENCES et_ratings_temp.grades (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- drop original column from ratings
ALTER TABLE et_ratings_temp.legislators
    DROP letter_grade;

-- UPDATE et_ratings_temp.colors
UPDATE et_ratings_temp.grades
SET css_color = '#000000'
WHERE name = 'N/A';
UPDATE et_ratings_temp.grades
SET css_color = '#ae3e25'
WHERE name = 'F-';
UPDATE et_ratings_temp.grades
SET css_color = '#ae3e25'
WHERE name = 'F';
UPDATE et_ratings_temp.grades
SET css_color = '#ae3e25'
WHERE name = 'F+';
UPDATE et_ratings_temp.grades
SET css_color = '#e5c21e'
WHERE name = 'D-';
UPDATE et_ratings_temp.grades
SET css_color = '#e5c21e'
WHERE name = 'D';
UPDATE et_ratings_temp.grades
SET css_color = '#e5c21e'
WHERE name = 'D+';
UPDATE et_ratings_temp.grades
SET css_color = '#e5c21e'
WHERE name = 'C-';
UPDATE et_ratings_temp.grades
SET css_color = '#e5c21e'
WHERE name = 'C';
UPDATE et_ratings_temp.grades
SET css_color = '#e5c21e'
WHERE name = 'C+';
UPDATE et_ratings_temp.grades
SET css_color = '#8ebd4e'
WHERE name = 'B-';
UPDATE et_ratings_temp.grades
SET css_color = '#8ebd4e'
WHERE name = 'B';
UPDATE et_ratings_temp.grades
SET css_color = '#8ebd4e'
WHERE name = 'B+';
UPDATE et_ratings_temp.grades
SET css_color = '#8ebd4e'
WHERE name = 'A-';
UPDATE et_ratings_temp.grades
SET css_color = '#8ebd4e'
WHERE name = 'A';
UPDATE et_ratings_temp.grades
SET css_color = '#8ebd4e'
WHERE name = 'A+';

-- optmiize table
OPTIMIZE TABLE et_ratings_temp.`legislators`;

-- add foreign key field to people
ALTER TABLE et_ratings_temp.people
    ADD COLUMN grade_id char(36) NULL AFTER letter_career_score;
ALTER TABLE et_ratings_temp.people
    MODIFY letter_career_score varchar(3) null;

-- UPDATE et_ratings_temp.N/A to be null
UPDATE et_ratings_temp.people
SET letter_career_score = null
WHERE letter_career_score = 'N/A';

-- populate foreign key field for legislators
UPDATE et_ratings_temp.people p
    INNER JOIN et_ratings_temp.grades g ON p.letter_career_score = g.name AND p.tenant_id = g.tenant_id
SET p.grade_id = g.id;

-- change to NOT NULL for ratings
-- ALTER TABLE et_ratings_temp.people MODIFY grade_id char(36) NOT NULL;

-- add foreign key constraints to ratings
ALTER TABLE et_ratings_temp.people
    ADD KEY `FK_grade_id` (`grade_id`);

ALTER TABLE et_ratings_temp.people
    ADD CONSTRAINT XFK2_people FOREIGN KEY (grade_id) REFERENCES et_ratings_temp.grades (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- drop original column from ratings
ALTER TABLE et_ratings_temp.people
    DROP letter_career_score;

-- optmiize table
OPTIMIZE TABLE et_ratings_temp.`people`;

--
-- rename People.grade_career_score to number_grade
--
ALTER TABLE et_ratings_temp.people
    CHANGE grade_career_score grade_number decimal(5, 2) NULL;
ALTER TABLE et_ratings_temp.legislators
    CHANGE number_grade grade_number decimal(5, 2) NULL;

--
-- add actions to ratings
--

-- add foreign key field to ratings
ALTER TABLE et_ratings_temp.ratings
    ADD COLUMN action_id char(36) NULL AFTER position;

ALTER TABLE et_ratings_temp.`actions`
    CHANGE `name` `name` VARCHAR(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

-- insert any missing values
INSERT INTO et_ratings_temp.actions
SELECT uuid()                              as id,
       results.tenant_id,
       results.rating_type_id,
       position                            as type,
       concat('Unknown Action ', position) as name,
       ''                                  as description,
       0                                   as denom_value,
       0                                   as multiplier_support,
       0                                   as mutiplier_oppose,
       now()                               as created,
       now()                               as modified
FROM (
         SELECT distinct ratings.tenant_id, ratings.rating_type_id, ratings.position
         FROM et_ratings_temp.ratings
                  LEFT JOIN et_ratings_temp.actions
                            ON actions.rating_type_id = ratings.rating_type_id AND actions.type = ratings.position
         WHERE ratings.action_id IS NULL
           AND actions.id IS NULL
     ) results;

-- populate foreign key field for legislators
UPDATE et_ratings_temp.ratings r
    INNER JOIN et_ratings_temp.actions a ON r.position = a.type AND r.tenant_id = r.tenant_id AND
                                       r.rating_type_id = a.rating_type_id
SET r.action_id = a.id;

-- change to NOT NULL for ratings
ALTER TABLE et_ratings_temp.ratings
    MODIFY action_id char(36) NOT NULL;

-- add foreign key constraints to ratings
ALTER TABLE et_ratings_temp.ratings
    ADD KEY `FK_action_id` (`action_id`);

ALTER TABLE et_ratings_temp.ratings
    ADD CONSTRAINT XFK5_ratings FOREIGN KEY (action_id) REFERENCES et_ratings_temp.actions (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- drop original column from ratings
ALTER TABLE et_ratings_temp.ratings
    DROP position;

-- optmiize table
OPTIMIZE TABLE et_ratings_temp.`ratings`;

--
-- add positions
--

CREATE TABLE et_ratings_temp.`positions`
(
    `id`        char(36) NOT NULL,
    `tenant_id` char(36) NOT NULL,
    `name`      char(50) NOT NULL,
    `created`   datetime DEFAULT NULL,
    `modified`  datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- add primary key
ALTER TABLE et_ratings_temp.`positions`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

-- populate with existing data
INSERT INTO et_ratings_temp.positions
SELECT uuid() as id,
       results.tenant_id,
       results.position_text,
       now()  as created,
       now()  as modified
FROM (
         SELECT distinct tenant_id, position_text
         FROM et_ratings_temp.ratings
     ) results;

-- add foreign key columns
ALTER TABLE et_ratings_temp.positions
    ADD CONSTRAINT XFK1_positions FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- add foreign key field to legislators
ALTER TABLE et_ratings_temp.ratings
    ADD COLUMN position_id char(36) NULL AFTER position_text;

-- populate foreign key field for legislators
UPDATE et_ratings_temp.ratings r
    INNER JOIN et_ratings_temp.positions p ON r.position_text = p.name AND r.tenant_id = p.tenant_id
SET r.position_id = p.id;

-- change to NOT NULL for ratings
ALTER TABLE et_ratings_temp.ratings
    MODIFY position_id char(36) NOT NULL;

-- add foreign key constraints to ratings
ALTER TABLE et_ratings_temp.ratings
    ADD KEY `FK_position_id` (`position_id`);

ALTER TABLE et_ratings_temp.ratings
    ADD CONSTRAINT XFK6_ratings FOREIGN KEY (position_id) REFERENCES et_ratings_temp.positions (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- drop original column from ratings
ALTER TABLE et_ratings_temp.ratings
    DROP position_text;

-- UPDATE et_ratings_temp.with friendly names
UPDATE et_ratings_temp.positions
SET name = 'Unknown'
WHERE name = 'U';
UPDATE et_ratings_temp.positions
SET name = 'Support'
WHERE name = 'S';
UPDATE et_ratings_temp.positions
SET name = 'Oppose'
WHERE name = 'O';
UPDATE et_ratings_temp.positions
SET name = 'Support Amendment'
WHERE name = 'SA';
UPDATE et_ratings_temp.positions
SET name = 'Support Amendment (Oppose Motion to Table)'
WHERE name = 'OM';
UPDATE et_ratings_temp.positions
SET name = 'Oppose Amendment'
WHERE name = 'OA';
UPDATE et_ratings_temp.positions
SET name = 'Oppose Amendment (Support Motion to Table)'
WHERE name = 'SM';

-- optmiize table
OPTIMIZE TABLE et_ratings_temp.`ratings`;

--
-- Address Type
--

CREATE TABLE et_ratings_temp.`address_types`
(
    `id`        char(36) NOT NULL,
    `tenant_id` char(36) NOT NULL,
    `name`      char(50) NOT NULL,
    `created`   datetime DEFAULT NULL,
    `modified`  datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- add primary key
ALTER TABLE et_ratings_temp.`address_types`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_tenant_id` (`tenant_id`);

-- populate with existing data
INSERT INTO et_ratings_temp.address_types
SELECT uuid() as id,
       results.tenant_id,
       results.type,
       now()  as created,
       now()  as modified
FROM (
         SELECT distinct tenant_id, type
         FROM et_ratings_temp.addresses
     ) results;

-- add foreign key columns
ALTER TABLE et_ratings_temp.address_types
    ADD CONSTRAINT XFK1_address_types FOREIGN KEY (tenant_id) REFERENCES et_ratings_temp.tenants (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- add foreign key field to legislators
ALTER TABLE et_ratings_temp.addresses
    ADD COLUMN address_type_id char(36) NULL AFTER type;

-- populate foreign key field for legislators
UPDATE et_ratings_temp.addresses a
    INNER JOIN et_ratings_temp.address_types t ON a.type = t.name AND a.tenant_id = t.tenant_id
SET a.address_type_id = t.id;

-- change to NOT NULL for ratings
ALTER TABLE et_ratings_temp.addresses
    MODIFY address_type_id char(36) NOT NULL;

-- add foreign key constraints to ratings
ALTER TABLE et_ratings_temp.addresses
    ADD KEY `FK_address_type` (`address_type_id`);

ALTER TABLE et_ratings_temp.addresses
    ADD CONSTRAINT XFK3_addresses FOREIGN KEY (address_type_id) REFERENCES et_ratings_temp.address_types (id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- alter original indexes
ALTER TABLE et_ratings_temp.`addresses`
    DROP INDEX `XUN1_addresses`,
    ADD UNIQUE `XUN1_addresses` (`tenant_id`, `address_type_id`, `person_id`) USING BTREE;

-- drop original column from ratings
ALTER TABLE et_ratings_temp.addresses
    DROP type;

-- User table tweaks
ALTER TABLE et_ratings_temp.`users`
    DROP `username`;
ALTER TABLE et_ratings_temp.`users`
    ADD UNIQUE `XUN1_users` (`email`);
ALTER TABLE et_ratings_temp.`users`
    ADD `is_super` tinyint(1) NOT NULL DEFAULT '0' after `is_enabled`;

UPDATE et_ratings_temp.users
SET is_super = 1
WHERE email IN ('el.jefe42@gmail.com', 'jeff@jpmstrategies.com');

ALTER TABLE et_ratings_temp.`accounts`
    ADD `role` CHAR(1) NOT NULL DEFAULT 'U' AFTER `user_id`;
ALTER TABLE et_ratings_temp.`accounts`
    ADD `status` CHAR(1) NOT NULL DEFAULT 'P' AFTER `role`;

ALTER TABLE et_ratings_temp.`tenants`
    ADD `is_enabled` tinyint(1) NOT NULL DEFAULT '0' after `domain_name`;
UPDATE et_ratings_temp.tenants
SET is_enabled = 1;

ALTER TABLE et_ratings_temp.`users`
    ADD `security_code` varchar(255) DEFAULT NULL after `password`;

-- UPDATE et_ratings_temp.legacy data to be more friendly
DELETE
FROM et_ratings_temp.users
WHERE email NOT IN ('el.jefe42@gmail.com', 'jeff@jpmstrategies.com');
UPDATE et_ratings_temp.users
SET password = '$2y$10$edGiMrScpBaz.vmI8Rcv8usIS/1BcKnidJCvLTM/X.Xah8ajXD0eq'
WHERE email IN ('el.jefe42@gmail.com', 'jeff@jpmstrategies.com');

UPDATE et_ratings_temp.address_types
SET name = 'Capitol'
WHERE name = 'C';
UPDATE et_ratings_temp.address_types
SET name = 'District'
WHERE name = 'D';

UPDATE et_ratings_temp.parties
SET name = 'Democrat'
WHERE abbreviation = 'D';
UPDATE et_ratings_temp.parties
SET name = 'Republican'
WHERE abbreviation = 'R';
UPDATE et_ratings_temp.parties
SET name = 'Independent'
WHERE abbreviation = 'I';
UPDATE et_ratings_temp.parties
SET name = 'Unknown Party'
WHERE abbreviation = 'U';

--
-- Configuration
--

-- -----------------------------------------------------
-- Table `configuration_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS et_ratings_temp.`configuration_types`
(
    `id`            CHAR(36)     NOT NULL,
    `name`          VARCHAR(100) NOT NULL,
    `description`   VARCHAR(255) NULL,
    `data_type`     CHAR(1)      NOT NULL,
    `default_value` LONGTEXT     NULL,
    `is_editable`   TINYINT(1)   NOT NULL DEFAULT 0,
    `is_enabled`    TINYINT(1)   NOT NULL DEFAULT 0,
    `created`       DATETIME     NULL,
    `modified`      DATETIME     NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `XUN1_configuration_types` (`name` ASC)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8;


-- -----------------------------------------------------
-- Table `configurations`
-- -----------------------------------------------------
CREATE TABLE et_ratings_temp.`configurations`
(
    `id`                    char(36) NOT NULL,
    `configuration_type_id` char(36) NOT NULL,
    `tenant_id`             char(36) NOT NULL,
    `value`                 longtext,
    `created`               datetime DEFAULT NULL,
    `modified`              datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE et_ratings_temp.`configurations`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_configurations` (`tenant_id`, `configuration_type_id`),
    ADD KEY `XFK_configuration_type_id` (`configuration_type_id`),
    ADD KEY `XFK_tenant_id` (`tenant_id`);

ALTER TABLE et_ratings_temp.`configurations`
    ADD CONSTRAINT `XFK_configuration_type_id` FOREIGN KEY (`configuration_type_id`) REFERENCES `configuration_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE et_ratings_temp.`configurations`
    ADD CONSTRAINT `XFK_tenant_id` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- public id
ALTER TABLE et_ratings_temp.`tenants`
    ADD `id_public` VARCHAR(8) NOT NULL AFTER `domain_name`;

UPDATE et_ratings_temp.`tenants`
SET id_public = SUBSTRING(CAST(id AS char(36)), 1, 8);

--
-- Remove cascading deletes
--

ALTER TABLE et_ratings_temp.`accounts`
    DROP FOREIGN KEY `XFK1_accounts`;

ALTER TABLE et_ratings_temp.`accounts`
    ADD CONSTRAINT `XFK1_accounts` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`accounts`
    DROP FOREIGN KEY `XFK2_accounts`;

ALTER TABLE et_ratings_temp.`accounts`
    ADD CONSTRAINT `XFK2_accounts` FOREIGN KEY (`user_id`) REFERENCES et_ratings_temp.`users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`actions`
    DROP FOREIGN KEY `XFK1_actions`;

ALTER TABLE et_ratings_temp.`actions`
    ADD CONSTRAINT `XFK1_actions` FOREIGN KEY (`rating_type_id`) REFERENCES et_ratings_temp.`rating_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`actions`
    DROP FOREIGN KEY `XFK2_actions`;

ALTER TABLE et_ratings_temp.`actions`
    ADD CONSTRAINT `XFK2_actions` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`addresses`
    DROP FOREIGN KEY `XFK1_addresses`;

ALTER TABLE et_ratings_temp.`addresses`
    ADD CONSTRAINT `XFK1_addresses` FOREIGN KEY (`person_id`) REFERENCES et_ratings_temp.`people` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`addresses`
    DROP FOREIGN KEY `XFK2_addresses`;

ALTER TABLE et_ratings_temp.`addresses`
    ADD CONSTRAINT `XFK2_addresses` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`addresses`
    DROP FOREIGN KEY `XFK3_addresses`;

ALTER TABLE et_ratings_temp.`addresses`
    ADD CONSTRAINT `XFK3_addresses` FOREIGN KEY (`address_type_id`) REFERENCES et_ratings_temp.`address_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`address_types`
    DROP FOREIGN KEY `XFK1_address_types`;

ALTER TABLE et_ratings_temp.`address_types`
    ADD CONSTRAINT `XFK1_address_types` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`chambers`
    DROP FOREIGN KEY `XFK1_chambers`;

ALTER TABLE et_ratings_temp.`chambers`
    ADD CONSTRAINT `XFK1_chambers` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`grades`
    DROP FOREIGN KEY `XFK1_grades`;

ALTER TABLE et_ratings_temp.`grades`
    ADD CONSTRAINT `XFK1_grades` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`legislative_sessions`
    DROP FOREIGN KEY `XFK2_legislative_sessions`;

ALTER TABLE et_ratings_temp.`legislative_sessions`
    ADD CONSTRAINT `XFK2_legislative_sessions` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`legislators`
    DROP FOREIGN KEY `XFK1_legislators`;

ALTER TABLE et_ratings_temp.`legislators`
    ADD CONSTRAINT `XFK1_legislators` FOREIGN KEY (`legislative_session_id`) REFERENCES et_ratings_temp.`legislative_sessions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`legislators`
    DROP FOREIGN KEY `XFK2_legislators`;

ALTER TABLE et_ratings_temp.`legislators`
    ADD CONSTRAINT `XFK2_legislators` FOREIGN KEY (`person_id`) REFERENCES et_ratings_temp.`people` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`legislators`
    DROP FOREIGN KEY `XFK3_legislators`;

ALTER TABLE et_ratings_temp.`legislators`
    ADD CONSTRAINT `XFK3_legislators` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`legislators`
    DROP FOREIGN KEY `XFK4_legislators`;

ALTER TABLE et_ratings_temp.`legislators`
    ADD CONSTRAINT `XFK4_legislators` FOREIGN KEY (`chamber_id`) REFERENCES et_ratings_temp.`chambers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`legislators`
    DROP FOREIGN KEY `XFK5_legislators`;

ALTER TABLE et_ratings_temp.`legislators`
    ADD CONSTRAINT `XFK5_legislators` FOREIGN KEY (`party_id`) REFERENCES et_ratings_temp.`parties` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`legislators`
    DROP FOREIGN KEY `XFK6_legislators`;

ALTER TABLE et_ratings_temp.`legislators`
    ADD CONSTRAINT `XFK6_legislators` FOREIGN KEY (`grade_id`) REFERENCES et_ratings_temp.`grades` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`parties`
    DROP FOREIGN KEY `XFK1_parties`;

ALTER TABLE et_ratings_temp.`parties`
    ADD CONSTRAINT `XFK1_parties` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`people`
    DROP FOREIGN KEY `XFK1_people`;

ALTER TABLE et_ratings_temp.`people`
    ADD CONSTRAINT `XFK1_people` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`people`
    DROP FOREIGN KEY `XFK2_people`;

ALTER TABLE et_ratings_temp.`people`
    ADD CONSTRAINT `XFK2_people` FOREIGN KEY (`grade_id`) REFERENCES et_ratings_temp.`grades` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`positions`
    DROP FOREIGN KEY `XFK1_positions`;

ALTER TABLE et_ratings_temp.`positions`
    ADD CONSTRAINT `XFK1_positions` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`ratings`
    DROP FOREIGN KEY `XFK1_ratings`;

ALTER TABLE et_ratings_temp.`ratings`
    ADD CONSTRAINT `XFK1_ratings` FOREIGN KEY (`legislative_session_id`) REFERENCES et_ratings_temp.`legislative_sessions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`ratings`
    DROP FOREIGN KEY `XFK2_ratings`;

ALTER TABLE et_ratings_temp.`ratings`
    ADD CONSTRAINT `XFK2_ratings` FOREIGN KEY (`rating_type_id`) REFERENCES et_ratings_temp.`rating_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`ratings`
    DROP FOREIGN KEY `XFK3_ratings`;

ALTER TABLE et_ratings_temp.`ratings`
    ADD CONSTRAINT `XFK3_ratings` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`ratings`
    DROP FOREIGN KEY `XFK4_ratings`;

ALTER TABLE et_ratings_temp.`ratings`
    ADD CONSTRAINT `XFK4_ratings` FOREIGN KEY (`chamber_id`) REFERENCES et_ratings_temp.`chambers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`ratings`
    DROP FOREIGN KEY `XFK5_ratings`;

ALTER TABLE et_ratings_temp.`ratings`
    ADD CONSTRAINT `XFK5_ratings` FOREIGN KEY (`action_id`) REFERENCES et_ratings_temp.`actions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`ratings`
    DROP FOREIGN KEY `XFK6_ratings`;

ALTER TABLE et_ratings_temp.`ratings`
    ADD CONSTRAINT `XFK6_ratings` FOREIGN KEY (`position_id`) REFERENCES et_ratings_temp.`positions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`rating_types`
    DROP FOREIGN KEY `XFK1_rating_types`;

ALTER TABLE et_ratings_temp.`rating_types`
    ADD CONSTRAINT `XFK1_rating_types` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`records`
    DROP FOREIGN KEY `XFK1_records`;

ALTER TABLE et_ratings_temp.`records`
    ADD CONSTRAINT `XFK1_records` FOREIGN KEY (`action_id`) REFERENCES et_ratings_temp.`actions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`records`
    DROP FOREIGN KEY `XFK2_records`;

ALTER TABLE et_ratings_temp.`records`
    ADD CONSTRAINT `XFK2_records` FOREIGN KEY (`legislator_id`) REFERENCES et_ratings_temp.`legislators` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`records`
    DROP FOREIGN KEY `XFK3_records`;

ALTER TABLE et_ratings_temp.`records`
    ADD CONSTRAINT `XFK3_records` FOREIGN KEY (`rating_id`) REFERENCES et_ratings_temp.`ratings` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`records`
    DROP FOREIGN KEY `XFK4_records`;

ALTER TABLE et_ratings_temp.`records`
    ADD CONSTRAINT `XFK4_records` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- configuration_type changes
ALTER TABLE et_ratings_temp.configuration_types
    ADD COLUMN tab char(10) NOT NULL AFTER `default_value`;
ALTER TABLE et_ratings_temp.configuration_types
    MODIFY data_type char(10) NOT NULL;

--
-- add specific cascading deletes back
--
ALTER TABLE et_ratings_temp.`records`
    DROP FOREIGN KEY `XFK1_records`;

ALTER TABLE et_ratings_temp.`records`
    ADD CONSTRAINT `XFK1_records` FOREIGN KEY (`action_id`) REFERENCES et_ratings_temp.`actions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`records`
    DROP FOREIGN KEY `XFK2_records`;

ALTER TABLE et_ratings_temp.`records`
    ADD CONSTRAINT `XFK2_records` FOREIGN KEY (`legislator_id`) REFERENCES et_ratings_temp.`legislators` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`records`
    DROP FOREIGN KEY `XFK3_records`;

ALTER TABLE et_ratings_temp.`records`
    ADD CONSTRAINT `XFK3_records` FOREIGN KEY (`rating_id`) REFERENCES et_ratings_temp.`ratings` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`records`
    DROP FOREIGN KEY `XFK4_records`;

ALTER TABLE et_ratings_temp.`records`
    ADD CONSTRAINT `XFK4_records` FOREIGN KEY (`tenant_id`) REFERENCES et_ratings_temp.`tenants` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`addresses`
    DROP FOREIGN KEY `XFK1_addresses`;

ALTER TABLE et_ratings_temp.`addresses`
    ADD CONSTRAINT `XFK1_addresses` FOREIGN KEY (`person_id`) REFERENCES et_ratings_temp.`people` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE et_ratings_temp.`accounts`
    DROP FOREIGN KEY `XFK2_accounts`;

ALTER TABLE et_ratings_temp.`accounts`
    ADD CONSTRAINT `XFK2_accounts` FOREIGN KEY (`user_id`) REFERENCES et_ratings_temp.`users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

--
-- TODO change in cake
--
ALTER TABLE et_ratings_temp.`parties`
    DROP INDEX `XUN1_parties`,
    ADD UNIQUE `XUN1_parties` (`tenant_id`, `name`) USING BTREE;

--
-- TODO add import_pk
--
ALTER TABLE et_ratings_temp.`people`
    ADD `import_pk` VARCHAR(45) NULL AFTER `os_pk`;
ALTER TABLE et_ratings_temp.`people`
    ADD UNIQUE `UN2_people` (`tenant_id`, `import_pk`);

ALTER TABLE et_ratings_temp.`legislative_sessions`
    DROP INDEX `XUN1_legislative_sessions`,
    ADD UNIQUE `XUN1_legislative_sessions` (`tenant_id`, `name`, `session_type`, `special_session_number`) USING BTREE;

-- remove index breaking default chamber logic
ALTER TABLE et_ratings_temp.`chambers`
    DROP INDEX `XUN3_chambers`;

-- add / remove unique tables

drop table et_ratings_temp.cake_sessions;

create table et_ratings_temp.phinxlog
(
    version        bigint               not null
        primary key,
    migration_name varchar(100)         null,
    start_time     timestamp            null,
    end_time       timestamp            null,
    breakpoint     tinyint(1) default 0 not null
);

create table et_ratings_temp.sessions
(
    id       char(40) collate ascii_bin         not null
        primary key,
    created  datetime default CURRENT_TIMESTAMP null,
    modified datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
    data     blob                               null,
    expires  int unsigned                       null
);

DROP VIEW IF EXISTS et_ratings_temp.v_chambers;
DROP VIEW IF EXISTS et_ratings_temp.v_max_session;
DROP VIEW IF EXISTS et_ratings_temp.v_parties;

ALTER TABLE `et_ratings_temp`.`chambers`
    ADD UNIQUE `XUN3_chambers` (`tenant_id`, `is_default`) USING BTREE;

--
-- 1789 March changes
--

-- add state to tenants
ALTER TABLE et_ratings_temp.`tenants`
    ADD `state` VARCHAR(2) NULL AFTER `id_public`;

UPDATE et_ratings_temp.`tenants`
SET `state` = 'TX';

-- rename people columns
ALTER TABLE et_ratings_temp.`people`
    CHANGE `import_pk` `state_pk` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

UPDATE et_ratings_temp.`people` p
SET `state_pk` = `lrl_pk`
WHERE `lrl_pk` IS NOT NULL;

ALTER TABLE et_ratings_temp.`people`
    DROP `lrl_pk`;

ALTER TABLE et_ratings_temp.`legislative_sessions`
    CHANGE `lrl_pk` `state_pk` CHAR(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE et_ratings_temp.`legislative_sessions`
    CHANGE `state_pk` `state_pk` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- change addresses
ALTER TABLE `et_ratings_temp`.`addresses`
    ADD COLUMN `capitol_address` varchar(100) NULL after `address_type_id`;

ALTER TABLE `et_ratings_temp`.`addresses`
    ADD COLUMN `full_address` text NULL after `capitol_address`;

ALTER TABLE `et_ratings_temp`.`addresses`
    ADD COLUMN `fax` varchar(15) NULL NULL after `phone`;

-- move capitol address from line 1
UPDATE et_ratings_temp.addresses
SET capitol_address = address_line_1,
    address_line_1  = address_line_2,
    address_line_2  = null
WHERE id IN
      (
          SELECT a.id
          FROM (SELECT * FROM et_ratings_temp.addresses) a
                   INNER JOIN et_ratings_temp.address_types at
                              ON a.address_type_id = at.id
          WHERE at.name = 'Capitol'
            AND (a.address_line_1 LIKE 'CAP%'
              OR a.address_line_1 LIKE 'EXT%')
      );

-- combine individual components into full address
UPDATE et_ratings_temp.addresses
SET full_address   = REPLACE(
        CONCAT(address_line_1, '<br>', ifnull(address_line_2, ''), '<br>', city, ', ', state, ' ', zip)
    , '<br><br>'
    , '<br>'),
    address_line_1 = null,
    address_line_2 = null,
    city           = null,
    state          = null,
    zip            = null;


UPDATE et_ratings_temp.people
SET image_url = replace(image_url, 'media/', '');

UPDATE et_ratings_temp.people
SET image_url = 'Silhouette_Placeholder.jpg'
WHERE image_url LIKE '%Silhouette_Placeholder.png';

-- update rating_types.sort_order
ALTER TABLE et_ratings_temp.`rating_types`
    ADD `sort_order` INT(11) NULL AFTER `th_vote_name`;

UPDATE et_ratings_temp.`rating_types`
SET sort_order = 1
WHERE name = 'Vote';

UPDATE et_ratings_temp.`rating_types`
SET sort_order = 2
WHERE name = 'Sponsorship';

UPDATE et_ratings_temp.`rating_types`
SET sort_order = 3
WHERE name = 'Leadership';

-- update ratings.sort_order
UPDATE et_ratings_temp.`ratings`
SET sort_order = sort_order - 100
WHERE sort_order < 300
  AND sort_order > 100
  AND sort_order IS NOT NULL;

UPDATE et_ratings_temp.`ratings`
SET sort_order = sort_order - 200 + 3
WHERE sort_order < 400
  AND sort_order > 300
  AND sort_order IS NOT NULL;

UPDATE et_ratings_temp.`ratings`
SET sort_order = sort_order - 400
WHERE sort_order < 500
  AND sort_order > 400
  AND sort_order IS NOT NULL;

UPDATE et_ratings_temp.`ratings`
SET sort_order = sort_order - 500
WHERE sort_order < 600
  AND sort_order > 500
  AND sort_order IS NOT NULL;

UPDATE et_ratings_temp.`ratings`
SET sort_order = sort_order - 520
WHERE sort_order < 700
  AND sort_order > 600
  AND sort_order IS NOT NULL;

UPDATE et_ratings_temp.`ratings`
SET sort_order = sort_order - 700
WHERE sort_order < 800
  AND sort_order > 700
  AND sort_order IS NOT NULL;

--
-- Table structure for table `action_types`
--

CREATE TABLE et_ratings_temp.`action_types`
(
    `id`              char(36) NOT NULL,
    `tenant_id`       char(36) NOT NULL,
    `name`            char(50) NOT NULL,
    `icon_text`       varchar(10)       DEFAULT NULL,
    `css_color`       char(7)  NOT NULL,
    `sort_order`      int(11)  NOT NULL,
    `evaluation_code` char(1)  NOT NULL DEFAULT 'N',
    `created`         datetime          DEFAULT NULL,
    `modified`        datetime          DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE et_ratings_temp.`action_types`
    ADD PRIMARY KEY (`id`);

ALTER TABLE et_ratings_temp.`action_types`
    ADD CONSTRAINT `XFK1_action_types` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

-- populate with existing data
INSERT INTO et_ratings_temp.action_types
SELECT uuid()                  as id,
       t.id                    as tenant_id,
       results.name            as name,
       results.icon_text       as icon_text,
       results.css_color       as css_color,
       results.sort_order      as sort_order,
       results.evaluation_code as evaluation_code,
       NOW()                   as created,
       NOW()                   as modified
FROM (
         SELECT 'E'                  as evaluation_code,
                'Stance Evaluation*' as name,
                ''                   as icon_text,
                ''                   as css_color,
                '0'                  as sort_order
         UNION ALL
         SELECT 'F'       as evaluation_code,
                'Favor'   as name,
                ''        as icon_text,
                '#8ebd4e' as css_color,
                '1'       as sort_order
         UNION ALL
         SELECT 'O'       as evaluation_code,
                'Oppose'  as name,
                ''        as icon_text,
                '#a5290d' as css_color,
                '2'       as sort_order
         UNION ALL
         SELECT 'N'       as evaluation_code,
                'Absent'  as name,
                'Absent'  as icon_text,
                '#251815' as css_color,
                '3'       as sort_order
         UNION ALL
         SELECT 'N'       as evaluation_code,
                'Speaker' as name,
                'Speaker' as icon_text,
                '#3c4485' as css_color,
                '4'       as sort_order
         UNION ALL
         SELECT 'N'       as evaluation_code,
                'Chair'   as name,
                'Chair'   as icon_text,
                '#251815' as css_color,
                '5'       as sort_order
         UNION ALL
         SELECT 'N'               as evaluation_code,
                'Absent, Excused' as name,
                'A-EX'            as icon_text,
                '#7a7a7a'         as css_color,
                '6'               as sort_order
         UNION ALL
         SELECT 'N'                   as evaluation_code,
                'Present, not voting' as name,
                'PNV'                 as icon_text,
                '#afafaf'             as css_color,
                '7'                   as sort_order
         UNION ALL
         SELECT 'N'              as evaluation_code,
                'Not Applicable' as name,
                'N/A'            as icon_text,
                '#7a7a7a'        as css_color,
                '8'              as sort_order
     ) results
         CROSS JOIN et_ratings_temp.tenants t;

-- add foreign key field to legislators
ALTER TABLE et_ratings_temp.actions
    ADD COLUMN action_type_id char(36) NULL AFTER rating_type_id;

-- populate foreign key field for legislators
UPDATE et_ratings_temp.actions a
    INNER JOIN et_ratings_temp.action_types at ON at.evaluation_code = 'E'
SET a.action_type_id = at.id
WHERE a.type IN ('Y', 'N');

UPDATE et_ratings_temp.actions a
    INNER JOIN et_ratings_temp.action_types at ON at.evaluation_code = 'N' AND at.name = 'Not Applicable'
SET a.action_type_id = at.id
WHERE a.type NOT IN ('Y', 'N');

-- add foreign key
ALTER TABLE et_ratings_temp.actions
    ADD CONSTRAINT `XFK3_actions` FOREIGN KEY (`action_type_id`) REFERENCES `action_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- drop original type column
ALTER TABLE et_ratings_temp.actions
    DROP `type`;

-- add json field
ALTER TABLE et_ratings_temp.ratings
    ADD `json` JSON NULL AFTER `slug`;

--
-- May 2019 tweaks
--

ALTER TABLE et_ratings_temp.`positions`
    ADD `display_code` char(1) NOT NULL DEFAULT 'N' after `name`;

UPDATE et_ratings_temp.`positions`
SET `display_code` = 'F'
WHERE `name` LIKE 'Support';

UPDATE et_ratings_temp.`positions`
SET `display_code` = 'O'
WHERE `name` LIKE 'Oppose';

UPDATE et_ratings_temp.`positions`
SET `display_code` = 'F'
WHERE `name` LIKE '%(Support%';

UPDATE et_ratings_temp.`positions`
SET `display_code` = 'O'
WHERE `name` LIKE '%(Oppose%';

ALTER TABLE et_ratings_temp.`parties`
    ADD `css_color` char(7) NULL AFTER `abbreviation`;

UPDATE et_ratings_temp.`parties`
SET `css_color` = '#800000'
WHERE `abbreviation` = 'R';

UPDATE et_ratings_temp.`parties`
SET `css_color` = '#000080'
WHERE `abbreviation` = 'D';

UPDATE et_ratings_temp.`parties`
SET `css_color` = '#808080'
WHERE `abbreviation` = 'U';

ALTER TABLE et_ratings_temp.`parties`
    CHANGE `css_color` `css_color` CHAR(7) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

UPDATE et_ratings_temp.`positions`
SET `display_code` = 'F'
WHERE `name` LIKE 'Support';

UPDATE et_ratings_temp.`positions`
SET `display_code` = 'O'
WHERE `name` LIKE 'Oppose';

UPDATE et_ratings_temp.`positions`
SET `display_code` = 'F'
WHERE `name` LIKE '%(Support%';

UPDATE et_ratings_temp.`positions`
SET `display_code` = 'O'
WHERE `name` LIKE '%(Oppose%';

UPDATE et_ratings_temp.`parties`
SET `css_color` = '#800000'
WHERE `abbreviation` = 'R';

UPDATE et_ratings_temp.`parties`
SET `css_color` = '#000080'
WHERE `abbreviation` = 'D';

UPDATE et_ratings_temp.`parties`
SET `css_color` = '#808080'
WHERE `abbreviation` = 'U';

ALTER TABLE et_ratings_temp.`parties`
    CHANGE `css_color` `css_color` CHAR(7) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

--
-- Jun - Dec 2019 tweaks
--

--
-- 2019-05-11 patch
--
ALTER TABLE et_ratings_temp.`action_types`
    ADD `export_text` VARCHAR(5) NULL AFTER `icon_text`;

--
-- 2019-06-04 patch
--
ALTER TABLE et_ratings_temp.`people`
    DROP `ttx_filer_name`;

ALTER TABLE et_ratings_temp.`people`
    ADD `ttx_candidate_slug` VARCHAR(255) NULL DEFAULT NULL AFTER `ttx_filer_slug`;

UPDATE et_ratings_temp.`people` p
    INNER JOIN
    (
        SELECT p.id, c.slug as candidateSlug
        FROM et_ratings_temp.people p
                 INNER JOIN tusa_app_tx.filers f ON p.ttx_filer_slug = f.slug
                 LEFT JOIN tusa_app_tx.candidates c ON f.candidateId = c.id
        WHERE p.ttx_filer_slug IS NOT NULL
    ) results ON p.id = results.id
SET p.ttx_candidate_slug = results.candidateSlug
WHERE p.ttx_filer_slug IS NOT NULL;

SELECT ttx_filer_slug, ttx_candidate_slug
FROM et_ratings_temp.people
WHERE ttx_candidate_slug IS NOT NULL;

ALTER TABLE et_ratings_temp.`people`
    DROP `ttx_filer_slug`;

--
-- 2019-07-02 patch
--
ALTER TABLE et_ratings_temp.`grades`
    ADD `lower_range` SMALLINT NOT NULL AFTER `sort_order`;

ALTER TABLE et_ratings_temp.`grades`
    ADD `upper_range` SMALLINT NOT NULL AFTER `lower_range`;

UPDATE et_ratings_temp.grades
SET lower_range = 0,
    upper_range = 50
WHERE name = 'F-';
UPDATE et_ratings_temp.grades
SET lower_range = 51,
    upper_range = 59
WHERE name = 'F';
UPDATE et_ratings_temp.grades
SET lower_range = 60,
    upper_range = 64
WHERE name = 'F+';
UPDATE et_ratings_temp.grades
SET lower_range = 65,
    upper_range = 68
WHERE name = 'D-';
UPDATE et_ratings_temp.grades
SET lower_range = 69,
    upper_range = 71
WHERE name = 'D';
UPDATE et_ratings_temp.grades
SET lower_range = 72,
    upper_range = 74
WHERE name = 'D+';
UPDATE et_ratings_temp.grades
SET lower_range = 75,
    upper_range = 77
WHERE name = 'C-';
UPDATE et_ratings_temp.grades
SET lower_range = 78,
    upper_range = 80
WHERE name = 'C';
UPDATE et_ratings_temp.grades
SET lower_range = 81,
    upper_range = 83
WHERE name = 'C+';
UPDATE et_ratings_temp.grades
SET lower_range = 84,
    upper_range = 86
WHERE name = 'B-';
UPDATE et_ratings_temp.grades
SET lower_range = 87,
    upper_range = 89
WHERE name = 'B';
UPDATE et_ratings_temp.grades
SET lower_range = 90,
    upper_range = 92
WHERE name = 'B+';
UPDATE et_ratings_temp.grades
SET lower_range = 93,
    upper_range = 95
WHERE name = 'A-';
UPDATE et_ratings_temp.grades
SET lower_range = 95,
    upper_range = 97
WHERE name = 'A';
UPDATE et_ratings_temp.grades
SET lower_range = 98,
    upper_range = 100
WHERE name = 'A+';

--
-- 2019-08-03 patch
--
ALTER TABLE et_ratings_temp.`people`
    ADD `email` VARCHAR(100) NULL AFTER `state_pk`;

--
-- 2019-11-14 patch
--


--
-- Table structure for table `configuration_options`
--

CREATE TABLE `configuration_options`
(
    `id`                    char(36)     NOT NULL,
    `configuration_type_id` char(36)     NOT NULL,
    `name`                  varchar(100) NOT NULL,
    `value`                 longtext,
    `sort_order`            int(11)      NOT NULL,
    `created`               datetime DEFAULT NULL,
    `modified`              datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `configuration_options`
--
ALTER TABLE `configuration_options`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_configuration_options` (`configuration_type_id`, `name`) USING BTREE,
    ADD KEY `XIE1_configuration_options` (`configuration_type_id`) USING BTREE;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `configuration_options`
--
ALTER TABLE `configuration_options`
    ADD CONSTRAINT `XFK1_configuration_options` FOREIGN KEY (`configuration_type_id`) REFERENCES `configuration_types` (`id`);

--
-- 2019-11-15 patch
--
ALTER TABLE et_ratings_temp.`action_types`
    ADD `score_type` CHAR(1) NOT NULL DEFAULT 'Z' AFTER `evaluation_code`;

UPDATE et_ratings_temp.action_types
SET score_type = 'Z';

UPDATE et_ratings_temp.action_types
SET score_type = 'P'
WHERE evaluation_code = 'F';

UPDATE et_ratings_temp.action_types
SET score_type = 'N'
WHERE evaluation_code = 'O';

UPDATE et_ratings_temp.ratings
SET rating_weight = 1
where rating_weight = 0;

--
-- 2019-12-10 patch
--
ALTER TABLE et_ratings_temp.`rating_types`
    ADD `method` CHAR(1) NOT NULL DEFAULT 'S' AFTER `th_vote_name`;

-- =============================================
--
-- 2020 Changes
--
-- =============================================

-- --------------------------------------------------------

--
-- 2020-09-21-texas-directory.sql
--

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE et_ratings_temp.`organizations`
(
    `id`            char(36)     NOT NULL,
    `tenant_id`     char(36)     NOT NULL,
    `org_tenant_id` char(36)     DEFAULT NULL,
    `name`          varchar(100) NOT NULL,
    `description`   text,
    `website`       varchar(100) DEFAULT NULL,
    `image_url`     varchar(100) DEFAULT NULL,
    `sort_order`    int(11)      DEFAULT NULL,
    `created`       datetime     DEFAULT NULL,
    `modified`      datetime     DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `organizations`
--
ALTER TABLE et_ratings_temp.`organizations`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_tenant_id` (`tenant_id`),
    ADD KEY `FK_scorecard_id` (`org_tenant_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `organizations`
--
ALTER TABLE et_ratings_temp.`organizations`
    ADD CONSTRAINT `XFK1_organizations` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`),
    ADD CONSTRAINT `XFK2_organizations` FOREIGN KEY (`org_tenant_id`) REFERENCES `tenants` (`id`);

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE et_ratings_temp.`scores`
(
    `id`              char(36) NOT NULL,
    `tenant_id`       char(36) NOT NULL,
    `legislator_id`   char(36) NOT NULL,
    `organization_id` char(36) NOT NULL,
    `display_text`    varchar(10) DEFAULT NULL,
    `created`         datetime    DEFAULT NULL,
    `modified`        datetime    DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `scores`
--
ALTER TABLE et_ratings_temp.`scores`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_scores` (`tenant_id`, `legislator_id`, `organization_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`),
    ADD KEY `FK_legislator_id` (`legislator_id`),
    ADD KEY `FK_organization_id` (`organization_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `scores`
--
ALTER TABLE et_ratings_temp.`scores`
    ADD CONSTRAINT `XFK1_scores` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK2_scores` FOREIGN KEY (`legislator_id`) REFERENCES `legislators` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK3_scores` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE;

-- --------------------------------------------------------

--
-- 2020-09-23-chambers-order.sql
--

-- --------------------------------------------------------

ALTER TABLE et_ratings_temp.`chambers`
    ADD COLUMN `sort_order` int(11) DEFAULT NULL after `is_default`;

UPDATE et_ratings_temp.`chambers`
SET chambers.sort_order = 1
WHERE type = 'L';

UPDATE et_ratings_temp.`chambers`
SET chambers.sort_order = 2
WHERE type = 'U';

UPDATE et_ratings_temp.`chambers`
SET chambers.sort_order = 3
WHERE type = 'C';

-- --------------------------------------------------------

--
-- 2020-09-23-jpm-auth-upgrade.sql
--

-- --------------------------------------------------------

ALTER TABLE et_ratings_temp.`accounts`
    ADD COLUMN `security_code` varchar(255) DEFAULT NULL after `status`;

-- --------------------------------------------------------

--
-- 2020-09-23-tenant-type.sql
--

-- --------------------------------------------------------

alter table et_ratings_temp.`tenants`
    add type char default 'S' not null after state;

alter table et_ratings_temp.`tenants`
    add layout varchar(50) default 'default' not null after type;

ALTER TABLE et_ratings_temp.`chambers`
    DROP INDEX `XUN3_chambers`;

-- --------------------------------------------------------

--
-- 2020-10-09-configurations.sql
--

-- --------------------------------------------------------

-- Add is_public column to hide config values from the client.
alter table et_ratings_temp.`configuration_types`
    add is_public tinyint(1) default 1 not null after is_enabled;

-- --------------------------------------------------------

--
-- 2020-11-02 committee fields.sql
--

-- --------------------------------------------------------

alter table et_ratings_temp.`people`
    add column `committee_list` longtext after `bio`;

-- TODO evaluate effectiveness
update et_ratings_temp.`people`
set people.committee_list = people.bio,
    people.bio            = null
where people.bio like '<u>Committee Assignments</u><br />%';

update et_ratings_temp.`people`
set people.committee_list = replace(people.committee_list, '<u>Committee Assignments</u><br />', '');

-- --------------------------------------------------------

--
-- 2020-11-19-tenant-sync.sql
--

-- --------------------------------------------------------

ALTER TABLE et_ratings_temp.`tenants`
    ADD `sync_pending` DATETIME NULL DEFAULT NULL AFTER `layout`;

-- --------------------------------------------------------

--
-- Misc data changes
--

-- --------------------------------------------------------

UPDATE et_ratings_temp.`people`
SET ballotpedia_url = replace(ballotpedia_url, 'http://', 'https://');

UPDATE et_ratings_temp.`people`
SET ballotpedia_url = replace(ballotpedia_url, '/wiki/index.php', '');

-- --------------------------------------------------------

--
-- Misc schema changes to match production
--

-- --------------------------------------------------------

DROP TABLE et_ratings_temp.`phinxlog`;

CREATE TABLE et_ratings_temp.`phinxlog`
(
    `version`        bigint(20)   NOT NULL,
    `migration_name` varchar(100) NULL,
    `start_time`     timestamp    NULL,
    `end_time`       timestamp    NULL,
    `breakpoint`     tinyint(1)   NOT NULL DEFAULT 0,
    CONSTRAINT `PRIMARY` PRIMARY KEY (`version`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE et_ratings_temp.`addresses`
    DROP INDEX `XUN1_addresses`;

CREATE INDEX `FK_tenant_id` ON et_ratings_temp.`action_types` (`tenant_id`);

DROP TABLE et_ratings_temp.`sessions`;

CREATE TABLE et_ratings_temp.`sessions`
(
    `id`       char(40) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
    `created`  datetime         DEFAULT CURRENT_TIMESTAMP,
    `modified` datetime         DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `data`     blob,
    `expires`  int(10) UNSIGNED DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE et_ratings_temp.`sessions`
    ADD PRIMARY KEY (`id`);

CREATE TABLE et_ratings_temp.`rating_fields`
(
    `id`         char(36)     NOT NULL,
    `tenant_id`  char(36)     NOT NULL,
    `name`       varchar(45)  NOT NULL,
    `json_path`  varchar(255) NOT NULL,
    `type`       char(1)      NOT NULL,
    `sort_order` int(11)      NULL,
    `created`    datetime     NULL,
    `modified`   datetime     NULL,
    CONSTRAINT `PRIMARY` PRIMARY KEY (`id`),
    CONSTRAINT `XUN1_rating_fields` UNIQUE KEY `XUN1_rating_fields` (`tenant_id`, `name`),
    CONSTRAINT `XFK1_rating_fields` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`),
    KEY `FK_tenant_id` (`tenant_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;