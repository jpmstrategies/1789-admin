use `1789_cornerstone`;

alter table tenants_bills
    add column `notes` text null after `priority_id`;

drop table rating_criterias;

create table rating_criterias
(
    id                 char(36) not null primary key,
    tenant_id          char(36) not null,
    legiscan_bill_id   mediumint unsigned not null,
    criteria_id        char(36) not null,
    criteria_detail_id char(36) null,
    action_id          char(36) null,
    constraint XUN1_rating_criterias
        unique (legiscan_bill_id, criteria_id),
    constraint XFK1_rating_criterias
        foreign key (criteria_id) references criterias (id),
    constraint XFK2_rating_criterias
        foreign key (action_id) references actions (id),
    constraint XFK3_rating_criterias
        foreign key (tenant_id) references tenants (id),
    constraint XFK4_rating_criterias
        foreign key (criteria_detail_id) references criteria_details (id)
) charset = utf8mb3;