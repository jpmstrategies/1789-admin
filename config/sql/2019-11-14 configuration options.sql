-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 14, 2019 at 04:41 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `1789_cornerstone`
--

-- --------------------------------------------------------

--
-- Table structure for table `configuration_options`
--

CREATE TABLE `configuration_options`
(
    `id`                    char(36)     NOT NULL,
    `configuration_type_id` char(36)     NOT NULL,
    `name`                  varchar(100) NOT NULL,
    `value`                 longtext,
    `sort_order`            int(11)      NOT NULL,
    `created`               datetime DEFAULT NULL,
    `modified`              datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `configuration_options`
--
ALTER TABLE `configuration_options`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_configuration_options` (`configuration_type_id`, `name`) USING BTREE,
    ADD KEY `XIE1_configuration_options` (`configuration_type_id`) USING BTREE;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `configuration_options`
--
ALTER TABLE `configuration_options`
    ADD CONSTRAINT `XFK1_configuration_options` FOREIGN KEY (`configuration_type_id`) REFERENCES `configuration_types` (`id`);

--
-- Dumping data for table `configuration_options`
--

INSERT INTO `configuration_options` (`id`, `configuration_type_id`, `name`, `value`, `sort_order`, `created`,
                                     `modified`)
VALUES ('0a81843c-f004-421c-b409-e53eb491a618', '323f1c21-8fbd-439b-a958-d32e8371de80', 'Medium (72px)', 'medium', 2,
        '2019-11-14 17:39:46', '2019-11-14 17:39:46'),
       ('0c10203b-1799-48e9-97cb-8bacedc50908', '2f5d8d4f-a6b1-4955-a25b-6d3f8dc00e8c', 'Thumbs Up / Thumbs Down',
        'thumbs', 1, '2019-11-14 22:22:26', '2019-11-14 22:23:15'),
       ('1c37b92d-9bc9-4565-b902-c08a19268d4e', 'ee24eff3-43d3-46f4-b45e-4aa89d479e93', 'District ASC (A-Z)',
        'district', 1, '2019-11-14 22:02:33', '2019-11-14 23:01:36'),
       ('1f820f0b-b6be-4c55-81be-1bc8f0187619', '2a4d38a6-4808-4698-830a-aec60da05080', 'Medium (72px)', 'medium', 2,
        '2019-11-14 21:55:08', '2019-11-14 21:55:08'),
       ('29473b30-7565-473b-9d29-0abecd89f388', 'ee24eff3-43d3-46f4-b45e-4aa89d479e93', 'District DESC (Z-A)',
        '-district', 2, '2019-11-14 22:02:49', '2019-11-14 23:01:42'),
       ('295db86d-87e4-4c05-80d4-94d029645c01', 'ee24eff3-43d3-46f4-b45e-4aa89d479e93', 'Score DESC (Z-A)', '-score', 8,
        '2019-11-14 22:03:29', '2019-11-14 22:03:29'),
       ('386a3d17-c269-42ba-9cc9-1ac62277896d', '7dfe182a-c635-4679-bc72-5932750e1177', 'Evaluated Legislator Vote',
        'evaluation', 2, '2019-11-14 22:24:13', '2019-11-14 22:24:13'),
       ('3d206781-fef1-4111-9264-78abd6fb9782', 'ee24eff3-43d3-46f4-b45e-4aa89d479e93', 'Party ASC (A-Z)', 'party', 5,
        '2019-11-14 22:02:59', '2019-11-14 22:02:59'),
       ('3d9e10c1-7fdd-4d4f-b515-2950e13c2ed2', 'ee24eff3-43d3-46f4-b45e-4aa89d479e93', 'Party DESC (Z-A)', '-party', 6,
        '2019-11-14 22:03:10', '2019-11-14 22:03:10'),
       ('42e9516c-1135-4c72-906d-71d9a32e11c5', 'f1d3b2bc-01ba-478c-87c3-f3a43b40dc29', 'Small (46px)', 'small', 1,
        '2019-11-14 21:54:27', '2019-11-14 21:54:27'),
       ('4a3030cb-444e-4600-bad5-af45f6e682b1', '7dfe182a-c635-4679-bc72-5932750e1177', 'Actual Legislator Vote',
        'actual', 1, '2019-11-14 22:24:06', '2019-11-14 22:24:06'),
       ('4f69a1e8-f159-403c-9100-940c7e46a666', 'db081ea1-50f1-4581-bccb-0efd0cf94933', 'Medium (72px)', 'medium', 2,
        '2019-11-14 21:55:38', '2019-11-14 21:55:38'),
       ('530f0f5d-efce-4e66-b2f4-60ec03e2e6df', 'db081ea1-50f1-4581-bccb-0efd0cf94933', 'Large (160px)', 'large', 3,
        '2019-11-14 21:55:45', '2019-11-14 21:55:45'),
       ('6262301b-87a3-40c9-ad5d-0ff12476550a', 'ee24eff3-43d3-46f4-b45e-4aa89d479e93', 'Score ASC (A-Z)', 'score', 7,
        '2019-11-14 22:03:20', '2019-11-14 22:03:20'),
       ('702db9bd-d5de-4e6a-9861-8027ef921621', '323f1c21-8fbd-439b-a958-d32e8371de80', 'Large (160px)', 'large', 3,
        '2019-11-14 17:39:58', '2019-11-14 17:39:58'),
       ('70d6a55d-1eca-4949-8272-350313b128f2', 'f1d3b2bc-01ba-478c-87c3-f3a43b40dc29', 'Large (160px)', 'large', 3,
        '2019-11-14 21:54:47', '2019-11-14 21:54:47'),
       ('73c014d9-8280-4e91-a0e1-094c587cdbde', 'ee24eff3-43d3-46f4-b45e-4aa89d479e93', 'Last Name DESC (Z-A)',
        '-lastName', 4, '2019-11-14 22:02:19', '2019-11-14 22:02:19'),
       ('b128ef24-2196-4a02-9c8b-d0bc0dce40a5', 'f1d3b2bc-01ba-478c-87c3-f3a43b40dc29', 'Medium (72px)', 'medium', 2,
        '2019-11-14 21:54:37', '2019-11-14 21:54:37'),
       ('bfd8f1b7-e89b-40ae-9583-d0df256c22f2', '2f5d8d4f-a6b1-4955-a25b-6d3f8dc00e8c', 'Check / X within Ovals',
        'ovals', 2, '2019-11-14 22:22:15', '2019-11-14 22:23:06'),
       ('cd9dc341-3c01-45c2-b78e-180856f1b70f', 'db081ea1-50f1-4581-bccb-0efd0cf94933', 'Small (46px)', 'small', 1,
        '2019-11-14 21:55:29', '2019-11-14 21:55:29'),
       ('d6579239-0f31-46d9-ab7d-50d0fd3a2929', '323f1c21-8fbd-439b-a958-d32e8371de80', 'Small (46px)', 'small', 1,
        '2019-11-14 17:38:27', '2019-11-14 17:41:46'),
       ('ec8a3ef9-2172-45e2-b031-8b1866f06327', '2a4d38a6-4808-4698-830a-aec60da05080', 'Large (160px)', 'large', 3,
        '2019-11-14 21:55:17', '2019-11-14 21:55:17'),
       ('ee2aefaa-6f5f-4e6e-8956-f45e169cd787', 'ee24eff3-43d3-46f4-b45e-4aa89d479e93', 'Last Name ASC (A-Z)',
        'lastName', 3, '2019-11-14 22:02:09', '2019-11-14 22:02:09'),
       ('fbeb7987-aef5-4341-9025-abf0e4cdff07', '2a4d38a6-4808-4698-830a-aec60da05080', 'Small (46px)', 'small', 1,
        '2019-11-14 21:54:57', '2019-11-14 21:54:57');

--
-- Dumping data for table `configuration_types`
--

UPDATE `configuration_types`
SET `id`            = '2a4d38a6-4808-4698-830a-aec60da05080',
    `name`          = 's3_image_size_lookup',
    `description`   = 'Legislator image size for the Find My Legislator page',
    `data_type`     = 'select',
    `default_value` = 'large',
    `tab`           = 'site',
    `is_editable`   = 1,
    `is_enabled`    = 1,
    `created`       = '2019-09-03 23:00:01',
    `modified`      = '2019-11-14 22:19:52'
WHERE `configuration_types`.`id` = '2a4d38a6-4808-4698-830a-aec60da05080';
UPDATE `configuration_types`
SET `id`            = '2f5d8d4f-a6b1-4955-a25b-6d3f8dc00e8c',
    `name`          = 'position_display_appearance',
    `description`   = 'Icon type for the visual display shown to the user',
    `data_type`     = 'select',
    `default_value` = 'ovals',
    `tab`           = 'display',
    `is_editable`   = 1,
    `is_enabled`    = 1,
    `created`       = '2019-07-15 16:34:51',
    `modified`      = '2019-11-14 22:25:17'
WHERE `configuration_types`.`id` = '2f5d8d4f-a6b1-4955-a25b-6d3f8dc00e8c';
UPDATE `configuration_types`
SET `id`            = '323f1c21-8fbd-439b-a958-d32e8371de80',
    `name`          = 's3_image_size_home',
    `description`   = 'Legislator image size for the Home page',
    `data_type`     = 'select',
    `default_value` = 'medium',
    `tab`           = 'home',
    `is_editable`   = 1,
    `is_enabled`    = 1,
    `created`       = '2019-09-03 22:51:45',
    `modified`      = '2019-11-14 22:20:56'
WHERE `configuration_types`.`id` = '323f1c21-8fbd-439b-a958-d32e8371de80';
UPDATE `configuration_types`
SET `id`            = '7abfad71-dd4e-4fb9-b8e3-58e99a23b4f7',
    `name`          = 'use_custom_favicon',
    `description`   = 'Use Custom Favicon (uploaded to S3)',
    `data_type`     = 'checkbox',
    `default_value` = 'no',
    `tab`           = 'admin',
    `is_editable`   = 1,
    `is_enabled`    = 1,
    `created`       = '2019-09-04 02:30:19',
    `modified`      = '2019-11-14 23:45:33'
WHERE `configuration_types`.`id` = '7abfad71-dd4e-4fb9-b8e3-58e99a23b4f7';
UPDATE `configuration_types`
SET `id`            = '7dfe182a-c635-4679-bc72-5932750e1177',
    `name`          = 'position_display_type',
    `description`   = 'Legislator Vote type to display',
    `data_type`     = 'select',
    `default_value` = 'evaluation',
    `tab`           = 'display',
    `is_editable`   = 1,
    `is_enabled`    = 1,
    `created`       = '2019-08-01 05:24:58',
    `modified`      = '2019-11-14 22:25:49'
WHERE `configuration_types`.`id` = '7dfe182a-c635-4679-bc72-5932750e1177';
UPDATE `configuration_types`
SET `id`            = 'db081ea1-50f1-4581-bccb-0efd0cf94933',
    `name`          = 's3_image_size_vote',
    `description`   = 'Legislator image size for the Vote page',
    `data_type`     = 'select',
    `default_value` = 'small',
    `tab`           = 'vote',
    `is_editable`   = 1,
    `is_enabled`    = 1,
    `created`       = '2019-09-03 22:52:05',
    `modified`      = '2019-11-14 22:20:45'
WHERE `configuration_types`.`id` = 'db081ea1-50f1-4581-bccb-0efd0cf94933';
UPDATE `configuration_types`
SET `id`            = 'ee24eff3-43d3-46f4-b45e-4aa89d479e93',
    `name`          = 'homepage_legislator_sort',
    `description`   = 'Column sorting for the homepage legislator table',
    `data_type`     = 'select',
    `default_value` = '-score',
    `tab`           = 'home',
    `is_editable`   = 1,
    `is_enabled`    = 1,
    `created`       = '2019-07-15 16:32:12',
    `modified`      = '2019-11-14 22:18:31'
WHERE `configuration_types`.`id` = 'ee24eff3-43d3-46f4-b45e-4aa89d479e93';
UPDATE `configuration_types`
SET `id`            = 'f1d3b2bc-01ba-478c-87c3-f3a43b40dc29',
    `name`          = 's3_image_size_legislator',
    `description`   = 'Legislator image size for the Legislator Profile page',
    `data_type`     = 'select',
    `default_value` = 'large',
    `tab`           = 'legislator',
    `is_editable`   = 1,
    `is_enabled`    = 1,
    `created`       = '2019-09-03 22:52:35',
    `modified`      = '2019-11-14 22:19:35'
WHERE `configuration_types`.`id` = 'f1d3b2bc-01ba-478c-87c3-f3a43b40dc29';
