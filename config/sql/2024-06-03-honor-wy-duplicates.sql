-- use this to clean up duplicate legislators from LegiScan sync process

-- review results
select *
from legislators l
         inner join people p on l.person_id = p.id
where l.tenant_id = '15629c88-a9cc-44a3-9cd7-69701d845f6b'
  and l.chamber_id = '21420487-def4-11ed-aea2-0242ac120003'
  and l.legislative_session_id = '7d3f3d6f-ebbf-11ee-b0c7-06e09ebca71f'
  and
    p.last_name in ('baldwin', 'barlow', 'biteman', 'furphy', 'gierau', 'hutchings', 'laursen', 'salazar', 'steinmetz');

-- delete duplicates
delete l
from legislators l
         inner join people p on l.person_id = p.id
where l.tenant_id = '15629c88-a9cc-44a3-9cd7-69701d845f6b'
  and l.chamber_id = '21420487-def4-11ed-aea2-0242ac120003'
  and l.legislative_session_id = '7d3f3d6f-ebbf-11ee-b0c7-06e09ebca71f'
  and
    p.last_name in ('baldwin', 'barlow', 'biteman', 'furphy', 'gierau', 'hutchings', 'laursen', 'salazar', 'steinmetz');
