use 1789_cornerstone;

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations`
(
    `id`            char(36)     NOT NULL,
    `tenant_id`     char(36)     NOT NULL,
    `org_tenant_id` char(36)     DEFAULT NULL,
    `name`          varchar(100) NOT NULL,
    `description`   text,
    `website`       varchar(100) DEFAULT NULL,
    `image_url`     varchar(100) DEFAULT NULL,
    `sort_order`    int(11)      DEFAULT NULL,
    `created`       datetime     DEFAULT NULL,
    `modified`      datetime     DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_tenant_id` (`tenant_id`),
    ADD KEY `FK_scorecard_id` (`org_tenant_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
    ADD CONSTRAINT `XFK1_organizations` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`),
    ADD CONSTRAINT `XFK2_organizations` FOREIGN KEY (`org_tenant_id`) REFERENCES `tenants` (`id`);

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores`
(
    `id`              char(36) NOT NULL,
    `tenant_id`       char(36) NOT NULL,
    `legislator_id`   char(36) NOT NULL,
    `organization_id` char(36) NOT NULL,
    `display_text`    varchar(10) DEFAULT NULL,
    `created`         datetime    DEFAULT NULL,
    `modified`        datetime    DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_scores` (`tenant_id`, `legislator_id`, `organization_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`),
    ADD KEY `FK_legislator_id` (`legislator_id`),
    ADD KEY `FK_organization_id` (`organization_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `scores`
--
ALTER TABLE `scores`
    ADD CONSTRAINT `XFK1_scores` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK2_scores` FOREIGN KEY (`legislator_id`) REFERENCES `legislators` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK3_scores` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE;
