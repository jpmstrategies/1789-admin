-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 04, 2019 at 12:16 PM
-- Server version: 5.7.25
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `1789_cornerstone`
--

--
-- Dumping data for table `configuration_types`
--

 REPLACE INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`, `is_enabled`, `created`, `modified`) VALUES
('0d8228e0-ff6a-4c83-8e04-ae1847621306', 'facebook_app_id', '', 'text', '', 'social', 1, 1, '2019-04-04 16:13:32', '2019-04-04 16:14:33'),
('26438b89-93a3-43dc-872a-74abf5160624', 'share_image', '', 'text', '', 'social', 1, 1, '2019-04-04 16:23:03', '2019-04-04 16:23:03'),
('7f4a9f10-405f-4b8e-a87a-8d6bbbf2d2ee', 'share_description', '', 'textarea', '', 'social', 1, 1, '2019-04-04 16:13:14', '2019-04-04 16:19:32');