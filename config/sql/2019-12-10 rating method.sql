-- Add method to rating_types
ALTER TABLE `rating_types`
    ADD `method` CHAR(1) NOT NULL DEFAULT 'S' AFTER `th_vote_name`;

-- Add custom scoring formula
INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `created`, `modified`)
VALUES ('f7e033fe-7295-4bbd-9c89-c29a3fa01f2c', 'scoring_formula',
        'Custom scoring formula; Ensure the equation starts with \"return \"', 'text',
        'return (positiveWeighted)/(positiveWeighted + negativeWeighted) * 100.0', 'admin', 1, 1, '2019-12-11 01:43:18',
        '2019-12-11 01:43:18');

-- custom ET formula
INSERT INTO `configurations` (`id`, `configuration_type_id`, `tenant_id`, `value`, `created`, `modified`)
VALUES ('9b58373e-1bb7-11ea-b07f-d8bb2276eb37', 'f7e033fe-7295-4bbd-9c89-c29a3fa01f2c',
        'e59f110e-3045-5598-99a9-9d402d523cf4',
        'let score = (positiveWeighted + positiveCountBonus)/(positiveCount+ negativeCount + positiveCountBonus) * 100.0; return score > 100 ? 100 : score',
        '2019-12-11 01:43:18',
        '2019-12-11 01:43:18');

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;
