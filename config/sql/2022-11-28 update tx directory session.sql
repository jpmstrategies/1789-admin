# tx directory: 4cfda83c-1343-5566-ab8a-0e06de9a96f1

update configurations
set value = 88
where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1'
  and id = 'ccb84464-25f0-11eb-8471-06ff9d002a08';

update legislative_sessions
set year     = 88,
    slug     = '88',
    state_pk = '88'
where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1'
  and id = 'dc858635-8d4a-47cf-be83-1d26439befcd';

delete
from legislators
where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1'
  and id in (
             'd7d91603-b25b-4775-87c6-065232f6a663',
             '103a8d6a-0431-4045-a29d-e8936edcc30c',
             'ce202afc-7862-4a88-8b47-1bb2861f6bbb',
             '26a3321f-cebf-4418-adf6-b95943226739',
             'de95b690-a5d0-4c62-b5d4-6342fc6a5365',
             '75530cf1-9c9c-4a66-9f08-18b1d44898aa',
             '03d0aa66-c2bb-4cd8-9cd1-75bce5d0de4b',
             '2b99f54a-a60d-4cf1-81a2-994f5980c906',
             'df1007de-41d0-4702-906e-84b15b75f6e0',
             'f9d998f0-4046-405c-9d17-e35423ef5bd4',
             '08d9d022-1544-4b13-a300-ced87256a646',
             'de4a9b65-9de8-4382-b287-182f0d66ab74',
             '63ce8caa-136c-4228-9c0d-33ad3f324152',
             '88a99938-9ad0-4bae-b034-7763a06eb189',
             '9eb5674a-83f4-4bc5-8236-a2a6fa6c49a6',
             'a6cd680e-4270-4118-9a40-823efe96985e',
             'a79a9789-ba7b-4f8b-92fd-7321aa2bca9d',
             'c64a487f-e758-483a-a8e2-dbf55c9ec1dc',
             '670960d6-7ac6-4276-835f-dfd2df778626',
             '6080a4a7-1b25-4f82-b2de-f03f1818f08f',
             '46659782-cf4e-4b45-823f-e064aa229037',
             'e3eb66c0-2dd1-4c36-aade-92b450e5071f',
             '185d211a-6b22-4290-9b7f-f2a7c59e6907',
             '4aec97ea-1f76-4244-8da8-b67e6990c437',
             '71125e11-4b46-4104-b9ed-838a61c61e4b', # Drew Springer's TX house record. He is now in the TX senate.
             'ee519317-3655-4b89-94c6-ac9587d84bb7', # Chris Paddie
             'b939cd81-b2a5-4e9d-a0b5-5827d6cdc256', # Garnet Coleman
             'dd84583f-7a59-4fdb-b31b-248aa29544e6', # James White
             '9a75235b-b712-4a42-9057-d58cbdad87a2', # Larry Taylor
             '841ed93d-3f0a-47c7-af63-3b6ac6ee3e11', # Jane Nelson
             'ab37b980-b6c4-4268-bb49-afa33978f33a', # Eddie Lucio
             'e96e890b-4a18-4038-b3b6-edd522a4a75b', # Kel Seliger
             '729d28b7-6c44-4479-94be-9432f51a0da8', # Mayra Flores
             '38a7de9d-86a0-11eb-a13c-063b539168c0', # George P. Bush
             # SBOE
             '315e2936-aeb0-11ec-9dce-0204798dbc1f', # Georgina Pérez
             '315eba90-aeb0-11ec-9dce-0204798dbc1f', # Ruben Cortez, Jr.
             '315ec22f-aeb0-11ec-9dce-0204798dbc1f', # Lawrence A. Allen, Jr.
             '315ef090-aeb0-11ec-9dce-0204798dbc1f', # Matt Robinson
             '315f041e-aeb0-11ec-9dce-0204798dbc1f', # Sue Melton-Malone
             '315f06d4-aeb0-11ec-9dce-0204798dbc1f', # Jay Johnson
             '598645a4-cc28-4b70-afd8-89aba43b68c7' # Phil Stephenson
    );

delete
from people
where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1'
  and id in (
             'ee15e435-2523-4432-8444-0db6a38b57af',
             '949b0f8b-e675-4498-ac9e-7ed718e7e40d',
             '14e9ba15-f5d8-45af-a4eb-9f542354f451',
             '77f682b7-9c93-4dac-8c31-8e2c602fc4e7',
             '72ba63fd-edeb-403e-a3eb-4b797efcce8c',
             'c9091acc-c8d3-436e-9e2b-c81b8e776dff',
             'f222cfc4-e791-41c1-9342-9dbdf8f21b4e',
             'a24f7a2c-ff2d-4b6c-b89e-b5566476895c',
             'e0aedacf-11f6-4a37-b934-d702cbdae9a7',
             '3835bf58-7153-43d2-86b0-731bbd1236cc',
             '54638b32-889b-4298-b2fa-9e904e0a7cd2',
             '25290266-e61a-419b-a19b-05dd5dbc6cd2',
             '7ba8f28a-02e7-4aaa-b338-7077731ee547',
             'fd4927ba-615a-4cfc-b7ad-8d7ab31f5d43',
             'dc531eb5-ac9b-47df-8fd2-8f6114d7b517',
             '3c965d3f-408a-467e-8c7d-9d19b3e31188',
             'af2962c4-63d6-48d6-ad17-ab48266630ad',
             'e6b5555f-43cf-4716-9be1-14c48e849071',
             '89f9ee23-a11f-4a53-b023-5443510fafb5',
             'f5c40a7c-2f6f-4756-8787-fe16dcaf544a',
             '9d7c6c26-8395-4e4f-ac8b-7da041d7623c',
             '10423480-cbfb-4e16-a1f2-2184ecc3500e',
             '6acd3012-6640-4096-ad9d-1375bdce268f',
             'bd18d87c-f9bb-4b46-95bc-ae65bcbe3872',
             'df4fc9ee-a035-41ba-a0c0-8869d67af538', # Chris Paddie
             'd92cf0be-877a-4003-830d-2d7931720154', # Garnet Coleman
             'a026b87c-32ab-4d62-a8f3-7b9b90c0fe4a', # James White
             '69ff35c1-d9fb-4bee-a353-4b09a986be30', # Larry Taylor
             '3c250dc9-b312-416e-9abf-0f7a0d1803e7', # Jane Nelson
             '396b98fe-3ef8-416d-adbd-de65e6f9a9b5', # Eddie Lucio
             '8a58c8f1-4ce9-4429-8ece-e64ee742c6ac', # Kel Seliger
             '54702517-0173-43e0-9a67-2012177362e1', # Mayra Flores
             '9347b558-813a-11eb-b8d1-0242ac110003'  # George P. Bush
             # SBOE
             '18cd9586-abcd-11ec-b909-0242ac120002', # Georgina Pérez
             '18cd9a4a-abcd-11ec-b909-0242ac120002', # Ruben Cortez, Jr.
             '18cd9d7e-abcd-11ec-b909-0242ac120002', # Lawrence A. Allen, Jr.
             '18cda1fc-abcd-11ec-b909-0242ac120002', # Matt Robinson
             '18cdafc6-abcd-11ec-b909-0242ac120002', # Sue Melton-Malone
             '18cdb232-abcd-11ec-b909-0242ac120002', # Jay Johnson
             '2cd707ec-fd36-4a54-9bd1-3a7b39406298' # Phil Stephenson
    );

update people p
join legislators l on l.person_id = p.id
set p.is_enabled = 0, l.is_enabled = 0
where p.tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1'
and (p.is_enabled = 0 or l.is_enabled = 0);

delete from legislators
where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1'
    and is_enabled = 0;

delete from people
where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1'
    and is_enabled = 0;

insert into people (tenant_id, id, full_name, first_name, last_name, slug, is_enabled)
values ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', '6984343e-4542-4bd6-be67-7e11f52b26cf', 'Trent Ashby', 'Trent', 'Ashby', 'trent-ashby', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', '36a5af6a-9eb4-4554-93b3-6ccbaac88998', 'James Talarico', 'James', 'Talarico', 'james-talarico', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', '3dac2352-37b4-4bb2-9c33-9cfe53b39081', 'Phil King', 'Phil', 'King', 'phil-king', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', '5367d107-a53a-4fac-bbfc-d8a994944adf', 'Mayes Middleton', 'Mayes', 'Middleton', 'mayes-middleton', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'eeb8aa39-6896-46b1-9626-491e0cbe9a96', 'Tan Parker', 'Tan', 'Parker', 'tan-parker', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', '93230d2a-d2a8-45a7-8217-30c77e15e881', 'Jasmine Crockett', 'Jasmine', 'Crockett', 'jasmine-crockett', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'd1d1febe-bf6a-4e2d-8722-f1d0a6ec4656', 'Vicente Gonzalez', 'Vicente', 'Gonzalez', 'vicente-gonzalez', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Angelia Orr', 'Angelia', 'Orr', 'angelia-orr', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Stan Gerdes', 'Stan', 'Gerdes', 'stan-gerdes', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Ellen Troxclair', 'Ellen', 'Troxclair', 'ellen-troxclair', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Christian Hayes', 'Christian', 'Hayes', 'christian-hayes', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Terri Leo-Wilson', 'Terri', 'Leo-Wilson', 'terri-leo-wilson', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Janie Lopez', 'Janie', 'Lopez', 'janie-lopez', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Maria Luisa Flores', 'Marie Luisa', 'Flores', 'maria-luisa-flores', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Caroline Harris', 'Caroline', 'Harris', 'caroline-harris', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Richard Hayes', 'Richard', 'Hayes', 'richard-hayes', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Frederick Frazier', 'Frederick', 'Frazier', 'frederick-frazier', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Ben Bamgarner', 'Ben', 'Bamgarner', 'ben-bamgarner', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Kronda Thimesch', 'Kronda', 'Thimesch', 'kronda-thimesch', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Mihaela Plesa', 'Mihaela', 'Plesa', 'mihaela-plesa', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Carrie Isaac', 'Carrie', 'Isaac', 'carrie-isaac', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Suleman Lalani', 'Suleman', 'Lalani', 'suleman-lalani', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Carl Tepper', 'Carl', 'Tepper', 'carl-tepper', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Salman Bhojani', 'Salman', 'Bhojani', 'salman-bhojani', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Nate Schatzline', 'Nate', 'Schatzline', 'nate-schatzline', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Venton Jones', 'Venton', 'Jones', 'venton-jones', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'John W. Bryant', 'John', 'Bryant', 'john-w-bryant', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Mark Dorazio', 'Mark', 'Dorazio', 'mark-dorazio', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Josey Garcia', 'Josey', 'Garcia', 'josey-garcia', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Charles Cunningham', 'Charles', 'Cunningham', 'charles-cunningham', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Mano Deayala', 'Mano', 'Deayala', 'mano-deayala', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Pete Flores', 'Pete', 'Flores', 'pete-flores', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Morgan LaMantia', 'Morgan', 'LaMantia', 'morgan-lamantia', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Kevin Sparks', 'Kevin', 'Sparks', 'kevin-sparks', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Nathaniel Moran', 'Nathaniel', 'Moran', 'nathaniel-moran', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Keith Self', 'Keith', 'Self', 'keith-self', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Morgan Luttrell', 'Morgan', 'Luttrell', 'morgan-luttrell', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Monica De La Cruz', 'Monica', 'De La Cruz', 'monica-de-la-cruz', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Greg Casar', 'Greg', 'Casar', 'greg-casar', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Wesley Hunt', 'Wesley', 'Hunt', 'wesley-hunt', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Stan Kitzman', 'Stan', 'Kitzman', 'stan-kitzman', true),
       # SBOE
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Melissa Ortega', 'Melissa', 'Ortega', 'melissa-ortega', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'LJ Francis', 'LJ', 'Francis', 'lj-francis', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Staci Childs', 'Staci', 'Childs', 'staci-childs', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Julie Pickren', 'Julie', 'Pickren', 'julie-pickren', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Evelyn Brooks', 'Evelyn', 'Brooks', 'evelyn-brooks', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', uuid(), 'Aaron Kinsey', 'Aaron', 'Kinsey', 'aaron-kinsey', true)
as p on duplicate key update full_name  = p.full_name,
                        first_name = p.first_name,
                        last_name  = p.last_name,
                        slug       = p.slug;

update people set image_url = concat(slug, '.jpg')
where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and image_url is null;

# Texas House: 28ad9e59-15e7-4c7f-aa20-69278dc91c1b
# Texas Senate: b0bf650e-4ac7-4889-b7fd-b13e9f120d4a
# US House: 3e8d8d3e-0a76-11eb-bed3-0242ac110004
# Texas SBOE: 76aefe5e-abca-11ec-9b75-47d55424b933

# Republican: 1ecb5347-2f0b-49b2-82e0-1648229f4dc1
# Democrat: 69451bba-a1b8-43a1-8e10-b16515cc31db

insert into legislators (tenant_id, legislative_session_id, id, person_id, chamber_id, party_id, district, is_enabled)
values ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', '56fc22de-5e88-429e-a079-e5e45a5ae134', 'Trent Ashby', '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '9', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', '4a70864b-d74c-4250-89d8-d01beeb7435e', 'James Talarico', '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '69451bba-a1b8-43a1-8e10-b16515cc31db', '50', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', '5730049d-135a-4340-87b2-604ac2ab92fe', 'Phil King', 'b0bf650e-4ac7-4889-b7fd-b13e9f120d4a', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '10', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', '540f4fd0-e55f-4dea-986c-f7dc260abcd7', 'Mayes Middleton', 'b0bf650e-4ac7-4889-b7fd-b13e9f120d4a', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '11', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', 'b5fbb536-678a-4c5d-b378-b91f3ed94adb', 'Tan Parker', 'b0bf650e-4ac7-4889-b7fd-b13e9f120d4a', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '12', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', 'd0eda31d-e025-4a4b-aec8-4e5129853e47', 'Jasmine Crockett', '3e8d8d3e-0a76-11eb-bed3-0242ac110004', '69451bba-a1b8-43a1-8e10-b16515cc31db', '30', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', '26548cb1-5f60-4cfc-965a-988217b89186', 'Vicente Gonzalez', '3e8d8d3e-0a76-11eb-bed3-0242ac110004', '69451bba-a1b8-43a1-8e10-b16515cc31db', '34', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Angelia Orr'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '13', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Stan Gerdes'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '17', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Ellen Troxclair'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '19', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Christian Hayes'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '69451bba-a1b8-43a1-8e10-b16515cc31db', '22', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Terri Leo-Wilson'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '23', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Janie Lopez'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '37', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Maria Luisa Flores'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '69451bba-a1b8-43a1-8e10-b16515cc31db', '51', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Caroline Harris'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '52', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Richard Hayes'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '57', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Frederick Frazier'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '61', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Ben Bamgarner'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '63', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Kronda Thimesch'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '65', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Mihaela Plesa'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '69451bba-a1b8-43a1-8e10-b16515cc31db', '70', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Carrie Isaac'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '73', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Suleman Lalani'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '69451bba-a1b8-43a1-8e10-b16515cc31db', '76', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Carl Tepper'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '84', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Salman Bhojani'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '69451bba-a1b8-43a1-8e10-b16515cc31db', '92', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Nate Schatzline'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '93', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Venton Jones'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '69451bba-a1b8-43a1-8e10-b16515cc31db', '100', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'John W. Bryant'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '69451bba-a1b8-43a1-8e10-b16515cc31db', '114', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Mark Dorazio'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '122', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Josey Garcia'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '69451bba-a1b8-43a1-8e10-b16515cc31db', '124', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Charles Cunningham'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '127', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Mano Deayala'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '133', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Pete Flores'), 'b0bf650e-4ac7-4889-b7fd-b13e9f120d4a', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '24', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Morgan LaMantia'), 'b0bf650e-4ac7-4889-b7fd-b13e9f120d4a', '69451bba-a1b8-43a1-8e10-b16515cc31db', '27', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Kevin Sparks'), 'b0bf650e-4ac7-4889-b7fd-b13e9f120d4a', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '31', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Nathaniel Moran'), '3e8d8d3e-0a76-11eb-bed3-0242ac110004', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '1', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Keith Self'), '3e8d8d3e-0a76-11eb-bed3-0242ac110004', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '3', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Morgan Luttrell'), '3e8d8d3e-0a76-11eb-bed3-0242ac110004', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '8', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Monica De La Cruz'), '3e8d8d3e-0a76-11eb-bed3-0242ac110004', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '15', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Greg Casar'), '3e8d8d3e-0a76-11eb-bed3-0242ac110004', '69451bba-a1b8-43a1-8e10-b16515cc31db', '35', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Wesley Hunt'), '3e8d8d3e-0a76-11eb-bed3-0242ac110004', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '38', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Stan Kitzman'), '28ad9e59-15e7-4c7f-aa20-69278dc91c1b', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '85', true),
       # SBOE
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Melissa Ortega'), '76aefe5e-abca-11ec-9b75-47d55424b933', '69451bba-a1b8-43a1-8e10-b16515cc31db', '1', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'LJ Francis'), '76aefe5e-abca-11ec-9b75-47d55424b933', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '2', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Staci Childs'), '76aefe5e-abca-11ec-9b75-47d55424b933', '69451bba-a1b8-43a1-8e10-b16515cc31db', '4', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Julie Pickren'), '76aefe5e-abca-11ec-9b75-47d55424b933', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '7', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Evelyn Brooks'), '76aefe5e-abca-11ec-9b75-47d55424b933', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '14', true),
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', uuid(), (select id from people where tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1' and full_name = 'Aaron Kinsey'), '76aefe5e-abca-11ec-9b75-47d55424b933', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', '15', true)
as l on duplicate key update party_id = l.party_id,
                               chamber_id = l.chamber_id,
                               district  = l.district;

insert into legislators (tenant_id, legislative_session_id, id, person_id, chamber_id, party_id, district, title_id) values
       ('4cfda83c-1343-5566-ab8a-0e06de9a96f1', 'dc858635-8d4a-47cf-be83-1d26439befcd', '452abb71-4d0a-4ce1-be04-5c45696006df', 'Dawn Buckingham', '3a332b6a-8133-11eb-b8d1-0242ac110003', '1ecb5347-2f0b-49b2-82e0-1648229f4dc1', null, 'b0b38366-8133-11eb-b8d1-0242ac110003')
as l on duplicate key update party_id = l.party_id,
                               chamber_id = l.chamber_id,
                               district  = l.district,
                               title_id  = l.title_id;

# Dawn Buckingham
update people
set committee_list = null
where id = '4e17b232-b8fd-4812-b3c3-d16fe00717b6';

# Dawn Buckingham
delete from addresses where person_id = '4e17b232-b8fd-4812-b3c3-d16fe00717b6';

# Texas House: 28ad9e59-15e7-4c7f-aa20-69278dc91c1b
# Texas Senate: b0bf650e-4ac7-4889-b7fd-b13e9f120d4a
update legislators
set term_starts = '2023-01-10',
    term_ends = '2025-01-14'
where chamber_id in ('28ad9e59-15e7-4c7f-aa20-69278dc91c1b', 'b0bf650e-4ac7-4889-b7fd-b13e9f120d4a');

# US House: 3e8d8d3e-0a76-11eb-bed3-0242ac110004
update legislators
set term_starts = '2023-01-03',
    term_ends = '2025-01-03'
where chamber_id = '3e8d8d3e-0a76-11eb-bed3-0242ac110004';

# SBOE: 76aefe5e-abca-11ec-9b75-47d55424b933
update legislators
set term_starts = '2023-01-01',
    term_ends = '2027-01-01'
where chamber_id = '76aefe5e-abca-11ec-9b75-47d55424b933';

# Statewide: 3a332b6a-8133-11eb-b8d1-0242ac110003
update legislators
set term_starts = null,
    term_ends = null
where chamber_id = '3a332b6a-8133-11eb-b8d1-0242ac110003';
