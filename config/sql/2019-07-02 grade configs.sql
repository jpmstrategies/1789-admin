-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 02, 2019 at 05:36 PM
-- Server version: 5.7.25
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `1789_cornerstone`
--

--
-- Dumping data for table `configuration_types`
--

REPLACE INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                    `is_enabled`, `created`, `modified`)
VALUES ('20ee4d69-b928-4ce6-956a-24ce94ea5749', 'session_grade_display', 'Please type either number or name', 'text',
        'number', 'site', 1, 1, '2019-07-02 21:02:17', '2019-07-02 23:03:26'),
       ('f52ecf9d-2fef-476c-8040-bd40e2843c30', 'career_grade_display', 'Please type either number or name', 'text',
        'number', 'site', 1, 1, '2019-07-02 23:03:15', '2019-07-02 23:03:44');

ALTER TABLE `grades`
    ADD `lower_range` SMALLINT NOT NULL AFTER `sort_order`;

ALTER TABLE `grades`
    ADD `upper_range` SMALLINT NOT NULL AFTER `lower_range`;

UPDATE grades
SET lower_range = 0,
    upper_range = 50
WHERE name = 'F-';
UPDATE grades
SET lower_range = 51,
    upper_range = 59
WHERE name = 'F';
UPDATE grades
SET lower_range = 60,
    upper_range = 64
WHERE name = 'F+';
UPDATE grades
SET lower_range = 65,
    upper_range = 68
WHERE name = 'D-';
UPDATE grades
SET lower_range = 69,
    upper_range = 71
WHERE name = 'D';
UPDATE grades
SET lower_range = 72,
    upper_range = 74
WHERE name = 'D+';
UPDATE grades
SET lower_range = 75,
    upper_range = 77
WHERE name = 'C-';
UPDATE grades
SET lower_range = 78,
    upper_range = 80
WHERE name = 'C';
UPDATE grades
SET lower_range = 81,
    upper_range = 83
WHERE name = 'C+';
UPDATE grades
SET lower_range = 84,
    upper_range = 86
WHERE name = 'B-';
UPDATE grades
SET lower_range = 87,
    upper_range = 89
WHERE name = 'B';
UPDATE grades
SET lower_range = 90,
    upper_range = 92
WHERE name = 'B+';
UPDATE grades
SET lower_range = 93,
    upper_range = 95
WHERE name = 'A-';
UPDATE grades
SET lower_range = 95,
    upper_range = 97
WHERE name = 'A';
UPDATE grades
SET lower_range = 98,
    upper_range = 100
WHERE name = 'A+';