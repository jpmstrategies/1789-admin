ALTER TABLE `people`
    ADD `facebook` VARCHAR(100) NULL AFTER `email`,
    ADD `twitter`  VARCHAR(100) NULL AFTER `facebook`;