-- --------------------------------------------------------

--
-- Modify structure for table `ratings`
--

ALTER TABLE `ratings`
  ADD `json` JSON NULL AFTER `slug`;

-- --------------------------------------------------------

--
-- Table structure for table `rating_fields`
--

CREATE TABLE `rating_fields`
(
  `id`         char(36)     NOT NULL,
  `tenant_id`  char(36)     NOT NULL,
  `name`       varchar(45)  NOT NULL,
  `json_path`  varchar(255) NOT NULL,
  `type`       char(1)      NOT NULL,
  `sort_order` int(11)  DEFAULT NULL,
  `created`    datetime DEFAULT NULL,
  `modified`   datetime DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Dumping data for table `rating_fields`
--

INSERT INTO `rating_fields` (`id`, `tenant_id`, `name`, `json_path`, `type`, `sort_order`, `created`, `modified`)
VALUES ('748f2b55-d801-4598-a508-d99363ceac45', '0f282b29-70a6-575e-abbe-1ff806c4cadf', 'Amendment Sponsor',
        'data.details.amendment.sponsorName', 'T', 7, '2019-04-02 18:15:12', '2019-04-02 18:15:12'),
       ('7d119c9b-90b9-4281-a5d4-3262ca6ff8e2', '0f282b29-70a6-575e-abbe-1ff806c4cadf', 'Amendment Action',
        'data.details.amendment.action', 'T', 6, '2019-04-02 18:14:56', '2019-04-02 18:14:56'),
       ('8bd0ff7a-df87-493d-af99-2601df87fb95', '0f282b29-70a6-575e-abbe-1ff806c4cadf', 'Sponsor',
        'data.details.sponsor', 'L', 1, '2019-04-02 18:13:10', '2019-04-02 18:13:17'),
       ('99b2dc09-fe7c-4795-aadf-c4f00661e35e', '0f282b29-70a6-575e-abbe-1ff806c4cadf', 'Printer Number',
        'data.details.printerNumber', 'T', 2, '2019-04-02 18:13:28', '2019-04-02 18:13:28'),
       ('ad622da8-9030-4f62-9e7f-e1ff89d146c4', '0f282b29-70a6-575e-abbe-1ff806c4cadf', 'Final Passage',
        'data.details.finalPassage', 'B', 3, '2019-04-02 18:13:44', '2019-04-02 18:15:38'),
       ('dbc3056f-eb15-4888-af75-cb026d84009e', '0f282b29-70a6-575e-abbe-1ff806c4cadf', 'Amendment',
        'data.details.amendment', 'L', 5, '2019-04-02 18:14:36', '2019-04-02 18:14:36'),
       ('f2fedaba-8816-43f4-aacf-023e405d3022', '0f282b29-70a6-575e-abbe-1ff806c4cadf', 'Identifier',
        'data.details.identifier', 'T', 4, '2019-04-02 18:14:24', '2019-04-02 18:14:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rating_fields`
--
ALTER TABLE `rating_fields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `XUN1_rating_fields` (`tenant_id`, `name`),
  ADD KEY `FK_tenant_id` (`tenant_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `rating_fields`
--
ALTER TABLE `rating_fields`
  ADD CONSTRAINT `XFK1_rating_fields` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`);
