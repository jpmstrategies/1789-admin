INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `is_public`, `created`, `modified`)
VALUES ('a0d0f469-89da-462a-8a30-f02942f7b428', 'ga4_measurement_id',
        'Google Analytics 4 - Measurement ID - Alternative to Google Analytics 3 (Universal)', 'text', '', 'social', 1,
        1, 1, '2021-01-07 01:27:51', '2021-01-07 01:29:44');

UPDATE `configuration_types`
SET `name`        = 'ga3_tracking_id',
    `description` = 'Google Analytics 3 (Universal) - Tracking ID - Alternative to Google Analytics 4',
    `modified`    = '2021-01-07 01:34:47'
WHERE `configuration_types`.`id` = 'e8ca55c0-fb1d-435e-b89f-8fa008d1eb4b';

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;
