ALTER TABLE `rating_types`
  ADD `sort_order` INT(11) NULL AFTER `th_vote_name`;

UPDATE `rating_types`
SET sort_order = 1
WHERE name = 'Vote';

UPDATE `rating_types`
SET sort_order = 2
WHERE name = 'Sponsorship';

UPDATE `rating_types`
SET sort_order = 3
WHERE name = 'Leadership';