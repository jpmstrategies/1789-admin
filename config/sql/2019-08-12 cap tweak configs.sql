-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 12, 2019 at 12:13 AM
-- Server version: 5.7.25
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `1789_cornerstone`
--

--
-- Dumping data for table `configuration_types`
--

REPLACE INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                    `is_enabled`, `created`, `modified`)
VALUES ('0013fb00-c324-43b2-949f-8ffb1a3fe689', 'global_cta_btn_link', '', 'text', '', 'site', 1, 1,
        '2019-08-12 16:06:57', '2019-08-12 16:06:57'),
       ('b6c1965b-7cf3-4a4d-9d52-d76c3b1be544', 'cdn_url', 'URL to CDN for Assets (No trailing slashes)', 'text',
        'https://s.cornerstoneapp.org', 'admin', 1, 1, '2019-08-12 05:06:21', '2019-08-12 05:06:33'),
       ('f8c7f10c-af2d-465a-b6e6-d7279a73e518', 'global_cta_btn_text', '', 'text', '', 'site', 1, 1,
        '2019-08-12 16:07:06', '2019-08-12 16:07:06');

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;