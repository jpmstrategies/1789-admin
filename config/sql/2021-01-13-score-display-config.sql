ALTER TABLE `legislative_sessions`
    ADD `show_scores` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_enabled`;

UPDATE legislative_sessions
SET legislative_sessions.show_scores = 1
WHERE state_pk != '87';