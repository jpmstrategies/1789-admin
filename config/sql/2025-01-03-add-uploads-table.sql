alter table bills
    drop constraint XFK12_bills,
    drop column upload_id;
drop table uploads;

create table uploads
(
    id        char(36)     not null primary key,
    tenant_id char(36)     not null,
    path      varchar(255) not null,
    mime      varchar(255) not null,
    width     smallint unsigned not null,
    height    smallint unsigned not null,
    created   datetime default null,
    modified  datetime default null
) engine = innodb
  default charset = utf8;

alter table bills
    add upload_id char(36) null after senate_rating_votes;

alter table bills
    add constraint XFK12_bills
        foreign key (upload_id) references uploads (id);
