INSERT INTO `1789_cornerstone`.configuration_types (id, name, description, data_type, default_value, tab, is_editable,
                                                    is_enabled, is_public, created, modified)
VALUES ('a0a71023-38e9-47eb-8a23-909a1c528c75', 'listing_header_text', '', 'text', '', 'home', 1, 1,
        1, '2022-09-28 00:19:19', '2022-09-28 00:19:19');
INSERT INTO `1789_cornerstone`.configuration_types (id, name, description, data_type, default_value, tab, is_editable,
                                                    is_enabled, is_public, created, modified)
VALUES ('f1586fe1-e5f8-4f21-bb35-1b1321665561', 'show_ballotpedia_links', '', 'checkbox', '1', 'legislator', 1, 1, 1,
        '2022-09-28 00:19:37', '2022-09-28 00:19:37');
INSERT INTO `1789_cornerstone`.configuration_types (id, name, description, data_type, default_value, tab, is_editable,
                                                    is_enabled, is_public, created, modified)
VALUES ('c192af0c-3b6b-4fcb-bd09-0308b7de6cdd', 'grade_svg', '', 'textarea', '', 'grade', 0, 1, 1,
        '2022-10-26 22:51:19', '2022-10-26 22:51:19');

-- Insert configuration values for new configuration types
INSERT INTO `1789_cornerstone`.configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM `1789_cornerstone`.configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
            ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;
