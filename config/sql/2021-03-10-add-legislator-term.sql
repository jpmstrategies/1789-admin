alter table legislators
    add term_starts date after journal_name,
    add term_ends   date after term_starts;