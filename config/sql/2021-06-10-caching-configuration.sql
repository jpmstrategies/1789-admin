INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `is_public`, `created`, `modified`)
VALUES ('6a3b93d4-9257-405e-bfa2-dc37bfe7e0da', 'enable_caching', 'Enable caching for web performance', 'checkbox',
        'no', 'site', 1, 1, 0, '2021-06-11 01:26:32', '2021-06-11 01:28:19');

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;
