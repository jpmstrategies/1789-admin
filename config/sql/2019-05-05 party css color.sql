ALTER TABLE `parties`
    ADD `css_color` char(7) NULL AFTER `abbreviation`;

UPDATE `parties`
SET `css_color` = '#800000'
WHERE `abbreviation` = 'R';

UPDATE `parties`
SET `css_color` = '#000080'
WHERE `abbreviation` = 'D';

UPDATE `parties`
SET `css_color` = '#808080'
WHERE `abbreviation` = 'U';

ALTER TABLE `parties`
    CHANGE `css_color` `css_color` CHAR(7) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;