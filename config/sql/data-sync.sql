-- copy legacy production Fiscal Index to 1789 Fiscal Index
update 1789_cornerstone.people
    inner join et_ratings.people srcPeople on people.state_pk = srcPeople.lrl_pk
    inner join tusa_app_tx.filers on srcPeople.ttx_filer_slug = filers.slug
    inner join tusa_app_tx.candidates ON filers.candidateId = candidates.id
SET people.first_elected      = srcPeople.first_elected,
    people.district_city      = srcPeople.district_city,
    people.ttx_candidate_slug = candidates.slug,
    people.bio                = srcPeople.bio,
    people.modified           = now()
where people.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';

-- copy 1789 Fiscal Index to other Texas tenants
UPDATE 1789_cornerstone.people
    INNER JOIN
    (
        select srcPeople.state_pk,
               srcPeople.ttx_candidate_slug,
               srcPeople.first_elected,
               srcPeople.district_city,
               srcPeople.bio,
               bp_people.bp_url
        from 1789_cornerstone.people srcPeople
                 inner join 1789_cornerstone.tenants t on srcPeople.tenant_id = t.id
                 inner join tusa_app_tx.candidates on candidates.slug = srcPeople.ttx_candidate_slug
                 inner join tusa_app_tx.bp_people on candidates.bpPersonId = bp_people.id
             -- source tenant
        where tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4'
          and ttx_candidate_slug is not null
    ) results
    ON people.state_pk = results.state_pk
SET people.first_elected      = results.first_elected,
    people.district_city      = results.district_city,
    people.ttx_candidate_slug = results.ttx_candidate_slug,
    people.ballotpedia_url    = results.bp_url,
    people.bio                = results.bio,
    people.modified           = now()
-- target tenant
WHERE people.tenant_id = '4cfda83c-1343-5566-ab8a-0e06de9a96f1';