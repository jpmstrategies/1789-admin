--
-- Dumping data for table `configuration_types`
--

INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `created`, `modified`)
VALUES ('72b12746-450d-42f2-916c-03d2e7e04201', 'scoring_round', '', 'select', 'round', 'grade', 1, 1,
        '2019-12-11 21:13:33', '2019-12-11 21:13:33');

--
-- Dumping data for table `configuration_options`
--

INSERT INTO `configuration_options` (`id`, `configuration_type_id`, `name`, `value`, `sort_order`, `created`,
                                     `modified`)
VALUES ('0c917c29-e0d6-4748-a5dc-64526cd4293e', '72b12746-450d-42f2-916c-03d2e7e04201', 'Round Up', 'ceil', 1,
        '2019-12-11 21:13:51', '2019-12-11 21:13:51'),
       ('1cf5d6bc-b650-488f-a5ee-11f36a912365', '72b12746-450d-42f2-916c-03d2e7e04201', 'Round (0.5 up)', 'round', 2,
        '2019-12-11 21:14:26', '2019-12-11 21:14:26'),
       ('d4535240-74e5-4591-9d4f-5497cac5d8ca', '72b12746-450d-42f2-916c-03d2e7e04201', 'Round Down', 'floor', 3,
        '2019-12-11 21:14:45', '2019-12-11 21:14:45');

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;

-- Change Empower Texans to "Round Up"
UPDATE configurations
SET value = 'floor'
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4'
  AND configuration_type_id = '72b12746-450d-42f2-916c-03d2e7e04201';
