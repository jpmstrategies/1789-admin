ALTER TABLE `people`
    DROP `ttx_filer_name`;

ALTER TABLE `people`
    ADD `ttx_candidate_slug` VARCHAR(255) NULL DEFAULT NULL AFTER `ttx_filer_slug`;

UPDATE `people` p
    INNER JOIN
    (
        SELECT p.id, c.slug as candidateSlug
        FROM people p
                 INNER JOIN ttx_finance.filers f ON p.ttx_filer_slug = f.slug
                 LEFT JOIN ttx_finance.candidates c ON f.candidateId = c.id
        WHERE p.ttx_filer_slug IS NOT NULL
    ) results ON p.id = results.id
SET p.ttx_candidate_slug = results.candidateSlug
WHERE p.ttx_filer_slug IS NOT NULL;

SELECT ttx_filer_slug, ttx_candidate_slug
FROM people
WHERE ttx_candidate_slug IS NOT NULL;

ALTER TABLE `people`
    DROP `ttx_filer_slug`;