-- --------------------------------------------------------

--
-- Temp Schema Changes for Data Compare
--

-- --------------------------------------------------------

-- Before Compare
# ALTER TABLE 1789_cornerstone.`ratings`
#     CHANGE `json` `json` TEXT NULL DEFAULT NULL;

-- After Compare
# ALTER TABLE 1789_cornerstone.`ratings`
#     CHANGE `json` `json` json NULL DEFAULT NULL;

-- --------------------------------------------------------

--
-- 2020-09-21-texas-directory.sql
--

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations`
(
    `id`            char(36)     NOT NULL,
    `tenant_id`     char(36)     NOT NULL,
    `org_tenant_id` char(36)     DEFAULT NULL,
    `name`          varchar(100) NOT NULL,
    `description`   text,
    `website`       varchar(100) DEFAULT NULL,
    `image_url`     varchar(100) DEFAULT NULL,
    `sort_order`    int(11)      DEFAULT NULL,
    `created`       datetime     DEFAULT NULL,
    `modified`      datetime     DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
    ADD PRIMARY KEY (`id`),
    ADD KEY `FK_tenant_id` (`tenant_id`),
    ADD KEY `FK_scorecard_id` (`org_tenant_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
    ADD CONSTRAINT `XFK1_organizations` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`),
    ADD CONSTRAINT `XFK2_organizations` FOREIGN KEY (`org_tenant_id`) REFERENCES `tenants` (`id`);

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores`
(
    `id`              char(36) NOT NULL,
    `tenant_id`       char(36) NOT NULL,
    `legislator_id`   char(36) NOT NULL,
    `organization_id` char(36) NOT NULL,
    `display_text`    varchar(10) DEFAULT NULL,
    `created`         datetime    DEFAULT NULL,
    `modified`        datetime    DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `XUN1_scores` (`tenant_id`, `legislator_id`, `organization_id`),
    ADD KEY `FK_tenant_id` (`tenant_id`),
    ADD KEY `FK_legislator_id` (`legislator_id`),
    ADD KEY `FK_organization_id` (`organization_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `scores`
--
ALTER TABLE `scores`
    ADD CONSTRAINT `XFK1_scores` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK2_scores` FOREIGN KEY (`legislator_id`) REFERENCES `legislators` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `XFK3_scores` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE;

-- --------------------------------------------------------

--
-- 2020-09-23-chambers-order.sql
--

-- --------------------------------------------------------

ALTER TABLE `chambers`
    ADD COLUMN `sort_order` int(11) DEFAULT NULL after `is_default`;

UPDATE `chambers`
SET chambers.sort_order = 1
WHERE type = 'L';

UPDATE `chambers`
SET chambers.sort_order = 2
WHERE type = 'U';

UPDATE `chambers`
SET chambers.sort_order = 3
WHERE type = 'C';

-- --------------------------------------------------------

--
-- 2020-09-23-tenant-type.sql
--

-- --------------------------------------------------------

alter table tenants
    add type char default 'S' not null after state;

alter table tenants
    add layout varchar(50) default 'default' not null after type;

ALTER TABLE `chambers`
    DROP INDEX `XUN3_chambers`;

-- --------------------------------------------------------

--
-- 2020-09-30-configurations-es.sql
--

-- --------------------------------------------------------

INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `created`, `modified`)
VALUES ('39003b3f-867d-4752-8ea2-4ac3b917638f', 'es_host_url', 'Elasticsearch - API Endpoint URL (no trailing slash)',
        'text', '', 'admin',
        1,
        1,
        '2020-09-30 03:45:13',
        '2020-09-30 03:45:13'),
       ('59f8e2c8-4e6c-45b9-8a55-68c8059076a1', 'es_index_name',
        'Elasticsearch - WordPress index name',
        'text', '', 'admin', 1, 1, '2020-09-30 03:45:30',
        '2020-09-30 03:45:30'),
       ('d9b26bba-d040-4db1-bd88-e3d74abe3d6e', 'es_wp_domain_name',
        'Elasticsearch - WordPress domain name for proxy validation (no trailing slash)', 'text', '', 'admin', 1, 1,
        '2020-09-30 03:45:46', '2020-09-30 03:45:46'),
       ('227ae5a9-ca15-41ff-b31f-b9c6c02aba0d', 'es_wp_show_posts',
        'Display related posts from WordPress (Requires Elasticsearch)',
        'checkbox',
        'no', 'admin', 1, 1,
        '2020-09-30 03:49:07', '2020-09-30 03:49:07')
on duplicate key update description   = values(description),
                        data_type     = values(data_type),
                        default_value = values(default_value),
                        tab           = values(tab),
                        is_editable   = values(is_editable),
                        is_enabled    = values(is_enabled),
                        modified      = now();

-- --------------------------------------------------------

--
-- 2020-10-09-configurations.sql
--

-- --------------------------------------------------------

INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `created`, `modified`)
VALUES ('8c45c44a-c97f-4088-abab-e741f7a45e93', 'subscribe_link_text', '', 'text', 'Subscribe to Updates', 'legislator',
        0, 1, '2018-10-03 01:11:37', '2020-10-09 17:32:55'),
       ('a4ef937d-902e-4566-9194-f07369638621', 'directory_homepage_link_text',
        'Legislator header link to homepage text', 'text', '', 'admin', 1, 1, '2020-10-09 17:31:41',
        '2020-10-09 17:31:41'),
       ('abd7d03a-7f25-48e1-b011-1aa5b2c224fb', 'header_data_url', 'Customer API Endpoint to pull data for the header',
        'text', '', 'admin', 1, 1, '2020-10-09 17:28:02', '2020-10-09 17:28:02')
on duplicate key update description   = values(description),
                        data_type     = values(data_type),
                        default_value = values(default_value),
                        tab           = values(tab),
                        is_editable   = values(is_editable),
                        is_enabled    = values(is_enabled),
                        modified      = now();

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;

-- Update subscribe_link_text for all tenants to match default.
update configurations
set value = 'Subscribe to Updates'
where configuration_type_id = '8c45c44a-c97f-4088-abab-e741f7a45e93';

-- Add is_public column to hide config values from the client.
alter table configuration_types
    add is_public tinyint(1) default 1 not null after is_enabled;

-- Make es_host_url and es_index_name configs private
update configuration_types
set is_public = 0
where id in ('39003b3f-867d-4752-8ea2-4ac3b917638f', '59f8e2c8-4e6c-45b9-8a55-68c8059076a1');

-- --------------------------------------------------------

--
-- 2020-11-02 committee fields.sql
--

-- --------------------------------------------------------

alter table people
    add column `committee_list` longtext after `bio`;

-- TODO evaluate effectiveness
update people
set people.committee_list = people.bio,
    people.bio            = null
where people.bio like '<u>Committee Assignments</u><br />%';

update people
set people.committee_list = replace(people.committee_list, '<u>Committee Assignments</u><br />', '');

-- --------------------------------------------------------

--
-- 2020-11-12-wordpress-api-configs.sql
--

-- --------------------------------------------------------


--
-- Dumping data for table `configuration_types`
--

INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `is_public`, `created`, `modified`)
VALUES ('a04fbd31-8b95-4ac8-8798-29e556d0ce58', 'related_articles_source', '', 'select', 'none', 'admin', 1, 1, 1,
        '2020-11-12 22:52:15', '2020-11-12 22:55:27');

--
-- Dumping data for table `configuration_options`
--

INSERT INTO `configuration_options` (`id`, `configuration_type_id`, `name`, `value`, `sort_order`, `created`,
                                     `modified`)
VALUES ('0cd7e651-b9fb-4708-b238-9ae32a0c9d05', 'a04fbd31-8b95-4ac8-8798-29e556d0ce58', 'Wordpress API', 'wpapi', 2,
        '2020-11-12 22:53:01', '2020-11-12 22:53:01'),
       ('8ea9dcac-7552-46b7-bed7-f0b2b344d2ce', 'a04fbd31-8b95-4ac8-8798-29e556d0ce58', 'None', 'none', 1,
        '2020-11-12 22:52:39', '2020-11-12 22:52:39'),
       ('929ec5d3-ea9d-4949-a647-c8f31563ee2a', 'a04fbd31-8b95-4ac8-8798-29e556d0ce58', 'ElasticSearch',
        'elasticsearch', 3, '2020-11-12 22:53:16', '2020-11-12 22:53:16');

-- Delete `es_wp_show_posts`
DELETE
FROM `configurations`
WHERE configuration_type_id IN (
    SELECT id
    FROM `configuration_types`
    WHERE name = 'es_wp_show_posts'
);

DELETE
FROM `configuration_types`
WHERE name = 'es_wp_show_posts';

-- Rename `es_wp_domain_name`
UPDATE `configuration_types`
SET name        = 'wp_domain_name',
    description = 'WordPress domain name for Elasticsearch proxy validation or WP API'
WHERE name = 'es_wp_domain_name';

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;

-- --------------------------------------------------------

--
-- 2020-11-13-directory-session-config.sql
--

-- --------------------------------------------------------

INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `is_public`, `created`, `modified`)
VALUES ('1287e15b-146f-4469-be5c-b90b8c622a92', 'directory_active_session_slug',
        'Directory - Legislative Session Slug to be displayed', 'text', '', 'site', 1, 1, 1, '2020-11-13 20:38:58',
        '2020-11-13 20:38:58');

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;

-- --------------------------------------------------------

--
-- 2020-11-19-tenant-sync.sql
--

-- --------------------------------------------------------

ALTER TABLE `tenants`
    ADD `sync_pending` DATETIME NULL DEFAULT NULL AFTER `layout`;

-- --------------------------------------------------------

--
-- Misc
--

-- --------------------------------------------------------

UPDATE configurations
SET value = '20201130'
where configuration_type_id = '202ec4f3-a8f2-4e48-9c93-17e6f74de500';

UPDATE people
SET ballotpedia_url = replace(ballotpedia_url, 'http://', 'https://');

UPDATE people
SET ballotpedia_url = replace(ballotpedia_url, '/wiki/index.php', '');