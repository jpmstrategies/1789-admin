-- delete tusa_default_slug
DELETE
FROM configurations
WHERE configuration_type_id = '68e3b76d-8fdc-447a-9554-13010af5b7a2';

DELETE
FROM configuration_types
WHERE id = '68e3b76d-8fdc-447a-9554-13010af5b7a2';

-- Add tusa_widget_type configuration
INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `created`, `modified`)
VALUES ('bdbe0c89-1d84-4f97-9b41-5ee10daf76bd', 'tusa_widget_type', '', 'select', 'details', 'admin', 1, 1,
        '2020-06-05 23:53:42', '2020-06-05 23:55:30');


-- Add tusa_widget_type options
INSERT INTO `configuration_options` (`id`, `configuration_type_id`, `name`, `value`, `sort_order`, `created`,
                                     `modified`)
VALUES ('353f92ee-909a-4609-b339-e275d4f452dd', 'bdbe0c89-1d84-4f97-9b41-5ee10daf76bd', 'Both', 'both', 3,
        '2020-06-05 23:57:54', '2020-06-05 23:57:54'),
       ('a520cc9d-fef5-41ec-9485-cdcd08026d5d', 'bdbe0c89-1d84-4f97-9b41-5ee10daf76bd',
        'Details (Totals / Top Donors / Top Payees)', 'details', 1, '2020-06-05 23:54:23', '2020-06-05 23:57:20'),
       ('f32fc764-275a-4f6d-9fc1-da763a0e8c36', 'bdbe0c89-1d84-4f97-9b41-5ee10daf76bd',
        'Summary (Total Contributions / Expenditures Only)', 'summary', 2, '2020-06-05 23:54:53',
        '2020-06-05 23:56:02');

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;
