ALTER TABLE `action_types`
    ADD `score_type` CHAR(1) NOT NULL DEFAULT 'Z' AFTER `evaluation_code`;

UPDATE action_types
SET score_type = 'Z';

UPDATE action_types
SET score_type = 'P'
WHERE evaluation_code = 'F';

UPDATE action_types
SET score_type = 'N'
WHERE evaluation_code = 'O';

UPDATE ratings
SET rating_weight = 1
where rating_weight = 0;

INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `created`, `modified`)
VALUES ('139fef38-6ee5-4a81-9f10-efb207451a45', 'scoring_method', 'Automated scoring (weighted or unweighted)', 'text',
        'weighted', 'grade', 1, 1, '2019-11-12 22:46:29', '2019-11-12 23:06:43');

--
-- Dumping data for table `configuration_types`
--

UPDATE `configuration_types`
SET `id`            = '139fef38-6ee5-4a81-9f10-efb207451a45',
    `name`          = 'scoring_method',
    `description`   = 'Automated scoring with override support',
    `data_type`     = 'select',
    `default_value` = 'weighted',
    `tab`           = 'grade',
    `is_editable`   = 1,
    `is_enabled`    = 1,
    `created`       = '2019-11-12 22:46:29',
    `modified`      = '2019-11-15 17:16:32'
WHERE `configuration_types`.`id` = '139fef38-6ee5-4a81-9f10-efb207451a45';
UPDATE `configuration_types`
SET `id`            = '20ee4d69-b928-4ce6-956a-24ce94ea5749',
    `name`          = 'session_grade_display',
    `description`   = '',
    `data_type`     = 'select',
    `default_value` = 'number',
    `tab`           = 'grade',
    `is_editable`   = 1,
    `is_enabled`    = 1,
    `created`       = '2019-07-02 21:02:17',
    `modified`      = '2019-11-15 17:17:32'
WHERE `configuration_types`.`id` = '20ee4d69-b928-4ce6-956a-24ce94ea5749';
UPDATE `configuration_types`
SET `id`            = 'f52ecf9d-2fef-476c-8040-bd40e2843c30',
    `name`          = 'career_grade_display',
    `description`   = '',
    `data_type`     = 'select',
    `default_value` = 'number',
    `tab`           = 'grade',
    `is_editable`   = 1,
    `is_enabled`    = 1,
    `created`       = '2019-07-02 23:03:15',
    `modified`      = '2019-11-15 17:15:20'
WHERE `configuration_types`.`id` = 'f52ecf9d-2fef-476c-8040-bd40e2843c30';

--
-- Dumping data for table `configuration_options`
--

INSERT INTO `configuration_options` (`id`, `configuration_type_id`, `name`, `value`, `sort_order`, `created`,
                                     `modified`)
VALUES ('1b590446-9518-4a24-883f-4dbe0cd810e8', '139fef38-6ee5-4a81-9f10-efb207451a45', 'Unweighted', 'unweighted', 2,
        '2019-11-15 17:16:11', '2019-11-15 17:16:11'),
       ('2a30fa06-4123-4d9c-a97a-831d9f54adcb', '139fef38-6ee5-4a81-9f10-efb207451a45', 'Weighted', 'weighted', 1,
        '2019-11-15 17:16:00', '2019-11-15 17:16:00'),
       ('2c2eeb35-101f-4511-b951-3f80226d8f90', '20ee4d69-b928-4ce6-956a-24ce94ea5749', 'Number', 'number', 1,
        '2019-11-15 17:17:03', '2019-11-15 17:18:35'),
       ('b8b52d91-5020-4587-9617-ac6f3dc768d0', 'f52ecf9d-2fef-476c-8040-bd40e2843c30', 'Grade (Configurable)', 'name',
        2, '2019-11-15 17:14:46', '2019-11-15 17:14:46'),
       ('cbbbbbc7-79c7-481a-ba4d-74ffd4e40b4a', 'f52ecf9d-2fef-476c-8040-bd40e2843c30', 'Number', 'number', 1,
        '2019-11-15 17:14:22', '2019-11-15 17:14:22'),
       ('dbb9ecab-ea2b-450f-9310-a7fc87d72a3e', '20ee4d69-b928-4ce6-956a-24ce94ea5749', 'Grade (Configurable)', 'name',
        2, '2019-11-15 17:17:15', '2019-11-15 17:17:15');
