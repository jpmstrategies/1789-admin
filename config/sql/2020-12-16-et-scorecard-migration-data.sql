-- DELETE ALL TENANT DATA
-- GET THE CORRECT TENANT_ID
-- do not delete tenants, users, accounts, configurations
DELETE
FROM 1789_cornerstone.ratings
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
DELETE
FROM 1789_cornerstone.records
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
DELETE
FROM 1789_cornerstone.legislators
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
DELETE
FROM 1789_cornerstone.people
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
DELETE
FROM 1789_cornerstone.addresses
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
DELETE
FROM 1789_cornerstone.address_types
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
DELETE
FROM 1789_cornerstone.actions
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
DELETE
FROM 1789_cornerstone.action_types
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
DELETE
FROM 1789_cornerstone.grades
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
DELETE
FROM 1789_cornerstone.parties
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
DELETE
FROM 1789_cornerstone.positions
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
DELETE
FROM 1789_cornerstone.rating_types
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
DELETE
FROM 1789_cornerstone.legislative_sessions
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
DELETE
FROM 1789_cornerstone.chambers
WHERE tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';

-- UPSERT tenants record(s)
INSERT INTO 1789_cornerstone.tenants
SELECT *
FROM et_ratings_temp.tenants src
ON DUPLICATE KEY UPDATE id = src.id;

-- INSERT chambers record(s)
INSERT INTO 1789_cornerstone.chambers
SELECT *
FROM et_ratings_temp.chambers;

-- INSERT legislative_sessions record(s)
INSERT INTO 1789_cornerstone.legislative_sessions
SELECT *
FROM et_ratings_temp.legislative_sessions;

-- INSERT rating_types record(s)
INSERT INTO 1789_cornerstone.rating_types
SELECT *
FROM et_ratings_temp.rating_types;

-- INSERT positions record(s)
INSERT INTO 1789_cornerstone.positions
SELECT *
FROM et_ratings_temp.positions;

-- INSERT parties record(s)
INSERT INTO 1789_cornerstone.parties
SELECT *
FROM et_ratings_temp.parties;

-- INSERT grades record(s)ß
INSERT INTO 1789_cornerstone.grades
SELECT *
FROM et_ratings_temp.grades;

-- INSERT action_types record(s)
INSERT INTO 1789_cornerstone.action_types
SELECT *
FROM et_ratings_temp.action_types;

-- INSERT actions record(s)
INSERT INTO 1789_cornerstone.actions
SELECT *
FROM et_ratings_temp.actions;

-- INSERT address_types record(s)
INSERT INTO 1789_cornerstone.address_types
SELECT *
FROM et_ratings_temp.address_types;

-- INSERT people record(s)
INSERT INTO 1789_cornerstone.people
SELECT *
FROM et_ratings_temp.people;

-- INSERT addresses record(s)
-- DEBUG
INSERT INTO 1789_cornerstone.addresses
SELECT *
FROM et_ratings_temp.addresses;

-- INSERT legislators record(s)
INSERT INTO 1789_cornerstone.legislators
SELECT *
FROM et_ratings_temp.legislators;

-- INSERT ratings record(s)
INSERT INTO 1789_cornerstone.ratings
SELECT *
FROM et_ratings_temp.ratings;

-- INSERT record(s) record(s)
INSERT INTO 1789_cornerstone.records
SELECT *
FROM et_ratings_temp.records;

-- upsert configurations
INSERT INTO 1789_cornerstone.configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM 1789_cornerstone.configuration_types ct
         CROSS JOIN 1789_cornerstone.tenants t
         LEFT JOIN 1789_cornerstone.configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;

-- remap grades using the following guidelines
# A+:	97-100
# A:	94-96
# A-:	90-93
# B+: 	87-89
# B:	84-86
# B-:	80-83
# C+: 	77-79
# C:	74-76
# C-:	70-73
# F:	69 and below

UPDATE 1789_cornerstone.`grades`
SET `lower_range` = 97,
    `upper_range` = 100,
    `sort_order`  = 10,
    `css_color`   = '#8ebd4e'
WHERE `grades`.`name` = 'A+'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE 1789_cornerstone.`grades`
SET `lower_range` = 94,
    `upper_range` = 96,
    `sort_order`  = 9,
    `css_color`   = '#8ebd4e'
WHERE `grades`.`name` = 'A'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE 1789_cornerstone.`grades`
SET `lower_range` = 90,
    `upper_range` = 93,
    `sort_order`  = 8,
    `css_color`   = '#8ebd4e'
WHERE `grades`.`name` = 'A-'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE 1789_cornerstone.`grades`
SET `lower_range` = 87,
    `upper_range` = 89,
    `sort_order`  = 7,
    `css_color`   = '#8ebd4e'
WHERE `grades`.`name` = 'B+'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE 1789_cornerstone.`grades`
SET `lower_range` = 84,
    `upper_range` = 86,
    `sort_order`  = 6,
    `css_color`   = '#8ebd4e'
WHERE `grades`.`name` = 'B'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE 1789_cornerstone.`grades`
SET `lower_range` = 80,
    `upper_range` = 83,
    `sort_order`  = 5,
    `css_color`   = '#8ebd4e'
WHERE `grades`.`name` = 'B-'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE 1789_cornerstone.`grades`
SET `lower_range` = 77,
    `upper_range` = 79,
    `sort_order`  = 4,
    `css_color`   = '#e5c21e'
WHERE `grades`.`name` = 'C+'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE 1789_cornerstone.`grades`
SET `lower_range` = 74,
    `upper_range` = 76,
    `sort_order`  = 3,
    `css_color`   = '#e5c21e'
WHERE `grades`.`name` = 'C'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE 1789_cornerstone.`grades`
SET `lower_range` = 70,
    `upper_range` = 73,
    `sort_order`  = 2,
    `css_color`   = '#e5c21e'
WHERE `grades`.`name` = 'C-'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE 1789_cornerstone.`grades`
SET `lower_range` = 0,
    `upper_range` = 69,
    `sort_order`  = 1,
    `css_color`   = '#ae3e25'
WHERE `grades`.`name` = 'F'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';

select *
from `1789_cornerstone`.people
         inner join `1789_cornerstone`.grades g on people.grade_id = g.id
where g.name IN ('D+', 'D', 'D-', 'F+', 'F-')
  AND g.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';

update `1789_cornerstone`.legislators
    inner join `1789_cornerstone`.grades g on legislators.grade_id = g.id
set legislators.grade_id = null
where g.name IN ('D+', 'D', 'D-', 'F+', 'F-')
  AND g.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';

update `1789_cornerstone`.people
    inner join `1789_cornerstone`.grades g on people.grade_id = g.id
set people.grade_id = null
where g.name IN ('D+', 'D', 'D-', 'F+', 'F-')
  AND g.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';

DELETE
FROM 1789_cornerstone.`grades`
WHERE `1789_cornerstone`.`grades`.`name` IN ('D+', 'D', 'D-', 'F+', 'F-')
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';

-- update Action Types
UPDATE `1789_cornerstone`.action_types
SET name        = 'Stance Evaluation*',
    icon_text   = '',
    export_text = null,
    css_color   = ''
WHERE name = 'Stance Evaluation*'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE `1789_cornerstone`.action_types
SET name        = 'Pro-Taxpayer',
    icon_text   = '',
    export_text = '1',
    css_color   = '#8ebd4e'
WHERE name = 'Favor'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE `1789_cornerstone`.action_types
SET name        = 'Anti-Taxpayer',
    icon_text   = '',
    export_text = '0',
    css_color   = '#a5290d'
WHERE name = 'Oppose'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE `1789_cornerstone`.action_types
SET name        = 'Absent',
    icon_text   = 'Absent',
    export_text = 'A',
    css_color   = '#251815'
WHERE name = 'Absent'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE `1789_cornerstone`.action_types
SET name        = 'Speaker',
    icon_text   = 'Speaker',
    export_text = 'S',
    css_color   = '#3c4485'
WHERE name = 'Speaker'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE `1789_cornerstone`.action_types
SET name        = 'Chair',
    icon_text   = 'Chair',
    export_text = 'CH',
    css_color   = '#251815'
WHERE name = 'Chair'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE `1789_cornerstone`.action_types
SET name        = 'Absent, Excused',
    icon_text   = 'A-EX',
    export_text = 'AE',
    css_color   = '#7a7a7a'
WHERE name = 'Absent, Excused'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE `1789_cornerstone`.action_types
SET name        = 'Present, not voting',
    icon_text   = 'PNV',
    export_text = 'PNV',
    css_color   = '#afafaf'
WHERE name = 'Present, not voting'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE `1789_cornerstone`.action_types
SET name        = 'Not Applicable',
    icon_text   = 'N/A',
    export_text = 'N/A',
    css_color   = '#7a7a7a'
WHERE name = 'Not Applicable'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';

-- update Positions
UPDATE `1789_cornerstone`.positions
SET display_code = 'O'
WHERE name = 'Oppose Amendment'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';
UPDATE `1789_cornerstone`.positions
SET display_code = 'F'
WHERE name = 'Support Amendment'
  AND tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';


select *
from `1789_cornerstone`.actions
         INNER JOIN `1789_cornerstone`.action_types at ON at.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4'

WHERE actions.name = 'A'
  AND actions.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4'
  AND at.name = 'Absent';


-- update actions
UPDATE `1789_cornerstone`.actions
    INNER JOIN `1789_cornerstone`.action_types at ON at.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4'
SET actions.action_type_id = at.id
WHERE actions.name = 'A'
  AND actions.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4'
  AND at.name = 'Absent';

UPDATE `1789_cornerstone`.actions
    INNER JOIN `1789_cornerstone`.action_types at ON at.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4'
SET actions.action_type_id = at.id
WHERE actions.name IN ('A-CM', 'AE')
  AND actions.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4'
  AND at.name = 'Absent, Excused';

UPDATE `1789_cornerstone`.actions
    INNER JOIN `1789_cornerstone`.action_types at ON at.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4'
SET actions.action_type_id = at.id
WHERE actions.name IN ('CH', 'PNV')
  AND actions.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4'
  AND at.name = 'Present, not voting';

UPDATE `1789_cornerstone`.actions
    INNER JOIN `1789_cornerstone`.action_types at ON at.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4'
SET actions.action_type_id = at.id
WHERE actions.name = 'SP'
  AND actions.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4'
  AND at.name = 'Speaker';

UPDATE `1789_cornerstone`.actions
    INNER JOIN `1789_cornerstone`.action_types at ON at.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4'
SET actions.name = 'Anti-Taxpayer'
WHERE actions.name = 'Unknown Action N'
  AND actions.tenant_id = 'e59f110e-3045-5598-99a9-9d402d523cf4';

-- ignore users

-- ignore accounts

-- run data sync script
