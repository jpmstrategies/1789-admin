drop view if exists legiscan_bills;

create view legiscan_bills as
select b.bill_id                       AS id,
       st.state_abbr                   AS state,
       b.bill_number                   AS bill_number,
       b.status_id                     AS legiscan_status_id,
       p.progress_desc                 AS status_desc,
       b.status_date                   AS status_date,
       b.title                         AS title,
       b.description                   AS description,
       b.bill_type_id                  AS legiscan_bill_type_id,
       t.bill_type_name                AS bill_type_name,
       t.bill_type_abbr                AS bill_type_abbr,
       b.body_id                       AS legiscan_chamber_id,
       bo1.body_abbr                   AS body_abbr,
       bo1.body_short                  AS body_short,
       bo1.body_name                   AS body_name,
       b.current_body_id               AS legiscan_current_chamber_id,
       bo2.body_abbr                   AS current_body_abbr,
       bo2.body_short                  AS current_body_short,
       bo2.body_name                   AS current_body_name,
       b.pending_committee_id          AS legiscan_pending_committee_id,
       c.committee_body_id             AS legiscan_pending_committee_chamber_id,
       bo3.body_abbr                   AS pending_committee_body_abbr,
       bo3.body_short                  AS pending_committee_body_short,
       bo3.body_name                   AS pending_committee_body_name,
       c.committee_name                AS pending_committee_name,
       b.legiscan_url                  AS legiscan_url,
       b.state_url                     AS state_url,
       b.change_hash                   AS change_hash,
       b.created                       AS created,
       b.updated                       AS updated,
       b.state_id                      AS legiscan_state_id,
       st.state_name                   AS state_name,
       b.session_id                    AS legiscan_session_id,
       s.year_start                    AS session_year_start,
       s.year_end                      AS session_year_end,
       s.prefile                       AS session_prefile,
       s.sine_die                      AS session_sine_die,
       s.prior                         AS session_prior,
       s.special                       AS session_special,
       s.session_tag                   AS session_tag,
       s.session_title                 AS session_title,
       s.session_name                  AS session_name,
       if(vote_counts.count > 1, 1, 0) AS has_bill_vote_details
from legiscan.ls_bill b
         join legiscan.ls_type t on b.bill_type_id = t.bill_type_id
         join legiscan.ls_session s on b.session_id = s.session_id
         join legiscan.ls_body bo1 on b.body_id = bo1.body_id
         join legiscan.ls_body bo2 on b.current_body_id = bo2.body_id
         left join legiscan.ls_committee c on b.pending_committee_id = c.committee_id
         left join legiscan.ls_body bo3 on c.committee_body_id = bo3.body_id
         left join legiscan.ls_progress p on b.status_id = p.progress_event_id
         join legiscan.ls_state st on b.state_id = st.state_id
         left join (select count(1) count, bill_id
                    from legiscan.ls_bill_vote_detail
                             join legiscan.ls_bill_vote v on ls_bill_vote_detail.roll_call_id = v.roll_call_id
                    group by bill_id) vote_counts
                   on b.bill_id = vote_counts.bill_id;

drop view if exists legiscan_bill_votes;

create view legiscan_bill_votes as
select bv.roll_call_id                 AS id,
       st.state_abbr                   AS state,
       b.bill_id                       AS legiscan_bill_id,
       b.bill_number                   AS bill_number,
       bv.roll_call_date               AS roll_call_date,
       bv.roll_call_desc               AS roll_call_desc,
       bv.roll_call_body_id            AS legiscan_bill_vote_chamber_id,
       bo.body_abbr                    AS roll_call_body_abbr,
       bo.body_short                   AS roll_call_body_short,
       bo.body_name                    AS roll_call_body_name,
       bv.yea                          AS yea,
       bv.nay                          AS nay,
       bv.nv                           AS nv,
       bv.absent                       AS absent,
       bv.total                        AS total,
       bv.passed                       AS passed,
       bv.legiscan_url                 AS legiscan_url,
       bv.state_url                    AS state_url,
       b.state_id                      AS legiscan_state_id,
       st.state_name                   AS state_name,
       b.session_id                    AS legiscan_session_id,
       b.body_id                       AS legiscan_chamber_id,
       b.current_body_id               AS legiscan_current_chamber_id,
       b.bill_type_id                  AS legiscan_bill_type_id,
       b.status_id                     AS legiscan_status_id,
       b.pending_committee_id          AS legiscan_pending_committee_id,
       if(vote_counts.count > 1, 1, 0) AS has_bill_vote_details,
       bv.created                      AS created,
       bv.updated                      AS updated
from legiscan.ls_bill b
         join legiscan.ls_bill_vote bv on b.bill_id = bv.bill_id
         join legiscan.ls_body bo on bv.roll_call_body_id = bo.body_id
         join legiscan.ls_state st on b.state_id = st.state_id
         left join (select count(1) count, roll_call_id
                    from legiscan.ls_bill_vote_detail
                    group by roll_call_id) vote_counts
                   on bv.roll_call_id = vote_counts.roll_call_id
where bo.role_id != 3;

drop view if exists legiscan_bill_vote_details;

create view legiscan_bill_vote_details as
select concat(bv.roll_call_id, '-', bvd.people_id) as id,
       st.state_abbr                               AS state,
       b.bill_id                                   AS legiscan_bill_id,
       bv.roll_call_id                             AS legiscan_bill_vote_id,
       bv.roll_call_body_id                        AS legiscan_bill_vote_chamber_id,
       bvd.people_id                               AS legiscan_people_id,
       b.bill_number                               AS bill_number,
       bvd.vote_id                                 AS legiscan_vote_id,
       v.vote_desc                                 AS vote_desc,
       p.party_id                                  AS legiscan_party_id,
       pa.party_abbr                               AS party_abbr,
       p.role_id                                   AS legiscan_role_id,
       r.role_abbr                                 AS role_abbr,
       r.role_name                                 AS role_name,
       p.name                                      AS name,
       p.first_name                                AS first_name,
       p.middle_name                               AS middle_name,
       p.last_name                                 AS last_name,
       p.suffix                                    AS suffix,
       p.nickname                                  AS nickname,
       p.ballotpedia                               AS ballotpedia,
       p.followthemoney_eid                        AS followthemoney_eid,
       p.votesmart_id                              AS votesmart_id,
       p.opensecrets_id                            AS opensecrets_id,
       p.knowwho_pid                               AS knowwho_pid,
       p.person_hash                               AS person_hash,
       p.district                                  AS person_district,
       people_body.body_id                         AS legiscan_people_chamber_id,
       b.state_id                                  AS legiscan_state_id,
       st.state_name                               AS state_name,
       b.session_id                                AS legiscan_session_id,
       b.body_id                                   AS legiscan_chamber_id,
       b.current_body_id                           AS legiscan_current_chamber_id,
       b.bill_type_id                              AS legiscan_bill_type_id,
       b.status_id                                 AS legiscan_status_id,
       b.pending_committee_id                      AS legiscan_pending_committee_id
from legiscan.ls_bill b
         join legiscan.ls_bill_vote bv on b.bill_id = bv.bill_id
         join legiscan.ls_body vote_body on vote_body.body_id = bv.roll_call_body_id
         join legiscan.ls_bill_vote_detail bvd on bv.roll_call_id = bvd.roll_call_id
         join legiscan.ls_vote v on bvd.vote_id = v.vote_id
         join legiscan.ls_people p on bvd.people_id = p.people_id
         join legiscan.ls_party pa on p.party_id = pa.party_id
         join legiscan.ls_role r on p.role_id = r.role_id
         join legiscan.ls_state st on b.state_id = st.state_id
         join legiscan.ls_body people_body on people_body.role_id = p.role_id and people_body.state_id = st.state_id
where vote_body.role_id != 3;

drop view if exists legiscan_chambers;

create view legiscan_chambers as
select st.state_abbr    as state,
       b.body_id        as id,
       b.body_abbr      as body_abbr,
       b.body_short     as body_short,
       b.body_name      as body_name,
       b.role_id        as role_id,
       b.body_role_abbr as role_abbr,
       b.body_role_name as role_name
from legiscan.ls_state st
         join legiscan.ls_body b on b.state_id = st.state_id
where b.role_id != 3;

drop view if exists legiscan_parties;

create view legiscan_parties as
select distinct st.state_abbr     as state,
                party.party_id    as id,
                party.party_abbr  as party_abbr,
                party.party_short as party_short,
                party.party_name  as party_name
from legiscan.ls_state st
         join legiscan.ls_people people on people.state_id = st.state_id
         join legiscan.ls_party party on party.party_id = people.party_id;

drop view if exists legiscan_sessions;

create view legiscan_sessions as
select st.state_abbr   as state,
       s.session_id    as id,
       s.year_start    as year_start,
       s.year_end      as year_end,
       s.prefile       as prefile,
       s.sine_die      as sine_die,
       s.prior         as prior,
       s.special       as special,
       s.session_name  as session_name,
       s.session_title as session_title,
       s.session_tag   as session_tag,
       s.import_date   as import_date,
       s.import_hash   as import_hash
from legiscan.ls_state st
         join legiscan.ls_session s on s.state_id = st.state_id;

drop view if exists legiscan_subjects;

create view legiscan_subjects as
select ls_state.state_abbr     as state,
       ls_subject.subject_id   as id,
       ls_subject.subject_name as subject_name
from legiscan.ls_subject
         inner join legiscan.ls_state on ls_subject.state_id = ls_state.state_id;

drop view if exists legiscan_bill_subjects;

create view legiscan_bill_subjects as
select st.state_abbr                         AS state,
       concat(bs.subject_id, '-', b.bill_id) AS id,
       bs.subject_id                         AS legiscan_subject_id,
       b.bill_id                             AS legiscan_bill_id,
       b.bill_number                         AS bill_number,
       s.subject_name                        AS subject_name,
       b.state_id                            AS legiscan_state_id,
       st.state_name                         AS state_name,
       b.session_id                          AS legiscan_session_id,
       b.body_id                             AS legiscan_chamber_id,
       b.current_body_id                     AS legiscan_current_chamber_id,
       b.bill_type_id                        AS legsican_bill_type_id,
       b.status_id                           AS legsican_status_id,
       b.pending_committee_id                AS legsican_pending_committee_id
from legiscan.ls_bill b
         join legiscan.ls_bill_subject bs on b.bill_id = bs.bill_id
         join legiscan.ls_subject s on bs.subject_id = s.subject_id
         join legiscan.ls_state st on b.state_id = st.state_id;

drop view if exists legiscan_actions;

create view legiscan_actions as
select v.vote_id as id,
       v.vote_desc as description
from legiscan.ls_vote v;

drop view if exists legiscan_vote_groups;
