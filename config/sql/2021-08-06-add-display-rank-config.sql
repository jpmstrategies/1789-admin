INSERT INTO configuration_types (id, name, description, data_type, default_value, tab, is_editable, is_enabled,
                                 is_public, created, modified)
VALUES ('9d466d8d-801e-4641-b5c1-529f4cf8bfe0', 'display_rank',
        'Display legislator rank based on calculated score', 'checkbox', 'no', 'grade', 1, 1, 1,
        '2021-08-06 22:41:52', '2021-08-06 22:42:20');

INSERT INTO configurations (id, configuration_type_id, tenant_id, value, created, modified)
SELECT UUID(),
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
    CROSS JOIN tenants t
    LEFT JOIN configurations c ON
    ct.id = c.configuration_type_id
    AND t.id = c.tenant_id
WHERE c.id IS NULL;
