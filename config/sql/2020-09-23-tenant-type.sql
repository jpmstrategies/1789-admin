use 1789_cornerstone;

ALTER TABLE `tenants`
    ADD `type` CHAR(1) NOT NULL DEFAULT 'S' AFTER `is_enabled`;

ALTER TABLE `tenants`
    ADD layout VARCHAR(50) DEFAULT 'default' NOT NULL AFTER type;