--
-- Dumping data for table `configuration_types`
--

INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `is_public`, `created`, `modified`)
VALUES ('09efc732-ed77-4237-ae56-855af80384e9', 'cloudflare_purge_method', '', 'select', 'everything', 'admin', 1, 1, 0,
        '2020-12-09 20:14:20', '2020-12-09 20:16:05'),
       ('54e00acd-a4c7-49cc-8692-064d3a8376f9', 'cloudflare_api_key', '', 'text', '', 'admin', 1, 1, 0,
        '2020-12-09 20:13:13', '2020-12-09 20:13:13'),
       ('7d68570e-dbf8-49f2-99b9-cd33afee5a07', 'cloudfront_key_id', '', 'text', '', 'admin', 1, 1, 0,
        '2020-12-09 20:11:36', '2020-12-09 20:11:36'),
       ('91706ad9-b52f-4a39-8676-672a90f74643', 'cloudfront_access_key', '', 'text', '', 'admin', 1, 1, 0,
        '2020-12-09 20:11:51', '2020-12-09 20:11:51'),
       ('a2215e4d-fceb-4f3e-a384-4a193666153d', 'cloudflare_api_zone', '', 'text', '', 'admin', 1, 1, 0,
        '2020-12-09 20:13:03', '2020-12-09 20:13:03'),
       ('e79271c9-df67-4222-a8e4-17a228c6cbb0', 'cloudfront_distribution', '', 'text', '', 'admin', 1, 1, 0,
        '2020-12-09 20:12:32', '2020-12-09 20:12:32');

--
-- Dumping data for table `configuration_options`
--

INSERT INTO `configuration_options` (`id`, `configuration_type_id`, `name`, `value`, `sort_order`, `created`,
                                     `modified`)
VALUES ('03629ad5-4a21-4c73-982a-21d31052a483', '09efc732-ed77-4237-ae56-855af80384e9', 'Tag', 'tag', 1,
        '2020-12-09 20:15:19', '2020-12-09 20:15:19'),
       ('8a381dc1-2023-45e0-abea-d1840d1c4b4a', '09efc732-ed77-4237-ae56-855af80384e9', 'Purge Everything',
        'everything', 2, '2020-12-09 20:15:50', '2020-12-09 20:15:50');

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;