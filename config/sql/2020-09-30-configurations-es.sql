INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `created`, `modified`)
VALUES ('39003b3f-867d-4752-8ea2-4ac3b917638f', 'es_host_url', 'Elasticsearch - API Endpoint URL (no trailing slash)',
        'text', '', 'admin',
        1,
        1,
        '2020-09-30 03:45:13',
        '2020-09-30 03:45:13'),
       ('59f8e2c8-4e6c-45b9-8a55-68c8059076a1', 'es_index_name',
        'Elasticsearch - WordPress index name',
        'text', '', 'admin', 1, 1, '2020-09-30 03:45:30',
        '2020-09-30 03:45:30'),
       ('d9b26bba-d040-4db1-bd88-e3d74abe3d6e', 'es_wp_domain_name',
        'Elasticsearch - WordPress domain name for proxy validation (no trailing slash)', 'text', '', 'admin', 1, 1,
        '2020-09-30 03:45:46', '2020-09-30 03:45:46'),
       ('227ae5a9-ca15-41ff-b31f-b9c6c02aba0d', 'es_wp_show_posts',
        'Display related posts from WordPress (Requires Elasticsearch)',
        'checkbox',
        'no', 'admin', 1, 1,
        '2020-09-30 03:49:07', '2020-09-30 03:49:07')
on duplicate key update description   = values(description),
                        data_type     = values(data_type),
                        default_value = values(default_value),
                        tab           = values(tab),
                        is_editable   = values(is_editable),
                        is_enabled    = values(is_enabled),
                        modified      = now();

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;