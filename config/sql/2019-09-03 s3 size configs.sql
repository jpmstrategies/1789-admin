-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 03, 2019 at 06:19 PM
-- Server version: 5.7.25
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `1789_cornerstone`
--

--
-- Dumping data for table `configuration_types`
--

REPLACE INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                    `is_enabled`, `created`, `modified`)
VALUES ('2a4d38a6-4808-4698-830a-aec60da05080', 's3_image_size_lookup', 'Valid options: small, medium or large', 'text',
        'large', 'site', 1, 1, '2019-09-03 23:00:01', '2019-09-03 23:14:47'),
       ('323f1c21-8fbd-439b-a958-d32e8371de80', 's3_image_size_home', 'Valid options: small, medium or large', 'text',
        'medium', 'home', 1, 1, '2019-09-03 22:51:45', '2019-09-03 23:14:37'),
       ('db081ea1-50f1-4581-bccb-0efd0cf94933', 's3_image_size_vote', 'Valid options: small, medium or large', 'text',
        'small', 'vote', 1, 1, '2019-09-03 22:52:05', '2019-09-03 23:14:52'),
       ('f1d3b2bc-01ba-478c-87c3-f3a43b40dc29', 's3_image_size_legislator', 'Valid options: small, medium or large',
        'text', 'large', 'legislator', 1, 1, '2019-09-03 22:52:35', '2019-09-03 23:14:42');

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;

-- update default after migration
UPDATE configuration_types
SET default_value = 'large'
WHERE id = '2a4d38a6-4808-4698-830a-aec60da05080';
UPDATE configuration_types
SET default_value = 'medium'
WHERE id = '323f1c21-8fbd-439b-a958-d32e8371de80';
UPDATE configuration_types
SET default_value = 'small'
WHERE id = 'db081ea1-50f1-4581-bccb-0efd0cf94933';
UPDATE configuration_types
SET default_value = 'large'
WHERE id = 'f1d3b2bc-01ba-478c-87c3-f3a43b40dc29';

UPDATE configurations
SET value = 'large'
WHERE configuration_type_id = '2a4d38a6-4808-4698-830a-aec60da05080';
UPDATE configurations
SET value = 'medium'
WHERE configuration_type_id = '323f1c21-8fbd-439b-a958-d32e8371de80';
UPDATE configurations
SET value = 'small'
WHERE configuration_type_id = 'db081ea1-50f1-4581-bccb-0efd0cf94933';
UPDATE configurations
SET value = 'large'
WHERE configuration_type_id = 'f1d3b2bc-01ba-478c-87c3-f3a43b40dc29';

UPDATE configurations
SET value = 'large'
WHERE configuration_type_id = '2a4d38a6-4808-4698-830a-aec60da05080'
  AND tenant_id = '84d73741-3c04-5c06-9dbc-ce8671d27fd2';
UPDATE configurations
SET value = 'large'
WHERE configuration_type_id = '323f1c21-8fbd-439b-a958-d32e8371de80'
  AND tenant_id = '84d73741-3c04-5c06-9dbc-ce8671d27fd2';
UPDATE configurations
SET value = 'large'
WHERE configuration_type_id = 'db081ea1-50f1-4581-bccb-0efd0cf94933'
  AND tenant_id = '84d73741-3c04-5c06-9dbc-ce8671d27fd2';
UPDATE configurations
SET value = 'large'
WHERE configuration_type_id = 'f1d3b2bc-01ba-478c-87c3-f3a43b40dc29'
  AND tenant_id = '84d73741-3c04-5c06-9dbc-ce8671d27fd2';