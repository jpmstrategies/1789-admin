INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `is_public`, `created`, `modified`)
VALUES ('5a878460-d23f-4bcb-84bc-346fc7f71509', 'tusa_widget_election_year', 'Election year to display', 'text', '',
        'admin', 1, 1, 1, '2021-06-15 20:15:47', '2021-06-15 20:18:38');

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;

update configurations
set value = '2020'
where configuration_type_id = '5a878460-d23f-4bcb-84bc-346fc7f71509'