UPDATE `configuration_types`
SET `name` = 'tusa_default_slug'
WHERE `name` = 'ttx_default_slug';

UPDATE `configuration_types`
SET `description` = 'Enable the Transparency USA integration',
    `name`        = 'tusa_widget'
WHERE `name` = 'ttx_widget';