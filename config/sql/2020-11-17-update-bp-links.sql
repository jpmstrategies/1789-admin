UPDATE people
SET ballotpedia_url = replace(ballotpedia_url, 'http://', 'https://');

UPDATE people
SET ballotpedia_url = replace(ballotpedia_url, '/wiki/index.php', '');