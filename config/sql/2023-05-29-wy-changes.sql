INSERT INTO configuration_types (id, name, description, data_type, default_value, tab, is_editable,
                                                    is_enabled, is_public, created, modified)
VALUES ('7c9d902c-c7b2-47bd-9e39-e764dfed527d', 'use_criteria', 'Enable criteria scoring', 'checkbox', 'no', 'admin', 1,
        1, 1, '2023-05-25 21:16:27', '2023-05-30 18:12:04')
on duplicate key update name = values(name);

create table criterias
(
    id             char(36)         not null primary key,
    tenant_id      char(36)         not null,
    # CONSIDER:
    # As you mentioned, these could be per rating_type.
    # No tenant is expected to use both, so I don't know which would make more sense.
    #
    # It might be a question of how difficult each approach is in cake regarding getting
    # a list of valid actions for each `criteria`, or whether the defined criteria should
    # apply to all rating types, or just a single one.
    # rating_type_id      char(36)         not null,
    name           varchar(45)      not null,
    th_detail_name varchar(20)      not null,
    th_vote_name   varchar(20)      not null,
    method         char default 'S' not null,
    party_id       char(36)         null,
    sort_order     int              null,
    created        datetime         null,
    modified       datetime         null,
    constraint XUN1_criteria
        unique (tenant_id, name),
    constraint XUN2_criteria
        unique (tenant_id, party_id),
    constraint XFK1_criteria
        foreign key (tenant_id) references tenants (id),
    constraint XFK2_criteria
        foreign key (party_id) references parties (id)
) charset = utf8mb3;


# The way I'm starting to think of this more clearly is that ratings are configured like they always have been,
# even with a rating type, so that it still has it's list of valid actions. This table provides a list
# of 'multiple overrides' for a rating and its rating type.
# ie.
# - The criteria will override most of the rating_type fields, but not its action list.
# - The action overrides the rating's action.
create table rating_criterias
(
    id          char(36) not null primary key,
    rating_id   char(36) not null,
    criteria_id char(36) not null,
    action_id   char(36) not null,
    # CONSIDER:
    # You brought this up a couple days ago.
    # It seems like it could be reasonable, but I'm not sure.
    # position_id char(36) null,
    constraint XUN1_rating_criterias
        unique (rating_id, criteria_id),
    constraint XFK1_rating_criterias
        foreign key (rating_id) references ratings (id),
    constraint XFK2_rating_criterias
        foreign key (criteria_id) references criterias (id),
    constraint XFK3_rating_criterias
        foreign key (action_id) references actions (id)
) charset = utf8mb3;

alter table rating_criterias
    modify column action_id char(36) null,
    add tenant_id char(36) not null after id,
    add constraint XFK4_rating_criterias
        foreign key (tenant_id) references tenants (id);

create table criteria_details
(
    id          char(36)    not null primary key,
    tenant_id   char(36)    not null,
    criteria_id char(36)    not null,
    name        varchar(45) not null,
    sort_order  int         null,
    created     datetime    null,
    modified    datetime    null,
    constraint XUN1_criteria_details
        unique (criteria_id, name),
    constraint XUN2_criteria_details
        unique (criteria_id, sort_order),
    constraint XFK1_criteria_details
        foreign key (tenant_id) references tenants (id),
    constraint XFK2_criteria_details
        foreign key (criteria_id) references criterias (id)
) charset = utf8mb3;

alter table rating_criterias
    add criteria_detail_id char(36) null after criteria_id,
    add constraint XFK5_rating_criterias
        foreign key (criteria_detail_id) references criteria_details (id);

alter table tenants_bills
    modify `status` varchar (255) null;

update tenants_bills
set `status` = null
where `status` = 'PENDING';

# CONSIDER:
# May want to enforce these being null/un-configurable in the admin tool when `use_criteria` is `yes`
# Otherwise we could allow it as the default action, and it would be overridden by an action in the `rating_criterias`
# table (and a `rating_criterias` record would not be required).
alter table ratings
    # modify rating_type_id char(36) null,
    modify action_id char(36) null,
    modify position_id char(36) null;
