-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 01, 2019 at 03:05 PM
-- Server version: 5.7.25
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `1789_cornerstone`
--

--
-- Dumping data for table `configuration_types`
--

REPLACE INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                    `is_enabled`, `created`, `modified`)
VALUES ('15347390-002f-4f10-8623-6bdd57495923', 'header_org_position',
        'Column header text for the Organization\'s Position (Actual mode)', 'text', 'Org\'s Position', 'display', 1, 1,
        '2019-08-01 05:24:03', '2019-08-01 20:45:52'),
       ('2f5d8d4f-a6b1-4955-a25b-6d3f8dc00e8c', 'position_display_appearance', 'Valid options: ovals or thumbs', 'text',
        'ovals', 'display', 1, 1, '2019-07-15 16:34:51', '2019-08-01 20:46:48'),
       ('819e73a7-346f-4fd5-921b-ed33a2b681ae', 'homepage_vote_header_bill',
        'Column header text for Rating Column', 'text', 'Bill', 'home', 1, 1,
        '2019-07-15 16:33:09', '2019-07-15 16:33:09'),
       ('431fda5b-1da2-4f42-8f89-0b36d2428fef', 'header_rating_column', 'Column header text for the Rating column',
        'text', 'Bill', 'display', 1, 1, '2019-07-15 16:35:22', '2019-08-01 20:45:22'),
       ('7dfe182a-c635-4679-bc72-5932750e1177', 'position_display_type', 'Valid options: actual or evaluation', 'text',
        'evaluation', 'display', 1, 1, '2019-08-01 05:24:58', '2019-08-01 05:25:48'),
       ('ee24eff3-43d3-46f4-b45e-4aa89d479e93', 'homepage_legislator_sort',
        'Valid options: lastName, -lastName, district, -district, party, -party, score, -score',
        'text', '-score', 'home', 1, 1, '2019-07-15 16:32:12', '2019-07-15 16:32:12');

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;

-- non-CAP specific changes
update configurations
set value = 'evaluation'
where configuration_type_id = '7dfe182a-c635-4679-bc72-5932750e1177';

update configurations
set value = 'ovals'
where configuration_type_id = '2f5d8d4f-a6b1-4955-a25b-6d3f8dc00e8c';

update configurations
set value = 'Org\'s Position'
where configuration_type_id = '15347390-002f-4f10-8623-6bdd57495923';

update configurations
set value = '-score'
where configuration_type_id = 'ee24eff3-43d3-46f4-b45e-4aa89d479e93';

update configurations
set value = 'Bill'
where configuration_type_id = '431fda5b-1da2-4f42-8f89-0b36d2428fef';

update configurations
set value = 'Bill'
where configuration_type_id = '819e73a7-346f-4fd5-921b-ed33a2b681ae';

-- CAP specific changes
update configurations
set value = 'actual'
where tenant_id = '0f282b29-70a6-575e-abbe-1ff806c4cadf'
  and configuration_type_id = '7dfe182a-c635-4679-bc72-5932750e1177';

update configurations
set value = 'thumbs'
where tenant_id = '0f282b29-70a6-575e-abbe-1ff806c4cadf'
  and configuration_type_id = '2f5d8d4f-a6b1-4955-a25b-6d3f8dc00e8c';

update configurations
set value = 'CAP\'s Position'
where tenant_id = '0f282b29-70a6-575e-abbe-1ff806c4cadf'
  and configuration_type_id = '15347390-002f-4f10-8623-6bdd57495923';

update configurations
set value = 'lastName'
where tenant_id = '0f282b29-70a6-575e-abbe-1ff806c4cadf'
  and configuration_type_id = 'ee24eff3-43d3-46f4-b45e-4aa89d479e93';

update configurations
set value = 'Scored Vote'
where tenant_id = '0f282b29-70a6-575e-abbe-1ff806c4cadf'
  and configuration_type_id = '431fda5b-1da2-4f42-8f89-0b36d2428fef';

update configurations
set value = 'Scored Votes'
where tenant_id = '0f282b29-70a6-575e-abbe-1ff806c4cadf'
  and configuration_type_id = '819e73a7-346f-4fd5-921b-ed33a2b681ae';

update rating_types
SET th_detail_name = 'Scored Vote',
    th_vote_name   = 'Member\'s Position'
where id = 'dcdbcdf6-420a-47da-8739-826f5cc0c38a';