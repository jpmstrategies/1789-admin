-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 27, 2019 at 12:00 AM
-- Server version: 5.7.25
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `1789_cornerstone`
--

--
-- Dumping data for table `configuration_types`
--

INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `created`, `modified`)
VALUES ('d4e4d850-f351-4e07-9a55-aafafa5f6e05', 'allow_search_engines', 'Allow search engines to index this site',
        'checkbox', 'no', 'site', 1, 1, '2019-08-27 04:59:36', '2019-08-27 04:59:36');

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;