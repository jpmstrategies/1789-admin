alter table people
    add column `committee_list` longtext after `bio`;

update people
set people.committee_list = people.bio
where people.bio like '<u>Committee Assignments%';

update people
set people.bio = null
where people.bio like '<u>Committee Assignments%';

update people
set people.committee_list = replace(people.committee_list, '<u>Committee Assignments</u><br />', '');
