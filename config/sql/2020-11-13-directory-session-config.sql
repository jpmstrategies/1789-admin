--
-- Database: `1789_cornerstone`
--

--
-- Dumping data for table `configuration_types`
--

INSERT INTO `configuration_types` (`id`, `name`, `description`, `data_type`, `default_value`, `tab`, `is_editable`,
                                   `is_enabled`, `is_public`, `created`, `modified`)
VALUES ('1287e15b-146f-4469-be5c-b90b8c622a92', 'directory_active_session_slug',
        'Directory - Legislative Session Slug to be displayed', 'text', '', 'site', 1, 1, 1, '2020-11-13 20:38:58',
        '2020-11-13 20:38:58');

-- Insert configuration values for new configuration types
INSERT INTO configurations
SELECT UUID()           as id,
       ct.id            as configuration_type_id,
       t.id             as tenant_id,
       ct.default_value as value,
       NOW()            as created,
       NOW()            as modified
FROM configuration_types ct
         CROSS JOIN tenants t
         LEFT JOIN configurations c ON
        ct.id = c.configuration_type_id
        AND t.id = c.tenant_id
WHERE c.id IS NULL;