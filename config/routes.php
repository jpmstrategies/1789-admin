<?php
/**
 * Routes configuration.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * It's loaded within the context of `Application::routes()` method which
 * receives a `RouteBuilder` instance `$routes` as method argument.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

/*
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 */
/** @var \Cake\Routing\RouteBuilder $routes */
$routes->setRouteClass(DashedRoute::class);

$routes->scope('/', function (RouteBuilder $builder) {
    /*
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, templates/Pages/home.php)...
     */
    $builder->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

    /*
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $builder->connect('/pages/*', 'Pages::display');
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.

    // bulk edit pages
    $builder->connect('/legislators/bulkedit/{sessionId}/{chamber}', array('controller' => 'Legislators', 'action' => 'bulkedit'));
    $builder->connect('/people/bulkedit/{sessionId}/{chamber}', array('controller' => 'People', 'action' => 'bulkedit'));
    $builder->connect('/ratings/bulkedit/{sessionId}/{chamber}', array('controller' => 'Ratings', 'action' => 'bulkedit'));
    $builder->connect('/rating-criterias/bulkedit/{sessionId}', array('controller' => 'RatingCriterias', 'action' => 'bulkedit'));
    $builder->connect('/rating-criterias/bulkedit/{sessionId}/{ratingId}', array('controller' => 'RatingCriterias', 'action' => 'bulkedit'));
    $builder->connect('/records/bulkedit/{ratingId}', array('controller' => 'Records', 'action' => 'bulkedit'));
    $builder->connect('/scores/bulkedit/{sessionId}/{chamber}/{orgId}', array('controller' => 'Scores', 'action' => 'bulkedit'));

    // public user pages
    $builder->connect('/forgot-password', array('controller' => 'users', 'action' => 'forgotPassword'));
    $builder->connect('/reset-password', array('controller' => 'users', 'action' => 'resetPassword'));
    $builder->connect('/login', array('controller' => 'users', 'action' => 'login'));
    $builder->connect('/logout', array('controller' => 'users', 'action' => 'logout'));
    $builder->connect('/signup', array('controller' => 'users', 'action' => 'signup'));
    $builder->connect('/finish-signup', array('controller' => 'users', 'action' => 'finishSignup'));

    // public account pages
    $builder->connect('/invite/*', array('controller' => 'accounts', 'action' => 'invite'));
    $builder->connect('/verify/*', array('controller' => 'accounts', 'action' => 'verify'));

    // public policy pages
    $builder->connect('/terms', array('controller' => 'pages', 'action' => 'terms'));
    $builder->connect('/privacy-policy', array('controller' => 'pages', 'action' => 'privacyPolicy'));
    $builder->connect('/cookie-policy', array('controller' => 'pages', 'action' => 'cookiePolicy'));

    // app-specific pages

    // password protected

    // jpm functions
    $builder->connect('/jpm/invalidate', array('controller' => 'Pages', 'action' => 'cacheInvalidate'));

    /*
     * Connect catchall routes for all controllers.
     *
     * The `fallbacks` method is a shortcut for
     *
     * ```
     * $builder->connect('/:controller', ['action' => 'index']);
     * $builder->connect('/:controller/:action/*', []);
     * ```
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $builder->fallbacks();
});

/*
 * If you need a different set of middleware or none at all,
 * open new scope and define routes there.
 *
 * ```
 * $routes->scope('/api', function (RouteBuilder $builder) {
 *     // No $builder->applyMiddleware() here.
 *
 *     // Parse specified extensions from URLs
 *     // $builder->setExtensions(['json', 'xml']);
 *
 *     // Connect API actions here.
 * });
 * ```
 */

$routes->prefix('admin', function (RouteBuilder $routes) {
    // All routes here will be prefixed with `/admin`, and
    // have the `'prefix' => 'Admin'` route element added that
    // will be required when generating URLs for these routes
    $routes->fallbacks(DashedRoute::class);
});

$routes->prefix('api', function (RouteBuilder $routes) {
    $routes->setExtensions(['json', 'xml']);

    $routes->resources('action-types');
    $routes->resources('actions');
    $routes->resources('address-types');
    $routes->resources('addresses');
    $routes->resources('chambers');
    $routes->resources('grades');
    $routes->resources('legislative-sessions');
    $routes->resources('legislators');
    $routes->resources('organizations');


    // All routes here will be prefixed with `/admin`, and
    // have the `'prefix' => 'Admin'` route element added that
    // will be required when generating URLs for these routes
    $routes->fallbacks(DashedRoute::class);
});

$routes->setExtensions(['csv']);
