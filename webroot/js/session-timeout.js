function sessionTimeout(maxTime) {
  // checkSessionUrl must be the same in `extendSession()` and
  // `XMLHttpRequest.prototype.open` handler.
  const checkSessionUrl = '/users/check-session'
  let timeoutTimerId
  let countdownTimerId
  let logoutTimerId
  let debugMode = false

  function debugWrite(sMessage, sFunctionName) {
    if (debugMode === true) {
      console.log(sFunctionName + ': ' + sMessage)
    }
  }

  function startTimer(duration, display) {
    debugWrite('Starting', 'startTimer')
    let start = Date.now(),
      diff,
      minutes,
      seconds

    function timer() {
      // get the number of seconds that have elapsed since
      // startTimer() was called
      diff = duration - (((Date.now() - start) / 1000) | 0)

      // does the same job as parseInt truncates the float
      minutes = (diff / 60) | 0
      seconds = diff % 60 | 0

      minutes = minutes < 10 ? '0' + minutes : minutes
      seconds = seconds < 10 ? '0' + seconds : seconds

      display.textContent = minutes + ':' + seconds
      debugWrite(minutes + ':' + seconds, 'timer')

      if (diff <= 0) {
        // add one second so that the count down starts at the full duration
        // example 05:00 not 04:59
        start = Date.now() + 1000
      }
    }

    // we don't want to wait a full second before the timer starts
    timer()
    return setInterval(timer, 1000)
  }

  function extendSession() {
    debugWrite('Starting', 'extendSession')
    $.ajax({
      type: 'GET',
      url: checkSessionUrl,
      global: false,
      success: function (data) {
        debugWrite(`new timeout: ${data.timeout}`, 'extendSession')
        timeoutStart()
      },
    })
  }

  function logout(source) {
    debugWrite(`Starting logout with source: ${source}`, 'logout')
    location.href = `/users/logout?source=${source}&redirect=${window.location.pathname}`
  }

  function timeoutStart() {
    debugWrite('Starting', 'timeoutStart')
    let phpTimeout = parseInt(maxTime)
    debugWrite(`phpTimeout: ${phpTimeout}`, 'timeoutStart')
    let timeoutCountdownMs = (phpTimeout - 30) * 1000
    clearTimeout(timeoutTimerId)
    timeoutTimerId = setTimeout(function () {
      clearInterval(countdownTimerId)
      countdownTimerId = startTimer(
        30,
        document.querySelector('#timeout-remaining')
      )
      $('#session-timeout').modal('toggle')

      clearTimeout(logoutTimerId)
      logoutTimerId = setTimeout(() => {
        clearInterval(countdownTimerId)
        logout(`timeout`)
      }, 30 * 1000)
    }, timeoutCountdownMs) // check every 2.5 minute
  }

  function timeoutReset() {
    debugWrite('Starting', 'timeoutReset')
    clearTimeout(timeoutTimerId)
    clearInterval(countdownTimerId)
    clearTimeout(logoutTimerId)
    extendSession()
  }

  $('#extend-timeout-session').on('click', function () {
    debugWrite('Clicked extendSession', 'onClick')
    $('#session-timeout').modal('toggle')
    timeoutReset()
  })

  $('#session-logout').on('click', function () {
    debugWrite('Clicked logout', 'onClick')
    logout(`user`)
  })

  // This solution only triggered on jquery .ajax requests.
  // jQuery(document).ajaxStop(function () {
  //     debugWrite('Triggered ajaxStop handler', 'ajaxStop');
  //     timeoutReset()
  // });

  // This will trigger on all XMLHttpRequests, which most (all?) other
  // libraries use.
  let origOpen = XMLHttpRequest.prototype.open
  XMLHttpRequest.prototype.open = function () {
    // Do not trigger on `extendSession()` because it would infinitely loop.
    if (arguments[1] !== checkSessionUrl) {
      timeoutReset()
    }
    origOpen.apply(this, arguments)
  }

  timeoutStart()
}
