function serializeFormJson(fields) {
    let o = {};
    let a = fields.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
}

jQuery(document).ready(function ($) {
    // Fixed header.
    $('.table-fixed-header').fixedHeader();

    // Get submit url from the form.
    let bulkForm = $('#RecordAdminBulkeditForm');
    let apiUrl = $("[name='saveUrl']").val();
    let csrfToken = $('[name="_csrfToken"]').val();

    bulkForm.find(':input').on("change", function (e) {
        let row = $(e.target).parents('tr');
        let tds = row.children('td');
        let fields = row.find(':input');
        let formData = serializeFormJson(fields);

        let actionLabel = row.find('option:selected').text();

        // Action id is required for the record to exist.
        if (formData.action_id) {
            formData.action = 'update-row';
            //let payload = JSON.stringify(formData);

            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                // dataType: 'json',
                // contentType: 'application/json',
                // accepts: {json: 'application/json'},
                type: 'POST',
                evalScripts: true,
                url: apiUrl,
                data: formData,
                success: function (data) {
                    let idField = fields.filter('[name="id"]');
                    let id = idField.val();
                    if (id === data.id) {
                        // Record was updated.
                        tds.removeClass('table-warning', 500).addClass('table-success', 500).delay(1000).removeClass('table-success', 500);
                    } else {
                        // Record was inserted.
                        idField.val(data.id);
                        fields.filter('[name="action_id"]').children('[value=""]').remove();
                        tds.switchClass('table-danger', 'table-success', 500).delay(1000).removeClass('table-success', 500);
                    }

                    if (actionLabel === 'N/A') {
                        tds.addClass('table-warning', 500);
                    }
                },
                error: function () {
                    tds.addClass('table-danger', 500);
                }
            });
        }
    });

    // Filter form.
    let filterForm = $('#FilterAdminBulkeditForm');
    let filters = filterForm.find('.update-ratings');

    filters.on("change", function (e) {
        let formData = serializeFormJson(filters);
        formData.action = 'fetch-ratings';
        //let payload = JSON.stringify(formData);

        $.ajax({
            headers: {
                'X-CSRF-Token': csrfToken
            },
            // dataType: 'json',
            // contentType: 'application/json',
            // accepts: {json: 'application/json'},
            type: 'POST',
            evalScripts: true,
            url: apiUrl,
            data: formData,
            success: function (data) {
                let select = filterForm.find('[name="rating"]');
                select.empty();
                if (data.length === 0) {
                    select.hide();
                } else {
                    select.show();
                    $.each(data, function (value, key) {
                        select.append($('<option></option>').attr('value', value).text(key));
                    });
                    select.trigger('change');
                }
            }
        });
    });

    // Filter form.
    let ratingFilter = filterForm.find('[name="rating"]');
    let filterRedirectUrl = filterForm.find('[name="url"]').val();

    ratingFilter.on("change", function (e) {
        let formData = serializeFormJson(ratingFilter);
        window.location.replace(filterRedirectUrl + '/' + formData.rating);
    });
});
