jQuery(document).ready(function ($) {
    let chamberVal;
    let sessionVal;
    let ratingAjaxUrl = $('[name="rating_ajax_url"]').val();
    let actionAjaxUrl = $('[name="action_ajax_url"]').val();
    let bulkEditUrl = $('[name="bulkedit_url"]').val() + '/';
    let selectedId = $('[name="selected_id"]').val();

    function updateActionDropDown() {

        let ratingVal = $('#rating-id').val();

        if (ratingVal !== null) {
            let targetUrl = actionAjaxUrl + '?rating_id=' + ratingVal;

            let actionSelect = $('#action-id');
            let options;

            $.ajax({
                type: 'get',
                url: targetUrl,
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                },
                success: function (response) {

                    if (response) {

                        actionSelect.empty();

                        for (let dbId in response) {
                            if (response.hasOwnProperty(dbId)) {
                                options += '<option value=' + dbId + '>' + response[dbId] + '</option>';
                            }
                        }

                        actionSelect.append(options);

                        // check if N/A exists as a choice
                        // then set as default
                        if ('N/A' in response) {
                            actionSelect.val(response['N/A']);
                        }

                    }
                },
                error: function (e) {
                    alert('An error occurred fetching actions: ' + e.responseText.message);
                }
            });
        }
    }

    function updateRatingDropDown() {

        chamberVal = $('#chamber').val();
        sessionVal = $('#legislative-session-id').val();

        let targetUrl = ratingAjaxUrl + '?chamber=' + chamberVal + '&legislative_session_id=' + sessionVal;

        let ratingSelect = $('#rating-id');
        let options;

        $.ajax({
            type: 'get',
            url: targetUrl,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            },
            success: function (response) {

                if (response) {

                    ratingSelect.empty();

                    for (let dbId in response) {
                        if (response.hasOwnProperty(dbId)) {
                            options += '<option value=' + dbId + '>' + response[dbId] + '</option>';
                        }
                    }

                    ratingSelect.append(options);

                    if (selectedId !== undefined) {
                        ratingSelect.val(selectedId);
                    }

                    updateActionDropDown();
                }
            },
            error: function (e) {
                alert('An error occurred fetching ratings: ' + e.responseText.message);
            }
        });
    }

    $('#chamber').on("change", function () {
        updateRatingDropDown();
    });

    $('#legislative-session-id').on("change", function (e) {
        updateRatingDropDown();
    });

    $('#rating-id').on("change", function () {
        updateActionDropDown();
        $('#BulkEditLink').attr('href', bulkEditUrl + this.value);
    });

    updateRatingDropDown();
});