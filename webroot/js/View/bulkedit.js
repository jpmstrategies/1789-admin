function serializeFormJson(fields) {
    let o = {};
    let a = fields.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
}

jQuery(document).ready(function ($) {
    let tenantState = $('.tenant-state').val()

    if (tenantState) {
        tenantState = tenantState.toLowerCase()
    }

    // Fixed header.
    $('.table-fixed-header').fixedHeader();

    // Select2.
    $('.ttx-candidate-select').select2({
        theme: 'bootstrap4',
        placeholder: "Search for a legislator",
        allowClear: true,
        minimumInputLength: 2,
        ajax: {
            url: $('.ttx-lookup-url').val(),
            dataType: 'json',
            data: function (params) {
                // Set query
                return {
                    state: tenantState,
                    apiType: 'entities',
                    aliasType: 'candidate',
                    searchTerm: params.term,
                };
            },
            processResults: function (data) {
                // Transforms the top-level key of the response object from 'items' to 'results'
                return {
                    results: data.results.map(function (item) {
                        return {
                            id: item.slug,
                            text: item.name
                        };
                    })
                };
            },
            delay: 250
        }
    });

    // Get submit url from the form.
    let bulkForm = $('.form-bulkedit');
    let saveRowUrl = $("[name='saveUrl']").val();
    let csrfToken = $('[name="_csrfToken"]').val();

    let saveRowHandler = function (e) {
        let row = $(e.target).parents('tr');
        let tds = row.children('td');
        let fields = row.find(':input');
        let formData = serializeFormJson(fields);
        //let payload = JSON.stringify(formData);

        $.ajax({
            headers: {
                'X-CSRF-Token': csrfToken
            },
            // dataType: 'json',
            // contentType: 'application/json',
            // accepts: {json: 'application/json'},
            type: 'POST',
            evalScripts: true,
            url: saveRowUrl,
            data: formData,
            success: function () {
                tds.removeClass('table-danger');
                fields.parent().popover('dispose');
                fields.parent().removeClass('table-danger');
                tds.addClass('table-success', 500).delay(1000).removeClass('table-success', 500);
            },
            error: function (jqXHR) {
                tds.addClass('table-danger', 500);

                let response = jqXHR.responseJSON;
                for (let name in response) {
                    let invalidField = fields.filter('[name="' + name + '"]');
                    if (!invalidField.parent().hasClass('table-danger')) {
                        let firstErrorKey = Object.keys(response[name])[0];
                        invalidField.parent().addClass('table-danger');
                        invalidField.parent().popover({
                            content: response[name][firstErrorKey],
                            placement: 'top',
                            trigger: 'manual'
                        }).popover('show');
                    }
                }
            }
        });
    };

    // Bind the save handler.
    bulkForm.find(':text').on("blur", saveRowHandler);
    bulkForm.find('textarea').on("change", saveRowHandler);
    bulkForm.find(':checkbox, select').on("change", saveRowHandler);
    bulkForm.find('input[type="date"]').on("change", saveRowHandler);

    // Filter form.
    const filterForm = $('#FilterAdminBulkeditForm');
    const filters = filterForm.find(':input');
    const filterRedirectUrl = filterForm.find('[name="url"]').val();

    filters.on("change", function (e) {
        const formData = serializeFormJson(filters);
        let newUrl = filterRedirectUrl + '/' + formData.session + '/' + formData.chamber
        if (formData.organization) {
            newUrl = newUrl + '/' + formData.organization
        }
        if (formData.rating) {
            newUrl = newUrl + '/' + formData.rating
        }
        window.location.replace(newUrl);
    });
});