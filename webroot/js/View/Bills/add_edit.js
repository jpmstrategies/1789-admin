$(document).ready(function () {
    // Initialize necessary variables
    var statusesUrl = $('#statuses-url').val();
    var legiscanSearchUrl = $('#legiscan-search-url').val();
    var stageId = $('#current-bill-stage-id').val();
    var selectedStatus = $('#current-bill-status-id').data('current-value');

    // Initialize the form
    initializeLegiscanAutocomplete();
    initializeStatusDropdown(stageId, selectedStatus);
    initializeFieldChangeHandlers();

    // Function to initialize the LegiScan Bill autocomplete
    function initializeLegiscanAutocomplete() {
        $('#legiscan-bill-id').select2({
            theme: 'bootstrap4',
            placeholder: "Search for a Bill",
            allowClear: true,
            minimumInputLength: 1,
            ajax: {
                url: legiscanSearchUrl,
                dataType: 'json',
                data: function (params) {
                    return {
                        chamber: $('#filed-chamber-id').val(),
                        session: $('#legislative-session-id').val(),
                        term: params.term,
                    };
                },
                processResults: function (data) {
                    return {
                        results: data.map(function (item) {
                            return {
                                id: item.legiscan_bill_id,
                                text: item.bill_number
                            };
                        })
                    };
                },
                delay: 250
            }
        });

        // Handle select and unselect events for LegiScan Bill
        $('#legiscan-bill-id').on('select2:select', function () {
            resetRatings(true);  // Clear and enable rating fields
        });

        $('#legiscan-bill-id').on('select2:unselecting', function () {
            resetRatings(false);  // Clear and disable rating fields
        });
    }

    // Function to load statuses based on selected stage
    function initializeStatusDropdown(stageId, selectedStatus) {
        if (stageId !== '') {
            loadStatuses(stageId, selectedStatus);
        }

        $('#current-bill-stage-id').change(function () {
            var newStageId = $(this).val();
            if (newStageId === '') {
                $('#current-bill-status-id').prop('disabled', true).empty();
            } else {
                loadStatuses(newStageId);
            }
        });
    }

    // Function to load statuses via AJAX and populate dropdown
    function loadStatuses(stageId, selectedStatus = null) {
        $.ajax({
            url: statusesUrl,
            type: 'GET',
            data: {stage_id: stageId},
            success: function (data) {
                var $statusDropdown = $('#current-bill-status-id');
                $statusDropdown.empty().append('<option value="">-- Select Status --</option>');
                $.each(data, function (key, value) {
                    $statusDropdown.append('<option value="' + key + '">' + value + '</option>');
                });
                if (selectedStatus) {
                    $statusDropdown.val(selectedStatus);
                }
                $statusDropdown.prop('disabled', false);
            },
            error: function () {
                alert('Unable to fetch statuses. Please try again.');
            }
        });
    }

    // Function to handle changes in chamber and session fields
    function initializeFieldChangeHandlers() {
        $('#filed-chamber-id, #legislative-session-id').change(function () {
            resetLegiScanBillAndRatingFields();
            enableLegiscanBillField();
        });

        enableLegiscanBillField();
    }

    // Enable or disable the LegiScan Bill field based on chamber and session selection
    function enableLegiscanBillField() {
        var chamberSelected = $('#filed-chamber-id').val() !== '';
        var sessionSelected = $('#legislative-session-id').val() !== '';
        $('#legiscan-bill-id').prop('disabled', !(chamberSelected && sessionSelected));
    }

    // Reset the LegiScan Bill and rating fields
    function resetLegiScanBillAndRatingFields() {
        resetRatings(false);  // Disable and clear ratings
        $('#legiscan-bill-id').val(null).trigger('change');  // Reset LegiScan Bill
    }

    // Reset the rating fields based on enable/disable flag
    function resetRatings(enable) {
        $('#house-rating-id, #senate-rating-id').val('');
    }
});
