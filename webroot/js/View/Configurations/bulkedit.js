jQuery(document).ready(function ($) {
    // Get submit url from the form.
    let recordForm = $("#ConfigurationsBulkEdit");
    let saveRowUrl = $("[name='saveUrl']").val();
    let csrfToken = $('[name="_csrfToken"]').val();

    function saveCheckboxSelect(obj, newValue, type) {
        let row = jQuery(obj).parents("div.field");
        let idField = row.find(":input#id").val();
        let defaultValue = row.find(":input#default-value").val();

        let formData = {};
        formData.id = idField;
        formData.value = newValue;
        //let payload = JSON.stringify(formData);

        $.ajax({
            headers: {
                'X-CSRF-Token': csrfToken
            },
            // dataType: "json",
            // contentType: "application/json",
            // accepts: {json: "application/json"},
            type: "POST",
            evalScripts: true,
            url: saveRowUrl,
            data: formData,
            success: function (data) {
                if (idField === data.id) {
                    // Record was updated.
                    row.removeClass("error-message", 500).removeClass("alert-warning", 500).addClass("alert-success", 500).delay(1000).removeClass("alert-success", 500).addClass("default-changed", 500);

                    if (newValue === defaultValue) {
                        if (type === 'select') {
                            row.find("div.btn-reset").hide();
                        }
                        row.removeClass("default-changed", 500);
                        row.removeClass("alert-warning", 500);
                    } else {
                        if (type === 'select') {
                            row.find("div.btn-reset").show();
                        } else {
                            row.addClass("alert-warning", 500);
                        }
                        row.addClass("default-changed", 500);
                    }

                    // update current_value
                    row.find(":input#current-value").val(newValue);
                }
            },
            error: function () {
                row.addClass("error-message", 500);
            }
        });
    }

    recordForm.find(":input").on("keyup", function () {
        let row = jQuery(this).parents("div.field");

        // show reset button if not readonly
        if (jQuery(this).prop("readonly") === false) {
            row.find("div.btn-reset").hide();
            row.find("div.btn-changes").show();
        }
    });

    recordForm.on('change', 'select', function (e) {
        let newValue = $(e.target).val();

        saveCheckboxSelect(this, newValue, 'select');
    });

    recordForm.find(":checkbox").on("change", function () {
        let newValue = (this.checked ? 'yes' : 'no');

        saveCheckboxSelect(this, newValue, 'checkbox');
    });

    recordForm.find(":button#submit-button").on("click", function () {
        let row = jQuery(this).parents("div.field");
        let idField = row.find(":input#id").val();
        let newValue = row.find(":input#value").val();
        let defaultValue = row.find(":input#default-value").val();

        let formData = {};
        formData.id = idField;
        formData.value = newValue;
        //let payload = JSON.stringify(formData);

        $.ajax({
            headers: {
                'X-CSRF-Token': csrfToken
            },
            // dataType: "json",
            // contentType: "application/json",
            // accepts: {json: "application/json"},
            type: "POST",
            evalScripts: true,
            url: saveRowUrl,
            data: formData,
            success: function (data) {
                if (idField === data.id) {
                    // Record was updated.
                    row.removeClass("error-message", 500).addClass("alert-success", 500).delay(1000).removeClass("alert-success", 500).addClass("default-changed", 500);

                    // show reset button
                    row.find("div.btn-changes").hide();

                    if (newValue === defaultValue) {
                        row.find("div.btn-reset").hide();
                        row.removeClass("default-changed", 500);
                    } else {
                        row.find("div.btn-reset").show();
                        row.addClass("default-changed", 500);
                    }

                    // update current_value
                    row.find(":input#current-value").val(newValue);
                }
            },
            error: function () {
                row.addClass("error-message", 500);
            }
        });
    });

    recordForm.find(":button#cancel-button").on("click", function () {
        let row = jQuery(this).parents("div.field");

        let idField = row.find(":input#id").val();
        let newValue = row.find(":input#current-value").val();
        let defaultValue = row.find(":input#default-value").val();

        let formData = {};
        formData.id = idField;
        formData.value = newValue;
        //let payload = JSON.stringify(formData);

        $.ajax({
            headers: {
                'X-CSRF-Token': csrfToken
            },
            // dataType: "json",
            // contentType: "application/json",
            // accepts: {json: "application/json"},
            type: "POST",
            evalScripts: true,
            url: saveRowUrl,
            data: formData,
            success: function (data) {
                if (idField === data.id) {
                    // Record was updated.
                    row.removeClass("error-message", 500).addClass("alert-success", 500).delay(1000).removeClass("alert-success", 500);

                    row.find("div.btn-changes").hide();

                    if (newValue === defaultValue) {
                        row.find("div.btn-reset").hide();
                        row.removeClass("default-changed", 500);
                    } else {
                        row.find("div.btn-reset").show();
                        row.addClass("default-changed", 500);
                    }

                    // update current_value
                    row.find(":input#value").val(newValue);
                    row.find(":input#current-value").val(newValue);
                }
            },
            error: function () {
                row.addClass("error-message", 500);
            }
        });
    });

    recordForm.find(":button#reset-default-button").on("click", function () {
        let row = jQuery(this).parents("div.field");

        let idField = row.find(":input#id").val();
        let newValue = row.find(":input#default-value").val();

        let formData = {};
        formData.id = idField;
        formData.value = newValue;
        //let payload = JSON.stringify(formData);

        $.ajax({
            headers: {
                'X-CSRF-Token': csrfToken
            },
            // dataType: "json",
            // contentType: "application/json",
            // accepts: {json: "application/json"},
            type: "POST",
            evalScripts: true,
            url: saveRowUrl,
            data: formData,
            success: function (data) {
                if (idField === data.id) {
                    // Record was updated.
                    row.removeClass("error-message", 500).addClass("alert-success", 500).delay(1000).removeClass("alert-success", 500).removeClass("default-changed", 500);

                    // support selects
                    row.removeClass("default-changed", 500);
                    row.removeClass("alert-warning", 500);

                    // hide reset button
                    row.find("div.btn-reset").hide();

                    // update current_value
                    row.find(":input#value").val(newValue);
                    row.find(":input#current-value").val(newValue);
                }
            },
            error: function () {
                row.addClass("error-message", 500);
            }
        });
    });
});