jQuery(document).ready(function () {
    //
    // Title - initial load logic
    //
    let titleSelect = $("#title-id");
    let chamberName = $("#chamber-id option:selected").text();

    // check if selected
    if (titleSelect.val() !== '') {

        // disable all option groups
        titleSelect.hideOptionGroup();

        // show new correct group
        let optionGroup = titleSelect.find("optgroup[label='" + chamberName + "']");

        optionGroup.prop("disabled", false);

        // enable title select
        titleSelect.prop("disabled", false).show();
    }

    $("#chamber-id").on("change", function () {
        // get current label text
        let titleSelect = $("#title-id");
        chamberName = $("#chamber-id option:selected").text();

        if (chamberName === '') {
            // unset current title
            titleSelect.find('option:selected').remove();

            // add blank title
            titleSelect.find('option[value=""]').remove();
            titleSelect.prepend("<option value='' selected='selected'></option>").prop("disabled", false);

            // disable title
            titleSelect.prop("disabled", true);

        } else {
            // unset current title
            titleSelect.find('option:selected').remove();

            // disable all option groups
            titleSelect.hideOptionGroup();

            // add blank title
            titleSelect.find('option[value=""]').remove();
            titleSelect.prepend("<option value='' selected='selected'></option>").prop("disabled", false);

            // show new correct group
            let optionGroup = titleSelect.find("optgroup[label='" + chamberName + "']");

            optionGroup.prop("disabled", false);

            // enable title select
            titleSelect.prop("disabled", false).show();
        }
    });
});

$.fn.hideOptionGroup = function () {
    $(this).hide();
    $(this).children().each(function () {
        $(this).attr("disabled", "disabled").removeAttr("selected");
    });
    $(this).appendTo($(this).parent());
}